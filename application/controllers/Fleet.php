<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fleet extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->model('Fleets');
        $this->load->model('orders');
    }



    public function index()
    {

        $data['fleets'] = $this->Fleets->getFleets();
        $data['fleettype'] = $this->Fleets->getFleettypes();
        $data['id'] = $this->Fleets->getLastID();


        $this->template->load('template', 'pages/fleets', $data);
    }

    public function saveFleets()
    {
        $this->Fleets->Simpan();
        redirect('Fleet?msg=Save Success');
    }

    public function saveEdit()

    {
        $this->Fleets->editFleets();
        redirect('fleet?msg=Save Success');
    }

    public function deleteFleets()
    {
        $this->Fleets->delete();
        redirect('Fleet?msg=Delete Success');
    }

    public function modaledit()
    {
        $data['fleets'] = $this->Fleets->getOneOrder();
        $data['fleettype'] = $this->Fleets->getFleettypes();


        $this->load->view('pages/modalFleets', $data);
    }

    public function autoComplete()
    {
        $q = trim($this->input->get("q")) != "" ? trim(strtolower($this->db->escape_str($this->input->get("q")))) : "";

        $this->db->select("id, fleetplateno");
        $this->db->where("active", 1);
        if ($q != "") {
            $this->db->like("LOWER(fleetplateno)", $q);
        }
        $result = $this->db->get("fleets");
        $rows = $result->result();

        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $data[] = array('id' => $row->id, 'text' => $row->fleetplateno);
            }
        } else {
            $data[] = array('id' => '', 'text' => 'No Data Found');
        }

        echo json_encode($data);
    }

    public function modaldetailreport()
    {
        if ($this->input->get_post('export') == 'excel') {
            $this->load->library('PHPExcel');
            $data['search'] = array();
            $this->load->model('Fleets');
            $rows = $this->orders->getOrderReport($data);

            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()
                ->setCreator('Taylor Ren')
                ->setTitle('PHPExcel Demo')
                ->setLastModifiedBy('Taylor Ren')
                ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
                ->setSubject('PHP Excel manipulation')
                ->setKeywords('excel php office phpexcel lakers')
                ->setCategory('programming');
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
                    )
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A3:T3')->applyFromArray($styleArray);

            $styleArra = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            //unset($styleArray);

            $ews = $objPHPExcel->getSheet(0);
            $ews->setTitle('Data');

            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
            // Assign cell values
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Truck Order Summary');
            $objPHPExcel->getActiveSheet()->mergeCells('A1:T1');
            $objPHPExcel->getActiveSheet()->getStyle("A1:T1")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getFill()->getStartColor()->setARGB('ffffcc');
            $objPHPExcel->getActiveSheet()->getStyle("A1:T1")->getFont()->setSize(16);

            foreach (range('A', 'U') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setWidth(20);
            }


            $filtertext = "";
            if ($this->input->get('fleetplateno') != '') {
                $filtertext .= " Plateno = " . $this->input->get('fleetplateno');
            }
            if ($this->input->get('type') != '') {
                $filtertext .= " Type = " . $this->input->get('type');
            }
            if ($this->input->get('customer') != '') {
                $filtertext .= " Customer = " . $this->input->get('customer');
            }
            if ($this->input->get('code') != '') {
                $filtertext .= " Fleet Code = " . $this->input->get('code');
            }
            if ($this->input->get('year') != '') {
                $filtertext .= " Year = " . $this->input->get('year');
            }
            if ($this->input->get('month') != '') {
                $filtertext .= " Month = " . date('F', strtotime("2018-" . $this->input->get('month') . "-01"));
            }
            $filters = ($filtertext != "" ? $filtertext : "All Data");

            $objPHPExcel->getActiveSheet()->mergeCells('A2:T2');
            $objPHPExcel->getActiveSheet()->getStyle("A2:T2")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getFill()->getStartColor()->setARGB('eaeaea');
            $objPHPExcel->getActiveSheet()->getStyle("A2:T2")->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Filter : " . $filters);

            $objPHPExcel->getActiveSheet()->getStyle("A3:T3")->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle('A3:T3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A3:T3')->getFill()->getStartColor()->setARGB('cce6ff');
            $objPHPExcel->getActiveSheet()->getStyle("A3:T3")->getFont()->setSize(12);

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'Tanggal');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, 'EMKL');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, 'Pabrik');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'Tujuan');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3, 'No Container');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 3, 'Total Tagihan');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 3, 'UJ');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 3, 'Tabungan');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 3, 'Komisi');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 3, 'Ops');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 3, 'Depo');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 3, 'Lolo');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 3, 'Parkir');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 3, 'SP2');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 3, 'Kawalan');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, 3, 'Angsur');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, 3, 'RC');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, 3, 'No. Inv');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, 3, 'Tgl. Bayar');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, 3, 'LABA');

            if ($rows != null) {
                $prices = 0;
                $ritase = 0;
                $tunitprice = 0;
                $tprice = 0;
                $ttabsupir = 0;
                $tkomsupir = 0;
                $tlolo = 0;
                $tdepo = 0;
                $top = 0;
                $tparkir = 0;
                $tlembur = 0;
                $tkawalan = 0;
                $trc = 0;
                $allowancereds = 0;
                $no  = 1;
                $baris = 4;
                foreach ($rows as $row) {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, date('d-m-Y', strtotime($row->orderdate)));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->cname);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->n1);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->c2);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->frameno);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris, $row->unitprice);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $baris, $row->prices);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $baris, $row->tabSupir);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $baris, $row->komSupir);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $baris, $row->OPS);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $baris, $row->DEPO);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $baris, $row->LOLO);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $baris, $row->PARKIR);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $baris, $row->SP2);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $baris, $row->KAWALAN);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $baris, 0);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $baris, $row->RC);


                    $no++;
                    $baris++;
                    $tunitprice += $row->unitprice;
                    $tprice += $row->prices;
                    $ttabsupir += $row->tabSupir;
                    $tkomsupir += $row->komSupir;
                    $tlolo += $row->LOLO;
                    $tdepo += $row->DEPO;
                    $top += $row->OPS;
                    $tparkir += $row->PARKIR;
                    $tlembur += $row->SP2;
                    $tkawalan += $row->KAWALAN;
                    $trc += $row->RC;
                }

                $barisfooter = $baris;
                $barisbaru = $barisfooter - 1;
                $objPHPExcel->getActiveSheet()->getStyle("A3:T$barisfooter")->applyFromArray($styleArra);
                $objPHPExcel->getActiveSheet()->getStyle("A3:T$barisfooter")->getNumberFormat()->setFormatCode('#,##0');
                //$sheet->getStyle('A1:B2')->getNumberFormat()->setFormatCode('#,##0.00');

                unset($styleArra);

                $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $barisfooter, $tunitprice);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $barisfooter, $tprice);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $barisfooter, $ttabsupir);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $barisfooter, $tkomsupir);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $barisfooter, $top);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $barisfooter, $tdepo);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $barisfooter, $tlolo);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $barisfooter, $tparkir);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $barisfooter, $tlembur);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $barisfooter, $tkawalan);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $barisfooter, $trc);
            } else {
                $objPHPExcel->getActiveSheet()->setCellValue('A4:I4', 'Data Not Found');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('e6f2ff');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(14);
            }




            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

            header('Content-Disposition: attachment;filename="Report Truck' . $filtertext . '.xlsx"');
            $writer->save('php://output');
        } else {
            $data['search'] = array();
            $rows['list'] = $this->orders->getOrderReport($data);
            $this->load->view('pages/modaldetailreport', $rows);
        }
    }

    public function lastPosition()
    {
        //$kilat = $this->load->database('kilat', true);
        $plateno = $this->input->post('plateno') != '' ? $this->input->post('plateno') : '';
        $where = "";
        if ($plateno != "") {
            $where .= " AND gps.gps_vehiclenumber LIKE '%" . $plateno . "%'";
        }
        $sql = "SELECT
                gps.gps_deviceid
                ,
                gps_vehiclenumber,
                gps.gps_datetime
                ,
                gps.gps_address
                ,
                gps.gps_lat
                ,
                gps.gps_long
                ,
                gps.gps_speed
                ,
                gps.gps_acc
                ,
                gps.gps_odometer
                ,
                gps.gps_hotport
                ,
                NULL gps_datetiemhotpot 
            FROM
                db_cpk_synergy.dbo.data_gps_fleet gps
                RIGHT OUTER JOIN (
                SELECT MAX
                    ( gps_id ) gps_id,
                    gps_deviceid 
                FROM
                    db_cpk_synergy.dbo.data_gps_fleet 
                WHERE
                    gps_deviceid IN ( SELECT fleetnumber FROM fleets ) 
                GROUP BY
                    gps_deviceid 
                ) a ON a.gps_id = gps.gps_id 
            WHERE
                gps_vehiclenumber IS NOT NULL 
                $where
            ORDER BY
                gps_datetime DESC
               ";

        $result = $this->db->query($sql);
        //$result = $kilat->query($sql);
        $data['row'] = $result->row();
        $this->load->view('charts/maps', $data);
    }

    function ordersummary($page = 0)
    {
        if ($this->input->get('export') == 'excel') {

            $this->load->library('PHPExcel');
            $data['search'] = array();
            $this->load->model('Fleets');
            $rows = $this->Fleets->ordersummary($data);

            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()
                ->setCreator('Taylor Ren')
                ->setTitle('PHPExcel Demo')
                ->setLastModifiedBy('Taylor Ren')
                ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
                ->setSubject('PHP Excel manipulation')
                ->setKeywords('excel php office phpexcel lakers')
                ->setCategory('programming');

            $ews = $objPHPExcel->getSheet(0);
            $ews->setTitle('Data');

            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
            // Assign cell values
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Truck Order Summary');
            $objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
            $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setARGB('ffffcc');
            $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setSize(16);



            $filtertext = "";
            if ($this->input->get('fleetplateno') != '') {
                $filtertext .= "Plateno = " . $this->input->get('fleetplateno');
            }
            if ($this->input->get('type') != '') {
                $filtertext .= " Type = " . $this->input->get('type');
            }
            if ($this->input->get('customer') != '') {
                $filtertext .= " Customer = " . $this->input->get('customer');
            }
            if ($this->input->get('code') != '') {
                $filtertext .= " Fleet Code = " . $this->input->get('code');
            }
            if ($this->input->get('year') != '') {
                $filtertext .= " Year = " . $this->input->get('year');
            }
            if ($this->input->get('month') != '') {
                $filtertext .= " Month = " . date('F', strtotime("2018-" . $this->input->get('month') . "-01"));
            }
            $filters = ($filtertext != "" ? $filtertext : "All Data");

            $objPHPExcel->getActiveSheet()->mergeCells('A2:T2');
            $objPHPExcel->getActiveSheet()->getStyle("A2:T2")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getFill()->getStartColor()->setARGB('eaeaea');
            $objPHPExcel->getActiveSheet()->getStyle("A2:T2")->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Filter : " . $filters);

            $objPHPExcel->getActiveSheet()->getStyle("A3:I3")->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->getStartColor()->setARGB('cce6ff');
            $objPHPExcel->getActiveSheet()->getStyle("A3:I3")->getFont()->setSize(12);

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'NO');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, 'CUSTOMER');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, 'FLEET PLATENO');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'RITASE');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3, 'SALES');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 3, 'ALLOWANCES');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 3, 'ADDITIONAL');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 3, 'COST');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 3, 'GROSS MARGIN');

            if ($rows != null) {
                $prices = 0;
                $ritase = 0;
                $allowance = 0;
                $allowanceadds = 0;
                $allowancereds = 0;
                $no  = 1;
                $baris = 4;
                foreach ($rows as $row) {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->namac);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->fleetplateno);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->ritase);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->allowances);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris, $row->prices);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $baris, $row->allowanceadds);

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $baris, $row->prices + $row->allowanceadds);


                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $baris, $row->allowances - ($row->prices + $row->allowanceadds));

                    $ritase += $row->ritase;
                    $allowance += $row->allowances;
                    $allowanceadds += $row->allowanceadds;
                    $prices += $row->prices;

                    $no++;
                    $baris++;
                }

                $barisfooter = $baris;
                $barisbaru = $barisfooter - 1;
                $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $barisfooter, $ritase);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $barisfooter, $allowance);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $barisfooter, $prices);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $barisfooter, $allowanceadds);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $barisfooter, $prices + $allowanceadds);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $barisfooter, $allowance - ($prices + $allowanceadds));

                //LABEL
                $dsl1 = array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$E$3', NULL, 1),
                );
                $dsl2 = array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$D$3', NULL, 1),
                );
                $dsl3 = array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$F$3', NULL, 1),
                );


                //AXIS
                $xal = array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$C$4:$C$' . $barisbaru . '', NULL, $barisbaru),
                );

                //SERIES

                //SERIES1 
                $dsv1 = array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$E$4:$E$' . $barisbaru . '', NULL, $barisbaru),
                );
                $series1 = new PHPExcel_Chart_DataSeries(
                    PHPExcel_Chart_DataSeries::TYPE_BARCHART,       // plotType
                    PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,  // plotGrouping
                    range(0, count($dsv1) - 1),          // plotOrder
                    $dsl1,                             // plotLabel
                    $xal,                               // plotCategory
                    $dsv1                             // plotValues
                );
                //  Set additional dataseries parameters
                //      Make it a vertical column rather than a horizontal bar graph
                $series1->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

                // SERIES2
                $dsv2 = array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$D$4:$D$' . $barisbaru . '', NULL, $barisbaru),
                );
                $series2 = new PHPExcel_Chart_DataSeries(
                    PHPExcel_Chart_DataSeries::TYPE_LINECHART,      // plotType
                    PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   // plotGrouping
                    range(0, count($dsv2) - 1),          // plotOrder
                    $dsl2,                             // plotLabel
                    NULL,                                           // plotCategory
                    $dsv2                              // plotValues
                );


                $dsv3 = array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$f$4:$f$' . $barisbaru . '', NULL, $barisbaru),
                );

                $series3 = new PHPExcel_Chart_DataSeries(
                    PHPExcel_Chart_DataSeries::TYPE_AREACHART,      // plotType
                    PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   // plotGrouping
                    range(0, count($dsv2) - 1),          // plotOrder
                    $dsl3,                             // plotLabel
                    NULL,                                           // plotCategory
                    $dsv3                              // plotValues
                );


                //$ds=new PHPExcel_Chart_DataSeries(
                //        PHPExcel_Chart_DataSeries::TYPE_LINECHART,
                //        PHPExcel_Chart_DataSeries::GROUPING_STANDARD,
                //        range(0, count($dsv)-1),
                //        $dsl,
                //        $xal,
                //        $dsv
                //        );

                //  Set the series in the plot area
                $pa = new PHPExcel_Chart_PlotArea(NULL, array($series1, $series2, $series3));
                //  Set the chart legend
                $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

                $title = new PHPExcel_Chart_Title('Average Weather Chart for Crete');

                //$pa=new PHPExcel_Chart_PlotArea(NULL, array($ds));
                $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
                $title = new PHPExcel_Chart_Title('RITASE / SALES');
                $chart = new PHPExcel_Chart(
                    'chart1',
                    $title,
                    $legend,
                    $pa,
                    true,
                    0,
                    NULL,
                    NULL
                );
                $chart->setTopLeftPosition('K1');
                $chart->setBottomRightPosition('AE23');
                $ews->addChart($chart);
            } else {
                $objPHPExcel->getActiveSheet()->setCellValue('A4:I4', 'Data Not Found');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('e6f2ff');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(14);
            }




            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            $writer->setIncludeCharts(true);
            //$writer->save('output.xlsx');
            // Save it as an excel 2003 file
            //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            //Nama File
            header('Content-Disposition: attachment;filename="TruckOrderSummary' . date('dmy') . '.xlsx"');
            $writer->save('php://output');
        } else {
            $perpage = 20;

            $data = array();
            $filter = array();
            $data['search'] = array();

            $limit  = $perpage;
            $offset = $page > 1 ? ($limit * $page) - $limit : 0;

            $all_data = $this->Fleets->ordersummary($filter);
            $data['total'] = count($all_data);
            $config['base_url'] = base_url() . 'fleet/ordersummary/';
            $config['use_page_numbers'] = TRUE;
            $config['total_rows'] = $data['total'];
            $config['per_page'] = $perpage;
            $config['num_links'] = 1;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = 'First';
            $config['first_tag_open'] = '<li class="prev page">';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = 'Last';
            $config['last_tag_open'] = '<li class="next page">';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li class="next page">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li class="prev page">';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a>';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="page">';
            $config['num_tag_close'] = '</li>';
            $config['anchor_class'] = 'follow_link';

            $this->pagination->initialize($config);
            $paginator = $this->pagination->create_links();
            $data['pagination'] = $paginator;

            //$data["page"] = $page;
            ///$data["offset"] = $offset;
            //$data["limit"] = $limit;

            $filter["limit"]  = $limit;
            $filter["page"] = $page;
            $filter["offset"] = $offset;


            $totalcar = $this->Fleets->getFleets();
            $data["totalcar"] = count($totalcar);
            $data["office"] = $this->Fleets->costoffice($filter);
            $data["result"] = $this->Fleets->ordersummary($filter);
            $data["row"] = $offset;
            echo json_encode($data);
        }
    }


    function costsummary($page = 0)
    {
        if ($this->input->get('export') == 'excel') {

            $this->load->library('PHPExcel');
            $data['search'] = array();
            $this->load->model('Fleets');
            $rows = $this->Fleets->costsummary($data);

            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()
                ->setCreator('Total Kilat')
                ->setTitle('Total Kilat')
                ->setLastModifiedBy('Total Kilat')
                ->setDescription('Total Kilat - GPS and System Solution')
                ->setSubject('Total Kilat')
                ->setKeywords('Total Kilat')
                ->setCategory('Total Kilat');

            $ews = $objPHPExcel->getSheet(0);
            $ews->setTitle('Data');

            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
            // Assign cell values
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Driver Summary');
            $objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
            $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setARGB('ffffcc');
            $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setSize(16);



            $filtertext = "";
            if ($this->input->get('fleetplateno') != '') {
                $filtertext .= "Plateno = " . $this->input->get('fleetplateno');
            }
            if ($this->input->get('type') != '') {
                $filtertext .= " Type = " . $this->input->get('type');
            }
            if ($this->input->get('customer') != '') {
                $filtertext .= " Customer = " . $this->input->get('customer');
            }
            if ($this->input->get('code') != '') {
                $filtertext .= " Fleet Code = " . $this->input->get('code');
            }
            if ($this->input->get('year') != '') {
                $filtertext .= " Year = " . $this->input->get('year');
            }
            if ($this->input->get('month') != '') {
                $filtertext .= " Month = " . date('F', strtotime("2018-" . $this->input->get('month') . "-01"));
            }
            $filters = ($filtertext != "" ? $filtertext : "All Data");

            $objPHPExcel->getActiveSheet()->mergeCells('A2:T2');
            $objPHPExcel->getActiveSheet()->getStyle("A2:T2")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getFill()->getStartColor()->setARGB('eaeaea');
            $objPHPExcel->getActiveSheet()->getStyle("A2:T2")->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Filter : " . $filters);

            $objPHPExcel->getActiveSheet()->getStyle("A3:I3")->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->getStartColor()->setARGB('cce6ff');
            $objPHPExcel->getActiveSheet()->getStyle("A3:I3")->getFont()->setSize(12);

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'NO');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, 'FLEET NO');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, 'NOMINAL');
            if ($rows != null) {
                $prices = 0;
                $ritase = 0;
                $allowance = 0;
                $allowanceadds = 0;
                $allowancereds = 0;
                $no  = 1;
                $baris = 4;
                foreach ($rows as $row) {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->fleetplateno);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->totalcost);

                    $ritase += $row->totalcost;
                    $no++;
                    $baris++;
                }

                $barisfooter = $baris;
                $barisbaru = $barisfooter - 1;
                $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $barisfooter, $ritase);
            } else {
                $objPHPExcel->getActiveSheet()->setCellValue('A4:I4', 'Data Not Found');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('e6f2ff');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(14);
            }




            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');


            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            //Nama File
            header('Content-Disposition: attachment;filename="Laporan Biaya Tambahan' . date('dmy') . '.xlsx"');
            $writer->save('php://output');
        } else {
            $perpage = 50;

            $data = array();
            $filter = array();
            $data['search'] = array();

            $limit  = $perpage;
            $offset = $page > 1 ? ($limit * $page) - $limit : 0;

            $all_data = $this->Fleets->costsummary($filter);
            $data['total'] = count($all_data);
            $config['base_url'] = base_url() . '/fleet/driversummary';
            $config['use_page_numbers'] = TRUE;
            $config['total_rows'] = $data['total'];
            $config['per_page'] = $perpage;
            $config['num_links'] = 1;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = 'First';
            $config['first_tag_open'] = '<li class="prev page">';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = 'Last';
            $config['last_tag_open'] = '<li class="next page">';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li class="next page">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li class="prev page">';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a>';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="page">';
            $config['num_tag_close'] = '</li>';
            $config['anchor_class'] = 'follow_link';

            $this->pagination->initialize($config);
            $paginator = $this->pagination->create_links();
            $data['pagination'] = $paginator;

            //$data["page"] = $page;
            ///$data["offset"] = $offset;
            //$data["limit"] = $limit;

            $filter["limit"]  = $limit;
            $filter["page"] = $page;
            $filter["offset"] = $offset;

            $data["result"] = $this->Fleets->costsummary($filter);
            $data["row"] = $offset;
            echo json_encode($data);
        }
    }


    public function reportDriver()
    {
        $this->template->load('template', 'pages/reportDriver');
    }

    public function reportDriverver2()
    {
        $this->template->load('template', 'pages/reportDriver_ver2');
    }

    public function reportTruckKR()
    {
        $this->template->load('template', 'pages/reportTruckKR');
    }

    function driversummary($page = 0)
    {
        if ($this->input->get('export') == 'excel') {

            $this->load->library('PHPExcel');
            $data['search'] = array();
            $this->load->model('Fleets');
            $rows = $this->Fleets->driversummary($data);

            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()
                ->setCreator('Total Kilat')
                ->setTitle('Total Kilat')
                ->setLastModifiedBy('Total Kilat')
                ->setDescription('Total Kilat - GPS and System Solution')
                ->setSubject('Total Kilat')
                ->setKeywords('Total Kilat')
                ->setCategory('Total Kilat');

            $ews = $objPHPExcel->getSheet(0);
            $ews->setTitle('Data');

            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
            // Assign cell values
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Driver Summary');
            $objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
            $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setARGB('ffffcc');
            $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setSize(16);



            $filtertext = "";
            if ($this->input->get('fleetplateno') != '') {
                $filtertext .= "Plateno = " . $this->input->get('fleetplateno');
            }
            if ($this->input->get('type') != '') {
                $filtertext .= " Type = " . $this->input->get('type');
            }
            if ($this->input->get('customer') != '') {
                $filtertext .= " Customer = " . $this->input->get('customer');
            }
            if ($this->input->get('code') != '') {
                $filtertext .= " Fleet Code = " . $this->input->get('code');
            }
            if ($this->input->get('year') != '') {
                $filtertext .= " Year = " . $this->input->get('year');
            }
            if ($this->input->get('month') != '') {
                $filtertext .= " Month = " . date('F', strtotime("2018-" . $this->input->get('month') . "-01"));
            }
            $filters = ($filtertext != "" ? $filtertext : "All Data");

            $objPHPExcel->getActiveSheet()->mergeCells('A2:T2');
            $objPHPExcel->getActiveSheet()->getStyle("A2:T2")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getFill()->getStartColor()->setARGB('eaeaea');
            $objPHPExcel->getActiveSheet()->getStyle("A2:T2")->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Filter : " . $filters);

            $objPHPExcel->getActiveSheet()->getStyle("A3:I3")->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->getStartColor()->setARGB('cce6ff');
            $objPHPExcel->getActiveSheet()->getStyle("A3:I3")->getFont()->setSize(12);

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'NO');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, 'DRIVER');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, 'CUSTOMER');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'FLEET TYPE');
            //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3,'ORIGIN');
            //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 3,'DESTINATION');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3, 'RITASE');
            if ($rows != null) {
                $prices = 0;
                $ritase = 0;
                $allowance = 0;
                $allowanceadds = 0;
                $allowancereds = 0;
                $no  = 1;
                $baris = 4;
                foreach ($rows as $row) {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->drivernama);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->namac);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->fleettype_name);
                    //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->n1);
                    //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris, $row->c2);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->ritase);

                    $ritase += $row->ritase;
                    $no++;
                    $baris++;
                }

                $barisfooter = $baris;
                $barisbaru = $barisfooter - 1;
                $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $barisfooter, $ritase);

                //LABEL
                $dsl1 = array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$E$3', NULL, 1),
                );
                $dsl2 = array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$D$3', NULL, 1),
                );
                $dsl3 = array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$F$3', NULL, 1),
                );


                //AXIS
                $xal = array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$B$4:$B$' . $barisbaru . '', NULL, $barisbaru),
                );

                //SERIES

                //SERIES1 
                $dsv1 = array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$E$4:$E$' . $barisbaru . '', NULL, $barisbaru),
                );
                $series1 = new PHPExcel_Chart_DataSeries(
                    PHPExcel_Chart_DataSeries::TYPE_BARCHART,       // plotType
                    PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,  // plotGrouping
                    range(0, count($dsv1) - 1),          // plotOrder
                    $dsl1,                             // plotLabel
                    $xal,                               // plotCategory
                    $dsv1                             // plotValues
                );
                //  Set additional dataseries parameters
                //      Make it a vertical column rather than a horizontal bar graph
                $series1->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

                // SERIES2
                $dsv2 = array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$D$4:$D$' . $barisbaru . '', NULL, $barisbaru),
                );
                $series2 = new PHPExcel_Chart_DataSeries(
                    PHPExcel_Chart_DataSeries::TYPE_LINECHART,      // plotType
                    PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   // plotGrouping
                    range(0, count($dsv2) - 1),          // plotOrder
                    $dsl2,                             // plotLabel
                    NULL,                                           // plotCategory
                    $dsv2                              // plotValues
                );


                $dsv3 = array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$f$4:$f$' . $barisbaru . '', NULL, $barisbaru),
                );

                $series3 = new PHPExcel_Chart_DataSeries(
                    PHPExcel_Chart_DataSeries::TYPE_AREACHART,      // plotType
                    PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   // plotGrouping
                    range(0, count($dsv2) - 1),          // plotOrder
                    $dsl3,                             // plotLabel
                    NULL,                                           // plotCategory
                    $dsv3                              // plotValues
                );


                //$ds=new PHPExcel_Chart_DataSeries(
                //        PHPExcel_Chart_DataSeries::TYPE_LINECHART,
                //        PHPExcel_Chart_DataSeries::GROUPING_STANDARD,
                //        range(0, count($dsv)-1),
                //        $dsl,
                //        $xal,
                //        $dsv
                //        );

                //  Set the series in the plot area
                $pa = new PHPExcel_Chart_PlotArea(NULL, array($series1));
                //  Set the chart legend
                $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

                $title = new PHPExcel_Chart_Title('Average Weather Chart for Crete');

                //$pa=new PHPExcel_Chart_PlotArea(NULL, array($ds));
                $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
                $title = new PHPExcel_Chart_Title('DRIVER / RITASE');
                $chart = new PHPExcel_Chart(
                    'chart1',
                    $title,
                    $legend,
                    $pa,
                    true,
                    0,
                    NULL,
                    NULL
                );
                $chart->setTopLeftPosition('K1');
                $chart->setBottomRightPosition('AE23');
                $ews->addChart($chart);
            } else {
                $objPHPExcel->getActiveSheet()->setCellValue('A4:I4', 'Data Not Found');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('e6f2ff');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(14);
            }




            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            $writer->setIncludeCharts(true);
            //$writer->save('output.xlsx');
            // Save it as an excel 2003 file
            //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            //Nama File
            header('Content-Disposition: attachment;filename="TruckOrderSummary' . date('dmy') . '.xlsx"');
            $writer->save('php://output');
        } else {
            $perpage = 50;

            $data = array();
            $filter = array();
            $data['search'] = array();

            $limit  = $perpage;
            $offset = $page > 1 ? ($limit * $page) - $limit : 0;

            $all_data = $this->Fleets->driversummary($filter);
            $data['total'] = count($all_data);
            $config['base_url'] = base_url() . '/fleet/driversummary';
            $config['use_page_numbers'] = TRUE;
            $config['total_rows'] = $data['total'];
            $config['per_page'] = $perpage;
            $config['num_links'] = 1;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = 'First';
            $config['first_tag_open'] = '<li class="prev page">';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = 'Last';
            $config['last_tag_open'] = '<li class="next page">';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li class="next page">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li class="prev page">';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a>';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="page">';
            $config['num_tag_close'] = '</li>';
            $config['anchor_class'] = 'follow_link';

            $this->pagination->initialize($config);
            $paginator = $this->pagination->create_links();
            $data['pagination'] = $paginator;

            //$data["page"] = $page;
            ///$data["offset"] = $offset;
            //$data["limit"] = $limit;

            $filter["limit"]  = $limit;
            $filter["page"] = $page;
            $filter["offset"] = $offset;

            $data["result"] = $this->Fleets->driversummary($filter);
            $data["row"] = $offset;
            echo json_encode($data);
        }
    }

    function ordersummaryKR($page = 0)
    {
        if ($this->input->get('export') == 'excel') {

            $this->load->library('PHPExcel');
            $data['search'] = array();
            $this->load->model('Fleets');
            $rows = $this->Fleets->ordersummary($data);

            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()
                ->setCreator('Total Kilat')
                ->setTitle('Total Kilat')
                ->setLastModifiedBy('Total Kilat')
                ->setDescription('Total Kilat - GPS and System Solution')
                ->setSubject('Total Kilat')
                ->setKeywords('Total Kilat')
                ->setCategory('Total Kilat');

            $ews = $objPHPExcel->getSheet(0);
            $ews->setTitle('Data');

            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
            // Assign cell values
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Driver Summary');
            $objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
            $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setARGB('ffffcc');
            $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setSize(16);



            $filtertext = "";
            if ($this->input->get('fleetplateno') != '') {
                $filtertext .= "Plateno = " . $this->input->get('fleetplateno');
            }
            if ($this->input->get('type') != '') {
                $filtertext .= " Type = " . $this->input->get('type');
            }
            if ($this->input->get('customer') != '') {
                $filtertext .= " Customer = " . $this->input->get('customer');
            }
            if ($this->input->get('code') != '') {
                $filtertext .= " Fleet Code = " . $this->input->get('code');
            }
            if ($this->input->get('year') != '') {
                $filtertext .= " Year = " . $this->input->get('year');
            }
            if ($this->input->get('month') != '') {
                $filtertext .= " Month = " . date('F', strtotime("2018-" . $this->input->get('month') . "-01"));
            }
            $filters = ($filtertext != "" ? $filtertext : "All Data");

            $objPHPExcel->getActiveSheet()->mergeCells('A2:T2');
            $objPHPExcel->getActiveSheet()->getStyle("A2:T2")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getFill()->getStartColor()->setARGB('eaeaea');
            $objPHPExcel->getActiveSheet()->getStyle("A2:T2")->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Filter : " . $filters);

            $objPHPExcel->getActiveSheet()->getStyle("A3:I3")->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->getStartColor()->setARGB('cce6ff');
            $objPHPExcel->getActiveSheet()->getStyle("A3:I3")->getFont()->setSize(12);

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'NO');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, 'CUSTOMER');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, 'FLEET NUMBER');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'RITASE');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3, 'LOADQTY');
            if ($rows != null) {
                $prices = 0;
                $ritase = 0;
                $allowance = 0;
                $allowanceadds = 0;
                $allowancereds = 0;
                $no  = 1;
                $baris = 4;
                foreach ($rows as $row) {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->namac);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->fleetplateno);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->ritase);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->loadqty);

                    $ritase += $row->ritase;
                    $no++;
                    $baris++;
                }

                $barisfooter = $baris;
                $barisbaru = $barisfooter - 1;
                $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $barisfooter, $ritase);

                //LABEL
                $dsl1 = array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$E$3', NULL, 1),
                );
                $dsl2 = array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$D$3', NULL, 1),
                );
                $dsl3 = array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$F$3', NULL, 1),
                );


                //AXIS
                $xal = array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$C$4:$C$' . $barisbaru . '', NULL, $barisbaru),
                );

                //SERIES

                //SERIES1 
                $dsv1 = array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$E$4:$E$' . $barisbaru . '', NULL, $barisbaru),
                );
                $series1 = new PHPExcel_Chart_DataSeries(
                    PHPExcel_Chart_DataSeries::TYPE_BARCHART,       // plotType
                    PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,  // plotGrouping
                    range(0, count($dsv1) - 1),          // plotOrder
                    $dsl1,                             // plotLabel
                    $xal,                               // plotCategory
                    $dsv1                             // plotValues
                );
                //  Set additional dataseries parameters
                //      Make it a vertical column rather than a horizontal bar graph
                $series1->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

                // SERIES2
                $dsv2 = array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$D$4:$D$' . $barisbaru . '', NULL, $barisbaru),
                );
                $series2 = new PHPExcel_Chart_DataSeries(
                    PHPExcel_Chart_DataSeries::TYPE_LINECHART,      // plotType
                    PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   // plotGrouping
                    range(0, count($dsv2) - 1),          // plotOrder
                    $dsl2,                             // plotLabel
                    NULL,                                           // plotCategory
                    $dsv2                              // plotValues
                );


                $dsv3 = array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$f$4:$f$' . $barisbaru . '', NULL, $barisbaru),
                );

                $series3 = new PHPExcel_Chart_DataSeries(
                    PHPExcel_Chart_DataSeries::TYPE_AREACHART,      // plotType
                    PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   // plotGrouping
                    range(0, count($dsv2) - 1),          // plotOrder
                    $dsl3,                             // plotLabel
                    NULL,                                           // plotCategory
                    $dsv3                              // plotValues
                );


                //$ds=new PHPExcel_Chart_DataSeries(
                //        PHPExcel_Chart_DataSeries::TYPE_LINECHART,
                //        PHPExcel_Chart_DataSeries::GROUPING_STANDARD,
                //        range(0, count($dsv)-1),
                //        $dsl,
                //        $xal,
                //        $dsv
                //        );

                //  Set the series in the plot area
                $pa = new PHPExcel_Chart_PlotArea(NULL, array($series1));
                //  Set the chart legend
                $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

                $title = new PHPExcel_Chart_Title('Average Weather Chart for Crete');

                //$pa=new PHPExcel_Chart_PlotArea(NULL, array($ds));
                $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
                $title = new PHPExcel_Chart_Title('DRIVER / RITASE');
                $chart = new PHPExcel_Chart(
                    'chart1',
                    $title,
                    $legend,
                    $pa,
                    true,
                    0,
                    NULL,
                    NULL
                );
                $chart->setTopLeftPosition('K1');
                $chart->setBottomRightPosition('AE23');
                $ews->addChart($chart);
            } else {
                $objPHPExcel->getActiveSheet()->setCellValue('A4:I4', 'Data Not Found');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('e6f2ff');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(14);
            }




            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            $writer->setIncludeCharts(true);
            //$writer->save('output.xlsx');
            // Save it as an excel 2003 file
            //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            //Nama File
            header('Content-Disposition: attachment;filename="TruckOrderSummary' . date('dmy') . '.xlsx"');
            $writer->save('php://output');
        } else {
            $perpage = 50;

            $data = array();
            $filter = array();
            $data['search'] = array();

            $limit  = $perpage;
            $offset = $page > 1 ? ($limit * $page) - $limit : 0;

            $all_data = $this->Fleets->ordersummary($filter);
            $data['total'] = count($all_data);
            $config['base_url'] = base_url() . '/fleet/driversummary';
            $config['use_page_numbers'] = TRUE;
            $config['total_rows'] = $data['total'];
            $config['per_page'] = $perpage;
            $config['num_links'] = 1;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = 'First';
            $config['first_tag_open'] = '<li class="prev page">';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = 'Last';
            $config['last_tag_open'] = '<li class="next page">';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li class="next page">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li class="prev page">';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a>';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="page">';
            $config['num_tag_close'] = '</li>';
            $config['anchor_class'] = 'follow_link';

            $this->pagination->initialize($config);
            $paginator = $this->pagination->create_links();
            $data['pagination'] = $paginator;

            //$data["page"] = $page;
            ///$data["offset"] = $offset;
            //$data["limit"] = $limit;

            $filter["limit"]  = $limit;
            $filter["page"] = $page;
            $filter["offset"] = $offset;

            $data["result"] = $this->Fleets->ordersummary($filter);
            $data["row"] = $offset;
            echo json_encode($data);
        }
    }
}
