<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');

class invoice extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta"); 
    }
    
    public function index()
    {
    
        $data["id"] = 0;
        $this->template->load('template','invoice/base',$data);
    }
    
    public function lists($page = 0){
        
        $this->load->model('Invoices');
	    $perpage = 20;
        
	    $data = array();
        $filter = array();
        $data['search'] = array();
        
        $limit  = $perpage;
        $offset = $page > 1 ? ($limit*$page)-$limit:0;
        
        $all_data = $this->Invoices->selectAll($filter);
        $data['total'] = count($all_data);
        $config['base_url'] = base_url() . 'invoice/lists/';
        $config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $data['total'];
		$config['per_page'] = $perpage;
        $config['num_links'] = 1;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';

        $config['anchor_class'] = 'follow_link';

        $this->pagination->initialize($config);
		$paginator = $this->pagination->create_links();
		$data['paginator'] = $paginator;
        
        $data["page"] = $page;
        $data["offset"] = $offset;
        $data["limit"] = $limit;
        
        $filter["limit"]  = $limit;
        $filter["page"] = $page;
        $filter["offset"] = $offset;
        $data["rows"]     = $this->Invoices->selectAll($filter);
        $this->load->view('invoice/list', $data);
    }
    
    public function form()
    {
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        $data["id"] = $id;
        $data["id_customers"] = 0;
        $data["customer_name"] = "";
        $data["customer_code"] = "";
        $data["code"] = date("ymdHis");
        $data["invoiceno"] = "";
        $data["receiptno"] = "";
        $data["dates"] = date("d-m-Y");
        $data["dates_overdue"] = date("d-m-Y", strtotime(" + 30 DAYS"));
        $data["po_number"] = "";
        $data["pr_number"] = "";
        $data["recap_id"] = "";
        $data["type"] = 1;
        $data["addressto"] = "";
        $data["pic_name"] = "";
        $data["pic_phone"] = "";
        $data["notes"] = "";
        $data["invoicetype_name"] = "";
        $data["id_companys"] = "";
        $data["company_nickname"] = "";
        $data["company_pic_name"] = "";
        $data["total_shipment"] = 0;
        $data["status"] = "N";
        
        
        if($id > 0)
        {
            $this->load->model("Invoices");
            $row = $this->Invoices->selectOne($data);
            $data["id_customers"] = $row->id_customers;
            $data["customer_name"] = $row->customer_name;
            $data["customer_code"] = $row->customer_code;
            $data["code"] = $row->code;
            $data["invoiceno"] = $row->invoiceno;
            $data["receiptno"] = $row->receiptno;
            $data["dates"] = date("d-m-Y", strtotime($row->dates));
            $data["dates_overdue"] = $row->dates_overdue != "" ? date("d-m-Y", strtotime($row->dates_overdue)) : "";
            $data["po_number"] = $row->po_number;
            $data["pr_number"] = $row->pr_number;
            $data["recap_id"] = $row->recap_id;
            $data["type"] = $row->invoice_type;
            $data["addressto"] = $row->addressto;
            $data["pic_name"] = $row->pic_name;
            $data["pic_phone"] = $row->pic_phone;
            $data["notes"] = $row->notes;
            $data["invoicetype_name"] = $row->invoicetype_name;
            $data["id_companys"] = $row->id_companys;
            $data["company_nickname"] = $row->company_nickname;
            $data["company_pic_name"] = $row->company_pic_name;
            $data["total_shipment"] = $row->total_shipment;
            $data["status"] = $row->status;
        }
        
        $data["type_arrs"] = array("type1" => "Basic Invoice 1"); 
        
        $this->load->view('invoice/form', $data);
    }
    
    public function set()
    {
        
        $return['type'] = 'success';
        $return['title'] = 'Update success!';
        $return['message'] = 'Update data invoice success!';
        
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'><strong>Error ! </strong>", "</div>");
        $this->form_validation->set_rules('invoiceno','Invoice No','trim|required');
        $this->form_validation->set_rules('id_customers','Customer Name','trim|required');
        $this->form_validation->set_rules('receiptno','Receipt No','trim|required');
        $this->form_validation->set_rules('dates','Invoice Date','trim|required');
        $this->form_validation->set_rules('type','Invoice Type','trim|required');
        $this->form_validation->set_rules('pic_name','PIC Name','trim|required');
        $this->form_validation->set_rules('id_companys','From Company','trim|required');
        $this->form_validation->set_rules('addressto','Invoice Address','trim|required');
        
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        $return["id"] = 0;
        //$invoiceno = trim($this->input->post('invoiceno'));
        if($this->form_validation->run())
        {
            if($this->validation_check())
            {
                $dates_ = $this->input->post("dates") != null ? $this->input->post("dates") : date("d-m-Y");
                $dates_parts = explode("-",$dates_);
                $dates = date("Y-m-d", strtotime($dates_parts[2]."-".$dates_parts[1]."-".$dates_parts[0]));
                
                $dates_overdue_ = $this->input->post("dates_overdue") != null ? $this->input->post("dates_overdue") : date("d-m-Y", strtotime("+30 DAYS"));
                $dates_overdue_parts = explode("-",$dates_overdue_);
                $dates_overdue = date("Y-m-d", strtotime($dates_overdue_parts[2]."-".$dates_overdue_parts[1]."-".$dates_overdue_parts[0]));
                
                if($id == 0) {
                    //$this->load->model('Invoices');
                    $this->db->set("code", $this->input->post("code"));
                    $this->db->set("id_customers", $this->input->post("id_customers"));
                    $this->db->set("invoiceno", $this->input->post("invoiceno"));
                    $this->db->set("receiptno", $this->input->post("receiptno"));
                    $this->db->set("dates", $dates);
                    $this->db->set("dates_overdue", $dates_overdue);
                    $this->db->set("type", $this->input->post("type"));
                    $this->db->set("id_companys", $this->input->post("id_companys"));
                    $this->db->set("po_number", $this->input->post("po_number") != "" ? $this->input->post("po_number") : null);
                    $this->db->set("pr_number", $this->input->post("pr_number") != "" ? $this->input->post("pr_number") : null);
                    $this->db->set("recap_id", $this->input->post("recap_id") != "" ? $this->input->post("recap_id") : null);
                    $this->db->set("pic_name", $this->input->post("pic_name") != "" ? $this->input->post("pic_name") : null);
                    $this->db->set("pic_phone", $this->input->post("pic_phone") != "" ? $this->input->post("pic_phone") : null);
                    $this->db->set("addressto", $this->input->post("addressto") != "" ? $this->input->post("addressto") : null);
                    $this->db->set("notes", $this->input->post("notes") != "" ? $this->input->post("notes") : null);
                    $this->db->set("active", 1);
                    $this->db->set("create_id", $this->session->userdata("id"));
                    $this->db->set("create_datetime", date("Y-m-d H:i:s"));
                    $this->db->set("update_id", $this->session->userdata("id"));
                    $this->db->set("update_datetime", date("Y-m-d H:i:s"));
                    
                    if(!$this->db->insert("fin_invoices")) {
                        
                        $return['type'] = 'error';
                        $return['title'] = 'Update failed!';
                        $return['message'] = 'Update data invoice failed!';
                    }   
                    else{
                        $return["id"] = $this->db->insert_id();
                    }
                }
                else 
                {
                    $this->db->set("id_customers", $this->input->post("id_customers"));
                    $this->db->set("invoiceno", $this->input->post("invoiceno"));
                    $this->db->set("receiptno", $this->input->post("receiptno"));
                    $this->db->set("dates", $dates);
                    $this->db->set("dates_overdue", $dates_overdue);
                    $this->db->set("type", $this->input->post("type"));
                    $this->db->set("id_companys", $this->input->post("id_companys"));
                    $this->db->set("po_number", $this->input->post("po_number") != "" ? $this->input->post("po_number") : null);
                    $this->db->set("pr_number", $this->input->post("pr_number") != "" ? $this->input->post("pr_number") : null);
                    $this->db->set("recap_id", $this->input->post("recap_id") != "" ? $this->input->post("recap_id") : null);
                    $this->db->set("pic_name", $this->input->post("pic_name") != "" ? $this->input->post("pic_name") : null);
                    $this->db->set("pic_phone", $this->input->post("pic_phone") != "" ? $this->input->post("pic_phone") : null);
                    $this->db->set("addressto", $this->input->post("addressto") != "" ? $this->input->post("addressto") : null);
                    $this->db->set("notes", $this->input->post("notes") != "" ? $this->input->post("notes") : null);
                    $this->db->set("update_id", $this->session->userdata("id"));
                    $this->db->set("update_datetime", date("Y-m-d H:i:s"));
                    $this->db->where("id", $id);
                    
                    if(!$this->db->update("fin_invoices")) {
                        $return['type'] = 'error';
                        $return['title'] = 'Update failed!';
                        $return['message'] = 'Update data invoice failed!';
                    }else{
                        $return["id"] = $id;
                    }
                }   
            }
            else
            {
                $return['type'] = 'error';
                $return['title'] = 'Update failed!';
                $return['message'] = 'Data invoice already exist!';   
            }
        }
        else
        {
            $return['type'] = 'error';
            $return['title'] = 'Update failed!';
            $return['message'] = validation_errors();
        }
        
        echo json_encode($return);
        
    }
    
    function validation_check()
    {
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0 ;
        $invoiceno = trim(strtoupper($this->input->post('invoiceno')));
        $receiptno = trim(strtoupper($this->input->post('receiptno')));
        
        if($id > 0)
        {
            $this->db->select("COUNT(*) AS total");
            $this->db->where("id != ".$id);
            $this->db->where("(  UPPER(invoiceno) = '$invoiceno' OR LOWER(receiptno) = '$receiptno' )" );
            $this->db->where("active", 1);
            $result = $this->db->get("fin_invoices");
            $valid = $result->row()->total;
            if($valid == 0){
                return true;
            }else{
                return false;
            }
        }
        else
        {
            $this->db->select("COUNT(*) AS total");
            $this->db->where("(  UPPER(invoiceno) = '$invoiceno' OR LOWER(receiptno) = '$receiptno' )" );
            $this->db->where("active", 1);
            $result = $this->db->get("fin_invoices");
            $valid = $result->row()->total;
            if($valid == 0){
                return true;
            }else{
                return false;
            }
        }
    }
    public function delete() {
        
        $return['type'] = 'success';
        $return['title'] = 'Delete success!';
        $return['message'] = 'Delete management data invoice success!';
        
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        if($id > 0)
        {
            $this->load->model('invoices');
            if(!$this->invoices->delete())
            {
                $return['type'] = 'error';
                $return['title'] = 'Delete failed!';
                $return['message'] = 'Delete management data invoice failed!';
            }
        }
        
        echo json_encode($return);
        
    }
    
    public function pdf($param = null, $code = 0)
    {
        if($param == "print")
        {
            $data["control"] = "";
            $data["document_name"] = "INVOICE_".$code."_".date("d-m-Y");
            $data['url'] = base_url().'invoice/printpdf/'.$code;
            $this->load->view('mainpdf2', $data);
        }
    }
    
    public function printpdf($code = null)
    {
        $data = null;
        $html_detail = "";
        if($code != null)
        {
            $data["code"] = $code;
            $this->load->model("invoices");
            $data["header"] = $this->invoices->selectOneByCode($data);
            
            
            $header = $data["header"];
            
            if( $header->type == "mulia_palette")
            {
                /** KHUSUS INVOICE MULIA GLASS PALETTE */
                
                $data["id_fin_invoices"] = $header->id;
                $this->load->model("invoicedetails");
    
                $data["inv_details"] = $this->invoicedetails->selectAllForType2($data);
                
                $inv_details = $data["inv_details"];
                $totalprices = 0;
                if($inv_details != null){
                    foreach($inv_details AS $inv_detail){
                        $totalprices += $inv_detail->prices;
                    }
                }
                
                $this->load->model("companys");
                $data["company"] = $this->companys->getOne(array("id" => $header->id_companys));
                
                $data["totalpricesspell"] = terbilang($totalprices);
                
                $data["totalprices"] = $totalprices;
                
                $html_recap = $this->load->view("invoice/type/mulia_palette_recap", $data, true);
                $html_receipt = $this->load->view("invoice/receipt/".$header->receipt_type, $data, true);
                
                $data["html_detail"] = "";
                $html = $this->load->view("invoice/type/".$header->type, $data, true);
                
                ini_set('memory_limit', '1024M');
                ini_set('max_execution_time', 0);
                
                $this->load->library('M_Pdf');
                //$pdf = new Mpdf\Mpdf(['L','A4','','' , 30 , 30 , 15, 15 , 8 , 8]);
                $pdf= new \Mpdf\Mpdf(['mode' => 'utf-8','format' => 'A4','margin_left' => 20,'margin_right' => 20,'margin_top' => 15,'margin_bottom' => 15,'margin_header' => 9,'margin_footer' => 9, 'orientation' => 'P']);
                //$html = "<h1>Hello World</h1>";
                $pdf->AddPage("P");
                $search = array(
                        '/\>[^\S ]+/s',
                        '/[^\S ]+\</s',
                        '/(\s)+/s', // shorten multiple whitespace sequences
                        '#(?://)?<!\[CDATA\[(.*?)(?://)?\]\]>#s' //leave CDATA alone
                );
                
                $replace = array(
                    '>',
                    '<',
                    '\\1',
                    "//&lt;![CDATA[\n".'\1'."\n//]]>"
                );
        
                $htmls = preg_replace($search, $replace, $html);
                $html_recaps = preg_replace($search, $replace, $html_recap);
                $html_receipts = preg_replace($search, $replace, $html_receipt);
                ob_clean();
                
                $pdf->WriteHTML($htmls);
                
                $pdf->AddPage("L",'','','','',10,10,10,10,5,5,'','','','','','','','','',"A4");
                
                $pdf->WriteHTML($html_recaps);
                
                $pdf->AddPage("P",'','','','',10,10,10,10,5,5,'','','','','','','','','',"A4");
                
                $pdf->WriteHTML($html_receipts);
                
                $pdf->Output($code.'.pdf',"I");
            }
            else
            {
                /** KHUSUS INVOICE LAINNYA */
                
                $data["id_fin_invoices"] = $header->id;
                $this->load->model("invoicedetails");
    
                $data["inv_details"] = $this->invoicedetails->selectAllForType1($data);
                if($header->type != "type1") {
                    $data["inv_details"] = $this->invoicedetails->selectAllForType2($data);
                }
                
                $inv_details = $data["inv_details"];
                $totalprices = 0;
                if($inv_details != null){
                    foreach($inv_details AS $inv_detail){
                        $totalprices += $inv_detail->prices;
                    }
                }
                
                $this->load->model("companys");
                $data["company"] = $this->companys->getOne(array("id" => $header->id_companys));
                
                $data["totalpricesspell"] = terbilang($totalprices);
                
                $data["totalprices"] = $totalprices;
                $html_detail = $this->load->view("invoice/type/".$header->type, $data, true);
                $html_receipt = $this->load->view("invoice/receipt/".$header->receipt_type, $data, true);
                
                
                $data["html_detail"] = $html_detail;
                $html = $this->load->view("invoice/pdf", $data, true);
                //echo $html_receipt;
                
                ini_set('memory_limit', '1024M');
                ini_set('max_execution_time', 0);
                
                $this->load->library('M_Pdf');
                //$pdf = new Mpdf\Mpdf(['L','A4','','' , 30 , 30 , 15, 15 , 8 , 8]);
                $pdf= new \Mpdf\Mpdf(['mode' => 'utf-8','format' => 'A4','margin_left' => 20,'margin_right' => 20,'margin_top' => 15,'margin_bottom' => 15,'margin_header' => 9,'margin_footer' => 9, 'orientation' => 'P']);
                //$html = "<h1>Hello World</h1>";
                $pdf->AddPage("L");
                $search = array(
                        '/\>[^\S ]+/s',
                        '/[^\S ]+\</s',
                        '/(\s)+/s', // shorten multiple whitespace sequences
                        '#(?://)?<!\[CDATA\[(.*?)(?://)?\]\]>#s' //leave CDATA alone
                );
                
                $replace = array(
                    '>',
                    '<',
                    '\\1',
                    "//&lt;![CDATA[\n".'\1'."\n//]]>"
                );
        
                $htmls = preg_replace($search, $replace, $html);
                $html_receipts = preg_replace($search, $replace, $html_receipt);
                ob_clean();
                
                $pdf->WriteHTML($htmls);
                
                $pdf->AddPage("P",'','','','',10,10,10,10,5,5,'','','','','','','','','',"A4");
                
                $pdf->WriteHTML($html_receipts);
                
                $pdf->Output($code.'.pdf',"I");
                
            }
            
            
            
        }
        
        
        
        
    }
    
    public function detail()
    {
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        if($id > 0)
        {
            $data["id"] = $id;
            $this->load->model("Invoices");
            $row = $this->Invoices->selectOne($data);
            $data["id_customers"] = $row->id_customers;
            $data["customer_name"] = $row->customer_name;
            $data["customer_code"] = $row->customer_code;
            $data["code"] = $row->code;
            $data["invoiceno"] = $row->invoiceno;
            $data["receiptno"] = $row->receiptno;
            $data["dates"] = date("d-m-Y", strtotime($row->dates));
            $data["dates_overdue"] = $row->dates_overdue != "" ? date("d-m-Y", strtotime($row->dates_overdue)) : "";
            $data["po_number"] = $row->po_number;
            $data["pr_number"] = $row->pr_number;
            $data["recap_id"] = $row->recap_id;
            $data["type"] = $row->invoice_type;
            $data["addressto"] = $row->addressto;
            $data["pic_name"] = $row->pic_name;
            $data["pic_phone"] = $row->pic_phone;
            $data["notes"] = $row->notes;
            $data["invoicetype_name"] = $row->invoicetype_name;
            $data["id_companys"] = $row->id_companys;
            $data["company_nickname"] = $row->company_nickname;
            $data["company_pic_name"] = $row->company_pic_name;
            $data["status"] = $row->status;
            $data["prices"] = $row->prices;
            $data["paid"] = $row->paid;
        }
        $this->load->view('invoice/detail', $data);
    }
    
    public function payment()
    {
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        if($id > 0)
        {
            $data["id"] = $id;
            $this->load->model("Invoices");
            $row = $this->Invoices->selectOne($data);
            $data["id_customers"] = $row->id_customers;
            $data["customer_name"] = $row->customer_name;
            $data["customer_code"] = $row->customer_code;
            $data["code"] = $row->code;
            $data["invoiceno"] = $row->invoiceno;
            $data["receiptno"] = $row->receiptno;
            $data["dates"] = date("d-m-Y", strtotime($row->dates));
            $data["dates_overdue"] = $row->dates_overdue != "" ? date("d-m-Y", strtotime($row->dates_overdue)) : "";
            $data["po_number"] = $row->po_number;
            $data["pr_number"] = $row->pr_number;
            $data["recap_id"] = $row->recap_id;
            $data["type"] = $row->invoice_type;
            $data["addressto"] = $row->addressto;
            $data["pic_name"] = $row->pic_name;
            $data["pic_phone"] = $row->pic_phone;
            $data["notes"] = $row->notes;
            $data["invoicetype_name"] = $row->invoicetype_name;
            $data["id_companys"] = $row->id_companys;
            $data["company_nickname"] = $row->company_nickname;
            $data["company_pic_name"] = $row->company_pic_name;
            $data["status"] = $row->status;
            $data["prices"] = $row->prices;
            $data["paid"] = $row->paid;
        }
        $this->load->view('invoice/payment', $data);
    }
    
    public function send_complete_invoice()
    {
        $return['type'] = 'error';
        $return['title'] = 'Sending failed!';
        $return['message'] = 'Sending data invoice failed!';
        
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        if($id > 0)
        {
            $this->db->set("status","S");
            $this->db->set("update_id", $this->session->userdata("id"));
            $this->db->set("update_datetime", date("Y-m-d H:i:s"));
            $this->db->where("id", $id);
            if($this->db->update("fin_invoices")) {
                $return['type'] = 'success';
                $return['title'] = 'Sending success!';
                $return['message'] = 'Sending data invoice success!';
            }
            
        }
        
        echo json_encode($return);
    }
    
}