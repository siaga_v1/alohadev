<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');

class invoicedetail extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta"); 
    }
    
    public function index()
    {
        $data["id_customers"] = $this->input->post("id_customers");
        $data["id_fin_invoices"] = $this->input->post("id_fin_invoices");
        $this->load->view('invoicedetail/base',$data);
    }
    
    public function detail()
    {
        $this->load->model('Invoicedetails');
        $data["id_customers"] = $this->input->post("id_customers");
        $data["id_fin_invoices"] = $this->input->post("id_fin_invoices");
        $data["rows"]     = $this->Invoicedetails->selectAll($data);
        $this->load->view('invoicedetail/detail',$data);
    }
    
    public function lists($page = 0){
        
        $this->load->model('Invoicedetails');
	    $perpage = 20;
        
	    $data = array();
        $filter = array();
        $data['search'] = array();
        
        $limit  = $perpage;
        $offset = $page > 1 ? ($limit*$page)-$limit:0;
        
        $all_data = $this->Invoicedetails->selectAll($filter);
        $data['total'] = count($all_data);
        $config['base_url'] = base_url() . 'invoicedetail/lists/';
        $config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $data['total'];
		$config['per_page'] = $perpage;
        $config['num_links'] = 1;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';

        $config['anchor_class'] = 'follow_link';

        $this->pagination->initialize($config);
		$paginator = $this->pagination->create_links();
		$data['paginator'] = $paginator;
        
        $data["page"] = $page;
        $data["offset"] = $offset;
        $data["limit"] = $limit;
        
        /**$filter["limit"]  = $limit;
        $filter["page"] = $page;
        $filter["offset"] = $offset;
        */
        
        $data["rows"]     = $this->Invoicedetails->selectAll($filter);
        $this->load->view('invoicedetail/list', $data);
    }
    
    public function form()
    {
        $this->load->model('Invoicedetails');
        $data["rows"] = $this->Invoicedetails->selectAllOrderForInvoice();
        $data["id_customers"] = $this->input->post('id_customers') > 0 ? $this->input->post('id_customers') : 0;
        $data["id_fin_invoices"] = $this->input->post('id_fin_invoices') > 0 ? $this->input->post('id_fin_invoices') : 0;
        $this->load->view('invoicedetail/form', $data);
    }
    
    public function set()
    {
        $return['type'] = 'success';
        $return['title'] = 'Update success!';
        $return['message'] = 'Update data invoice detail shipment success!';
        
        $error = 0;
        $exist = 0;
        
        $id_fin_invoices = $this->input->post("id_fin_invoices");
        $details = $this->input->post("detail");
        if($details != null && count($details) > 0)
        {
            foreach($details AS $detail)
            {
                $this->db->select("count(*) AS total");
                $this->db->where("id_orders", $detail["id_orders"]);
                $this->db->where("active",1);
                $result = $this->db->get("fin_invoice_details");
                $row = $result->row();
                
                if($row != null && $row->total == 0)
                {
                    $this->db->set("id_fin_invoices", $id_fin_invoices);
                    $this->db->set("id_orders", $detail["id_orders"]);
                    $this->db->set("active", 1);
                    if(!$this->db->insert("fin_invoice_details")){
                        $error++;
                    }
                }
                else
                {
                    $exist++;
                }
            }
        }
        else
        {
            $return['type'] = 'error';
            $return['title'] = 'Update failed!';
            $return['message'] = 'Please choose minimum 1 (one) shipment!';
        }
        
        if($exist > 0){
            $return['type'] = 'warning';
            $return['title'] = 'Update success!';
            $return['message'] = 'Update data invoice success but theres some shipment exist!';
        }
        
        if($error > 0){
            $return['type'] = 'warning';
            $return['title'] = 'Update success!';
            $return['message'] = 'Update data invoice success but theres some shipment invalid!';
        }
        
        echo json_encode($return);
    }
    
    public function delete()
    {
        
        $details = $this->input->post("detail_");
        
        if($details != null && count($details) > 0)
        {

            $error = 0;
            foreach($details AS $key => $value) {
                if($value > 0){
                    $this->db->set("active", 0);
                    $this->db->where("id", $value);
                    if(!$this->db->update("fin_invoice_details")){
                        $error++;
                    }
                }
            }
            
            if($error > 0)
            {
                $return['type'] = 'warning';
                $return['title'] = 'Delete success!';
                $return['message'] = 'Delete data invoice detail shipment success but theres some errors!';    
            }
            else
            {
                $return['type'] = 'success';
                $return['title'] = 'Delete success!';
                $return['message'] = 'Delete data invoice detail shipment success!';    
            }
        
        }
        else
        {
            $return['type'] = 'error';
            $return['title'] = 'Caution!';
            $return['message'] = 'Please choose minimum 1 (one) shipment!';   
        }
        
        echo json_encode($return);
    }

}