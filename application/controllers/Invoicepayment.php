<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');

class invoicepayment extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta"); 
    }
    
    public function index()
    {
    
        $data["id_fin_invoices"] = $this->input->post("id_fin_invoices");
        $this->load->view('invoicepayment/base',$data);
    }
    
    public function lists($page = 0){
        
        $this->load->model('invoicepayments');
	    $perpage = 20;
        
	    $data = array();
        $filter = array();
        $data['search'] = array();
        
        $limit  = $perpage;
        $offset = $page > 1 ? ($limit*$page)-$limit:0;
        
        $all_data = $this->invoicepayments->selectAll($filter);
        $data['total'] = count($all_data);
        $config['base_url'] = base_url() . 'invoicepayment/lists/';
        $config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $data['total'];
		$config['per_page'] = $perpage;
        $config['num_links'] = 1;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';

        $config['anchor_class'] = 'follow_link';

        $this->pagination->initialize($config);
		$paginator = $this->pagination->create_links();
		$data['paginator'] = $paginator;
        
        $data["page"] = $page;
        $data["offset"] = $offset;
        $data["limit"] = $limit;
        
        $filter["limit"]  = $limit;
        $filter["page"] = $page;
        $filter["offset"] = $offset;
        $data["rows"]     = $this->invoicepayments->selectAll($filter);
        $this->load->view('invoicepayment/list', $data);
    }
    
    public function form()
    {
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        $id_fin_invoices = $this->input->post('id_fin_invoices') > 0 ? $this->input->post('id_fin_invoices') : 0;
        $rows = $this->db->get_where("fin_invoices", array('id' => $id_fin_invoices))->row();
        $data["id"] = $id;
        $data["id_fin_invoices"] = $id_fin_invoices;
        $data["invoiceno"] = $rows->invoiceno;
        $data["code"] = $this->getCode();
        $data["dates_date"] = date("Y-m-d");
        $data["dates_time"] = date("H:i");
        $data["description"] = "";
        $data["description2"] = "";
        $data["description3"] = "";
        $data["prices"] = 0;
        
        if($id > 0)
        {
            $this->load->model("invoicepayments");
            $row = $this->invoicepayments->selectOne($data);
            $data["code"] = $row->code;
            $data["id_fin_invoices"] = $row->id_fin_invoices;
            $data["dates_date"] = date("Y-m-d", strtotime($row->payment_date) );
            $data["dates_time"] = date("H:i", strtotime($row->payment_date));
            $data["description"] = $row->description;
            $data["description2"] = $row->description2;
            $data["description3"] = $row->description3;
            $data["prices"] = $row->prices;
        }
        
        $this->load->view('invoicepayment/form', $data);
    }
    
    public function set()
    {
        
        $return['type'] = 'success';
        $return['title'] = 'Update success!';
        $return['message'] = 'Update data payment success!';
        
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'><strong>Error ! </strong>", "</div>");
        $this->form_validation->set_rules('id_fin_invoices','Invoice No','trim|required');
        $this->form_validation->set_rules('dates_date','Date','trim|required');
        $this->form_validation->set_rules('dates_time','Time','trim|required');
        $this->form_validation->set_rules('description','Description','trim|required');
        $this->form_validation->set_rules('prices','Nominal','trim|required');
        
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        $return["id"] = 0;
       
        if($this->form_validation->run())
        {
             
            $dates_date = $this->input->post('dates_date') != '' ? ($this->input->post('dates_date')) : date("d-m-Y");
            $dates_time = $this->input->post('dates_time') != '' ? ($this->input->post('dates_time')) : date("H:i");
            $id_fin_invoices = $this->db->escape_str($this->input->post("id_fin_invoices"));
            //$dates_parts = explode("-",$dates_date);
            $dates = date("Y-m-d H:i:s", strtotime($dates_date." ".$dates_time));
            $prices = $this->db->escape_str($this->input->post("prices"));
            
            $fin_invoices = $this->db->get_where("fin_invoices", array('id' => $id_fin_invoices))->row();
            $fin_invoices_saldo = ($fin_invoices->prices - $fin_invoices->paid);
            $return["id_fin_invoices"] = $id_fin_invoices;    

            if($id == 0) {
                
                if($prices > $fin_invoices_saldo)
                {
                    $return['type'] = 'error';
                    $return['title'] = 'Update failed!';
                    $return['message'] = 'Anda memasukan angka pembayaran lebih!';
                }
                else
                {
                    $this->db->set("code", $this->db->escape_str($this->input->post("code")) );
                    $this->db->set("id_fin_invoices", $this->db->escape_str($this->input->post("id_fin_invoices")) );
                    $this->db->set("payment_date", $dates );
                    $this->db->set("description", $this->db->escape_str($this->input->post("description")) );
                    $this->db->set("description2", $this->db->escape_str($this->input->post("description2")) );
                    $this->db->set("description3", $this->db->escape_str($this->input->post("description3")) );
                    $this->db->set("prices", $this->db->escape_str($this->input->post("prices")) );
                    
                    $this->db->set("active", 1);
                    $this->db->set("create_by", $this->session->userdata("id"));
                    $this->db->set("create_datetime", date("Y-m-d H:i:s"));
                    $this->db->set("update_by", $this->session->userdata("id"));
                    $this->db->set("update_datetime", date("Y-m-d H:i:s"));
                    
                    if(!$this->db->insert("fin_invoice_payments")) {
                        
                        $return['type'] = 'error';
                        $return['title'] = 'Update failed!';
                        $return['message'] = 'Update data payment failed!';
                    }   
                    else{
                        $return["id"] = $this->db->insert_id();
                    }
                }
            }
            else 
            {
                $this->db->select("COALESCE(prices,0) AS prices");
                $this->db->where("id", $id);
                $this->db->where("active", 1);
                $result = $this->db->get("fin_invoice_payments");
                $fin_invoice_payments = $result->row();
                
                $temp_saldo = ($fin_invoices_saldo + $fin_invoice_payments->prices);
                
                if($prices > $temp_saldo)
                {
                    $return['type'] = 'error';
                    $return['title'] = 'Update failed!';
                    $return['message'] = 'Anda memasukan angka pembayaran lebih!';
                }
                else
                {
                    $this->db->set("id_fin_invoices", $this->db->escape_str($this->input->post("id_fin_invoices")) );
                    $this->db->set("payment_date", $dates );
                    $this->db->set("description", $this->db->escape_str($this->input->post("description")) );
                    $this->db->set("description2", $this->db->escape_str($this->input->post("description2")) );
                    $this->db->set("description3", $this->db->escape_str($this->input->post("description3")) );
                    $this->db->set("prices", $this->db->escape_str($this->input->post("prices")) );
                    
                    $this->db->set("update_by", $this->session->userdata("id"));
                    $this->db->set("update_datetime", date("Y-m-d H:i:s"));
                    $this->db->where("id", $id);
                    
                    if(!$this->db->update("fin_invoice_payments")) {
                        $return['type'] = 'error';
                        $return['title'] = 'Update failed!';
                        $return['message'] = 'Update data payment failed!';
                    }else{
                        $return["id"] = $id;
                    }   
                }
            }   
        }
        else
        {
            $return['type'] = 'error';
            $return['title'] = 'Update failed!';
            $return['message'] = validation_errors();
        }
        
        echo json_encode($return);
        
    }
    
    function validation_check()
    {
        /**
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0 ;
        $name = strtoupper($this->db->escape_str($this->input->post("name")));
        $address_line1 = strtoupper($this->db->escape_str($this->input->post("address_line1")));
        
        if($id > 0)
        {
            $this->db->select("COUNT(*) AS total");
            $this->db->where("id != ".$id);
            $this->db->where("(  UPPER(name) = '$name' )" );
            $this->db->where("(  UPPER(name) LIKE '%$address_line1%' )" );
            $this->db->where("active", 1);
            $result = $this->db->get("fin_invoice_payments");
            $valid = $result->row()->total;
            if($valid == 0){
                return true;
            }else{
                return false;
            }
        }
        else
        {
            $this->db->select("COUNT(*) AS total");
            $this->db->where("(  UPPER(name) = '$name' )" );
            $this->db->where("(  UPPER(name) LIKE '%$address_line1%' )" );
            $this->db->where("active", 1);
            $result = $this->db->get("fin_invoice_payments");
            $valid = $result->row()->total;
            if($valid == 0){
                return true;
            }else{
                return false;
            }
        }
        */
    }
    
    public function delete() {
        
        $return['type'] = 'success';
        $return['title'] = 'Delete success!';
        $return['message'] = 'Delete management data invoicepayment success!';
        $return["id_fin_invoices"] = 0;
        
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        if($id > 0)
        {
            $row = $this->db->get_where("fin_invoice_payments", array('id' => $id))->row();
            $return["id_fin_invoices"] = $row->id_fin_invoices;
            
            $this->load->model('invoicepayments');
            if(!$this->invoicepayments->delete())
            {
                $return['type'] = 'error';
                $return['title'] = 'Delete failed!';
                $return['message'] = 'Delete management data invoicepayment failed!';
            }
        }
        
        echo json_encode($return);
        
    }
    
    public function autocomplete()
    {
        $this->load->model('invoicepayments');
        $rows = $this->invoicepayments->selectAll();
        
        if(count($rows) > 0)
        {
           foreach ($rows as $key => $value) {
        	$data[] = array('id' => $value->id, 'text' => $value->code.' '.$value->prices );			 	
           } 
        }
        else 
        {
           $data[] = array('id' => '', 'text' => 'No Data Found');
        }
        
        echo json_encode($data); 
    }
    
    public function getCode()
    {
        $this->db->select("COUNT(*) AS id");
        $this->db->where("SUBSTRING(code, 5, 6) = ", date("ymd"));
        $result = $this->db->get("fin_invoice_payments");
        $row = $result->row();
        $lastid = $row->id + 1;
        
        $code = "";
        for($x=1;$x <= 4;$x++){
            if($x > strlen($lastid)){
                $code .= "0";
            }
        }
        
        $code_ = "INVP".date("ymd").$code.$lastid;
        return $code_;
    }
    
    public function pdf($param = null, $code = 0)
    {
        if($param == "print")
        {
            $data["control"] = "";
            $data["document_name"] = "INV_PAYMENT".$code."_".date("d-m-Y");
            $data['url'] = base_url().'invoicepayment/printpdf/'.$code;
            $this->load->view('mainpdf2', $data);
        }
    }
    
    public function printpdf($code = null)
    {
        $data = null;
        $html_detail = "";
        if($code != null)
        {
            $data["code"] = $code;
            $this->load->model("ppus");
            $data["header"] = $this->ppus->selectOneByCode($data);
            
            $header = $data["header"];
            $data["id_fin_invoices"] = $header->id;
            $this->load->model("invoicepayments");
            $data["rows"] = $this->invoicepayments->selectAllPDF($data);
            
            $html_detail = $this->load->view("invoicepayment/pdf_detail", $data, true);
            
        }
        
        $data["html_detail"] = $html_detail;
        
        $html = $this->load->view("invoicepayment/pdf", $data, true);
        //echo $html;
        
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 0);
        
        $this->load->library('M_Pdf');
        //$pdf = new Mpdf\Mpdf(['L','A4','','' , 30 , 30 , 15, 15 , 8 , 8]);
        $pdf= new \Mpdf\Mpdf(['mode' => 'utf-8','format' => 'A4','margin_left' => 20,'margin_right' => 20,'margin_top' => 15,'margin_bottom' => 15,'margin_header' => 9,'margin_footer' => 9, 'orientation' => 'P']);
        //$html = "<h1>Hello World</h1>";
        $pdf->AddPage("P");
        $search = array(
                '/\>[^\S ]+/s',
                '/[^\S ]+\</s',
                '/(\s)+/s', // shorten multiple whitespace sequences
                '#(?://)?<!\[CDATA\[(.*?)(?://)?\]\]>#s' //leave CDATA alone
        );
        
        $replace = array(
            '>',
            '<',
            '\\1',
            "//&lt;![CDATA[\n".'\1'."\n//]]>"
        );

        $htmls = preg_replace($search, $replace, $html);
        ob_clean();
        
        $pdf->WriteHTML($htmls);
        
        $pdf->Output('INV_PAYMENT_'.$code.'.pdf',"I");

        
    }
    
    public function export($code = "")
    {
        
        $data["code"] = $code;
        $this->load->model("ppus");
        $data["header"] = $this->ppus->selectOneByCode($data);
        
        $header = $data["header"];
        $data["id_fin_invoices"] = $header->id;
        $this->load->model("invoicepayments");
        $rows = $this->invoicepayments->selectAllPDF($data);
        
        $this->load->library('PHPExcel');
        $data['search'] = array();
        //$rows = $this->fin_invoicess->exportexcelppu($data);
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()
         ->setCreator('TOTAL KILAT')
         ->setTitle('TOTAL KILAT')
         ->setLastModifiedBy('SYSTEM')
         ->setDescription('TOTAL KILAT')
         ->setSubject('TOTAL KILAT')
         ->setKeywords('TOTAL KILAT')
         ->setCategory('EXPORT EXCEL')
         ;
        
        $ews = $objPHPExcel->getSheet(0);
        $ews->setTitle('Data');
        
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'PPU Data Pengeluaran & Pemasukan');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
        $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setARGB('ffffcc');
        $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setSize(16);
        
        $objPHPExcel->getActiveSheet()->mergeCells('A2:I2');
        $objPHPExcel->getActiveSheet()->getStyle("A2:I2")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getFill()->getStartColor()->setARGB('eaeaea');
        $objPHPExcel->getActiveSheet()->getStyle("A2:I2")->getFont()->setSize(14);
        
        $objPHPExcel->getActiveSheet()->getStyle("A3:A6")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("E3:E6")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Code');
        $objPHPExcel->getActiveSheet()->mergeCells('A3:B3');
        $objPHPExcel->getActiveSheet()->setCellValue('C3', $header->code);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Tanggal');
        $objPHPExcel->getActiveSheet()->mergeCells('A4:B4');
        $objPHPExcel->getActiveSheet()->setCellValue('C4', $header->dates);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Penerima');
        $objPHPExcel->getActiveSheet()->mergeCells('A5:B5');
        $objPHPExcel->getActiveSheet()->setCellValue('C5', $header->penerima_name);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A6', 'Kebutuhan');
        $objPHPExcel->getActiveSheet()->mergeCells('A6:B6');
        $objPHPExcel->getActiveSheet()->setCellValue('C6', $header->desc_1_name);
        
        $objPHPExcel->getActiveSheet()->setCellValue('E3', 'Deskripsi Kebutuhan');
        $objPHPExcel->getActiveSheet()->setCellValue('F3', $header->desc_2);
        
        $objPHPExcel->getActiveSheet()->setCellValue('E4', 'Nominal');
        $objPHPExcel->getActiveSheet()->setCellValue('F4', $header->price);
        
        $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Pengeluaran');
        $objPHPExcel->getActiveSheet()->setCellValue('F5', $header->paid);
        
        $objPHPExcel->getActiveSheet()->setCellValue('E6', 'Sisa Saldo');
        $objPHPExcel->getActiveSheet()->setCellValue('F6', ($header->price - $header->paid));
        
        $baris_judul = 8;
        $objPHPExcel->getActiveSheet()->getStyle("A$baris_judul:I$baris_judul")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A$baris_judul:I$baris_judul")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle("A$baris_judul:I$baris_judul")->getFill()->getStartColor()->setARGB('cce6ff');
        $objPHPExcel->getActiveSheet()->getStyle("A$baris_judul:I$baris_judul")->getFont()->setSize(12);
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris_judul,'No');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris_judul,'Code');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris_judul,'Transaction Date');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris_judul,'Koordinator');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris_judul,'Source');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris_judul,'Relation');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $baris_judul,'Description');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $baris_judul,'Debit');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $baris_judul,'Credit');
        
        $grand_total_prices_in = 0;
        $grand_total_prices_out = 0;
        
        if($rows != null)
        {
            
          $no  = 1;
          $baris = $baris_judul+1;
          
          $prices_in = 0;
          $prices_out = 0;
          
          foreach($rows AS $row)
          {   
            
            $prices_in = ($row->type == 2) ? $row->prices : 0;
            $prices_out = ($row->type == 1) ? $row->prices : 0;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->code);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->dates);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->koordinator_name);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->ppurelation_name);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris, $row->relation_code);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $baris, $row->description);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $baris, $prices_out);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $baris, $prices_in);
              
            $grand_total_prices_in += $prices_in;
            $grand_total_prices_out += $prices_out;
            
            $no++;
            $baris++;
          }
        }
        else
        {
          $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Data Not Found');
          $objPHPExcel->getActiveSheet()->mergeCells('A4:I4');
          $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
          $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
          $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('ffffcc');
          $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(16);
        }
        
        $objPHPExcel->getActiveSheet()->getStyle("A$baris:I$baris")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$baris:G$baris");
        $objPHPExcel->getActiveSheet()->setCellValue("A$baris", 'Grand Total');
        $objPHPExcel->getActiveSheet()->setCellValue("H$baris", $grand_total_prices_out);
        $objPHPExcel->getActiveSheet()->setCellValue("I$baris", $grand_total_prices_in);
        
        
        
        
        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        
        //$writer->setIncludeCharts(true);
        //$writer->save('output.xlsx');
        // Save it as an excel 2003 file
        //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //Nama File
        header('Content-Disposition: attachment;filename="INV_PAYMENT_'.date('dmy').'.xlsx"');
        $writer->save('php://output');
    }
    
    
    
}