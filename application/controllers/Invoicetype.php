<?php
defined('BASEPATH') or exit('No direct script access allowed');

class invoicetype extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        //is_logged_in();
    }

    public function index()
    {
        $data["contents"] = "invoicetype/base";
        $this->template->load("template", "invoicetype/base", $data);
    }

    public function lists($page = 0)
    {
        $this->load->model('invoicetypes');
        $perpage = 20;

        $data = array();
        $filter = array();

        $limit  = $perpage;
        $offset = $page > 1 ? ($limit * $page) - $limit : 0;

        $all_data = $this->invoicetypes->selectAll($filter);
        $data['total'] = count($all_data);
        $config['base_url'] = base_url() . 'invoicetype/lists/';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $perpage;
        $config['num_links'] = 1;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';

        $config['anchor_class'] = 'follow_link';

        $this->pagination->initialize($config);
        $paginator = $this->pagination->create_links();
        $data['paginator'] = $paginator;

        $data["page"] = $page;
        $data["offset"] = $offset;
        $data["limit"] = $limit;

        $filter["limit"]  = $limit;
        $filter["page"] = $page;
        $filter["offset"] = $offset;
        //$data["contents"] = "invoicetype/list";
        $data["rows"]     = $this->invoicetypes->selectAll($filter);
        $this->load->view('invoicetype/list', $data);
    }

    public function form()
    {
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        $data['id'] = $id;
        $data['sequence'] = "";
        $data["name"] = "";

        if ($id > 0) {

            $this->load->model('invoicetypes');
            $row = $this->invoicetypes->selectOne($data);
            $data['id'] = $row->id;
            $data['sequence'] = $row->sequence;
            $data["name"] = $row->name;
        }

        $this->load->view('invoicetype/form', $data);
    }

    public function set()
    {
        $return['type'] = 'success';
        $return['title'] = 'Update success!';
        $return['message'] = 'Update management data invoicetype success!';

        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'><strong>Error ! </strong>", "</div>");
        $this->form_validation->set_rules('sequence', 'Sequence Invoice', 'trim|required');
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;

        if ($this->form_validation->run()) {
            if ($id > 0) {

                $this->db->set("sequence", $this->input->post("sequence"));
                $this->db->where("id", $id);
                if (!$this->db->update("invoicetypes")) {

                    $return['type'] = 'error';
                    $return['title'] = 'Update failed!';
                    $return['message'] = 'Update management data invoicetype failed!';
                }
            }
        } else {
            $return['type'] = 'error';
            $return['title'] = 'Update failed!';
            $return['message'] = validation_errors();
        }

        echo json_encode($return);
    }

    /**
    public function validation_check() {
        
        $this->load->model('invoicetypes');
        return $this->invoicetypes->check();
        
    }
    
    public function delete() {
        
        $return['type'] = 'success';
        $return['title'] = 'Delete success!';
        $return['message'] = 'Delete management data invoicetype success!';
        
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        if($id > 0)
        {
            $this->load->model('invoicetypes');
            if(!$this->invoicetypes->delete())
            {
                $return['type'] = 'error';
                $return['title'] = 'Delete failed!';
                $return['message'] = 'Delete management data invoicetype failed!';
            }
        }
        
        echo json_encode($return);
        
    }
     */

    public function autocomplete()
    {
        //$this->load->model('invoicetypes');
        //$rows = $this->invoicetypes->selectAll();
        $q = $this->db->escape_str($this->input->post("q"));
        $id_customers = $this->input->post("id_customers") > 0 ? $this->input->post("id_customers") : 0;
        // $id_companys = $this->input->post("id_company") > 0 ? $this->input->post("id_company") : 0;

        $this->db->select("*");
        $this->db->where("active", 1);
        // $this->db->where("id_companys", $id_companys);
        $this->db->where(" ( id_customers = $id_customers OR id_customers = 0 )");
        if ($q != "") {
            $this->db->like("name", $q);
        }
        $result = $this->db->get("invoicetypes");
        $rows = $result->result();

        if (count($rows) > 0) {
            foreach ($rows as $key => $value) {
                $data[] = array('id' => $value->invoice_type, 'text' => $value->name, 'type' => $value->invoice_type, 'receipt' => $value->receipt_type);
            }
        } else {
            $data[] = array('id' => '', 'text' => 'No Data Found');
        }

        echo json_encode($data);
    }
}
