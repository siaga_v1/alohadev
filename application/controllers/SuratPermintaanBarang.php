<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class SuratPermintaanBarang extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    //mengambil library session
     $this->load->library(array('session'));
     //mengambil url di helper
     $this->load->helper('url');
     // mengambil m_login yang berada di folder model yang tadi di buat
     $this->load->model('modelArmada');
     $this->load->model('modelEmployee');
     $this->load->model('modelInventory');
     $this->load->model('permintaanBarang');
    //mengambil database
     $this->load->database();
      
      
  }
   
  // membuat fungsi index
  public function index()
  {
    //belum di approved atau minta approved
    $data['SPB']=$this->permintaanBarang->listPermintaanBarang(); 
    $this->template->load('template','Pages/listPermintaanBarang',$data);
  }

  public function listSPB()
  {
    //belum di approved atau minta approved
    $data['SPB']=$this->permintaanBarang->listPermintaanBarangAll(); 
    $this->template->load('template','Pages/listPermintaanBarang',$data);
  }

  public function approvedSPB()
  {
    //sudah di approved
    $data['SPB']=$this->permintaanBarang->listPermintaanBarangApproved(); 
    $this->template->load('template','Pages/approveSPB',$data);
  } 

  public function tambahPermintaan()
  {
    $data['armada']=$this->modelArmada->ambilArmada();
    $data['employee']=$this->permintaanBarang->ambilEmployee();
    $data['inventory']=$this->modelInventory ->ambilInventory();
    $data['kategori']=$this->modelInventory ->ambilKategori();
    $data['nomor']=$this->permintaanBarang ->ambilNomor();
    $this->template->load('template','Pages/tambahPermintaan',$data);
  }

  public function detailPermintaan()
  {
    $data['SPB']=$this->permintaanBarang->detailPermintaanBarang();
    $this->template->load('template','Pages/detailPermintaanBarang',$data);
  }

  public function printSPB()
  {
    $this->load->library('M_pdf');

    $mpdf = $this->m_pdf->load([
      'mode' => 'utf-8',
      'format' => 'A4'
    ]);
    $itle=$_GET['id'];
    $data['SPB']=$this->permintaanBarang->detailPermintaanBarang();
    $view = $this->load->view('pages/printSPB',$data,true);
    
    $mpdf->SetHeader("PT. Arief Nusa Raya <br> Coal Hauling Project - TCM Melak Kutai Barat||$itle");
    $mpdf->SetTitle($itle);
    $mpdf->WriteHTML($view);

    $mpdf->Output();
  }

  public function simpanPermintaan()
  {
    $idp=$this->session->userdata('emp_code');
    $this->permintaanBarang->simpanPermintaan($idp);
    redirect('SuratPermintaanBarang?msg=Data Berhasil di Tambah');
  }

  public function approveSPB()
  {
    //$idp=$this->session->userdata('emp_code');
    $this->permintaanBarang->approveSPB();
    redirect('SuratPermintaanBarang?msg=Berhasil di Approve');
  }

  public function getStock(){
    $data=$_POST['labi'];

    $this->db->select('*');
    $this->db->where('ii_id',$data);
    $result=$this->db->get('mst_inv_item');

    foreach ($result->result_array() as $row){
      echo $row['ii_stock'] ;
    }
  }  

  public function getMerk(){
    $data=$_POST['idarm'];

    $this->db->select('*');
    $this->db->where('arm_id',$data);
    $result=$this->db->get('mst_armada');

    foreach ($result->result_array() as $row){
      echo " ".$row['arm_merek_mobil']."- ".$row['arm_jenis_code']." " ;
    }


  }  

  public function minusStock(){
    $minus=$_POST['labi'];
    $item=$_POST['lab'];

    $data = array(
               'ii_stock' => $minus
            );

  $this->db->where('ii_id', $item);
  $this->db->update('mst_inv_item', $data); 

  } 

  public function plusStock(){
    $plus=$_POST['qty'];
    $item=$_POST['item'];
    $this->db->where('ii_id', $item);
    $this->db->set('ii_stock',$plus);
    $this->db->update('mst_inv_item'); 

  } 

  public function getItem()
  {
    
    $data=$_POST['labi'];

    $this->db->select('*');
    $this->db->where('ii_category',$data);
    //$this->db->where('ii_stock >',0);
    $result=$this->db->get('mst_inv_item');


    echo "<option value='0'>- Pilih Item -</option>";
    foreach ($result->result_array() as $row) {
    echo "<option value=";
    echo $row['ii_id'];
    echo ">";
    echo $row['ii_code']." - ".$row['ii_name'];
    echo "</option>";
    };
  }
  /* VERSI 2*/
  
   
}
?>