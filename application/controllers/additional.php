<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
class additional extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('additionals');
    $this->load->model('finances');
      
  }
  public function index()
  {

    $data['additionals']=$this->additionals->getAdditional();
    $data['akun']=$this->finances->getAkun();



    $this->template->load('template','pages/additionals',$data);
  } 

  public function routeAdd()
  {

    $data['additionals']=$this->additionals->getRouteAdd();


    $this->template->load('template','pages/routeadd',$data);
  } 

  public function saveEdit()
  {
    $this->cities->editCities();
    redirect('city?msg=Save Success');
  }

  public function modaledit()
  {
    $data['cities']=$this->cities->getOneOrder();


    $this->load->view('pages/modalCities',$data);

   
  }

  public function getAkun()
    {
        $data=$this->input->post('id');

        $this->db->select('*');
        $this->db->where('id', $data);
        $result = $this->db->get('costcomponents');

        foreach ($result->result_array() as $key) {
         echo $key['akun_id'];

        };
    }

  public function saveAdditional()
  {
    $this->additionals->Simpan();
    redirect('additional?msg=Save Success');
  }

  public function saveRouteadd()
  {
    $this->additionals->SimpanRoute();
    redirect('additional/routeadd?msg=Save Success');
  }



  public function deleteAdditional()
  {
    $this->additionals->delete();
    redirect('additional?msg=Delete Success');
   
  }
  
}
?>