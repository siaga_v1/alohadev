<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class billing extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('billings');
      
  }
   
  // membuat fungsi index
  public function index()
  { 


    $data['billing']=$this->billings->getBilling();
 
    $data['id']=$this->billings->getLastID();
    $this->template->load('template','pages/billings',$data);
  } 

  public function saveEdit()
  {
    $this->billings->editBilling();
    redirect('billing?msg=Save Success');
  }

  public function modalBilling()
  {
    $data['billing']=$this->billings->getOneBilling();

    $this->load->model('customers');
    $data['customer']=$this->customers->getCustomer();

    $this->load->model('cities');
    $data['cities']=$this->cities->getCities();

    $this->load->model('ordersizeunits');
    $data['ordersizeunits']=$this->ordersizeunits->getOrderSizeUnits();

    $this->load->model('ordertypes');
    $data['ordertypes']=$this->ordertypes->getOrderTypes();

    $this->load->model('routes');
    $data['routes']=$this->routes->getRoutes();
    $this->load->view('pages/modalBilling',$data);
  }

  public function savebilling()
  {
    $this->billings->Simpan();
    redirect('billing?msg=Save Success');
  }



  public function deletebilling()
  {
    $this->billings->delete();
    redirect('billing?msg=Delete Success');
   
  }
  
}
?>