<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class city extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('cities');
      
  }
   
  // membuat fungsi index
  public function index()
  {

    $data['cities']=$this->cities->getCities();
    $data['id']=$this->cities->getLastID();


    $this->template->load('template','pages/cities',$data);
  } 

  public function saveEdit()
  {
    $this->cities->editCities();
    redirect('city?msg=Save Success');
  }

  public function modaledit()
  {
    $data['cities']=$this->cities->getOneOrder();


    $this->load->view('pages/modalCities',$data);

   
  }

  public function saveCities()
  {
    $this->cities->Simpan();
    redirect('City?msg=Save Success');
  }



  public function deleteCities()
  {
    $this->cities->delete();
    redirect('city?msg=Delete Success');
   
  }
  
}
?>