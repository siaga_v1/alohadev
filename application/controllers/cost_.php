<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class cost extends CI_Controller
{
// udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('costs');

  }
  public function index()
  {
    $data['cost']=$this->costs->getCostComponent();
    $data['list']=$this->costs->getAdditionalCost();
    $data['id']=$this->costs->getLastId();

    $this->load->model('cities');
    $data['cities']=$this->cities->getCities();
    $this->template->load('template','pages/cost',$data);
  }

  public function operasional()
  {
    $data['cost']=$this->costs->getComponentOffice();
    $this->load->model('employees');
    $data['karyawan']=$this->employees->getEmployees();
    $data['list']=$this->costs->getCostOffice();
    $data['id']=$this->costs->getLastId();
    $this->template->load('template','pages/costoperational',$data);
  }

  public function modaleditOffice()
  {
    $data['cost']=$this->costs->getComponentOffice();
    $this->load->model('employees');
    $data['karyawan']=$this->employees->getEmployees();
    $data['list']=$this->costs->getOneCostOffice();
    $data['id']=$this->costs->getLastId();
    $this->load->view('pages/modaleditOffice',$data);
  }

  public function getID()
    {   
        $id = $this->input->post_get('id');
        $text = $this->input->post_get('text');
        $this->db->select('*');
        $this->db->like('order_id',trim($text," "), 'both');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('cost',1);
        foreach ($result->result_array() as $key) {
          $number = $key['order_id'];
        }
        if (date('d')=='01' && !isset($number))
        { 
          $a = '0001'; 
        }
        elseif(isset($number))
        { 
          $nmr=$number;
          $ex = explode('/', $nmr);
          $a = sprintf("%04d", $ex[0]+1); 
        }elseif(!isset($number))
        { 
          $a = '0001'; 
        }
        $now=date('d-m-Y');
        $c = array('','I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
        $tahun=date('y');
        echo "".$a."/".$c[date('m')]."/".trim($text," ")."/OPRS";
    }

  public function get_autocomplete()
  {
    $query = $this->input->get('query');
    if (isset($query)) {
      $this->db->like('code', $query , 'both');
      $this->db->order_by('code', 'ASC');
      $this->db->limit(10);
      $result = $this->db->get('orders')->result();
      if (count($result) > 0) {
        foreach ($result as $row)
        {
          $arr_result[]=$row->code;
        }
        echo json_encode($arr_result);
      }
    }
  }

  public function deleteCostOffice()
    {
        $id=$this->input->post('id');
        
        $data['active']     = 0;
            
        $this->db->where('order_id', $id);
        $this->db->update('cost', $data); 
    }

  public function saveCost()
  {
    $this->costs->saveCosts();
    redirect('cost/operasional?msg=Save Success');
  }

  public function simpanCostOffice()
  {
    $this->form_validation->set_error_delimiters("<div class='alert alert-danger'><strong>Error ! </strong>", "</div>");
    $this->form_validation->set_rules('date','Tanggal','trim|required');
    $this->form_validation->set_rules('kategori','Kategori','trim|required');
    $this->form_validation->set_rules('karyawan','Karawan','trim|required');
    $this->form_validation->set_rules('keterangan','keterangan','trim|required');
    $this->form_validation->set_rules('nominal','Nominal','trim|required');

    if($this->form_validation->run())
    {
      $this->db->select("COUNT(*) AS total");
      $this->db->where("active", 1);
      $this->db->like("code", $this->input->post("code"));
      $result = $this->db->get("cost");
      if($result->row()->total == 0)
      {
        if(!$this->costs->saveCostOffice())
        {
          $return['type'] = 'error';
          $return['title'] = 'Update failed!';
          $return['message'] = 'Save order error, Internal server ersror!';
        }
      }
      else
      {
        $return['type'] = 'error';
        $return['title'] = 'Update failed!';
        $return['message'] = 'Shipment already exist!';
      }
    }
    else
    {
      $return['type'] = 'error';
      $return['title'] = 'Update failed!';
      $return['message'] = validation_errors();
    }
    
    redirect('cost/operasional?msg=Save Success');
  }

  public function saveEditCostOffice()
  {
      $this->costs->saveEditCostOffice();
    
    
    redirect('cost/operasional?msg=Save Success');
  }

}
?>