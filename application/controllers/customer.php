<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class customer extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('customers');
      
  }
   
  // membuat fungsi index
  public function index()
  {
    $data['customer']=$this->customers->getCustomer();
    $data['id']=$this->customers->getLastID();
    $this->template->load('template','pages/customers',$data);
  } 

  public function saveEdit()
  {
    $this->customers->editCustomer();
    redirect('customer?msg=Save Success');
  }

  public function modaledit()
  {
    $data=$_POST['id'];

    $this->db->select('*');
    $this->db->where('id',$data);
    $result=$this->db->get('customers');

    foreach ($result->result_array() as $row){
      echo "
        <div class='modal-dialog modal-lg'>
          <form class='form-horizontal' id='formspb' action='".site_url('customer/saveEdit')."' method='POST'>
            <div class='modal-content animated bounceInRight'>
              <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
                <h4 class='modal-title'>Edit Customer</h4>
              </div>
              
              <div class='modal-body'>
                <div class='form-group'>
                  <label class='col-sm-3 control-label'>Customer Code</label>
                  
                  <div class='col-sm-8'>
                    <input class='form-control' name='code'  type='text' value='".$row['code']."' readonly='readonly' />
                  </div>
                </div>  
                
                <div class='form-group'>
                  <label class='col-sm-3 control-label'>Customer Name</label>
                  
                  <div class='col-sm-8'>
                    <input class='form-control' name='name'  type='text' value='".$row['name']."' />
                  </div>
                </div>
                
                <div class='form-group'>
                  <label class='col-sm-3 control-label'>Customer Nickname</label>
                  
                  <div class='col-sm-8'>
                    <input class='form-control' name='nickname'  type='text' value='".$row['nickname']."' />
                  </div>
                </div>
                
                <div class='form-group'>
                  <label class='col-sm-3 control-label'>Customer Address</label>
                  
                  <div class='col-sm-8'>
                    <input type='text' class='form-control' name='address_1' value='".$row['address_1']."' ><br>
                    <input type='text' class='form-control' name='address_2' value='".$row['address_2']."' ><br>
                    <input type='text' class='form-control' name='address_3' value='".$row['address_3']."' >
                  </div>
                </div>
                
                <div class='form-group'>
                  <label class='col-sm-3 control-label'>Customer Phone</label>
                  
                  <div class='col-sm-8'>
                    <input class='form-control' name='phone'  type='text' value='".$row['phone']."'/>
                  </div>
                </div>
                
                <div class='form-group'>
                  <label class='col-sm-3 control-label'>Customer Email</label>
                  
                  <div class='col-sm-8'>
                    <input class='form-control' name='email'  type='text' value='".$row['email']."'/>
                  </div>
                </div>
                
                <div class='form-group'>
                  <label class='col-sm-3 control-label'>PIC Name</label>
                  
                  <div class='col-sm-8'>
                    <input class='form-control' name='pic_name'  type='text' value='".$row['pic_name']."' />
                  </div>
                </div>
                
                <div class='form-group'>
                  <label class='col-sm-3 control-label'>Term</label>
                  
                  <div class='col-sm-2'>
                    <input class='form-control' name='term'  type='text' value='".$row['term']."' />
                  </div>
                  
                  <label class='col-sm-2 control-label'>PPN</label>
                    
                 
                  <div class='input-group col-sm-2'>
                    <input class='form-control' name='ppn'  type='text' value='".$row['ppn']."' />

                    <span class='input-group-addon'>%</span> 
                  </div>
                </div>
              </div>
              
              <div class='modal-footer'>
                <button type='button' class='btn btn-danger' data-dismiss='modal'>Reset</button>
                <input type='submit' style='margin-bottom: 5px;' name='submit' class='btn btn-primary' value='Save!'>
              </div>
            </div>
          </form>
        </div>
      ";
    }
  }

  public function saveCustomer()
  {
    $this->customers->Simpan();
    redirect('customer?msg=Save Success');
  }


  public function getCust()
  {
    $check = $this->input->post('setting');
    if($check == 'ambil')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('customers');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['name']."'>".$key['name']."</option>";

        };
      }

       if($check == 'costtype')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('costcomponents');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['id']."'>".$key['name']."</option>";

        };
      }

     if($check == 'tipe')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('drivers');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['name']."'>".$key['name']."</option>";

        };
      }
      if($check == 'drv')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('drivers');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['id']."'>".$key['name']."</option>";

        };
      }
      if($check == 'koordinator')
      {
        $this->db->select('*');
        $this->db->where('id_userroles', '3');
        $result = $this->db->get('users');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['id']."'>".$key['firstname']."</option>";

        };
      }
     if($check == 'tipearm')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('fleettypes');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['name']."'>".$key['name']."</option>";

        };
      }
      if($check == 'typearm')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('fleettypes');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['id']."'>".$key['name']."</option>";

        };
      }
      if($check == 'type')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('fleettypes');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['name']."'>".$key['name']."</option>";

        };
      }
      if($check == 'dari')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('cities');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['name']."'>".$key['name']."</option>";

        };
      }

      if($check == 'plat')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('fleets');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['fleetplateno']."'>".$key['fleetplateno']."</option>";

        };
      }
      if($check == 'ke')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('cities');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['name']."'>".$key['name']."</option>";

        };
      }
      if($check == 'kiki')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $this->db->where('type', '2');
        $result = $this->db->get('costcomponents');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['id']."'>".$key['name']."</option>";

        };
      }
      if($check == 'cccc')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $this->db->where('parent', '111');
        $result = $this->db->get('chartofaccount');

        echo "<option value=''>- ALL - </option>";
        foreach ($result->result_array() as $key) {
         echo "<option value='".$key['id']."'>".$key['name']."</option>";

        };
      }
      if($check == 'kik')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $this->db->where('id', $origin);
        $result = $this->db->get('costcomponents');

        foreach ($result->result_array() as $key) {
         echo $key['nominal'];

        };
      }
      if($check == 'ki')
      {
        $this->db->select('*');
        $this->db->where('active', '1');
        $this->db->where('id', $origin);
        $result = $this->db->get('costcomponents');

        foreach ($result->result_array() as $key) {
         echo $key['nominal2'];

        };
      }
  }

  public function deleteCustomer()
  {
    $this->customers->delete();
    redirect('customer?msg=Delete Success');
   
  }
  
}
?>