<?php if (!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class Dashboard extends CI_Controller
{
    // udah tau ini apa :p
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('modelDashboard');
        $this->load->model('modelMaintenance');
        $this->load->model('modelNotifikasi');
    }

    // membuat fungsi index
    public function index()
    {
        $thisyear   = date('Y-m-01');
        $lastyear   = date('Y-m-d', strtotime($thisyear . ' -1 years'));
        $y = date('Y');
        $lasty = date('Y', strtotime($lastyear));

        $this->load->model('Reports');
        $data['orders'] = $this->Reports->getOrderNum();
        $orderyears = $this->Reports->getOrderNumYear($y);
        $datayears = [
            $orderyears->order1,
            $orderyears->order2,
            $orderyears->order3,
            $orderyears->order4,
            $orderyears->order5,
            $orderyears->order6,
            $orderyears->order7,
            $orderyears->order8,
            $orderyears->order9,
            $orderyears->order10,
            $orderyears->order11,
            $orderyears->order12
        ];
        $orderlastyears = $this->Reports->getOrderNumYear($lasty);

        $datalastyears = [
            $orderyears->order1,
            $orderyears->order2,
            $orderyears->order3,
            $orderyears->order4,
            $orderyears->order5,
            $orderlastyears->order6,
            $orderlastyears->order7,
            $orderlastyears->order8,
            $orderlastyears->order9,
            $orderlastyears->order10,
            $orderlastyears->order11,
            $orderlastyears->order12
        ];

        $data['datayears'] = json_encode($datayears);
        $data['datalastyears'] = json_encode($datalastyears);
        //$data['item']=$this->modelDashboard->itemDashboard();
        $this->template->load('template', 'Pages/dashboard', $data);
    }

    public function dashboardSales()
    {
        $thisyear   = date('Y-m-01');
        $lastyear   = date('Y-m-d', strtotime($thisyear . ' -1 years'));
        $y = date('Y');
        $lasty = date('Y', strtotime($lastyear));

        $this->load->model('Reports');
        $data['orders'] = $this->Reports->getOrderNum();

        $this->load->model('orderclaims');
        $data['last'] = $this->orderclaims->lastInput();

        $orderyears = $this->Reports->getOrderNumYear($y);
        $datayears = [
            $orderyears->order1,
            $orderyears->order2,
            $orderyears->order3,
            $orderyears->order4,
            $orderyears->order5,
            $orderyears->order6,
            $orderyears->order7,
            $orderyears->order8,
            $orderyears->order9,
            $orderyears->order10,
            $orderyears->order11,
            $orderyears->order12
        ];

        $orderlastyears = $this->Reports->getSalesNumYear($y);
        $addspld = $this->Reports->getAddSalesspld($y);
        $addpalembang = $this->Reports->getAddSalespalembang($y);
        $datalastyears = [
            $orderlastyears->order1 + $addspld->hasilpareto1 + $addpalembang->palembang1,
            $orderlastyears->order2 + $addspld->hasilpareto2 + $addpalembang->palembang2,
            $orderlastyears->order3 + $addspld->hasilpareto3 + $addpalembang->palembang3,
            $orderlastyears->order4 + $addspld->hasilpareto4 + $addpalembang->palembang4,
            $orderlastyears->order5 + $addspld->hasilpareto5 + $addpalembang->palembang5,
            $orderlastyears->order6 + $addspld->hasilpareto6 + $addpalembang->palembang6,
            $orderlastyears->order7 + $addspld->hasilpareto7 + $addpalembang->palembang7,
            $orderlastyears->order8 + $addspld->hasilpareto8 + $addpalembang->palembang8,
            $orderlastyears->order9 + $addspld->hasilpareto9 + $addpalembang->palembang9,
            $orderlastyears->order10 + $addspld->hasilpareto10 + $addpalembang->palembang10,
            $orderlastyears->order11 + $addspld->hasilpareto11 + $addpalembang->palembang11,
            $orderlastyears->order12 + $addspld->hasilpareto12 + $addpalembang->palembang12
        ];

        $data['datayears'] = json_encode($datayears);
        $data['stnk'] = $this->modelNotifikasi->notifikasiSTNK();
        $data['datalastyears'] = json_encode($datalastyears);
        $this->template->load('template', 'Pages/dashboardSales', $data);
        //$this->template->load('template', 'Pages/dashboardSales');
    }

    public function productivity()
    {
        $thisyear   = date('Y-m-01');
        $lastyear   = date('Y-m-d', strtotime($thisyear . ' -1 years'));
        $y = date('Y');
        $lasty = date('Y', strtotime($lastyear));

        $this->load->model('Reports');
        $data['orders'] = $this->Reports->getOrderNum();

        $this->load->model('orderclaims');
        $data['last'] = $this->orderclaims->lastInput();

        $orderyears = $this->Reports->getOrderNumYear($y);
        $datayears = [
            $orderyears->order1,
            $orderyears->order2,
            $orderyears->order3,
            $orderyears->order4,
            $orderyears->order5,
            $orderyears->order6,
            $orderyears->order7,
            $orderyears->order8,
            $orderyears->order9,
            $orderyears->order10,
            $orderyears->order11,
            $orderyears->order12
        ];

        $orderlastyears = $this->Reports->getSalesNumYear($y);
        $addspld = $this->Reports->getAddSalesspld($y);
        $addpalembang = $this->Reports->getAddSalespalembang($y);
        $datalastyears = [
            $orderlastyears->order1 + $addspld->hasilpareto1 + $addpalembang->palembang1,
            $orderlastyears->order2 + $addspld->hasilpareto2 + $addpalembang->palembang2,
            $orderlastyears->order3 + $addspld->hasilpareto3 + $addpalembang->palembang3,
            $orderlastyears->order4 + $addspld->hasilpareto4 + $addpalembang->palembang4,
            $orderlastyears->order5 + $addspld->hasilpareto5 + $addpalembang->palembang5,
            $orderlastyears->order6 + $addspld->hasilpareto6 + $addpalembang->palembang6,
            $orderlastyears->order7 + $addspld->hasilpareto7 + $addpalembang->palembang7,
            $orderlastyears->order8 + $addspld->hasilpareto8 + $addpalembang->palembang8,
            $orderlastyears->order9 + $addspld->hasilpareto9 + $addpalembang->palembang9,
            $orderlastyears->order10 + $addspld->hasilpareto10 + $addpalembang->palembang10,
            $orderlastyears->order11 + $addspld->hasilpareto11 + $addpalembang->palembang11,
            $orderlastyears->order12 + $addspld->hasilpareto12 + $addpalembang->palembang12
        ];

        $data['datayears'] = json_encode($datayears);
        $data['stnk'] = $this->modelNotifikasi->notifikasiSTNK();
        $data['datalastyears'] = json_encode($datalastyears);
        $this->template->load('template', 'Pages/dashboardproductivity', $data);
    }

    public function finance()
    {
        $thisyear   = date('Y-m-01');
        $lastyear   = date('Y-m-d', strtotime($thisyear . ' -1 years'));
        $y = date('Y');
        $lasty = date('Y', strtotime($lastyear));

        $this->load->model('Reports');
        $data['orders'] = $this->Reports->getOrderNum();

        $this->load->model('orderclaims');
        $data['last'] = $this->orderclaims->lastInput();

        $orderyears = $this->Reports->getOrderNumYear($y);
        $datayears = [
            $orderyears->order1,
            $orderyears->order2,
            $orderyears->order3,
            $orderyears->order4,
            $orderyears->order5,
            $orderyears->order6,
            $orderyears->order7,
            $orderyears->order8,
            $orderyears->order9,
            $orderyears->order10,
            $orderyears->order11,
            $orderyears->order12
        ];

        $orderlastyears = $this->Reports->getSalesNumYear($y);
        $addspld = $this->Reports->getAddSalesspld($y);
        $addpalembang = $this->Reports->getAddSalespalembang($y);
        $datalastyears = [
            $orderlastyears->order1 + $addspld->hasilpareto1 + $addpalembang->palembang1,
            $orderlastyears->order2 + $addspld->hasilpareto2 + $addpalembang->palembang2,
            $orderlastyears->order3 + $addspld->hasilpareto3 + $addpalembang->palembang3,
            $orderlastyears->order4 + $addspld->hasilpareto4 + $addpalembang->palembang4,
            $orderlastyears->order5 + $addspld->hasilpareto5 + $addpalembang->palembang5,
            $orderlastyears->order6 + $addspld->hasilpareto6 + $addpalembang->palembang6,
            $orderlastyears->order7 + $addspld->hasilpareto7 + $addpalembang->palembang7,
            $orderlastyears->order8 + $addspld->hasilpareto8 + $addpalembang->palembang8,
            $orderlastyears->order9 + $addspld->hasilpareto9 + $addpalembang->palembang9,
            $orderlastyears->order10 + $addspld->hasilpareto10 + $addpalembang->palembang10,
            $orderlastyears->order11 + $addspld->hasilpareto11 + $addpalembang->palembang11,
            $orderlastyears->order12 + $addspld->hasilpareto12 + $addpalembang->palembang12
        ];

        $data['datayears'] = json_encode($datayears);
        $data['stnk'] = $this->modelNotifikasi->notifikasiSTNK();
        $data['datalastyears'] = json_encode($datalastyears);
        $this->template->load('template', 'Pages/dashboardfinance', $data);
    }

    public function truck()
    {
        $data['tahun'] = (int)date('Y');
        $data['totalmonth'] = $data['tahun'] > 2018 ? 12 : 7;
        $data['x'] = $data['tahun'] > 2018 ? 1 : 6;
        $check = $this->input->post('setting');


        // $data['nums'] = json_encode($datanum);
        // $this->load->view('charts/target2',$data);


        if ($check == 'truckritase') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reporttruckmonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            $datacost = [];
            $dataritase = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->fleetplateno;
                $datanum[] = (int)$row->prices;
                $datacost[] = (int)$row->costs;
                $dataritase[] = (int)$row->ritase;
                if ($x == 10) {
                    break;
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $data['datacost'] = json_encode($datacost);
            $data['dataritase'] = json_encode($dataritase);

            $this->load->view('charts/ritase', $data);
        }

        if ($check == 'claim') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reportclaims($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->driver;
                $datanum[] = (int)$row->ritase;
                //if($x == 25){
                //break;
                //}

                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $this->load->view('charts/claim', $data);
        }

        if ($check == 'truckritaselow') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reporttruckmonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            $datacost = [];
            $dataritase = [];
            $lasttotal = ($total - 5);
            foreach ($rows as $row) {
                if ($x > $lasttotal) {
                    $datalabel[] = $row->fleetplateno;
                    $datanum[] = (int)$row->prices;
                    $datacost[] = (int)$row->costs;
                    $dataritase[] = (int)$row->ritase;
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['lasttotal'] = $lasttotal;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $data['datacost'] = json_encode($datacost);
            $data['dataritase'] = json_encode($dataritase);
            $this->load->view('charts/truckritaselow', $data);
        }

        //DRIVER RITASE

        if ($check == 'driverritase') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reportdrivermonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            $datacost = [];
            $dataritase = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->name;
                $datanum[] = (int)$row->prices;
                $datacost[] = (int)$row->costs;
                $dataritase[] = (int)$row->ritase;
                if ($x == 10) {
                    break;
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $data['datacost'] = json_encode($datacost);
            $data['dataritase'] = json_encode($dataritase);

            $this->load->view('charts/driverritase', $data);
        }

        if ($check == 'driverritaselow') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reportdrivermonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            $datacost = [];
            $dataritase = [];
            $lasttotal = ($total - 5);
            foreach ($rows as $row) {
                if ($x > $lasttotal) {
                    $datalabel[] = $row->name;
                    $datanum[] = (int)$row->prices;
                    $datacost[] = (int)$row->costs;
                    $dataritase[] = (int)$row->ritase;
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['lasttotal'] = $lasttotal;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $data['datacost'] = json_encode($datacost);
            $data['dataritase'] = json_encode($dataritase);
            $this->load->view('charts/driverritaselow', $data);
        }
        ////

        if ($check == 'trucksales') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reporttrucks($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->fleetplateno;
                $datanum[] = (int)$row->prices;
                if ($x == 10) {
                    break;
                }

                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $this->load->view('charts/trucksales', $data);
        }

        if ($check == 'truckachieves') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reporttruckmonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            $datacost = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->fleetplateno;
                $datanum[] = number_format($row->prices / 1000000, 2);
                $datacost[] = number_format($row->allowances / 1000000, 2);
                if ($x == 10) {
                    break;
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $data['datacost'] = json_encode($datacost);
            $this->load->view('charts/trucksales', $data);
        }
        if ($check == 'trucke') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reporttruckmonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            $datacost = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->fleetplateno;
                $datanum[] = number_format($row->allowances / 1000000, 2);
                $datacost[] = number_format($row->achieves / 1000000, 2);
                if ($x == 10) {
                    break;
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $data['datacost'] = json_encode($datacost);
            $this->load->view('charts/trucksalesCC', $data);
        }
        if ($check == 'truckf') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reporttruckmonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            $datacost = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->fleetplateno;
                $datanum[] = number_format($row->allowances / 1000000, 2);
                $datacost[] = number_format($row->achieves / 1000000, 2);
                if ($x == 10) {
                    break;
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $data['datacost'] = json_encode($datacost);
            $this->load->view('charts/trucksalesTansya', $data);
        }

        if ($check == 'truckg') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reporttruckmonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            $datacost = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->fleetplateno;
                $datanum[] = number_format($row->allowances / 1000000, 2);
                $datacost[] = number_format($row->achieves / 1000000, 2);
                if ($x == 10) {
                    break;
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $data['datacost'] = json_encode($datacost);
            $this->load->view('charts/trucksalesTW', $data);
        }

        if ($check == 'truckh') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reporttruckmonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            $datacost = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->fleetplateno;
                $datanum[] = number_format($row->allowances / 1000000, 2);
                $datacost[] = number_format($row->achieves / 1000000, 2);
                if ($x == 10) {
                    break;
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $data['datacost'] = json_encode($datacost);
            $this->load->view('charts/trucksalesPareto', $data);
        }

        if ($check == 'trucki') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reporttruckmonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            $datacost = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->fleetplateno;
                $datanum[] = number_format($row->allowances / 1000000, 2);
                $datacost[] = number_format($row->achieves / 1000000, 2);
                if ($x == 10) {
                    break;
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $data['datacost'] = json_encode($datacost);
            $this->load->view('charts/trucksalesWB', $data);
        }

        if ($check == 'truckj') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reporttruckmonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            $datacost = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->fleetplateno;
                $datanum[] = number_format($row->costs / 1000000, 2);
                $datacost[] = number_format($row->achieves / 1000000, 2);
                if ($x == 10) {
                    break;
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $data['datacost'] = json_encode($datacost);
            $this->load->view('charts/trucksalesDSO', $data);
        }

        if ($check == 'truckachieveslow') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reporttruckmonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            $datacost = [];
            $lasttotal = ($total - 5);
            foreach ($rows as $row) {
                if ($x > $lasttotal) {
                    $datalabel[] = $row->fleetplateno;
                    $datanum[] = number_format($row->prices / 1000000, 2);
                    $datacost[] = number_format($row->achieves / 1000000, 2);
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['lasttotal'] = $lasttotal;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $data['datacost'] = json_encode($datacost);
            $this->load->view('charts/trucksaleslow', $data);
        }
    }

    public function customer()
    {
        $data['tahun'] = (int)date('Y');
        $data['totalmonth'] = $data['tahun'] > 2018 ? 12 : 7;
        $data['x'] = $data['tahun'] > 2018 ? 1 : 6;
        $check = $this->input->post('setting');

        if ($check == 'ritase') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reportcustomermonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->customers_alias;
                $datanum[] = (int)$row->ritase;
                if ($x == 10) {
                    break;
                }

                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $this->load->view('charts/customerritase', $data);
        }

        if ($check == 'sales') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reportcustomermonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->customers_alias;
                $datanum[] = number_format($row->prices / 1000000, 2);
                if ($x == 10) {
                    break;
                }

                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $this->load->view('dashboard/customersales', $data);
        }

        if ($check == 'achieves') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->reportcustomermonths($data);
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            foreach ($rows as $row) {
                $datalabel[] = $row->customers_alias;
                $datanum[] = (int)$row->achieves;
                if ($x == 10) {
                    break;
                }
                $x++;
            }

            $data['rows'] = $rows;
            $data['label'] = json_encode($datalabel);
            $data['nums'] = json_encode($datanum);
            $this->load->view('dashboard/customerachieves', $data);
        }
    }


    public function target()
    {
        $data['tahun'] = (int)date('Y');
        $data['totalmonth'] = $data['tahun'] > 2018 ? 12 : 7;
        $data['x'] = $data['tahun'] > 2018 ? 1 : 6;
        $check = $this->input->post('setting');

        if ($check == 'achieves') {
            $data['setting'] = $check;
            $this->load->model('Reports');
            $a['target'] = $this->Reports->targetVer2($data);
            $a['dso'] = $this->Reports->targetDSOw($data);

            $this->load->view('charts/targetVer2', $a);
        }

        if ($check == 'achieves2') {

            $data['setting'] = $check;
            $this->load->model('Reports');
            $rows = $this->Reports->targettruck();
            $total = count($rows);
            $label = "";
            $x = 1;
            $datalabel = [];
            $datanum = [];
            foreach ($rows as $row) {
                $datanum[] = array($x, $row->ritase);
                if ($x == 12) {
                    break;
                }

                $x++;
            }

            // $data['rows'] = $rows;
            $data['nums'] = $datanum;
            $data['label'] = $datalabel;
            $d['myarray'] = json_encode($datanum);

            /////

            $rows2 = $this->Reports->targetdso();
            $total2 = count($rows2);
            $label2 = "";
            $x2 = 1;
            $datalabel2 = [];
            $datanum2 = [];
            foreach ($rows2 as $ro) {
                $datanum2[] = array($x2, $ro->ritase);
                if ($x2 == 12) {
                    break;
                }

                $x2++;
            }
            $d['myarray2'] = json_encode($datanum2);

            ////////

            $rows3 = $this->Reports->targetdsoluar();
            $total3 = count($rows3);
            $label3 = "";
            $x3 = 1;
            $datalabel3 = [];
            $datanum3 = [];
            foreach ($rows3 as $r) {
                $datanum3[] = array($x3, $r->ritase);
                if ($x3 == 12) {
                    break;
                }

                $x3++;
            }
            $d['myarray3'] = json_encode($datanum3);

            $sum1 = $this->Reports->sumdso();
            foreach ($sum1 as $satu) {
                $rit1 = $satu['ritase'];
                $cost1 = $satu['allowances'] + $satu['allowanceadds'];
            }
            $d['rit1'] = $rit1;
            $d['cost1'] = $cost1;


            $sum3 = $this->Reports->sumcc();
            foreach ($sum3 as $tiga) {
                $rit3 = $tiga['ritase'];
                $cost3 = $tiga['allowances'] + $tiga['allowanceadds'];
            }
            $d['rit3'] = $rit3;
            $d['cost3'] = $cost3;

            $this->load->view('charts/target2', $d);
        }

        if ($check == 'detailCC') {
            $data['setting'] = $check;
            $this->load->model('Reports');

            $rows['top'] = $this->Reports->detailCC($data);


            $this->load->view('charts/modalTarget', $rows);
        }

        if ($check == 'detailTN') {
            $data['setting'] = $check;
            $this->load->model('Reports');

            $rows['top'] = $this->Reports->detailTN($data);


            $this->load->view('charts/modalTarget', $rows);
        }

        if ($check == 'detailTW') {
            $data['setting'] = $check;
            $this->load->model('Reports');

            $rows['top'] = $this->Reports->detailTW($data);


            $this->load->view('charts/modalTarget', $rows);
        }

        if ($check == 'detailWB') {
            $data['setting'] = $check;
            $this->load->model('Reports');

            $rows['top'] = $this->Reports->detailWB($data);


            $this->load->view('charts/modalTarget', $rows);
        }

        if ($check == 'detailDSOw') {
            $data['setting'] = $check;
            $this->load->model('Reports');

            $rows['top'] = $this->Reports->detailDSOw($data);


            $this->load->view('charts/modalTarget', $rows);
        }
        if ($check == 'detailPR') {
            $data['setting'] = $check;
            $this->load->model('Reports');

            $rows['top'] = $this->Reports->detailPR($data);


            $this->load->view('charts/modalTarget', $rows);
        }
        if($check == 'periodeTarget')
        {
            $this->load->model('Reports');
            $data['orders'] = $this->Reports->getSalesNum();
            $this->load->view('charts/periodeTarget',$data);
        }
        if($check == 'piutang')
        {
            $this->load->model('Reports');
            $sum1 = $this->Reports->sumunpaid();
            foreach($sum1 AS $satu)
            {
                $paid= $satu['PAID'];
                $unpaid= $satu['UNPAID'];
                $lastpaid= $satu['LASTPAID'];
                $lastunpaid= $satu['LASTUNPAID'];
            }
            $d['paid'] = $paid;
            $d['unpaid'] = $unpaid;
            $d['lastpaid'] = $lastpaid;
            $d['lastunpaid'] = $lastunpaid;

            $sum2 = $this->Reports->monthar();
            foreach($sum2 AS $dua)
            {
                $ujs= $dua['ujs'];
                $lastujs= $dua['lastujs'];
                $monthar= $dua['monthar'];
                $lastmonthar= $dua['lastmonthar'];
            }
            $d['ujs'] = $ujs;
            $d['lastujs'] = $lastujs;
            $d['monthar'] = $monthar;
            $d['lastmonthar'] = $lastmonthar;

            $this->load->view('charts/piutang',$d);
        }

        if($check == 'ARControl')
        {
            $this->load->model('Reports');
            $sum1['Invoice'] = $this->Reports->ARControl();

            $this->load->view('charts/ARControl',$sum1);
        }
    }

    public function fleetavailable()
    {
        $this->load->model('fleets');
        //$rows = $this->fleets->getAvailableFleets();
        //$data['rows'] = $rows;
        $this->load->view('charts/fleetavailable');
    }
}
