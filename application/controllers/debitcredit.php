<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class debitcredit extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('debitcredits');
      
  }

     
   
  // membuat fungsi index
  public function index()
  {
    $data['debit']=$this->debitcredits->getDebitcredit();
    $data['necessary']=$this->debitcredits->getNecessary();
    $data['koordinator']=$this->debitcredits->getKoordinator();
    $data['id']=$this->debitcredits->getLastID();
    $this->load->model('employees');
    $data['employee']=$this->employees->getEmployees();
    $this->template->load('template','pages/debitcredit',$data);
  } 


  public function saldo()
  {
    $data['debit']=$this->debitcredits->getDebitcredit();
    $data['necessary']=$this->debitcredits->getNecessary();
    $data['koordinator']=$this->debitcredits->getKoordinator();
    $data['id']=$this->debitcredits->getLastID();
    $this->load->model('employees');
    $data['employee']=$this->employees->getEmployees();
    $this->template->load('template','pages/saldokr',$data);
  }

  public function getDetailNecessary()
  {   
     $id = $this->input->post('id'); 
     $check = $this->input->post('setting');

        if ($check == "nominal") {
           
        $this->db->select('*');
        $this->db->where('id', $id);
        $result = $this->db->get('requestpayable');
          foreach ($result->result() as $key) {
           echo number_format($key->price);

          };
        }
        if ($check == "deskripsi") {
           
        $this->db->select('*');
        $this->db->where('id', $id);
        $result = $this->db->get('requestpayable');
          foreach ($result->result() as $key) {
           echo $key->desciption;

          };
        }
   }

  
  public function credit()
  {
    $data['credit']=$this->debitcredits->getCredit();
    $this->template->load('template','pages/credit',$data);
  } 


  public function refund()
  {
    $data['refund']=$this->debitcredits->getRefund();
    $this->template->load('template','pages/refund',$data);
  } 

  public function requestpayable()
  {
    $data['necessary']=$this->debitcredits->getNecessary();
    $this->template->load('template','pages/requestpayable',$data);
  }

  public function modalDetail()
  {
    $data['debit']=$this->debitcredits->getOneDebit();

    $this->load->view('pages/modalDebit',$data);
   
  }

  public function modalDetailDownpayment()
  {
    $data['detail']=$this->debitcredits->getDetailDownpayment();

    $this->load->view('pages/modalDetailDownpayment',$data);
   
  }

  

  public function modalEdit()
  {
    $data['debit']=$this->debitcredits->getOneDebit();
    $data['necessary']=$this->debitcredits->getNecessary();
    $data['koordinator']=$this->debitcredits->getKoordinator();
    $data['id']=$this->debitcredits->getLastID();
    $this->load->model('employees');
    $data['employee']=$this->employees->getEmployees();

    $this->load->view('pages/modalDedit',$data);
   
  }

  public function modalRedit()
  {
    $data['debit']=$this->debitcredits->getOneRequest();

    $this->load->view('pages/modalRedit',$data);
   
  }

  public function saveEdit()
  {
    $this->debitcredits->editDebit();
    redirect('debitcredit?msg=Save Success');
  }

  public function saveRequest()
  {
    $this->debitcredits->editRequest();
    redirect('debitcredit/requestpayable?msg=Save Success');
  }
  public function saveRequestPayable()
  {
    $this->debitcredits->saveRequest();
    redirect('debitcredit/requestpayable?msg=Save Success');
  }

  public function saveRefund()
  {
    $this->debitcredits->saveRefund();
    redirect('debitcredit/refund?msg=Save Success');
  }

  public function approvedebit()
  {
    $this->debitcredits->approvedebit();
    redirect('debitcredit?msg=Approved Success');
  }


  public function saveDebitcredit()
  {
    $this->debitcredits->Simpan();
    redirect('debitcredit?msg=Save Success');
  }



  public function deleteRequest()
  {
    $this->debitcredits->deleteRequest();
    redirect('debitcredit/requestpayable?msg=Delete Success');
   
  }
  
}
?>