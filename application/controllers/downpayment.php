<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class downpayment extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('downpayments');
    $this->load->model('employees');
      
  }

  public function index()
  {
    $data['downpayment']=$this->downpayments->getDownpayment();
    $data['employee']=$this->downpayments->getKaryawan();
    $this->template->load('template','pages/downpayment',$data);
  } 

  public function costoperational()
  {

    $data['costopr']=$this->downpayments->getCostOpr();
    $data['employee']=$this->employees->getEmployees();
    $data['keterangan']=$this->downpayments->getKeterangan();
    $this->template->load('template','pages/costoperational',$data);
  } 


  public function saveDownpayment()
  {
    $this->downpayments->Simpan();
    redirect('downpayment?msg=Save Success');
  }

  public function simpanDownpayment()
  {
    $this->downpayments->simpanDownpayment();
    redirect('downpayment?msg=Save Success');
  }

  public function bayarDownpayment()
  {
    $this->downpayments->bayarDownpayment();
    redirect('downpayment?msg=Save Success');
  }

  public function saldo()
  {
    $data['debit']=$this->debitcredits->getDebitcredit();
    $data['necessary']=$this->debitcredits->getNecessary();
    $data['koordinator']=$this->debitcredits->getKoordinator();
    $data['id']=$this->debitcredits->getLastID();
    $this->load->model('employees');
    $data['employee']=$this->employees->getEmployees();
    $this->template->load('template','pages/saldokr',$data);
  }

  public function getDetailNecessary()
  {   
    $id = $this->input->post('id'); 
    $check = $this->input->post('setting');

    if ($check == "nominal") {

      $this->db->select('*');
      $this->db->where('id', $id);
      $result = $this->db->get('requestpayable');
      foreach ($result->result() as $key) {
        echo number_format($key->price);

      };
    }
    if ($check == "deskripsi") {

      $this->db->select('*');
      $this->db->where('id', $id);
      $result = $this->db->get('requestpayable');
      foreach ($result->result() as $key) {
        echo $key->desciption;

      };
    }
  }

  public function getGaji()
  {   
    $id = $this->input->post('id'); 
    $ids = $this->input->post('employees'); 

    if ($id== 1) {

      $this->db->select('*');
      $this->db->where('id', $ids);
      $result = $this->db->get('employees');
      foreach ($result->result() as $key) {
        echo number_format($key->salary);

      };
    }
     if ($id== 2) {

      $this->db->select('*');
      $this->db->where('id', $ids);
      $result = $this->db->get('employees');
      foreach ($result->result() as $key) {
        echo number_format($key->feeLunch);

      };
    }
  }


  public function credit()
  {
    $data['credit']=$this->debitcredits->getCredit();
    $this->template->load('template','pages/credit',$data);
  } 

  public function requestpayable()
  {
    $data['necessary']=$this->debitcredits->getNecessary();
    $this->template->load('template','pages/requestpayable',$data);
  }

  public function modalDetail()
  {
    $data['debit']=$this->debitcredits->getOneDebit();

    $this->load->view('pages/modalDebit',$data);
   
  }

  public function modalEdit()
    { 
      $data['downpayment']=$this->downpayments->getOneDownpayment();
      $data['detail']=$this->downpayments->getDetailDownpayment();
      $this->load->model('drivers');
      $data['drivers']=$this->downpayments->getKaryawan();

      $this->load->view('pages/modalEditDownpayment',$data);
     
    }

  public function modalRedit()
  {
    $data['debit']=$this->debitcredits->getOneRequest();

    $this->load->view('pages/modalRedit',$data);
   
  }

  public function modalAddDownpayment()
  {
    $this->load->model('drivers');
    $data['drivers']=$this->downpayments->getKaryawan();
    $data['id'] = $this->input->post('id');

    $this->load->view('pages/modalAddDownpayment',$data);
   
  }

  public function modalBayarDownpayment()
  {
    $this->load->model('drivers');
    $data['drivers']=$this->downpayments->getKaryawan();
    $data['id'] = $this->input->post('id');

    $this->load->view('pages/modalBayarDownpayment',$data);
   
  }

  public function saveEdit()
  {
    $this->downpayments->editDownpayment();
    redirect('downpayment?msg=Save Success');
  }

  public function saveRequest()
  {
    $this->debitcredits->editRequest();
    redirect('debitcredit/requestpayable?msg=Save Success');
  }

  public function approvedebit()
  {
    $this->debitcredits->approvedebit();
    redirect('debitcredit?msg=Approved Success');
  }

  public function deleteDownpayment()
  {
    $this->downpayments->delete();
   // redirect('customer?msg=Delete Success');
   
  }

  public function editinline()
  {
    $id = $_POST['id'];
    $nominal = $_POST['nominal'];
    $data['nominal'] = $nominal;
    $this->db->where('id',$id);
    $this->db->update('downpaymentouts',$data);
  }
  
}
?>