<?php if (!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class driver extends CI_Controller
{
  // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('drivers');
  }

  // membuat fungsi index
  public function index()
  {

    $data['driver'] = $this->drivers->getDrivers();
    $data['id'] = $this->drivers->getLastID();


    $this->template->load('template', 'pages/drivers', $data);
  }

  public function getDriversAbsensi()
  {
    $tgl = $this->input->post('date');

    $sql = "SELECT dbo.drivers.* FROM dbo.drivers WHERE dbo.drivers.active = 1 and  dbo.drivers.id IN (SELECT dbo.absensi.id_drivers FROM dbo.absensi WHERE dbo.absensi.tanggal_absensi = '$tgl' and  dbo.absensi.tipe_absensi = '1')";
    $result = $this->db->query($sql);
    foreach ($result->result_array() as $row) {
      if (is_null($row['id'])) {
        echo "Kosong Dong";
      } else {
        echo "<option value=''></option>  
                    <option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
      }
    }
  }

  public function listAbsensi()
  {

    $data['driver'] = $this->drivers->getDriversAbsensi();
    $data['list_driver'] = $this->drivers->getDriversAbsensi();


    $this->template->load('template', 'pages/listAbsensi', $data);
  }

  public function absensiDriver()
  {

    $data['driver'] = $this->drivers->getDriversAbsensi();
    $data['list_driver'] = $this->drivers->getDriversAbsensi();


    $this->template->load('template', 'pages/absensiDriver', $data);
  }


  public function loadListAbsensi($page = 0)

  {
    $perpage = 70;

    $data = array();
    $filter = array();
    $data['search'] = array();

    $limit  = $perpage;
    $offset = $page > 1 ? ($limit * $page) - $limit : 0;

    $all_data =  $this->drivers->loadListAbsensi($filter);
    $data['total'] = count($all_data);
    $config['base_url'] = base_url() . '/driver/loadListAbsensi';
    $config['use_page_numbers'] = TRUE;
    $config['total_rows'] = $data['total'];
    $config['per_page'] = $perpage;
    $config['num_links'] = 1;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['first_link'] = 'First';
    $config['first_tag_open'] = '<li class="prev page">';
    $config['first_tag_close'] = '</li>';
    $config['last_link'] = 'Last';
    $config['last_tag_open'] = '<li class="next page">';
    $config['last_tag_close'] = '</li>';
    $config['next_link'] = 'Next';
    $config['next_tag_open'] = '<li class="next page">';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_open'] = '<li class="prev page">';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a>';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li class="page">';
    $config['num_tag_close'] = '</li>';
    $config['anchor_class'] = 'follow_link';

    $this->pagination->initialize($config);
    $paginator = $this->pagination->create_links();
    $data['pagination'] = $paginator;

    $filter["limit"]  = $limit;
    $filter["page"] = $page;
    $filter["offset"] = $offset;

    $data["result"] =  $this->drivers->loadListAbsensi($filter);
    $data["row"] = $offset;
    echo json_encode($data);
  }

  public function loadAbsensi($page = 0)

  {
    $perpage = 70;

    $data = array();
    $filter = array();
    $data['search'] = array();

    $limit  = $perpage;
    $offset = $page > 1 ? ($limit * $page) - $limit : 0;

    $all_data =  $this->drivers->loadAbsensi($filter);
    $data['total'] = count($all_data);
    $config['base_url'] = base_url() . '/driver/loadListAbsensi';
    $config['use_page_numbers'] = TRUE;
    $config['total_rows'] = $data['total'];
    $config['per_page'] = $perpage;
    $config['num_links'] = 1;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['first_link'] = 'First';
    $config['first_tag_open'] = '<li class="prev page">';
    $config['first_tag_close'] = '</li>';
    $config['last_link'] = 'Last';
    $config['last_tag_open'] = '<li class="next page">';
    $config['last_tag_close'] = '</li>';
    $config['next_link'] = 'Next';
    $config['next_tag_open'] = '<li class="next page">';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_open'] = '<li class="prev page">';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a>';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li class="page">';
    $config['num_tag_close'] = '</li>';
    $config['anchor_class'] = 'follow_link';

    $this->pagination->initialize($config);
    $paginator = $this->pagination->create_links();
    $data['pagination'] = $paginator;

    $filter["limit"]  = $limit;
    $filter["page"] = $page;
    $filter["offset"] = $offset;

    $data["result"] =  $this->drivers->loadAbsensi($filter);
    $data["row"] = $offset;
    echo json_encode($data);
  }

  public function komisi()
  {
    $data['list'] = $this->drivers->getKomisis();
    $this->template->load('template', 'pages/komisi', $data);
  }

  public function saveEdit()
  {
    $this->drivers->editDriver();
    redirect('driver?msg=Save Success');
  }

  public function modaledit()
  {
    $data['drivers'] = $this->drivers->getOneOrder();


    $this->load->view('pages/modalDrivers', $data);
  }

  public function saveDrivers()
  {
    $this->drivers->Simpan();
    redirect('driver?msg=Save Success');
  }



  public function deleteDrivers()
  {
    $this->drivers->delete();
    redirect('driver?msg=Delete Success');
  }

  public function deleteGaji()
  {
    $id = $_GET['id'];

    $data['active']     = 0;

    $this->db->where('code', $id);
    $this->db->update('komisi_supir', $data);
  }


  public function driversummary($page = 0)
  {
    if ($this->input->get('export') == 'excel') {

      $this->load->library('PHPExcel');
      $data['search'] = array();
      $rows = $this->drivers->driversummary($data);

      $objPHPExcel = new PHPExcel();
      $objPHPExcel->getProperties()
        ->setCreator('Total Kilat')
        ->setTitle('Total Kilat')
        ->setLastModifiedBy('Total Kilat')
        ->setDescription('Total Kilat - GPS and System Solution')
        ->setSubject('Total Kilat')
        ->setKeywords('Total Kilat')
        ->setCategory('Total Kilat');

      $ews = $objPHPExcel->getSheet(0);
      $ews->setTitle('Data');

      $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

      $objPHPExcel->setActiveSheetIndex(0);
      $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Driver Summary');
      $objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
      $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setARGB('ffffcc');
      $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setSize(16);

      $filtertext = "";
      if ($this->input->get('fleetplateno') != '') {
        $filtertext .= "Plateno = " . $this->input->get('fleetplateno');
      }
      if ($this->input->get('type') != '') {
        $filtertext .= " Type = " . $this->input->get('type');
      }
      if ($this->input->get('customer') != '') {
        $filtertext .= " Customer = " . $this->input->get('customer');
      }
      if ($this->input->get('code') != '') {
        $filtertext .= " Fleet Code = " . $this->input->get('code');
      }
      if ($this->input->get('year') != '') {
        $filtertext .= " Year = " . $this->input->get('year');
      }
      if ($this->input->get('month') != '') {
        $filtertext .= " Month = " . date('F', strtotime("2018-" . $this->input->get('month') . "-01"));
      }
      $filters = ($filtertext != "" ? $filtertext : "All Data");

      $objPHPExcel->getActiveSheet()->mergeCells('A2:I2');
      $objPHPExcel->getActiveSheet()->getStyle("A2:I2")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getFill()->getStartColor()->setARGB('eaeaea');
      $objPHPExcel->getActiveSheet()->getStyle("A2:I2")->getFont()->setSize(14);
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Filter : " . $filters);

      $objPHPExcel->getActiveSheet()->getStyle("A3:I3")->getFont()->setBold(false);
      $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->getStartColor()->setARGB('cce6ff');
      $objPHPExcel->getActiveSheet()->getStyle("A3:I3")->getFont()->setSize(12);

      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'NO');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, 'DRIVER');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, 'CUSTOMER');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'FLEET TYPE');

      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3, 'RITASE');
      if ($rows != null) {
        $prices = 0;
        $ritase = 0;
        $allowance = 0;
        $allowanceadds = 0;
        $allowancereds = 0;
        $no  = 1;
        $baris = 4;
        foreach ($rows as $row) {
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->drivernama);
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->namac);
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->fleettype_name);

          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->ritase);

          $ritase += $row->ritase;
          $no++;
          $baris++;
        }

        $barisfooter = $baris;
        $barisbaru = $barisfooter - 1;
        $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $barisfooter, $ritase);
      } else {
        $objPHPExcel->getActiveSheet()->setCellValue('A4:I4', 'Data Not Found');
        $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('e6f2ff');
        $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(14);
      }




      $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');


      header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
      header("Cache-Control: no-store, no-cache, must-revalidate");
      header("Cache-Control: post-check=0, pre-check=0", false);
      header("Pragma: no-cache");
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="TruckOrderSummary' . date('dmy') . '.xlsx"');
      $writer->save('php://output');
    } else {
      $perpage = 50;

      $data = array();
      $filter = array();
      $data['search'] = array();

      $limit  = $perpage;
      $offset = $page > 1 ? ($limit * $page) - $limit : 0;

      $all_data = $this->drivers->driversummary($filter);
      $data['total'] = count($all_data);
      $config['base_url'] = base_url() . '/fleet/driversummary';
      $config['use_page_numbers'] = TRUE;
      $config['total_rows'] = $data['total'];
      $config['per_page'] = $perpage;
      $config['num_links'] = 1;
      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['first_link'] = 'First';
      $config['first_tag_open'] = '<li class="prev page">';
      $config['first_tag_close'] = '</li>';
      $config['last_link'] = 'Last';
      $config['last_tag_open'] = '<li class="next page">';
      $config['last_tag_close'] = '</li>';
      $config['next_link'] = 'Next';
      $config['next_tag_open'] = '<li class="next page">';
      $config['next_tag_close'] = '</li>';
      $config['prev_link'] = 'Prev';
      $config['prev_tag_open'] = '<li class="prev page">';
      $config['prev_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a>';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li class="page">';
      $config['num_tag_close'] = '</li>';
      $config['anchor_class'] = 'follow_link';

      $this->pagination->initialize($config);
      $paginator = $this->pagination->create_links();
      $data['pagination'] = $paginator;

      $filter["limit"]  = $limit;
      $filter["page"] = $page;
      $filter["offset"] = $offset;

      $data["result"] = $this->drivers->driversummary($filter);
      $data["row"] = $offset;
      echo json_encode($data);
    }
  }


  public function modalGaji()
  {
    if ($this->input->post('sett') == 'edit') {
      $code = $_POST['code'];
      $id = $_POST['id'];
      $this->db->select("*");
      $this->db->where("code", $code);
      $result = $this->db->get("komisi_supir");

      foreach ($result->result_array() as $key) {
        $start = $key['date_start'];
        $end = $key['date_end'];


        $sql = "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,
        e.type,
        e.unitprice,
        e.frameno,
        e.noshippent,
        o.prices,
        b.name AS n1,
        c.name AS c2,
        d.name AS cname,
        g.fleetplateno AS fleet,
        o.komSupir,
        h.name AS driver
        FROM
        orders o
        LEFT JOIN orderload e ON o.code = e.order_code
        LEFT JOIN cost ccc on o.code = ccc.order_id
        LEFT JOIN cities b ON e.id_citieso = b.id
        LEFT JOIN cities c ON e.id_citiesd = c.id
        LEFT JOIN customers d ON o.id_customers = d.id
        LEFT JOIN fleets g ON o.id_fleets = g.id
        LEFT JOIN drivers h ON o.id_drivers = h.id 
        WHERE
        o.active = 1 
        AND ccc.coa = 141        
        AND h.id = '$id' 
        AND o.[date] between '$start' and '$end'
        GROUP BY
        o.id,
        o.noVoucher,
        o.code,
        o.orderdate,
        o.ritase,
        e.id,
        e.type,
        e.noshippent,
        e.unitprice,
        e.frameno,
        o.komSupir,
        o.prices,
        e.nosummary,
        g.fleetplateno,
        g.CODE,
        ccc.nominal,
        g.fleetnumber,
        d.NAME,
        b.name,
        c.name,
        h.name
        ORDER BY
        o.orderdate ASC";

        $result = $this->db->query($sql);
        $data['detail'] = $result->result();
        $data['driver'] = $this->drivers->getDrivers();
        $data['kasbon'] = $this->drivers->getKasbon();

        $data['id'] = $key['driver_id'];
        $data['code'] = $key['code'];
        $data['start'] = $key['date_start'];
        $data['end'] = $key['date_end'];
        $data['komisi'] = $key['komisi'];
        $data['bonus_hadir'] = $key['bonus_hadir'];
        $data['bonus_ritase'] = $key['bonus_ritase'];
        $data['kasbons'] = $key['potongan_kasbon'];
        $data['izin'] = $key['potongan_izin'];


        $this->load->view('pages/modalGaji2', $data);
      }
    } else {
      $id = $_POST['id'];
      $start = $_POST['start'];
      $end = $_POST['end'];


      $sql = "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,
        e.type,
        e.unitprice,
        e.frameno,
        e.noshippent,
        o.prices,
        b.name AS n1,
        c.name AS c2,
        d.name AS cname,
        g.fleetplateno AS fleet,
        o.komSupir as nominal,
        h.name AS driver
        FROM
        orders o
        LEFT JOIN orderload e ON o.code = e.order_code
        LEFT JOIN cost ccc on o.code = ccc.order_id
        LEFT JOIN cities b ON e.id_citieso = b.id
        LEFT JOIN cities c ON e.id_citiesd = c.id
        LEFT JOIN customers d ON o.id_customers = d.id
        LEFT JOIN fleets g ON o.id_fleets = g.id
        LEFT JOIN drivers h ON o.id_drivers = h.id 
        WHERE
        o.active = 1 
        AND ccc.coa = 141        
        AND h.id = '$id' 
        AND o.[date] between '$start' and '$end'
        GROUP BY
        o.id,
        o.noVoucher,
        o.code,
        o.orderdate,
        o.ritase,
        e.id,
        e.type,
        e.noshippent,
        e.unitprice,
        e.frameno,
        o.komSupir,
        o.prices,
        e.nosummary,
        g.fleetplateno,
        g.CODE,
        ccc.nominal,
        g.fleetnumber,
        d.NAME,
        b.name,
        c.name,
        h.name
        ORDER BY
        o.orderdate ASC";

      $result = $this->db->query($sql);
      $data['detail'] = $result->result();
      $data['driver'] = $this->drivers->getDrivers();
      $data['kasbon'] = $this->drivers->getKasbon();

      $data['id'] = $_POST['id'];
      $data['start'] = $_POST['start'];
      $data['end'] = $_POST['end'];


      $this->load->view('pages/modalGaji', $data);
    }
  }

  public function modalGajiver2()
  {
    if ($this->input->post('sett') == 'edit') {
      $this->db->select("*");
      $this->db->like("code", $this->input->post("id"));
      $result = $this->db->get("komisi_supir");

      foreach ($result->result_array() as $key) {
        $id = $key['driver_id'];
        $start = $key['date_start'];
        $end = $key['date_end'];


        $sql = "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,
        e.type,
        e.unitprice,
        e.frameno,
        e.noshippent,
        o.prices,
        b.name AS n1,
        c.name AS c2,
        d.name AS cname,
        g.fleetplateno AS fleet,
        o.komSupir,
        h.name AS driver,
        ccc.nominal
        FROM
        orders o
        LEFT JOIN orderload e ON o.code = e.order_code
        LEFT JOIN cost ccc on o.code = ccc.order_id
        LEFT JOIN cities b ON e.id_citieso = b.id
        LEFT JOIN cities c ON e.id_citiesd = c.id
        LEFT JOIN customers d ON o.id_customers = d.id
        LEFT JOIN fleets g ON o.id_fleets = g.id
        LEFT JOIN drivers h ON o.id_drivers = h.id 
        WHERE
        o.active = 1 
        AND ccc.coa = 141        
        AND h.id = '$id' 
        AND o.[date] between '$start' and '$end'
        GROUP BY
        o.id,
        o.noVoucher,
        o.code,
        o.orderdate,
        o.ritase,
        e.id,
        e.type,
        e.noshippent,
        e.unitprice,
        e.frameno,
        o.komSupir,
        o.prices,
        e.nosummary,
        g.fleetplateno,
        g.CODE,
        ccc.nominal,
        g.fleetnumber,
        d.NAME,
        b.name,
        c.name,
        h.name
        ORDER BY
        o.orderdate ASC";

        $result = $this->db->query($sql);
        $data['detail'] = $result->result();
        $data['driver'] = $this->drivers->getDrivers();
        $data['kasbon'] = $this->drivers->getKasbon();

        $data['id'] = $key['driver_id'];
        $data['code'] = $key['code'];
        $data['start'] = $key['date_start'];
        $data['end'] = $key['date_end'];
        $data['komisi'] = $key['komisi'];
        $data['bonus_hadir'] = $key['bonus_hadir'];
        $data['bonus_ritase'] = $key['bonus_ritase'];
        $data['kasbons'] = $key['potongan_kasbon'];
        $data['izin'] = $key['potongan_izin'];


        $this->load->view('pages/modalGaji2', $data);
      }
    } else {
      $id = $_POST['id'];
      $start = $_POST['start'];
      $end = $_POST['end'];


      $sql = "SELECT a.tanggal_absensi, d.fleetplateno AS platno, e.name as tujuan, b.komSupir,a.tipe_absensi
      FROM absensi a
      LEFT JOIN orders b ON a.id_drivers = b.id_drivers AND a.tanggal_absensi = b.[date] AND b.active =1
      LEFT JOIN orderload c ON b.code = c.order_code
      LEFT JOIN fleets d ON d.id = b.id_fleets
      LEFT JOIN cities e ON e.id = b.id_citiesd
      WHERE a.id_drivers = '$id'
      AND a.tanggal_absensi between '$start' and '$end'
      ORDER BY
        a.tanggal_absensi ASC";

      $result = $this->db->query($sql);
      $data['detail'] = $result->result();
      $data['driver'] = $this->drivers->getDrivers();
      $data['kasbon'] = $this->drivers->getKasbon();

      $data['id'] = $_POST['id'];
      $data['start'] = $_POST['start'];
      $data['end'] = $_POST['end'];


      $this->load->view('pages/modalGajiver2', $data);
    }
  }

  public function gajiDriver()
  {
    $code       = $this->input->post('code');
    $id         = $this->input->post('id');
    $start      = $this->input->post('start');
    $end        = $this->input->post('end');
    $supirnama  = $this->input->post('supir');

    $bonus_h  = str_replace(",", "", $this->input->post('bonus_hadir')) > 0 ? str_replace(",", "", $this->input->post('bonus_hadir')) : 0;
    $bonus_r  = str_replace(",", "", $this->input->post('bonus_ritase')) > 0 ? str_replace(",", "", $this->input->post('bonus_ritase')) : 0;
    $komisi   = str_replace(",", "", $this->input->post('totalkomisi'));
    $kasbon   = str_replace(",", "", $this->input->post('kasbon')) > 0 ? str_replace(",", "", $this->input->post('kasbon')) : 0;
    $poti     = str_replace(",", "", $this->input->post('potongan_izin')) > 0 ? str_replace(",", "", $this->input->post('potongan_izin')) : 0;

    $hasil      = ($komisi + $bonus_r + $bonus_h) - ($kasbon + $poti);
    $hasilbonus = $bonus_r + $bonus_h;

    $datas['driver_id'] = $this->input->post('id');
    $datas['code'] = $this->input->post('code');
    $datas['date_start'] = $this->input->post('start');
    $datas['date_end'] = $this->input->post('end');
    $datas['bonus_hadir'] = str_replace(",", "", $this->input->post('bonus_hadir')) > 0 ? str_replace(",", "", $this->input->post('bonus_hadir')) : 0;
    $datas['bonus_ritase'] = str_replace(",", "", $this->input->post('bonus_ritase')) > 0 ? str_replace(",", "", $this->input->post('bonus_ritase')) : 0;
    $datas['potongan_izin'] = str_replace(",", "", $this->input->post('potongan_izin')) > 0 ? str_replace(",", "", $this->input->post('potongan_izin')) : 0;
    $datas['komisi'] = $komisi;
    $datas['potongan_kasbon'] = str_replace(",", "", $this->input->post('kasbon')) > 0 ? str_replace(",", "", $this->input->post('kasbon')) : 0;

    $this->db->insert('komisi_supir', $datas);

    $c = array(
      'order_id' => $this->input->post('code'),
      'active' => 1,
      'akun_id' => 99,
      'date'    => date('Y/m/d'),
      'nominal' => $hasil,
      'coa' => 141,
      'cost_type' => 1,
      'createby' => $this->session->userdata('id')
    );
    $this->db->insert('cost', $c);

    $KB = array(
      'description' => "Pembayaran Kasbon - " . $this->input->post('code'),
      'order_id' => "Pembayaran Kasbon - " . $this->input->post('code'),
      'active' => 1,
      'akun_id' => 1,
      'date'    => date('Y/m/d'),
      'nominal' => $kasbon,
      'coa' => 4,
      'cost_type' => 2,
      'createby' => $this->session->userdata('id')
    );
    $this->db->insert('cost', $KB);

    $dp = array(
      'id_drivers' => $this->input->post('id'),
      'dates' => date('Y-m-d'),
      'description' => $this->input->post('code'),
      'type'    => 2,
      'nominal' => $kasbon,
      'create_by' => $this->session->userdata('id')
    );
    $this->db->insert('downpaymentouts', $dp);

    $d = array(
      'order_id' => $this->input->post('code'),
      'active' => 1,
      'akun_id' => 99,
      'date'    => date('Y/m/d'),
      'nominal' => $bonus_h,
      'coa' => 146,
      'cost_type' => 1,
      'createby' => $this->session->userdata('id')
    );
    $this->db->insert('cost', $d);

    $e = array(
      'order_id' => $this->input->post('code'),
      'active' => 1,
      'akun_id' => 99,
      'date'    => date('Y/m/d'),
      'nominal' => $bonus_r,
      'coa' => 147,
      'cost_type' => 1,
      'createby' => $this->session->userdata('id')
    );
    $this->db->insert('cost', $e);

    $this->load->library('M_pdf');

    $mpdf = $this->m_pdf->load([
      'mode' => 'utf-8',
      'format' => 'Legal'
    ]);

    //$this->drivers->simpanGaji();

    $sql = "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,
    e.type,
    e.unitprice,
    e.frameno,
    e.noshippent,
    o.prices,
    b.name AS n1,
    c.name AS c2,
    d.name AS cname,
    g.fleetplateno AS fleet,
    o.komSupir,
    h.name AS driver,
    ccc.nominal
    FROM
    orders o
    LEFT JOIN orderload e ON o.code = e.order_code
    LEFT JOIN cost ccc on o.code = ccc.order_id
    LEFT JOIN cities b ON e.id_citieso = b.id
    LEFT JOIN cities c ON e.id_citiesd = c.id
    LEFT JOIN customers d ON o.id_customers = d.id
    LEFT JOIN fleets g ON o.id_fleets = g.id
    LEFT JOIN drivers h ON o.id_drivers = h.id 
    WHERE
    o.active = 1 
    AND ccc.coa = 141        
    AND h.id = '$id' 
    AND o.[date] between '$start' and '$end'
    GROUP BY
    o.id,
    o.noVoucher,
    o.code,
    o.orderdate,
    o.ritase,
    e.id,
    e.type,
    e.noshippent,
    e.unitprice,
    e.frameno,
    o.komSupir,
    o.prices,
    e.nosummary,
    g.fleetplateno,
    g.CODE,
    ccc.nominal,
    g.fleetnumber,
    d.NAME,
    b.name,
    c.name,
    h.name
    ORDER BY
    o.orderdate ASC";

    $result = $this->db->query($sql);
    $data['detail'] = $result->result_array();
    $data['code'] = $this->input->post('code');
    $data['kasbon'] = $this->input->post('kasbon') > 0 ? $this->input->post('kasbon') : 0;
    $data['bonus_r'] = $this->input->post('bonus_ritase') > 0 ? $this->input->post('bonus_ritase') : 0;
    $data['bonus_h'] = $this->input->post('bonus_hadir') > 0 ? $this->input->post('bonus_hadir') : 0;
    $data['poti'] = $this->input->post('potongan_izin') > 0 ? $this->input->post('potongan_izin') : 0;
    $data['komisi'] = $hasil;


    $view = $this->load->view('pages/printGaji', $data, true);

    $mpdf->WriteHTML($view);
    $file_name = "SLIP GAJI " . $supirnama . ".pdf";
    $mpdf->Output($file_name, 'D');

    //asasa$mpdf->Output();
  }

  public function print()
  {
    $type = $_POST['sett'];
    $code = $_POST['id'];

    $this->db->select("*");
    $this->db->like("code", $code);
    $result = $this->db->get("komisi_supir");

    foreach ($result->result_array() as $key) {
      $kasbon = $key['potongan_kasbon'];
      $hadir = $key['bonus_hadir'];
      $ritase = $key['bonus_ritase'];
      $code = $key['code'];
      $izin = $key['potongan_izin'];
      $start = $key['date_start'];
      $end = $key['date_end'];
      $komisi = $key['komisi'];
      $id = $key['driver_id'];
    }
    $this->load->library('M_pdf');

    $mpdf = $this->m_pdf->load([
      'mode' => 'utf-8',
      'format' => 'Legal'
    ]);


    $sql = "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,
    e.type,
    e.unitprice,
    e.frameno,
    e.noshippent,
    o.prices,
    b.name AS n1,
    c.name AS c2,
    d.name AS cname,
    g.fleetplateno AS fleet,
    o.komSupir,
    h.name AS driver,
    ccc.nominal
    FROM
    orders o
    LEFT JOIN orderload e ON o.code = e.order_code
    LEFT JOIN cost ccc on o.code = ccc.order_id
    LEFT JOIN cities b ON e.id_citieso = b.id
    LEFT JOIN cities c ON e.id_citiesd = c.id
    LEFT JOIN customers d ON o.id_customers = d.id
    LEFT JOIN fleets g ON o.id_fleets = g.id
    LEFT JOIN drivers h ON o.id_drivers = h.id 
    WHERE
    o.active = 1 
    AND ccc.coa = 141        
    AND h.id = '$id' 
    AND o.[date] between '$start' and '$end'
    GROUP BY
    o.id,
    o.noVoucher,
    o.code,
    o.orderdate,
    o.ritase,
    e.id,
    e.type,
    e.noshippent,
    e.unitprice,
    e.frameno,
    o.komSupir,
    o.prices,
    e.nosummary,
    g.fleetplateno,
    g.CODE,
    ccc.nominal,
    g.fleetnumber,
    d.NAME,
    b.name,
    c.name,
    h.name
    ORDER BY
    o.orderdate ASC";

    $result = $this->db->query($sql);
    $data['detail'] = $result->result_array();
    $data['code'] = $_POST['id'];
    $data['kasbon'] = $kasbon;
    $data['bonus_r'] = $ritase;
    $data['bonus_h'] = $hadir;
    $data['poti'] = $izin;
    $data['komisi'] = $komisi;


    $view = $this->load->view('pages/printGaji', $data, true);

    $mpdf->WriteHTML($view);
    $file_name = "SLIP GAJI .pdf";
    $mpdf->Output($file_name, 'D');
  }

  public function gajiDriverKoms()
  {
    $code = $this->input->get('code');
    $id = $this->input->get('id');
    $start = $this->input->get('start');
    $end = $this->input->get('end');
    $supirnama = $this->input->get('supir');
    $bonus_h = str_replace(",", "", $this->input->get('bonus_hadir')) > 0 ? str_replace(",", "", $this->input->get('bonus_hadir')) : 0;
    $bonus_r = str_replace(",", "", $this->input->get('bonus_ritase')) > 0 ? str_replace(",", "", $this->input->get('bonus_ritase')) : 0;
    $komisi = str_replace(",", "", $this->input->get('totalkomisi'));
    $kasbon = str_replace(",", "", $this->input->get('kasbon')) > 0 ? str_replace(",", "", $this->input->get('kasbon')) : 0;
    $poti = str_replace(",", "", $this->input->get('potongan_izin')) > 0 ? str_replace(",", "", $this->input->get('potongan_izin')) : 0;

    $hasil = ($komisi + $bonus_r + $bonus_h) - ($kasbon + $poti);
    $hasilbonus = $bonus_r + $bonus_h;

    $datas['driver_id'] = $this->input->get('id');
    $datas['code'] = $this->input->get('code');
    $datas['date_start'] = $this->input->get('start');
    $datas['date_end'] = $this->input->get('end');
    $datas['bonus_hadir'] = str_replace(",", "", $this->input->get('bonus_hadir')) > 0 ? str_replace(",", "", $this->input->get('bonus_hadir')) : 0;
    $datas['bonus_ritase'] = str_replace(",", "", $this->input->get('bonus_ritase')) > 0 ? str_replace(",", "", $this->input->get('bonus_ritase')) : 0;
    $datas['potongan_izin'] = str_replace(",", "", $this->input->get('potongan_izin')) > 0 ? str_replace(",", "", $this->input->get('potongan_izin')) : 0;
    $datas['komisi'] = $komisi;
    $datas['potongan_kasbon'] = str_replace(",", "", $this->input->get('kasbon')) > 0 ? str_replace(",", "", $this->input->get('kasbon')) : 0;

    $this->db->where('code', $code);
    $this->db->update('komisi_supir', $datas);

    $c = array(
      'active' => 1,
      'akun_id' => 99,
      'date'    => date('Y/m/d'),
      'nominal' => $hasil,
      'cost_type' => 1,
      'createby' => $this->session->userdata('id')
    );

    $this->db->where('order_id', $code);
    $this->db->where('coa', '141');
    $this->db->update('cost', $c);

    $d = array(
      'active' => 1,
      'akun_id' => 99,
      'date'    => date('Y/m/d'),
      'nominal' => $bonus_h,
      'cost_type' => 1,
      'createby' => $this->session->userdata('id')
    );
    $this->db->where('order_id', $code);
    $this->db->where('coa', '146');
    $this->db->update('cost', $d);

    $KB = array(
      'active' => 1,
      'akun_id' => 1,
      'date'    => date('Y/m/d'),
      'nominal' => $kasbon,
      'cost_type' => 2,
      'createby' => $this->session->userdata('id')
    );
    $this->db->where('order_id', "Pembayaran Kasbon - " . $code);
    $this->db->where('coa', '146');
    $this->db->update('cost', $KB);

    $dp = array(
      'id_drivers' => $this->input->get('id'),
      'dates' => date('Y-m-d'),
      'type'    => 2,
      'nominal' => $kasbon,
      'create_by' => $this->session->userdata('id')
    );
    $this->db->where('description', $code);
    $this->db->update('downpaymentouts', $dp);



    $e = array(
      'active' => 1,
      'akun_id' => 99,
      'date'    => date('Y/m/d'),
      'nominal' => $bonus_r,
      'cost_type' => 1,
      'createby' => $this->session->userdata('id')
    );
    $this->db->where('order_id', $code);
    $this->db->where('coa', '147');
    $this->db->update('cost', $e);

    $this->load->library('M_pdf');

    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4', 'margin_left' => 20, 'margin_right' => 20, 'margin_top' => 15, 'margin_bottom' => 15, 'margin_header' => 9, 'margin_footer' => 9, 'orientation' => 'P']);

    //$this->drivers->simpanGaji();

    $sql = "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,
    e.type,
    e.unitprice,
    e.frameno,
    e.noshippent,
    o.prices,
    b.name AS n1,
    c.name AS c2,
    d.name AS cname,
    g.fleetplateno AS fleet,
    o.komSupir,
    h.name AS driver,
    ccc.nominal
    FROM
    orders o
    LEFT JOIN orderload e ON o.code = e.order_code
    LEFT JOIN cost ccc on o.code = ccc.order_id
    LEFT JOIN cities b ON e.id_citieso = b.id
    LEFT JOIN cities c ON e.id_citiesd = c.id
    LEFT JOIN customers d ON o.id_customers = d.id
    LEFT JOIN fleets g ON o.id_fleets = g.id
    LEFT JOIN drivers h ON o.id_drivers = h.id 
    WHERE
    o.active = 1 
    AND ccc.coa = 141        
    AND h.id = '$id' 
    AND o.[date] between '$start' and '$end'
    GROUP BY
    o.id,
    o.noVoucher,
    o.code,
    o.orderdate,
    o.ritase,
    e.id,
    e.type,
    e.noshippent,
    e.unitprice,
    e.frameno,
    o.komSupir,
    o.prices,
    e.nosummary,
    g.fleetplateno,
    g.CODE,
    ccc.nominal,
    g.fleetnumber,
    d.NAME,
    b.name,
    c.name,
    h.name
    ORDER BY
    o.orderdate ASC";

    $result = $this->db->query($sql);
    $data['detail'] = $result->result_array();
    $data['code'] = $this->input->get('code');
    $data['kasbon'] = $this->input->get('kasbon') > 0 ? $this->input->get('kasbon') : 0;
    $data['bonus_r'] = $this->input->get('bonus_ritase') > 0 ? $this->input->get('bonus_ritase') : 0;
    $data['bonus_h'] = $this->input->get('bonus_hadir') > 0 ? $this->input->get('bonus_hadir') : 0;
    $data['poti'] = $this->input->get('potongan_izin') > 0 ? $this->input->get('potongan_izin') : 0;
    $data['komisi'] = $hasil;


    $view = $this->load->view('pages/printGaji', $data, true);
    $mpdf->WriteHTML($view);
    $file_name = "SLIP GAJI " . $supirnama . ".pdf";
    $mpdf->Output();

    //$mpdf->Output();
  }

  public function printKomisi()
  {
    $id = $this->input->get('id');
    $this->load->library('M_pdf');

    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4', 'margin_left' => 20, 'margin_right' => 20, 'margin_top' => 15, 'margin_bottom' => 15, 'margin_header' => 9, 'margin_footer' => 9, 'orientation' => 'P']);

    $this->db->select("*");
    $this->db->like("code", "$id");
    $result = $this->db->get("komisi_supir");

    foreach ($result->result_array() as $key) {
      $id = $key['driver_id'];
      $start = $key['date_start'];
      $end = $key['date_end'];


      $sql = "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,
        e.type,
        e.unitprice,
        e.frameno,
        e.noshippent,
        o.prices,
        b.name AS n1,
        c.name AS c2,
        d.name AS cname,
        g.fleetplateno AS fleet,
        o.komSupir,
        h.name AS driver,
        ccc.nominal
        FROM
        orders o
        LEFT JOIN orderload e ON o.code = e.order_code
        LEFT JOIN cost ccc on o.code = ccc.order_id
        LEFT JOIN cities b ON e.id_citieso = b.id
        LEFT JOIN cities c ON e.id_citiesd = c.id
        LEFT JOIN customers d ON o.id_customers = d.id
        LEFT JOIN fleets g ON o.id_fleets = g.id
        LEFT JOIN drivers h ON o.id_drivers = h.id 
        WHERE
        o.active = 1 
        AND ccc.coa = 141        
        AND h.id = '$id' 
        AND o.[date] between '$start' and '$end'
        GROUP BY
        o.id,
        o.noVoucher,
        o.code,
        o.orderdate,
        o.ritase,
        e.id,
        e.type,
        e.noshippent,
        e.unitprice,
        e.frameno,
        o.komSupir,
        o.prices,
        e.nosummary,
        g.fleetplateno,
        g.CODE,
        ccc.nominal,
        g.fleetnumber,
        d.NAME,
        b.name,
        c.name,
        h.name
        ORDER BY
        o.orderdate ASC";

      $result = $this->db->query($sql);
      $data['detail'] = $result->result_array();
      //$data['driver'] = $this->drivers->getDrivers();
      //$data['kasbon'] = $this->drivers->getKasbon();

      $data['id'] = $key['driver_id'];
      $data['code'] = $key['code'];
      $data['start'] = $key['date_start'];
      $data['end'] = $key['date_end'];
      $data['komisi'] = $key['komisi'];
      $data['bonus_h'] = $key['bonus_hadir'];
      $data['bonus_r'] = $key['bonus_ritase'];
      $data['kasbon'] = $key['potongan_kasbon'];
      $data['poti'] = $key['potongan_izin'];

      //print_r($data);
      $view = $this->load->view('pages/printGaji', $data, true);
      $mpdf->WriteHTML($view);
      $file_name = "SLIP GAJI " . $supirnama . ".pdf";
      $mpdf->Output();
    }
  }

  public function absensi()
  {
    date_default_timezone_set("Asia/Bangkok");
    $id_drivers = $this->input->post('idcus');
    $waktu      = $this->input->post('date');
    $type       = $this->input->post('type');

    $c = array(
      'id_drivers' => $id_drivers,
      'waktu_absensi' => $waktu,
      'tanggal_absensi' => $waktu,
      'tipe_absensi' => $type
    );
    $this->db->insert('absensi', $c);
  }

  public function update_absensi()
  {
    date_default_timezone_set("Asia/Bangkok");
    $id = $this->input->post('idcus');
    $waktu      = $this->input->post('date');
    $type       = $this->input->post('type');

    $c = array(
      'waktu_absensi' => $waktu,
      'tanggal_absensi' => $waktu,
      'tipe_absensi' => $type
    );
    $this->db->where('id', $id);
    $this->db->update('absensi', $c);
  }
}
