<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
class employee extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('employees');

	}

	public function index()
	{
		$data['employee']=$this->employees->getEmployees();
		$data['id']=$this->employees->getLastId();

		$this->template->load('template','pages/employee',$data);
	}

	public function modaledit()
	{
		$data['employee']=$this->employees->getOneEmployees();

		$this->load->view('pages/modalemployee',$data);
	}
	public function saveEmployee()
	{
		$this->employees->saveEmployees();
		redirect('employee?msg=Save Success');
	}

	public function saveEdit()
	{
		$this->employees->saveEditEmployees();
		redirect('employee?msg=Save Success');
	}

	public function deleteEmployee()
	{
		$this->employees->Delete();
		echo "Delete Success !";
	}


}
?>