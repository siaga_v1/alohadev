<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excelexport extends CI_Controller {
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    
    function truckmonth($data = null)
    {
        if($data != null)
        {
            if($data == 'truckritase')
            {
                $data['tahun'] = date('Y');
                $this->load->library('PHPExcel');
                $this->load->model('Reports');
                $rows = $this->Reports->reporttruckmonths($data);
                
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
                // Assign cell values
                $objPHPExcel->setActiveSheetIndex(0);
                $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Table Ritase '.date('F Y'));
                $objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
                $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('ffffcc');
                $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->getFont()->setSize(16);
                
                $objPHPExcel->getActiveSheet()->getStyle("A3:C3")->getFont()->setBold(false);
                $objPHPExcel->getActiveSheet()->getStyle('A3:C3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A3:C3')->getFill()->getStartColor()->setARGB('cce6ff');
                $objPHPExcel->getActiveSheet()->getStyle("A3:C3")->getFont()->setSize(12);
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3,'RANK');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3,'PLATENO');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3,'RITASE');
                
                if($rows != null)
                {
                    $ritase = 0;
                    $total30 = 0;
                    $total5 = 0;
                    $total59 = 0;
                    $total1019 = 0;
                    $total2029 = 0;
                    
                    $no  = 1;
                    $baris = 4;
                    foreach($rows AS $row)
                    {   
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->fleetplateno);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->ritase);
    
                        $ritase += $row->ritase;
                        
                        if($row->ritase >= 30){
                        $total30 += $row->ritase;
                        }
                        
                        if($row->ritase >= 20 && $row->ritase <= 29){
                            $total2029 += $row->ritase;
                        }
                        
                        if($row->ritase >= 10 && $row->ritase <= 19){
                            $total1019 += $row->ritase;
                        }
                        
                        if($row->ritase >= 5 && $row->ritase <= 9){
                            $total59 += $row->ritase;
                        }
                        
                        if($row->ritase < 5){
                            $total5 += $row->ritase;
                        }
                        
                        $no++;
                        $baris++;
                    }
                    
                    $barisfooter = $baris;
                    $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:B$barisfooter");
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisfooter, 'Total');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $barisfooter, $ritase);
                    $totalritase = $ritase;
                    $nextfooter = ($baris+3);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter, 'Summary:');
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+1, 'Total Unit/Trip');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+1, ($no-1).' / '.$ritase);
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+2, 'AVG Trip');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+2, round($ritase/$no));
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+3, 'Trip >= 30');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+3, round($total30)." / ".(($total30 > 0) ? number_format(($total30/$totalritase)*100,2) : 0)." %");
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+4, 'Trip 20-29');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+4, round($total2029)." / ".(($total2029 > 0) ? number_format(($total2029/$totalritase)*100,2) : 0)." %");
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+5, 'Trip 10-19');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+5, round($total1019)." / ".(($total1019 > 0) ? number_format(($total1019/$totalritase)*100,2) : 0)." %");
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+6, 'Trip 5-9');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+6, round($total59)." / ".(($total59 > 0) ? number_format(($total59/$totalritase)*100,2) : 0)." %");
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+7, 'Trip < 5');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+7, round($total5)." / ".(($total5 > 0) ? number_format(($total5/$totalritase)*100,2) : 0)." %");
                    
                }
                else
                {
                    $objPHPExcel->getActiveSheet()->setCellValue('A4:C4', 'Data Not Found');
                    $objPHPExcel->getActiveSheet()->getStyle("A4:C4")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:C4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:C4')->getFill()->getStartColor()->setARGB('e6f2ff');
                    $objPHPExcel->getActiveSheet()->getStyle("A4:C4")->getFont()->setSize(14);
                }
                
                ob_clean();
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="TruckRitase'.date('FY').'.xlsx"');
                $objWriter->save('php://output');   
            }
            
            if($data == 'trucksales')
            {
                $data['tahun'] = date('Y');
                $this->load->library('PHPExcel');
                $this->load->model('Reports');
                $rows = $this->Reports->reporttruckmonths($data);
                
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
                // Assign cell values
                $objPHPExcel->setActiveSheetIndex(0);
                $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Table Sales '.date('F Y'));
                $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
                $objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFill()->getStartColor()->setARGB('ffffcc');
                $objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setSize(16);
                
                $objPHPExcel->getActiveSheet()->getStyle("A3:G3")->getFont()->setBold(false);
                $objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getFill()->getStartColor()->setARGB('cce6ff');
                $objPHPExcel->getActiveSheet()->getStyle("A3:G3")->getFont()->setSize(12);
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3,'RANK');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3,'PLATENO');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3,'RITASE');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3,'SALES');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3,'COSTS');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 3,'GROSS MARGIN');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 3,'GROSS MARGIN (%)');
                
                if($rows != null)
                {
                    $ritase = 0;
                    $prices = 0;
                    $costs = 0;
                    $achieves = 0;
                    
                    $total30 = 0;
                    $total5 = 0;
                    $total59 = 0;
                    $total1019 = 0;
                    $total2029 = 0;
                    
                    $no  = 1;
                    $baris = 4;
                    foreach($rows AS $row)
                    {   
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->fleetplateno);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->ritase);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->prices);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->costs);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris, $row->achieves);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $baris, ($row->prices > 0 && $row->achieves > 0) ? (($row->achieves/$row->prices)*100) : 0 );
    
                        $ritase += $row->ritase;
                        $prices += $row->prices;
                        $costs += $row->costs;
                        $achieves += $row->achieves;
                        
                        if($row->ritase >= 30){
                        $total30 += $row->ritase;
                        }
                        
                        if($row->ritase >= 20 && $row->ritase <= 29){
                            $total2029 += $row->ritase;
                        }
                        
                        if($row->ritase >= 10 && $row->ritase <= 19){
                            $total1019 += $row->ritase;
                        }
                        
                        if($row->ritase >= 5 && $row->ritase <= 9){
                            $total59 += $row->ritase;
                        }
                        
                        if($row->ritase < 5){
                            $total5 += $row->ritase;
                        }
                        
                        $no++;
                        $baris++;
                    }
                    
                    $barisfooter = $baris+1;
                    $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:B$barisfooter");
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisfooter, 'Total');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $barisfooter, $ritase);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $barisfooter, $prices);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $barisfooter, $costs);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $barisfooter, $achieves);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $barisfooter, ($prices > 0 && $achieves > 0) ? (($achieves/$prices)*100)  : 0);
                    
                    $totalritase = $ritase;
                    $nextfooter = ($baris+4);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter, 'Summary:');
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+1, 'Total Unit');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+1, 'Total Ritase');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $nextfooter+1, 'Total Sales');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $nextfooter+1, 'Total Costs');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $nextfooter+1, 'Total Gross Margin');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $nextfooter+1, 'Total GM (%)');
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+2, $no-1);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+2, $ritase);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $nextfooter+2, $prices);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $nextfooter+2, $costs);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $nextfooter+2, $achieves);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $nextfooter+2, ($prices > 0 && $achieves > 0) ? (($achieves/$prices)*100)  : 0);
                    
                }
                else
                {
                    $objPHPExcel->getActiveSheet()->setCellValue('A4:G4', 'Data Not Found');
                    $objPHPExcel->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:G4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:G4')->getFill()->getStartColor()->setARGB('e6f2ff');
                    $objPHPExcel->getActiveSheet()->getStyle("A4:G4")->getFont()->setSize(14);
                }
                
                ob_clean();
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="TruckSales'.date('FY').'.xlsx"');
                $objWriter->save('php://output');   
            }
        }
    }
    
    function customermonth($data = null)
    {
        if($data != null)
        {
            if($data == 'customerritase')
            {
                $data['tahun'] = date('Y');
                $this->load->library('PHPExcel');
                $this->load->model('Reports');
                $rows = $this->Reports->reportcustomermonths($data);
                
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
                // Assign cell values
                $objPHPExcel->setActiveSheetIndex(0);
                $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Table Ritase/ Customer '.date('F Y'));
                $objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
                $objPHPExcel->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->getStartColor()->setARGB('ffffcc');
                $objPHPExcel->getActiveSheet()->getStyle("A1:D1")->getFont()->setSize(16);
                
                $objPHPExcel->getActiveSheet()->getStyle("A3:D3")->getFont()->setBold(false);
                $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->getStartColor()->setARGB('cce6ff');
                $objPHPExcel->getActiveSheet()->getStyle("A3:D3")->getFont()->setSize(12);
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3,'RANK');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3,'CUSTOMER NICKNAME');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3,'CUSTOMER NAME');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3,'RITASE');
                
                if($rows != null)
                {
                    $ritase = 0;
                    $total30 = 0;
                    $total5 = 0;
                    $total59 = 0;
                    $total1019 = 0;
                    $total2029 = 0;
                    
                    $no  = 1;
                    $baris = 4;
                    foreach($rows AS $row)
                    {   
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->customers_nickname);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->customers_name);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->ritase);
    
                        $ritase += $row->ritase;
                        
                        if($row->ritase >= 30){
                        $total30 += $row->ritase;
                        }
                        
                        if($row->ritase >= 20 && $row->ritase <= 29){
                            $total2029 += $row->ritase;
                        }
                        
                        if($row->ritase >= 10 && $row->ritase <= 19){
                            $total1019 += $row->ritase;
                        }
                        
                        if($row->ritase >= 5 && $row->ritase <= 9){
                            $total59 += $row->ritase;
                        }
                        
                        if($row->ritase < 5){
                            $total5 += $row->ritase;
                        }
                        
                        $no++;
                        $baris++;
                    }
                    
                    $barisfooter = $baris;
                    $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisfooter, 'Total');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $barisfooter, $ritase);
                    $totalritase = $ritase;
                    $nextfooter = ($baris+3);
                    /*
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter, 'Summary:');
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+1, 'Total Unit/Trip');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+1, ($no-1).' / '.$ritase);
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+2, 'AVG Trip');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+2, round($ritase/$no));
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+3, 'Trip >= 30');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+3, round($total30)." / ".(($total30 > 0) ? number_format(($total30/$totalritase)*100,2) : 0)." %");
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+4, 'Trip 20-29');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+4, round($total2029)." / ".(($total2029 > 0) ? number_format(($total2029/$totalritase)*100,2) : 0)." %");
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+5, 'Trip 10-19');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+5, round($total1019)." / ".(($total1019 > 0) ? number_format(($total1019/$totalritase)*100,2) : 0)." %");
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+6, 'Trip 5-9');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+6, round($total59)." / ".(($total59 > 0) ? number_format(($total59/$totalritase)*100,2) : 0)." %");
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+7, 'Trip < 5');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+7, round($total5)." / ".(($total5 > 0) ? number_format(($total5/$totalritase)*100,2) : 0)." %");
                    */
                }
                else
                {
                    $objPHPExcel->getActiveSheet()->setCellValue('A4:D4', 'Data Not Found');
                    $objPHPExcel->getActiveSheet()->getStyle("A4:D4")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:D4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:D4')->getFill()->getStartColor()->setARGB('e6f2ff');
                    $objPHPExcel->getActiveSheet()->getStyle("A4:D4")->getFont()->setSize(14);
                }
                
                ob_clean();
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="CustomerRitase'.date('FY').'.xlsx"');
                $objWriter->save('php://output');   
            }
            
            if($data == 'customersales')
            {
                $data['tahun'] = date('Y');
                $this->load->library('PHPExcel');
                $this->load->model('Reports');
                $rows = $this->Reports->reportcustomermonths($data);
                
                
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
                // Assign cell values
                $objPHPExcel->setActiveSheetIndex(0);
                $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Table Sales/ Customer '.date('F Y'));
                $objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
                $objPHPExcel->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->getStartColor()->setARGB('ffffcc');
                $objPHPExcel->getActiveSheet()->getStyle("A1:D1")->getFont()->setSize(16);
                
                $objPHPExcel->getActiveSheet()->getStyle("A3:D3")->getFont()->setBold(false);
                $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->getStartColor()->setARGB('cce6ff');
                $objPHPExcel->getActiveSheet()->getStyle("A3:D3")->getFont()->setSize(12);
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3,'RANK');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3,'CUSTOMER NICKNAME');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3,'CUSTOMER NAME');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3,'RITASE');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3,'SALES');
                
                if($rows != null)
                {
                    $ritase = 0;
                    $prices = 0;
                    $total30 = 0;
                    $total5 = 0;
                    $total59 = 0;
                    $total1019 = 0;
                    $total2029 = 0;
                    
                    $no  = 1;
                    $baris = 4;
                    foreach($rows AS $row)
                    {   
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->customers_nickname);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->customers_name);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->ritase);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->prices);
    
                        $ritase += $row->ritase;
                        $prices += $row->prices;
                        
                        if($row->ritase >= 30){
                        $total30 += $row->ritase;
                        }
                        
                        if($row->ritase >= 20 && $row->ritase <= 29){
                            $total2029 += $row->ritase;
                        }
                        
                        if($row->ritase >= 10 && $row->ritase <= 19){
                            $total1019 += $row->ritase;
                        }
                        
                        if($row->ritase >= 5 && $row->ritase <= 9){
                            $total59 += $row->ritase;
                        }
                        
                        if($row->ritase < 5){
                            $total5 += $row->ritase;
                        }
                        
                        $no++;
                        $baris++;
                    }
                    
                    $barisfooter = $baris;
                    $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisfooter, 'Total');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $barisfooter, $ritase);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $barisfooter, $prices);
                    
                    $totalritase = $ritase;
                    $nextfooter = ($baris+4);
                    /*
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter, 'Summary:');
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+1, 'Total Unit');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+1, 'Total Ritase');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $nextfooter+1, 'Total Sales');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $nextfooter+1, 'Total Costs');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $nextfooter+1, 'Total Gross Margin');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $nextfooter+1, 'Total GM (%)');
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $nextfooter+2, $no-1);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $nextfooter+2, $ritase);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $nextfooter+2, $prices);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $nextfooter+2, $costs);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $nextfooter+2, $achieves);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $nextfooter+2, ($prices > 0 && $achieves > 0) ? (($achieves/$prices)*100)  : 0);
                    */
                }
                else
                {
                    $objPHPExcel->getActiveSheet()->setCellValue('A4:G4', 'Data Not Found');
                    $objPHPExcel->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:G4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:G4')->getFill()->getStartColor()->setARGB('e6f2ff');
                    $objPHPExcel->getActiveSheet()->getStyle("A4:G4")->getFont()->setSize(14);
                }
                
                ob_clean();
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="CustomerSales'.date('FY').'.xlsx"');
                $objWriter->save('php://output');   
            }
        }
    }
    
}