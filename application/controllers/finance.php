<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class finance extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('finances');
      
  }
   
  public function index()
  {

    $data['cities']=$this->cities->getCities();
    $data['id']=$this->cities->getLastID();

    $this->template->load('template','pages/cities',$data);
  } 

  public function akun()
  {
    $data['akun']=$this->finances->getAkun();
    $this->template->load('template','pages/financeAkun',$data);
  }

  public function createAkun()
  {
    $this->finances->createAkun();
    redirect('finance/akun?msg=Save Success');
  }

  public function editAkun()
  {
    $data['akun']=$this->finances->getOneAkun();
    $this->load->view('pages/modalAkun',$data);  
  }

  public function saveAkun()
  {
    $this->finances->saveAkun();
    redirect('finance/akun?msg=Save Success');
  }

  public function deleteAkun()
  {
    $this->finances->deleteAkun();
    redirect('finance/akun?msg=Delete Success');   
  }

  public function deleteBudget()
  {
    $this->db->where('order_id',$this->input->post('id'));
    $this->db->delete('cost'); 
  }

  public function inventaris()
  {
    $data['inventaris']=$this->finances->getInventaris();
    $this->template->load('template','pages/financeInventaris',$data);
  }

  public function saveInventaris()
  {
    $this->finances->saveInventaris();
    redirect('finance/inventaris?msg=Save Success');
  }

  public function budget()
  {
    $data['Budget']=$this->finances->getBudget();
    $data['id']=$this->finances->getLastIDBudget();
    $data['akun']=$this->finances->getAkunBudget();
    $this->template->load('template','pages/financeBudget',$data);
  }

  public function modalBudget()
  {
    $data['budget']=$this->finances->getOneBudget();
    $data['akun']=$this->finances->getAkunBudget();
    $this->load->view('pages/modalBudget',$data);
  }

  public function saveBudget()
  {
    $this->finances->saveBudget();
    redirect('finance/budget?msg=Save Success');
  }

  public function saveEditBudget()
  {
    $this->finances->saveEditBudgets();
    redirect('finance/budget?msg=Save Success');
  }

  public function credit()
  {
    $data['credit']=$this->finances->getCredit();
    $this->load->view('pages/financeCredit',$data);
  }

  public function report()
  {
    //$data['akun']=$this->finances->getAkun();
   // $this->template->load('template','pages/reportFinance',$data);
    $this->template->load('template','pages/financeProfit');
  }

  public function testreport()
  {
    //$data['akun']=$this->finances->getAkun();
   // $this->template->load('template','pages/reportFinance',$data);
    $this->template->load('template','pages/financeProfitver2');
  }

  public function neraca()
  {
    //$data['akun']=$this->finances->getAkun();
   // $this->template->load('template','pages/reportFinance',$data);
    $this->template->load('template','pages/financeNeraca');
  }

  public function profitnloss()
  {
    //$data['akun']=$this->finances->getAkun();
   // $this->template->load('template','pages/reportFinance',$data);
    $this->template->load('template','pages/financeProfitLoss');
  }

  function listCoa()
  {
    $filter = array();

    $data= $this->finances->ChartofAccount($filter);
    $a = 0;
    $b = 0;
    $c = 0;
    $d = 0;


    foreach ($data as $key) {
      if ($key['parent']=="0") {
        # code...
        echo "
        <tr>
        <td colspan='5'><b>".$key['name']."</b></td>
        </tr>";
      }else{
      echo "
        <tr>
        <td> &emsp;".$key['name']."</td>
        ";
        if ($key['type']==1) {
        echo "
          <td align='right'>".number_format($key['totalnominal'])."</td>
          <td align='right'>0</td>";
        }
      }
    }
    
  }

  public function testdoang()
  {
    $filter = array();
    $data= $this->finances->driversummarya($filter);

    $a = 0;
    $b = 0;
    $c = 0;
    $d = 0;


    foreach ($data as $key) {

      if($key['type'] == 'NK') {
        echo "
        <tr>
        <td>".$key['name']."</td>
        <td align='right'>0</td>
        <td align='right'>0</td>
        <td align='right'>0</td>
        <td align='right'>".number_format($key['totalnominal'])."</td>
        </tr>";
        $a += $key['totalnominal'];
      }elseif ($key['type'] == 'ND'){
        if ($key['name']=='KAS') {
          
        echo "
        <tr>
        <td>".$key['name']."</td>
        <td align='right'>0</td>
        <td align='right'>0</td>
        <td align='right'>".number_format($key['totalnominal'])."</td>
        <td align='right'>0</td>
        </tr>";
        $KAS += $key['totalnominal'];

        }elseif($key['name']=='PIUTANG KARYAWAN / KASBON'){
          echo "
        <tr>
        <td>".$key['name']."</td>
        <td align='right'>0</td>
        <td align='right'>0</td>
        <td align='right'>".number_format($key['totalnominal'])."</td>
        <td align='right'>0</td>
        </tr>";
        $KASBON += $key['totalnominal'];
        }else{
        echo "
        <tr>
        <td>".$key['name']."</td>
        <td align='right'>0</td>
        <td align='right'>0</td>
        <td align='right'>".number_format($key['totalnominal'])."</td>
        <td align='right'>0</td>
        </tr>";
        if ($key['name'] =='PIUTANG TRUCKING') {
          $pt = $key['totalnominal'];
        }else{
          $ee += $key['totalnominal'];
        }
      }
      
        $b = $pt + $KAS + $KASBON;
      }elseif ($key['type'] == 'LK'){
        echo "
        <tr>
        <td>".$key['name']."</td>
        <td align='right'>0</td>
        <td align='right'>".number_format($key['totalnominal'])."</td>
        <td align='right'>0</td>
        <td align='right'>0</td>
        </tr>";
        $c += $key['totalnominal'];
      }elseif ($key['type'] == 'LD'){
        echo "
        <tr>
        <td>".$key['name']."</td>
        <td align='right'>".number_format($key['totalnominal'])."</td>
        <td align='right'>0</td>
        <td align='right'>0</td>
        <td align='right'>0</td>
        </tr>";
        $d += $key['totalnominal'];
      }

    }
    echo "
        <tr>
        <td>LABA</td>
        <td align='right'>".number_format($e = $c - $d)."</td>
        <td align='right'>0</td>
        <td align='right'>0</td>
        <td align='right'>".number_format($e = $c - $d)."</td>
        </tr>";

    echo "
        <tr>
        <td>TOTAL</td>
        <td align='right'>".number_format($f = $e + $d)."</td>
        <td align='right'>".number_format($c)."</td>
        <td align='right'>".number_format($b)."</td>
        <td align='right'>".number_format($g = $a + $e + $ee)."</td>
        </tr>";
  }

  

  public function driversummary($page = 0)
  {
    if($this->input->get('export') == 'excel')
    {

      $this->load->library('PHPExcel');
      $data['search'] = array();
      $this->load->model('Fleets');
      $rows = $this->finances->driversummarya($data);

      $objPHPExcel = new PHPExcel();
      $objPHPExcel->getProperties()
      ->setCreator('Total Kilat')
      ->setTitle('Total Kilat')
      ->setLastModifiedBy('Total Kilat')
      ->setDescription('Total Kilat - GPS and System Solution')
      ->setSubject('Total Kilat')
      ->setKeywords('Total Kilat')
      ->setCategory('Total Kilat')
      ;

      $ews = $objPHPExcel->getSheet(0);
      $ews->setTitle('Data');

      $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

      $objPHPExcel->setActiveSheetIndex(0);
      $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Driver Summary');
      $objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
      $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setARGB('ffffcc');
      $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setSize(16);



      $filtertext = "";
      if($this->input->get('fleetplateno') != '') { $filtertext .= "Plateno = ".$this->input->get('fleetplateno'); }
      if($this->input->get('type') != '') { $filtertext .= " Type = ".$this->input->get('type'); }
      if($this->input->get('customer') != '') { $filtertext .= " Customer = ".$this->input->get('customer'); }
      if($this->input->get('code') != '') { $filtertext .= " Fleet Code = ".$this->input->get('code'); }
      if($this->input->get('year') != '') { $filtertext .= " Year = ".$this->input->get('year'); }
      if($this->input->get('month') != '') { $filtertext .= " Month = ".date('F', strtotime("2018-".$this->input->get('month')."-01")); }
      $filters = ($filtertext != "" ? $filtertext : "All Data");

      $objPHPExcel->getActiveSheet()->mergeCells('A2:I2');
      $objPHPExcel->getActiveSheet()->getStyle("A2:I2")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getFill()->getStartColor()->setARGB('eaeaea');
      $objPHPExcel->getActiveSheet()->getStyle("A2:I2")->getFont()->setSize(14);
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Filter : ".$filters);

      $objPHPExcel->getActiveSheet()->getStyle("A3:F4")->getFont()->setBold(false);
      $objPHPExcel->getActiveSheet()->getStyle('A3:F4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('A3:F4')->getFill()->getStartColor()->setARGB('cce6ff');
      $objPHPExcel->getActiveSheet()->getStyle("A3:F4")->getFont()->setSize(12);

      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3,'NO');

      $objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
      $objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3,'KETERANGAN');

      $objPHPExcel->getActiveSheet()->mergeCells('C3:D3');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3,'LABA RUGI');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 4,'D');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 4,'K');

      $objPHPExcel->getActiveSheet()->mergeCells('E3:F3');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3,'NERACA');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 4,'D');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 4,'K');

      if($rows != null)
      {
        $prices = 0;
        $ritase = 0;
        $allowance = 0;
        $allowanceadds = 0;
        $allowancereds = 0;
        $no  = 1;
        $baris = 5;
        foreach($rows AS $row)
        {   
          if($row['type'] == 'NK') {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row['name']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, 0);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris,0);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris,0);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris, number_format($row['totalnominal']));

          }elseif ($row['type'] == 'ND'){
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row['name']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, 0);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris,0);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris,number_format($row['totalnominal']));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris,0 );

          }elseif ($row['type'] == 'LK'){
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row['name']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris,0);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, number_format($row['totalnominal']));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris,0);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris,0);
          }elseif ($row['type'] == 'LD'){
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row['name']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, number_format($row['totalnominal']));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris,0);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris,0);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris,0);
          }

          $ritase += $row['totalnominal'];
          $no++;
          $baris++;
        }

        $barisfooter = $baris;
        $barisbaru = $barisfooter-1;
        $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $barisfooter, $ritase);

        $dsl1=array(
          new PHPExcel_Chart_DataSeriesValues('String', 'Data!$E$3', NULL, 1),
        );
        $dsl2=array(
          new PHPExcel_Chart_DataSeriesValues('String', 'Data!$D$3', NULL, 1),
        );
        $dsl3=array(
          new PHPExcel_Chart_DataSeriesValues('String', 'Data!$F$3', NULL, 1),
        );

        $xal=array(
          new PHPExcel_Chart_DataSeriesValues('String', 'Data!$B$4:$B$'.$barisbaru.'', NULL, $barisbaru),
        );

        $dsv1=array(
          new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$E$4:$E$'.$barisbaru.'', NULL, $barisbaru),
        );
        $series1 = new PHPExcel_Chart_DataSeries(
          PHPExcel_Chart_DataSeries::TYPE_BARCHART,       
          PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,  
          range(0, count($dsv1)-1),         
          $dsl1,                            
          $xal,                             
          $dsv1                         
        );

        $series1->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);


        $dsv2=array(
          new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$D$4:$D$'.$barisbaru.'', NULL, $barisbaru),
        );
        $series2 = new PHPExcel_Chart_DataSeries(
          PHPExcel_Chart_DataSeries::TYPE_LINECHART,      
          PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   
          range(0, count($dsv2)-1),          
          $dsl2,                             
          NULL,                              
          $dsv2                            
        );


        $dsv3=array(
          new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$f$4:$f$'.$barisbaru.'', NULL, $barisbaru),
        );

        $series3 = new PHPExcel_Chart_DataSeries(
          PHPExcel_Chart_DataSeries::TYPE_AREACHART,      
          PHPExcel_Chart_DataSeries::GROUPING_STANDARD,  
          range(0, count($dsv2)-1),         
          $dsl3,                             
          NULL,                                           
          $dsv3                              
        );


        $pa = new PHPExcel_Chart_PlotArea(NULL, array($series1));

        $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

        $title = new PHPExcel_Chart_Title('Average Weather Chart for Crete');


        $legend=new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
        $title = new PHPExcel_Chart_Title('DRIVER / RITASE');
        $chart= new PHPExcel_Chart(
          'chart1',
          $title,
          $legend,
          $pa,
          true,
          0,
          NULL, 
          NULL
        );
        $chart->setTopLeftPosition('K1');
        $chart->setBottomRightPosition('AE23');
      }
      else
      {
        $objPHPExcel->getActiveSheet()->setCellValue('A4:I4', 'Data Not Found');
        $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('e6f2ff');
        $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(14);
      }




      $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

      $writer->setIncludeCharts(true);

      header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
      header("Cache-Control: no-store, no-cache, must-revalidate");
      header("Cache-Control: post-check=0, pre-check=0", false);
      header("Pragma: no-cache");
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

      header('Content-Disposition: attachment;filename="Report Finance'.date('dmy').'.xlsx"');
      $writer->save('php://output');


    }
    else
    {

      $perpage = 50;

      $data = array();
      $filter = array();
      $data['search'] = array();

      $limit  = $perpage;
      $offset = $page > 1 ? ($limit*$page)-$limit:0;

      $all_data = $this->finances->driversummary($filter);
      $data['total'] = count($all_data);
      $config['base_url'] = base_url() . '/finances/driversummary';
      $config['use_page_numbers'] = TRUE;
      $config['total_rows'] = $data['total'];
      $config['per_page'] = $perpage;
      $config['num_links'] = 1;
      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['first_link'] = 'First';
      $config['first_tag_open'] = '<li class="prev page">';
      $config['first_tag_close'] = '</li>';
      $config['last_link'] = 'Last';
      $config['last_tag_open'] = '<li class="next page">';
      $config['last_tag_close'] = '</li>';
      $config['next_link'] = 'Next';
      $config['next_tag_open'] = '<li class="next page">';
      $config['next_tag_close'] = '</li>';
      $config['prev_link'] = 'Prev';
      $config['prev_tag_open'] = '<li class="prev page">';
      $config['prev_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a>';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li class="page">';
      $config['num_tag_close'] = '</li>';
      $config['anchor_class'] = 'follow_link';

      $this->pagination->initialize($config);
      $paginator = $this->pagination->create_links();
      $data['pagination'] = $paginator;



      $filter["limit"]  = $limit;
      $filter["page"] = $page;
      $filter["offset"] = $offset;

      $data["result"] = $this->finances->driversummary($filter);
      $data["row"] = $offset;
      echo json_encode($data);
    }
  }

  public function Testr($page=0)
  {
     $perpage = 50;
    
      $data = array();
      $filter = array();
      $data['search'] = array();
      
      $limit  = $perpage;
      $offset = $page > 1 ? ($limit*$page)-$limit:0;
              
      $all_data =  $this->finances->getcoa(0);
      $data['total'] = count($all_data);
      $config['base_url'] = base_url() . '/returnDo/loadsummary';
      $config['use_page_numbers'] = TRUE;
      $config['total_rows'] = $data['total'];
      $config['per_page'] = $perpage;
      $config['num_links'] = 1;
      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['first_link'] = 'First';
      $config['first_tag_open'] = '<li class="prev page">';
      $config['first_tag_close'] = '</li>';
      $config['last_link'] = 'Last';
      $config['last_tag_open'] = '<li class="next page">';
      $config['last_tag_close'] = '</li>';
      $config['next_link'] = 'Next';
      $config['next_tag_open'] = '<li class="next page">';
      $config['next_tag_close'] = '</li>';
      $config['prev_link'] = 'Prev';
      $config['prev_tag_open'] = '<li class="prev page">';
      $config['prev_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a>';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li class="page">';
      $config['num_tag_close'] = '</li>';
      $config['anchor_class'] = 'follow_link';
      
      $this->pagination->initialize($config);
      $paginator = $this->pagination->create_links();
      $data['pagination'] = $paginator;
              
      //$data["page"] = $page;
      ///$data["offset"] = $offset;
      //$data["limit"] = $limit;
              
      $filter["limit"]  = $limit;
      $filter["page"] = $page;
      $filter["offset"] = $offset;
              
      $data["result"] =  $this->finances->getcoa(0);
      $data["row"] = $offset;
      echo json_encode($data);
    
  }
}
?>