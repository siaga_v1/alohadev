<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class fleettype extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('fleettypes');
      
  }
   
  // membuat fungsi index
  public function index()
  {

    $data['fleet']=$this->fleettypes->getFleettype();
    $data['id']=$this->fleettypes->getLastID();


    $this->template->load('template','pages/fleettypes',$data);
  } 

  public function saveEdit()
  {
    $this->fleettypes->editfleet();
    redirect('city?msg=Save Success');
  }

  public function modaledit()
  {
    $data['fleet']=$this->fleettypes->getOneOrder();


    $this->load->view('pages/modalfleet',$data);

   
  }

  public function savefleet()
  {
    $this->fleettypes->Simpan();
    redirect('fleettype?msg=Save Success');
  }



  public function deletefleet()
  {
    $this->fleettypes->delete();
    redirect('fleettype?msg=Delete Success');
   
  }
  
}
?>