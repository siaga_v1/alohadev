<?php if (!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');

class Kas extends CI_Controller
{
    public function index()
    {
        $this->template->load('template', 'kas/base');
    }

    public function list($page = 0)
    {
        $this->load->model('kass');
        $perpage = 20;

        $data = array();
        $filter = array();
        $data['search'] = array();

        $limit  = $perpage;
        $offset = $page > 1 ? ($limit * $page) - $limit : 0;

        $all_data =  $this->kass->selectAll($filter);
        $data['total'] = count($all_data);
        $config['base_url'] = base_url() . '/kas/list';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $perpage;
        $config['num_links'] = 1;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';
        $config['anchor_class'] = 'follow_link';


        $this->pagination->initialize($config);
        $paginator = $this->pagination->create_links();
        $data['paginator'] = $paginator;

        $data["page"] = $page;
        $data["offset"] = $offset;
        $data["limit"] = $limit;

        $filter["limit"]  = $limit;
        $filter["page"] = $page;
        $filter["offset"] = $offset;
        $data["rows"]     = $this->kass->selectAll($filter);
        $this->load->view('kas/list', $data);
    }

    public function form()
    {
        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        $data["id"] = $id;
        $data["code"] = $this->getCode();
        $data["date"] = "";
        $data["akun"] = "";
        $data["deskripsi"] = "";
        $data["nominal"] = "";

        if ($id > 0) {
            $this->load->model("kass");
            $row = $this->kass->selectOne($data);
            $data["id"] = $row->id;
            $data["code"] = $row->order_id;
            $data["date"] = $row->date;
            $data["akun"] = $row->coa;
            $data["deskripsi"] = $row->description;
            $data["nominal"] = $row->nominal;
            $data["akun_name"] = $row->name;
        }

        $this->load->view('kas/form', $data);
    }

    public function set()
    {

        $return['type'] = 'success';
        $return['title'] = 'Update success!';
        $return['message'] = 'Update data maintenance success!';

        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        $return["id"] = 0;
        if ($id == 0) {
            $this->db->set("date", $this->input->post("date"));
            $this->db->set("coa", $this->input->post("akun"));
            $this->db->set("order_id", $this->input->post("code"));
            $this->db->set("description", $this->input->post("deskripsi"));
            $this->db->set("akun_id", 1);
            $this->db->set("nominal", str_replace(",", "", $this->input->post("nominal")));

            $this->db->set("active", 1);
            $this->db->set("createby", $this->session->userdata("id"));
            $this->db->set("createdate", date("Y-m-d H:i:s"));

            if (!$this->db->insert("cost")) {
                $return['type'] = 'error';
                $return['title'] = 'Update failed!';
                $return['message'] = 'Update data invoice failed!';
            }
        } else {
            $this->db->set("order_id", $this->input->post("code"));
            $this->db->set("coa", $this->input->post("akun"));
            $this->db->set("date", $this->input->post("date"));
            $this->db->set("description", $this->input->post("deskripsi"));
            $this->db->set("nominal", str_replace(",", "", $this->input->post("nominal")));
            $this->db->set("akun_id", 1);
            $this->db->where("id", $id);

            if (!$this->db->update("cost")) {
                $return['type'] = 'error';
                $return['title'] = 'Update failed!';
                $return['message'] = 'Update data maintenance failed!';
            }
        }

        echo json_encode($return);
    }

    public function delete()
    {

        $return['type'] = 'success';
        $return['title'] = 'Delete success!';
        $return['message'] = 'Delete management data invoice success!';

        $id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
        if ($id > 0) {
            $this->load->model('kass');
            if (!$this->kass->delete()) {
                $return['type'] = 'error';
                $return['title'] = 'Delete failed!';
                $return['message'] = 'Delete management data invoice failed!';
            }
        }

        echo json_encode($return);
    }


    private function getCode()
    {
        $this->db->select('*');
        $this->db->like('order_id', 'KAS', 'after');
        $this->db->order_by("order_id", "desc");
        $result = $this->db->get('cost', 1);

        foreach ($result->result() as $row) {
            $code = $row->order_id;
            $pattern = "/(\d+)/";

            $array = preg_split($pattern, $code, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
            $ccode = sprintf("%04d", $array[1] + 1);
        }
        $cccode = "KAS" . $ccode;
        return $cccode;
    }

    public function autoComplete()
    {
        $q = trim($this->input->get("q")) != "" ? trim(strtolower($this->db->escape_str($this->input->get("q")))) : "";

        $this->db->select("id, name");
        $this->db->where('parent', '3');
        $this->db->where("active", '1');
        if ($q != "") {
            $this->db->like("LOWER(name)", $q);
        }
        $result = $this->db->get("chartofaccount");
        $rows = $result->result();

        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $data[] = array('id' => $row->id, 'text' => $row->name);
            }
        } else {
            $data[] = array('id' => '', 'text' => 'No Data Found');
        }

        echo json_encode($data);
    }
}
