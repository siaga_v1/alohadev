<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !!');
//Membuat class Login = karena file'nya bernama Login.php | harus sama !
class Login extends CI_Controller{
    //Membuat fungsi Construc
    public function __construct(){
        parent::__construct();
        $this->load->model('m_login');
        $this->load->model('finances');
        $this->load->library(array('form_validation','session'));
        $this->load->database();
        $this->load->helper('url');   
    }

    public function index(){

       // $session = $this->session->userdata('isLogin');
            //jika session salah 
           // if($session == FALSE){
            //    redirect('login/loginPage');   
          //  }else{
                //jika session benar 
          //      redirect('home');
          //  }

        $this->load->view('login/loginPage');

    }
    //membuat fungsi login_form 
    public function loginPage()
    {   
        $filter = array();
    $data= $this->finances->driversummarya($filter);
 
    $KAS=0;
    $KAWS=0;
    $KWSA=0;


    foreach ($data as $key) {
        $KAS += $key['totalnominal'];
        $KAWS += $key['totalpengeluaran'];

       

    }
     $KWSA = $KAS - $KAWS;

        //jika form yang di isi benar 
        $data['username'] = $this->input->post('username');
        $data['password'] = $this->input->post('password');
        $cek = $this->m_login->ambilPengguna($data);
        
        if($cek != null){
			foreach ($cek as $row){

        $this->session->set_userdata('isLogin', TRUE);
        $this->session->set_userdata('firstname', $row->firstname);
        $this->session->set_userdata('jabatan', $row->lastname);
        $this->session->set_userdata('id', $row->id);
        $this->session->set_userdata('saldo', $KWSA);
        $this->session->set_userdata('role', $row->id_userroles);
        redirect('dashboard');
		}
        }else{
        // jika salah
        ?>
            <script>
            alert('Gagal Login: Cek username dan password anda!');
            history.go(-1);
            </script>
            <?php
            }
        
    }
    //membuat fungsi keluar /logout 
    public function logout(){
        // menghapus session dan mengembalikan ke login_form
        $this->session->sess_destroy();
        redirect('login');
    }
}
?>