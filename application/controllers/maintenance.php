<?php if (!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
class maintenance extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('maintenances');
	}

	public function index()
	{
		//$data['cost']=$this->maintenances->getMaintenance();
		$data['supplier'] = $this->maintenances->getSupplier();
		$data['id'] = $this->maintenances->getLastId();

		$this->load->model('fleets');
		$data['fleet'] = $this->fleets->getFleets();
		$this->template->load('template', 'pages/maintenance', $data);
	}

	public function base()
	{
		$this->template->load('template', 'maintenance/base');
	}

	public function list($page = 0)
	{
		$this->load->model('maintenances');
		$perpage = 20;

		$data = array();
		$filter = array();
		$data['search'] = array();

		$limit  = $perpage;
		$offset = $page > 1 ? ($limit * $page) - $limit : 0;

		$all_data =  $this->maintenances->loadMaintenance($filter);
		$data['total'] = count($all_data);
		$config['base_url'] = base_url() . '/maintenance/list';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $data['total'];
		$config['per_page'] = $perpage;
		$config['num_links'] = 1;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['anchor_class'] = 'follow_link';


		$this->pagination->initialize($config);
		$paginator = $this->pagination->create_links();
		$data['paginator'] = $paginator;

		$data["page"] = $page;
		$data["offset"] = $offset;
		$data["limit"] = $limit;

		$filter["limit"]  = $limit;
		$filter["page"] = $page;
		$filter["offset"] = $offset;
		$data["rows"]     = $this->maintenances->loadMaintenance($filter);
		$this->load->view('maintenance/list', $data);
	}

	public function form()
	{
		$id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
		$data["id"] = $id;
		$data["code"] = $this->getCode();
		$data["fleet"] = 0;
		$data["supplier"] = "";
		$data["date"] = "";
		$data["detail"] = "";


		if ($id > 0) {
			$this->load->model("maintenances");
			$row = $this->maintenances->selectOne($data);
			$data["id"] = $row->id;
			$data["code"] = $row->code;
			$data["fleet"] = $row->fleet_id;
			$data["supplier"] = $row->supplier_id;
			$data["date"] = $row->date;
			$data["plateno"] = $row->fleet;
			$data["supplier_name"] = $row->supplier;
			$data["detail"] = $this->maintenances->getDetailMaintenance($data);
		}

		$this->load->view('maintenance/form', $data);
	}

	public function set()
	{

		$return['type'] = 'success';
		$return['title'] = 'Update success!';
		$return['message'] = 'Update data maintenance success!';

		$id = $this->input->post('id') > 0 ? $this->input->post('id') : 0;
		$return["id"] = 0;
		if ($id == 0) {
			$this->db->set("code", $this->input->post("code"));
			$this->db->set("supplier_id", $this->input->post("supplier"));
			$this->db->set("date", $this->input->post("date"));
			$this->db->set("fleet_id", $this->input->post("fleet"));

			$this->db->set("active", 1);
			$this->db->set("createby", $this->session->userdata("id"));
			$this->db->set("createdate", date("Y-m-d H:i:s"));
			$this->db->set("updateby", $this->session->userdata("id"));
			$this->db->set("updatetime", date("Y-m-d H:i:s"));

			if (!$this->db->insert("maintenance")) {
				$return['type'] = 'error';
				$return['title'] = 'Update failed!';
				$return['message'] = 'Update data invoice failed!';
			} else {
				$codeparts    = $this->input->post('codeparts');
				$partnumber     = $this->input->post('partnumber');
				$fleet_id     = $this->input->post('fleet');
				$price   = str_replace(",", "", $this->input->post('price'));

				$adddefect = count($partnumber);


				if ($adddefect >= 0) {
					for ($i = 0; $i < $adddefect; $i++) {
						$dt = array(
							'maintenance_code' => $this->input->post('code'),
							'date' => $this->input->post('date'),
							'code_part' => $codeparts[$i],
							'part_number' => $partnumber[$i],
							'fleet_id' => $fleet_id,
							'price' => $price[$i],
							'active' => 1
						);
						$this->db->insert('maintenance_detail', $dt);
					}
					for ($i = 0; $i < $adddefect; $i++) {
						$dt = array(
							'order_id' => $this->input->post('code'),
							'date' => $this->input->post('date'),
							'nominal' => $price[$i],
							'coa' => 110,
							'cost_type' => 1,
							'active' => 1,
							'akun_id' => 13,
						);
						$this->db->insert('cost', $dt);
					}
				}
			}
		} else {
			$this->db->set("code", $this->input->post("code"));
			$this->db->set("supplier_id", $this->input->post("supplier"));
			$this->db->set("date", $this->input->post("date"));
			$this->db->set("fleet_id", $this->input->post("fleet"));
			$this->db->where("id", $id);

			if (!$this->db->update("maintenance")) {
				$return['type'] = 'error';
				$return['title'] = 'Update failed!';
				$return['message'] = 'Update data maintenance failed!';
			} else {
				$codes    = $this->input->post('code');
				$codeparts    = $this->input->post('codeparts');
				$partnumber     = $this->input->post('partnumber');
				$fleet_id     = $this->input->post('fleet');
				$price   = str_replace(",", "", $this->input->post('price'));

				$adddefect = count($codeparts);


				if ($adddefect >= 0) {
					for ($i = 0; $i < $adddefect; $i++) {
						$ids   = $this->input->post('ids[' . $i . ']');

						$dt = array(
							'maintenance_code' => $this->input->post('code'),
							'code_part' => $codeparts[$i],
							'part_number' => $partnumber[$i],
							'fleet_id' => $this->input->post('fleet'),
							'price' => $price[$i]
						);
						if (isset($id)) {
							$this->db->where('id', $ids);
							$this->db->update('maintenance_detail', $dt);
						} else {

							$this->db->insert('maintenance_detail', $dt);
						}
					}

					for ($i = 0; $i < $adddefect; $i++) {

						if (isset($codes)) {
							$dt = array(
								'date' => $this->input->post('date'),
								'nominal' => $price[$i],
								'coa' => 110,
								'cost_type' => 1,
								'active' => 1,
								'akun_id' => 13,
							);
							$this->db->where('order_id', $codes);
							$this->db->update('cost', $dt);
						} else {
							$dt = array(
								'order_id' => $this->input->post('code'),
								'date' => $this->input->post('date'),
								'nominal' => $price[$i],
								'coa' => 110,
								'cost_type' => 1,
								'active' => 1,
								'akun_id' => 13,
							);
							$this->db->insert('cost', $dt);
						}
					}
				}
				$return["id"] = $id;
			}
		}

		echo json_encode($return);
	}

	private function getCode()
	{
		$this->db->select('*');
		$this->db->order_by("code", "desc");
		$result = $this->db->get('maintenance', 1);

		foreach ($result->result() as $row) {
			$code = $row->code;
			$pattern = "/(\d+)/";

			$array = preg_split($pattern, $code, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
			$ccode = sprintf("%04d", $array[1] + 1);
		}
		$cccode = "M" . $ccode;
		return $cccode;
	}


	public function modaledit()
	{
		$this->load->model('fleets');

		$data['supplier'] = $this->maintenances->getSupplier();
		$data['fleet'] = $this->fleets->getFleets();
		$data['maintenance'] = $this->maintenances->getOneMaintenance();
		$data['detail'] = $this->maintenances->getDetailMaintenance();

		$this->load->view('pages/modalmaintenance', $data);
	}

	public function modaldetail()
	{
		$this->load->model('fleets');

		$data['supplier'] = $this->maintenances->getSupplier();
		$data['fleet'] = $this->fleets->getFleets();
		$data['maintenance'] = $this->maintenances->getOneMaintenance();
		$data['detail'] = $this->maintenances->getDetailMaintenance();

		$this->load->view('pages/modaldetailmaintenance', $data);
	}

	public function getItems()
	{
		$this->db->select('*');
		$this->db->where('active', '1');
		$result = $this->db->get('items');

		echo "<option value=''>- Select -</option>";
		foreach ($result->result_array() as $key) {
			echo "<option value='" . $key['partnumber'] . "'>" . $key['partnumberdesc'] . "</option>";
		};
	}
	public function get_autocomplete()
	{
		$query = $this->input->get('query');
		if (isset($query)) {
			$this->db->like('code', $query, 'both');
			$this->db->order_by('code', 'ASC');
			$this->db->limit(10);
			$result = $this->db->get('orders')->result();
			if (count($result) > 0) {
				foreach ($result as $row) {
					$arr_result[] = $row->code;
				}
				echo json_encode($arr_result);
			}
		}
	}

	public function saveMaintenance()
	{
		$this->maintenances->savemaintenance();
		redirect('maintenance?msg=Save Success');
	}

	public function saveEdit()
	{
		$this->maintenances->saveEditMaintenance();
		redirect('maintenance?msg=Save Success');
	}

	public function delete()
	{

		$return =  "BENER";

		$id = $this->input->post('id') != "" ? $this->input->post('id') : null;
		if ($id != "") {
			$this->load->model('maintenances');
			if (!$this->maintenances->delete()) {
				$return = "SALAH";
			}
		}

		echo $return;
	}

	public function deleteMaintenance()
	{
		$this->maintenances->Delete();
		echo "Delete Success !";
	}

	function loadMaintenance($page = 0)
	{
		$perpage = 70;

		$data = array();
		$filter = array();
		$data['search'] = array();

		$limit  = $perpage;
		$offset = $page > 1 ? ($limit * $page) - $limit : 0;

		$all_data =  $this->maintenances->loadMaintenance($filter);
		$data['total'] = count($all_data);
		$config['base_url'] = base_url() . '/maintenance/loadMaintenance';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $data['total'];
		$config['per_page'] = $perpage;
		$config['num_links'] = 1;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['anchor_class'] = 'follow_link';

		$this->pagination->initialize($config);
		$paginator = $this->pagination->create_links();
		$data['pagination'] = $paginator;

		$filter["limit"]  = $limit;
		$filter["page"] = $page;
		$filter["offset"] = $offset;

		$data["result"] =  $this->maintenances->loadMaintenance($filter);
		$data["row"] = $offset;
		echo json_encode($data);
	}
}
