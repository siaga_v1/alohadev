<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
class maintenance extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('maintenances');

	}

	public function index()
	{
		$data['cost']=$this->maintenances->getMaintenance();
		$data['supplier']=$this->maintenances->getSupplier();
		$data['id']=$this->maintenances->getLastId();

		$this->load->model('fleets');
		$data['fleet']=$this->fleets->getFleets();
		$this->template->load('template','pages/maintenance',$data);
	}

	public function modaledit()
	{
		$this->load->model('fleets');

		$data['supplier']=$this->maintenances->getSupplier();
		$data['fleet']=$this->fleets->getFleets();
		$data['maintenance']=$this->maintenances->getOneMaintenance();
		$data['detail']=$this->maintenances->getDetailMaintenance();

		$this->load->view('pages/modalmaintenance',$data);
	}

	public function modaldetail()
	{
		$this->load->model('fleets');

		$data['supplier']=$this->maintenances->getSupplier();
		$data['fleet']=$this->fleets->getFleets();
		$data['maintenance']=$this->maintenances->getOneMaintenance();
		$data['detail']=$this->maintenances->getDetailMaintenance();

		$this->load->view('pages/modaldetailmaintenance',$data);
	}
	public function get_autocomplete()
	{
		$query = $this->input->get('query');
		if (isset($query)) {
			$this->db->like('code', $query , 'both');
			$this->db->order_by('code', 'ASC');
			$this->db->limit(10);
			$result = $this->db->get('orders')->result();
			if (count($result) > 0) {
				foreach ($result as $row)
				{
					$arr_result[]=$row->code;
				}
				echo json_encode($arr_result);
			}
		}
	}

	public function saveMaintenance()
	{
		$this->maintenances->savemaintenance();
		redirect('maintenance?msg=Save Success');
	}

	public function saveEdit()
	{
		$this->maintenances->saveEditMaintenance();
		redirect('maintenance?msg=Save Success');
	}

	public function deleteMaintenance()
	{
		$this->maintenances->Delete();
		echo "Delete Success !";
	}


}
?>