<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class masterArmada extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
     $this->load->model('modelArmada');
      
  }
   
  // membuat fungsi index
  public function index()
  {
    $data['armada']=$this->modelArmada->ambilArmada();
    $this->template->load('template','pages/listArmada',$data);
  } 

  public function tambahArmada()
  {
    $this->template->load('template','pages/tambahArmada');
  }

  public function updateArmada()
  {
    
  }

  public function showArmada()
  {
    
  }

  public function deleteArmada()
  {
    
  }


   
}
?>