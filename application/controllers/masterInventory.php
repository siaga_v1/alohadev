<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class masterInventory extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
     $this->load->model('modelInventory');
      
  }
   
  // membuat fungsi index
  public function index()
  {
    $data['inventory']=$this->modelInventory->ambilinventory();
    $this->template->load('template','pages/listInventory',$data);
  } 

  public function riwayatItem()
  {
    $data['riwayat']=$this->modelInventory->riwayatItem();
    $this->template->load('template','pages/riwayatItem',$data);
  } 

  public function cobapdf()
    {
         //load mpdf libray
  $this->load->library('M_pdf');

  $mpdf = $this->m_pdf->load([
    'mode' => 'utf-8',
    'format' => 'A4'
  ]);

  $mpdf->WriteHTML("Hello World!");

  $mpdf->Output();
    }

}
?>