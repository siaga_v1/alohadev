<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class masterIssued extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
     $this->load->model('modelArmada');
     $this->load->model('modelEmployee');
     $this->load->model('modelInventory');
     $this->load->model('permintaanBarang');
     $this->load->model('modelIssued');
      
  }
   
  // membuat fungsi index
  public function index()
  {
    $data['SPB']=$this->modelIssued->listPermintaanBarangApproved(); 
    $this->template->load('template','Pages/listIssued',$data);
  } 
  public function storeman()
  {
    $data['SPB']=$this->modelIssued->listPermintaanBarangA(); 
    $this->template->load('template','Pages/listIssued',$data);
  } 

  public function detailIssued()
  {
    $data['employee']=$this->permintaanBarang->ambilEmployee();
    $data['SPB']=$this->modelIssued->detailOrderBarang();
    $data['nomor']=$this->modelIssued->ambilNomor();
    $this->template->load('template','Pages/detailIssued',$data);
  }

  public function issuedAll()
  {
    $data['issued']=$this->modelIssued->issuedAll();
    $this->template->load('template','Pages/issuedAll',$data);
  }


  public function approveLogistic()
  {

    $idpb=$_GET['id'];

    $this->db->query("UPDATE data_permintaan_barang SET dpb_status='11'  WHERE dpb_code = '$idpb'");

    redirect('masterIssued?msg=Berhasil di Approve');
  }

  public function barangKeluar()
  {

    $idpb= $this->input->post('idspb');
    $now=date('d-m-Y');
    $detail = $this->input->post('detail');
    //$data['bk_ref']        = $this->input->post('idspb');
   // $data['bk_id']         = $this->input->post('issued');
   // $data['bk_date']       = $now;
   // $data['bk_arm']        = $this->input->post('armada');
   // $data['bk_emp']        = $this->input->post('emp');
   // $data['bk_status']     = "0";
        //$data['dpb_createby']  = $idp;
        
    //$this->db->insert('data_barang_keluar',$data);
     
    foreach($detail as $details)
    { 
      $tmp = $details['count'] - $details['stock'];

      if ($details['stock']=="0") {
          $qty = $details['count']; 
          
          $d2        = $this->input->post('idspb');
          $kode        = $this->input->post('idpr');
          $a2        = $this->input->post('idspb');
          $b2        = $now;
          $c2        = $this->input->post('armada');
          $f2        = $this->input->post('emp');
          $e2        = "13";

          $this->db->query("INSERT INTO data_order_barang (dob_code, dob_date,dob_arm_id, dob_emp_id, dob_status,dob_ref) VALUES('$kode', '$b2','$c2', '$f2','$e2','$d2') ON DUPLICATE KEY UPDATE dob_status='$e2'");
          //$this->db->insert('data_order_barang',$data);

          $datd['dobi_dob_id']   = $kode;
          $datd['dobi_item_id']  = $details['item'];
          $datd['dobi_count']    = $qty;
          $datd['dobi_note']     = $details['note'];
          $it=$details['item'];
  
          $this->db->insert('data_order_barang_item',$datd);

          $this->db->query("UPDATE data_permintaan_barang SET dpb_status = '13'  WHERE dpb_code = '$d2'");
          

          //$this->db->select('*');
          //$this->db->order_by("dob_code", "desc");
          //$this->db->like('dob_code', $d2 ,'after');
          //$this->db->where();
          //$result = $this->db->get('data_order_barang',1);

          /*if($result->result() == NULL)

          {
            $kode = $d2."/1"."";

            $this->db->query("INSERT INTO data_order_barang (dob_code, dob_date,dob_arm_id, dob_emp_id, dob_status) VALUES('$kode', '$b2','$c2', '$f2','$e2') ON DUPLICATE KEY UPDATE dob_status='$e2'");
          //$this->db->insert('data_order_barang',$data);

          $datd['dobi_dob_id']   = $kode;
          $datd['dobi_item_id']  = $details['item'];
          $datd['dobi_count']    = $tmp;
          $datd['dobi_note']     = $details['note'];
          $it=$details['item'];
  
          $this->db->insert('data_order_barang_item',$datd);

          $this->db->query("UPDATE data_permintaan_barang SET dpb_status = '13'  WHERE dpb_code = '$d2'");
          
          }else{
            foreach ($result->result() as $row ) {
              # code...
           
            $nmr=$row->dob_code ;
            $ex = explode('/', $nmr);
            $z = sprintf("%d", $ex[4]+1); 
            $kode =  $d2."/".$z;
             }
             
          //$data['dpb_createby']  = $idp;
          $this->db->query("INSERT INTO data_order_barang (dob_code, dob_date,dob_arm_id, dob_emp_id, dob_status) VALUES('$kode', '$b2','$c2', '$f2','$e2') ON DUPLICATE KEY UPDATE dob_status='$e2'");
          //$this->db->insert('data_order_barang',$data);

          $datd['dobi_dob_id']   = $kode;
          $datd['dobi_item_id']  = $details['item'];
          $datd['dobi_count']    = $tmp;
          $datd['dobi_note']     = $details['note'];
          $it=$details['item'];
  
          $this->db->insert('data_order_barang_item',$datd);

          $this->db->query("UPDATE data_permintaan_barang SET dpb_status = '13'  WHERE dpb_code = '$d2'");
          }
          */

        //printf($tmp);

      }elseif ($details['count'] > $details['stock']) {
          $tm = $details['count'] - $details['stock'];
          $tpm = $details['count'] - $tm;
          
          $a        = $this->input->post('idspb');
          $b        = $this->input->post('issued');
          $c        = $now;
          $d        = $this->input->post('armada');
          $e        = $this->input->post('emp');
          $f        = "0";

          $this->db->query("INSERT INTO data_barang_keluar (bk_ref, bk_id,bk_date, bk_arm, bk_emp,bk_status) VALUES('$a', '$b','$c', '$d','$e','$f') ON DUPLICATE KEY UPDATE bk_status='$f'");

          $datad['id_bk']         = $this->input->post('issued');
          $datad['bki_item']      = $details['item'];
          $datad['bki_qty']       = $tpm;
          $datad['bki_note']      = $details['note'];
          $it=$details['item'];
        
          $this->db->insert('data_barang_keluar_detail',$datad);

          $this->db->query("UPDATE mst_inv_item SET ii_stock = '0'  WHERE ii_id = '$it'");

          $d1        = $this->input->post('idspb');
          //$kode        = $this->input->post('idpr');
          $a1       = $this->input->post('idspb');
          $b1        = $now;
          $c1        = $this->input->post('armada');
          $f1        = $this->input->post('emp');
          $e1        = "13";
          //$data['dpb_createby']  = $idp;
          $this->db->query("INSERT INTO data_order_barang (dob_code, dob_date,dob_arm_id, dob_emp_id, dob_status, dob_ref) VALUES('$kode', '$b1','$c1', '$f1','$e1','$e1') ON DUPLICATE KEY UPDATE dob_status='$e1'");
          //$this->db->insert('data_order_barang',$data);

          $daad['dobi_dob_id']   = $kode;
          $daad['dobi_item_id']  = $details['item'];
          $daad['dobi_count']    = $tm;
          $daad['dobi_note']     = $details['note'];
          $it=$details['item'];
        
          $this->db->insert('data_order_barang_item',$daad);

        //printf($tmp);

      }
      else{

          $a        = $this->input->post('idspb');
          $b        = $this->input->post('issued');
          $c        = $now;
          $d        = $this->input->post('armada');
          $e        = $this->input->post('emp');
          $f        = "13";

          $this->db->query("INSERT INTO data_barang_keluar (bk_ref, bk_id,bk_date, bk_arm, bk_emp,bk_status) VALUES('$a', '$b','$c', '$d','$e','$f') ON DUPLICATE KEY UPDATE bk_status='$f'");

          $tpm =$details['stock'] - $details['count'] ;
          $dtad['id_bk']         = $this->input->post('issued');
          $dtad['bki_item']  = $details['item'];
          $dtad['bki_qty']    = $details['count'];
          $dtad['bki_note']     = $details['note'];
          $y    = $details['id'];
          $it=$details['item'];
        
          $this->db->insert('data_barang_keluar_detail',$dtad);

          $data = array(
               'dpbi_status' => '1'
            );

          $this->db->where('dpbi_id', $y);
          $this->db->update('data_permintaan_barang_item', $data); 



          //$this->db->query("UPDATE mst_inv_item SET ii_stock = '$tpm'  WHERE ii_id = '$it'");
         

    }

  }


          //$d        = $this->input->post('idspb');
          //$data['bk_ref']        = $this->input->post('idspb');
         // $data['bk_id']         = $this->input->post('issued');
         // $data['bk_date']       = $now;
         // $data['bk_arm']        = $this->input->post('armada');
         // $data['bk_emp']        = $this->input->post('emp');
         //$data['bk_status']     = "0";
          //$data['dpb_createby']  = $idp;
        
          //$this->db->insert('data_barang_keluar',$data);
          #$this->db->query("UPDATE mst_inv_item SET ii_stock = '$tpm'  WHERE ii_id = '$it'");
        
     // $this->db->query("UPDATE data_permintaan_barang SET dpb_status='11'  WHERE dpb_code = '$d'");
     
     //$this->db->query("UPDATE data_permintaan_barang SET dpb_status='7'  WHERE dpb_code = '$idpb'");
      //foreach($detail as $details)
        //{ 
         // $item  = $details['item'];
         // $stok    = $details['stok'];
        
         // $this->db->query("UPDATE mst_inv_item SET ii_stock = '$stok' WHERE ii_id = '$item'");
        //}

        $this->db->select('*');
        $this->db->where('dpbi_dpb_id', $idpb);
        $this->db->where('dpbi_status', '0');
        $query = $this->db->get('data_permintaan_barang_item');
        //return $result->row();
        if($query->row() == NULL){
        //$test=$this->modelIssued->ubahStatus();
        //redirect('masterIssued?msg=Berhasil di Approve');

        $this->load->library('M_pdf');

        $mpdf = $this->m_pdf->load([
          'mode' => 'utf-8',
          'format' => 'A4'
        ]);
        
        $this->db->query("UPDATE data_permintaan_barang SET dpb_status='7'  WHERE dpb_code = '$idpb'");

        $dss         = $this->input->post('issued');

        $data['SPB']=$this->modelIssued->detailBarangKeluar();
        $view = $this->load->view('pages/printIssued',$data,true);

        $mpdf->WriteHTML($view);

        $mpdf->Output();
      }
      else{
        redirect('masterIssued?msg=Proses PR');
      }
  }

   public function print()
    {
      
        $idpb= $this->input->post('idspb');
        $now=date('d-m-Y');
        $detail = $this->input->post('detail');
        $data['bk_ref']        = $this->input->post('idspb');
        
        $data['bk_id']         = $this->input->post('issued');
        $data['bk_date']       = $now;
        $data['bk_arm']        = $this->input->post('armada');
        $data['bk_emp']        = $this->input->post('emp');
        $data['bk_status']     = "0";
        //$data['dpb_createby']  = $idp;
        
        $this->db->insert('data_barang_keluar',$data);
     
        foreach($detail as $details)
        { 
          $datad['id_bk']         = $this->input->post('issued');
          $datad['bki_item']  = $details['item'];
          $datad['bki_qty']    = $details['count'];
          $datad['bki_note']     = $details['note'];
        
            $this->db->insert('data_barang_keluar_detail',$datad);
        }

        $this->load->library('M_pdf');

        $mpdf = $this->m_pdf->load([
          'mode' => 'utf-8',
          'format' => 'A4'
        ]);
        
        $this->db->query("UPDATE data_permintaan_barang SET dpb_status='7'  WHERE dpb_code = '$idpb'");

        $dss         = $this->input->post('issued');

        $data['SPB']=$this->modelIssued->detailBarangKeluar();
        $view = $this->load->view('pages/printIssued',$data,true);

  $mpdf->WriteHTML($view);

  $mpdf->Output();
    }


   
}
?>