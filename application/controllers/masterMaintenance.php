<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class masterMaintenance extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
     $this->load->model('modelMaintenance');
      
  }
   
  // membuat fungsi index
  public function index()
  {
    $data['driver']=$this->modelMaintenance->ambilDriver();
    $data['armada']=$this->modelMaintenance->ambilArmada();
    $this->template->load('template','pages/tambahMaintenance',$data);
  } 

  public function tambahMaintenance()
  {
    $idp=$this->session->userdata('emp_code');
    $this->modelMaintenance->tambahMaintenance($idp);
    redirect('masterMaintenance?msg=Data Berhasil di Tambah');
  }

  public function updateArmada()
  {
    
  }

  public function showArmada()
  {
    
  }

  public function deleteArmada()
  {
    
  }


   
}
?>