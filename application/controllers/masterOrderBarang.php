<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class masterOrderBarang extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    //mengambil library session
     $this->load->library(array('session'));
     //mengambil url di helper
     $this->load->helper('url');
     // mengambil m_login yang berada di folder model yang tadi di buat
     $this->load->model('modelArmada');
     $this->load->model('modelEmployee');
     $this->load->model('modelOrderBarang');
     $this->load->model('permintaanBarang');
    //mengambil database
     $this->load->database();
      
      
  }
   
  // membuat fungsi index
  public function index()
  {
    //belum di approved atau minta approved
    $data['SPB']=$this->modelOrderBarang->listOrderBarang(); 
    $this->template->load('template','Pages/listOrderBarang',$data);
  }

  public function listOrderBarang()
  {
    //belum di approved atau minta approved
    $data['SPB']=$this->modelOrderBarang->listOrderBarangAll(); 
    $this->template->load('template','Pages/listOrderBarang',$data);
  }

  public function approvedSPB()
  {
    //sudah di approved
    $data['SPB']=$this->permintaanBarang->listPermintaanBarangApproved(); 
    $this->template->load('template','Pages/approveSPB',$data);
  } 

  public function tambahOrderBarang()
  {
    $data['armada']=$this->modelArmada->ambilArmada();
    $data['employee']=$this->permintaanBarang->ambilEmployee();
    $this->template->load('template','Pages/tambahOrderBarang',$data);
  }

  public function detailOrderBarang()
  {
    $data['SPB']=$this->modelOrderBarang->detailOrderBarang();
    $this->template->load('template','Pages/detailOrderBarang',$data);
  }

  public function simpanOrderBarang()
  {
    $idp=$this->session->userdata('emp_code');
    $this->modelOrderBarang->simpanOrderBarang($idp);
    redirect('masterOrderBarang?msg=Data Berhasil di Tambah');
  }

  public function approveOrderBarang()
  {
    //$idp=$this->session->userdata('emp_code');
    $this->modelOrderBarang->approveOrderBarang();
    redirect('masterOrderBarang?msg=Berhasil di Approve');
  }

  public function getItem()
  {
    
    $data=$_POST['labi'];

    $this->db->select('*');
    //$this->db->where('ii_merek',$data);
    $this->db->where('ii_stock',0);
    $result=$this->db->get('mst_inv_item');


    echo "<option value='0'>- Pilih User -</option>";
    foreach ($result->result_array() as $row) {
    echo "<option value=";
    echo $row['ii_id'];
    echo ">";
    echo $row['ii_code']." - ".$row['ii_name'];
    echo "</option>";
    };
  }

  public function Print()
  {
    $this->load->library('M_pdf');

    $mpdf = $this->m_pdf->load([
      'mode' => 'utf-8',
      'format' => 'A4'
    ]);
    $itle=$_GET['id'];
    $data['SPB']=$this->modelOrderBarang->detailOrderBarang();
    $view = $this->load->view('pages/printSOB',$data,true);
    
    $mpdf->SetHeader("PT. Arief Nusa Raya <br> Coal Hauling Project - TCM Melak Kutai Barat||$itle");
    $mpdf->SetTitle($itle);
    $mpdf->WriteHTML($view);

    $mpdf->Output();
  }
   
}
?>