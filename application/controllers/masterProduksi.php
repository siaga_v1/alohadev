<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class masterProduksi extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('modelProduksi');
      
  }
   
  // membuat fungsi index
  public function index()
  {
    $data['armada']=$this->modelProduksi->ambilArmada();
    $data['driver']=$this->modelProduksi->ambilDriver();
    $this->template->load('template','pages/tambahProduksi',$data);
  } 

  public function listProduksi()
  {
    $data['listProduksi']=$this->modelProduksi->listProduksi();
    $this->template->load('template','Pages/listProduksi',$data);
  }

  public function simpanProduksi()
  {
    $this->modelProduksi->simpanProduksi();
    redirect('masterProduksi?msg=Data Berhasil di Tambah');
  } 

  
}
?>