<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class masterPurchase extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();  
    $this->load->model('modelPurchase');
        
  }
   
  // membuat fungsi index
  public function index()
  {
    $data['SPB']=$this->modelPurchase->listOrderBarangApproved(); 
    $this->template->load('template','Pages/listPurcase',$data);
  } 

  public function detailPurchase()
  {
    $data['SPB']=$this->modelPurchase->detailPurchase();
    $this->template->load('template','Pages/detailPurchase',$data);
  }

  public function approvePurchase ()
  {
    //$idp=$this->session->userdata('emp_code');
    $this->modelPurchase->approvePurchase();
    redirect('masterPurchase?msg=Berhasil di Approve');
  }

  public function listPurcase()
  {
    //belum di approved atau minta approved
    $data['SPB']=$this->modelPurchase ->listPurcaseAll(); 
    $this->template->load('template','Pages/listPurcase',$data);
  }

  public function showArmada()
  {
    
  }

  public function deleteArmada()
  {
    
  }


   
}
?>