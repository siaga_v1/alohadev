<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class masterSupplier extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
     $this->load->model('modelSupplier');
      
  }
   
  // membuat fungsi index
  public function index()
  {
    $data['supplier']=$this->modelSupplier->ambilSupplier();
    $this->template->load('template','pages/listSupplier',$data);
  } 

  public function tambahSupplier()
  {
    $this->template->load('template','pages/tambahSupplier');
  }

  public function simpanSupplier()
  {
    $this->modelSupplier->simpanSupplier();
    redirect('masterSupplier?msg=Data Berhasil di Tambah');
  }

  public function showArmada()
  {
    
  }

  public function deleteArmada()
  {
    
  }


   
}
?>