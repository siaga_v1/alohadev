<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class monitoringKas extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    //mengambil library session
     $this->load->library(array('session'));
     //mengambil url di helper
     $this->load->helper('url');
     // mengambil m_login yang berada di folder model yang tadi di buat
     $this->load->model('m_login');
    //mengambil database
     $this->load->database();
      
      
  }
   
  // membuat fungsi index
  public function index()
  {
    $this->template->load('template','Pages/monitoringKas');
  } 

  public function tambahKas()
  {
    $this->template->load('template','Pages/tambahKas');
  } 
   
}
?>