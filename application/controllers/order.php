<?php if (!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class order extends CI_Controller
{
	// udah tau ini apa :p
	public function __construct()
	{
		parent::__construct();
		$this->load->model('orders');
	}

	public function index()
	{
		$data['order'] = $this->orders->getOrderMonth();

		$this->load->model('customers');
		$data['customer'] = $this->customers->getCustomer();

		$this->load->model('fleets');
		$data['fleet'] = $this->fleets->getFleets();
		$data['fleettypes'] = $this->fleets->getFleettypes();

		$this->load->model('drivers');
		$data['driver'] = $this->drivers->getDrivers();

		$this->load->model('cities');
		$data['cities'] = $this->cities->getCities();

		$this->load->model('ordersizeunits');
		$data['ordersizeunits'] = $this->ordersizeunits->getOrderSizeUnits();

		$this->load->model('ordertypes');
		$data['ordertypes'] = $this->ordertypes->getOrderTypes();

		$data['id'] = $this->orders->getLastID();

		$this->template->load('template', 'pages/orders', $data);
	}

	public function pareto()
	{
		$data['order'] = $this->orders->getOrderMonth();

		$this->load->model('customers');
		$data['customer'] = $this->customers->getCustomer();

		$this->load->model('fleets');
		$data['fleet'] = $this->fleets->getFleets();
		$data['fleettypes'] = $this->fleets->getFleettypes();

		$this->load->model('drivers');
		$data['driver'] = $this->drivers->getDrivers();

		$this->load->model('cities');
		$data['cities'] = $this->cities->getCities();

		$this->load->model('ordersizeunits');
		$data['ordersizeunits'] = $this->ordersizeunits->getOrderSizeUnits();

		$this->load->model('ordertypes');
		$data['ordertypes'] = $this->ordertypes->getOrderTypes();

		$data['id'] = $this->orders->getLastID();

		$this->template->load('template', 'pages/orderpareto', $data);
	}

	public function operational()
	{
		$data['order'] = $this->orders->getOrderOprs();

		$this->load->model('customers');
		$data['customer'] = $this->customers->getCustomer();

		$this->load->model('fleets');
		$data['fleet'] = $this->fleets->getFleets();
		$data['fleettypes'] = $this->fleets->getFleettypes();

		$this->load->model('drivers');
		$data['driver'] = $this->drivers->getDrivers();

		$this->load->model('cities');
		$data['cities'] = $this->cities->getCities();

		$this->load->model('ordersizeunits');
		$data['ordersizeunits'] = $this->ordersizeunits->getOrderSizeUnits();

		$this->load->model('ordertypes');
		$data['ordertypes'] = $this->ordertypes->getOrderTypes();

		$data['id'] = $this->orders->getLastID();

		$this->template->load('template', 'pages/orderoprs', $data);
	}

	public function spotOrder()
	{
		$data['order'] = $this->orders->getOrderSpot();

		$this->load->model('customers');
		$data['customer'] = $this->customers->getCustomer();

		$this->load->model('fleets');
		$data['fleet'] = $this->fleets->getFleets();
		$data['fleettypes'] = $this->fleets->getFleettypes();

		$this->load->model('drivers');
		$data['driver'] = $this->drivers->getDrivers();

		$this->load->model('cities');
		$data['cities'] = $this->cities->getCities();

		$this->load->model('ordersizeunits');
		$data['ordersizeunits'] = $this->ordersizeunits->getOrderSizeUnits();

		$this->load->model('ordertypes');
		$data['ordertypes'] = $this->ordertypes->getOrderTypes();

		$data['id'] = $this->orders->getLastID();

		$this->template->load('template', 'pages/orderspot', $data);
	}

	public function tessst()
	{
		$setting = $_POST['sett'];
		$origin = $_POST['origin'];
		$destination = $_POST['dest'];
		$moda = $_POST['moda'];
		$cust = $_POST['cust'];

		if ($setting == "drv") {
			$this->db->select('*');
			$this->db->where('active', '1');
			$result = $this->db->get('drivers');
			foreach ($result->result_array() as $row) {
				echo "<option></option>";
				echo "<option value=" . $row['id'] . ">" . $row['name'] . "</option>";
			}
		}
		if ($setting == "flt") {
			$this->db->select('*');
			$this->db->where('active', '1');
			$result = $this->db->get('fleets');
			foreach ($result->result_array() as $row) {
				echo "<option></option>";
				echo "<option value=" . $row['id'] . ">" . $row['fleetplateno'] . "</option>";
			}
		}

		if ($setting == 'ujs') {

			$this->db->select('*');
			$this->db->where('id_citieso', $origin);
			$this->db->where('id_citiesd', $destination);
			$this->db->where('id_customers', $cust);
			$this->db->where('id_ordertypes', $moda);
			$this->db->where('active', '1');
			$result = $this->db->get('routes');

			foreach ($result->result() as $row) {
				echo $row->ujs;
			}
		}
		if ($setting == 'unp') {

			$this->db->select('*');
			$this->db->where('id_citieso', $origin);
			$this->db->where('id_citiesd', $destination);
			$this->db->where('id_customers', $cust);
			$this->db->where('id_ordertypes', $moda);
			$this->db->where('active', '1');
			$result = $this->db->get('routes');

			foreach ($result->result() as $row) {
				echo $row->allowance;
			}
		}


		//$data['fleettypes']=$this->fleets->getFleettypes();

		//$this->load->model('drivers');
		//$data['driver']=$this->drivers->getDrivers();


		//echo json_encode($data);
	}

	public function orderlist()
	{
		$this->template->load('template', 'pages/orderall');
	}

	public function orderlistFinance()
	{
		$this->template->load('template', 'pages/orderallfinance');
	}

	public function orderlistInput()
	{
		$this->template->load('template', 'pages/orderallinput');
	}

	public function approveOrder()
	{
		$this->template->load('template', 'pages/approveOrder');
	}

	public function loadRecordInput($page = 0)

	{
		$perpage = 150;

		$data = array();
		$filter = array();
		$data['search'] = array();

		$limit  = $perpage;
		$offset = $page > 1 ? ($limit * $page) - $limit : 0;

		$all_data =  $this->orders->getOrderInput($filter);
		$data['total'] = count($all_data);
		$config['base_url'] = base_url() . '/order/loadRecordInput';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $data['total'];
		$config['per_page'] = $perpage;
		$config['num_links'] = 1;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['anchor_class'] = 'follow_link';

		$this->pagination->initialize($config);
		$paginator = $this->pagination->create_links();
		$data['pagination'] = $paginator;

		//$data["page"] = $page;
		///$data["offset"] = $offset;
		//$data["limit"] = $limit;

		$filter["limit"]  = $limit;
		$filter["page"] = $page;
		$filter["offset"] = $offset;

		$data["result"] = $this->orders->getOrderInput($filter);
		$data["row"] = $offset;
		echo json_encode($data);
	}

	public function loadRecord($page = 0)

	{
		$perpage = 50;

		$data = array();
		$filter = array();
		$data['search'] = array();

		$limit  = $perpage;
		$offset = $page > 1 ? ($limit * $page) - $limit : 0;

		$all_data =  $this->orders->getOrder($filter);
		$data['total'] = count($all_data);
		$config['base_url'] = base_url() . '/order/loadRecord';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $data['total'];
		$config['per_page'] = $perpage;
		$config['num_links'] = 1;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['anchor_class'] = 'follow_link';

		$this->pagination->initialize($config);
		$paginator = $this->pagination->create_links();
		$data['pagination'] = $paginator;

		//$data["page"] = $page;
		///$data["offset"] = $offset;
		//$data["limit"] = $limit;

		$filter["limit"]  = $limit;
		$filter["page"] = $page;
		$filter["offset"] = $offset;

		$data["result"] = $this->orders->getOrder($filter);
		$data["row"] = $offset;
		echo json_encode($data);
	}

	public function loadRecordApprove($page = 0)
	{
		$perpage = 50;

		$data = array();
		$filter = array();
		$data['search'] = array();

		$limit  = $perpage;
		$offset = $page > 1 ? ($limit * $page) - $limit : 0;

		$all_data =  $this->orders->getOrderApprove($filter);
		$data['total'] = count($all_data);
		$config['base_url'] = base_url() . '/order/loadRecord';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $data['total'];
		$config['per_page'] = $perpage;
		$config['num_links'] = 1;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['anchor_class'] = 'follow_link';

		$this->pagination->initialize($config);
		$paginator = $this->pagination->create_links();
		$data['pagination'] = $paginator;

		//$data["page"] = $page;
		///$data["offset"] = $offset;
		//$data["limit"] = $limit;

		$filter["limit"]  = $limit;
		$filter["page"] = $page;
		$filter["offset"] = $offset;

		$data["result"] = $this->orders->getOrderApprove($filter);
		$data["row"] = $offset;
		echo json_encode($data);
	}

	public function getPriceRoutes()
	{
		$origin = $_POST['citieso'];
		$destination = $_POST['citiesd'];
		$types = $_POST['tipes'];
		$cust = $_POST['cust'];
		$abc = $_POST['sett'];

		$this->db->select('a.*, b.name as citieso,c.name as citiesd');
		$this->db->join('cities b', 'a.id_citieso = b.id', 'left');
		$this->db->join('cities c', 'a.id_citiesd = c.id', 'left');
		$array = array('a.id_citieso' => $origin, 'a.id_citiesd' => $destination, 'a.id_ordertypes' => $types, 'a.active' => 1, 'a.id_customers' => $cust);
		$this->db->where($array);
		$result = $this->db->get('routes a');
		if ($abc == "ujs") {
			foreach ($result->result_array() as $row) {
				if (is_null($row['code'])) {
					echo "Kosong Dong";
				} else {
					echo $row['ujs'];
				}
			}
		}
		if ($abc == "unit") {
			foreach ($result->result_array() as $row) {
				if (is_null($row['code'])) {
					echo "Kosong Dong";
				} else {
					echo $row['allowance'];
				}
			}
		}
		if ($abc == "option") {
			foreach ($result->result_array() as $row) {
				if (is_null($row['code'])) {
					echo "Kosong Dong";
				} else {
					echo "<option value=''></option>  
	                  	  <option value='" . $row['id'] . "'>" . $row['code'] . " | Rp." . number_format($row['ujs']) . " | " . $row['citieso'] . " To " . $row['citiesd'] . "</option>";
				}
			}
		}
	}

	public function getFleetType()
	{
		$this->db->select('*');
		//$this->db->join('fleettypes b','a.id_fleettypes = b.id');
		$this->db->where('active', 1);
		$result = $this->db->get('fleettypes');
		foreach ($result->result_array() as $row) {
			if (is_null($row['id'])) {
				echo "Kosong Dong";
			} else {
				echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
			}
		}
	}

	public function getDestination()
	{
		$origin = $_POST['citieso'];
		$types = $_POST['tipes'];
		$cust = $_POST['cust'];


		$this->db->select('a.id_citiesd, b.name as destination');
		$this->db->join('cities b', 'a.id_citiesd = b.id');
		$this->db->where('a.id_citieso', $origin);
		$this->db->where('a.id_customers', $cust);
		$this->db->where('a.id_ordertypes', $types);
		$this->db->group_by(array('id_citiesd', 'b.name'));
		$result = $this->db->get('routes a');

		echo "<option></option>";
		foreach ($result->result_array() as $row) {

			if (is_null($row['destination'])) {
				echo "Kosong Dong";
			} else {
				echo "<option value='" . $row['id_citiesd'] . "'>" . $row['destination'] . "</option>";
			}
		}
	}

	public function getOrigin()
	{
		$cust = $this->input->post('cust');
		$types = $this->input->post('tipes');


		$sql = ("SELECT
				dbo.cities.id,
				dbo.cities.name 
			FROM
				dbo.cities
				INNER JOIN dbo.routes ON dbo.cities.id = dbo.routes.id_citieso 
			WHERE
				dbo.routes.id_customers = '$cust'
				AND	
				dbo.routes.id_ordertypes = '$types'
				AND dbo.cities.active = 1 
			GROUP BY
				dbo.cities.id,
				dbo.cities.name");
		$result = $this->db->query($sql);
		echo "<option></option>";
		foreach ($result->result_array() as $row) {

			echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
		}
	}

	public function getPricebyRoutes()
	{
		$route = $_POST['route'];
		$setting = $_POST['sett'];

		$this->db->select('*');
		$this->db->where('id', $route);
		$result = $this->db->get('routes');

		if ($setting == "ujs") {
			foreach ($result->result() as $row) {
				echo $row->ujs;
			}
		}
		if ($setting == "unit") {
			foreach ($result->result() as $row) {
				echo $row->allowance;
			}
		}
		if ($setting == "wash") {
			foreach ($result->result() as $row) {
				echo $row->feewash;
			}
		}
		if ($setting == "saving") {
			foreach ($result->result() as $row) {
				echo $row->feesaving;
			}
		}
	}

	public function saveEdit()
	{
		$this->orders->editOrders();
		redirect('order?msg=Save Success');
	}

	public function saveEditspld()
	{
		$this->orders->editOrderspld();
		redirect('order/orderlist?msg=Save Success');
	}

	public function saveEditspot()
	{
		$this->orders->editOrderspot();
		redirect('order/spotOrder?msg=Save Success');
	}

	public function saveEditFinance()
	{
		$this->orders->editOrderspld();
		redirect('order/orderlistFinance?msg=Save Success');
	}

	public function modaledit()
	{
		$data['order'] = $this->orders->getOneOrder();

		$data['orderload'] = $this->orders->getOrderload();

		$this->load->model('customers');
		$data['customer'] = $this->customers->getCustomer();

		$this->load->model('fleets');
		$data['fleet'] = $this->fleets->getFleets();
		$data['fleettypes'] = $this->fleets->getFleettypes();

		$this->load->model('drivers');
		$data['driver'] = $this->drivers->getDrivers();

		$this->load->model('cities');
		$data['cities'] = $this->cities->getCities();

		$this->load->model('routes');
		$data['routes'] = $this->routes->getRoutes();

		$this->load->model('ordertypes');
		$data['ordertypes'] = $this->ordertypes->getOrderTypes();


		$this->load->view('pages/modaledit', $data);
	}

	public function modaleditspld()
	{
		$data['order'] = $this->orders->getOneOrder();

		$data['orderload'] = $this->orders->getOrderload();
		$data['additional'] = $this->orders->getAdditionalCost();
		$data['adds'] = $this->orders->adds();
		$data['addss'] = $this->orders->addss();

		$this->load->model('customers');
		$data['customer'] = $this->customers->getCustomer();

		$this->load->model('fleets');
		$data['fleet'] = $this->fleets->getFleets();
		$data['fleettypes'] = $this->fleets->getFleettypes();

		$this->load->model('drivers');
		$data['driver'] = $this->drivers->getDrivers();

		$this->load->model('cities');
		$data['cities'] = $this->cities->getCities();


		$this->load->model('routes');
		$data['routes'] = $this->routes->getRoutes();

		$this->load->model('ordertypes');
		$data['ordertypes'] = $this->ordertypes->getOrderTypes();


		$this->load->view('pages/modaleditspld', $data);
	}

	public function modaleditfinance()
	{
		$data['order'] = $this->orders->getOneOrder();

		$data['orderload'] = $this->orders->getOrderload();
		$data['additional'] = $this->orders->getAdditionalCost();
		$data['adds'] = $this->orders->adds();
		$data['addss'] = $this->orders->addss();

		$this->load->model('customers');
		$data['customer'] = $this->customers->getCustomer();

		$this->load->model('fleets');
		$data['fleet'] = $this->fleets->getFleets();
		$data['fleettypes'] = $this->fleets->getFleettypes();

		$this->load->model('drivers');
		$data['driver'] = $this->drivers->getDrivers();

		$this->load->model('cities');
		$data['cities'] = $this->cities->getCities();


		$this->load->model('routes');
		$data['routes'] = $this->routes->getRoutes();

		$this->load->model('ordertypes');
		$data['ordertypes'] = $this->ordertypes->getOrderTypes();


		$this->load->view('pages/modaleditfinance', $data);
	}


	public function modaleditoprs()
	{
		$data['order'] = $this->orders->getOneOprs();

		$data['orderload'] = $this->orders->getLoadOprs();
		$data['additional'] = $this->orders->getAdditionalOprs();
		$data['adds'] = $this->orders->addsOprs();
		$data['addss'] = $this->orders->addssOprs();

		$this->load->model('customers');
		$data['customer'] = $this->customers->getCustomer();

		$this->load->model('fleets');
		$data['fleet'] = $this->fleets->getFleets();
		$data['fleettypes'] = $this->fleets->getFleettypes();

		$this->load->model('drivers');
		$data['driver'] = $this->drivers->getDrivers();

		$this->load->model('cities');
		$data['cities'] = $this->cities->getCities();


		$this->load->model('routes');
		$data['routes'] = $this->routes->getRoutes();

		$this->load->model('ordertypes');
		$data['ordertypes'] = $this->ordertypes->getOrderTypes();


		$this->load->view('pages/modaleditoprs', $data);
	}

	public function modalEditSpot()
	{
		$data['order'] = $this->orders->getOneOrder();

		$data['orderload'] = $this->orders->getOrderload();
		$data['additional'] = $this->orders->getAdditionalCost();
		$data['adds'] = $this->orders->adds();
		$data['addss'] = $this->orders->addss();

		$this->load->model('customers');
		$data['customer'] = $this->customers->getCustomer();

		$this->load->model('fleets');
		$data['fleet'] = $this->fleets->getFleets();
		$data['fleettypes'] = $this->fleets->getFleettypes();

		$this->load->model('drivers');
		$data['driver'] = $this->drivers->getDrivers();

		$this->load->model('cities');
		$data['cities'] = $this->cities->getCities();


		$this->load->model('routes');
		$data['routes'] = $this->routes->getRoutes();

		$this->load->model('ordertypes');
		$data['ordertypes'] = $this->ordertypes->getOrderTypes();


		$this->load->view('pages/modalEditSpot', $data);
	}

	public function modaldetail()
	{
		$data['order'] = $this->orders->getOneOrder();

		$data['orderload'] = $this->orders->getOrderload();
		$data['additional'] = $this->orders->getAdditionalCost();
		$data['adds'] = $this->orders->adds();
		$data['addss'] = $this->orders->addss();

		$this->load->model('customers');
		$data['customer'] = $this->customers->getCustomer();

		$this->load->model('fleets');
		$data['fleet'] = $this->fleets->getFleets();
		$data['fleettypes'] = $this->fleets->getFleettypes();

		$this->load->model('drivers');
		$data['driver'] = $this->drivers->getDrivers();

		$this->load->model('cities');
		$data['cities'] = $this->cities->getCities();


		$this->load->model('routes');
		$data['routes'] = $this->routes->getRoutes();

		$this->load->model('ordertypes');
		$data['ordertypes'] = $this->ordertypes->getOrderTypes();


		$this->load->view('pages/modaldetail', $data);
	}

	public function modalApprove()
	{
		$data['order'] = $this->orders->getOneOrder();

		$data['orderload'] = $this->orders->getOrderload();

		$this->load->model('customers');
		$data['customer'] = $this->customers->getCustomer();


		$this->load->model('routes');
		$data['routes'] = $this->routes->getRoutes();

		$this->load->model('fleets');
		$data['fleet'] = $this->fleets->getFleets();
		$data['fleettypes'] = $this->fleets->getFleettypes();

		$this->load->model('drivers');
		$data['driver'] = $this->drivers->getDrivers();

		$this->load->model('cities');
		$data['cities'] = $this->cities->getCities();

		$this->load->model('ordertypes');
		$data['ordertypes'] = $this->ordertypes->getOrderTypes();

		$this->load->view('pages/modalApprove', $data);
	}

	public function saveOrders()
	{
		$this->orders->Simpan();
		redirect('order?msg=Save Success');
	}

	public function savePareto()
	{
		$return['type'] = 'success';
		$return['title'] = 'Update success!';
		$return['message'] = 'Update data invoice success!';

		$this->form_validation->set_error_delimiters("<div class='alert alert-danger'><strong>Error ! </strong>", "</div>");
		$this->form_validation->set_rules('orderdate', 'Order Date', 'trim|required');
		$this->form_validation->set_rules('id_customers', 'Customer Name', 'trim|required');
		$this->form_validation->set_rules('id_fleets', 'Fleet', 'trim|required');
		$this->form_validation->set_rules('id_drivers', 'Driver Name', 'trim|required');
		$this->form_validation->set_rules('price', 'Prices', 'trim|required');
		$this->form_validation->set_rules('allowance', 'Allowances', 'trim|required');

		if ($this->form_validation->run()) {
			$this->db->select("COUNT(*) AS total");
			$this->db->where("active", 1);
			$this->db->like("code", $this->input->post("code"));
			$result = $this->db->get("orders");
			if ($result->row()->total == 0) {
				if (!$this->orders->simpanParetoBeta()) {
					$return['type'] = 'error';
					$return['title'] = 'Update failed!';
					$return['message'] = 'Save order error, Internal server ersror!';
				}
			} else {
				$return['type'] = 'error';
				$return['title'] = 'Update failed!';
				$return['message'] = 'Shipment already exist!';
			}
			//
			//redirect('order/pareto?msg=Save Success');
		} else {
			$return['type'] = 'error';
			$return['title'] = 'Update failed!';
			$return['message'] = validation_errors();
		}
		//echo json_encode($return);
		redirect('order/pareto?msg=Save Success');
	}

	public function saveOprs()
	{
		$this->orders->simpanOprsBeta();
		redirect('order/operational?msg=Save Success');
	}

	public function saveSpot()
	{
		$this->orders->simpanSpotBeta();
		redirect('order/spotOrder?msg=Save Success');
	}

	public function printSpot()
	{
		$this->load->library('M_pdf');

		$mpdf = $this->m_pdf->load([
			'mode' => 'utf-8',
			'format' => 'A4'
		]);


		$data['spot'] = $this->orders->printSpot();

		$this->load->model('customers');
		$data['customer'] = $this->customers->getCustomer();


		$this->load->model('cities');
		$data['cities'] = $this->cities->getCities();

		$view = $this->load->view('pages/printSpot', $data, true);

		$mpdf->WriteHTML($view);

		$mpdf->Output();
	}

	public function deleteOrder()
	{
		$this->orders->delete();
		redirect('order?msg=Delete Success');
	}

	public function deleteOrderSpot()
	{
		$this->orders->delete();
		redirect('order/spotOrder??msg=Delete Success');
	}

	public function delAdditional()
	{
		$id = $_POST['id'];

		$data['active']     = 0;

		$this->db->where('id', $id);
		$this->db->delete('cost');

		echo "YES";
	}

	public function delLoadUnit()
	{
		$id = $_POST['id'];
		$rute = $_POST['rute'];

		//$data['irisno']     = 'B4ISA';

		$this->db->where('order_code', $id);
		$this->db->where('id_routes', $rute);
		$this->db->delete('orderload');

		echo "YES";
	}

	public function exportExcel($value = '')
	{
		if ($this->input->get('export') == 'excel') {

			$this->load->library('PHPExcel');
			$data['search'] = array();
			$rows = $this->orders->getOrder($data);

			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
				->setCreator('TOTAL KILAT')
				->setTitle('TOTAL KILAT')
				->setLastModifiedBy('SYSTEM')
				->setDescription('TOTAL KILAT')
				->setSubject('TOTAL KILAT')
				->setKeywords('TOTAL KILAT')
				->setCategory('EXPORT EXCEL');

			$ews = $objPHPExcel->getSheet(0);
			$ews->setTitle('Data');

			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
			// Assign cell values
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'All Order List');
			$objPHPExcel->getActiveSheet()->mergeCells('A1:T1');
			$objPHPExcel->getActiveSheet()->getStyle("A1:T1")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getFill()->getStartColor()->setARGB('ffffcc');
			$objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setSize(16);



			$filtertext = "";
			if ($this->input->get('fleetplateno') != '') {
				$filtertext .= "Plateno = " . $this->input->get('fleetplateno');
			}
			if ($this->input->get('type') != '') {
				$filtertext .= " Type = " . $this->input->get('type');
			}
			if ($this->input->get('customer') != '') {
				$filtertext .= " Customer = " . $this->input->get('customer');
			}
			if ($this->input->get('code') != '') {
				$filtertext .= " Fleet Code = " . $this->input->get('code');
			}
			if ($this->input->get('year') != '') {
				$filtertext .= " Year = " . $this->input->get('year');
			}
			if ($this->input->get('month') != '') {
				$filtertext .= " Month = " . date('F', strtotime("2018-" . $this->input->get('month') . "-01"));
			}

			if ($this->input->get('Koordinator') != '') {
				$filtertext .= " Koordinator = " . $this->input->get('Koordinator');
			}
			$filters = ($filtertext != "" ? $filtertext : "All Data");

			$objPHPExcel->getActiveSheet()->mergeCells('A2:T2');
			$objPHPExcel->getActiveSheet()->getStyle("A2:T2")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A2:T2')->getFill()->getStartColor()->setARGB('eaeaea');
			$objPHPExcel->getActiveSheet()->getStyle("A2:T2")->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Filter : " . $filters);

			$objPHPExcel->getActiveSheet()->getStyle("A3:T3")->getFont()->setBold(false);
			$objPHPExcel->getActiveSheet()->getStyle('A3:T3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A3:T3')->getFill()->getStartColor()->setARGB('cce6ff');
			$objPHPExcel->getActiveSheet()->getStyle("A3:T3")->getFont()->setSize(12);

			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'CODE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, 'VCR');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, 'TRIP REFERENCE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'Doc No');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3, 'DATE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 3, 'CUSTOMER');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 3, 'RITASE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 3, 'DRIVER');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 3, 'TRUCK');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 3, 'TYPE UNIT');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 3, 'NO RANGKA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 3, 'NO MESIN');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 3, 'NO IRIS');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 3, 'ALAMAT');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 3, 'CUST CODE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, 3, 'CASE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, 3, 'WARNA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, 3, 'ORIGIN');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, 3, 'DESTINATION');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, 3, 'UNIT PRICE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, 3, 'SELISIH');

			if ($rows != null) {
				$prices = 0;
				$ritase = 0;
				$allowance = 0;
				$allowanceadds = 0;
				$allowancereds = 0;
				$no  = 1;
				$baris = 4;
				$codes = "kosong";
				$nomine2 = "kosong";
				$idol = "kosong";
				foreach ($rows as $row) {


					//	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $row->code);
					//	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->orderdate);
					//    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->cname);
					//    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->ritase);
					//    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->driver);
					//    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris, $row->fleet);

					//    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $baris, $row->n1);
					//    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $baris, $row->c2);


					//    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $baris, $row->prices);
					//     $prices += $row->prices;
					//}



					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $row->idol);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->noVoucher);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->shippment);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->noshippent);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->orderdate);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris, $row->cname);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $baris, $row->driver);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $baris, $row->fleet);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $baris, $row->type);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $baris, $row->frameno);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $baris, $row->machineno);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $baris, $row->irisno);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $baris, $row->address);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $baris, $row->custcode);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $baris, $row->case);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $baris, $row->color);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $baris, $row->n1);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $baris, $row->c2);
					if ($row->cname == "TOYOTA ASTRA MOTOR" || $row->cname == "TOYOTA ASTRA MOTOR PARETO") {
						if ($row->code != $codes) {
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $baris, $row->allowances);
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $baris, $row->ritase);
							//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $baris, $row->cost_id);

						} else if ($row->custcode == $row->ccost) {
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $baris, $row->nominal2);
						} else {



							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $baris, 0);
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $baris, 0);
						}


						$prices += $row->allowances;
					} else {

						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $baris, $row->ritase);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $baris, $row->unitprice);
						//if ($row->c2 == 'PALEMBANG') {
						//    			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $baris, $row->nominal2);
						//        	}
						$idtest = $row->code;
						$testqwe = 'beda';


						$this->db->select('a.*, b.name as an');
						$this->db->join('costcomponents b', 'a.cost_id = b.id');
						$this->db->where('a.order_id', $idtest);
						$result = $this->db->get('cost a');

						foreach ($result->result_array() as $key) {
							//if ($key['cost_id'] == 19 || $key['cost_id'] == 20 || $key['cost_id'] == 21 || $key['cost_id'] == 26) {
							//if (!empty($key->nominal2)) {
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $baris, $key['nominal2']);
						}
						//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $baris, $row->cost_id);
						$prices += $row->unitprice;
					}
					/*$idtest = $row->code;

		                	$this->db->select('a.*, b.name as an');
	            			$this->db->join('costcomponents b','a.cost_id = b.id');
				            $this->db->where('a.order_id', $idtest);
				            $result = $this->db->get('cost a');

				          	foreach ($result->result_array() as $key ) {
				                			//if ($key['cost_id'] == 19 || $key['cost_id'] == 20 || $key['cost_id'] == 21 || $key['cost_id'] == 26) {
				                			//if (!empty($key->nominal2)) {
				                			if ($row->custcode==$key['an']) {
				                			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $baris, $key['cost_id']);
				          			# code...
				          		}else{
				                		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $baris, 0);
				          			
				          		}
				          	}*/

					$codes = $row->code;
					//$nomine2= $row->cost_id;
					$ritase += $row->ritase;


					$no++;
					$baris++;
				}
				//$idol= $row->idol;
				//}

				$barisfooter = $baris;
				$barisbaru = $barisfooter - 1;
				$objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $barisfooter, $ritase);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $barisfooter, $prices);
			} else {
				$objPHPExcel->getActiveSheet()->setCellValue('A4:I4', 'Data Not Found');
				$objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('e6f2ff');
				$objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(14);
			}




			$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

			//$writer->setIncludeCharts(true);
			//$writer->save('output.xlsx');
			// Save it as an excel 2003 file
			//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			//Nama File
			header('Content-Disposition: attachment;filename="TruckOrderSummary' . date('dmy') . '.xlsx"');
			$writer->save('php://output');
		}
	}

	public function exportExcelAccounting($value = '')
	{
		if ($this->input->get('export') == 'excel') {

			$this->load->library('PHPExcel');
			$data['search'] = array();
			$rows = $this->orders->getOrderReport($data);

			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
				->setCreator('TOTAL KILAT')
				->setTitle('TOTAL KILAT')
				->setLastModifiedBy('SYSTEM')
				->setDescription('TOTAL KILAT')
				->setSubject('TOTAL KILAT')
				->setKeywords('TOTAL KILAT')
				->setCategory('EXPORT EXCEL');

			$ews = $objPHPExcel->getSheet(0);
			$ews->setTitle('Data');

			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
			// Assign cell values
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Export All Order List');
			$objPHPExcel->getActiveSheet()->mergeCells('A1:U1');
			$objPHPExcel->getActiveSheet()->getStyle("A1:U1")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A1:U1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A1:U1')->getFill()->getStartColor()->setARGB('ffffcc');
			$objPHPExcel->getActiveSheet()->getStyle("A1:U1")->getFont()->setSize(16);



			$filtertext = "";
			if ($this->input->get('fleetplateno') != '') {
				$filtertext .= "Plateno = " . $this->input->get('fleetplateno');
			}
			if ($this->input->get('type') != '') {
				$filtertext .= " Type = " . $this->input->get('type');
			}
			if ($this->input->get('customer') != '') {
				$filtertext .= " Customer = " . $this->input->get('customer');
			}
			if ($this->input->get('code') != '') {
				$filtertext .= " Fleet Code = " . $this->input->get('code');
			}
			if ($this->input->get('year') != '') {
				$filtertext .= " Year = " . $this->input->get('year');
			}
			if ($this->input->get('month') != '') {
				$filtertext .= " Month = " . date('F', strtotime("2018-" . $this->input->get('month') . "-01"));
			}

			if ($this->input->get('Koordinator') != '') {
				$filtertext .= " Koordinator = " . $this->input->get('Koordinator');
			}
			$filters = ($filtertext != "" ? $filtertext : "All Data");

			$objPHPExcel->getActiveSheet()->mergeCells('A2:U2');
			$objPHPExcel->getActiveSheet()->getStyle("A2:U2")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A2:U2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A2:U2')->getFill()->getStartColor()->setARGB('eaeaea');
			$objPHPExcel->getActiveSheet()->getStyle("A2:U2")->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Filter : " . $filters);

			$objPHPExcel->getActiveSheet()->getStyle("A3:U3")->getFont()->setBold(false);
			$objPHPExcel->getActiveSheet()->getStyle('A3:U3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A3:U3')->getFill()->getStartColor()->setARGB('cce6ff');
			$objPHPExcel->getActiveSheet()->getStyle("A3:U3")->getFont()->setSize(12);

			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'No');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, 'Tanggal');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, 'Armada');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'Supir');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3, 'Customer');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 3, 'Pabrik');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 3, 'Tujuan');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 3, 'Feet');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 3, 'Sales');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 3, 'Uang Jalan');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 3, 'Biaya Operasional');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 3, 'LOLO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 3, 'DEPO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 3, 'Parkir');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 3, 'Kawalan');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, 3, 'Storage');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, 3, 'RC');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, 3, 'Seal');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, 3, 'Alih Kapal');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, 3, 'Cuci Kontainer');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, 3, 'SP2');
			if ($rows != null) {
				$pricesw = 0;
				$ritase = 0;
				$allowance = 0;
				$allowanceadds = 0;
				$allowancereds = 0;
				$no  = 1;
				$baris = 4;
				$codes = "kosong";
				foreach ($rows as $row) {
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->orderdate); //VCR O
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->fleetplateno); //TGL O
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->driver); //SUPIR O
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->cname); //NOPOL O
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris, $row->n1); //DEST O
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $baris, $row->c2); //ORIGINO
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $baris, $row->name); //DISTANCEO
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $baris, $row->prices); //GP/SALESO
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $baris, $row->allowances); //)
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $baris, $row->OPS > 0 ? $row->OPS : 0); //0
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $baris, $row->LOLO > 0 ? $row->LOLO : 0);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $baris, $row->DEPO > 0 ? $row->DEPO : 0);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $baris, $row->PARKIR > 0 ? $row->PARKIR : 0);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $baris, $row->KAWALAN > 0 ? $row->KAWALAN : 0); //feewash * loadqty
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $baris, $row->STORAGE > 0 ? $row->STORAGE : 0);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $baris, $row->RC > 0 ? $row->RC : 0);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $baris, $row->SEAL > 0 ? $row->SEAL : 0); //distance * fleet type
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $baris, $row->ALIHKAPAL > 0 ? $row->ALIHKAPAL : 0); //
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $baris, $row->CUCI > 0 ? $row->CUCI : 0); //ujs + uang titip
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $baris, $row->SP2 > 0 ? $row->SP2 : 0); //GP/SALES
					$pricesw += $row->allowances;



					$codes = $row->code;
					$ritase += $row->ritase;


					$no++;
					$baris++;
				}

				$barisfooter = $baris;
				$barisbaru = $barisfooter - 1;
				$objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(23, $barisfooter, $ritase);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $barisfooter, $pricesw);
			} else {
				$objPHPExcel->getActiveSheet()->setCellValue('A4:I4', 'Data Not Found');
				$objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('e6f2ff');
				$objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(14);
			}




			$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

			//$writer->setIncludeCharts(true);
			//$writer->save('output.xlsx');
			// Save it as an excel 2003 file
			//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			//Nama File
			header('Content-Disposition: attachment;filename="Laporan Customer ' . date('dmy') . '.xlsx"');
			$writer->save('php://output');
		}
	}
}
