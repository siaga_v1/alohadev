<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class orderclaim extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
    {
      parent::__construct();
     $this->load->model('orderclaims');
        
    }
   
  // membuat fungsi index
  public function index()
    {
      $data['claim']=$this->orderclaims->getClaim();
      $data['id']=$this->orderclaims->getLastID();

      $this->template->load('template','pages/claim', $data);
    } 

  public function deskripsi()
    {
        $check = $this->input->post('setting');
        $code  = $this->input->post('code');

       /* if ($check == "unit") {

            $this->db->select('*');
            $this->db->where('order_code', $code);
            $result = $this->db->get('orderload')->result();
            echo "<option>- -</option>";
            foreach ($result as $key) {
                echo "<option value='".$key->id."'>".$key->type." | Frame No : ".$key->frameno." | Machine No : ".$key->machineno."</option>";
            }
        }
       */
        

          $this->db->select('a.*, h.code as ordercode, b.fleetnumber as fleetnum, c.name as drivername, d.name as cusname, e.name as oriname, f.name as desname, g.name as fleetname');

          $this->db->join('orders h','a.order_code = h.code','left');
          $this->db->join('fleets b','h.id_fleets = b.id','left');
          $this->db->join('drivers c','h.id_drivers = c.id','left');
          $this->db->join('customers d','h.id_customers = d.id','left');
          $this->db->join('cities e','h.id_citieso = e.id','left');
          $this->db->join('cities f','h.id_citiesd = f.id','left');
          $this->db->join('fleettypes g','h.id_ordertypes = g.id','left');
          $this->db->where('a.id', $code);
          $result = $this->db->get('orderload a')->result();

          if ($check == "unit") {
            foreach ($result as $key) {
              echo $key->ordercode;
            }
          }

          if ($check == "customer") {
            foreach ($result as $key) {
              echo $key->cusname;
            }
          }

          if ($check == "model") {
            foreach ($result as $key) {
              echo $key->type;
            }
          }

          if ($check == "citieso") {
            foreach ($result as $key) {
              echo $key->oriname;
            }
          }

          if ($check == "citiesd") {
            foreach ($result as $key) {
              echo $key->desname;
            }
          }
          if ($check == "fleet") {
            foreach ($result as $key) {
              echo $key->fleetnum;
            }
          }
          if ($check == "driver") {
            foreach ($result as $key) {
              echo $key->drivername;
            }
          }
          if ($check == "harga") {
            foreach ($result as $key) {
              echo $key->harga;
            }
          }
          if ($check == "type") {
            foreach ($result as $key) {
              echo $key->fleetname;
            }
          }
          if ($check == "jenis") {
            foreach ($result as $key) {
              echo $key->type;
            }
          }
          if ($check == "color") {
            foreach ($result as $key) {
              echo $key->color;
            }
          }
        

    }

  public function getOption()
    {
        $check = $this->input->post('setting');
       
        if ($check == "damage") {

            $this->db->select('*');
            $result = $this->db->get('claimdamage')->result();
            echo "<option>- -</option>";
            foreach ($result as $key) {
                echo "<option value='".$key->id."' >".$key->name."</option>";
            }
        }

        if ($check == "type") {

            $this->db->select('*');
            $result = $this->db->get('claimtypes')->result();
            echo "<option>- -</option>";
            foreach ($result as $key) {
                echo "<option value='".$key->id."' >".$key->name."</option>";
            }
        }

    }

  public function getDetail(){
    $check  = $this->input->post('setting');
    $code   = $this->input->post('code');
    //$code= $_GET['code'];
    $this->db->select('a.*,b.name as citieso,c.name as citiesd, d.name as customer, g.fleetplateno as fleet, h.name as driver, i.allowance as harga, j.name as type');
    $this->db->join('cities b','a.id_citieso = b.id', 'left');
    $this->db->join('cities c','a.id_citiesd = c.id');
    $this->db->join('customers d','a.id_customers = d.id', 'left');
    $this->db->join('fleets g','a.id_fleets = g.id', 'left');
    $this->db->join('drivers h','a.id_drivers = h.id', 'left');
    $this->db->join('routes i','a.id_routes = i.id', 'left');
    $this->db->join('fleettypes j','a.id_ordertypes = j.id', 'left');
    $this->db->where('a.active', '1');
    $this->db->where('a.code', $code);
    $result = $this->db->get('orders a')->result();
    /*
        $sql = "
            SELECT
            customers.name AS cusname, 
            cities.name AS citiname,
            ordertypes.name AS typename,
            fleets.fleetplateno AS fleetname,
            drivers.name AS drivername,
            routes.allowance
            FROM
            orders
            LEFT JOIN customers ON orders.id_customers = customers.id
            LEFT JOIN cities ON orders.id_citieso = cities.id
            LEFT JOIN ordertypes ON orders.id_ordertypes = ordertypes.id
            LEFT JOIN routes ON orders.id_routes = routes.id
            LEFT JOIN fleets ON orders.id_fleets = fleets.id
            LEFT JOIN drivers ON orders.id_drivers = drivers.id
            WHERE
            orders.id = '".$code."'"; 
        
        $result = $this->db->query($sql)->result();
    */
        
        if ($check == "customer") {
          foreach ($result as $key) {
            echo $key->customer;
          }
        }
        if ($check == "citieso") {
          foreach ($result as $key) {
            echo $key->citieso;
          }
        }
        if ($check == "citiesd") {
          foreach ($result as $key) {
            echo $key->citiesd;
          }
        }
        if ($check == "fleet") {
          foreach ($result as $key) {
            echo $key->fleet;
          }
        }
        if ($check == "driver") {
          foreach ($result as $key) {
            echo $key->driver;
          }
        }
        if ($check == "harga") {
          foreach ($result as $key) {
            echo $key->harga;
          }
        }
        if ($check == "type") {
          foreach ($result as $key) {
            echo $key->type;
          }
        }
        
    } 

  function get_autocomplete()
    {
      $query = $this->input->get('query');
        if (isset($query)) {
            $this->db->like('frameno', $query , 'both');
            $this->db->order_by('frameno', 'ASC');
            $this->db->limit(10);
            $result = $this->db->get('orderload')->result();
            if (count($result) > 0) {
              foreach ($result as $row)
              {
                $arr_result[]="".$row->id." / ".$row->frameno."";
              }
                echo json_encode($arr_result);
            }
          }
    }

  public function getPriceRoutes()
    {
      $origin=$_POST['citieso'];
      $destination=$_POST['citiesd'];
      $otype=$_POST['type'];
     

      $this->db->select('a.*, b.name as citieso,c.name as citiesd');
      $this->db->join('cities b','a.id_citieso = b.id', 'left');
      $this->db->join('cities c','a.id_citiesd = c.id', 'left');
      $array = array('a.id_citieso' => $origin, 'a.id_citiesd' => $destination,'a.id_ordertypes' => $otype, 'a.active' => 1);
      $this->db->where($array); 
      $result=$this->db->get('routes a');

      foreach ($result->result_array() as $row){
        if (is_null($row['code'])) {
          echo "Kosong Dong";
        }else{

         // echo "<option value=''></option>  
                //<option value='".$row['id']."'>".$row['code']." | Rp.".number_format($row['allowance'])." | ".$row['citieso']." To ".$row['citiesd']."</option>";
          echo number_format($row['allowance']);
        }
        
      }
    }

  public function getPriceAllowance()
    {
      $route=$_POST['routes'];

      $this->db->select('*');
      $this->db->where('id',$route);
      $result=$this->db->get('routes');

      foreach ($result->result() as $row){
        echo $row->allowance;
      }
    }

  public function saveClaim()
    {
      $this->orderclaims->Simpan();
      redirect('orderclaim?msg=Save Success');
    }

  public function saveEdit()
    {
      $this->customers->editCustomer();
      redirect('customer?msg=Save Success');
    }

  public function modaledit()
    {
      $data['order']=$this->orderclaims->getOneOrder();
  	
      $data['orderload']=$this->orderclaims->getOrderload();

      $this->load->model('customers');
      $data['customer']=$this->customers->getCustomer();

      $this->load->model('fleets');
      $data['fleet']=$this->fleets->getFleets();

      $this->load->model('drivers');
      $data['driver']=$this->drivers->getDrivers();

      $this->load->model('cities');
      $data['cities']=$this->cities->getCities();
  	
  	  $this->load->model('ordertypes');
      $data['ordertypes']=$this->ordertypes->getOrderTypes();

      $this->load->view('pages/modaledit_',$data);
     
    }
  
  public function modaldetail()
    {
      $data['order']=$this->orders->getOneOrder();
    	
      $data['orderload']=$this->orders->getOrderload();

      $this->load->model('customers');
      $data['customer']=$this->customers->getCustomer();

      $this->load->model('fleets');
      $data['fleet']=$this->fleets->getFleets();

      $this->load->model('drivers');
      $data['driver']=$this->drivers->getDrivers();

      $this->load->model('cities');
      $data['cities']=$this->cities->getCities();
    	
    	$this->load->model('ordertypes');
      $data['ordertypes']=$this->ordertypes->getOrderTypes();


      $this->load->view('pages/modaldetail',$data);
     
    }

  public function deleteCustomer()
  {
    $this->customers->delete();
    redirect('customer?msg=Delete Success');
   
  }
  
}
?>