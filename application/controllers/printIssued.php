<style>
body { 
    font-family:Courier;
    height: auto;
    font-size:13pt;
}

h3{
    color:orange;
}

h1{
    font-size: 20pt;
}
.text-red{
    font-weight: bolder;
    color:red;
}
.detail-desc{
    font-size: 7pt;
}
.detail-invoice{
    border-collapse: collapse;
    border-color: silver;
    border-width: thin;
    font-size: 7pt;
}

.detail-invoice tbody td{
    font-size: 8pt;
}
.footer{
    font-size: 8pt;
}
table{
    border-collapse: collapse;
}

hr{
    border-collapse: collapse;
    border: 1px solid black;
}
#code-text{
    width:100%;
    float: right;
    text-align: right;
}
</style>
<body>
<div id="code-text" style="float: right;"><?=$SPB[0]['dpb_issued_id']?></div>
<table style="width: 100%;" cellpadding="4" cellspacing="5">
    <tr>
        <th colspan="4" align="center" valign="middle">
            <h2>FORM ISSUED ITEM</h2>
            <hr />
        </th>
    </tr>
    <tr>
        <th colspan="4">&nbsp;</th>
    </tr>
    <tr>
        <td align="left" width="30%"><b>Nomor Issued</b></td>
        <td align="left" colspan="3" >: <?=$SPB[0]['dpb_code']?>." / ".$drivers->name;?></td>
    </tr>
    <tr>
        <td align="left"><b>Nomor SPB</b></td>
        <td align="left" width="25%">: <?=$fleets->fleetplateno;?></td>
    </tr>
    <tr>
        <td align="left"><b>Rute</b></td>
        <td align="left">: <?=$citieso->name?></td>
        <td align="left"><strong>Ke</strong> </td>
        <td align="left"><?=$citiesd->name?></td>
    </tr>
    <tr>
        <td align="left"><b>Nama Pelanggan</b></td>
        <td align="left" colspan="2">: <?=$customers->name?></td>
    </tr>
    <tr>
        <td align="left"><b>No Shipment</b></td>
        <td align="left" colspan="2">:
            <?php if($shipments != null){
                foreach($shipments AS $shipment){
                    echo $shipment->shipment." ";
                }
            } ?>
        </td>
    </tr>
    
    <tr>
        <th colspan="4"><hr /></th>
    </tr>
    <tr>
        <td align="left" colspan="2"><b>Uang Jalan</b></td>
        <td>Rp </td>
        <td align="right"><?=number_format($routes->allowance);?></td>
    </tr>
    <tr>
        <td align="left" colspan="4"><b>Uang Tambahan</b></td>
    </tr>
    <?php if($orderrouteaddarrays != null){
        $no1 = "a";
        foreach($orderrouteaddarrays AS $orderrouteaddarray){
        ?>
    <tr>
        <td align="left" colspan="2">&nbsp;&nbsp;<b style="text-transform: capitalize;"><?=$no1;?>. <?=$orderrouteaddarray->costcomponents_name?></b></td>
        <td>Rp </td>
        <td align="right"><?=number_format($orderrouteaddarray->price);?></td>
    </tr>
    <?php $no1++; } } ?>
    <tr>
        <td align="left" colspan="4"><b>Uang Potongan</b></td>
    </tr>
    <?php if($orderrouteredarrays != null){
        $no = "a";
        foreach($orderrouteredarrays AS $orderrouteredarray){
        ?>
    <tr>
        <td align="left" colspan="2">&nbsp;&nbsp;<b style="text-transform: capitalize;"><?=$no;?>. <?=$orderrouteredarray->feecomponents_name?></b></td>
        <td>Rp </td>
        <td align="right"><?=number_format($orderrouteredarray->price);?></td>
    </tr>
    <?php $no++; } } ?>
    <tr>
        <th colspan="2"></th>
        <th colspan="2"><hr /></th>
    </tr>
    
    <tr>
        <td align="left" colspan="2"><b>Total Menerima Uang Jalan</b></td>
        <td>Rp </td>
        <td align="right"><b><?=number_format($routes->allowance+$orderrouteadds->price - $orderroutereds->price);?></b></td>
    </tr>
    <tr>
        <th colspan="4">&nbsp;</th>
    </tr>
    <tr>
        <th colspan="4">&nbsp;</th>
    </tr>
    <tr>
        <th colspan="4">
            <table border="1" width="100%" cellpadding="5">
                <tr>
                    <th width="33%">Memberikan</th>
                    <th width="33%">Menerima</th>
                    <th width="34%">Mengetahui</th>
                </tr>
                <tr>
                    <td>&nbsp; <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </th>
    </tr>
</table>
