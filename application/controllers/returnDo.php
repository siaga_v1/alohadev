<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class returnDo extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('returnDOS');
      
  }
   
  // membuat fungsi index
  public function index()
  {
    $this->load->model('customers');
    $data['customer']=$this->customers->getCustomer(); 
    $this->template->load('template','pages/returnDo',$data);
  } 

  public function loadRecord($page =0)

    {
      $perpage = 50;
    
      $data = array();
      $filter = array();
      $data['search'] = array();
      
      $limit  = $perpage;
      $offset = $page > 1 ? ($limit*$page)-$limit:0;
              
      $all_data =  $this->returnDOS->getReturnDO($filter);
      $data['total'] = count($all_data);
      $config['base_url'] = base_url() . '/returnDo/loadRecord';
      $config['use_page_numbers'] = TRUE;
      $config['total_rows'] = $data['total'];
      $config['per_page'] = $perpage;
      $config['num_links'] = 1;
      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['first_link'] = 'First';
      $config['first_tag_open'] = '<li class="prev page">';
      $config['first_tag_close'] = '</li>';
      $config['last_link'] = 'Last';
      $config['last_tag_open'] = '<li class="next page">';
      $config['last_tag_close'] = '</li>';
      $config['next_link'] = 'Next';
      $config['next_tag_open'] = '<li class="next page">';
      $config['next_tag_close'] = '</li>';
      $config['prev_link'] = 'Prev';
      $config['prev_tag_open'] = '<li class="prev page">';
      $config['prev_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a>';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li class="page">';
      $config['num_tag_close'] = '</li>';
      $config['anchor_class'] = 'follow_link';
      
      $this->pagination->initialize($config);
      $paginator = $this->pagination->create_links();
      $data['pagination'] = $paginator;
              
      $filter["limit"]  = $limit;
      $filter["page"] = $page;
      $filter["offset"] = $offset;
              
      $data["result"] =  $this->returnDOS->getReturnDO($filter);
      $data["row"] = $offset;
      echo json_encode($data);


  }

  public function approve($value='')
  {
    $id = $this->input->post('id');

    $data['rdo']     = 'YES';
            
    $this->db->where('id', $id);
    $this->db->update('orderload', $data); 
    

  }

   public function unapprove($value='')
  {
    $id = $this->input->post('id');

    $data['rdo']     = NULL;
            
    $this->db->where('id', $id);
    $this->db->update('orderload', $data); 
  }

  public function modaledit()
  {
    $data['cities']=$this->cities->getOneOrder();


    $this->load->view('pages/modalCities',$data);

   
  }

  public function saveCities()
  {
    $this->cities->Simpan();
    redirect('City?msg=Save Success');
  }



  public function deleteCities()
  {
    $this->cities->delete();
    redirect('city?msg=Delete Success');
   
  }
  
}
?>