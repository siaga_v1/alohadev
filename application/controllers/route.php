<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class route extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('routes');
      
  }
   
  // membuat fungsi index
  public function index()
  {
    $data['route']=$this->routes->getRoutes();
    $data['id']=$this->routes->getLastID();

    $this->load->model('cities');
    $data['cities']=$this->cities->getCities();
    
    $this->load->model('fleets');
    $data['fleettypes']=$this->fleets->getFleettypes();

    $this->load->model('customers');
    $data['customer']=$this->customers->getCustomer();

    $this->template->load('template','pages/routes',$data);
  } 

  public function saveEdit()
  {
    $this->routes->editRoute();
    redirect('route?msg=Save Success');
  }

  public function modaledit()
  {
	$data['route']=$this->routes->getOneOrder();

    $this->load->model('cities');
    $data['cities']=$this->cities->getCities();
  $this->load->model('fleets');
    $data['fleettypes']=$this->fleets->getFleettypes();

    $this->load->model('customers');
    $data['customer']=$this->customers->getCustomer();

    $this->load->view('pages/modalroute',$data);

   
  }

  public function saveRoute()
  {
    $this->routes->Simpan();
    redirect('route?msg=Save Success');
  }



  public function deleteRoute()
  {
    $this->routes->delete();
    redirect('route?msg=Delete Success');
   
  }
  
}
?>