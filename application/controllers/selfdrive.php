<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');

class selfdrive extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('selfdrives');
      
  }
   
  public function index()
        {
          $data['list']=$this->selfdrives->getSelfDrive();
          $this->template->load('template','pages/selfdrive',$data);
        }

  public function get_autocomplete()
    {
      $query = $this->input->get('query');
        if (isset($query)) {
            $this->db->like('code', $query , 'both');
            $this->db->order_by('code', 'ASC');
            $this->db->limit(10);
            $result = $this->db->get('orders')->result();
            if (count($result) > 0) {
              foreach ($result as $row)
              {
                $arr_result[]=$row->code;
              }
                echo json_encode($arr_result);
            }
          }
    }

  public function saveSelfDrive()
    {
      $this->selfdrives->Simpan();
      redirect('selfdrive?msg=Save Success');
    }

  public function saveEdit()
    {
      $this->selfdrives->saveEdit();
      redirect('selfdrive?msg=Save Success');
    }
  
  public function modalEdit()
    {
      $data['detail']=$this->selfdrives->getOneSelfDrive();
      $this->load->view('pages/modalSelfEdit',$data);
    }

  public function modalDetail()
    {
      $data['detail']=$this->selfdrives->getSelfDrive();
      $this->load->view('pages/modalSelfDetail',$data);
    }
}
?>