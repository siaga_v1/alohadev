<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class summarySlip extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('summarySlips');
      
  }
   
  // membuat fungsi index
  public function index()
  {   
    $this->load->model('customers');
    $data['customer']=$this->customers->getCustomer(); 
    $data['id']=$this->summarySlips->getLastID(); 
    $this->template->load('template','pages/summarySlip',$data);
  } 

  public function summarylist()
  {
    $data['list'] = $this->summarySlips->listsummary();
    $this->template->load('template','pages/summarylist',$data);
  } 
   public function piutang()
  {
    $data['list'] = $this->summarySlips->listsummarya();
    $this->template->load('template','pages/piutang',$data);
  } 

  public function savePiutang()
  {
    $this->summarySlips->savePiutangs();
    redirect('summarySlip/piutang');
  }

  public function modalPiutang()
  {
    $id = $_POST['id'];
    $this->db->select('a.*, b.name as cname, DATEADD(day, 14, a.dateInvoice) as tempo');
    $this->db->where('a.active', '1');
    $this->db->where('a.noinvoice', $id);
    $this->db->join('customers b','a.id_customers = b.id', 'left');

    $result = $this->db->get('invoices a');   
    $data['invoice']= $result->result();
    
    $this->load->model('customers');
    $data['customer']=$this->customers->getCustomer();
    $this->load->view('pages/modalPiutang',$data);
  } 


  public function modalInvoice()
  {
    $id = $_POST['id'];

    $test = "SELECT * FROM dbo.invoices WHERE noInvoice = '$id'";

    $rtest = $this->db->query($test);  
    foreach ($rtest->result() as $key ) {
      if ($key->selisih == 1 ) {

         $sql= "SELECT o.id,o.noVoucher,o.orderdate,e.selisihcode as nosummary,o.ritase,e.id AS idol,e.type,e.frameno,e.noshippent,o.prices,b.name AS n1,c.name AS c2,d.name AS cname,g.fleetplateno AS fleet,h.name AS driver,COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,COALESCE ( SUM ( o.allowanceadds ), 0 ) AS unitprice,COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,COALESCE ( SUM ( o.prices ), 0 ) AS pricess FROM orders o LEFT JOIN orderload e ON o.code = e.order_code LEFT JOIN cities b ON e.id_citieso = b.id LEFT JOIN cities c ON e.id_citiesd = c.id LEFT JOIN customers d ON o.id_customers = d.id  LEFT JOIN fleets g ON o.id_fleets = g.id LEFT JOIN drivers h ON o.id_drivers = h.id WHERE o.active = 1 AND e.selisihcode = '$id' GROUP BY o.id, o.noVoucher,o.code,o.orderdate,o.ritase,e.id,e.type,e.noshippent,e.frameno,o.prices,e.selisihcode,g.fleetplateno,g.CODE,g.fleetnumber,d.NAME,b.name,c.name,h.name ";
      
          $result = $this->db->query($sql);  
          $data['detail']= $result->result();

          $queryinv ="SELECT dbo.invoices.noinvoice, dbo.invoices.faktur, dbo.invoices.code, dbo.invoices.dateInvoice, dbo.customers.address_1, dbo.customers.npwp, dbo.customers.address_2, dbo.customers.address_3, dbo.customers.cabang, dbo.customers.norekening, dbo.invoices.nosummary, dbo.invoices.nominal, dbo.customers.id as idcust, dbo.invoices.ppn, dbo.customers.name FROM dbo.invoices INNER JOIN dbo.customers ON dbo.invoices.id_customers = dbo.customers.id WHERE dbo.invoices.nosummary = '$id'";

          $resultinv = $this->db->query($queryinv);

          $data['invoice']=$resultinv->result();


          $this->load->model('customers');
          $data['customer']=$this->customers->getCustomer();

          $data['id'] = $_POST['id'];

          $this->load->view('pages/modalInvoice',$data);

      }else{
         $sql= "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,e.type,e.unitprice,e.frameno,e.noshippent,o.prices,b.name AS n1,c.name AS c2,d.name AS cname,g.fleetplateno AS fleet,h.name AS driver,COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,COALESCE ( SUM ( o.prices ), 0 ) AS pricess FROM orders o LEFT JOIN orderload e ON o.code = e.order_code LEFT JOIN cities b ON e.id_citieso = b.id LEFT JOIN cities c ON e.id_citiesd = c.id LEFT JOIN customers d ON o.id_customers = d.id  LEFT JOIN fleets g ON o.id_fleets = g.id LEFT JOIN drivers h ON o.id_drivers = h.id WHERE o.active = 1 AND e.nosummary = '$id' GROUP BY o.id, o.noVoucher,o.code,o.orderdate,o.ritase,e.id,e.type,e.noshippent,e.unitprice,e.frameno,o.prices,e.nosummary,g.fleetplateno,g.CODE,g.fleetnumber,d.NAME,b.name,c.name,h.name ";
              
            $result = $this->db->query($sql);  
            $data['detail']= $result->result();

            $queryinv ="SELECT dbo.invoices.noinvoice, dbo.invoices.faktur, dbo.invoices.code, dbo.invoices.dateInvoice, dbo.customers.address_1, dbo.customers.npwp, dbo.customers.address_2, dbo.customers.address_3, dbo.customers.cabang, dbo.customers.norekening, dbo.invoices.nosummary, dbo.invoices.nominal, dbo.customers.id as idcust, dbo.invoices.ppn, dbo.customers.name FROM dbo.invoices INNER JOIN dbo.customers ON dbo.invoices.id_customers = dbo.customers.id WHERE dbo.invoices.nosummary = '$id'";

            $resultinv = $this->db->query($queryinv);

            $data['invoice']=$resultinv->result();


            $this->load->model('customers');
            $data['customer']=$this->customers->getCustomer();

            $data['id'] = $_POST['id'];

            $this->load->view('pages/modalInvoice',$data);
      }
    }

   
  } 

  public function getCustomers()
  {
    $id = $_POST['id'];

    $this->db->select('*');
    $this->db->where('active', '1');
    $this->db->where('id',$id );
    $result = $this->db->get('customers');

    foreach ($result->result_array() as $key) {
       $data['address_1'] = $key['address_1'];
       $data['address_2'] = $key['address_2'];
       $data['address_3'] = $key['address_3'];
       $data['phone'] = $key['phone'];
       $data['pic_name'] = $key['pic_name'];
    }

    echo json_encode($data);
  }

  public function loadRecord($page =0)

  {
      $perpage = 50;
    
      $data = array();
      $filter = array();
      $data['search'] = array();
      
      $limit  = $perpage;
      $offset = $page > 1 ? ($limit*$page)-$limit:0;
              
      $all_data =  $this->summarySlips->getReturnDO($filter);
      $data['total'] = count($all_data);
      $config['base_url'] = base_url() . '/returnDo/loadRecord';
      $config['use_page_numbers'] = TRUE;
      $config['total_rows'] = $data['total'];
      $config['per_page'] = $perpage;
      $config['num_links'] = 1;
      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['first_link'] = 'First';
      $config['first_tag_open'] = '<li class="prev page">';
      $config['first_tag_close'] = '</li>';
      $config['last_link'] = 'Last';
      $config['last_tag_open'] = '<li class="next page">';
      $config['last_tag_close'] = '</li>';
      $config['next_link'] = 'Next';
      $config['next_tag_open'] = '<li class="next page">';
      $config['next_tag_close'] = '</li>';
      $config['prev_link'] = 'Prev';
      $config['prev_tag_open'] = '<li class="prev page">';
      $config['prev_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a>';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li class="page">';
      $config['num_tag_close'] = '</li>';
      $config['anchor_class'] = 'follow_link';
      
      $this->pagination->initialize($config);
      $paginator = $this->pagination->create_links();
      $data['pagination'] = $paginator;
              
      //$data["page"] = $page;
      ///$data["offset"] = $offset;
      //$data["limit"] = $limit;
              
      $filter["limit"]  = $limit;
      $filter["page"] = $page;
      $filter["offset"] = $offset;
              
      $data["result"] =  $this->returnDOS->getReturnDO($filter);
      $data["row"] = $offset;
      echo json_encode($data);


  }

    public function getIDInvoice()
  {
    $id = $_POST['id'];
    date_default_timezone_set("Asia/Bangkok");
    $this->db->select('*');
    $this->db->where('active', '1');
    $this->db->where('id_customers',$id );
    $result = $this->db->get('invoices');

    foreach ($result->result_array() as $key) {
      $number = $key['noinvoice'];
    }
    if (date('d')=='01' && !isset($number))
      { 
        $a = '0001'; 
      }
      elseif(isset($number))
      { 
        $nmr=$number;
        $ex = explode('/', $nmr);
        $a = sprintf("%04d", $ex[0]+1); 
      }elseif(!isset($number))
      { 
        $a = '0001'; 
      }
      $now=date('d-m-Y');
      $c = array('','I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
      $tahun=date('y');
      echo "".$a."/".$c[date('n')]."/AST-";

  }

  public function invoicelama()
  {
    $this->load->library('M_pdf');

    $mpdf = $this->m_pdf->load([
    'mode' => 'utf-8',
    'format' => 'Legal'
    ]);
    
    $this->summarySlips->Simpan();
    
    $data['invoice']=$this->summarySlips->printInvoice();
    $view = $this->load->view('pages/printInvoice',$data,true);

    $mpdf->WriteHTML($view);

    $mpdf->Output();
  }

  public function invoice()
  {

    $this->load->library('M_pdf');

    $mpdf = $this->m_pdf->load([
    'mode' => 'utf-8',
    'format' => 'Legal'
    ]);
    
    $this->summarySlips->Simpan();
    
    $id = $this->input->post('code');
    $data['kawalan'] = $this->input->post('KAWALAN');
    $data['pph'] = $this->input->post('PPH');
    $data['ops'] = $this->input->post('Operasional');
    $data['lolo'] = $this->input->post('LOLO');
    $data['depo'] = $this->input->post('DEPO');
    $data['parkir'] = $this->input->post('PARKIR');
    echo $id;

    $test = "SELECT * FROM dbo.invoices WHERE nosummary = '$id'";

    $rtest = $this->db->query($test);  
    foreach ($rtest->result() as $key ) {
      if ($key->selisih == 1 ) {

         $sql= "SELECT o.id,o.noVoucher,o.orderdate,e.selisihcode as nosummary,o.ritase,e.id AS idol,e.type,e.frameno,e.noshippent,o.prices,o.feeOps,o.feeLolo,o.feeDepo,o.feeParkir,o.feeKawalan,b.name AS n1,c.name AS c2,d.name AS cname,g.fleetplateno AS fleet,h.name AS driver, COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,COALESCE ( SUM ( o.allowanceadds ), 0 ) AS unitprice,COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,COALESCE ( SUM ( o.prices ), 0 ) AS pricess FROM orders o LEFT JOIN orderload e ON o.code = e.order_code LEFT JOIN cities b ON e.id_citieso = b.id LEFT JOIN cities c ON e.id_citiesd = c.id LEFT JOIN customers d ON o.id_customers = d.id  LEFT JOIN fleets g ON o.id_fleets = g.id LEFT JOIN drivers h ON o.id_drivers = h.id WHERE o.active = 1 AND e.selisihcode = '$id' GROUP BY o.id, o.noVoucher,o.code,o.orderdate,o.ritase,e.id,e.type,e.noshippent,e.frameno,o.prices,e.selisihcode,g.fleetplateno,g.CODE,g.fleetnumber,d.NAME,b.name,c.name,h.name ,o.feeOps,o.feeLolo,o.feeDepo,o.feeParkir,o.feeKawalan ";
      
          $result = $this->db->query($sql);  
          $data['detail']= $result->result();

          $queryinv ="SELECT dbo.invoices.noinvoice, dbo.invoices.faktur, dbo.invoices.code, dbo.invoices.dateInvoice, dbo.customers.address_1, dbo.customers.npwp, dbo.customers.address_2, dbo.customers.address_3, dbo.customers.cabang, dbo.customers.norekening, dbo.invoices.nosummary, dbo.invoices.nominal, dbo.customers.id as idcust, dbo.invoices.ppn, dbo.customers.name FROM dbo.invoices INNER JOIN dbo.customers ON dbo.invoices.id_customers = dbo.customers.id WHERE dbo.invoices.nosummary = '$id'";

          $resultinv = $this->db->query($queryinv);

          $data['invoice']=$resultinv->result();


          $this->load->model('customers');
          $data['customer']=$this->customers->getCustomer();

          $data['id'] = $this->input->post('code');

          $view = $this->load->view('pages/printInvoice',$data,true);

          $mpdf->WriteHTML($view);

          $mpdf->Output();

      }else{
         $sql= "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,i.name,e.type,e.unitprice,e.frameno,e.noshippent,o.prices,b.name AS n1,o.feeOps,o.feeLolo,o.feeDepo,o.feeParkir,o.feeKawalan,c.name AS c2,d.name AS cname,g.fleetplateno AS fleet,h.name AS driver,COALESCE ( SUM ( ccc.nominal ), 0 ) AS nominals,COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,COALESCE ( SUM ( o.prices ), 0 ) AS pricess FROM orders o LEFT JOIN orderload e ON o.code = e.order_code LEFT JOIN cities b ON e.id_citieso = b.id LEFT JOIN cities c ON e.id_citiesd = c.id LEFT JOIN customers d ON o.id_customers = d.id LEFT JOIN fleets g ON o.id_fleets = g.id LEFT JOIN cost ccc ON o.code = ccc.order_id LEFT JOIN fleettypes i ON g.id_fleettypes = i.id  LEFT JOIN drivers h ON o.id_drivers = h.id WHERE o.active = 1 AND e.nosummary = '$id' GROUP BY o.id, o.noVoucher,o.code,o.orderdate,o.ritase,e.id,e.type,e.noshippent,e.unitprice,e.frameno,o.prices,e.nosummary,g.fleetplateno,ccc.nominal,g.CODE,g.fleetnumber,i.name,d.NAME,b.name,c.name,o.feeOps,o.feeLolo,o.feeDepo,o.feeParkir,o.feeKawalan,h.name ";
              
            $result = $this->db->query($sql);  
            $data['detail']= $result->result();

            $queryinv ="SELECT dbo.invoices.noinvoice, dbo.invoices.faktur, dbo.invoices.code, dbo.invoices.dateInvoice, dbo.customers.address_1, dbo.customers.npwp, dbo.customers.address_2, dbo.customers.address_3, dbo.customers.cabang, dbo.customers.norekening, dbo.invoices.nosummary, dbo.invoices.nominal, dbo.customers.id as idcust, dbo.invoices.ppn, dbo.customers.name FROM dbo.invoices INNER JOIN dbo.customers ON dbo.invoices.id_customers = dbo.customers.id WHERE dbo.invoices.nosummary = '$id'";

            $resultinv = $this->db->query($queryinv);

            $data['invoice']=$resultinv->result();


            $this->load->model('customers');
            $data['customer']=$this->customers->getCustomer();

            $data['id'] = $this->input->post('code');

            $view = $this->load->view('pages/printInvoice',$data,true);

            $mpdf->WriteHTML($view);

            $mpdf->Output();
      }
    }

   
  } 

  public function loadsummary($page =0)

    {
      $perpage = 50;
    
      $data = array();
      $filter = array();
      $data['search'] = array();
      
      $limit  = $perpage;
      $offset = $page > 1 ? ($limit*$page)-$limit:0;
              
      $all_data =  $this->summarySlips->getReturnDO($filter);
      $data['total'] = count($all_data);
      $config['base_url'] = base_url() . '/returnDo/loadsummary';
      $config['use_page_numbers'] = TRUE;
      $config['total_rows'] = $data['total'];
      $config['per_page'] = $perpage;
      $config['num_links'] = 1;
      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['first_link'] = 'First';
      $config['first_tag_open'] = '<li class="prev page">';
      $config['first_tag_close'] = '</li>';
      $config['last_link'] = 'Last';
      $config['last_tag_open'] = '<li class="next page">';
      $config['last_tag_close'] = '</li>';
      $config['next_link'] = 'Next';
      $config['next_tag_open'] = '<li class="next page">';
      $config['next_tag_close'] = '</li>';
      $config['prev_link'] = 'Prev';
      $config['prev_tag_open'] = '<li class="prev page">';
      $config['prev_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a>';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li class="page">';
      $config['num_tag_close'] = '</li>';
      $config['anchor_class'] = 'follow_link';
      
      $this->pagination->initialize($config);
      $paginator = $this->pagination->create_links();
      $data['pagination'] = $paginator;
              
      //$data["page"] = $page;
      ///$data["offset"] = $offset;
      //$data["limit"] = $limit;
              
      $filter["limit"]  = $limit;
      $filter["page"] = $page;
      $filter["offset"] = $offset;
              
      $data["result"] =  $this->returnDOS->getReturnDO($filter);
      $data["row"] = $offset;
      echo json_encode($data);


  }

  public function search($page =0)
  { 
    if($this->input->get('export') == 'excel')
    {

      $this->load->library('PHPExcel');
      $data['search'] = array(); 
      $rows = $this->summarySlips->Search($data);


      date_default_timezone_set("Asia/Bangkok");
      $tanggalupdate = date("Y-m-d H:i:s");
      $tanggal= date("Y-m-d");

      if($this->input->get('nosummary') != '') 
      {                
        $da['noinvoice']          = $this->input->get('nosummary');
        $da['id_customers']       = $this->input->get('id_customer');
        $da['nominal']            = $this->input->get('totalinv');
        $da['faktur']            = $this->input->get('nsfp');
        $da['active']             = 1;
        $da['createby']           = $this->session->userdata('id');
        $da['createdatetime']     = $tanggalupdate;
        $da['dateinvoice']     = $tanggal;

        $this->db->insert('invoices',$da);
        foreach ($rows as $key) {

          $dat['id_orderload'] = $key->idol;
          $dat['nosummary']    = $this->input->get('nosummary');
          $dat['active']       = 1;
          $dat['prices']       = $key->unitprice;
          $dat['createby']             = $this->session->userdata('id');
          $dat['createdatetime']       = $tanggalupdate;

          $this->db->insert('invoicedetails',$dat);
        }
      }

      foreach ($rows as $key) {

        $d = $key->idol;
        $upda['nosummary'] = $this->input->get('nosummary');


        $this->db->where('id', $d);
        $this->db->update('orderload', $upda);
      }



      $objPHPExcel = new PHPExcel();
      $objPHPExcel->getProperties()
      ->setCreator('PT Total Kilat')
      ->setTitle('PHPExcel Demo')
      ->setLastModifiedBy('Taylor Ren')
      ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
      ->setSubject('PHP Excel manipulation')
      ->setKeywords('excel php office phpexcel lakers')
      ->setCategory('programming')
      ;

      $ews = $objPHPExcel->getSheet(0);
      $ews->setTitle('Data');

      $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

      $style = array(
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
        )
      );

			$styleArray = array(
			      'borders' => array(
			          'allborders' => array(
			              'style' => PHPExcel_Style_Border::BORDER_THIN
			          )
			      )
			  );

			$objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);
      $objPHPExcel->setActiveSheetIndex(0);
      $objPHPExcel->getActiveSheet()->setCellValue('A1', 'PT ARMADA SEJAHTERA TRANSINDO');
      $objPHPExcel->getActiveSheet()->mergeCells('A1:N1');
      $objPHPExcel->getActiveSheet()->getStyle("A1:N1")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle("A1:N1")->getFont()->setSize(16);
      $objPHPExcel->getActiveSheet()->getStyle("A1:N1")->applyFromArray($style);
            
      $objPHPExcel->getActiveSheet()->setCellValue('A2', '');
      $objPHPExcel->getActiveSheet()->mergeCells('A2:N2');
      $objPHPExcel->getActiveSheet()->getStyle("A2:N2")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle("A2:N2")->getFont()->setSize(11);
      $objPHPExcel->getActiveSheet()->getStyle("A2:N2")->applyFromArray($style);

      $objPHPExcel->getActiveSheet()->setCellValue('A3', 'TELP : ');
      $objPHPExcel->getActiveSheet()->mergeCells('A3:N3');
      $objPHPExcel->getActiveSheet()->getStyle("A3:N3")->getFont()->setBold(TRUE);
      $objPHPExcel->getActiveSheet()->getStyle("A3:N3")->getFont()->setSize(11);
      $objPHPExcel->getActiveSheet()->getStyle("A3:N3")->applyFromArray($style);

      $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No. [NO INVOICE]');
      $objPHPExcel->getActiveSheet()->setCellValue('H5', 'TANGGAL : [TANGGAL]');
      $objPHPExcel->getActiveSheet()->getStyle("A5:N5")->getFont()->setBold(TRUE);
      $objPHPExcel->getActiveSheet()->getStyle("A5:N5")->getFont()->setSize(11);


      $objPHPExcel->getActiveSheet()->setCellValue('A7', 'Kepada Yth : ');
      $objPHPExcel->getActiveSheet()->setCellValue('A8', 'NAMA CUSTOMER ');
      $objPHPExcel->getActiveSheet()->setCellValue('A9', 'Up. Finance Dept. ');
      $objPHPExcel->getActiveSheet()->setCellValue('A11', 'Dengan Hormat,');
      $objPHPExcel->getActiveSheet()->setCellValue('A13', 'Dengan ini kami tagihan ongkos trucking (surat jalan terlampir) :');
      $objPHPExcel->getActiveSheet()->getStyle("A6:N6")->getFont()->setBold(TRUE);
      $objPHPExcel->getActiveSheet()->getStyle("A6:N6")->getFont()->setSize(11);
            
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 14,'No');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 14,'Tgl');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 14,'No. Pol');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 14,'Nama Pabrik');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 14,'Daerah');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 14,'No Kontainer');
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 14,'Biaya Trucking');
      $garis = 8;
      if (!empty($this->input->get('LOLO')) && !isset($garisbaru)) {
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($garis, 14,'LOLO');
        $garisbaru = $garis + 1;        
      }
      if (!empty($this->input->get('DEPO')) && !isset($garisbaru)) {
        $garisbaru = $garis +1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($garis, 14,'DEPO');
      }
      $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 14,'Total');

      if($rows != null)
      {
        $prices = 0;
        $ritases = 0;
        $allowance = 0;
        $allowanceaddss = 0;
        $allowancereds = 0;
        $no  = 1;
        $baris = 15;
        foreach($rows AS $row)
        {   
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $no);
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->orderdate);
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->fleet);
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->n1);
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->c2);
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris, $row->frameno);
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $baris, $row->unitprice);

          $ritases += $row->ritase;
          $allowance += $row->unitprice;

          $no++;
          $baris++;
        }

        $barisfooter = $baris;
        $barisbaru = $barisfooter-1;
        $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $barisfooter, 'TOTAL');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $barisfooter, $allowance);
        $objPHPExcel->getActiveSheet()->getStyle("A$barisfooter:N$barisfooter")->getFont()->setBold(TRUE);
        $barisbaru3 = $barisfooter+1;

        function penyebut($nilai) 
        {
          $nilai = abs($nilai);
          $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
          $temp = "";
          if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
          } else if ($nilai <20) {
            $temp = penyebut($nilai - 10). " Belas";
          } else if ($nilai < 100) {
            $temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
          } else if ($nilai < 200) {
            $temp = " Seratus" . penyebut($nilai - 100);
          } else if ($nilai < 1000) {
            $temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
          } else if ($nilai < 2000) {
            $temp = " Seribu" . penyebut($nilai - 1000);
          } else if ($nilai < 1000000) {
            $temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
          } else if ($nilai < 1000000000) {
            $temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
          } else if ($nilai < 1000000000000) {
            $temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
          } else if ($nilai < 1000000000000000) {
            $temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
          }     
          return $temp;
        }

        function terbilang($nilai) 
        {
          if($nilai<0) {
            $hasil = "minus ". trim(penyebut($nilai));
          } else {
            $hasil = trim(penyebut($nilai));
          }           
          return $hasil;
        }

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru3, 'TERBILANG ');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $barisbaru3, terbilang($allowance),' RUPIAH');
        $objPHPExcel->getActiveSheet()->getStyle("A$barisbaru3:N$barisbaru3")->getFont()->setBold(TRUE);
        $objPHPExcel->getActiveSheet()->getStyle("A$barisbaru3:N$barisbaru3")->getFont()->setItalic(TRUE);

        $barisbaru6 = $barisfooter+6;
        $barisbaru7 = $barisfooter+7;
        $barisbaru8 = $barisfooter+8;
        $barisbaru9 = $barisfooter+9;

        date_default_timezone_set("Asia/Bangkok");
        $tanggalan = date("d-F-Y");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru6, 'Demikian kami sampaikan tagihan diatas mohon di transfer ke No. Rekening :');
        if ($this->input->get('rekening')==1) {
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru7, "Bank BCA");
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru8, 'Ref : 123 123 1234');
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru9, 'A/N : SIDIK');
        }elseif ($this->input->get('rekening')==2) {
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru7, 'Bank BCA');
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru8, 'Ref : 123 123 1234');
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru9, 'A/N : SYAHRIL');
        }elseif ($this->input->get('rekening')==3) {
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru7, 'Bank MANDIRI');
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru8, 'Ref : 123 123 1234');
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru9, 'A/N : SYAHRIL SIDIK');
        }

      }
      else
      {
        $objPHPExcel->getActiveSheet()->setCellValue('A4:I4', 'Data Not Found');
        $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('e6f2ff');
        $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(14);
      }




      $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');


      header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
      header("Cache-Control: no-store, no-cache, must-revalidate");
      header("Cache-Control: post-check=0, pre-check=0", false);
      header("Pragma: no-cache");
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="TruckOrderSummary'.date('dmy').'.xlsx"');
      $writer->save('php://output');
    } 
    elseif($this->input->get('export') == 'pdf')
    {

      $this->load->library('PHPExcel');
      $data['search'] = array(); 
      $rows = $this->summarySlips->Search($data);


      date_default_timezone_set("Asia/Bangkok");
      $tanggalupdate = date("Y-m-d H:i:s");
      $tanggal= date("Y-m-d");
      $a = $this->input->get('nosummary');

      $add = array(
        'order_id' =>$this->input->get('nosummary'),
        'nominal' => $this->input->get('totalinv'), 
        'coa' => 11,
        'active' => 1,
        'createby' => $this->session->userdata('id')
      );

      $this->db->insert('cost', $add);

      if($this->input->get('nosummary') != '') 
      {                
        $da['noinvoice']          = $this->input->get('nosummary');
        $da['id_customers']       = $this->input->get('id_customer');        
        $da['nominal']            = $this->input->get('totalinv');
        $da['faktur']            = $this->input->get('nsfp');
        $da['active']             = 1;
        $da['createby']           = $this->session->userdata('id');
        $da['createdatetime']     = $tanggalupdate;
        $da['dateinvoice']     = $tanggal;
        if ($this->input->get('PPN')==1) {
          $da['ppn'] = $this->input->get('totalinv')*0.1 ;
        }

        $this->db->insert('invoices',$da);



        foreach ($rows as $key) {

          $dat['id_orderload'] = $key->idol;
          $dat['nosummary']    = $this->input->get('nosummary');
          $dat['active']       = 1;
          $dat['prices']       = $key->unitprice;
          $dat['createby']             = $this->session->userdata('id');
          $dat['createdatetime']       = $tanggalupdate;

          $this->db->insert('invoicedetails',$dat);
        }
        
      }

      foreach ($rows as $key) {

        $d = $key->idol;
        $upda['nosummary'] = $this->input->get('nosummary');


        $this->db->where('id', $d);
        $this->db->update('orderload', $upda);
      }

      $this->load->library('M_pdf');

      $mpdf = $this->m_pdf->load([
        'mode' => 'utf-8',
        'format' => 'Legal-L'
      ]);

        //$this->summarySlips->Simpan();

      //$datas['invoice']=$this->summarySlips->printInvoice($a);
      //$view = $this->load->view('pages/printInvoice',$datas,true);

      //$mpdf->WriteHTML($view);

      //$mpdf->Output();
      $datas['kawalan'] = $this->input->get('KAWALAN');
      $datas['PPN'] = $this->input->get('PPN');
      $datas['ops'] = $this->input->get('Operasional');
      $datas['lolo'] = $this->input->get('LOLO');
      $datas['depo'] = $this->input->get('DEPO');
      $datas['parkir'] = $this->input->get('PARKIR');
      $datas['rekening'] = $this->input->get('rekening');

      $sql= "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,i.name,e.type,e.unitprice,e.frameno,e.noshippent,o.prices,b.name AS n1,o.feeOps,o.feeLolo,o.feeDepo,o.feeKawalan,o.feeStorage,o.feeSeal,o.feeRC,o.feeAlih,o.feeCuci,o.feeSP2,o.feeParkir,c.name AS c2,d.name AS cname,g.fleetplateno AS fleet,h.name AS driver,COALESCE ( SUM ( ccc.nominal ), 0 ) AS nominals,COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,COALESCE ( SUM ( o.prices ), 0 ) AS pricess FROM orders o LEFT JOIN orderload e ON o.code = e.order_code LEFT JOIN cities b ON e.id_citieso = b.id LEFT JOIN cities c ON e.id_citiesd = c.id LEFT JOIN customers d ON o.id_customers = d.id LEFT JOIN fleets g ON o.id_fleets = g.id LEFT JOIN cost ccc ON o.code = ccc.order_id LEFT JOIN fleettypes i ON g.id_fleettypes = i.id  LEFT JOIN drivers h ON o.id_drivers = h.id WHERE o.active = 1 AND e.nosummary = '$a' GROUP BY o.id, o.noVoucher,o.code,o.orderdate,o.ritase,e.id,e.type,e.noshippent,e.unitprice,e.frameno,o.prices,e.nosummary,g.fleetplateno,ccc.nominal,g.CODE,g.fleetnumber,i.name,d.NAME,b.name,c.name,o.feeOps,o.feeLolo,o.feeDepo,o.feeKawalan,o.feeStorage,o.feeSeal,o.feeRC,o.feeAlih,o.feeCuci,o.feeSP2,o.feeParkir,h.name ";
              
            $result = $this->db->query($sql);  
            $datas['detail']= $result->result();

            $queryinv ="SELECT dbo.invoices.noinvoice, dbo.invoices.faktur, dbo.invoices.code, dbo.invoices.dateInvoice, dbo.customers.address_1, dbo.customers.npwp, dbo.customers.address_2, dbo.customers.address_3, dbo.customers.cabang, dbo.customers.norekening, dbo.invoices.nosummary, dbo.invoices.nominal, dbo.customers.id as idcust, dbo.invoices.ppn, dbo.customers.name FROM dbo.invoices INNER JOIN dbo.customers ON dbo.invoices.id_customers = dbo.customers.id WHERE dbo.invoices.noinvoice = '$a'";

            $resultinv = $this->db->query($queryinv);

            $datas['invoice']=$resultinv->result();


            $this->load->model('customers');
            $datas['customer']=$this->customers->getCustomer();

            $datas['id'] = $this->input->get('nosummary');

            $view = $this->load->view('pages/printInvoice',$datas,true);

            $mpdf->WriteHTML($view);

            $mpdf->Output();
      //print_r($datas);

    }
    else 
    { 

      $perpage = 50;
      $data = array();
      $filter = array();
      $data['seach'] = array();

      $limit  = $perpage;
      $offset = $page > 1 ? ($limit*$page)-$limit:0;

      $all_data =  $this->summarySlips->Search($filter);
      $data['total'] = count($all_data);
      $config['base_url'] = base_url() . '/summarySlip/search';
      $config['use_page_numbers'] = TRUE;
      $config['total_rows'] = $data['total'];
      $config['per_page'] = $perpage;
      $config['num_links'] = 1;
      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['first_link'] = 'First';
      $config['first_tag_open'] = '<li class="prev page">';
      $config['first_tag_close'] = '</li>';
      $config['last_link'] = 'Last';
      $config['last_tag_open'] = '<li class="next page">';
      $config['last_tag_close'] = '</li>';
      $config['next_link'] = 'Next';
      $config['next_tag_open'] = '<li class="next page">';
      $config['next_tag_close'] = '</li>';
      $config['prev_link'] = 'Prev';
      $config['prev_tag_open'] = '<li class="prev page">';
      $config['prev_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a>';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li class="page">';
      $config['num_tag_close'] = '</li>';
      $config['anchor_class'] = 'follow_link';

      $this->pagination->initialize($config);
      $paginator = $this->pagination->create_links();
      $data['pagination'] = $paginator;

      $filter["limit"]  = $limit;
      $filter["page"] = $page;
      $filter["offset"] = $offset;

      $data["result"] =   $this->summarySlips->Search($filter);
      $data["row"] = $offset;
      echo json_encode($data);
    }
  }

  public function saveCities()
  {
    $this->cities->Simpan();
    redirect('City?msg=Save Success');
  }



  public function deleteCities()
  {
    $this->cities->delete();
    redirect('city?msg=Delete Success');
   
  }


  public function printListInv()
  {
    $id = $this->input->get('id');
    $type = $this->input->get('type');

    if ($type == 1 ) {
      $this->load->library('M_pdf');

      $mpdf = $this->m_pdf->load([
        'mode' => 'utf-8',
        'format' => 'Legal-L'
      ]);


      $sql= "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,i.name,e.type,e.unitprice,e.frameno,e.noshippent,o.prices,b.name AS n1,o.feeOps,o.feeLolo,o.feeDepo,o.feeParkir,o.feeKawalan,c.name AS c2,d.name AS cname,g.fleetplateno AS fleet,h.name AS driver,COALESCE ( SUM ( ccc.nominal ), 0 ) AS nominals,COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,COALESCE ( SUM ( o.prices ), 0 ) AS pricess FROM orders o LEFT JOIN orderload e ON o.code = e.order_code LEFT JOIN cities b ON e.id_citieso = b.id LEFT JOIN cities c ON e.id_citiesd = c.id LEFT JOIN customers d ON o.id_customers = d.id LEFT JOIN fleets g ON o.id_fleets = g.id LEFT JOIN cost ccc ON o.code = ccc.order_id LEFT JOIN fleettypes i ON g.id_fleettypes = i.id  LEFT JOIN drivers h ON o.id_drivers = h.id WHERE o.active = 1 AND e.nosummary = '$id' GROUP BY o.id, o.noVoucher,o.code,o.orderdate,o.ritase,e.id,e.type,e.noshippent,e.unitprice,e.frameno,o.prices,e.nosummary,g.fleetplateno,ccc.nominal,g.CODE,g.fleetnumber,i.name,d.NAME,b.name,c.name,o.feeOps,o.feeLolo,o.feeDepo,o.feeParkir,o.feeKawalan,h.name ";

      $result = $this->db->query($sql);  
      $datas['detail']= $result->result();

      $queryinv ="SELECT dbo.invoices.noinvoice, dbo.invoices.faktur, dbo.invoices.code, dbo.invoices.dateInvoice, dbo.customers.address_1, dbo.customers.npwp, dbo.customers.address_2, dbo.customers.address_3, dbo.customers.cabang, dbo.customers.norekening, dbo.invoices.nosummary, dbo.invoices.nominal, dbo.customers.id as idcust, dbo.invoices.ppn, dbo.customers.name FROM dbo.invoices INNER JOIN dbo.customers ON dbo.invoices.id_customers = dbo.customers.id WHERE dbo.invoices.noinvoice = '$id'";

      $resultinv = $this->db->query($queryinv);

      $datas['invoice']=$resultinv->result();


      $this->load->model('customers');
      $datas['customer']=$this->customers->getCustomer();

      $datas['id'] = $this->input->post('noinvoice');

      $view = $this->load->view('pages/printInvoice',$datas,true);

      $mpdf->WriteHTML($view);

      $mpdf->Output();
    }
    elseif ($type == 2 ) {
            $this->load->library('M_pdf');

      $mpdf = $this->m_pdf->load([
        'mode' => 'utf-8',
        'format' => 'Legal-L'
      ]);


      $sql= "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,i.name,e.type,e.unitprice,e.frameno,e.noshippent,o.prices,b.name AS n1,o.feeOps,o.feeLolo,o.feeDepo,o.feeParkir,o.feeKawalan,c.name AS c2,d.name AS cname,g.fleetplateno AS fleet,h.name AS driver,COALESCE ( SUM ( ccc.nominal ), 0 ) AS nominals,COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,COALESCE ( SUM ( o.prices ), 0 ) AS pricess FROM orders o LEFT JOIN orderload e ON o.code = e.order_code LEFT JOIN cities b ON e.id_citieso = b.id LEFT JOIN cities c ON e.id_citiesd = c.id LEFT JOIN customers d ON o.id_customers = d.id LEFT JOIN fleets g ON o.id_fleets = g.id LEFT JOIN cost ccc ON o.code = ccc.order_id LEFT JOIN fleettypes i ON g.id_fleettypes = i.id  LEFT JOIN drivers h ON o.id_drivers = h.id WHERE o.active = 1 AND e.nosummary = '$id' GROUP BY o.id, o.noVoucher,o.code,o.orderdate,o.ritase,e.id,e.type,e.noshippent,e.unitprice,e.frameno,o.prices,e.nosummary,g.fleetplateno,ccc.nominal,g.CODE,g.fleetnumber,i.name,d.NAME,b.name,c.name,o.feeOps,o.feeLolo,o.feeDepo,o.feeParkir,o.feeKawalan,h.name ";

      $result = $this->db->query($sql);  
      $datas['detail']= $result->result();

      $queryinv ="SELECT dbo.invoices.noinvoice, dbo.invoices.faktur, dbo.invoices.code, dbo.invoices.dateInvoice, dbo.customers.address_1, dbo.customers.npwp, dbo.customers.address_2, dbo.customers.address_3, dbo.customers.cabang, dbo.customers.norekening, dbo.invoices.nosummary, dbo.invoices.nominal, dbo.customers.id as idcust, dbo.invoices.ppn, dbo.customers.name FROM dbo.invoices INNER JOIN dbo.customers ON dbo.invoices.id_customers = dbo.customers.id WHERE dbo.invoices.noinvoice = '$id'";

      $resultinv = $this->db->query($queryinv);

      $datas['invoice']=$resultinv->result();


      $this->load->model('customers');
      $datas['customer']=$this->customers->getCustomer();

      $datas['id'] = $this->input->post('noinvoice');

      $view = $this->load->view('pages/printInvoicecopy',$datas,true);

      $mpdf->WriteHTML($view);

      $mpdf->Output();
    }
    elseif ($type == 3 ) {
            $this->load->library('M_pdf');

      $mpdf = $this->m_pdf->load([
        'mode' => 'utf-8',
        'format' => 'Legal-L'
      ]);


      $sql= "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,i.name,e.type,e.unitprice,e.frameno,e.noshippent,o.prices,b.name AS n1,o.feeOps,o.feeLolo,o.feeDepo,o.feeParkir,o.feeKawalan,c.name AS c2,d.name AS cname,g.fleetplateno AS fleet,h.name AS driver,COALESCE ( SUM ( ccc.nominal ), 0 ) AS nominals,COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,COALESCE ( SUM ( o.prices ), 0 ) AS pricess FROM orders o LEFT JOIN orderload e ON o.code = e.order_code LEFT JOIN cities b ON e.id_citieso = b.id LEFT JOIN cities c ON e.id_citiesd = c.id LEFT JOIN customers d ON o.id_customers = d.id LEFT JOIN fleets g ON o.id_fleets = g.id LEFT JOIN cost ccc ON o.code = ccc.order_id LEFT JOIN fleettypes i ON g.id_fleettypes = i.id  LEFT JOIN drivers h ON o.id_drivers = h.id WHERE o.active = 1 AND e.nosummary = '$id' GROUP BY o.id, o.noVoucher,o.code,o.orderdate,o.ritase,e.id,e.type,e.noshippent,e.unitprice,e.frameno,o.prices,e.nosummary,g.fleetplateno,ccc.nominal,g.CODE,g.fleetnumber,i.name,d.NAME,b.name,c.name,o.feeOps,o.feeLolo,o.feeDepo,o.feeParkir,o.feeKawalan,h.name ";

      $result = $this->db->query($sql);  
      $datas['detail']= $result->result();

      $queryinv ="SELECT dbo.invoices.noinvoice, dbo.invoices.faktur, dbo.invoices.code, dbo.invoices.dateInvoice, dbo.customers.address_1, dbo.customers.npwp, dbo.customers.address_2, dbo.customers.address_3, dbo.customers.cabang, dbo.customers.norekening, dbo.invoices.nosummary, dbo.invoices.nominal, dbo.customers.id as idcust, dbo.invoices.ppn, dbo.customers.name FROM dbo.invoices INNER JOIN dbo.customers ON dbo.invoices.id_customers = dbo.customers.id WHERE dbo.invoices.noinvoice = '$id'";

      $resultinv = $this->db->query($queryinv);

      $datas['invoice']=$resultinv->result();


      $this->load->model('customers');
      $datas['customer']=$this->customers->getCustomer();

      $datas['id'] = $this->input->post('noinvoice');

      $view = $this->load->view('pages/printinvsalesppn',$datas,true);

      $mpdf->WriteHTML($view);

      $mpdf->Output();
    }
    else {
            $this->load->library('M_pdf');

      $mpdf = $this->m_pdf->load([
        'mode' => 'utf-8',
        'format' => 'Legal-L'
      ]);


      $sql= "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,i.name,e.type,e.unitprice,e.frameno,e.noshippent,o.prices,b.name AS n1,o.feeOps,o.feeLolo,o.feeDepo,o.feeParkir,o.feeKawalan,c.name AS c2,d.name AS cname,g.fleetplateno AS fleet,h.name AS driver,COALESCE ( SUM ( ccc.nominal ), 0 ) AS nominals,COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,COALESCE ( SUM ( o.prices ), 0 ) AS pricess FROM orders o LEFT JOIN orderload e ON o.code = e.order_code LEFT JOIN cities b ON e.id_citieso = b.id LEFT JOIN cities c ON e.id_citiesd = c.id LEFT JOIN customers d ON o.id_customers = d.id LEFT JOIN fleets g ON o.id_fleets = g.id LEFT JOIN cost ccc ON o.code = ccc.order_id LEFT JOIN fleettypes i ON g.id_fleettypes = i.id  LEFT JOIN drivers h ON o.id_drivers = h.id WHERE o.active = 1 AND e.nosummary = '0002/XII/AST-MES/2020' GROUP BY o.id, o.noVoucher,o.code,o.orderdate,o.ritase,e.id,e.type,e.noshippent,e.unitprice,e.frameno,o.prices,e.nosummary,g.fleetplateno,ccc.nominal,g.CODE,g.fleetnumber,i.name,d.NAME,b.name,c.name,o.feeOps,o.feeLolo,o.feeDepo,o.feeParkir,o.feeKawalan,h.name ";

      $result = $this->db->query($sql);  
      $datas['detail']= $result->result();

      $queryinv ="SELECT dbo.invoices.noinvoice, dbo.invoices.faktur, dbo.invoices.code, dbo.invoices.dateInvoice, dbo.customers.address_1, dbo.customers.npwp, dbo.customers.address_2, dbo.customers.address_3, dbo.customers.cabang, dbo.customers.norekening, dbo.invoices.nosummary, dbo.invoices.nominal, dbo.customers.id as idcust, dbo.invoices.ppn, dbo.customers.name FROM dbo.invoices INNER JOIN dbo.customers ON dbo.invoices.id_customers = dbo.customers.id WHERE dbo.invoices.noinvoice = '0002/XII/AST-MES/2020'";

      $resultinv = $this->db->query($queryinv);

      $datas['invoice']=$resultinv->result();


      $this->load->model('customers');
      $datas['customer']=$this->customers->getCustomer();

      $datas['id'] = $this->input->post('noinvoice');

      $view = $this->load->view('pages/printinvaddcost',$datas,true);

      $mpdf->WriteHTML($view);

      $mpdf->Output();
    }

  }
  
}
