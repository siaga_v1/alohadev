<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class summarySlip extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('summarySlips');
      
  }
   
  // membuat fungsi index
  public function index()
  {
    $this->template->load('template','pages/summarySlip');
  } 

  public function summarylist()
  {
    $data['list'] = $this->summarySlips->listsummary();
    $this->template->load('template','pages/summarylist',$data);
  } 

  public function modalInvoice()
  {
    $id = $_POST['id'];

    $sql= "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,e.type,e.unitprice,e.frameno,e.noshippent,o.prices,b.name AS n1,c.name AS c2,d.name AS cname,g.fleetplateno AS fleet,h.name AS driver,COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,COALESCE ( SUM ( o.prices ), 0 ) AS pricess FROM orders o LEFT JOIN orderload e ON o.code = e.order_code LEFT JOIN cities b ON e.id_citieso = b.id LEFT JOIN cities c ON e.id_citiesd = c.id LEFT JOIN customers d ON o.id_customers = d.id  LEFT JOIN fleets g ON o.id_fleets = g.id LEFT JOIN drivers h ON o.id_drivers = h.id WHERE o.active = 1 AND e.nosummary = '$id' GROUP BY o.id, o.noVoucher,o.code,o.orderdate,o.ritase,e.id,e.type,e.noshippent,e.unitprice,e.frameno,o.prices,e.nosummary,g.fleetplateno,g.CODE,g.fleetnumber,d.NAME,b.name,c.name,h.name ";
      
    $result = $this->db->query($sql);

        
    $data['detail']= $result->result();

    $queryinv ="SELECT dbo.invoices.noinvoice, dbo.invoices.faktur, dbo.invoices.code, dbo.invoices.dateInvoice, dbo.customers.address_1, dbo.customers.npwp, dbo.customers.address_2, dbo.customers.address_3, dbo.customers.cabang, dbo.customers.norekening, dbo.invoices.nosummary, dbo.invoices.nominal, dbo.customers.id as idcust, dbo.invoices.ppn, dbo.customers.name FROM dbo.invoices INNER JOIN dbo.customers ON dbo.invoices.id_customers = dbo.customers.id WHERE dbo.invoices.nosummary = '$id'";

    $resultinv = $this->db->query($queryinv);

    $data['invoice']=$resultinv->result();


    $this->load->model('customers');
    $data['customer']=$this->customers->getCustomer();

    $data['id'] = $_POST['id'];

    $this->load->view('pages/modalInvoice',$data);
  } 

  public function getCustomers()
  {
    $id = $_POST['id'];

    $this->db->select('*');
    $this->db->where('active', '1');
    $this->db->where('id',$id );
    $result = $this->db->get('customers');

    foreach ($result->result_array() as $key) {
       $data['address_1'] = $key['address_1'];
       $data['address_2'] = $key['address_2'];
       $data['address_3'] = $key['address_3'];
       $data['phone'] = $key['phone'];
       $data['pic_name'] = $key['pic_name'];
    }

    echo json_encode($data);
  }

  public function loadRecord($page =0)

  {
      $perpage = 50;
    
      $data = array();
      $filter = array();
      $data['search'] = array();
      
      $limit  = $perpage;
      $offset = $page > 1 ? ($limit*$page)-$limit:0;
              
      $all_data =  $this->summarySlips->getReturnDO($filter);
      $data['total'] = count($all_data);
      $config['base_url'] = base_url() . '/returnDo/loadRecord';
      $config['use_page_numbers'] = TRUE;
      $config['total_rows'] = $data['total'];
      $config['per_page'] = $perpage;
      $config['num_links'] = 1;
      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['first_link'] = 'First';
      $config['first_tag_open'] = '<li class="prev page">';
      $config['first_tag_close'] = '</li>';
      $config['last_link'] = 'Last';
      $config['last_tag_open'] = '<li class="next page">';
      $config['last_tag_close'] = '</li>';
      $config['next_link'] = 'Next';
      $config['next_tag_open'] = '<li class="next page">';
      $config['next_tag_close'] = '</li>';
      $config['prev_link'] = 'Prev';
      $config['prev_tag_open'] = '<li class="prev page">';
      $config['prev_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a>';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li class="page">';
      $config['num_tag_close'] = '</li>';
      $config['anchor_class'] = 'follow_link';
      
      $this->pagination->initialize($config);
      $paginator = $this->pagination->create_links();
      $data['pagination'] = $paginator;
              
      //$data["page"] = $page;
      ///$data["offset"] = $offset;
      //$data["limit"] = $limit;
              
      $filter["limit"]  = $limit;
      $filter["page"] = $page;
      $filter["offset"] = $offset;
              
      $data["result"] =  $this->returnDOS->getReturnDO($filter);
      $data["row"] = $offset;
      echo json_encode($data);


  }

  public function invoice()
  {
    $this->load->library('M_pdf');

    $mpdf = $this->m_pdf->load([
    'mode' => 'utf-8',
    'format' => 'Legal'
    ]);
    
    $this->summarySlips->Simpan();
    
    $data['invoice']=$this->summarySlips->printInvoice();
    $view = $this->load->view('pages/printInvoice',$data,true);

    $mpdf->WriteHTML($view);

    $mpdf->Output();
  }

  public function loadsummary($page =0)

    {
      $perpage = 50;
    
      $data = array();
      $filter = array();
      $data['search'] = array();
      
      $limit  = $perpage;
      $offset = $page > 1 ? ($limit*$page)-$limit:0;
              
      $all_data =  $this->summarySlips->getReturnDO($filter);
      $data['total'] = count($all_data);
      $config['base_url'] = base_url() . '/returnDo/loadsummary';
      $config['use_page_numbers'] = TRUE;
      $config['total_rows'] = $data['total'];
      $config['per_page'] = $perpage;
      $config['num_links'] = 1;
      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['first_link'] = 'First';
      $config['first_tag_open'] = '<li class="prev page">';
      $config['first_tag_close'] = '</li>';
      $config['last_link'] = 'Last';
      $config['last_tag_open'] = '<li class="next page">';
      $config['last_tag_close'] = '</li>';
      $config['next_link'] = 'Next';
      $config['next_tag_open'] = '<li class="next page">';
      $config['next_tag_close'] = '</li>';
      $config['prev_link'] = 'Prev';
      $config['prev_tag_open'] = '<li class="prev page">';
      $config['prev_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a>';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li class="page">';
      $config['num_tag_close'] = '</li>';
      $config['anchor_class'] = 'follow_link';
      
      $this->pagination->initialize($config);
      $paginator = $this->pagination->create_links();
      $data['pagination'] = $paginator;
              
      //$data["page"] = $page;
      ///$data["offset"] = $offset;
      //$data["limit"] = $limit;
              
      $filter["limit"]  = $limit;
      $filter["page"] = $page;
      $filter["offset"] = $offset;
              
      $data["result"] =  $this->returnDOS->getReturnDO($filter);
      $data["row"] = $offset;
      echo json_encode($data);


  }

  public function search($page =0)
  { 
     if($this->input->get('export') == 'excel')
        {
            
            $this->load->library('PHPExcel');
            $data['search'] = array();
            $rows = $this->summarySlips->Search($data);
            date_default_timezone_set("Asia/Bangkok");
            $tanggalupdate = date("d-m-y H:i:s");

            if($this->input->get('nosummary') != '') {                
              $da['nosummary']          = $this->input->get('nosummary');
              $da['active']             = 1;
              $da['createby']           = $this->session->userdata('id');
              $da['createdatetime']     = $tanggalupdate;
                      
              $this->db->insert('invoices',$da);
            


              foreach ($rows as $key) {

                $dat['id_orderload'] = $key->idol;
                $dat['nosummary']    = $this->input->get('nosummary');
                $dat['active']       = 1;
                $dat['prices']       = $key->unitprice;
                $dat['createby']             = $this->session->userdata('id');
                $dat['createdatetime']       = $tanggalupdate;
                        
                $this->db->insert('invoicedetails',$dat);
              }
            }

            foreach ($rows as $key) {

              $d = $key->idol;
              $upda['nosummary'] = $this->input->get('nosummary');
                      
              
              $this->db->where('id', $d);
              $this->db->update('orderload', $upda);
            }

            
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()
               ->setCreator('Taylor Ren')
               ->setTitle('PHPExcel Demo')
               ->setLastModifiedBy('Taylor Ren')
               ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
               ->setSubject('PHP Excel manipulation')
               ->setKeywords('excel php office phpexcel lakers')
               ->setCategory('programming')
               ;

            $ews = $objPHPExcel->getSheet(0);
            $ews->setTitle('Data');

            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
            // Assign cell values

            $style = array(
		        'alignment' => array(
		            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		        )
		    );

			$styleArray = array(
			      'borders' => array(
			          'allborders' => array(
			              'style' => PHPExcel_Style_Border::BORDER_THIN
			          )
			      )
			  );
			$objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);
		    //$sheet->getStyle("A1:B1")->applyFromArray($style);
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'PT ARTINDO NUSA GRAHA');
            $objPHPExcel->getActiveSheet()->mergeCells('A1:N1');
            $objPHPExcel->getActiveSheet()->getStyle("A1:N1")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle("A1:N1")->getFont()->setSize(16);
            $objPHPExcel->getActiveSheet()->getStyle("A1:N1")->applyFromArray($style);
            


             $filtertext = "";
             if($this->input->get('fleetplateno') != '') { $filtertext .= "Plateno = ".$this->input->get('fleetplateno'); }
             if($this->input->get('type') != '') { $filtertext .= " Type = ".$this->input->get('type'); }
             if($this->input->get('customer') != '') { $filtertext .= " Customer = ".$this->input->get('customer'); }
             if($this->input->get('code') != '') { $filtertext .= " Fleet Code = ".$this->input->get('code'); }
             if($this->input->get('year') != '') { $filtertext .= " Year = ".$this->input->get('year'); }
             if($this->input->get('month') != '') { $filtertext .= " Month = ".date('F', strtotime("2018-".$this->input->get('month')."-01")); }
            $filters = ($filtertext != "" ? $filtertext : "All Data");
            
            $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Jl. Baru Terusan I Gusti Ngurah Rai No.9 Pondok Kopi');
            $objPHPExcel->getActiveSheet()->mergeCells('A2:N2');
            $objPHPExcel->getActiveSheet()->getStyle("A2:N2")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle("A2:N2")->getFont()->setSize(11);
            $objPHPExcel->getActiveSheet()->getStyle("A2:N2")->applyFromArray($style);
            //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Filter : ".$filters);
            //$objPHPExcel->getActiveSheet()->getStyle('A2:N2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            //$objPHPExcel->getActiveSheet()->getStyle('A2:N2')->getFill()->getStartColor()->setARGB('eaeaea');

            $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Jakarta Timur');
            $objPHPExcel->getActiveSheet()->mergeCells('A3:N3');
            $objPHPExcel->getActiveSheet()->getStyle("A3:N3")->getFont()->setBold(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle("A3:N3")->getFont()->setSize(11);
            $objPHPExcel->getActiveSheet()->getStyle("A3:N3")->applyFromArray($style);


            $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Rincian Pengiriman Kendaraan');
            //$objPHPExcel->getActiveSheet()->mergeCells('A3:N3');
            $objPHPExcel->getActiveSheet()->getStyle("A5:N5")->getFont()->setBold(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle("A5:N5")->getFont()->setSize(11);
            //$objPHPExcel->getActiveSheet()->getStyle("A3:N3")->applyFromArray($style);
            //$objPHPExcel->getActiveSheet()->getStyle('A3:N3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            //$objPHPExcel->getActiveSheet()->getStyle('A3:N3')->getFill()->getStartColor()->setARGB('cce6ff');


            $objPHPExcel->getActiveSheet()->setCellValue('L6', 'No Invoice ');
            //$objPHPExcel->getActiveSheet()->mergeCells('A3:N3');
            $objPHPExcel->getActiveSheet()->getStyle("A6:N6")->getFont()->setBold(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle("A6:N6")->getFont()->setSize(11);
            //$objPHPExcel->getActiveSheet()->getStyle("A6:N6")->applyFromArray($style);
            //$objPHPExcel->getActiveSheet()->getStyle('A3:N3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            //$objPHPExcel->getActiveSheet()->getStyle('A3:N3')->getFill()->getStartColor()->setARGB('cce6ff');
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 7,'No. Bukti');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 7,'Tgl. Kirim');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 7,'Tgl. Cetak');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 7,'∑DO');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 7,'No. SP-PDC');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 7,'Type');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 7,'No. Chasis / Rangka');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 7,'No Mesin');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 7,'No Iris');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 7,'Nama Supir');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 7,'No. Polisi');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 7,'Tujuan');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 7,'Biaya Pengiriman');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 7,'No. Summary Slip');
            
            if($rows != null)
            {
                $prices = 0;
                $ritases = 0;
                $allowance = 0;
                $allowanceaddss = 0;
                $allowancereds = 0;
                $no  = 1;
                $baris = 8;
                foreach($rows AS $row)
                {   
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $row->noVoucher);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->orderdate);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->orderdate);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->ritase);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->noshippent);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris, $row->type);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $baris, $row->frameno);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $baris, $row->machineno);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $baris, $row->irisno);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $baris, $row->driver);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $baris, $row->fleet);                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $baris, "".$row->n1." - ".$row->c2."");
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $baris, $row->unitprice);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $baris, $this->input->get('nosummary'));

                    $ritases += $row->ritase;
                    $allowance += $row->unitprice;
                    
                    $no++;
                    $baris++;
                }
                
                $barisfooter = $baris;
                $barisbaru = $barisfooter-1;
                $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $barisfooter, 'SUB TOTAL');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $barisfooter, $allowance);
                $objPHPExcel->getActiveSheet()->getStyle("A$barisfooter:N$barisfooter")->getFont()->setBold(TRUE);
                $PPN= 0.1 * $allowance;
                $barisbaru1 = $barisfooter+1;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $barisfooter+1, 'PPN');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $barisfooter+1, $PPN);
                $objPHPExcel->getActiveSheet()->getStyle("A$barisbaru1:N$barisbaru1")->getFont()->setBold(TRUE);

                $FULLTOTAL= $PPN + $allowance;
                $barisbaru2 = $barisfooter+2;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $barisfooter+2, 'TOTAL');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $barisfooter+2, $FULLTOTAL);
                $objPHPExcel->getActiveSheet()->getStyle("A$barisbaru2:N$barisbaru2")->getFont()->setBold(TRUE);
                //$objPHPExcel->getActiveSheet()->getStyle("A7:N$barisfooter")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
                //$objPHPExcel->getActiveSheet()->getStyle("A7:N$barisfooter")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
               	//$objPHPExcel->getActiveSheet()->getStyle("A7:N$barisfooter")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

               // $objPHPExcel->getActiveSheet()->getStyle("A7:N$barisfooter")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
                $barisbaru3 = $barisfooter+3;

			    function penyebut($nilai) {
			        $nilai = abs($nilai);
			        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
			        $temp = "";
			        if ($nilai < 12) {
			            $temp = " ". $huruf[$nilai];
			        } else if ($nilai <20) {
			            $temp = penyebut($nilai - 10). " Belas";
			        } else if ($nilai < 100) {
			            $temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
			        } else if ($nilai < 200) {
			            $temp = " Seratus" . penyebut($nilai - 100);
			        } else if ($nilai < 1000) {
			            $temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
			        } else if ($nilai < 2000) {
			            $temp = " Seribu" . penyebut($nilai - 1000);
			        } else if ($nilai < 1000000) {
			            $temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
			        } else if ($nilai < 1000000000) {
			            $temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
			        } else if ($nilai < 1000000000000) {
			            $temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
			        } else if ($nilai < 1000000000000000) {
			            $temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
			        }     
			        return $temp;
			    }
			 
			    function terbilang($nilai) {
			        if($nilai<0) {
			            $hasil = "minus ". trim(penyebut($nilai));
			        } else {
			            $hasil = trim(penyebut($nilai));
			        }           
			        return $hasil;
			    }

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru3, 'TERBILANG ');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $barisbaru3, terbilang($FULLTOTAL),' RUPIAH');
                $objPHPExcel->getActiveSheet()->getStyle("A$barisbaru3:N$barisbaru3")->getFont()->setBold(TRUE);
                $objPHPExcel->getActiveSheet()->getStyle("A$barisbaru3:N$barisbaru3")->getFont()->setItalic(TRUE);
                
                $barisbaru6 = $barisfooter+6;

	            date_default_timezone_set("Asia/Bangkok");
	            $tanggalan = date("d-F-Y");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru6, 'Jakarta,');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $barisbaru6, $tanggalan);
                $objPHPExcel->getActiveSheet()->getStyle("A$barisbaru3:N$barisbaru3")->getFont()->setBold(TRUE);

                $barisbaru10 = $barisfooter+10;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru10, 'IVAN HAMZAH');
                //$objPHPExcel->getActiveSheet()->getStyle("A$barisbaru3:N$barisbaru3")->getFont()->setBold(TRUE);
                $barisbaru11 = $barisfooter+11;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $barisbaru11, 'Direktur');
                //$objPHPExcel->getActiveSheet()->getStyle("A$barisbaru3:N$barisbaru3")->getFont()->setBold(TRUE);


                //LABEL
                $dsl1=array(
                new PHPExcel_Chart_DataSeriesValues('String', 'Data!$E$3', NULL, 1),
                );
                $dsl2=array(
                new PHPExcel_Chart_DataSeriesValues('String', 'Data!$D$3', NULL, 1),
                );
                $dsl3=array(
                new PHPExcel_Chart_DataSeriesValues('String', 'Data!$F$3', NULL, 1),
                );


                //AXIS
                $xal=array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$C$4:$C$'.$barisbaru.'', NULL, $barisbaru),
                );

                //SERIES

                //SERIES1 
                $dsv1=array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$E$4:$E$'.$barisbaru.'', NULL, $barisbaru),
                );
                $series1 = new PHPExcel_Chart_DataSeries(
                        PHPExcel_Chart_DataSeries::TYPE_BARCHART,       // plotType
                        PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,  // plotGrouping
                        range(0, count($dsv1)-1),          // plotOrder
                        $dsl1,                             // plotLabel
                        $xal,                               // plotCategory
                        $dsv1                             // plotValues
                    );
                    //  Set additional dataseries parameters
                    //      Make it a vertical column rather than a horizontal bar graph
                    $series1->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

                // SERIES2
                $dsv2=array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$D$4:$D$'.$barisbaru.'', NULL, $barisbaru),
                );
                $series2 = new PHPExcel_Chart_DataSeries(
                        PHPExcel_Chart_DataSeries::TYPE_LINECHART,      // plotType
                        PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   // plotGrouping
                        range(0, count($dsv2)-1),          // plotOrder
                        $dsl2,                             // plotLabel
                        NULL,                                           // plotCategory
                        $dsv2                              // plotValues
                    );


                $dsv3=array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$f$4:$f$'.$barisbaru.'', NULL, $barisbaru),
                );

                $series3 = new PHPExcel_Chart_DataSeries(
                        PHPExcel_Chart_DataSeries::TYPE_AREACHART,      // plotType
                        PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   // plotGrouping
                        range(0, count($dsv2)-1),          // plotOrder
                        $dsl3,                             // plotLabel
                        NULL,                                           // plotCategory
                        $dsv3                              // plotValues
                    );
                

            //$ds=new PHPExcel_Chart_DataSeries(
            //        PHPExcel_Chart_DataSeries::TYPE_LINECHART,
            //        PHPExcel_Chart_DataSeries::GROUPING_STANDARD,
            //        range(0, count($dsv)-1),
            //        $dsl,
            //        $xal,
            //        $dsv
            //        );

            //  Set the series in the plot area
            $pa = new PHPExcel_Chart_PlotArea(NULL, array($series1));
            //  Set the chart legend
            $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

            $title = new PHPExcel_Chart_Title('Average Weather Chart for Crete');

            //$pa=new PHPExcel_Chart_PlotArea(NULL, array($ds));
            $legend=new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
            $title = new PHPExcel_Chart_Title('RITASE / SALES');
            $chart= new PHPExcel_Chart(
                    'chart1',
                    $title,
                    $legend,
                    $pa,
                    true,
                    0,
                    NULL, 
                    NULL
                    );
            $chart->setTopLeftPosition('K1');
            $chart->setBottomRightPosition('AE23');
            $ews->addChart($chart);
            }
            else
            {
                $objPHPExcel->getActiveSheet()->setCellValue('A4:I4', 'Data Not Found');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('e6f2ff');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(14);
            }
            

            

            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            
            $writer->setIncludeCharts(false);
            //$writer->save('output.xlsx');
            // Save it as an excel 2003 file
            //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            //Nama File
            header('Content-Disposition: attachment;filename="TruckOrderSummary'.date('dmy').'.xlsx"');
            $writer->save('php://output');
            
            
        }
        else
        {
          $perpage = 50;
        
          $data = array();
          $filter = array();
          $data['seach'] = array();
          
          $limit  = $perpage;
          $offset = $page > 1 ? ($limit*$page)-$limit:0;
                  
          $all_data =  $this->summarySlips->Search($filter);
          $data['total'] = count($all_data);
          $config['base_url'] = base_url() . '/summarySlip/search';
          $config['use_page_numbers'] = TRUE;
          $config['total_rows'] = $data['total'];
          $config['per_page'] = $perpage;
          $config['num_links'] = 1;
          $config['full_tag_open'] = '<ul class="pagination">';
          $config['full_tag_close'] = '</ul>';
          $config['first_link'] = 'First';
          $config['first_tag_open'] = '<li class="prev page">';
          $config['first_tag_close'] = '</li>';
          $config['last_link'] = 'Last';
          $config['last_tag_open'] = '<li class="next page">';
          $config['last_tag_close'] = '</li>';
          $config['next_link'] = 'Next';
          $config['next_tag_open'] = '<li class="next page">';
          $config['next_tag_close'] = '</li>';
          $config['prev_link'] = 'Prev';
          $config['prev_tag_open'] = '<li class="prev page">';
          $config['prev_tag_close'] = '</li>';
          $config['cur_tag_open'] = '<li class="active"><a>';
          $config['cur_tag_close'] = '</a></li>';
          $config['num_tag_open'] = '<li class="page">';
          $config['num_tag_close'] = '</li>';
          $config['anchor_class'] = 'follow_link';
          
          $this->pagination->initialize($config);
          $paginator = $this->pagination->create_links();
          $data['pagination'] = $paginator;
                  
          //$data["page"] = $page;
          ///$data["offset"] = $offset;
          //$data["limit"] = $limit;
                  
          $filter["limit"]  = $limit;
          $filter["page"] = $page;
          $filter["offset"] = $offset;
                  
          $data["result"] =   $this->summarySlips->Search($filter);
          $data["row"] = $offset;
          echo json_encode($data);
        }
  }

  /*public function searchtrisya($page =0)
  { 
     if($this->input->get('export') == 'excel')
        {
            
            $this->load->library('PHPExcel');
            $data['search'] = array();
            $rows = $this->summarySlips->Search($data);
            date_default_timezone_set("Asia/Bangkok");
            $tanggalupdate = date("d-m-y H:i:s");

            if($this->input->get('nosummary') != '') {                
              $da['nosummary']          = $this->input->get('nosummary');
              $da['active']             = 1;
              $da['createby']           = $this->session->userdata('id');
              $da['createdatetime']     = $tanggalupdate;
                      
              $this->db->insert('invoices',$da);
            


              foreach ($rows as $key) {

                $dat['id_orderload'] = $key->idol;
                $dat['nosummary']    = $this->input->get('nosummary');
                $dat['active']       = 1;
                $dat['prices']       = $key->unitprice;
                $dat['createby']             = $this->session->userdata('id');
                $dat['createdatetime']       = $tanggalupdate;
                        
                $this->db->insert('invoicedetails',$dat);
              }
            }

            foreach ($rows as $key) {

              $d = $key->idol;
              $upda['nosummary'] = $this->input->get('nosummary');
                      
              
              $this->db->where('id', $d);
              $this->db->update('orderload', $upda);
            }

            
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()
               ->setCreator('Taylor Ren')
               ->setTitle('PHPExcel Demo')
               ->setLastModifiedBy('Taylor Ren')
               ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
               ->setSubject('PHP Excel manipulation')
               ->setKeywords('excel php office phpexcel lakers')
               ->setCategory('programming')
               ;

            $ews = $objPHPExcel->getSheet(0);
            $ews->setTitle('Data');

            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
            // Assign cell values
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Summary Slip');
            $objPHPExcel->getActiveSheet()->mergeCells('A1:L1');
            $objPHPExcel->getActiveSheet()->getStyle("A1:L1")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()->getStartColor()->setARGB('ffffcc');
            $objPHPExcel->getActiveSheet()->getStyle("A1:L1")->getFont()->setSize(16);
            


             $filtertext = "";
             if($this->input->get('fleetplateno') != '') { $filtertext .= "Plateno = ".$this->input->get('fleetplateno'); }
             if($this->input->get('type') != '') { $filtertext .= " Type = ".$this->input->get('type'); }
             if($this->input->get('customer') != '') { $filtertext .= " Customer = ".$this->input->get('customer'); }
             if($this->input->get('code') != '') { $filtertext .= " Fleet Code = ".$this->input->get('code'); }
             if($this->input->get('year') != '') { $filtertext .= " Year = ".$this->input->get('year'); }
             if($this->input->get('month') != '') { $filtertext .= " Month = ".date('F', strtotime("2018-".$this->input->get('month')."-01")); }
            $filters = ($filtertext != "" ? $filtertext : "All Data");
            
            $objPHPExcel->getActiveSheet()->mergeCells('A2:L2');
            $objPHPExcel->getActiveSheet()->getStyle("A2:L2")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A2:L2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A2:L2')->getFill()->getStartColor()->setARGB('eaeaea');
            $objPHPExcel->getActiveSheet()->getStyle("A2:L2")->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Filter : ".$filters);
            
            $objPHPExcel->getActiveSheet()->getStyle("A3:L3")->getFont()->setBold(false);
            $objPHPExcel->getActiveSheet()->getStyle('A3:L3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A3:L3')->getFill()->getStartColor()->setARGB('cce6ff');
            $objPHPExcel->getActiveSheet()->getStyle("A3:L3")->getFont()->setSize(12);
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3,'No. Bukti');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3,'Tgl. Kirim');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3,'Tgl. Cetak');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3,'∑DO');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3,'No. SP-PDC');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 3,'Type');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 3,'No. Chasis / Rangka');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 3,'No Mesin');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 3,'No Iris');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 3,'Nama Supir');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 3,'No. Polisi');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, 3,'Tujuan');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, 3,'Biaya Pengiriman');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, 3,'No. Summary Slip');
            
            if($rows != null)
            {
                $prices = 0;
                $ritases = 0;
                $allowance = 0;
                $allowanceaddss = 0;
                $allowancereds = 0;
                $no  = 1;
                $baris = 4;
                foreach($rows AS $row)
                {   
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $baris, $row->noVoucher);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $baris, $row->orderdate);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $baris, $row->orderdate);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $baris, $row->ritase);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $baris, $row->noshippent);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $baris, $row->type);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $baris, $row->frameno);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $baris, $row->machineno);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $baris, $row->irisno);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $baris, $row->driver);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $baris, $row->fleet);                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $baris, "".$row->n1." - ".$row->c2."");
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $baris, $row->unitprice);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $baris, $this->input->get('nosummary'));

                    $ritases += $row->ritase;
                    $allowance += $row->unitprice;
                    
                    $no++;
                    $baris++;
                }
                
                $barisfooter = $baris;
                $barisbaru = $barisfooter-1;
                $objPHPExcel->getActiveSheet()->mergeCells("A$barisfooter:C$barisfooter");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $barisfooter, $ritases);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $barisfooter, $allowance);
                
                //LABEL
                $dsl1=array(
                new PHPExcel_Chart_DataSeriesValues('String', 'Data!$E$3', NULL, 1),
                );
                $dsl2=array(
                new PHPExcel_Chart_DataSeriesValues('String', 'Data!$D$3', NULL, 1),
                );
                $dsl3=array(
                new PHPExcel_Chart_DataSeriesValues('String', 'Data!$F$3', NULL, 1),
                );


                //AXIS
                $xal=array(
                    new PHPExcel_Chart_DataSeriesValues('String', 'Data!$C$4:$C$'.$barisbaru.'', NULL, $barisbaru),
                );

                //SERIES

                //SERIES1 
                $dsv1=array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$E$4:$E$'.$barisbaru.'', NULL, $barisbaru),
                );
                $series1 = new PHPExcel_Chart_DataSeries(
                        PHPExcel_Chart_DataSeries::TYPE_BARCHART,       // plotType
                        PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,  // plotGrouping
                        range(0, count($dsv1)-1),          // plotOrder
                        $dsl1,                             // plotLabel
                        $xal,                               // plotCategory
                        $dsv1                             // plotValues
                    );
                    //  Set additional dataseries parameters
                    //      Make it a vertical column rather than a horizontal bar graph
                    $series1->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

                // SERIES2
                $dsv2=array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$D$4:$D$'.$barisbaru.'', NULL, $barisbaru),
                );
                $series2 = new PHPExcel_Chart_DataSeries(
                        PHPExcel_Chart_DataSeries::TYPE_LINECHART,      // plotType
                        PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   // plotGrouping
                        range(0, count($dsv2)-1),          // plotOrder
                        $dsl2,                             // plotLabel
                        NULL,                                           // plotCategory
                        $dsv2                              // plotValues
                    );


                $dsv3=array(
                    new PHPExcel_Chart_DataSeriesValues('Number', 'Data!$f$4:$f$'.$barisbaru.'', NULL, $barisbaru),
                );

                $series3 = new PHPExcel_Chart_DataSeries(
                        PHPExcel_Chart_DataSeries::TYPE_AREACHART,      // plotType
                        PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   // plotGrouping
                        range(0, count($dsv2)-1),          // plotOrder
                        $dsl3,                             // plotLabel
                        NULL,                                           // plotCategory
                        $dsv3                              // plotValues
                    );
                

            //$ds=new PHPExcel_Chart_DataSeries(
            //        PHPExcel_Chart_DataSeries::TYPE_LINECHART,
            //        PHPExcel_Chart_DataSeries::GROUPING_STANDARD,
            //        range(0, count($dsv)-1),
            //        $dsl,
            //        $xal,
            //        $dsv
            //        );

            //  Set the series in the plot area
            $pa = new PHPExcel_Chart_PlotArea(NULL, array($series1));
            //  Set the chart legend
            $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

            $title = new PHPExcel_Chart_Title('Average Weather Chart for Crete');

            //$pa=new PHPExcel_Chart_PlotArea(NULL, array($ds));
            $legend=new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
            $title = new PHPExcel_Chart_Title('RITASE / SALES');
            $chart= new PHPExcel_Chart(
                    'chart1',
                    $title,
                    $legend,
                    $pa,
                    true,
                    0,
                    NULL, 
                    NULL
                    );
            $chart->setTopLeftPosition('K1');
            $chart->setBottomRightPosition('AE23');
            $ews->addChart($chart);
            }
            else
            {
                $objPHPExcel->getActiveSheet()->setCellValue('A4:I4', 'Data Not Found');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFill()->getStartColor()->setARGB('e6f2ff');
                $objPHPExcel->getActiveSheet()->getStyle("A4:I4")->getFont()->setSize(14);
            }
            

            

            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            
            $writer->setIncludeCharts(false);
            //$writer->save('output.xlsx');
            // Save it as an excel 2003 file
            //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            //Nama File
            header('Content-Disposition: attachment;filename="TruckOrderSummary'.date('dmy').'.xlsx"');
            $writer->save('php://output');
            
            
        }
        else
        {
          $perpage = 50;
        
          $data = array();
          $filter = array();
          $data['seach'] = array();
          
          $limit  = $perpage;
          $offset = $page > 1 ? ($limit*$page)-$limit:0;
                  
          $all_data =  $this->summarySlips->Searchtrisya($filter);
          $data['total'] = count($all_data);
          $config['base_url'] = base_url() . '/summarySlip/searchtrisya';
          $config['use_page_numbers'] = TRUE;
          $config['total_rows'] = $data['total'];
          $config['per_page'] = $perpage;
          $config['num_links'] = 1;
          $config['full_tag_open'] = '<ul class="pagination">';
          $config['full_tag_close'] = '</ul>';
          $config['first_link'] = 'First';
          $config['first_tag_open'] = '<li class="prev page">';
          $config['first_tag_close'] = '</li>';
          $config['last_link'] = 'Last';
          $config['last_tag_open'] = '<li class="next page">';
          $config['last_tag_close'] = '</li>';
          $config['next_link'] = 'Next';
          $config['next_tag_open'] = '<li class="next page">';
          $config['next_tag_close'] = '</li>';
          $config['prev_link'] = 'Prev';
          $config['prev_tag_open'] = '<li class="prev page">';
          $config['prev_tag_close'] = '</li>';
          $config['cur_tag_open'] = '<li class="active"><a>';
          $config['cur_tag_close'] = '</a></li>';
          $config['num_tag_open'] = '<li class="page">';
          $config['num_tag_close'] = '</li>';
          $config['anchor_class'] = 'follow_link';
          
          $this->pagination->initialize($config);
          $paginator = $this->pagination->create_links();
          $data['pagination'] = $paginator;
                  
          //$data["page"] = $page;
          ///$data["offset"] = $offset;
          //$data["limit"] = $limit;
                  
          $filter["limit"]  = $limit;
          $filter["page"] = $page;
          $filter["offset"] = $offset;
                  
          $data["result"] =   $this->summarySlips->Searchtrisya($filter);
          $data["row"] = $offset;
          echo json_encode($data);
        }
  }*/

  public function saveCities()
  {
    $this->cities->Simpan();
    redirect('City?msg=Save Success');
  }



  public function deleteCities()
  {
    $this->cities->delete();
    redirect('city?msg=Delete Success');
   
  }
  
}
?>