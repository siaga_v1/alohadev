<?php if (!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
class supplier extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Suppliers');
  }

  public function index()
  {

    $data['supplier'] = $this->Suppliers->getSupplier();
    $data['id'] = $this->Suppliers->getLastID();


    $this->template->load('template', 'pages/Supplier', $data);
  }

  public function autoComplete()
  {
    $q = trim($this->input->get("q")) != "" ? trim(strtolower($this->db->escape_str($this->input->get("q")))) : "";

    $this->db->select("id, name");
    $this->db->where("active", 1);
    if ($q != "") {
      $this->db->like("LOWER(name)", $q);
    }
    $result = $this->db->get("supplier");
    $rows = $result->result();

    if (count($rows) > 0) {
      foreach ($rows as $row) {
        $data[] = array('id' => $row->id, 'text' => $row->name);
      }
    } else {
      $data[] = array('id' => '', 'text' => 'No Data Found');
    }

    echo json_encode($data);
  }

  public function saveEdit()
  {
    $this->Suppliers->editSupplier();
    redirect('city?msg=Save Success');
  }

  public function modaledit()
  {
    $data['Supplier'] = $this->Suppliers->getOneOrder();


    $this->load->view('pages/modalSupplier', $data);
  }

  public function saveSupplier()
  {
    $this->Suppliers->Simpan();
    redirect('supplier?msg=Save Success');
  }



  public function deleteSupplier()
  {
    $this->Suppliers->delete();
    redirect('supplier?msg=Delete Success');
  }
}
