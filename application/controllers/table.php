<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class table extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    //mengambil library session
     $this->load->library(array('session'));
     //mengambil url di helper
     $this->load->helper('url');
     // mengambil m_login yang berada di folder model yang tadi di buat
     $this->load->model('m_PGSPage');
    //mengambil database
     $this->load->database();
      
      
  }
   
  // membuat fungsi index
  public function index()
  {

    $data['armada']=$this->m_PGSPage->ambilArmada()->result();
    $this->template->load('template','Pages/table',$data);
  } 
   
}
?>