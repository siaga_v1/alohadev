<?php if(!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');
//membuat Class home , sesuai nama file home.php
class target extends CI_Controller
{
    // udah tau ini apa :p
  public function __construct()
  {
    parent::__construct();
    $this->load->model('targets');
      
  }
   
  // membuat fungsi index
  public function index()
  {

    $data['targets']=$this->targets->getTarget();
    $data['id']=$this->targets->getLastID();


    $this->template->load('template','pages/targets',$data);
  } 

  public function saveEdit()
  {
    $this->targets->editTarget();
    redirect('target?msg=Save Success');
  }

  public function modaledit()
  {
    $data['target']=$this->targets->getOneOrder();


    $this->load->view('pages/modaltarget',$data);

   
  }

  public function saveTarget()
  {
    $this->targets->Simpan();
    redirect('Target?msg=Save Success');
  }



  public function deleteCities()
  {
    $this->cities->delete();
    redirect('city?msg=Delete Success');
   
  }
  
}
?>