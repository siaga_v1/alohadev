<?php

function is_logged_in()
{

    $ci = get_instance();

    if (!$ci->session->userdata('id')) {
        redirect('login', 'location', 301);
    } else {
        $segment2 = $ci->uri->segment(2) != "" ? '/' . $ci->uri->segment(2) : "";

        $menu = $ci->uri->segment(1) . $segment2;
        $menuQuery = $ci->db->get_where('menus', ['url' => $menu])->row();
        $id_menus = $menuQuery->id;
        $id_userroles = $ci->session->userdata('role');

        $userAccess = $ci->db->get_where('usermenus', ['id_userroles' => $id_userroles, 'id_menus' => $id_menus]);
        if ($userAccess->num_rows() < 1) {
            redirect('blocked', 'location', 301);
        }
    }
}

function has_login()
{

    $ci = get_instance();

    if ($ci->session->userdata('id') != null) {
        redirect('', 'location', 301);
    }
}

function penyebut($nilai)
{
    $nilai = abs($nilai);
    $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " " . $huruf[$nilai];
    } else if ($nilai < 20) {
        $temp = penyebut($nilai - 10) . " Belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai / 10) . " Puluh" . penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " Seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai / 100) . " Ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " Seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai / 1000) . " Ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai / 1000000) . " Juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai / 1000000000) . " Milyar" . penyebut(fmod($nilai, 1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai / 1000000000000) . " Trilyun" . penyebut(fmod($nilai, 1000000000000));
    }
    return $temp;
}

function terbilang($nilai)
{
    if ($nilai < 0) {
        $hasil = "minus " . trim(penyebut($nilai));
    } else {
        $hasil = trim(penyebut($nilai));
    }
    return $hasil;
}

function get_periode_months()
{
    $array = array(
        1 => "Januari",
        2 => "Februari",
        3 => "Maret",
        4 => "April",
        5 => "Mei",
        6 => "Juni",
        7 => "Juli",
        8 => "Agustus",
        9 => "September",
        10 => "Oktober",
        11 => "November",
        12 => "Desember"
    );
    return $array;
}

function get_periode_years()
{
    $array = array();
    for ($a = (int)date("Y"); $a >= ((int)date("Y") - 3); $a--) {
        array_push($array, $a);
    }

    return $array;
}

function get_isvalid($param = "")
{
    $array = array("Y" => "Yes", "N" => "No");

    if (isset($param) && $param != "") {
        return $array[$param];
    } else {
        return $array;
    }
}
function tgl_singkat($tanggal)
{
    $bulan = array(
        1 =>   'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Agu',
        'Sep',
        'Okt',
        'Nov',
        'Des'
    );
    $pecahkan = explode('-', $tanggal);


    return $pecahkan[2] . '-' . $bulan[(int)$pecahkan[1]] . '-' . $pecahkan[0];
}
function get_stnk_notif()
{
    $ci = get_instance();

    $ci->load->model("modelNotifikasi");
    return $ci->modelNotifikasi->notifikasiSTNK();
}

function get_update_saldo()
{
    $ci = get_instance();

    $result = $ci->db->query("SELECT
    a.name,
    a.id,
    a.type,
    totalnominal = ( SELECT SUM ( C.nominal ) FROM [cost] c WHERE c.coa = a.id AND c.active = 1 and c.coa = 4),
    totalpengeluaran = ( SELECT SUM ( C.nominal ) FROM [cost] c WHERE c.coa = a.id AND c.active = 1 and c.cost_type = 1 )
FROM
    chartofaccount a 
WHERE
    a.active = 1 
ORDER BY
    a.id ASC ");
    //$row = $result->row();

    $KAS = 0;
    $KAWS = 0;
    $KWSA = 0;


    foreach ($result->result_array() as $key) {
        $KAS += $key['totalnominal'];
        $KAWS += $key['totalpengeluaran'];
    }

    $KWSA = $KAS - $KAWS;



    return $KWSA;
}

function get_update_saldo_kas()
{
    $ci = get_instance();

    $ci->db->select("COALESCE(SUM(prices),0) AS prices");
    $ci->db->where("active", 1);
    $result = $ci->db->get("cashs");
    $cahs = $result->row() != null && $result->row()->prices > 0 ? $result->row()->prices : 0;

    $ci->db->select("COALESCE(SUM(price),0) AS price");
    $ci->db->where("active", 1);
    $ci->db->where("status", "APPROVED");
    $result = $ci->db->get("debitcredit");
    $outs = $result->row() != null && $result->row()->price > 0 ? $result->row()->price : 0;

    return ($cahs - $outs);
}

function get_costcomponent_type($param = 0)
{
    $array = array(1 => "Additional", 2 => "Reductional");

    if (isset($param) && $param > 0) {
        return $array[$param];
    } else {
        return $array;
    }
}

function get_billingcomponent_type($param = 0)
{
    $array = array(1 => "Full Price", 2 => "Percent");

    if (isset($param) && $param > 0) {
        return $array[$param];
    } else {
        return $array;
    }
}

function get_status_invoice($param = "N")
{
    $param = $param == "" ? "N" : $param;
    $array = array("N" => "New Invoice", "S" => "Invoice Sent", "P" => "Paid (Lunas)");

    if (isset($param) && $param != "") {
        return $array[$param];
    } else {
        return $array;
    }
}

function get_bound_type($param = "")
{
    $param = $param == "" ? "" : $param;
    $array = array(1 => "INBOUND", 2 => "OUTBOUND");

    if (isset($param) && $param != "") {
        return $array[$param];
    } else {
        return $array;
    }
}

function get_absensi_type($param = "")
{
    $param = $param == "" ? "" : $param;
    $array = array(1 => "Hadir", 2 => "Izin", 3 => "Minggu / Hari Besar");

    if (isset($param) && $param != "") {
        return $array[$param];
    } else {
        return $array;
    }
}

function get_user_photo()
{
    $ci = get_instance();
    $ci->db->select("file");
    $ci->db->where("id", $ci->session->userdata("id"));
    $result = $ci->db->get("users");
    $file = $result->row() != null && $result->row()->file != "" ? $result->row()->file : "assets/img/profile_small.png";

    return $file;
}
