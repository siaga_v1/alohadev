<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class invoicedetails extends CI_Model{

    public function __construct(){
        parent::__construct();
    }
    
    public function selectAll()
    {
        $id_fin_invoices = $this->input->post("id_fin_invoices") > 0 ? $this->input->post("id_fin_invoices") : 0;
        $shipment = trim($this->input->post_get("shipment")) != "" ? $this->db->escape_str($this->input->post_get("shipment"))  : "";
    
        $where = "";
        if($shipment != "") {
            $where .= " AND (o.shippment LIKE '%$shipment%') ";
        }
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( A.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
        SELECT A.* FROM 
            ( SELECT ROW_NUMBER() OVER (Order by o.orderdate) AS RowNum, 
                a.*,
                o.shippment,
                o.orderdate,
                o.prices,
                o.pricesadd,
                o.allowances,
                o.allowanceadds,
                o.allowancereds,
                d.name AS driver_name,
                f.fleetplateno,
                ft.name AS fleettype_name,
                droplocations = STUFF((
                      SELECT '<br/>' + name
                      FROM orderdroplocations
                      WHERE (id_orders = o.id)
                      FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 5, ''),
                cio.name AS origin,
                cid.name AS destination,
                cu.name AS customer_name
              FROM fin_invoice_details a
                LEFT JOIN orders o ON a.id_orders = o.id AND o.active = 1
                LEFT JOIN customers cu ON o.id_customers = cu.id
                LEFT JOIN cities cio ON o.id_citieso = cio.id
                LEFT JOIN cities cid ON o.id_citiesd = cid.id
                LEFT JOIN fleets f ON o.id_fleets = f.id
                LEFT JOIN drivers d ON o.id_drivers = d.id
                LEFT JOIN fleettypes ft ON f.id_fleettypes = ft.id
              WHERE
                a.active = 1
                AND a.id_fin_invoices = $id_fin_invoices
                $where
            )A
        WHERE 
            A.id IS NOT NULL
            $paging
        ";
        
        $result = $this->db->query($sql);
        $rows = $result->result();
        /**
        $myArray = array();
        foreach($rows AS $row) {
            $shipment = explode("_", $row->shippment);
            $row->shippment = count($shipment) > 1 ? $shipment[0] : $row->shippment;
            array_push($myArray, $row);
        }
        */
        return $rows;
        
    }
    
    public function selectAllOrderForInvoice()
    {
        $id_customers = $this->input->post("id_customers") > 0 ? $this->input->post("id_customers") : 0;
    
        $where = "";
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( A.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
        SELECT A.* FROM 
            ( SELECT ROW_NUMBER() OVER (Order by o.orderdate) AS RowNum, 
                o.id,
                o.shippment,
                o.orderdate,
                d.name AS driver_name,
                o.description,
                f.fleetplateno,
                ft.name AS fleettype_name,
                cio.name AS origin,
                cid.name AS destination,
                cu.name AS customer_name,
                o.prices,
                o.pricesadd,
                o.allowances,
                o.allowanceadds,
                o.allowancereds
              FROM orders o
                LEFT JOIN fin_invoice_details a ON o.id = a.id_orders AND a.active = 1
                LEFT JOIN customers cu ON o.id_customers = cu.id
                LEFT JOIN cities cio ON o.id_citieso = cio.id
                LEFT JOIN cities cid ON o.id_citiesd = cid.id
                LEFT JOIN fleets f ON o.id_fleets = f.id
                LEFT JOIN drivers d ON o.id_drivers = d.id
                LEFT JOIN fleettypes ft ON f.id_fleettypes = ft.id
              WHERE
                o.active = 1
                AND a.id IS NULL
                AND o.id_customers = $id_customers
                $where
            )A
        WHERE 
            A.id IS NOT NULL
            $paging
        ";
        
        $result = $this->db->query($sql);
        $rows = $result->result();
        
        $newArray = array();
        foreach($rows AS $row) {
            $shipment = explode("_", $row->shippment);
            $row->shippment = count($shipment) > 1 ? $shipment[0] : $row->shippment;
            array_push($newArray, $row);
        }
        
        return (object)$newArray;
    }
    
    public function selectAllForType0($data = null)
    {
        $id_fin_invoices = isset($data["id_fin_invoices"]) && $data["id_fin_invoices"] > 0 ? $data["id_fin_invoices"] : 0;
        $where = "";
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
            SELECT B.* FROM 
            (  
              SELECT
                ROW_NUMBER() OVER (Order by A.orderdate) AS RowNum,
            	A.orderdate,
                A.origin,
            	A.destination,
                A.invoiceno,
                A.customer_name,
                SUM(A.loadqty) AS loadqty,
            	shippment = STUFF((
                      SELECT '<br/>' + o.shippment
                      FROM orders o
                      WHERE (CONVERT(DATE, o.orderdate) = A.orderdate 
            									AND o.id_citiesd = A.id_citiesd
            									AND o.id_fleets = A.id_fleets)
                      FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 5, ''),
            	A.fleetplateno,
            	A.fleettype_name,
            	SUM(A.prices) AS prices
            FROM (
            	SELECT
            		a.id_orders,
            		CONVERT(DATE, o.orderdate) orderdate,
            		(o.prices + COALESCE(o.pricesadd,0)) AS prices,
            		o.allowances,
            		o.allowanceadds,
                    o.loadqty,
            		d.name AS driver_name,
            		o.id_fleets,
            		f.fleetplateno,
            		ft.name AS fleettype_name,
            		o.id_citieso,
            		cio.name AS origin,
            		o.id_citiesd,
                    fi.invoiceno,
                    o.description,
            		cid.name AS destination,
            		cu.name AS customer_name
            	FROM
            		fin_invoice_details a
            		LEFT JOIN orders o ON a.id_orders = o.id AND o.active = 1
            		LEFT JOIN customers cu ON o.id_customers = cu.id
            		LEFT JOIN cities cio ON o.id_citieso = cio.id
            		LEFT JOIN cities cid ON o.id_citiesd = cid.id
            		LEFT JOIN fleets f ON o.id_fleets = f.id
            		LEFT JOIN drivers d ON o.id_drivers = d.id
            		LEFT JOIN fleettypes ft ON f.id_fleettypes = ft.id
                    LEFT JOIN fin_invoices fi ON a.id_fin_invoices = fi.id
            	WHERE
            		a.active = 1
            		AND a.id_fin_invoices = $id_fin_invoices
            ) A
            GROUP BY
                A.invoiceno,
            	A.orderdate,
                A.origin,
            	A.destination,
                A.customer_name,
            	A.id_citiesd,
            	A.id_fleets,
            	A.fleetplateno,
            	A.fleettype_name
                
            )B
        WHERE 
            B.shippment IS NOT NULL
            $paging
        ORDER BY
            orderdate
        ";
        
        $result = $this->db->query($sql);
        $rows = $result->result();
        
        $newArray = array();
        foreach($rows AS $row) {
            $shipment = explode("_", $row->shippment);
            $row->shippment = count($shipment) > 1 ? $shipment[0] : $row->shippment;
            array_push($newArray, $row);
        }
        
        return (object)$newArray;
    }
    
    public function selectAllForType1($data = null)
    {
        $id_fin_invoices = isset($data["id_fin_invoices"]) && $data["id_fin_invoices"] > 0 ? $data["id_fin_invoices"] : 0;
        $where = "";
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( A.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
        SELECT A.* FROM 
            ( SELECT ROW_NUMBER() OVER (Order by o.orderdate) AS RowNum, 
                a.*,
                o.shippment,
                o.noVoucher,
                o.orderdate,
                (o.prices + COALESCE(o.pricesadd,0))AS prices,
                o.allowances,
                o.allowanceadds,
                o.description,
                o.loadqty,
                d.name AS driver_name,
                droplocations = STUFF((
                      SELECT '<br/>' + name
                      FROM orderdroplocations
                      WHERE (id_orders = o.id)
                      FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 5, ''),
                f.fleetplateno,
                ft.name AS fleettype_name,
                fi.invoiceno,
                cio.name AS origin,
                cid.name AS destination,
                cu.name AS customer_name,
                r.allowance AS unit_price
              FROM fin_invoice_details a
                LEFT JOIN orders o ON a.id_orders = o.id AND o.active = 1
                LEFT JOIN customers cu ON o.id_customers = cu.id
                LEFT JOIN cities cio ON o.id_citieso = cio.id
                LEFT JOIN cities cid ON o.id_citiesd = cid.id
                LEFT JOIN fleets f ON o.id_fleets = f.id
                LEFT JOIN drivers d ON o.id_drivers = d.id
                LEFT JOIN fleettypes ft ON f.id_fleettypes = ft.id
                LEFT JOIN fin_invoices fi ON a.id_fin_invoices = fi.id
                LEFT JOIN routes r ON o.id_routes = r.id
              WHERE
                a.active = 1
                AND a.id_fin_invoices = $id_fin_invoices
                $where
            )A
        WHERE 
            A.id IS NOT NULL
            $paging
        ";
        
        $result = $this->db->query($sql);
        $rows = $result->result();
        
        $newArray = array();
        foreach($rows AS $row) {
            $shipment = explode("_", $row->shippment);
            $row->shippment = count($shipment) > 1 ? $shipment[0] : $row->shippment;
            array_push($newArray, $row);
        }
        
        return (object)$newArray;
    }

    public function selectAllForType2($data = null)
    {
        $id_fin_invoices = isset($data["id_fin_invoices"]) && $data["id_fin_invoices"] > 0 ? $data["id_fin_invoices"] : 0;
        $where = "";
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( A.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
        SELECT A.* FROM 
            ( SELECT ROW_NUMBER() OVER (Order by o.orderdate) AS RowNum, 
                a.*,
                o.shippment,
                o.noVoucher,
                o.orderdate,
                (o.prices + COALESCE(o.pricesadd,0))AS prices,
                o.allowances,
                o.allowanceadds,
                o.description,
                o.loadqty,
                d.name AS driver_name,
                droplocations = STUFF((
                      SELECT '<br/>' + name
                      FROM orderdroplocations
                      WHERE (id_orders = o.id)
                      FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 5, ''),
                f.fleetplateno,
                ft.name AS fleettype_name,
                fi.invoiceno,
                cio.name AS origin,
                cid.name AS destination,
                cu.name AS customer_name,
                r.allowance AS unit_price
              FROM fin_invoice_details a
                LEFT JOIN orders o ON a.id_orders = o.id AND o.active = 1
                LEFT JOIN customers cu ON o.id_customers = cu.id
                LEFT JOIN cities cio ON o.id_citieso = cio.id
                LEFT JOIN cities cid ON o.id_citiesd = cid.id
                LEFT JOIN fleets f ON o.id_fleets = f.id
                LEFT JOIN drivers d ON o.id_drivers = d.id
                LEFT JOIN fleettypes ft ON f.id_fleettypes = ft.id
                LEFT JOIN fin_invoices fi ON a.id_fin_invoices = fi.id
                LEFT JOIN routes r ON o.id_routes = r.id
              WHERE
                a.active = 1
                AND a.id_fin_invoices = $id_fin_invoices
                $where
            )A
        WHERE 
            A.id IS NOT NULL
            $paging
        ";
        
        $result = $this->db->query($sql);
        $rows = $result->result();
        
        $newArray = array();
        foreach($rows AS $row) {
            $shipment = explode("_", $row->shippment);
            $row->shippment = count($shipment) > 1 ? $shipment[0] : $row->shippment;
            array_push($newArray, $row);
        }
        
        return (object)$newArray;
    }
    
}