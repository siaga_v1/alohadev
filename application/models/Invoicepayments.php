<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class invoicepayments extends CI_Model {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    
    function selectAll($data = null)
    {
        $id = $this->db->escape_str($this->input->post('id')) > 0 ? $this->db->escape_str($this->input->post('id')) : 0;
        $id_fin_invoices = $this->db->escape_str($this->input->post('id_fin_invoices')) > 0 ? $this->db->escape_str($this->input->post('id_fin_invoices')) : 0;
        $search = $this->db->escape_str($this->input->post('search'));
        $dates = $this->db->escape_str($this->input->post('dates'));
        $q = $this->db->escape_str($this->input->get('q'));
        
        $limit  = isset($data['limit']) ? $data['limit'] : 0;
        $page   = isset($data['page']) ? $data['page'] : 1;
        $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
        $last   = ($page > 1) ? ($limit*$page) : $limit;
        
        $paging = "";
        if(isset($data['limit']) && isset($data['offset']) ){
            $paging = " AND A.RowNum BETWEEN $first AND $last ";
        }
        
        $where = "";
        if($id > 0){ $where .= " AND a.id = ".$id; }
        if($id_fin_invoices > 0){ $where .= " AND a.id_fin_invoices = ".$id_fin_invoices; }
        if($search != ""){ $where .= " AND ( a.code LIKE '%$search%' OR  ) "; }
        if($dates != ""){ $where .= " AND a.payment_date BETWEEN '".$dates." 00:00:00' AND '".$dates." 23:59:59' "; }
        if($q != ""){ $where .= " AND ( a.code LIKE '%$q%' ) "; }
        
        $sql = "
        SELECT A.* FROM 
            ( SELECT ROW_NUMBER() OVER (Order by a.payment_date) AS RowNum, 
                a.*, dl.invoiceno
              FROM fin_invoice_payments a
                LEFT JOIN fin_invoices dl ON a.id_fin_invoices = dl.id
              WHERE
                a.active = 1
                $where
            )A
        WHERE 
            A.id IS NOT NULL
            $paging
        ORDER BY A.payment_date 
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }
    
    function getAll()
    {
        $this->db->where('active', 1);
        $result = $this->db->get('fin_invoice_payments');
        return $result->result();
    }
    
    public function selectOne($data = null)
    {
        $id = isset($data['id']) && $data['id'] > 0 ? $data['id'] : 0;
        if($id > 0) {
            $this->db->select("a.*, dl.invoiceno");
            $this->db->join("fin_invoices dl","a.id_fin_invoices = dl.id","LEFT");
            $this->db->where('a.id', $id);
            $result = $this->db->get('fin_invoice_payments a');
            return $result->row();
        }
    }
    
    public function selectAllPDF($data = null)
    {
        $id_fin_invoices = isset($data) && $data["id_fin_invoices"] > 0 ? $data["id_fin_invoices"] : 0;
        
        $where = "";
        if($id_fin_invoices > 0){ $where .= " AND a.id_fin_invoices = ".$id_fin_invoices; }
        
        $sql = "
        SELECT A.* FROM 
            ( SELECT ROW_NUMBER() OVER (Order by a.payment_date) AS RowNum, 
                a.*, dl.invoiceno
              FROM fin_invoice_payments a
                LEFT JOIN fin_invoices dl ON a.id_fin_invoices = dl.id
              WHERE
                a.active = 1
                $where
            )A
        WHERE 
            A.id IS NOT NULL
        ORDER BY A.payment_date
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }
    
    public function delete() {
        $id = $this->input->post('id');
        $this->db->set('active', 0);
        $this->db->set('update_by', ($this->session->userdata('id') > 0 ? $this->session->userdata('id') : 1));
        $this->db->set('update_datetime', date('Y-m-d H:i:s'));
        $this->db->where('id', $id);
        return $this->db->update('fin_invoice_payments');
    }
    
}
