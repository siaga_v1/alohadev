<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class invoices extends CI_Model{

    public function __construct(){
        parent::__construct();
    }
    
    //coding aloha listsummary
     public function selectAll(){

        $this->db->select('a.*, b.name as cname');
        $this->db->where('a.active', '1');
        $this->db->join('customers b','a.id_customers = b.id', 'left');

        $result = $this->db->get('invoices a');

        return $result->result();
        
    }   

    // coding lpjm selectall 
    public function selectAlll($data = null)
    {
        $invoiceno = trim($this->input->post_get("invoiceno")) != "" ? $this->db->escape_str($this->input->post_get("invoiceno"))  : "";
        $customer_name = trim($this->input->post_get("customer_name")) != "" ? $this->db->escape_str($this->input->post_get("customer_name"))  : "";
        
        $where = "";
        if( $invoiceno != "" ) {
            $where .= " AND (a.invoiceno LIKE '%$invoiceno%') ";
        }
        if( $customer_name != "" ) {
            $where .= " AND (cu.name LIKE '%$customer_name%') ";
        }
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( A.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
        SELECT A.* FROM 
            ( SELECT ROW_NUMBER() OVER (Order by a.id) AS RowNum, 
                a.*,
                cu.name AS customer_name,
                cu.ppn AS customer_ppn,
                cu.pph AS customer_pph,
                ip.name AS invoicetype_name,
                c.nickname AS company_nickname,
                c.pic_name AS company_pic_name,
                (SELECT count(*) FROM fin_invoice_details WHERE active = 1 AND id_fin_invoices = (a.id)) AS total_shipment
              FROM fin_invoices a
                LEFT JOIN customers cu ON a.id_customers = cu.id
                LEFT JOIN invoicetypes ip ON a.type = ip.invoice_type
                LEFT JOIN companys c ON a.id_companys = c.id
              WHERE
                a.active = 1
                $where
            )A
        WHERE 
            A.id IS NOT NULL
            $paging
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }
    
    public function selectOne($data = null )
    {
        
        $this->db->select("a.id, 
                                a.code, 
                                a.id_customers, 
                                a.invoiceno, 
                                a.receiptno, 
                                a.dates, 
                                a.dates_overdue,
                                a.po_number, 
                                a.pr_number, 
                                a.recap_id, 
                                a.type, 
                                a.addressto,
                                a.pic_name,
                                a.pic_phone,
                                a.notes,
                                a.id_companys,
                                a.status,
                                a.prices,
                                a.paid,
                                b.code AS customer_code,
                                b.name AS customer_name,
                                b.ppn AS customer_ppn,
                                b.pph AS customer_pph,
                                c.name AS invoicetype_name,
                                c.invoice_type,
                                c.receipt_type,
                                co.nickname AS company_nickname,
                                co.pic_name AS company_pic_name,
                                (SELECT count(*) FROM fin_invoice_details WHERE active = 1 AND id_fin_invoices = (a.id)) AS total_shipment");
        $this->db->join("customers b","a.id_customers = b.id","LEFT");
        $this->db->join("invoicetypes c","a.type = c.invoice_type","LEFT");
        $this->db->join("companys co","a.id_companys = co.id","LEFT");
        $this->db->where("a.active",1);
        if(isset($data["id"]) && $data["id"] > 0)
        {
            $this->db->where("a.id", $data["id"]);
        }
        
        $result = $this->db->get("fin_invoices a");
        return $result->row();
    }
    
    public function selectOneByCode($data = null )
    {
        
        $this->db->select("a.id, 
                                a.code, 
                                a.id_customers, 
                                a.invoiceno, 
                                a.receiptno, 
                                a.dates, 
                                a.dates_overdue,
                                a.po_number, 
                                a.pr_number, 
                                a.recap_id, 
                                a.type, 
                                a.addressto,
                                a.pic_name,
                                a.pic_phone,
                                a.notes,
                                a.id_companys,
                                a.status,
                                a.prices,
                                a.paid,
                                b.code AS customer_code,
                                b.name AS customer_name,
                                b.ppn AS customer_ppn,
                                b.pph AS customer_pph,
                                c.name AS invoicetype_name,
                                c.invoice_type,
                                c.receipt_type,
                                co.nickname AS company_nickname,
                                co.pic_name AS company_pic_name,
                                (SELECT count(*) FROM fin_invoice_details WHERE active = 1 AND id_fin_invoices = (a.id)) AS total_shipment,
                                (SELECT MIN(o.orderdate) AS orderdate FROM fin_invoice_details id LEFT JOIN orders o ON id.id_orders = o.id WHERE id.id_fin_invoices = a.id) AS min_orderdates,
                                (SELECT MAX(o.orderdate) AS orderdate FROM fin_invoice_details id LEFT JOIN orders o ON id.id_orders = o.id WHERE id.id_fin_invoices = a.id) AS max_orderdates,,
                                b.name AS customer_name");
        $this->db->join("customers b","a.id_customers = b.id","LEFT");
        $this->db->join("invoicetypes c","a.type = c.invoice_type","LEFT");
        $this->db->join("companys co","a.id_companys = co.id","LEFT");
        $this->db->where("a.active",1);
        if(isset($data["code"]) && $data["code"] != "")
        {
            $this->db->where("a.code", $data["code"]);
        }
        
        $result = $this->db->get("fin_invoices a");
        return $result->row();
    }
    
    public function delete() {
        $id = $this->input->post('id');
        $this->db->set('active', 0);
        $this->db->set('update_id', ($this->session->userdata('id') > 0 ? $this->session->userdata('id') : 1));
        $this->db->set('update_datetime', date('Y-m-d H:i:s'));
        $this->db->where('id', $id);
        if($this->db->update('fin_invoices')) {
            $this->db->set('active', 0);
            $this->db->where('id_fin_invoices', $id);
            return $this->db->update('fin_invoice_details');
        }
        
    }
    
    
    
}