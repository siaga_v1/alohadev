<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends CI_Model {
    public function __construct()
    {
        parent::__construct();    
    }
    
    function selectAll($data = null)
    {
        $where = "";
        if($data['name'] != '') { $where .= " AND m.name LIKE '%".$this->db->escape_str($data['name'])."%'"; }
        if($data['url'] != '') { $where .= " AND m.url LIKE '%".$this->db->escape_str($data['url'])."%'"; }
        if($data['parent_name'] != '') { $where .= " AND m1.name LIKE '%".$this->db->escape_str($data['parent_name'])."%'"; }
        
        $limit  = isset($data['limit']) ? $data['limit'] : 0;
        $page   = isset($data['page']) ? $data['page'] : 1;
        $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
        $last   = ($page > 1) ? ($limit*$page) : $limit;
        
        $paging = "";
        if(isset($data['limit']) && isset($data['offset']) ){
            $paging = " AND A.RowNum BETWEEN $first AND $last ";
        }
        
        $sql = "
        SELECT A.* FROM 
            ( SELECT ROW_NUMBER() OVER (Order by m.id) AS RowNum, 
                m.*,
                m1.name AS parent_name
              FROM menus m
                LEFT JOIN menus m1 ON m.parent = m1.id
              WHERE
                m.active = 1
                $where
            )A
        WHERE 
            A.id IS NOT NULL
            $paging
        ORDER BY
            A.parent, A.sort, A.id
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }
    
    function selectOne($data = null)
    {
        if($data['id'] > 0){
            $this->db->where('id', $data['id']);
            $result = $this->db->get('menus');
            return $result->row();
        }
    }
    
    
    
    function setInsert($data = null)
    {
        if($data != null)
        {
            $this->db->set('parent', $data['parent']);
            $this->db->set('url', $data['url']);
            $this->db->set('icon', $data['icon']);
            $this->db->set('name', $data['name']);
            $this->db->set('sort', $data['sort']);
            $this->db->set('active', 1);
            return $this->db->insert('menus');
        }
    }
    
    function setUpdate($data = null)
    {
        if($data != null)
        {
            $this->db->set('parent', $data['parent']);
            $this->db->set('url', $data['url']);
            $this->db->set('icon', $data['icon']);
            $this->db->set('name', $data['name']);
            $this->db->set('sort', $data['sort']);
            $this->db->where('id', $data['id']);
            return $this->db->update('menus');
        }
    }
    
    function setDelete($id = 0){
        if($id > 0){
            $this->db->set('active', 0);
            $this->db->where('id', $id);
            return $this->db->update('menus');
        }
    }
    
    function validation_check($data = null)
    {
        if($data != null)
        {
            if($data['id'] > 0)
            {
                $this->db->select('COUNT(*) AS total');
                $this->db->where('id != '.$data['id']);
                $this->db->where('name', $this->db->escape_str($data['name']));
                $this->db->where('active', 1);
                $result = $this->db->get('menus');
                $valid = $result->row()->total;
                
                $this->db->select('COUNT(*) AS total');
                $this->db->where('id != '.$data['id']);
                $this->db->where('name', $this->db->escape_str($data['name']));
                $this->db->where('url', $this->db->escape_str($data['url']));
                $this->db->where('active', 1);
                $result = $this->db->get('menus');
                $valid2 = $result->row()->total;
                if($valid == 0 && $valid2 == 0){
                    return true;
                }else{
                    return false;
                }
            }
            else
            {
                $this->db->select('COUNT(*) AS total');
                $this->db->where('name', $this->db->escape_str($data['name']));
                $this->db->where('active', 1);
                $result = $this->db->get('menus');
                $valid = $result->row()->total;
                
                $this->db->select('COUNT(*) AS total');
                $this->db->where('name', $this->db->escape_str($data['name']));
                $this->db->where('url', $this->db->escape_str($data['name']));
                $this->db->where('active', 1);
                $result = $this->db->get('menus');
                $valid2 = $result->row()->total;
                if($valid == 0 && $valid2 == 0){
                    return true;
                }else{
                    return false;
                }
            }
            
        }
    }
    
    function getAllByUrl($data = null)
    {
        $this->db->select('m.*, m2.name AS parent_name');
        $this->db->join('menus m2','m.parent = m2.id', 'LEFT');
        $this->db->where('m.url', $data['url']);
        $result = $this->db->get('menus m');
        return $result->row();
    }
    
    function getAll()
    {
        $this->db->where('active', 1);
        $this->db->order_by('id', 'ASC');
        $this->db->order_by('parent', 'ASC');
        $this->db->order_by('sort', 'ASC');
        $result = $this->db->get('menus');
        return $result->result();
    }
    
    function getMenu($parent)
    {
        $this->db->where('active', 1);
        $this->db->where('parent', $parent);
        $this->db->order_by('sort', 'ASC');
        $result = $this->db->get('menus');
        return $result->result();
    }
    
    function getMenuLevel($parent, $userroles)
    {
        $this->db->select('a.id, a.parent, a.url, a.name, a.active, a.sort, a.icon ');
        $this->db->join('menus a','b.id_menus = a.id', 'LEFT');
        $this->db->where('a.active', 1);
        $this->db->where('a.parent', $parent);
        $this->db->where('b.id_userroles', $userroles);
        $this->db->order_by('a.sort', 'ASC');
        $result = $this->db->get('usermenus b');
        return $result->result();
    }
    
    function getMenuByLevel($level)
    {
        $this->db->select('id_menus');
        $this->db->where('id_userroles', $level);
        $this->db->order_by('id_userroles', 'ASC');
        $result = $this->db->get('usermenus');
        return $result->result();
    }
    
    function getMenuByLevelMenu($level, $menu)
    {
        $this->db->select('COUNT(*) AS total');
        $this->db->join('menus b','a.id_menus = b.id','LEFT');
        $this->db->where('a.id_userroles', $level);
        $this->db->like('LOWER(b.url)', strtolower(trim($this->db->escape_str($menu))) ,'after');
        $result = $this->db->get('usermenus a');
        if($result->row()->total > 0){
            return true;
        }else{
            return false;
        }
    }
    
    function deleteUsermenus($id_userroles)
    {
        $this->db->where('id_userroles', $id_userroles);
        return $this->db->delete('usermenus');
    }
    
}
