<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reports extends CI_Model
{

    public function getOrderNum($id_companies = 0)
    {
        $y = date('Y');

        $today      = date('Y-m-d');
        if (date('D', strtotime($today . ' -1 Days')) == 'Sun') {
            $yesterday  = date('Y-m-d', strtotime($today . ' -2 Days'));
        } else {
            $yesterday  = date('Y-m-d', strtotime($today . ' -1 Days'));
        }


        $thismonday = date('Y-m-d', strtotime($today . ' this week'));
        $thissunday = date('Y-m-d', strtotime($today . ' this sunday'));

        $lastmonday = date('Y-m-d', strtotime($thismonday . ' -1 week'));
        $lastsunday = date('Y-m-d', strtotime($thissunday . ' -1 week'));

        $thismonth  = date('Y-m-01');
        $thismonthlast = date('Y-m-t');
        $lastmonth  = date('Y-m-01', strtotime($thismonth . ' -1 Months'));
        $lastmonthlast = date('Y-m-t', strtotime($lastmonth));

        $thism = date('m', strtotime($thismonth));
        $lastm = date('m', strtotime($lastmonth));

        $thisy = date('Y', strtotime($thismonth));
        $lastys = date('Y', strtotime($lastmonth));

        $thisyear   = date('Y-m-01');
        $lastyear   = date('Y-m-d', strtotime($thisyear . ' -1 years'));

        $lasty = date('Y', strtotime($lastyear));

        $sql = "
            SELECT 
                COUNT( CASE WHEN orderdate = '$today' THEN 1 ELSE NULL END ) AS order_today,
                COUNT( CASE WHEN orderdate = '$yesterday' THEN 1 ELSE NULL END ) AS order_yesterday,
                COUNT( CASE WHEN orderdate BETWEEN '$thismonday' AND '$thissunday' THEN 1 ELSE NULL END ) AS order_thisweek,
                COUNT( CASE WHEN orderdate BETWEEN '$lastmonday' AND '$lastsunday' THEN 1 ELSE NULL END ) AS order_lastweek,
                COUNT( CASE WHEN orderdate BETWEEN '$thismonth' AND '$thismonthlast' THEN 1 ELSE NULL END ) AS order_thismonth,
                COUNT( CASE WHEN orderdate BETWEEN '$lastmonth' AND '$lastmonthlast' THEN 1 ELSE NULL END ) AS order_lastmonth,
                COUNT( CASE WHEN YEAR(orderdate) = '$y' THEN 1 ELSE NULL END ) AS order_thisyear,
                COUNT( CASE WHEN YEAR(orderdate) = '$lasty' THEN 1 ELSE NULL END ) AS order_lastyear
            FROM 
                orders 
            WHERE active = 1 
                
        ";

        $result = $this->db->query($sql);
        return $result->row();
    }

    public function FleetNW()
    {
        $y = date('Y');

        $today      = date('Y-m-d');
        if (date('D', strtotime($today . ' -1 Days')) == 'Sun') {
            $yesterday  = date('Y-m-d', strtotime($today . ' -2 Days'));
        } else {
            $yesterday  = date('Y-m-d', strtotime($today . ' -1 Days'));
        }


        $thismonday = date('Y-m-d', strtotime($today . ' this week'));
        $thissunday = date('Y-m-d', strtotime($today . ' this sunday'));

        $lastmonday = date('Y-m-d', strtotime($thismonday . ' -1 week'));
        $lastsunday = date('Y-m-d', strtotime($thissunday . ' -1 week'));

        $thismonth  = date('Y-m-01');
        $thismonthlast = date('Y-m-t');
        $lastmonth  = date('Y-m-01', strtotime($thismonth . ' -1 Months'));
        $lastmonthlast = date('Y-m-t', strtotime($lastmonth));

        $thism = date('m', strtotime($thismonth));
        $lastm = date('m', strtotime($lastmonth));

        $thisy = date('Y', strtotime($thismonth));
        $lastys = date('Y', strtotime($lastmonth));

        $thisyear   = date('Y-m-01');
        $lastyear   = date('Y-m-d', strtotime($thisyear . ' -1 years'));

        $lasty = date('Y', strtotime($lastyear));

        $sql = "
            SELECT 
            
            f.fleetplateno,
                COUNT( CASE WHEN o.orderdate = '$today' THEN 1 ELSE NULL END ) AS FreeToday,
                COUNT( CASE WHEN o.orderdate = '$yesterday' THEN 1 ELSE NULL END ) AS FreeYesterday,
                COUNT( CASE WHEN o.orderdate BETWEEN '$thismonday' AND '$thissunday' THEN 1 ELSE NULL END ) AS FreeWeek,
                COUNT( CASE WHEN o.orderdate BETWEEN '$lastmonday' AND '$lastsunday' THEN 1 ELSE NULL END ) AS FreeLastWeek,
                COUNT( CASE WHEN o.orderdate BETWEEN '$thismonth' AND '$thismonthlast' THEN 1 ELSE NULL END ) AS FreeMonth,
                COUNT( CASE WHEN o.orderdate BETWEEN '$lastmonth' AND '$lastmonthlast' THEN 1 ELSE NULL END ) AS FreeLastMonth,
                COUNT( CASE WHEN YEAR(o.orderdate) = '$y' THEN 1 ELSE NULL END ) AS FreeYears,
                COUNT( CASE WHEN YEAR(o.orderdate) = '$lasty' THEN 1 ELSE NULL END ) AS FreeLastYears
            FROM
            fleets f
            LEFT JOIN orders o ON f.id = o.id_fleets 
            AND o.active = 1 
             WHERE
            f.active = 1 
            GROUP BY
                f.fleetplateno
                
        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function getSalesNum()
    {
        $y = date('Y');

        $today      = date('Y-m-d');
        if (date('D', strtotime($today . ' -1 Days')) == 'Sun') {
            $yesterday  = date('Y-m-d', strtotime($today . ' -2 Days'));
        } else {
            $yesterday  = date('Y-m-d', strtotime($today . ' -1 Days'));
        }


        $thismonday = date('Y-m-d', strtotime($today . ' this week'));
        $thissunday = date('Y-m-d', strtotime($today . ' this sunday'));

        $lastmonday = date('Y-m-d', strtotime($thismonday . ' -1 week'));
        $lastsunday = date('Y-m-d', strtotime($thissunday . ' -1 week'));

        $thismonth  = date('Y-m-01');
        $thismonthlast = date('Y-m-t');
        $lastmonth  = date('Y-m-01', strtotime($thismonth . ' -1 Months'));
        $lastmonthlast = date('Y-m-t', strtotime($lastmonth));

        $thism = date('m', strtotime($thismonth));
        $lastm = date('m', strtotime($lastmonth));

        $thisy = date('Y', strtotime($thismonth));
        $lastys = date('Y', strtotime($lastmonth));

        $thisyear   = date('Y-m-01');
        $lastyear   = date('Y-m-d', strtotime($thisyear . ' -1 years'));

        $lasty = date('Y', strtotime($lastyear));

        $sql = "SELECT 
                    SUM( CASE WHEN [date] = '$today' THEN prices ELSE 0 END ) AS order_today,
                    SUM( CASE WHEN [date] = '$yesterday' THEN prices ELSE 0 END ) AS order_yesterday,
                    SUM( CASE WHEN orderdate BETWEEN '$thismonday' AND '$thissunday' THEN prices ELSE 0 END ) AS order_thisweek,
                    SUM( CASE WHEN orderdate BETWEEN '$lastmonday' AND '$lastsunday' THEN prices ELSE 0 END ) AS order_lastweek,
                    SUM( CASE WHEN orderdate BETWEEN '$thismonth' AND '$thismonthlast' THEN prices ELSE 0 END ) AS order_thismonth,
                    SUM( CASE WHEN orderdate BETWEEN '$lastmonth' AND '$lastmonthlast' THEN prices ELSE 0 END ) AS order_lastmonth,
                    SUM( CASE WHEN YEAR(orderdate) = '$y' THEN prices ELSE 0 END ) AS order_thisyear,
                    SUM( CASE WHEN YEAR(orderdate) = '$lasty' THEN prices ELSE 0 END ) AS order_lastyear
                FROM 
                    orders 
                WHERE active = 1";

        $result = $this->db->query($sql);
        return $result->row();
    }

    public function getOrderNumYear($year = 0)
    {
        if ($year > 0) {
            $select = "";
            for ($a = 1; $a <= 12; $a++) {
                $select .= " COALESCE(SUM(CASE WHEN YEAR(orderdate) = '$year' AND MONTH(orderdate) = '$a' THEN ritase ELSE NULL END ),0) AS order$a, ";
            }
            $sql = "SELECT
                        $select
                        COALESCE(SUM( CASE WHEN YEAR(orderdate) = '$year' THEN ritase ELSE NULL END ),0) AS order_year
                    FROM 
                        orders 
                    WHERE active = 1 
                
            ";

            $result = $this->db->query($sql);
            return $result->row();
        }
    }
    //INI YANG BELUM SELESAI
    public function TruckProductivity($year = 1)
    {
        if ($year > 0) {
            $maint = "";
            $select = "";
            $grup = "";
            $tahun = date('Y');
            for ($a = 1; $a <= 12; $a++) {
                $maint .= " SUM (CASE WHEN YEAR ( dbo.maintenance_detail.[date] ) = $tahun AND MONTH ( dbo.maintenance_detail.[date] ) = $a THEN COALESCE(maintenance_detail.price,0) ELSE 0 END ) AS Maintenance_$a, ";

                $select .= " Maintenance_$a,
                            SUM ( CASE WHEN YEAR ( o.orderdate ) = $tahun AND MONTH ( o.orderdate ) = $a THEN o.allowances ELSE 0 END ) AS UJS_$a,
                            SUM ( CASE WHEN YEAR ( o.orderdate ) = $tahun AND MONTH ( o.orderdate ) = $a THEN o.prices ELSE 0 END ) AS SALES_$a,
                            SUM ( CASE WHEN YEAR ( o.orderdate ) = $tahun AND MONTH ( o.orderdate ) = $a THEN o.loadqty ELSE 0 END ) AS UNIT_$a, 
                            SUM ( CASE WHEN YEAR ( o.orderdate ) = $tahun AND MONTH ( o.orderdate ) = $a THEN c.nominal ELSE 0 END ) AS ADDCOST_$a,
                            SUM ( CASE WHEN YEAR ( o.orderdate ) = $tahun AND MONTH ( o.orderdate ) = $a THEN d.cost ELSE 0 END ) AS Claim_$a,";

                $grup .= "test.Maintenance_$a,";
            }


            $sql = "SELECT
                        * 
                    FROM
                        (
                        SELECT
                            ROW_NUMBER ( ) OVER ( ORDER BY A.active DESC ) AS RowNum,
                            A.* 
                        FROM
                            (
                            SELECT
                                f.active,
                                $select
                                f.fleetplateno
                            FROM
                                fleets f
                                LEFT JOIN orders o ON f.id = o.id_fleets 
                                LEFT JOIN cost c ON o.code = c.order_id 
                                LEFT JOIN orderclaims d ON o.code = d.ordercode 
                                AND o.active = 1
                                LEFT JOIN (
                                SELECT
                                    $maint
                                    maintenance_detail.fleet_id
                                        FROM
                                            maintenance_detail 
                                        GROUP BY
                                            maintenance_detail.fleet_id 
                                        ) test ON f.id = test.fleet_id
                                    WHERE
                                        f.active = 1 
                                    GROUP BY
                                        f.active,
                                        $grup
                                        f.fleetplateno
                                    ) A 
                                ) B 
                        WHERE
                        B.fleetplateno IS NOT NULL AND
                        B.active = 1";

            $result = $this->db->query($sql);
            return $result->result_array();
            //print_r($result);
        }
    }

    public function CustomerProductivity($year = 1)
    {

        if ($year > 0) {
            $maint = "";
            $select = "";
            $grup = "";
            $tahun = date('Y');
            for ($a = 1; $a <= 12; $a++) {

                $select .= "
                            SUM ( CASE WHEN YEAR ( o.orderdate ) = $tahun AND MONTH ( o.orderdate ) = $a THEN o.prices ELSE 0 END ) AS Sales_$a,
                            SUM ( CASE WHEN YEAR ( o.orderdate ) = $tahun AND MONTH ( o.orderdate ) = $a THEN o.ritase ELSE 0 END ) AS Ritase_$a,";
            }


            $sql = "SELECT
                        * 
                    FROM
                        (
                        SELECT
                            ROW_NUMBER ( ) OVER ( ORDER BY A.id DESC ) AS RowNum,
                            A.* 
                        FROM
                            (
                            SELECT
                                f.id,
                                $select
                                f.name
                            FROM
                                customers f
                                LEFT JOIN orders o ON f.id = o.id_customers 
                                LEFT JOIN cost c ON o.code = c.order_id 
                                LEFT JOIN orderclaims d ON o.code = d.ordercode 
                                AND o.active = 1
                                
                                    WHERE
                                        f.active = 1 
                                    GROUP BY
                                        f.id,
                                        f.name
                                    ) A 
                                ) B 
                        WHERE
                        B.name IS NOT NULL ";

            $result = $this->db->query($sql);
            return $result->result_array();
            //print_r($result);
        }
    }

    public function DriverProductivity($year = 1)
    {

        if ($year > 0) {
            $maint = "";
            $select = "";
            $grup = "";
            $tahun = date('Y');
            for ($a = 1; $a <= 12; $a++) {

                $select .= " SUM ( CASE WHEN YEAR ( o.orderdate ) = $tahun AND MONTH ( o.orderdate ) = $a THEN o.ritase ELSE 0 END ) AS Ritase_$a,
            SUM ( CASE WHEN YEAR ( o.orderdate ) = $tahun AND MONTH ( o.orderdate ) = $a THEN o.prices ELSE 0 END ) AS Sales_$a,
            SUM ( CASE WHEN YEAR ( o.orderdate ) = $tahun AND MONTH ( o.orderdate ) = $a THEN d.cost ELSE 0 END ) AS Claim_$a,";
            }


            $sql = "SELECT
                        * 
                    FROM
                        (
                        SELECT
                            ROW_NUMBER ( ) OVER ( ORDER BY A.id DESC ) AS RowNum,
                            A.* 
                        FROM
                            (
                            SELECT
                                f.id,
                                $select
                                f.name
                            FROM
                                drivers f
                                LEFT JOIN orders o ON f.id = o.id_drivers 
                                LEFT JOIN cost c ON o.code = c.order_id 
                                LEFT JOIN orderclaims d ON o.code = d.ordercode 
                                AND o.active = 1
                                
                                    WHERE
                                        f.active = 1 
                                    GROUP BY
                                        f.id,
                                        f.name
                                    ) A 
                                ) B 
                        WHERE
                        B.name IS NOT NULL ";

            $result = $this->db->query($sql);
            return $result->result_array();
            //print_r($result);
        }
    }

    public function getSalesNumYear($year = 0)
    {
        if ($year > 0) {
            $select = "";
            for ($a = 1; $a <= 12; $a++) {
                $select .= " SUM(CASE WHEN YEAR(orderdate) = '$year' AND MONTH(orderdate) = '$a' THEN prices ELSE NULL END ) AS order$a, ";
            }
            $sql = "SELECT
                        $select
                        SUM( CASE WHEN YEAR(orderdate) = '$year' THEN prices ELSE NULL END ) AS sales_year
                    FROM 
                        orders 
                    WHERE active = 1 
                
            ";

            $result = $this->db->query($sql);
            return $result->row();
        }
    }

    public function getAddSalesPalembang($year = 0)
    {
        if ($year > 0) {
            $select = "";
            for ($a = 1; $a <= 12; $a++) {
                $select .= " SUM(CASE WHEN YEAR(orderdate) = '$year' AND MONTH(orderdate) = '$a' THEN (o.loadqty * o.pricesadd) ELSE 0 END ) AS palembang$a,";
            }
            $sql = "SELECT
                        $select
                        SUM( CASE WHEN YEAR(orderdate) = '$year' THEN (o.loadqty * o.pricesadd) ELSE 0 END ) AS palembangsales_year
                    FROM
                orders o
                LEFT JOIN cities c ON o.id_citieso = c.id 
            WHERE
                o.active = 1 
                AND o.ritase > 0 
                AND o.pricesadd = 44000";

            $result = $this->db->query($sql);
            return $result->row();
        }

        //$tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        //$totalmonth = isset($data['totalmonth']) ? 7 : 12;
        //$x = isset($data['x']) ? $data['x'] : 1;

        //$a = (int)date('m');

        //$sql="SELECT
        //        SUM ( CASE WHEN o.orderdate BETWEEN '2019-12-01' AND '2020-01-01' THEN (o.loadqty * o.allowanceadds) ELSE 0 END ) AS hasil
        //    FROM
        //        orders o
        //        LEFT JOIN cities c ON o.id_citieso = c.id 
        //    WHERE
        //        o.active = 1 
        //        AND o.ritase > 0 
        //        AND o.allowanceadds = 44000";

        //$result = $this->db->query($sql);
        //return $result->result_array();
    }

    public function getAddSalesspld($year = 0)
    {
        if ($year > 0) {
            $select = "";
            for ($a = 1; $a <= 12; $a++) {
                $select .= " SUM(CASE WHEN YEAR(orderdate) = '$year' AND MONTH(orderdate) = '$a' THEN o.pricesadd ELSE 0 END ) AS hasilpareto$a,";
            }
            $sql = "SELECT
                        $select
                        SUM( CASE WHEN YEAR(orderdate) = '$year' THEN o.pricesadd ELSE 0 END ) AS hasilparetos
                    FROM
                orders o
                LEFT JOIN cities c ON o.id_citieso = c.id 
            WHERE
                o.active = 1 
                AND o.ritase > 0 
                AND o.pricesadd != 44000";

            $result = $this->db->query($sql);
            return $result->row();
        }
    }

    public function truckritase($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $wherem = "";
        for ($a = $x; $a <= 12; $a++) {
            $wherem .= " COUNT( CASE WHEN YEAR(o.orderdate) = '$tahun' AND MONTH(o.orderdate) = '$a' THEN 1 ELSE NULL END ) AS ritase$a, ";
        }

        $where = " AND YEAR(o.orderdate) = $tahun";
        if ($data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleetplateno']) . "%' ";
        }

        $sql = "
            SELECT
            	orders.id_fleets,
            	fleets.fleetplateno,
                $wherem
            	COUNT(*) AS ritase
            FROM
            	orders o
            	LEFT JOIN orderassigns oa ON o.id_orderassigns = orders.id
            	LEFT JOIN fleets f ON orders.id_fleets = fleets.id
            WHERE
            	orders.active = 1
            	AND o.active = 1
                $where
            GROUP BY
            	orders.id_fleets,
            	f.fleetplateno
            ORDER BY
            	ritase DESC
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function trucksales($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $wherem = "";
        for ($a = $x; $a <= 12; $a++) {
            $wherem .= " SUM( CASE WHEN YEAR(o.orderdate) = '$tahun' AND MONTH(o.orderdate) = '$a' THEN o.prices ELSE 0 END ) AS prices$a, ";
        }

        $where = " AND YEAR(o.orderdate) = $tahun";
        if ($data['fleetplateno'] != '') {
            $where .= " AND LOWER(f.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleetplateno']) . "%' ";
        }

        $sql = "
            SELECT
            	orders.id_fleets,
            	f.fleetplateno,
                $wherem
            	SUM(o.prices) AS prices
            FROM
            	orders o
            	LEFT JOIN fleets f ON orders.id_fleets = f.id
            WHERE
            	orders.active = 1
            	AND o.active = 1
                $where
            GROUP BY
            	orders.id_fleets,
            	f.fleetplateno
            ORDER BY
            	prices DESC
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function truckritasesales($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $wherem = "";
        for ($a = $x; $a <= 12; $a++) {
            $wherem .= " COUNT( CASE WHEN YEAR(o.orderdate) = '$tahun' AND MONTH(o.orderdate) = '$a' THEN 1 ELSE NULL END ) AS ritase$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(o.orderdate) = '$tahun' AND MONTH(o.orderdate) = '$a' THEN o.prices ELSE 0 END ) AS prices$a, ";
        }

        $where = " AND YEAR(o.orderdate) = $tahun";
        if ($data['fleetplateno'] != '') {
            $where .= " AND LOWER(f.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleetplateno']) . "%' ";
        }

        $sql = "
            SELECT
            	orders.id_fleets,
            	f.fleetplateno,
                $wherem
                COUNT(*) AS ritase,
            	SUM(o.prices) AS prices
            FROM
            	orders o
            	LEFT JOIN orderassigns oa ON o.id_orderassigns = orders.id
            	LEFT JOIN fleets f ON orders.id_fleets = f.id
            WHERE
            	orders.active = 1
            	AND o.active = 1
                $where
            GROUP BY
            	orders.id_fleets,
            	f.fleetplateno
            ORDER BY
            	prices DESC
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }



    public function reportcustomers($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $wherem = "";
        for ($a = $x; $a <= 12; $a++) {
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritase$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowances$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceadds$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS prices$a, ";
        }

        $where = " AND YEAR(orders.orderdate) = $tahun";
        if (isset($data['customers_name']) && $data['customers_name'] != '') {
            $where .= " AND LOWER(customers.name) LIKE '%" . $this->db->escape_str($data['customers_name']) . "%' ";
        }

        $order = " ritase DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {
            if ($data['setting'] == 'sales') {
                $order = " prices DESC ";
            }
            if ($data['setting'] == 'achieves') {
                $order = " achieves DESC ";
            }
        }

        $sql = "
            SELECT
            	orders.id_customers,
                customers.code AS customers_code,
                customers.name AS customers_name,
            	CASE WHEN customers.alias != '' OR customers.alias IS NOT NULL THEN customers.alias ELSE customers.name END AS customers_alias,
                $wherem
                SUM(orders.ritase) AS ritase,
                SUM(orders.allowances) AS allowances,
                SUM(orders.allowanceadds) AS allowanceadds,
            	SUM(orders.allowances+orders.allowanceadds) AS costs,
            	SUM(orders.prices) AS prices,
                SUM(orders.prices-(orders.allowances+orders.allowanceadds)) AS achieves
            FROM
            	orders
            	LEFT JOIN customers ON orders.id_customers = customers.id
            WHERE
            	orders.active = 1
            	AND orders.ritase > 0
                AND orders.id_customers > 0
                $where
            GROUP BY
            	orders.id_customers,
                customers.code,
                customers.alias,
            	customers.name
            ORDER BY
            	$order
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function reportcustomermonths($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        $a = date('m');

        $wherem = "";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritase, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowances, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceadds, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS prices, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.prices-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achieves, ";

        $where = " AND YEAR(orders.orderdate) = $tahun";
        if (isset($data['customers_name']) && $data['customers_name'] != '') {
            $where .= " AND LOWER(customers.name) LIKE '%" . $this->db->escape_str($data['customers_name']) . "%' ";
        }

        $order = " ritase DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {
            if ($data['setting'] == 'sales') {
                $order = " prices DESC ";
            }
            if ($data['setting'] == 'achieves') {
                $order = " achieves DESC ";
            }
        }

        $sql = "
            SELECT
            	orders.id_customers,
                customers.code AS customers_code,
                $wherem
                CASE WHEN customers.nickname != '' OR customers.nickname IS NOT NULL THEN customers.nickname ELSE customers.name END AS customers_alias,
                customers.name AS customers_name
            FROM
            	orders
            	LEFT JOIN customers ON orders.id_customers = customers.id
            WHERE
            	orders.active = 1
                AND orders.id_customers > 0
                $where
            GROUP BY
            	orders.id_customers,
                customers.code,
                customers.nickname,
            	customers.name
            ORDER BY
            	$order
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    /** TRUCK */
    public function targettruck($value = '')
    {
        $sql = "SELECT
                    SUM ( o.ritase ) AS ritase,
                    MONTH(o.orderdate) as date
                FROM
                    orders o
                    LEFT JOIN fleets f ON o.id_fleets = f.id 
                WHERE
                    o.active = 1 
                    AND o.ritase > 0 
                    AND o.id_customers != 48 
                    
                GROUP BY
                    MONTH(o.orderdate)";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function targetdsoluar($value = '')
    {
        $sql = "SELECT SUM
                    ( o.ritase ) AS ritase,
                    MONTH ( o.orderdate ) AS DATE
                FROM
                    orders o
                    LEFT JOIN fleets f ON o.id_fleets = f.id 
                WHERE
                    o.active = 1 
                    AND o.ritase > 0 
                    AND o.id_customers = 48 
                   AND NOT (
                    o.id_citiesd = 78 
                OR o.id_citiesd = 65 
                OR o.id_citiesd = 41)
                GROUP BY
                    MONTH (o.orderdate)";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function targetdso($value = '')
    {
        $sql = "SELECT SUM
                    ( o.ritase ) AS ritase,
                    MONTH ( o.orderdate ) AS DATE
                FROM
                    orders o
                    LEFT JOIN fleets f ON o.id_fleets = f.id 
                WHERE
                    o.active = 1 
                    AND o.ritase > 0 
                    AND o.id_customers = 48 
                    AND (o.id_citiesd = 78 OR o.id_citiesd = 65 OR o.id_citiesd = 41)
                GROUP BY
                    MONTH (o.orderdate)";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function sumdso()
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $a = (int)date('m');
        $b = (int)date('Y');

        $sql = "SELECT
                SUM ( CASE WHEN YEAR(o.orderdate)= '$b' AND MONTH(o.orderdate)= '$a'  THEN o.active ELSE 0 END ) AS ritase,
                SUM ( CASE WHEN YEAR(o.orderdate)= '$b' AND MONTH(o.orderdate)= '$a' THEN o.allowances ELSE 0 END ) AS allowances

            FROM
                orders o
                LEFT JOIN cities c ON o.id_citieso = c.id 
                LEFT JOIN orderload l ON o.code = l.order_code 
                LEFT JOIN routes r ON l.id_routes = r.id 
            WHERE
                o.active = 1 
                AND o.ritase = 1               
                AND r.bound = 2";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function sumunpaid()
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $thismonth  = date('Y-m-01');

        $c = (int)date('m', strtotime('-1 Months'));
        $a = (int)date('m');
        $b = (int)date('Y');

        $sql = "SELECT
                SUM ( CASE WHEN YEAR(dateinvoice)= '$b' AND MONTH(dateinvoice)= '$a' THEN pay_nominal ELSE 0 END ) AS PAID,
                SUM ( CASE WHEN YEAR(dateinvoice)= '$b' AND MONTH(dateinvoice)= '$a' THEN (nominal-pay_nominal) ELSE 0 END ) AS UNPAID,
                SUM ( CASE WHEN YEAR(dateinvoice)= '$b' AND MONTH(dateinvoice)= $c THEN pay_nominal ELSE 0 END ) AS LASTPAID,
                SUM ( CASE WHEN YEAR(dateinvoice)= '$b' AND MONTH(dateinvoice)= $c THEN (nominal-pay_nominal) ELSE 0 END ) AS LASTUNPAID
            FROM
                invoices
            WHERE
                active = 1 ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function monthar()
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        $thismonth  = date('Y-m-01');

        $a = (int)date('m');
        $b = (int)date('Y');
        $c = (int)date('m', strtotime('-1 Months'));


        $sql = "SELECT 
                SUM ( CASE WHEN YEAR(orderdate)= '$b' AND MONTH(orderdate)= '$a' THEN allowances ELSE 0 END ) AS ujs,
				SUM	( CASE WHEN YEAR(orderdate)= '$b' AND MONTH(orderdate)= $c THEN allowances ELSE 0 END ) AS lastujs,
                SUM ( CASE WHEN YEAR(orderdate)= '$b' AND MONTH(orderdate)= '$a' THEN prices ELSE 0 END ) AS monthar,
				SUM ( CASE WHEN YEAR(orderdate)= '$b' AND MONTH(orderdate)= $c THEN prices ELSE 0 END ) AS lastmonthar 
			FROM
				orders 
			WHERE
				active = 1 
				";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function ARControl()
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        $thismonth  = date('Y-m-01');

        $a = (int)date('m');
        $b = (int)date('Y');
        $c = date('Y-m-d');


        $sql = "  SELECT COUNT
                ( CASE WHEN DATEADD( DAY, a.dates_overdue, a.dates ) > '$c' AND a.status != 'P' THEN 1 ELSE NULL END ) AS QuantitiInvoice,
                SUM (
                CASE
                        
                        WHEN DATEADD( DAY, a.dates_overdue, a.dates ) > '$c' AND dbo.invoices.status != 'P' THEN
                        (a.prices-a.paid) ELSE 0 
                    END 
                    ) AS TotalValue,
                    c.name  AS Customer 
                FROM
                    fin_invoices a
                    INNER JOIN customers c ON a.id_customers = c.id
                WHERE
                    YEAR ( a.dates ) = $b 
                    AND MONTH ( a.dates ) = $a
            GROUP BY
                c.name
                ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function lastmonthar()
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        $thismonth  = date('Y-m-01');

        $a = (int)date('Y-m-01', strtotime($thismonth . ' -1 Months'));
        $b = (int)date('Y');

        $sql = "SELECT 
				SUM	( CASE WHEN YEAR(orderdate)= '$b' AND MONTH(orderdate)= '$a' THEN allowances ELSE 0 END ) AS lastujs,
				SUM ( CASE WHEN YEAR(orderdate)= '$b' AND MONTH(orderdate)= '$a' THEN prices ELSE 0 END ) AS lastmonthar 
			FROM
				orders 
			WHERE
				active = 1 
				";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function sumpaid()
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $a = (int)date('m');
        $b = (int)date('Y');

        $sql = "SELECT
                SUM ( CASE WHEN YEAR(dateinvoice)= '$b' AND MONTH(dateinvoice)= '$a' THEN active ELSE 0 END ) AS ritase,
                SUM ( CASE WHEN YEAR(dateinvoice)= '$b' AND MONTH(dateinvoice)= '$a' THEN nominal ELSE 0 END ) AS allowances

            FROM
                invoices o
            WHERE
                active = 1 
                AND status = 'PAID'";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function sumcc()
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $a = (int)date('m');
        $b = (int)date('Y');

        $sql = "SELECT
                SUM ( CASE WHEN YEAR(o.orderdate)= '$b' AND MONTH(o.orderdate)= '$a'  THEN o.active ELSE 0 END ) AS ritase,
                SUM ( CASE WHEN YEAR(o.orderdate)= '$b' AND MONTH(o.orderdate)= '$a' THEN o.allowances ELSE 0 END ) AS allowances

            FROM
                orders o
                LEFT JOIN cities c ON o.id_citieso = c.id 
                LEFT JOIN orderload l ON o.code = l.order_code 
                LEFT JOIN routes r ON l.id_routes = r.id 
            WHERE
                o.active = 1 
                AND o.ritase = 1               
                AND r.bound = 1";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function sumdsopalembang()
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $a = (int)date('m');
        $b = (int)date('Y');

        $sql = "SELECT
                SUM ( CASE WHEN YEAR(o.orderdate)= '$b' AND MONTH(o.orderdate)= '$a' THEN (o.loadqty * o.pricesadd) ELSE 0 END ) AS hasil
            FROM
                orders o
                LEFT JOIN cities c ON o.id_citieso = c.id 
            WHERE
                o.active = 1 
                AND o.ritase > 0 
                AND o.pricesadd = 44000";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function sumdparetoselisih()
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $a = (int)date('m');
        $b = (int)date('Y');

        $sql = "SELECT
                SUM ( CASE WHEN YEAR(o.orderdate)= '$b' AND MONTH(o.orderdate)= '$a' THEN  o.pricesadd ELSE 0 END ) AS hasilpareto
            FROM
                orders o
                LEFT JOIN cities c ON o.id_citieso = c.id 
            WHERE
                o.active = 1 
                AND o.ritase > 0 
                AND o.pricesadd != 44000";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function reporttrucks($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $wherem = "";
        for ($a = $x; $a <= 12; $a++) {
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritase-$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowances$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceadds$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS prices$a, ";
        }

        $where = " AND YEAR(orders.orderdate) = $tahun";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(f.fleetplateno) LIKE '%" . $this->db->escape_str($data['f.fleetplateno']) . "%' ";
        }

        $order = " ritase DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {
            if ($data['setting'] == 'trucksales') {
                $order = " prices DESC ";
            }
            if ($data['setting'] == 'truckachieves') {
                $order = " prices DESC ";
            }
        }

        $sql = "
            SELECT
            	orders.id_fleets,
                f.fleetplateno,
                $wherem
                SUM(orders.ritase) AS ritase,
                SUM(orders.allowances) AS allowances, 
                SUM(orders.allowanceadds) AS allowanceadds,
                SUM(orders.allowances+orders.allowanceadds) AS costs,
            	SUM(orders.prices) AS prices,
                SUM(orders.prices-(orders.allowances+orders.allowanceadds)) AS achieves
            FROM
            	orders
            	LEFT JOIN fleets f ON orders.id_fleets = f.id
            WHERE
            	orders.active = 1
                AND orders.id_fleets > 0
            	AND orders.ritase > 0
                $where
            GROUP BY
            	orders.id_fleets,
                f.fleetplateno
            ORDER BY
            	$order
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function reporttruckmonths($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $a = (int)date('m');
        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritase, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowances, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceadds, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS prices, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.prices-orders.allowances) ELSE 0 END ) AS achieves, ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " ritase DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }

            if ($data['setting'] == 'truckritaselow') {
                $order = " ritase DESC ";
            }

            if ($data['setting'] == 'trucke') {
                $order = " ritase DESC ";
                $where .= " AND orders.id_fleettypes = 1 ";
            }
            if ($data['setting'] == 'truckf') {
                $order = " ritase DESC ";
                $where .= " AND orders.id_fleettypes = 2 ";
            }
            if ($data['setting'] == 'truckg') {
                $order = " ritase DESC ";
                $where .= " AND orders.id_fleettypes = 3 ";
            }
            if ($data['setting'] == 'truckh') {
                $order = " ritase DESC ";
                $where .= " AND orders.id_fleettypes = 4 ";
            }
            if ($data['setting'] == 'trucki') {
                $order = " ritase DESC ";
                $where .= " AND orders.id_fleettypes = 5 ";
            }
            if ($data['setting'] == 'truckj') {
                $order = " ritase DESC ";
                $where .= " AND orders.id_fleettypes = 6 ";
            }

            if ($data['setting'] == 'trucksales') {
                $order = " prices DESC ";
            }

            if ($data['setting'] == 'truckachieves') {
                $order = " prices DESC ";
            }

            if ($data['setting'] == 'truckachieveslow') {
                $order = " prices DESC ";
            }
        }

        $sql = "
            SELECT
            	orders.id_fleets,
                fleets.fleetplateno,
                $wherem
                fleets.fleetnumber,
                target.carcarrier,
                target.tansya,
                target.towing,
                target.pareto,
                target.wingbox,
                target.dso
            FROM
            	orders
            	LEFT JOIN fleets ON orders.id_fleets = fleets.id,
                target
            WHERE
            	orders.active = 1
                AND orders.id_fleets > 0
                AND orders.ritase > 0
                AND target.month = '$a'
            	AND target.year = '$tahun'
                $where
            GROUP BY
            	orders.id_fleets,
                fleets.fleetplateno,
                fleets.fleetnumber,
                target.carcarrier,
                target.tansya,
                target.towing,
                target.pareto,
                target.wingbox,
                target.dso
            ORDER BY
            	$order
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function fleetPeriode()
    {
        $y = date('Y');

        $today      = date('Y-m-d');
        if (date('D', strtotime($today . ' -1 Days')) == 'Sun') {
            $yesterday  = date('Y-m-d', strtotime($today . ' -2 Days'));
        } else {
            $yesterday  = date('Y-m-d', strtotime($today . ' -1 Days'));
        }


        $thismonday = date('Y-m-d', strtotime($today . ' this week'));
        $thissunday = date('Y-m-d', strtotime($today . ' this sunday'));

        $lastmonday = date('Y-m-d', strtotime($thismonday . ' -1 week'));
        $lastsunday = date('Y-m-d', strtotime($thissunday . ' -1 week'));

        $thismonth  = date('Y-m-01');
        $thismonthlast = date('Y-m-t');
        $lastmonth  = date('Y-m-01', strtotime($thismonth . ' -1 Months'));
        $lastmonthlast = date('Y-m-t', strtotime($lastmonth));

        $thism = date('m', strtotime($thismonth));
        $lastm = date('m', strtotime($lastmonth));

        $thisy = date('Y', strtotime($thismonth));
        $lastys = date('Y', strtotime($lastmonth));

        $thisyear   = date('Y-m-01');
        $lastyear   = date('Y-m-d', strtotime($thisyear . ' -1 years'));

        $lasty = date('Y', strtotime($lastyear));

        $sql = "
            SELECT fleets.fleetnumber,
                SUM( CASE WHEN orderdate = '$today' THEN prices ELSE 0 END ) AS order_today,
                SUM( CASE WHEN orderdate = '$yesterday' THEN prices ELSE 0 END ) AS order_yesterday,
                SUM( CASE WHEN orderdate BETWEEN '$thismonday' AND '$thissunday' THEN prices ELSE 0 END ) AS order_thisweek,
                SUM( CASE WHEN orderdate BETWEEN '$lastmonday' AND '$lastsunday' THEN prices ELSE 0 END ) AS order_lastweek,
                SUM( CASE WHEN orderdate BETWEEN '$thismonth' AND '$thismonthlast' THEN prices ELSE 0 END ) AS order_thismonth,
                SUM( CASE WHEN orderdate BETWEEN '$lastmonth' AND '$lastmonthlast' THEN prices ELSE 0 END ) AS order_lastmonth,
                SUM( CASE WHEN YEAR(orderdate) = '$y' THEN prices ELSE 0 END ) AS order_thisyear,
                SUM( CASE WHEN YEAR(orderdate) = '$lasty' THEN prices ELSE 0 END ) AS order_lastyear
            FROM 
                orders 
                LEFT JOIN fleets ON orders.id_fleets = fleets.id

            WHERE 
                orders.active = 1
            GROUP BY
                fleets.fleetnumber                
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    //TARGET
    public function targetmonth($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $a = (int)date('m');
        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess, ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " ritase DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }

            if ($data['setting'] == 'truckritaselow') {
                $order = " ritase DESC ";
            }

            if ($data['setting'] == 'trucksales') {
                $order = " prices DESC ";
            }

            if ($data['setting'] == 'truckachieves') {
                $order = " prices DESC ";
            }

            if ($data['setting'] == 'truckachieveslow') {
                $order = " prices DESC ";
            }
        }

        $sql = "
        SELECT
                orders.id_customers,
                customers.code AS customers_code,

                orders.active as act,
                $wherem

                CASE WHEN customers.alias != '' OR customers.alias IS NOT NULL THEN customers.alias ELSE customers.name END AS customers_alias,
                customers.name AS customers_name
            FROM
                orders
                LEFT JOIN customers ON orders.id_customers = customers.id
            WHERE
                orders.active = 1
                AND orders.id_customers > 0
                $where
            GROUP BY
                orders.id_customers,
                customers.code,
                customers.alias,
                customers.name
                HAVING
                allowancess >1
            ORDER BY
                $order
        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function targetmontah($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');


        foreach ($result->result() as $key) {
            if (isset($key->top)) {

                $wherem = "";

                $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
                $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
                $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
                $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
                $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
                $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess, ";

                $where = " AND YEAR(orders.orderdate) = $tahun ";
                if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
                    $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
                }

                $order = " allowancess DESC ";
                if (isset($data['setting']) && $data['setting'] != "") {

                    if ($data['setting'] == 'truckritase') {
                        $order = " ritase DESC ";
                    }

                    if ($data['setting'] == 'truckritaselow') {
                        $order = " ritase DESC ";
                    }

                    if ($data['setting'] == 'trucksales') {
                        $order = " prices DESC ";
                    }

                    if ($data['setting'] == 'truckachieves') {
                        $order = " prices DESC ";
                    }

                    if ($data['setting'] == 'truckachieveslow') {
                        $order = " prices DESC ";
                    }
                }

                $sql = "
                    SELECT
                        orders.id_customers,
                        customers.code AS customers_code,
                        orders.active as act,
                        $wherem

                        CASE 
                            WHEN customers.alias != '' 
                            OR customers.alias IS NOT NULL 
                            THEN customers.alias ELSE customers.name END 
                            AS customers_alias,

                        customers.name AS customers_name
                        FROM
                            orders
                            LEFT JOIN customers ON orders.id_customers = customers.id
                        WHERE
                            orders.active = 1
                            AND orders.id_customers > 0
                            $where
                        GROUP BY
                            orders.id_customers,
                            orders.active,
                            customers.code,
                            customers.alias,
                            customers.name
                           HAVING
                                SUM (
                                    CASE
                                        WHEN 
                                        YEAR ( orders.orderdate ) = $tahun
                                        AND 
                                        MONTH ( orders.orderdate ) = $a THEN
                                        orders.allowances ELSE 0 
                                        END
                                    ) > $key->top
                        ORDER BY
                            $order
                    ";

                $result = $this->db->query($sql);
                return $result->result_array();

                //print_r($result);  
            }
        }
    }

    public function targetCC($data = null)

    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');



        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " allowancess DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
                        SELECT
                            COUNT(orders.active) AS act,
                            $wherem
                            FROM
                            orders
                            LEFT JOIN cities c ON orders.id_citieso = c.id
                            WHERE
                            orders.active = 1 
                            AND orders.id_fleettypes = 1 
                            AND c.name NOT LIKE '%DSO%'
                            GROUP BY
                            orders.active
                            ORDER BY
                            $order
                        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function detailCC($data = null)

    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');



        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " allowancess DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
                        SELECT 
                            COUNT( orders.active ) AS act,
                            fleets.fleetnumber,
                            target.carcarrier as tartget,
                            $wherem
                                FROM
                                    orders
                                        LEFT JOIN fleets ON orders.id_fleets = fleets.id
                                        LEFT JOIN cities c ON orders.id_citieso = c.id,
                                        target 
                                            WHERE
                                                orders.active = 1 
                                                AND orders.id_fleettypes = 1 
                                                AND c.name NOT LIKE '%DSO%' 
                                                AND target.month = '$a'
                                                AND target.year = '$tahun'
                                                    GROUP BY
                                                        fleets.fleetnumber,
                                                        orders.active,
                                                        target.carcarrier 
                                                            ORDER BY
                                                                $order
                        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }


    public function targetTN($data = null)

    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');



        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " allowancess DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
                        SELECT
                            COUNT(orders.active) AS act,
                            $wherem
                            FROM
                            orders
                            LEFT JOIN cities c ON orders.id_citieso = c.id
                            WHERE
                            orders.active = 1 
                            AND orders.id_fleettypes = 2 
                            AND c.name NOT LIKE '%DSO%'
                            GROUP BY
                            orders.active
                            ORDER BY
                            $order
                        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function detailTN($data = null)

    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');



        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " allowancess DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
                         SELECT 
                            COUNT( orders.active ) AS act,
                            fleets.fleetnumber,
                            target.tansya as tartget,
                            $wherem
                                FROM
                                    orders
                                        LEFT JOIN fleets ON orders.id_fleets = fleets.id
                                        LEFT JOIN cities c ON orders.id_citieso = c.id,
                                        target 
                                            WHERE
                                                orders.active = 1 
                                                AND orders.id_fleettypes = 2 
                                                AND c.name NOT LIKE '%DSO%' 
                                                AND target.month = '$a'
                                                AND target.year = '$tahun'
                                                    GROUP BY
                                                        fleets.fleetnumber,
                                                        orders.active,
                                                        target.tansya 
                                                            ORDER BY
                                                                $order
                        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }



    public function targetTW($data = null)

    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');



        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " allowancess DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
                        SELECT
                            COUNT(orders.active) AS act,
                            $wherem
                            FROM
                            orders
                            LEFT JOIN cities c ON orders.id_citieso = c.id
                            WHERE
                            orders.active = 1 
                            AND orders.id_fleettypes = 3 
                            AND c.name NOT LIKE '%DSO%'
                            GROUP BY
                            orders.active
                            ORDER BY
                            $order
                        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function detailTW($data = null)

    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');



        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " allowancess DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
                         SELECT 
                            COUNT( orders.active ) AS act,
                            fleets.fleetnumber,
                            target.towing as tartget,
                            $wherem
                                FROM
                                    orders
                                        LEFT JOIN fleets ON orders.id_fleets = fleets.id
                                        LEFT JOIN cities c ON orders.id_citieso = c.id,
                                        target 
                                            WHERE
                                                orders.active = 1 
                                                AND orders.id_fleettypes = 3 
                                                AND c.name NOT LIKE '%DSO%' 
                                                AND target.month = '$a'
                                                AND target.year = '$tahun'
                                                    GROUP BY
                                                        fleets.fleetnumber,
                                                        orders.active,
                                                        target.towing 
                                                            ORDER BY
                                                                $order
                        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }


    public function targetWB($data = null)

    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');



        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " allowancess DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
                        SELECT
                            COUNT(orders.active) AS act,
                            $wherem
                            FROM
                            orders
                            LEFT JOIN cities c ON orders.id_citieso = c.id
                            WHERE
                            orders.active = 1 
                            AND orders.id_fleettypes = 5 
                            AND c.name NOT LIKE '%DSO%'
                            GROUP BY
                            orders.active
                            ORDER BY
                            $order
                        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function detailWB($data = null)

    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');



        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " allowancess DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
                         SELECT 
                            COUNT( orders.active ) AS act,
                            fleets.fleetnumber,
                            target.wingbox as tartget,
                            $wherem
                                FROM
                                    orders
                                        LEFT JOIN fleets ON orders.id_fleets = fleets.id
                                        LEFT JOIN cities c ON orders.id_citieso = c.id,
                                        target 
                                            WHERE
                                                orders.active = 1 
                                                AND orders.id_fleettypes = 5 
                                                AND c.name NOT LIKE '%DSO%' 
                                                AND target.month = '$a'
                                                AND target.year = '$tahun'
                                                    GROUP BY
                                                        fleets.fleetnumber,
                                                        orders.active,
                                                        target.wingbox 
                                                            ORDER BY
                                                                $order
                        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function targetVer2($data = null)
    {
        $today = date("Y-m-d");
        $years = (int)date("Y");
        $months = (int)date("m");

        /**
        $sql = "
        SELECT
        	a.id,
        	a.name,
            COALESCE((SELECT COUNT(DISTINCT f.id) AS unit_ready FROM orders o 
        				LEFT JOIN fleets f ON o.id_fleets = f.id
        				LEFT JOIN fleettypes ft ON ft.id = f.id_fleettypes
        	 WHERE YEAR(o.orderdate) = $years 
        		AND MONTH(o.orderdate) = $months
        		AND f.id_fleettypes = (a.id)
        	),0) AS QuantitiType,
        	COALESCE((SELECT SUM(o.prices) AS prices FROM orders o 
        				LEFT JOIN fleets f ON o.id_fleets = f.id
        				LEFT JOIN fleettypes ft ON ft.id = f.id_fleettypes
        	 WHERE YEAR(o.orderdate) = $years 
        		AND MONTH(o.orderdate) = $months
        		AND f.id_fleettypes = (a.id)
        	),0) AS Actual,
        	COALESCE((SELECT target_prices FROM targets WHERE years = $years AND months = $months AND id_fleettypes = (a.id) ),0) AS Target
        FROM
        	fleettypes a
        ";
         */
        $sql = "SELECT
                    a.id,
                    a.name,
                    COALESCE((SELECT COUNT(DISTINCT f.id) AS unit_ready 
                                FROM fleets f
                                LEFT JOIN fleettypes ft ON ft.id = f.id_fleettypes
                    WHERE f.id_fleettypes = (a.id)
                    ),0) AS QuantitiType,
                    COALESCE((SELECT SUM(o.prices) AS prices FROM orders o 
                                LEFT JOIN fleets f ON o.id_fleets = f.id
                                LEFT JOIN fleettypes ft ON ft.id = o.id_ordertypes
                    WHERE YEAR(o.orderdate) = $years 
                        AND MONTH(o.orderdate) = $months
                        AND f.id_fleettypes = (a.id)
                    ),0) AS Actual,
                    COALESCE((SELECT target_prices FROM targets WHERE years = $years AND months = $months AND id_fleettypes = (a.id) ),0) AS Target
                FROM
                    fleettypes a
                WHERE
                    a.active = 1";

        $result = $this->db->query($sql);
        return $result->result_array();
    }
    public function targetPR($data = null)

    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');



        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " allowancess DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
                        SELECT
                            COUNT(orders.active) AS act,
                            $wherem
                            FROM
                            orders
                            LEFT JOIN cities c ON orders.id_citieso = c.id
                            WHERE
                            orders.active = 1 
                            AND orders.id_fleettypes = 4 
                            AND c.name NOT LIKE '%DSO%'
                            GROUP BY
                            orders.active
                            ORDER BY
                            $order
                        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function detailPR($data = null)

    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');



        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " allowancess DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
                         SELECT 
                            COUNT( orders.active ) AS act,
                            fleets.fleetnumber,
                            target.pareto as tartget,
                            $wherem
                                FROM
                                    orders
                                        LEFT JOIN fleets ON orders.id_fleets = fleets.id
                                        LEFT JOIN cities c ON orders.id_citieso = c.id,
                                        target 
                                            WHERE
                                                orders.active = 1 
                                                AND orders.id_fleettypes = 4 
                                                AND c.name NOT LIKE '%DSO%' 
                                                AND target.month = '$a'
                                                AND target.year = '$tahun'
                                                    GROUP BY
                                                        fleets.fleetnumber,
                                                        orders.active,
                                                        target.pareto 
                                                            ORDER BY
                                                                $order
                        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }


    public function targetDSOw($data = null)

    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');



        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        //$wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ( (orders.prices+orders.pricesadd)-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achievess ";
        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " allowancess DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
                        SELECT
                            COUNT(orders.active) AS act,
                            $wherem
                            FROM
                            orders
                            LEFT JOIN cities c ON orders.id_citieso = c.id
                            WHERE
                            orders.active = 1 
                            AND c.name LIKE '%DSO%'
                            GROUP BY
                            orders.active
                            ORDER BY
                            $order
                        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function detailDSOw($data = null)

    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;



        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month', $a);
        $result = $this->db->get('target');



        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ((orders.prices+orders.pricesadd)-orders.allowances+orders.allowanceadds) ELSE 0 END ) AS achievess ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['fleetplateno']) && $data['fleetplateno'] != '') {
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%" . $this->db->escape_str($data['fleets.fleetplateno']) . "%' ";
        }

        $order = " allowancess DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'truckritase') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
                        SELECT
                            COUNT( orders.active ) AS act,
                            fleets.fleetnumber,
                            target.dso as tartget,
                            $wherem
                                FROM
                                    orders
                                        LEFT JOIN fleets ON orders.id_fleets = fleets.id
                                        LEFT JOIN cities c ON orders.id_citieso = c.id,
                                        target  
                                            WHERE
                                                orders.active = 1  
                                                AND c.name LIKE '%DSO%'
                                                AND target.month = '$a'
                                                AND target.year = '$tahun'
                                                    GROUP BY
                                                        fleets.fleetnumber,
                                                        orders.active,
                                                        target.dso  
                                                            ORDER BY
                                                                $order
                        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    /*DRIVER*/

    public function reportdrivermonths($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $a = (int)date('m');
        $wherem = "";

        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.ritase ELSE 0 END ) AS ritase, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowances ELSE 0 END ) AS allowances, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.allowanceadds ELSE 0 END ) AS allowanceadds, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN orders.prices ELSE 0 END ) AS prices, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = '$a' THEN ((orders.prices)-orders.allowances) ELSE 0 END ) AS achieves ";

        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if (isset($data['name']) && $data['name'] != '') {
            $where .= " AND LOWER(drivers.name) LIKE '%" . $this->db->escape_str($data['drivers.name']) . "%' ";
        }

        $order = " ritase DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'driverritase') {
                $order = " ritase DESC ";
            }

            if ($data['setting'] == 'driverritaselow') {
                $order = " ritase DESC ";
            }

            if ($data['setting'] == 'trucksales') {
                $order = " prices DESC ";
            }

            if ($data['setting'] == 'truckachieves') {
                $order = " prices DESC ";
            }

            if ($data['setting'] == 'truckachieveslow') {
                $order = " prices DESC ";
            }
        }

        $sql = "
                SELECT
                    orders.id_drivers,
                    drivers.name,
                    $wherem
                FROM
                    orders
                    LEFT JOIN drivers ON orders.id_drivers = drivers.id
                    LEFT JOIN customers ON orders.id_customers = customers.id 
                WHERE
                    orders.active = 1
                    AND orders.id_drivers > 0
                    AND orders.ritase > 0
                    $where
                GROUP BY
                    orders.id_drivers,
                    drivers.name
                ORDER BY
                    $order
            ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    /** CLAIM */

    public function reportclaims($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $a = (int)date('m');
        $wherem = "";

        $wherem .= " SUM( CASE WHEN orderclaims.claimdate BETWEEN '$tahun-01-01' AND '$tahun-$a-30' THEN orderclaims.active ELSE  0 END ) AS ritase, ";
        $wherem .= " SUM( CASE WHEN orderclaims.claimdate BETWEEN '$tahun-01-01' AND '$tahun-$a-30' THEN orderclaims.cost ELSE 0 END) AS allowances ";

        $where = " AND YEAR(orderclaims.claimdate) = $tahun ";
        if (isset($data['name']) && $data['name'] != '') {
            $where .= " AND LOWER(drivers.name) LIKE '%" . $this->db->escape_str($data['drivers.name']) . "%' ";
        }

        $order = " ritase DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {

            if ($data['setting'] == 'claim') {
                $order = " ritase DESC ";
            }
        }

        $sql = "
            SELECT 
                orderclaims.driver,
                        COALESCE ( SUM ( orderclaims.cost ), 0 ) AS allowances,
                        COALESCE ( COUNT ( orderclaims.driver ), 0 ) AS ritase 
            FROM
                orderclaims
            WHERE
                orderclaims.active = 1
            GROUP BY
                    orderclaims.driver
                    order by 
                    $order
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }




    function reportclaimmonths($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;

        $wherem = "";
        $a = isset($data['bulan']) && $data['bulan'] > 0 ? $data['bulan'] : date('m');
        $wherem .= " SUM( CASE WHEN YEAR(oc.claimdate) = '$tahun' AND MONTH(oc.claimdate) = '$a' THEN 1 ELSE 0 END ) AS totals, ";
        $wherem .= " SUM( CASE WHEN YEAR(oc.claimdate) = '$tahun' AND MONTH(oc.claimdate) = '$a' THEN COALESCE(oc.prices,0) ELSE 0 END ) AS prices, ";


        $where = " AND YEAR(oc.claimdate) = $tahun";
        if (isset($data['driver_fullname']) && $data['driver_fullname'] != '') {
            $where .= " AND LOWER(d.name) LIKE '%" . $this->db->escape_str($data['driver_fullname']) . "%' ";
        }

        $order = " totals DESC ";
        if (isset($data['setting']) && $data['setting'] != "") {
            if ($data['setting'] == 'ritase') {
                $order = " totals DESC ";
            }
        }

        $sql = "
            SELECT
            	d.code,
            	d.name,
                $wherem
            	COUNT(CASE WHEN YEAR(oc.claimdate) = $tahun THEN 1 ELSE NULL END ) AS grandtotals,
                SUM(CASE WHEN YEAR(oc.claimdate) = $tahun THEN COALESCE(oc.prices,0) ELSE NULL END ) AS grandprices
            FROM
            	orderclaims oc
            	LEFT JOIN orderassigns oa ON oc.id_orderassigns = orders.id
            	LEFT JOIN orders o ON oc.id_orders = o.id
            	LEFT JOIN drivers d ON orders.id_drivers = d.id
            WHERE
            	oc.active = 1
            GROUP BY
            	d.code,
            	d.name
            ORDER BY
            	$order
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function profit_and_loss($data = null)
    {
        $code = $this->db->escape_str($this->input->post_get("code"));
        $shipment = $this->db->escape_str($this->input->post_get("shippment"));
        $customer_name = $this->db->escape_str($this->input->post_get("customer_name"));
        $datefrom = $this->db->escape_str($this->input->post_get("datefrom")) != "" ? $this->db->escape_str($this->input->post_get("datefrom")) : "";
        $dateto = $this->db->escape_str($this->input->post_get("dateto")) != "" ? $this->db->escape_str($this->input->post_get("dateto")) : "";
        $driver_name = $this->db->escape_str($this->input->post_get("driver_name"));
        $fleetplateno = $this->db->escape_str($this->input->post_get("fleetplateno"));
        $origin = $this->db->escape_str($this->input->post_get("origin"));
        $destination = $this->db->escape_str($this->input->post_get("destination"));
        $fleettype_name = $this->db->escape_str($this->input->post_get("fleettype_name"));

        $where = "";
        $where_orderdate = "";
        $where_maindate = "";
        $where_leasdate = "";
        if ($datefrom != "" && $dateto) {
            $where_orderdate .= " AND a.orderdate BETWEEN '$datefrom 00:00:00' AND '$dateto 23:59:59' ";
            $where_maindate .= " AND dates BETWEEN '$datefrom 00:00:00' AND '$dateto 23:59:59' ";
            $where_leasdate .= " AND due_dates BETWEEN '$datefrom' AND '$dateto' ";
        }

        if ($fleetplateno != "") {
            $where .= " AND f.fleetplateno LIKE '%$fleetplateno%' ";
        }

        if ($fleettype_name != "") {
            $where .= " AND fl.name LIKE '%$fleettype_name%' ";
        }

        /**
        if($this->session->userdata("id_userroles") == 2) {
            $where .= " AND ( a.createby = '".$this->session->userdata("id")."' OR a.id_koordinators = '".$this->session->userdata("id")."' ) ";
        }
         */
        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( A.RowNum BETWEEN $first AND $last )";
        }

        $sql = "
        SELECT A.* FROM 
        ( 
            SELECT
            	ROW_NUMBER() OVER (Order by f.fleetplateno) AS RowNum,
            	f.id,
            	f.fleetplateno,
            	ft.name AS fleettype_name,
            	COALESCE(A.prices, 0) AS prices,
            	COALESCE(A.pricesadd, 0) AS pricesadd,
            	COALESCE(A.allowances, 0) AS allowances,
            	COALESCE(A.allowanceadds, 0) AS allowanceadds,
            	COALESCE(A.allowancereds, 0) AS allowancereds,
            	COALESCE(m.prices_part, 0) AS prices_part,
            	COALESCE(m.prices_service, 0) AS prices_service,
                COALESCE(fte.prices, 0) AS prices_leasing
            FROM 
            	fleets f
            	LEFT JOIN fleettypes ft ON f.id_fleettypes = ft.id
            	LEFT JOIN (
            		SELECT
            			a.id_fleets,
            			SUM(a.prices) AS prices,
            			SUM(a.pricesadd) AS pricesadd,
            			SUM(a.allowances) AS allowances,
            			SUM(a.allowanceadds) AS allowanceadds,
            			SUM(a.allowancereds) AS allowancereds
            		FROM orders a
            		WHERE
            			a.active = 1
       		            $where_orderdate
            		GROUP BY a.id_fleets
                ) A ON f.id = A.id_fleets
            	LEFT JOIN (SELECT id_fleets, SUM(prices_part) AS prices_part, SUM(prices_service) AS prices_service
            						 FROM maintenances 
            				     WHERE active = 1 $where_maindate
                         GROUP BY id_fleets) m ON f.id = m.id_fleets 
                LEFT JOIN (SELECT id_fleets, SUM(prices) AS prices
            						 FROM fleet_tenors 
            				     WHERE active = 1 $where_leasdate
                         GROUP BY id_fleets) fte ON f.id = fte.id_fleets 
            WHERE
            	F.active = 1
            $where
        )A
        WHERE 
            A.fleetplateno IS NOT NULL
            $paging
        ORDER BY fleetplateno DESC
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }
}
