<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Model {
    
    public function getOrderNum($id_companies = 0)
    {
        $y = date('Y');
        
        $today      = date('Y-m-d');
        if(date('D', strtotime($today.' -1 Days')) == 'Sun' )
        {
            $yesterday  = date('Y-m-d', strtotime($today.' -2 Days'));
        }
        else
        {
            $yesterday  = date('Y-m-d', strtotime($today.' -1 Days'));
        }
        
        
        $thismonday = date('Y-m-d', strtotime($today.' this week'));
        $thissunday = date('Y-m-d', strtotime($today.' this sunday'));
        
        $lastmonday = date('Y-m-d', strtotime($thismonday.' -1 week'));
        $lastsunday = date('Y-m-d', strtotime($thissunday.' -1 week'));
        
        $thismonth  = date('Y-m-01');
        $thismonthlast = date('Y-m-t');
        $lastmonth  = date('Y-m-01', strtotime($thismonth.' -1 Months'));
        $lastmonthlast = date('Y-m-t', strtotime($lastmonth));
        
        $thism = date('m', strtotime($thismonth));
        $lastm = date('m', strtotime($lastmonth));
        
        $thisy = date('Y', strtotime($thismonth));
        $lastys = date('Y', strtotime($lastmonth));
        
        $thisyear   = date('Y-m-01');
        $lastyear   = date('Y-m-d', strtotime($thisyear.' -1 years'));
        
        $lasty = date('Y', strtotime($lastyear));
        
        $sql = "
            SELECT 
                COUNT( CASE WHEN orderdate = '$today' THEN 1 ELSE NULL END ) AS order_today,
                COUNT( CASE WHEN orderdate = '$yesterday' THEN 1 ELSE NULL END ) AS order_yesterday,
                COUNT( CASE WHEN orderdate BETWEEN '$thismonday' AND '$thissunday' THEN 1 ELSE NULL END ) AS order_thisweek,
                COUNT( CASE WHEN orderdate BETWEEN '$lastmonday' AND '$lastsunday' THEN 1 ELSE NULL END ) AS order_lastweek,
                COUNT( CASE WHEN orderdate BETWEEN '$thismonth' AND '$thismonthlast' THEN 1 ELSE NULL END ) AS order_thismonth,
                COUNT( CASE WHEN orderdate BETWEEN '$lastmonth' AND '$lastmonthlast' THEN 1 ELSE NULL END ) AS order_lastmonth,
                COUNT( CASE WHEN YEAR(orderdate) = '$y' THEN 1 ELSE NULL END ) AS order_thisyear,
                COUNT( CASE WHEN YEAR(orderdate) = '$lasty' THEN 1 ELSE NULL END ) AS order_lastyear
            FROM 
                orders 
            WHERE active = 1 
                
        ";
    
        $result = $this->db->query($sql);
        return $result->row();
        
    }
    
    public function getOrderNumYear($year = 0)
    {
        if($year > 0)
        {
            $select = "";
            for($a=1;$a<=12;$a++){
                $select .= " COUNT(CASE WHEN YEAR(orderdate) = '$year' AND MONTH(orderdate) = '$a' THEN 1 ELSE NULL END ) AS order$a, ";
            }
            $sql = "SELECT
                        $select
                        COUNT( CASE WHEN YEAR(orderdate) = '$year' THEN 1 ELSE NULL END ) AS order_year
                    FROM 
                        orders 
                    WHERE active = 1 
                
            ";
            
            $result = $this->db->query($sql);
            return $result->row();
        }
        
        
    }
    
    public function truckritase($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        
        $wherem = "";
        for($a=$x;$a<=12;$a++){
            $wherem .= " COUNT( CASE WHEN YEAR(o.orderdate) = '$tahun' AND MONTH(o.orderdate) = 9 THEN 1 ELSE NULL END ) AS ritase$a, ";
        }
        
        $where = " AND YEAR(o.orderdate) = $tahun";
        if($data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleetplateno'])."%' ";
        }
        
        $sql = "
            SELECT
            	orders.id_fleets,
            	fleets.fleetplateno,
                $wherem
            	COUNT(*) AS ritase
            FROM
            	orders o
            	LEFT JOIN orderassigns oa ON o.id_orderassigns = orders.id
            	LEFT JOIN fleets f ON orders.id_fleets = fleets.id
            WHERE
            	orders.active = 1
            	AND o.active = 1
                $where
            GROUP BY
            	orders.id_fleets,
            	f.fleetplateno
            ORDER BY
            	ritase DESC
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }
    
    public function trucksales($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        
        $wherem = "";
        for($a=$x;$a<=12;$a++){
            $wherem .= " SUM( CASE WHEN YEAR(o.orderdate) = '$tahun' AND MONTH(o.orderdate) = 9 THEN o.prices ELSE 0 END ) AS prices$a, ";
        }
        
        $where = " AND YEAR(o.orderdate) = $tahun";
        if($data['fleetplateno'] != ''){
            $where .= " AND LOWER(f.fleetplateno) LIKE '%".$this->db->escape_str($data['fleetplateno'])."%' ";
        }
        
        $sql = "
            SELECT
            	orders.id_fleets,
            	f.fleetplateno,
                $wherem
            	SUM(o.prices) AS prices
            FROM
            	orders o
            	LEFT JOIN fleets f ON orders.id_fleets = f.id
            WHERE
            	orders.active = 1
            	AND o.active = 1
                $where
            GROUP BY
            	orders.id_fleets,
            	f.fleetplateno
            ORDER BY
            	prices DESC
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }
    
    public function truckritasesales($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        
        $wherem = "";
        for($a=$x;$a<=12;$a++){
            $wherem .= " COUNT( CASE WHEN YEAR(o.orderdate) = '$tahun' AND MONTH(o.orderdate) = 9 THEN 1 ELSE NULL END ) AS ritase$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(o.orderdate) = '$tahun' AND MONTH(o.orderdate) = 9 THEN o.prices ELSE 0 END ) AS prices$a, ";
        }
        
        $where = " AND YEAR(o.orderdate) = $tahun";
        if($data['fleetplateno'] != ''){
            $where .= " AND LOWER(f.fleetplateno) LIKE '%".$this->db->escape_str($data['fleetplateno'])."%' ";
        }
        
        $sql = "
            SELECT
            	orders.id_fleets,
            	f.fleetplateno,
                $wherem
                COUNT(*) AS ritase,
            	SUM(o.prices) AS prices
            FROM
            	orders o
            	LEFT JOIN orderassigns oa ON o.id_orderassigns = orders.id
            	LEFT JOIN fleets f ON orders.id_fleets = f.id
            WHERE
            	orders.active = 1
            	AND o.active = 1
                $where
            GROUP BY
            	orders.id_fleets,
            	f.fleetplateno
            ORDER BY
            	prices DESC
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }
    
    
    
    public function reportcustomers($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        
        $wherem = "";
        for($a=$x;$a<=12;$a++){
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritase$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowances$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceadds$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS prices$a, ";
        }
        
        $where = " AND YEAR(orders.orderdate) = $tahun";
        if(isset($data['customers_name']) && $data['customers_name'] != ''){
            $where .= " AND LOWER(customers.name) LIKE '%".$this->db->escape_str($data['customers_name'])."%' ";
        }
        
        $order = " ritase DESC ";
        if(isset($data['setting']) && $data['setting'] != ""){
            if($data['setting'] == 'sales'){
                $order = " prices DESC ";
            }
            if($data['setting'] == 'achieves'){
                $order = " achieves DESC ";
            }
        }
        
        $sql = "
            SELECT
            	orders.id_customers,
                customers.code AS customers_code,
                customers.name AS customers_name,
            	CASE WHEN customers.nickname != '' OR customers.nickname IS NOT NULL THEN customers.nickname ELSE customers.name END AS customers_nickname,
                $wherem
                SUM(orders.ritase) AS ritase,
                SUM(orders.allowances) AS allowances,
                SUM(orders.allowanceadds) AS allowanceadds,
            	SUM(orders.allowances+orders.allowanceadds) AS costs,
            	SUM(orders.prices) AS prices,
                SUM(orders.prices-(orders.allowances+orders.allowanceadds)) AS achieves
            FROM
            	orders
            	LEFT JOIN customers ON orders.id_customers = customers.id
            WHERE
            	orders.active = 1
            	AND orders.ritase > 0
                AND orders.id_customers > 0
                $where
            GROUP BY
            	orders.id_customers,
                customers.code,
                customers.nickname,
            	customers.name
            ORDER BY
            	$order
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }
    
    public function reportcustomermonths($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        $a = date('m');
        
        $wherem = "";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritase, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowances, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceadds, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS prices, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.prices-(orders.allowances+orders.allowanceadds)) ELSE 0 END ) AS achieves, ";
    
        $where = " AND YEAR(orders.orderdate) = $tahun";
        if(isset($data['customers_name']) && $data['customers_name'] != ''){
            $where .= " AND LOWER(customers.name) LIKE '%".$this->db->escape_str($data['customers_name'])."%' ";
        }
        
        $order = " ritase DESC ";
        if(isset($data['setting']) && $data['setting'] != ""){
            if($data['setting'] == 'sales'){
                $order = " prices DESC ";
            }
            if($data['setting'] == 'achieves'){
                $order = " achieves DESC ";
            }
        }
        
        $sql = "
            SELECT
            	orders.id_customers,
                customers.code AS customers_code,
                $wherem
                CASE WHEN customers.nickname != '' OR customers.nickname IS NOT NULL THEN customers.nickname ELSE customers.name END AS customers_nickname,
                customers.name AS customers_name
            FROM
            	orders
            	LEFT JOIN customers ON orders.id_customers = customers.id
            WHERE
            	orders.active = 1
                AND orders.id_customers > 0
                $where
            GROUP BY
            	orders.id_customers,
                customers.code,
                customers.nickname,
            	customers.name
            ORDER BY
            	$order
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }
    
    /** TRUCK */
    public function targettruck($value='')
    {
        $sql = "SELECT
                    SUM ( o.ritase ) AS ritase,
                    MONTH(o.orderdate) as date
                FROM
                    orders o
                    LEFT JOIN fleets f ON o.id_fleets = f.id 
                WHERE
                    o.active = 1 
                    AND o.ritase > 0 
                    AND o.id_customers != 48 
                    
                GROUP BY
                    MONTH(o.orderdate)";
        $result = $this->db->query($sql);
        return $result->result();

    }

     public function targetdsoluar($value='')
    {
        $sql = "SELECT SUM
                    ( o.ritase ) AS ritase,
                    MONTH ( o.orderdate ) AS DATE
                FROM
                    orders o
                    LEFT JOIN fleets f ON o.id_fleets = f.id 
                WHERE
                    o.active = 1 
                    AND o.ritase > 0 
                    AND o.id_customers = 48 
                   AND NOT (
                    o.id_citiesd = 78 
                OR o.id_citiesd = 65 
                OR o.id_citiesd = 41)
                GROUP BY
                    MONTH (o.orderdate)";
        $result = $this->db->query($sql);
        return $result->result();

    }

     public function targetdso($value='')
    {
        $sql = "SELECT SUM
                    ( o.ritase ) AS ritase,
                    MONTH ( o.orderdate ) AS DATE
                FROM
                    orders o
                    LEFT JOIN fleets f ON o.id_fleets = f.id 
                WHERE
                    o.active = 1 
                    AND o.ritase > 0 
                    AND o.id_customers = 48 
                    AND (o.id_citiesd = 78 OR o.id_citiesd = 65 OR o.id_citiesd = 41)
                GROUP BY
                    MONTH (o.orderdate)";
        $result = $this->db->query($sql);
        return $result->result();

    }

    public function sumdso()
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        
        $a = (int)date('m');

        $sql="SELECT
                SUM ( CASE WHEN o.orderdate BETWEEN '2019-09-01' AND '2019-09-30 00:00:00' THEN o.active ELSE 0 END ) AS ritase,
                SUM ( CASE WHEN o.orderdate BETWEEN '2019-09-01' AND '2019-09-30' THEN o.allowances ELSE 0 END ) AS allowances,
                SUM ( CASE WHEN o.orderdate BETWEEN '2019-09-01' AND '2019-09-30' THEN o.allowanceadds ELSE 0 END ) AS allowanceadds 

            FROM
                orders o
                LEFT JOIN cities c ON o.id_citieso = c.id 
            WHERE
                o.active = 1 
                AND o.ritase > 0 
                AND c.name LIKE '%DSO%'";
       
       $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function sumcc()
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        
        $a = (int)date('m');

        $sql="SELECT
                SUM ( CASE WHEN o.orderdate BETWEEN '2019-09-01' AND '2019-10-01 00:00:00' THEN o.active ELSE 0 END ) AS ritase,
                SUM ( CASE WHEN o.orderdate BETWEEN '2019-09-01' AND '2019-09-30' THEN o.allowances ELSE 0 END ) AS allowances,
                SUM ( CASE WHEN o.orderdate BETWEEN '2019-09-01' AND '2019-09-30' THEN o.allowanceadds ELSE 0 END ) AS allowanceadds 
            FROM
                orders o
                LEFT JOIN cities c ON o.id_citieso = c.id 
            WHERE
                o.active = 1 
                AND o.ritase > 0 
                AND c.name NOT LIKE '%DSO%'";
       
       $result = $this->db->query($sql);
        return $result->result_array();
    }
    
    public function reporttrucks($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        
        $wherem = "";
        for($a=$x;$a<=12;$a++){
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritase-$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowances$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceadds$a, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS prices$a, ";
        }
        
        $where = " AND YEAR(orders.orderdate) = $tahun";
        if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(f.fleetplateno) LIKE '%".$this->db->escape_str($data['f.fleetplateno'])."%' ";
        }
        
        $order = " ritase DESC ";
        if(isset($data['setting']) && $data['setting'] != ""){
            if($data['setting'] == 'trucksales'){
                $order = " prices DESC ";
            }
            if($data['setting'] == 'truckachieves'){
                $order = " prices DESC ";
            }
        }
        
        $sql = "
            SELECT
            	orders.id_fleets,
                f.fleetplateno,
                $wherem
                SUM(orders.ritase) AS ritase,
                SUM(orders.allowances) AS allowances, 
                SUM(orders.allowanceadds) AS allowanceadds,
                SUM(orders.allowances+orders.allowanceadds) AS costs,
            	SUM(orders.prices) AS prices,
                SUM(orders.prices-(orders.allowances+orders.allowanceadds)) AS achieves
            FROM
            	orders
            	LEFT JOIN fleets f ON orders.id_fleets = f.id
            WHERE
            	orders.active = 1
                AND orders.id_fleets > 0
            	AND orders.ritase > 0
                $where
            GROUP BY
            	orders.id_fleets,
                f.fleetplateno
            ORDER BY
            	$order
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }
    
    public function reporttruckmonths($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        
        $a = (int)date('m');
        $wherem = "";
        
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritase, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowances, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceadds, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS prices, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances-orders.prices) ELSE 0 END ) AS achieves, ";
        
        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
        }
        
        $order = " ritase DESC ";
        if(isset($data['setting']) && $data['setting'] != ""){
            
            if($data['setting'] == 'truckritase'){
                $order = " ritase DESC ";
            }
            
            if($data['setting'] == 'truckritaselow'){
                $order = " ritase DESC ";
            } 

            if($data['setting'] == 'trucke'){
                $order = " ritase DESC ";
                $where .= " AND orders.id_ordertypes = 1 ";
            }
            if($data['setting'] == 'truckf'){
                $order = " ritase DESC ";
                $where .= " AND orders.id_ordertypes = 2 ";
            }
            if($data['setting'] == 'truckg'){
                $order = " ritase DESC ";
                $where .= " AND orders.id_ordertypes = 3 ";
            }
            if($data['setting'] == 'truckh'){
                $order = " ritase DESC ";
                $where .= " AND orders.id_ordertypes = 4 ";
            }
            if($data['setting'] == 'trucki'){
                $order = " ritase DESC ";
                $where .= " AND orders.id_ordertypes = 5 ";
            }
            if($data['setting'] == 'truckj'){
                $order = " ritase DESC ";
                $where .= " AND orders.id_ordertypes = 6 ";
            }
            
            if($data['setting'] == 'trucksales'){
                $order = " prices DESC ";
            }
            
            if($data['setting'] == 'truckachieves'){
                $order = " prices DESC ";
            }
            
            if($data['setting'] == 'truckachieveslow'){
                $order = " prices DESC ";
            }
        }
        
        $sql = "
            SELECT
            	orders.id_fleets,
                fleets.fleetplateno,
                $wherem
                fleets.fleetnumber,
                target.carcarrier,
                target.tansya,
                target.towing,
                target.pareto,
                target.wingbox,
                target.dso
            FROM
            	orders
            	LEFT JOIN fleets ON orders.id_fleets = fleets.id,
                target
            WHERE
            	orders.active = 1
                AND orders.id_fleets > 0
                AND orders.ritase > 0
                AND target.month = 9
            	AND target.year = '$tahun'
                $where
            GROUP BY
            	orders.id_fleets,
                fleets.fleetplateno,
                fleets.fleetnumber,
                target.carcarrier,
                target.tansya,
                target.towing,
                target.pareto,
                target.wingbox,
                target.dso
            ORDER BY
            	$order
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }
    
    //TARGET
    public function targetmonth($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        
        $a = (int)date('m');
        $wherem = "";
        
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
        $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess, ";
        
        $where = " AND YEAR(orders.orderdate) = $tahun ";
        if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
        }
        
        $order = " ritase DESC ";
        if(isset($data['setting']) && $data['setting'] != ""){
            
            if($data['setting'] == 'truckritase'){
                $order = " ritase DESC ";
            }
            
            if($data['setting'] == 'truckritaselow'){
                $order = " ritase DESC ";
            }
            
            if($data['setting'] == 'trucksales'){
                $order = " prices DESC ";
            }
            
            if($data['setting'] == 'truckachieves'){
                $order = " prices DESC ";
            }
            
            if($data['setting'] == 'truckachieveslow'){
                $order = " prices DESC ";
            }
        }
        
        $sql = "
        SELECT
                orders.id_customers,
                customers.code AS customers_code,

                orders.active as act,
                $wherem

                CASE WHEN customers.nickname != '' OR customers.nickname IS NOT NULL THEN customers.nickname ELSE customers.name END AS customers_nickname,
                customers.name AS customers_name
            FROM
                orders
                LEFT JOIN customers ON orders.id_customers = customers.id
            WHERE
                orders.active = 1
                AND orders.id_customers > 0
                $where
            GROUP BY
                orders.id_customers,
                customers.code,
                customers.nickname,
                customers.name
                HAVING
                allowancess >1
            ORDER BY
                $order
        ";
        
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function targetmontah($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        
        

        $a = (int)date('m');

        $this->db->select('*');
        $this->db->where('month',$a);
        $result=$this->db->get('target');
    
        
        foreach ($result->result() as $key) {
            if (isset($key->top)) {

                $wherem = "";
        
                $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
                $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
                $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
                $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
                $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
                $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess, ";
                
                $where = " AND YEAR(orders.orderdate) = $tahun ";
                if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
                    $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
                }
                
                $order = " allowancess DESC ";
                if(isset($data['setting']) && $data['setting'] != ""){
                    
                    if($data['setting'] == 'truckritase'){
                        $order = " ritase DESC ";
                    }
                    
                    if($data['setting'] == 'truckritaselow'){
                        $order = " ritase DESC ";
                    }
                    
                    if($data['setting'] == 'trucksales'){
                        $order = " prices DESC ";
                    }
                    
                    if($data['setting'] == 'truckachieves'){
                        $order = " prices DESC ";
                    }
                    
                    if($data['setting'] == 'truckachieveslow'){
                        $order = " prices DESC ";
                    }
                }
                
               $sql = "
                    SELECT
                        orders.id_customers,
                        customers.code AS customers_code,
                        orders.active as act,
                        $wherem

                        CASE 
                            WHEN customers.nickname != '' 
                            OR customers.nickname IS NOT NULL 
                            THEN customers.nickname ELSE customers.name END 
                            AS customers_nickname,

                        customers.name AS customers_name
                        FROM
                            orders
                            LEFT JOIN customers ON orders.id_customers = customers.id
                        WHERE
                            orders.active = 1
                            AND orders.id_customers > 0
                            $where
                        GROUP BY
                            orders.id_customers,
                            orders.active,
                            customers.code,
                            customers.nickname,
                            customers.name
                           HAVING
                                SUM (
                                    CASE
                                        WHEN 
                                        YEAR ( orders.orderdate ) = $tahun
                                        AND 
                                        MONTH ( orders.orderdate ) = $a THEN
                                        orders.allowances ELSE 0 
                                        END
                                    ) > $key->top
                        ORDER BY
                            $order
                    ";
                
                $result = $this->db->query($sql);
                return $result->result_array();  

                //print_r($result);  
            }
        }
        
    }

    public function targetCC($data = null)
    
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            

            $a = (int)date('m');

            $this->db->select('*');
            $this->db->where('month',$a);
            $result=$this->db->get('target');
        
          

            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
                    
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
            }
                    
            $order = " allowancess DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                        
            if($data['setting'] == 'truckritase'){
            $order = " ritase DESC ";
            }
            }
                    
                   $sql = "
                        SELECT
                            COUNT(orders.active) AS act,
                            $wherem
                            FROM
                            orders
                            LEFT JOIN cities c ON orders.id_citieso = c.id
                            WHERE
                            orders.active = 1 
                            AND orders.id_ordertypes = 1 
                            AND c.name NOT LIKE '%DSO%'
                            GROUP BY
                            orders.active
                            ORDER BY
                            $order
                        ";
                    
                    $result = $this->db->query($sql);
                    return $result->result_array();  

               
        }
        
    public function detailCC($data = null)
    
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            

            $a = (int)date('m');

            $this->db->select('*');
            $this->db->where('month',$a);
            $result=$this->db->get('target');
        
          

            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
                    
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
            }
                    
            $order = " allowancess DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                        
            if($data['setting'] == 'truckritase'){
            $order = " ritase DESC ";
            }
            }
                    
                   $sql = "
                        SELECT 
                            COUNT( orders.active ) AS act,
                            fleets.fleetnumber,
                            target.carcarrier as tartget,
                            $wherem
                                FROM
                                    orders
                                        LEFT JOIN fleets ON orders.id_fleets = fleets.id
                                        LEFT JOIN cities c ON orders.id_citieso = c.id,
                                        target 
                                            WHERE
                                                orders.active = 1 
                                                AND orders.id_ordertypes = 1 
                                                AND c.name NOT LIKE '%DSO%' 
                                                AND target.month = 9
                                                AND target.year = '$tahun'
                                                    GROUP BY
                                                        fleets.fleetnumber,
                                                        orders.active,
                                                        target.carcarrier 
                                                            ORDER BY
                                                                $order
                        ";
                    
                    $result = $this->db->query($sql);
                    return $result->result_array();  

               
        }


    public function targetTN($data = null)
    
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            

            $a = (int)date('m');

            $this->db->select('*');
            $this->db->where('month',$a);
            $result=$this->db->get('target');
        
          

            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
                    
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
            }
                    
            $order = " allowancess DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                        
            if($data['setting'] == 'truckritase'){
            $order = " ritase DESC ";
            }
            }
                    
                   $sql = "
                        SELECT
                            COUNT(orders.active) AS act,
                            $wherem
                            FROM
                            orders
                            LEFT JOIN cities c ON orders.id_citieso = c.id
                            WHERE
                            orders.active = 1 
                            AND orders.id_ordertypes = 2 
                            AND c.name NOT LIKE '%DSO%'
                            GROUP BY
                            orders.active
                            ORDER BY
                            $order
                        ";
                    
                    $result = $this->db->query($sql);
                    return $result->result_array();  

               
        }
    
    public function detailTN($data = null)
    
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            

            $a = (int)date('m');

            $this->db->select('*');
            $this->db->where('month',$a);
            $result=$this->db->get('target');
        
          

            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
                    
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
            }
                    
            $order = " allowancess DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                        
            if($data['setting'] == 'truckritase'){
            $order = " ritase DESC ";
            }
            }
                    
                   $sql = "
                         SELECT 
                            COUNT( orders.active ) AS act,
                            fleets.fleetnumber,
                            target.tansya as tartget,
                            $wherem
                                FROM
                                    orders
                                        LEFT JOIN fleets ON orders.id_fleets = fleets.id
                                        LEFT JOIN cities c ON orders.id_citieso = c.id,
                                        target 
                                            WHERE
                                                orders.active = 1 
                                                AND orders.id_ordertypes = 2 
                                                AND c.name NOT LIKE '%DSO%' 
                                                AND target.month = 9
                                                AND target.year = '$tahun'
                                                    GROUP BY
                                                        fleets.fleetnumber,
                                                        orders.active,
                                                        target.tansya 
                                                            ORDER BY
                                                                $order
                        ";
                    
                    $result = $this->db->query($sql);
                    return $result->result_array();  

               
        }



    public function targetTW($data = null)
    
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            

            $a = (int)date('m');

            $this->db->select('*');
            $this->db->where('month',$a);
            $result=$this->db->get('target');
        
          

            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
                    
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
            }
                    
            $order = " allowancess DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                        
            if($data['setting'] == 'truckritase'){
            $order = " ritase DESC ";
            }
            }
                    
                   $sql = "
                        SELECT
                            COUNT(orders.active) AS act,
                            $wherem
                            FROM
                            orders
                            LEFT JOIN cities c ON orders.id_citieso = c.id
                            WHERE
                            orders.active = 1 
                            AND orders.id_ordertypes = 3 
                            AND c.name NOT LIKE '%DSO%'
                            GROUP BY
                            orders.active
                            ORDER BY
                            $order
                        ";
                    
                    $result = $this->db->query($sql);
                    return $result->result_array();  

               
        }
    
    public function detailTW($data = null)
    
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            

            $a = (int)date('m');

            $this->db->select('*');
            $this->db->where('month',$a);
            $result=$this->db->get('target');
        
          

            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
                    
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
            }
                    
            $order = " allowancess DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                        
            if($data['setting'] == 'truckritase'){
            $order = " ritase DESC ";
            }
            }
                    
                   $sql = "
                         SELECT 
                            COUNT( orders.active ) AS act,
                            fleets.fleetnumber,
                            target.towing as tartget,
                            $wherem
                                FROM
                                    orders
                                        LEFT JOIN fleets ON orders.id_fleets = fleets.id
                                        LEFT JOIN cities c ON orders.id_citieso = c.id,
                                        target 
                                            WHERE
                                                orders.active = 1 
                                                AND orders.id_ordertypes = 3 
                                                AND c.name NOT LIKE '%DSO%' 
                                                AND target.month = 9
                                                AND target.year = '$tahun'
                                                    GROUP BY
                                                        fleets.fleetnumber,
                                                        orders.active,
                                                        target.towing 
                                                            ORDER BY
                                                                $order
                        ";
                    
                    $result = $this->db->query($sql);
                    return $result->result_array();  

               
        }


    public function targetWB($data = null)
    
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            

            $a = (int)date('m');

            $this->db->select('*');
            $this->db->where('month',$a);
            $result=$this->db->get('target');
        
          

            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
                    
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
            }
                    
            $order = " allowancess DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                        
            if($data['setting'] == 'truckritase'){
            $order = " ritase DESC ";
            }
            }
                    
                   $sql = "
                        SELECT
                            COUNT(orders.active) AS act,
                            $wherem
                            FROM
                            orders
                            LEFT JOIN cities c ON orders.id_citieso = c.id
                            WHERE
                            orders.active = 1 
                            AND orders.id_ordertypes = 5 
                            AND c.name NOT LIKE '%DSO%'
                            GROUP BY
                            orders.active
                            ORDER BY
                            $order
                        ";
                    
                    $result = $this->db->query($sql);
                    return $result->result_array();  

               
        }
    
    public function detailWB($data = null)
    
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            

            $a = (int)date('m');

            $this->db->select('*');
            $this->db->where('month',$a);
            $result=$this->db->get('target');
        
          

            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
                    
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
            }
                    
            $order = " allowancess DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                        
            if($data['setting'] == 'truckritase'){
            $order = " ritase DESC ";
            }
            }
                    
                   $sql = "
                         SELECT 
                            COUNT( orders.active ) AS act,
                            fleets.fleetnumber,
                            target.wingbox as tartget,
                            $wherem
                                FROM
                                    orders
                                        LEFT JOIN fleets ON orders.id_fleets = fleets.id
                                        LEFT JOIN cities c ON orders.id_citieso = c.id,
                                        target 
                                            WHERE
                                                orders.active = 1 
                                                AND orders.id_ordertypes = 5 
                                                AND c.name NOT LIKE '%DSO%' 
                                                AND target.month = 9
                                                AND target.year = '$tahun'
                                                    GROUP BY
                                                        fleets.fleetnumber,
                                                        orders.active,
                                                        target.wingbox 
                                                            ORDER BY
                                                                $order
                        ";
                    
                    $result = $this->db->query($sql);
                    return $result->result_array();  

               
        }


    public function targetPR($data = null)
    
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            

            $a = (int)date('m');

            $this->db->select('*');
            $this->db->where('month',$a);
            $result=$this->db->get('target');
        
          

            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
                    
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
            }
                    
            $order = " allowancess DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                        
            if($data['setting'] == 'truckritase'){
            $order = " ritase DESC ";
            }
            }
                    
                   $sql = "
                        SELECT
                            COUNT(orders.active) AS act,
                            $wherem
                            FROM
                            orders
                            LEFT JOIN cities c ON orders.id_citieso = c.id
                            WHERE
                            orders.active = 1 
                            AND orders.id_ordertypes = 4 
                            AND c.name NOT LIKE '%DSO%'
                            GROUP BY
                            orders.active
                            ORDER BY
                            $order
                        ";
                    
                    $result = $this->db->query($sql);
                    return $result->result_array();  

               
        }
    
    public function detailPR($data = null)
    
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            

            $a = (int)date('m');

            $this->db->select('*');
            $this->db->where('month',$a);
            $result=$this->db->get('target');
        
          

            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
                    
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
            }
                    
            $order = " allowancess DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                        
            if($data['setting'] == 'truckritase'){
            $order = " ritase DESC ";
            }
            }
                    
                   $sql = "
                         SELECT 
                            COUNT( orders.active ) AS act,
                            fleets.fleetnumber,
                            target.pareto as tartget,
                            $wherem
                                FROM
                                    orders
                                        LEFT JOIN fleets ON orders.id_fleets = fleets.id
                                        LEFT JOIN cities c ON orders.id_citieso = c.id,
                                        target 
                                            WHERE
                                                orders.active = 1 
                                                AND orders.id_ordertypes = 4 
                                                AND c.name NOT LIKE '%DSO%' 
                                                AND target.month = 9
                                                AND target.year = '$tahun'
                                                    GROUP BY
                                                        fleets.fleetnumber,
                                                        orders.active,
                                                        target.pareto 
                                                            ORDER BY
                                                                $order
                        ";
                    
                    $result = $this->db->query($sql);
                    return $result->result_array();  

               
        }


    public function targetDSOw($data = null)
    
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            

            $a = (int)date('m');

            $this->db->select('*');
            $this->db->where('month',$a);
            $result=$this->db->get('target');
        
          

            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
                    
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
            }
                    
            $order = " allowancess DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                        
            if($data['setting'] == 'truckritase'){
            $order = " ritase DESC ";
            }
            }
                    
                   $sql = "
                        SELECT
                            COUNT(orders.active) AS act,
                            $wherem
                            FROM
                            orders
                            LEFT JOIN cities c ON orders.id_citieso = c.id
                            WHERE
                            orders.active = 1 
                            AND c.name LIKE '%DSO%'
                            GROUP BY
                            orders.active
                            ORDER BY
                            $order
                        ";
                    
                    $result = $this->db->query($sql);
                    return $result->result_array();  

               
        }
    
    public function detailDSOw($data = null)
    
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            

            $a = (int)date('m');

            $this->db->select('*');
            $this->db->where('month',$a);
            $result=$this->db->get('target');
        
          

            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritases, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowancess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceaddss, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS pricess, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances+orders.allowanceadds)-orders.prices) ELSE 0 END ) AS achievess ";
                    
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['fleetplateno']) && $data['fleetplateno'] != ''){
            $where .= " AND LOWER(fleets.fleetplateno) LIKE '%".$this->db->escape_str($data['fleets.fleetplateno'])."%' ";
            }
                    
            $order = " allowancess DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                        
            if($data['setting'] == 'truckritase'){
            $order = " ritase DESC ";
            }
            }
                    
                   $sql = "
                        SELECT
                            COUNT( orders.active ) AS act,
                            fleets.fleetnumber,
                            target.dso as tartget,
                            $wherem
                                FROM
                                    orders
                                        LEFT JOIN fleets ON orders.id_fleets = fleets.id
                                        LEFT JOIN cities c ON orders.id_citieso = c.id,
                                        target  
                                            WHERE
                                                orders.active = 1  
                                                AND c.name LIKE '%DSO%'
                                                AND target.month = 9
                                                AND target.year = '$tahun'
                                                    GROUP BY
                                                        fleets.fleetnumber,
                                                        orders.active,
                                                        target.dso  
                                                            ORDER BY
                                                                $order
                        ";
                    
                    $result = $this->db->query($sql);
                    return $result->result_array();  

               
        }

    /*DRIVER*/

    public function reportdrivermonths($data = null)
        {
            $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
            $totalmonth = isset($data['totalmonth']) ? 7 : 12;
            $x = isset($data['x']) ? $data['x'] : 1;
            
            $a = (int)date('m');
            $wherem = "";
            
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.ritase ELSE 0 END ) AS ritase, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowances ELSE 0 END ) AS allowances, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.allowanceadds ELSE 0 END ) AS allowanceadds, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN (orders.allowances+orders.allowanceadds) ELSE 0 END ) AS costs, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN orders.prices ELSE 0 END ) AS prices, ";
            $wherem .= " SUM( CASE WHEN YEAR(orders.orderdate) = '$tahun' AND MONTH(orders.orderdate) = 9 THEN ((orders.allowances)-orders.prices) ELSE 0 END ) AS achieves ";
            
            $where = " AND YEAR(orders.orderdate) = $tahun ";
            if(isset($data['name']) && $data['name'] != ''){
                $where .= " AND LOWER(drivers.name) LIKE '%".$this->db->escape_str($data['drivers.name'])."%' ";
            }
            
            $order = " ritase DESC ";
            if(isset($data['setting']) && $data['setting'] != ""){
                
                if($data['setting'] == 'driverritase'){
                    $order = " ritase DESC ";
                }
                
                if($data['setting'] == 'driverritaselow'){
                    $order = " ritase DESC ";
                }
                
                if($data['setting'] == 'trucksales'){
                    $order = " prices DESC ";
                }
                
                if($data['setting'] == 'truckachieves'){
                    $order = " prices DESC ";
                }
                
                if($data['setting'] == 'truckachieveslow'){
                    $order = " prices DESC ";
                }
            }
            
            $sql = "
                SELECT
                    orders.id_drivers,
                    drivers.name,
                    $wherem
                FROM
                    orders
                    LEFT JOIN drivers ON orders.id_drivers = drivers.id
                    LEFT JOIN customers ON orders.id_customers = customers.id 
                WHERE
                    orders.active = 1
                    AND orders.id_drivers > 0
                    AND orders.ritase > 0
                    $where
                GROUP BY
                    orders.id_drivers,
                    drivers.name
                ORDER BY
                    $order
            ";
            
            $result = $this->db->query($sql);
            return $result->result();
        }

    /** CLAIM */

     public function reportclaims($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        
        $a = (int)date('m');
        $wherem = "";
        
        $wherem .= " SUM( CASE WHEN orderclaims.claimdate BETWEEN '$tahun-01-01' AND '$tahun-$a-30' THEN orderclaims.active ELSE  0 END ) AS ritase, ";
        $wherem .= " SUM( CASE WHEN orderclaims.claimdate BETWEEN '$tahun-01-01' AND '$tahun-$a-30' THEN orderclaims.cost ELSE 0 END) AS allowances ";
        
        $where = " AND YEAR(orderclaims.claimdate) = $tahun ";
        if(isset($data['name']) && $data['name'] != ''){
            $where .= " AND LOWER(drivers.name) LIKE '%".$this->db->escape_str($data['drivers.name'])."%' ";
        }
        
        $order = " ritase DESC ";
        if(isset($data['setting']) && $data['setting'] != ""){
            
            if($data['setting'] == 'claim'){
                $order = " ritase DESC ";
            }
        }
        
        $sql = "
            SELECT 
                orderclaims.customer,
                orderclaims.driver,
                $wherem
            FROM
                orderclaims
            WHERE
                orderclaims.active = 1
            GROUP BY
                    orderclaims.customer,
                    orderclaims.driver
            ORDER BY
                $order
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }

    

    
    function reportclaimmonths($data = null)
    {
        $tahun = isset($data['tahun']) ? $data['tahun'] : date('Y');
        $totalmonth = isset($data['totalmonth']) ? 7 : 12;
        $x = isset($data['x']) ? $data['x'] : 1;
        
        $wherem = "";
        $a = isset($data['bulan']) && $data['bulan'] > 0 ? $data['bulan'] : date('m');
        $wherem .= " SUM( CASE WHEN YEAR(oc.claimdate) = '$tahun' AND MONTH(oc.claimdate) = '$a' THEN 1 ELSE 0 END ) AS totals, ";
        $wherem .= " SUM( CASE WHEN YEAR(oc.claimdate) = '$tahun' AND MONTH(oc.claimdate) = '$a' THEN COALESCE(oc.prices,0) ELSE 0 END ) AS prices, ";
        
        
        $where = " AND YEAR(oc.claimdate) = $tahun";
        if(isset($data['driver_fullname']) && $data['driver_fullname'] != ''){
            $where .= " AND LOWER(d.name) LIKE '%".$this->db->escape_str($data['driver_fullname'])."%' ";
        }
        
        $order = " totals DESC ";
        if(isset($data['setting']) && $data['setting'] != ""){
            if($data['setting'] == 'ritase'){
                $order = " totals DESC ";
            }
        }
        
        $sql = "
            SELECT
            	d.code,
            	d.name,
                $wherem
            	COUNT(CASE WHEN YEAR(oc.claimdate) = $tahun THEN 1 ELSE NULL END ) AS grandtotals,
                SUM(CASE WHEN YEAR(oc.claimdate) = $tahun THEN COALESCE(oc.prices,0) ELSE NULL END ) AS grandprices
            FROM
            	orderclaims oc
            	LEFT JOIN orderassigns oa ON oc.id_orderassigns = orders.id
            	LEFT JOIN orders o ON oc.id_orders = o.id
            	LEFT JOIN drivers d ON orders.id_drivers = d.id
            WHERE
            	oc.active = 1
            GROUP BY
            	d.code,
            	d.name
            ORDER BY
            	$order
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }
    
    
}
    