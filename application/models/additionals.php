<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class additionals extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getAdditional(){

        $this->db->select('a.*, c.name as akun');
        $this->db->join('akun c','a.akun_id = c.id');
        $this->db->where('a.active', '1');
        $this->db->where('a.type', '1');
        $result = $this->db->get('costcomponents a');

        return $result->result_array();
        
    } 

    public function getRouteAdd(){

        $this->db->select('*');
        $this->db->where('active', '1');
        $this->db->where('type', '2');
        $result = $this->db->get('costcomponents');

        return $result->result_array();
        
    }  

    function selectOne($data = null)
    {
        if($data['id'] > 0){
            $this->db->where('id', $data['id']);
            $result = $this->db->get('customers');
            return $result->row();
        }
    }

    public function editDriver()
    {
        $id               = $this->input->post('id');
        $data['name']       = $this->input->post('name');
        $data['phone']    = $this->input->post('phone');
        $data['idlicense']      = $this->input->post('idlicense');
        $data['active']     = 1;
        
        $this->db->where('id', $id);
        $this->db->update('drivers', $data); 
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("code", "desc");
        $result = $this->db->get('drivers',1);
        return $result->result_array();
    }

    public function Simpan()
    {
        $data['name']       = $this->input->post('name');
        $data['active']     = 1;
        $data['type']     = 1;
                
        $this->db->insert('costcomponents',$data);
    }

    public function SimpanRoute()
    {
        $data['name']       = $this->input->post('name');
        $data['nominal']    = $this->input->post('nominals');
        $data['nominal2']   = $this->input->post('nominals2');
        $data['active']     = 1;
        $data['type']       = 2;
                
        $this->db->insert('costcomponents',$data);
    }

    public function getOneOrder()
    {
        $data=$_POST['id'];

        $this->db->select('*');
        $this->db->where('id',$data);
        $result=$this->db->get('drivers');

        return $result->result();
    }
    
    public function Delete($value='')
    {
        $id=$_GET['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('costcomponents', $data); 
    }
}