<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class billings extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getBilling(){

        $this->db->select('a.*,d.name as cname, b.name as corigin, c.name as cdesti, e.name as ortypes, f.name as orsize, g.name as csarea ');
        $this->db->join('cities b','a.id_citieso = b.id', 'left');
        $this->db->join('cities c','a.id_citiesd = c.id', 'left');
        $this->db->join('customers d','a.id_customers = d.id', 'left');
        $this->db->join('ordertypes e','a.id_ordertypes = e.id', 'left');
        $this->db->join('ordersizeunits f','a.id_ordersizeunits = f.id', 'left');
        $this->db->join('customerareas g','a.id_customerareas = g.id', 'left');
        $this->db->order_by('a.id','asc');
        $this->db->where('a.active', '1');
        $result = $this->db->get('billings a');

        return $result->result_array();        
    }   

    public function getOneBilling()
    {
        $data=$_POST['id'];

        $this->db->select('*');
        $this->db->where('id',$data);
        $result=$this->db->get('billings');

        return $result->result();
    }

    public function editCustomer()
    {
        $id                 = $this->input->post('code');
        $data['name']       = $this->input->post('name');
        $data['address']    = $this->input->post('address');
        $data['phone']      = $this->input->post('phone');
        $data['pic_name']   = $this->input->post('pic_name');
        $data['email']      = $this->input->post('email');
        $data['nickname']   = $this->input->post('nickname');
        $data['term']       = $this->input->post('term');
        $data['ppn']        = $this->input->post('ppn');
        $data['active']     = 1;
        
        $this->db->where('code', $id);
        $this->db->update('customers', $data); 
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('customers',1);
        return $result->result_array();
    }

    public function Simpan()
    {
        $data['code']       = $this->input->post('code');
        $data['name']       = $this->input->post('name');
        $data['address']    = $this->input->post('address');
        $data['phone']      = $this->input->post('phone');
        $data['pic_name']   = $this->input->post('pic_name');
        $data['email']      = $this->input->post('email');
        $data['nickname']   = $this->input->post('nickname');
        $data['term']       = $this->input->post('term');
        $data['ppn']        = $this->input->post('ppn');
        $data['active']     = 1;
                
        $this->db->insert('customers',$data);
    }
    
    public function Delete($value='')
    {
        $id=$_GET['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('customers', $data); 
    }
}