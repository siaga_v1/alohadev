<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class cities extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getCities(){

        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('cities');

        return $result->result_array();
        
    }   

   public function getOneOrder()
    {
        $data=$_POST['id'];

        $this->db->select('*');
        $this->db->where('id',$data);
        $result=$this->db->get('cities');

        return $result->result();
    }

    public function editCities()
    {
       
        date_default_timezone_set("Asia/Bangkok");
        $tanggal = date("d-m-y H:i:s");
        $id     = $this->input->post('id');
        $data['name']           = $this->input->post('cities');
        $data['updateby']       = $this->session->userdata('id');;
        $data['updatedatetime'] = $tanggal;
        $data['active']         = 1;
                
        
        $this->db->where('id', $id);
        $this->db->update('cities',$data);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('cities',1);
        return $result->result_array();
    }

    public function Simpan()
    {

        date_default_timezone_set("Asia/Bangkok");
        $tanggal = date("d-m-y H:i:s");
        $data['id']       = $this->input->post('id');
        $data['name']       = $this->input->post('cities');
        $data['createby']       = $this->session->userdata('id');;
        $data['createdatetime']       = $tanggal;
        $data['active']     = 1;
                
        $this->db->insert('cities',$data);
    }
    
    public function Delete($value='')
    {
        $id=$_GET['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('cities', $data); 
    }
}