<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class costs extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getCostComponent(){

        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('costcomponents');

        return $result->result_array();
        
    }   

    public function getComponentOffice(){

        $this->db->select('*');
        $this->db->where('active', '1');
        $this->db->where('nominal2', '4');
        $result = $this->db->get('costcomponents');

        return $result->result_array();
        
    }

    public function getAdditionalCost(){

        $this->db->select('a.*, b.name as nama');
        $this->db->join('costcomponents b','a.cost_id = b.id','left');
        $this->db->where('a.akun_id', '14');
        $this->db->where('a.nominal !=', '0');
        $result = $this->db->get('cost a');

        return $result->result_array();
        
    }   

    public function getCostOffice(){

        $this->db->select('a.*, b.name as nama,c.name as supir');
        $this->db->join('costcomponents b','a.cost_id = b.id','left');
        $this->db->join('employees c','a.employee_id = c.id','left');
        $this->db->where('a.active','1');
        $this->db->like('a.order_id', 'OPRS','both');
        $result = $this->db->get('cost a');

        return $result->result_array();
        
    } 

    public function getOneDebit()
    {
        $data=$_POST['id'];

        $this->db->select('a.*, b.firstname as nama, b.id as idc');
        $this->db->join('users b','a.id_employees = b.id','left');
        $this->db->where('a.active', '1');
        $this->db->where('a.id',$data);
        $result = $this->db->get('debitcredit a');

        return $result->result();
    } 

    public function getOneCostOffice()
    {
        $data=$_POST['id'];

        $this->db->select('*');
        $this->db->where('active', '1');
        $this->db->where('order_id',$data);
        $result = $this->db->get('cost');

        return $result->result_array();
    } 

    public function getOneRequest()
    {
        $data=$_POST['id'];
        $this->db->select('*');
        $this->db->where('id', $data);
        $result = $this->db->get('requestpayable');
        return $result->result();
    }

    public function getKoordinator()
    {
        $this->db->select('*');
        $this->db->where('id_userroles', '3');
        $result = $this->db->get('users');
        return $result->result_array();
    }
    
    public function getCredit(){
        $akun = $this->session->userdata('id');
        $this->db->select('a.*, b.firstname as nama');
        $this->db->join('users b','a.createby = b.id','left');
        $this->db->where('a.active', '1');
        $this->db->where('a.createby', $akun);
        $result = $this->db->get('orders a');

        return $result->result_array();
        
    } 

    function selectOne($data = null)
    {
        if($data['id'] > 0){
            $this->db->where('id', $data['id']);
            $result = $this->db->get('debitcredit');
            return $result->row();
        }
    }

    public function editDebit()
    {   $nominal = $this->input->post('nomnals');
        $ntk = str_replace(",", "", $nominal);
        $id          = $this->input->post('code');
        $data['id_employees']   = $this->input->post('nama');
        $data['dates']          = $this->input->post('date');
        $data['desc_1']         = $this->input->post('necesary');
        $data['desc_2']         = $this->input->post('descripton');
        $data['price']          = $ntk;
        $data['active']         = 1;
        
        $this->db->where('code', $id);
        $this->db->update('debitcredit',$data);
    }

    public function editRequest()
    {
        $id          = $this->input->post('id');
        $data['necessary']         = $this->input->post('necessary');
        $data['desciption']         = $this->input->post('desc');
        $data['price']          = $this->input->post('nominals');
        
        $this->db->where('id', $id);
        $this->db->update('requestpayable',$data);
    }

    public function approvedebit()
    {
        $id          = $this->input->post('code');
        $nama        = $this->input->post('nama');

        //
        $this->db->select('*');
        $this->db->where('firstname', $nama);
        $result = $this->db->get('users');
        foreach ($result->result() as $key) {
            $awal   = $key->nominal;
        }

        
        $tambah = $this->input->post('nominal');

        $add['nominal']        = $awal + $tambah;
        $data['status']        = "VALID";
        $data['trfdate']       = $this->input->post('trfdate');
        
        $this->db->where('code', $id);
        $this->db->update('debitcredit',$data);

        $this->db->where('firstname', $nama);
        $this->db->update('users',$add);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('cost',1);
        return $result->result_array();
    }

    public function getNecessary()
    {   
        $this->db->select('*');
        $result = $this->db->get('requestpayable');
        return $result->result_array();
    }

   
    public function saveCosts()
    {
        date_default_timezone_set("Asia/Bangkok");
        $nominal = $this->input->post('nominals');
        $ntk = str_replace(",", "", $nominal);
        $data['code']           = $this->input->post('code');
        $data['cost_id']        = $this->input->post('name');
        $data['date']           = $this->input->post('ecdate');
        $data['order_id']       = $this->input->post('order');
        $data['description']    = $this->input->post('description');
        $data['nominal']        = $ntk;
        $data['nominal2']       = $this->input->post('nominals2');
        $data['active']         = 1;
        $data['createby']       = $this->session->userdata('id');
        $data['createdate']     = date("Y-m-d h:i:s");
                
        $this->db->insert('cost',$data);
    }

    public function saveCostOffice()
    {
        date_default_timezone_set("Asia/Bangkok");
        $nominal = $this->input->post('nominal');
        $data['order_id']           = $this->input->post('code');
        $data['cost_id']        = $this->input->post('cost_id');
        $data['date']           = $this->input->post('date');
        $data['akun_id']       = $this->input->post('akun_id');
        $data['description']    = $this->input->post('keterangan');
        $data['nominal']        = str_replace(",", "", $nominal) ?: 0;
        $data['nominal2']       = $this->input->post('kategori');
        $data['employee_id']       = $this->input->post('karyawan');
        $data['active']         = 1;
        $data['createby']       = $this->session->userdata('id');
        $data['createdate']     = date("Y-m-d h:i:s");
                
        $this->db->insert('cost',$data);
    }

    public function saveEditCostOffice()
    {
        date_default_timezone_set("Asia/Bangkok");
        $nominal = $this->input->post('modalnominal');
        $id = $this->input->post('modalcode');
        $data['cost_id']        = $this->input->post('modalcost_id');
        $data['date']           = $this->input->post('modaldate');
        $data['akun_id']       = $this->input->post('modalakun_id');
        $data['description']    = $this->input->post('modalketerangan');
        $data['nominal']        = str_replace(",", "", $nominal) ?: 0;
        $data['nominal2']       = $this->input->post('modalkategori');
        $data['employee_id']       = $this->input->post('modalkaryawan');
        $data['active']         = 1;
        $data['createby']       = $this->session->userdata('id');
        $data['createdate']     = date("Y-m-d h:i:s");

        $this->db->where('order_id', $id);        
        $this->db->update('cost',$data);
    }
    
    public function Delete($value='')
    {
        $id=$_POST['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('customers', $data); 
    }
}