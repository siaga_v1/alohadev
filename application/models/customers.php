<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class customers extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getCustomer(){

        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('customers');

        return $result->result_array();
        
    }   

    function selectOne($data = null)
    {
        if($data['id'] > 0){
            $this->db->where('id', $data['id']);
            $result = $this->db->get('customers');
            return $result->row();
        }
    }

    public function editCustomer()
    {
        $id                 = $this->input->post('code');
        $data['name']       = $this->input->post('name');
        $data['address_1']    = $this->input->post('address_1');
        $data['address_2']    = $this->input->post('address_2');
        $data['address_3']    = $this->input->post('address_3');
        $data['phone']      = $this->input->post('phone');
        $data['pic_name']   = $this->input->post('pic_name');
        $data['email']      = $this->input->post('email');
        $data['nickname']   = $this->input->post('nickname');
        $data['term']       = $this->input->post('term');
        $data['ppn']        = $this->input->post('ppn');
        $data['active']     = 1;
        
        $this->db->where('code', $id);
        $this->db->update('customers', $data); 
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('customers',1);
        return $result->result_array();
    }

    public function Simpan()
    {
        $data['id']       = $this->input->post('id');
        $data['code']       = $this->input->post('code');
        $data['name']       = $this->input->post('name');
        $data['address_1']    = $this->input->post('address_1');
        $data['address_2']    = $this->input->post('address_2');
        $data['address_3']    = $this->input->post('address_3');
        $data['phone']      = $this->input->post('phone');
        $data['pic_name']   = $this->input->post('pic_name');
        $data['email']      = $this->input->post('email');
        $data['nickname']   = $this->input->post('nickname');
        $data['term']       = $this->input->post('term');
        $data['ppn']        = $this->input->post('ppn');
        $data['active']     = 1;
                
        $this->db->insert('customers',$data);
    }
    
    public function Delete($value='')
    {
        $id=$_GET['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('customers', $data); 
    }
}