<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class debitcredits extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getDebitcredit(){

        $this->db->select('a.*, b.firstname as nama, c.necessary as nes');
        $this->db->join('users b','a.id_employees = b.id','left');
        $this->db->join('requestpayable c','a.desc_1 = c.id','left');
        $this->db->where('a.active', '1');
        $result = $this->db->get('debitcredit a');

        return $result->result_array();
        
    }   

    public function getRefund(){

        $this->db->select('a.*, b.firstname as nama');
        $this->db->join('users b','a.id_user = b.id','left');
        $result = $this->db->get('refund a');

        return $result->result_array();
        
    }  

    public function getOneDebit()
    {
        $data=$_POST['id'];

        $this->db->select('a.*, b.firstname as nama, b.id as idc');
        $this->db->join('users b','a.id_employees = b.id','left');
        $this->db->where('a.active', '1');
        $this->db->where('a.id',$data);
        $result = $this->db->get('debitcredit a');

        return $result->result();
    } 

    public function getOneRequest()
    {
        $data=$_POST['id'];
        $this->db->select('*');
        $this->db->where('id', $data);
        $result = $this->db->get('requestpayable');
        return $result->result();
    }

    public function getKoordinator()
    {
        $this->db->select('*');
        $this->db->where('id_userroles', '3');
        $this->db->where('active', '1');
        $result = $this->db->get('users');
        return $result->result_array();
    }
	
	public function getCredit(){
		$akun = $this->session->userdata('id');
        $this->db->select('a.*, b.firstname as nama');
        $this->db->join('users b','a.createby = b.id','left');
        $this->db->where('a.active', '1');
        $this->db->where('a.createby', $akun);
        $result = $this->db->get('orders a');

        return $result->result_array();
        
    } 

    function selectOne($data = null)
    {
        if($data['id'] > 0){
            $this->db->where('id', $data['id']);
            $result = $this->db->get('debitcredit');
            return $result->row();
        }
    }

    public function editDebit()
    {   $nominal = $this->input->post('nomnals');
        $ntk = str_replace(",", "", $nominal);
        $id          = $this->input->post('code');
        $data['id_employees']   = $this->input->post('nama');
        $data['dates']          = $this->input->post('date');
        $data['desc_1']         = $this->input->post('necesary');
        $data['desc_2']         = $this->input->post('descripton');
        $data['price']          = $ntk;
        $data['active']         = 1;
        
        $this->db->where('code', $id);
        $this->db->update('debitcredit',$data);
    }

    public function editRequest()
    {
        $id          = $this->input->post('id');
        $data['necessary']         = $this->input->post('necessary');
        $data['desciption']         = $this->input->post('desc');
        $data['price']          = $this->input->post('nominals');
        
        $this->db->where('id', $id);
        $this->db->update('requestpayable',$data);
    }

    public function saveRequest()
    {
        $data['necessary']         = $this->input->post('necessary');
        $data['desciption']         = $this->input->post('description');
        $data['price']          = $this->input->post('nominals');
        $data['active']          = 1;
        
        $this->db->insert('requestpayable',$data);
    }

    public function approvedebit()
    {
        $id          = $this->input->post('code');
        $nama        = $this->input->post('nama');

        //
        $this->db->select('*');
        $this->db->where('firstname', $nama);
        $result = $this->db->get('users');
        foreach ($result->result() as $key) {
            $awal   = $key->nominal;
        }

        
        $tambah = $this->input->post('nominal');

        $add['nominal']        = $awal + $tambah;
        $data['status']        = "VALID";
        $data['trfdate']       = $this->input->post('trfdate');
        
        $this->db->where('code', $id);
        $this->db->update('debitcredit',$data);

        $this->db->where('firstname', $nama);
        $this->db->update('users',$add);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('debitcredit',1);
        return $result->result_array();
    }

    public function getNecessary()
    {   
        $this->db->select('*');
        $this->db->where('active', 1);
        $result = $this->db->get('requestpayable');
        return $result->result_array();
    }

   
    public function Simpan()
    {
        date_default_timezone_set("Asia/Bangkok");
        $nominal = $this->input->post('nominals');
        $ntk = str_replace(",", "", $nominal);
        $data['code']           = $this->input->post('code');
        $data['id_employees']   = $this->input->post('name');
        $data['dates']          = $this->input->post('ecdate');
        $data['desc_1']         = $this->input->post('necessary');
        $data['desc_2']         = $this->input->post('description');
        $data['price']          = $ntk;
        $data['status']          = "PENDING";
        $data['active']         = 1;
        $data['createby']         = $this->session->userdata('id');
        $data['createtime']         = date("Y-m-d h:i:s");
                
        $this->db->insert('debitcredit',$data);
    }

     public function saveRefund()
    {
        date_default_timezone_set("Asia/Bangkok");
        $data['code']           = $this->input->post('code');
        $data['id_user']   = $this->input->post('id_user');
        $data['date']          = $this->input->post('rfdate');
        $data['nominal']         = $this->input->post('nominals');
                
        $this->db->insert('refund',$data);

        $ids   = $this->input->post('id_user');

        $this->db->select('*');
        $this->db->where('id', $ids);
        $result = $this->db->get('users');
        foreach ($result->result() as $key) {
            $awal   = $key->nominal;
        }

        
        $kurang = $this->input->post('nominals');

        $add['nominal']        = $awal - $kurang;

        $this->db->where('id', $ids);
        $this->db->update('users',$add);
    }
    
    public function Delete($value='')
    {
        $id=$_GET['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('customers', $data); 
    }

     public function deleteRequest($value='')
    {
        $id=$_GET['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('requestpayable', $data); 
    }

    public function getDetailDownpayment()
    {
        $id=$this->input->post('id');
        $sql="SELECT A.* FROM (SELECT
        ROW_NUMBER () OVER ( ORDER BY dr.dates ) AS RowNums,
        a.id,
        a.name,
        dr.type,
        dr.id as idd,
        dr.nominal,
        dr.dates,
        dr.description
        FROM
        drivers a
        LEFT JOIN downpaymentouts dr ON a.id = dr.id_drivers
        WHERE
        a.active = 1) A WHERE 
        A.id IS NOT NULL
        AND A.id = '$id'";

        $result=$this->db->query($sql);
        return $result->result();
    }
}