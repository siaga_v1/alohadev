<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class downpayments extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getDownpayment(){

        $query = "SELECT
    A.* 
FROM
    (
    SELECT
        ROW_NUMBER () OVER ( ORDER BY a.id ) AS RowNums,
        a.id,
        a.name,
        tkasbon = ( SELECT SUM ( dr.nominal ) FROM downpaymentouts dr WHERE a.id = dr.id_drivers AND dr.type = 1 ),
        tbayar = ( SELECT SUM ( dr.nominal ) FROM downpaymentouts dr WHERE a.id = dr.id_drivers AND dr.type = 2 )
        
    FROM
        drivers a 
    WHERE
        a.active = 1 
    ) A 
WHERE
    A.id IS NOT NULL";

        $result = $this->db->query($query);

        return $result->result_array();
        
    }  

    public function getDownpayment__(){

        $query = "SELECT
        dbo.drivers.name AS nama1,
        dbo.employees.name AS nama2,
        dbo.downpayments.*

        FROM
        dbo.downpayments
        LEFT JOIN dbo.drivers ON dbo.drivers.id = dbo.downpayments.id_drivers AND dbo.drivers.active = 1
        LEFT JOIN dbo.employees ON dbo.employees.id = dbo.downpayments.id_drivers AND dbo.employees.active = 1
        WHERE dbo.downpayments.active=1";

        $result = $this->db->query($query);

        return $result->result_array();
        
    }  

    public function getCostOpr(){

        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('debitcredit');

        return $result->result_array();
    } 

    public function getKaryawan(){

        $query = "SELECT id, name, active FROM drivers WHERE active = 1 UNION ALL SELECT id,name,active FROM employees WHERE active =1";

        $result = $this->db->query($query);

        return $result->result_array();
    } 

    public function simpanDownpayment()
    {
        date_default_timezone_set("Asia/Bangkok");
        $nominal = $this->input->post('nominals');
        $ntk = str_replace(",", "", $nominal);

        $data['id_drivers']     = $this->input->post('id_drivers');
        $data['dates']          = $this->input->post('date');
        $data['description']    = $this->input->post('description');
        $data['nominal']         = $ntk;
        $data['type']         = 1;
        $data['create_by']         = $this->session->userdata('id');
        $data['create_datetime']         = date("Y-m-d h:i:s");
                
        $this->db->insert('downpaymentouts',$data);
    }

    public function bayarDownpayment()
    {
        date_default_timezone_set("Asia/Bangkok");
        $nominal = $this->input->post('nominals');
        $ntk = str_replace(",", "", $nominal);

        $data['id_drivers']     = $this->input->post('id_drivers');
        $data['dates']          = $this->input->post('date');
        $data['description']    = $this->input->post('description');
        $data['nominal']        = $ntk;
        $data['type']           = 2;
        $data['create_by']         = $this->session->userdata('id');
        $data['create_datetime']         = date("Y-m-d h:i:s");
                
        $this->db->insert('downpaymentouts',$data);
    }

    public function getOneDownpayment()
    {
        $data=$_POST['id'];

 
        $this->db->select('*');
        $this->db->where('active', '1');
        $this->db->where('id',$data);        
        $result = $this->db->get('downpayments');

        return $result->result();
    } 

    public function getDetailDownpayment()
    {
        $data=$_POST['id'];

 
        $this->db->select('*');
        $this->db->where('ref_dp',$data);        
        $result = $this->db->get('downpayment_detail');

        return $result->result();
    } 

    public function getOneRequest()
    {
        $data=$_POST['id'];
        $this->db->select('*');
        $this->db->where('id', $data);
        $result = $this->db->get('requestpayable');
        return $result->result();
    }

    public function getKeterangan()
    {
        $this->db->select('*');
        $this->db->where('nominal2', 2);
        $result = $this->db->get('costcomponents');
        return $result->result_array();
    }

    public function getKoordinator()
    {
        $this->db->select('*');
        $this->db->where('id_userroles', '3');
        $result = $this->db->get('users');
        return $result->result_array();
    }
	
	public function getCredit(){
		$akun = $this->session->userdata('id');
        $this->db->select('a.*, b.firstname as nama');
        $this->db->join('users b','a.createby = b.id','left');
        $this->db->where('a.active', '1');
        $this->db->where('a.createby', $akun);
        $result = $this->db->get('orders a');

        return $result->result_array();
        
    } 

    function selectOne($data = null)
    {
        if($data['id'] > 0){
            $this->db->where('id', $data['id']);
            $result = $this->db->get('debitcredit');
            return $result->row();
        }
    }

    public function editDownpayment()
    {   
        $nominal = $this->input->post('nominal');
        $ntk = str_replace(",", "", $nominal);
        $code          = $this->input->post('code');
        $data['id_drivers']     = $this->input->post('name');
        $data['dates']          = $this->input->post('date');
        $data['description']    = $this->input->post('desc_1');
        $data['prices']          = $ntk;
        $data['active']         = 1;
        
        $this->db->where('code', $code);
        $this->db->update('downpayments',$data);

        date_default_timezone_set("Asia/Bangkok");
        $tanggal = date("Y-m-d H:i:s");
        
        $date = $this->input->post('paydate');
        $nominals = $this->input->post('jumlahbayar');
        $ref_id = $this->input->post('ref_id');

        if (isset($nominals)) {
            $addcostcounts = count($nominals);

            if ($addcostcounts>=0) 
            {
                for($i=0; $i < $addcostcounts; $i++) 
                {
                    $id = $this->input->post('id['.$i.']');
                       
                    $ad = array(
                        'ref_dp' =>$ref_id,
                        'date' =>$date[$i],
                        'nominal' => $nominals[$i]
                    );
                    $a = array(
                        'order_id' =>$this->input->post('code'),
                        'coa' => 32,
                        'cost_type' => 1,
                        'date'    => $date[$i],
                        'nominal' => $nominals[$i], 
                        'active' => 1,
                        'createby' => $this->session->userdata('id')
                    );
                    if (isset($id)) {
                        $this->db->where('id', $id);
                        $this->db->update('downpayment_detail', $ad);
                        $this->db->update('cost', $a);
                    }else{

                        $this->db->insert('downpayment_detail', $ad);
                        $this->db->insert('cost', $a);

                    }
                }  
            }
        }
    }

    public function editRequest()
    {
        $id          = $this->input->post('id');
        $data['necessary']         = $this->input->post('necessary');
        $data['desciption']         = $this->input->post('desc');
        $data['prices']          = $this->input->post('nominals');
        
        $this->db->where('id', $id);
        $this->db->update('requestpayable',$data);
    }

    public function approvedebit()
    {
        $id          = $this->input->post('code');
        $nama        = $this->input->post('nama');

        //
        $this->db->select('*');
        $this->db->where('firstname', $nama);
        $result = $this->db->get('users');
        foreach ($result->result() as $key) {
            $awal   = $key->nominal;
        }

        
        $tambah = $this->input->post('nominal');

        $add['nominal']        = $awal + $tambah;
        $data['status']        = "VALID";
        $data['trfdate']       = $this->input->post('trfdate');
        
        $this->db->where('code', $id);
        $this->db->update('debitcredit',$data);

        $this->db->where('firstname', $nama);
        $this->db->update('users',$add);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('debitcredit',1);
        return $result->result_array();
    }

    public function getNecessary()
    {   
        $this->db->select('*');
        $result = $this->db->get('requestpayable');
        return $result->result_array();
    }

   
    
    
    public function Delete($value='')
    {
        $id=$_POST['id'];
        
        $data['active']     = 0;

        $this->db->where('id', $id);
        $this->db->update('downpayments', $data); 

        $this->db->select('*');
        $this->db->where('id', $id);
        $result = $this->db->get('downpayments');


        foreach ($result->result_array() as $row){
            $a    = $row['code'];

            $dat['active']     = 0;

            $this->db->where('order_id', $a);
            $this->db->update('cost', $dat);

        }
    }
}