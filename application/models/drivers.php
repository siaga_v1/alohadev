<?php if (!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class drivers extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getDrivers()
    {

        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('drivers');

        return $result->result_array();
    }

    public function loadListAbsensi($data = null)
    {
        $where = "";
        if ($this->input->post('drivers') != '') {
            $text = $this->input->post('drivers');
            $where .= " AND dbo.absensi.id_drivers = '" . $text . "'";
        }
        if ($this->input->get('drivers') != '') {
            $textq = $this->input->get('drivers');
            $where .= " AND dbo.absensi.id_drivers = '" . $textq . "'";
        }
        if ($this->input->post('date') > 0 && $this->input->post('date') > 0) {
            $where .= " AND dbo.absensi.tanggal_absensi = '" . $this->input->post('date') . "'";
        }

        if ($this->input->get('date') > 0 && $this->input->get('date') > 0) {
            $where .= " AND dbo.absensi.tanggal_absensi = '" . $this->input->get('date') . "'";
        }

        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }

        $sql = "SELECT
                * 
                FROM
                    (
                    SELECT
                        ROW_NUMBER ( ) OVER ( ORDER BY A.id DESC ) AS RowNum,
                        A.* 
                    FROM
                        (
                            SELECT
                            dbo.drivers.id as id_driver,
                            dbo.drivers.name,
                            dbo.absensi.tipe_absensi as type,
                            dbo.absensi.id,
                            dbo.absensi.waktu_absensi
                        FROM
                            dbo.drivers
                            INNER JOIN dbo.absensi ON dbo.absensi.id_drivers = dbo.drivers.id 
                        WHERE
                            dbo.drivers.active = 1
                        $where
                        ) A 
                    ) B 
                WHERE
                    B.id IS NOT NULL
                $paging
            ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function loadAbsensi($data = null)
    {
        $where = "";
        if ($this->input->post('date') > 0 && $this->input->post('date') > 0) {
            $where .= " where tanggal_absensi = '" . $this->input->post('date') . "'";
        }

        if ($this->input->get('date') > 0 && $this->input->get('date') > 0) {
            $where .= " where tanggal_absensi = '" . $this->input->get('date') . "'";
        }

        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }

        $sql = "SELECT
                * 
                FROM
                    (
                    SELECT
                        ROW_NUMBER ( ) OVER ( ORDER BY A.id DESC ) AS RowNum,
                        A.* 
                    FROM
                        (

                            SELECT dbo.drivers.* 
                            FROM dbo.drivers 
                            WHERE dbo.drivers.active = 1 
                            and  dbo.drivers.id NOT IN (SELECT id_drivers FROM absensi $where)
                        ) A 
                    ) B 
                WHERE
                    B.id IS NOT NULL
                $paging
            ";

        $result = $this->db->query($sql);
        return $result->result();
    }


    public function getDriversAbsensi()
    {
        date_default_timezone_set("Asia/Bangkok");
        $tgl = date('Y-m-d');

        $sql = "SELECT dbo.drivers.* FROM dbo.drivers WHERE dbo.drivers.active = 1 and  dbo.drivers.id NOT IN (SELECT id_drivers FROM absensi WHERE tanggal_absensi = '$tgl')";
        $result = $this->db->query($sql);

        return $result->result_array();
    }

    public function getKasbon()
    {
        $id = $this->input->post('id');
        $sql = "SELECT
        tkasbon = ( SELECT SUM ( dr.nominal ) FROM downpaymentouts dr WHERE a.id = dr.id_drivers AND dr.type = 1 ),
        tbayar = ( SELECT SUM ( dr.nominal ) FROM downpaymentouts dr WHERE a.id = dr.id_drivers AND dr.type = 2 )
        
    FROM
        drivers a 
    WHERE
        a.active = 1
        AND a.id = '$id'";
        $result = $this->db->query($sql);

        return $result->result_array();
    }

    public function getKomisis()
    {

        $this->db->select('a.*,b.name as supir,b.id as iddrv');
        $this->db->join('drivers b', 'a.driver_id = b.id', 'left');
        $result = $this->db->get('komisi_supir a');

        return $result->result_array();
    }

    function selectOne($data = null)
    {
        if ($data['id'] > 0) {
            $this->db->where('id', $data['id']);
            $result = $this->db->get('customers');
            return $result->row();
        }
    }

    public function editDriver()
    {
        $id               = $this->input->post('id');
        $data['name']       = $this->input->post('name');
        $data['phone']    = $this->input->post('phone');
        $data['idlicense']      = $this->input->post('idlicense');
        $data['active']     = 1;

        $this->db->where('id', $id);
        $this->db->update('drivers', $data);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("code", "desc");
        $result = $this->db->get('drivers', 1);
        return $result->result_array();
    }

    public function Simpan()
    {
        $data['id']       = $this->input->post('id');
        $data['code']       = $this->input->post('code');
        $data['name']       = $this->input->post('name');
        $data['phone']    = $this->input->post('phone');
        $data['idlicense']      = $this->input->post('idlicense');
        $data['active']     = 1;

        $this->db->insert('drivers', $data);
    }

    public function getOneOrder()
    {
        $data = $_POST['id'];

        $this->db->select('*');
        $this->db->where('id', $data);
        $result = $this->db->get('drivers');

        return $result->result();
    }

    public function Delete($value = '')
    {
        $id = $_GET['id'];

        $data['active']     = 0;

        $this->db->where('id', $id);
        $this->db->update('drivers', $data);
    }

    function driversummary($data = null)
    {
        $where = "";
        $wheres = "";
        $wheree = "";
        if ($this->input->get('supir') > 0 && $this->input->get('supir') > 0) {
            $wheree .= " AND a.id = '" . $this->input->get('supir') . "'";
        }
        if ($this->input->post('start') > 0 && $this->input->post('end') > 0) {
            $where .= " AND d.date BETWEEN '" . $this->input->post('start') . "' AND '" . $this->input->post('end') . "'";
            $wheres .= " AND c.date BETWEEN '" . $this->input->post('start') . "' AND '" . $this->input->post('end') . "'";
        }

        if ($this->input->get('start') > 0 && $this->input->get('end') > 0) {
            $where .= " AND d.date BETWEEN '" . $this->input->get('start') . "' AND '" . $this->input->get('end') . "'";
            $wheres .= " AND c.date BETWEEN '" . $this->input->get('start') . "' AND '" . $this->input->get('end') . "'";
        }

        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }

        $sql = "
        SELECT
    a.name,
    a.id,
    tabungan = ( SELECT SUM ( C.tabSupir ) FROM [orders] c WHERE c.id_drivers  = a.id AND c.active=1  $wheres),
    komisi = ( SELECT SUM ( C.komSupir ) FROM [orders] c WHERE c.id_drivers  = a.id AND c.active=1 $wheres),
    hadir =( SELECT count ( d.id_drivers ) FROM [orders] d WHERE d.id_drivers = a.id AND d.active=1  $where),
    ritase =( SELECT count ( d.id_drivers ) FROM [orders] d WHERE d.id_drivers = a.id AND d.active=1 and d.allowances >0 $where)
FROM
    drivers a 
                    
                    WHERE
                        a.active = 1
                        $wheree 
                    ORDER BY
                    a.name asc
        ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }
}
