<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class employees extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getEmployees(){

        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('employees');

        return $result->result_array();
        
    }

    public function saveEmployees()
    {
        $data['id']       = $this->input->post('id');
        $data['name']       = $this->input->post('name');
        $data['birthdate']   = $this->input->post('birthdate');
        $data['birthplace']      = $this->input->post('birthplace');
        $data['address']    = $this->input->post('address');
        $data['phone']      = $this->input->post('phone');
        $data['salary']   = str_replace(",", "", $this->input->post('salary'));
        $data['feeLunch']       = str_replace(",", "", $this->input->post('feeLunch'));
        $data['active']     = 1;
                
        $this->db->insert('employees',$data);
    }

    public function saveEditEmployees()
    {
        $id      = $this->input->post('modalid');
        $data['name']       = $this->input->post('modalname');
        $data['birthdate']   = $this->input->post('modalbirthdate');
        $data['birthplace']      = $this->input->post('modalbirthplace');
        $data['address']    = $this->input->post('modaladdress');
        $data['phone']      = $this->input->post('modalphone');
        $data['salary']   = str_replace(",", "", $this->input->post('modalsalary'));
        $data['feeLunch']       = str_replace(",", "", $this->input->post('modalfeeLunch'));
        $data['active']     = 1;
        
        $this->db->where('id', $id);
        $this->db->update('employees', $data); 
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('employees',1);
        return $result->result_array();
    }

    public function getOneEmployees()
    {
        $data=$_POST['id'];

        $this->db->select('*');
        $this->db->where('id', $data);
        $result = $this->db->get('employees');
        return $result->result_array();
    }

    
    
    public function Delete()
    {
        $id=$_POST['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('employees', $data); 
    }
}