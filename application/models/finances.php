<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class finances extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    function getCoaI($parent)
    {


        $this->db->select('a.*');
        $this->db->where('a.active', 1);
        $this->db->where('a.parent', $parent);
        $this->db->order_by('a.sort', 'ASC');
        $result = $this->db->get('chartofaccount a');
        return $result->result();
    }

    function getChild($child,$parent)
    {
        $this->db->select('*');
        $this->db->where('active', 1);
        $this->db->where('child', $child);
        $this->db->where('parent', $parent);
        $this->db->order_by('sort', 'ASC');
        $result = $this->db->get('chartofaccount');
        return $result->result();
    }

    public function getAkun(){

        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('akun');

        return $result->result_array();
    }  

    public function testsaldo()
    {
        $filter = array();
        $data= $this->finances->driversummarya($filter);

        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;
        $KAS=0;


        foreach ($data as $key) {

            if ($key['type'] == 'ND'){
                if ($key['name']=='KAS') {
                    $KAS += $key['totalnominal'];
                }
            }

        }
        echo $KAS;
    }

    public function getAkunBudget(){

        $this->db->select('*');
        $this->db->where('active', '1');
        $this->db->where('parent', '3');
        $result = $this->db->get('chartofaccount');

        return $result->result_array();
    } 


    public function createAkun()
    {
        date_default_timezone_set("Asia/Bangkok");
        $tanggal = date("d-m-y H:i:s");
        $data['code']                     = $this->input->post('code');
        $data['name']                   = $this->input->post('name');
        $data['active']                 = 1;
                
        $this->db->insert('akun',$data);
    }

    public function getOneAkun()
    {
        $data=$_POST['id'];

        $this->db->select('*');
        $this->db->where('id',$data);
        $result=$this->db->get('akun');

        return $result->result();
    }

    public function saveAkun()
    {
       
        date_default_timezone_set("Asia/Bangkok");
        $tanggal = date("d-m-y H:i:s");
        $id                = $this->input->post('idmodal');
        $data['code']                     = $this->input->post('codemodal');
        $data['name']           = $this->input->post('namemodal');
        $data['active']         = 1;
                
        
        $this->db->where('id', $id);
        $this->db->update('akun',$data);
    }

    public function saveEditBudgets()
    {
        $this->db->where('order_id',$this->input->post('modalcode'));
        $this->db->delete('cost');
       
        $ad = array(
            'cost_id' => $this->input->post('modalcostid'), 
            'date' => $this->input->post('modaldate'),
            'nominal' => str_replace(",", "", $this->input->post('modalnominals')), 
            'order_id' => $this->input->post('modalcode'), 
            'description' => $this->input->post('modalketerangan'),
            'active'=>1,
            'coa'=>4,
            'cost_type' => 2, 
            'akun_id' => 1, 
            'createby' => $this->session->userdata('id')
        );

        $this->db->insert('cost', $ad);
    }

    public function deleteAkun()
    {
        $id=$_GET['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('akun', $data); 
    }

    function driversummarya($data = null)
    {
        $where = "";
        if($this->input->post('start') > 0 && $this->input->post('end') > 0){
            $where .= " AND c.date BETWEEN '".$this->input->post('start')."' AND '".$this->input->post('end')."'";
        }
        
        if($this->input->get('start') > 0 && $this->input->get('end') > 0){
            $where .= " AND c.date BETWEEN '".$this->input->get('start')."' AND '".$this->input->get('end')."'";
        }
        
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
                    SELECT
    a.name,
    a.id,
    a.type,
    totalnominal = ( SELECT SUM ( C.nominal ) FROM [cost] c WHERE c.coa = a.id AND c.active = 1 and c.coa = 4 $where),
    totalpengeluaran = ( SELECT SUM ( C.nominal ) FROM [cost] c WHERE c.coa = a.id AND c.active = 1 and c.cost_type = 1 $where)
FROM
    chartofaccount a 
WHERE
    a.active = 1 
ORDER BY
    a.id ASC 
        ";
        
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getcoa($parent,$datea)
    {
        $where = "AND c.date > '$datea'";
        $parenta = "AND a.parent = $parent";
        if($this->input->post('start') > 0 && $this->input->post('end') > 0){
            $where .= " AND c.date BETWEEN '".$this->input->post('start')."' AND '".$this->input->post('end')."'";
        }
        
        if($this->input->get('start') > 0 && $this->input->get('end') > 0){
            $where .= " AND c.date BETWEEN '".$this->input->get('start')."' AND '".$this->input->get('end')."'";
        }
        
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
                    SELECT a.*,
                        totalnominal = (SELECT SUM(C.nominal) FROM [cost] c WHERE c.coa = a.id AND c.active =1 $where),
                        pengurang = (SELECT SUM(C.nominal) FROM [cost] c WHERE  c.active =1  AND c.cost_type = 1 $where)
                      FROM chartofaccount a 
                    WHERE
                        a.active = 1 
                        $parenta
                    ORDER BY
                    a.sort asc
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }

    function getNeraca($parent)
    {
        $where = "";
        $parenta = "AND a.parent = $parent";
        if($this->input->post('start') > 0 && $this->input->post('end') > 0){
            $where .= " AND c.date BETWEEN '".$this->input->post('start')."' AND '".$this->input->post('end')."'";
        }
        
        if($this->input->get('start') > 0 && $this->input->get('end') > 0){
            $where .= " AND c.date BETWEEN '".$this->input->get('start')."' AND '".$this->input->get('end')."'";
        }
        
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
                    SELECT a.*,
                        totalnominal = (SELECT SUM(C.nominal) FROM [cost] c WHERE c.coa = a.id AND c.active =1  
                        $where),
                        totalhead = (SELECT SUM(C.nominal) FROM [cost] c WHERE c.coa = a.parent AND c.active =1  
                        $where)
                      FROM chartofaccount a 
                    WHERE
                        a.active = '1' 
                        AND a.[group] = '0' 
                        $parenta
                    ORDER BY
                    a.sort ASC
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }

    function getprofitloss($parent)
    {
        $where = "";
        $parenta = "AND a.parent = $parent";
        if($this->input->post('start') > 0 && $this->input->post('end') > 0){
            $where .= " AND c.date BETWEEN '".$this->input->post('start')."' AND '".$this->input->post('end')."'";
        }
        
        if($this->input->get('start') > 0 && $this->input->get('end') > 0){
            $where .= " AND c.date BETWEEN '".$this->input->get('start')."' AND '".$this->input->get('end')."'";
        }
        
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
                    SELECT a.*,
                        totalnominal = (SELECT SUM(C.nominal) FROM [cost] c WHERE c.coa = a.id AND c.active =1  
                        $where),
                        totalhead = (SELECT SUM(C.nominal) FROM [cost] c WHERE c.coa = a.parent AND c.active =1  
                        $where)
                      FROM chartofaccount a 
                    WHERE
                        a.active = '1' 
                        AND a.[group] = '1' 
                        $parenta
                    ORDER BY
                    a.sort ASC
        ";
        
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function getLastIDBudget()
    {
        $this->db->select('*');
        $this->db->like('order_id', 'KAS', 'after');
        $this->db->order_by("order_id", "desc");
        $result = $this->db->get('cost',1);
        return $result->result_array();
    }

    public function getBudget()
    {
        $result = $this->db->query("SELECT
        dbo.akun.name,
        dbo.cost.*
        
        FROM
        dbo.cost
        INNER JOIN dbo.akun ON dbo.cost.akun_id = dbo.akun.id
        where dbo.cost.akun_id = 1 and dbo.cost.order_id like 'KAS%'  ");

        return $result->result_array();
    }
    public function getOneBudget()
    {
        $id=$_POST['id'];
        
        $this->db->select('*');
        $this->db->where('id',$id);
        $result=$this->db->get('cost');

        return $result->result();
    }
     public function saveBudget()
    {
        $ad = array(
            'coa' => $this->input->post('costid'), 
            'order_id' => $this->input->post('code'), 
            'date' => $this->input->post('date'),
            'description' => $this->input->post('keterangan'),
            'nominal' => str_replace(",", "", $this->input->post('nominals')), 
            'active'=>1,
            'akun_id'=>1,
            'coa'=>4,
            'createby' => $this->session->userdata('id')
        );

        $this->db->insert('cost', $ad);
    }

    public function getInventaris(){

        $this->db->select('*');
        //$this->db->where('active', '1');
        $result = $this->db->get('inventaris');

        return $result->result_array();
    }

    public function saveInventaris()
    {
        $ad = array(
            'name' => $this->input->post('nama'), 
            'tanggal_beli' => $this->input->post('date'),
            'qty' => 1, 
            'harga_beli' => $this->input->post('nominals')
        );

        $this->db->insert('inventaris', $ad);

        $ad = array(
            'order_id' =>$this->input->post('nama'),
            'cost_id' => 46, 
            'date' => $this->input->post('date'),
            'nominal' => $this->input->post('nominals'), 
            'akun_id' => 4, 
            'createby' => $this->session->userdata('id')
        );

        $this->db->insert('cost', $ad);
    }

    
}