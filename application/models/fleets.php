<?php if (!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class fleets extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getFleets()
    {

        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('fleets');

        return $result->result_array();
    }

    public function getFleettypes()
    {

        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('fleettypes');

        return $result->result_array();
    }

    public function getOneOrder()
    {
        $data = $_POST['id'];

        $this->db->select('*');
        $this->db->where('id', $data);
        $result = $this->db->get('fleets');

        return $result->result();
    }

    function selectOne($data = null)
    {
        if ($data['id'] > 0) {
            $this->db->where('id', $data['id']);
            $result = $this->db->get('customers');
            return $result->row();
        }
    }

    function costoffice()
    {
        $where = "";

        if ($this->input->post('start') > 0 && $this->input->post('end') > 0) {
            $start = $this->input->post('start');
            $end = $this->input->post('end');
            $where .= " AND [date] BETWEEN '" . $this->input->post('start') . "' AND '" . $this->input->post('end') . "'";
        }

        if ($this->input->get('start') > 0 && $this->input->get('end') > 0) {
            $start = $this->input->get('start');
            $end = $this->input->get('end');
            $where .= " AND [date] BETWEEN '" . $this->input->get('start') . "' AND '" . $this->input->get('end') . "'";
        }

        $sql = ("SELECT COALESCE ( SUM (nominal ), 0 ) AS costsd FROM cost WHERE akun_id ='99' AND  active = 1 $where");

        $result = $this->db->query($sql);
        return $result->result();
    }

    function ordersummary($data = null)
    {
        //$start = date('Y-m-01');
        //$end = date('Y-m-t');
        $where = "";
        if ($this->input->post('fleetplateno') != '') {
            $where .= " AND f.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->post('fleetplateno'))) . "%' ";
        }


        if ($this->input->post('type') != '') {
            $where .= " AND o.id_ordertypes LIKE '%" . $this->db->escape_str(strtolower($this->input->post('type'))) . "%' ";
        }

        if ($this->input->post('origin') != '') {
            $where .= " AND b.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('origin'))) . "%' ";
        }
        if ($this->input->post('destination') != '') {
            $where .= " AND c.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('destination'))) . "%' ";
        }
        if ($this->input->post('code') != '') {
            $where .= " AND f.code LIKE '%" . $this->db->escape_str(strtolower($this->input->post('code'))) . "%' ";
        }

        if ($this->input->post('customer') != '') {
            $where .= "AND cu.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('customer'))) . "%' ";
        }

        if ($this->input->get('fleetplateno') != '') {
            $where .= " AND f.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->get('fleetplateno'))) . "%' ";
        }

        if ($this->input->get('code') != '') {
            $where .= " AND f.code LIKE '%" . $this->db->escape_str(strtolower($this->input->get('code'))) . "%' ";
        }

        if ($this->input->get('customer') != '') {
            $where .= " AND cu.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('customer'))) . "%' ";
        }

        if ($this->input->get('origin') != '') {
            $where .= " AND b.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('origin'))) . "%' ";
        }
        if ($this->input->get('destination') != '') {
            $where .= " AND c.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('destination'))) . "%' ";
        }

        if ($this->input->get('type') != '') {
            $where .= " AND o.id_ordertypes LIKE '%" . $this->db->escape_str(strtolower($this->input->get('type'))) . "%' ";
        }

        if ($this->input->post('start') > 0 && $this->input->post('end') > 0) {
            $start = $this->input->post('start');
            $end = $this->input->post('end');
            //$where .= " AND o.[date] BETWEEN '" . $this->input->post('start') . "' AND '" . $this->input->post('end') . "'";
        }

        if ($this->input->get('start') > 0 && $this->input->get('end') > 0) {
            $start = $this->input->get('start');
            $end = $this->input->get('end');
            // $where .= " AND o.[date] BETWEEN '" . $this->input->get('start') . "' AND '" . $this->input->get('end') . "'";
        }

        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }

        $sql = "SELECT
                    * 
                FROM
                    (
                    SELECT
                        ROW_NUMBER ( ) OVER ( ORDER BY A.ritase DESC ) AS RowNum,
                        A.* 
                    FROM
                        (
                        SELECT
                            f.id,
                            f.fleetplateno,
                            f.fleetnumber,
                            f.code,
                            Maintenance,
                            lainlain,
                            biayatagih,
                            ft.name AS fleettype_name,
                            COALESCE ( SUM ( CASE WHEN o.[date] BETWEEN '$start' AND '$end' THEN o.ritase ELSE 0 END  ), 0 ) AS ritase,
                            COALESCE ( SUM ( CASE WHEN o.[date] BETWEEN '$start' AND '$end' THEN o.allowances ELSE 0 END ), 0 ) AS allowances,
                            COALESCE ( SUM ( CASE WHEN o.[date] BETWEEN '$start' AND '$end' THEN o.allowanceadds ELSE 0 END  ), 0 ) AS allowanceadds,
                            COALESCE ( SUM ( CASE WHEN o.[date] BETWEEN '$start' AND '$end' THEN o.loadqty ELSE 0 END  ), 0 ) AS loadqty,
                            COALESCE ( SUM (CASE WHEN o.[date] BETWEEN '$start' AND '$end' THEN o.prices ELSE 0 END  ), 0 ) AS prices 
                        FROM
                            fleets f
                            LEFT JOIN orders o ON f.id = o.id_fleets 
                            AND o.active = 1
                            LEFT JOIN fleettypes ft ON f.id_fleettypes = ft.id
                            LEFT JOIN (
                            SELECT
                                maintenance_detail.fleet_id,
                                SUM ( CASE WHEN dbo.maintenance_detail.[date] BETWEEN '$start' AND '$end' AND dbo.maintenance_detail.active = 1 THEN maintenance_detail.price ELSE 0 END ) AS Maintenance 
                            FROM
                                maintenance_detail 
                            GROUP BY
                                maintenance_detail.fleet_id 
                            ) test ON f.id = test.fleet_id
                            LEFT JOIN (
                            SELECT
                                orders.id_fleets,
                                SUM ( CASE WHEN dbo.cost.[date] BETWEEN '$start' AND '$end' AND cost.nominal2 = 1 THEN cost.nominal ELSE 0 END ) AS lainlain 
                            FROM
                                cost
                                LEFT JOIN orders ON cost.order_id = orders.code 
                            GROUP BY
                                orders.id_fleets 
                            ) tests ON f.id = tests.id_fleets
                            LEFT JOIN (
                            SELECT
                                orders.id_fleets,
                                SUM ( CASE WHEN dbo.cost.[date] BETWEEN '$start' AND '$end' AND cost.nominal2 = 2 THEN cost.nominal ELSE 0 END ) AS biayatagih 
                            FROM
                                cost
                                LEFT JOIN orders ON cost.order_id = orders.code 
                            GROUP BY
                                orders.id_fleets 
                            ) testss ON f.id = testss.id_fleets 
                        WHERE
                            f.active = 1 
                            $where
                        GROUP BY
                            f.id,
                            f.fleetplateno,
                            f.code,
                            test.Maintenance,
                            tests.lainlain,
                            testss.biayatagih,
                            f.fleetnumber,
                            ft.name 
                        ) A 
                    ) B 
                WHERE
                    B.fleetplateno IS NOT NULL
                            $paging
                        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    function costsummary($data = null)
    {
        $where = "";
        if ($this->input->post('fleetnumber') != '') {
            $where .= " AND f.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->post('fleetnumber'))) . "%' ";
        }

        if ($this->input->post('costtype') != '') {
            $where .= " AND cc.cost_id = '" . $this->db->escape_str(strtolower($this->input->post('costtype'))) . "' ";
        }

        if ($this->input->post('code') != '') {
            $where .= " AND f.code LIKE '%" . $this->db->escape_str(strtolower($this->input->post('code'))) . "%' ";
        }


        if ($this->input->get('fleetnumber') != '') {
            $where .= " AND f.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->get('fleetnumber'))) . "%' ";
        }

        if ($this->input->get('code') != '') {
            $where .= " AND f.code LIKE '%" . $this->db->escape_str(strtolower($this->input->get('code'))) . "%' ";
        }

        if ($this->input->get('costtype') != '') {
            $where .= " AND cc.cost_id = '" . $this->db->escape_str(strtolower($this->input->get('costtype'))) . "' ";
        }

        if ($this->input->post('start') > 0 && $this->input->post('end') > 0) {
            $where .= " AND o.orderdate BETWEEN '" . $this->input->post('start') . "' AND '" . $this->input->post('end') . "'";
        }

        if ($this->input->get('start') > 0 && $this->input->get('end') > 0) {
            $where .= " AND o.orderdate BETWEEN '" . $this->input->get('start') . "' AND '" . $this->input->get('end') . "'";
        }

        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }

        $sql = "
            SELECT
                * 
            FROM
                (
                SELECT
                    ROW_NUMBER () OVER ( ORDER BY A.totalcost DESC ) AS RowNum,
                    A.* 
                FROM
                    (
                    SELECT
                        f.id,
                        f.fleetplateno,                   
                        COALESCE ( SUM ( cc.nominal ), 0 ) AS totalcost
                    FROM
                        fleets f
                        LEFT JOIN orders o ON f.id = o.id_fleets AND o.active = 1
                        LEFT JOIN cost cc ON o.code = cc.order_id 
                    WHERE
                        f.active = 1 
                        AND cc.akun_id = 14

                        $where
                    GROUP BY
                        f.id,
                        f.fleetplateno
                ) A
            ) B
            WHERE
            B.fleetplateno IS NOT NULL
            $paging
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    function driversummary($data = null)
    {
        $where = "";
        if ($this->input->post('fleetnumber') != '') {
            $where .= " AND f.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->post('fleetnumber'))) . "%' ";
        }

        if ($this->input->post('origin') != '') {
            $where .= " AND b.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('origin'))) . "%' ";
        }

        if ($this->input->post('destination') != '') {
            $where .= " AND c.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('destination'))) . "%' ";
        }

        if ($this->input->post('fleettype') != '') {
            $where .= " AND ft.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('fleettype'))) . "%' ";
        }

        if ($this->input->post('driver') != '') {
            $where .= " AND dr.name LIKE '" . $this->db->escape_str(strtolower($this->input->post('driver'))) . "%' ";
        }

        if ($this->input->post('customer') != '') {
            $where .= "AND cu.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('customer'))) . "%' ";
        }

        if ($this->input->get('fleetnumber') != '') {
            $where .= " AND f.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->get('fleetnumber'))) . "%' ";
        }

        if ($this->input->get('driver') != '') {
            $where .= " AND dr.name LIKE '" . $this->db->escape_str(strtolower($this->input->get('driver'))) . "%' ";
        }

        if ($this->input->get('customer') != '') {
            $where .= " AND cu.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('customer'))) . "%' ";
        }

        if ($this->input->get('fleettype') != '') {
            $where .= " AND ft.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('fleettype'))) . "%' ";
        }

        if ($this->input->post('start') > 0 && $this->input->post('end') > 0) {
            $where .= " AND o.orderdate BETWEEN '" . $this->input->post('start') . "' AND '" . $this->input->post('end') . "'";
        }
        if ($this->input->get('origin') != '') {
            $where .= " AND b.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('origin'))) . "%' ";
        }
        if ($this->input->get('destination') != '') {
            $where .= " AND c.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('destination'))) . "%' ";
        }

        if ($this->input->get('start') > 0 && $this->input->get('end') > 0) {
            $where .= " AND o.orderdate BETWEEN '" . $this->input->get('start') . "' AND '" . $this->input->get('end') . "'";
        }

        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }

        $sql = "
            SELECT
                * 
            FROM
                (
                SELECT
                    ROW_NUMBER () OVER ( ORDER BY A.ritase DESC ) AS RowNum,
                    A.* 
                FROM
                    (
                    SELECT
                        dr.name AS drivernama,
                        f.fleetplateno AS plat,
                        cu.name AS namac,
                        ft.name AS fleettype_name,
                        COALESCE ( SUM ( o.ritase ), 0 ) AS ritase 
                    FROM
                        orders o
                        LEFT JOIN fleets f ON f.id = o.id_fleets 
                        AND o.active = 1
                        LEFT JOIN drivers dr ON o.id_drivers = dr.id
                        
                        LEFT JOIN fleettypes ft ON f.id_fleettypes = ft.id
                        LEFT JOIN customers cu ON o.id_customers = cu.id 
                    WHERE
                        f.active = 1 
                        $where
                    GROUP BY
                        dr.name,
                        ft.name,
                        f.fleetplateno,
                        cu.name 
                    ) A 
                ) B 
            WHERE
                B.drivernama IS NOT NULL
            $paging
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }


    public function editFleets()
    {
        $id                 = $this->input->post('id');
        $data['noframe']        = $this->input->post('frameno');
        $data['noengine']       = $this->input->post('engineno');
        $data['fleetnumber']    = $this->input->post('plateno');
        $data['fleetplateno']   = $this->input->post('plateno');
        $data['id_fleettypes']   = $this->input->post('fleettype');

        $data['active']     = 1;

        $this->db->where('id', $id);
        $this->db->update('fleets', $data);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('fleets', 1);
        return $result->result_array();
    }

    public function Simpan()
    {
        $data['id']             = $this->input->post('id');
        $data['noframe']        = $this->input->post('frameno');
        $data['noengine']       = $this->input->post('engineno');
        $data['fleetnumber']    = $this->input->post('plateno');
        $data['fleetplateno']   = $this->input->post('plateno');
        $data['id_fleettypes']   = $this->input->post('fleettype');
        $data['active']         = 1;

        $this->db->insert('fleets', $data);
    }

    public function Delete($value = '')
    {
        $id = $_GET['id'];

        $data['active']     = 0;

        $this->db->where('id', $id);
        $this->db->update('fleets', $data);
    }

    function getAvailableFleets()
    {
        //$kilat = $this->load->database('kilat', true);
        $sql = "SELECT * FROM
                (
                    SELECT 
                        
                        CASE
                            WHEN isnull(t.id,0) = 0 OR ( (isnull(t.id,0) > 0 AND ( LOWER(gps_hotport) = 'pool pondok kopi' OR LOWER(gps_hotport) = 'pull karawang'  ) ) ) then 'Available' ELSE 'On Duty' END order_status
                        , CASE
                            WHEN isnull(t.id,0) = 0 OR ( (isnull(t.id,0) > 0 AND ( LOWER(gps_hotport) = 'pool pondok kopi' OR LOWER(gps_hotport) = 'pull karawang'  ) ) ) then 1 ELSE 2 END order_status_no
                        , CASE
                            WHEN DATEDIFF ( hour , gps_datetime , GETDATE()) >= 12
                            THEN 'Inactive'
                            ELSE
                            CASE
                                WHEN gps_acc = 0 THEN                   'Stop'
                                WHEN gps_acc = 1 AND gps_speed = 0 THEN 'Idle' 
                                ELSE                                    'Run'
                            END
                          END gps_status
                        , CASE
                            WHEN DATEDIFF ( hour , gps_datetime , GETDATE()) >= 12
                            THEN '-1'
                            ELSE
                            CASE
                                WHEN gps_acc = 0 THEN                   '2'
                                WHEN gps_acc = 1 AND gps_speed = 0 THEN '1' 
                                ELSE                                    '0'
                            END
                          END gps_status_nu
                        , gps_datetime
                        , CASE gps_acc
                            WHEN 0 THEN 'Off'
                            WHEN 1 THEN 'On'
                        END gps_acc
                        , 0 gps_idle_time
                        , gps.gps_vehiclenumber AS fleetplateno
                        , ISNULL(gps.gps_speed,0) as gps_speed
                        , gps.gps_address
                         , gps_odometer
                        , isnull(gps_hotport,'-') as gps_hotport
                        , gps_datetiemhotpot
                         ,isnull(gps_lat,0) gps_lat
                        , isnull(gps_long,0) gps_long
                    FROM
                    (
                        SELECT
                            gps.gps_deviceid
                            , gps_vehiclenumber
                            , gps.gps_datetime
                            , gps.gps_address
                            , gps.gps_lat
                            , gps.gps_long
                            , gps.gps_speed
                            , gps.gps_acc
                            , gps.gps_odometer
                            ,gps.gps_hotport
                            , null gps_datetiemhotpot
                        FROM
                            db_cpk_synergy.dbo.data_gps_fleet gps
                            RIGHT OUTER JOIN (
                                select max(gps_id) gps_id, gps_deviceid from db_cpk_synergy.dbo.data_gps_fleet
                                where gps_vehiclenumber in ('B9158NEU','B9208NEU','B9219NEU')
                                group by gps_deviceid
                            )a ON a.gps_id = gps.gps_id
                        ) gps
                        LEFT OUTER JOIN fleets a ON a.fleetnumber = gps.gps_deviceid
                        LEFT OUTER JOIN (SELECT CASE WHEN COUNT(id) = 0 THEN 0 ELSE 1 END AS id, id_fleets FROM orderassigns WHERE status = 2 AND active = 1 GROUP BY id_fleets) t ON t.id_fleets = a.id
                    )a
                    ORDER BY a.order_status_no                  ";
        $result = $this->db->query($sql);
        //$result = $kilat->query($sql);
        return $result->result();
    }
}
