<?php if (!defined('BASEPATH')) exit('Hacking Attempt. Keluar dari sistem.');

class kass extends CI_Model
{
    public function selectAll($data = null)
    {
        $where = "";
        if ($this->input->post('akun') != "") {
            $where .= "AND dbo.akun.id = '" . $this->input->post('akun') . "'";
        }

        if ($this->input->post('keterangan') != "") {
            $where .= "AND dbo.cost.description LIKE '%" . $this->input->post('keterangan') . "%'";
        }

        if ($this->input->post('datefrom') > 0 && $this->input->post('dateto') > 0) {
            $where .= "AND dbo.cost.date BETWEEN '" . $this->input->post('datefrom') . "' AND '" . $this->input->post('dateto') . "'";
        }

        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }

        $sql = "SELECT
                * 
                FROM
                    (
                    SELECT
                        ROW_NUMBER ( ) OVER ( ORDER BY A.name DESC ) AS RowNum,
                        A.* 
                    FROM
                        (
                            SELECT dbo.akun.name,dbo.akun.id as idakun,dbo.cost.* FROM dbo.cost INNER JOIN dbo.akun ON dbo.cost.akun_id = dbo.akun.id WHERE dbo.cost.akun_id = 1 AND dbo.cost.coa = 4 and dbo.cost.active =1  $where
                        ) A 
                    ) B 
                WHERE
                    B.name IS NOT NULL
                $paging
            ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function selectOne($data = null)
    {
        $this->db->select("a.name,a.id as idakun,b.*");

        if (isset($data["id"]) && $data["id"] > 0) {
            $this->db->where("b.id", $data["id"]);
        }
        $this->db->join('cost b', 'a.id = b.akun_id', 'left');
        $result = $this->db->get('akun a');
        return $result->row();
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->db->set('active', 0);
        $this->db->where('id', $id);
        $this->db->update('cost');
    }
}
