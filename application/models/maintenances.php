<?php if (!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class maintenances extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getMaintenance()
    {

        $this->db->select('a.*, b.fleetplateno as fleet, c.name as supplier, d.part_number,d.code_part,d.price');
        $this->db->join('fleets b', 'a.fleet_id = b.id', 'left');
        $this->db->join('supplier c', 'a.supplier_id = c.id', 'left');
        $this->db->join('maintenance_detail d', 'a.code = d.maintenance_code', 'left');
        $this->db->where('a.active', '1');
        $result = $this->db->get('maintenanc a');

        return $result->result_array();
    }

    public function loadMaintenance($data = null)
    {
        $where = "";

        if (!empty($this->input->post('code'))) {
            $where .= "AND a.code LIKE '%" . $this->input->post('code') . "%'";
        }
        if (!empty($this->input->post('fleet'))) {
            $where .= "AND b.fleetplateno LIKE '%" . $this->input->post('fleet') . "%'";
        }

        if (!empty($this->input->get('fleet'))) {
            $where .= "AND b.fleetplateno LIKE '%" . $this->input->get('fleet') . "%'";
        }

        if ($this->input->post('datefrom') > 0 && $this->input->post('dateto') > 0) {
            $where .= "AND a.date between '" . $this->input->post('datefrom') . "' and '" . $this->input->post('dateto') . "'";
        }

        if ($this->input->get('datefrom') > 0 && $this->input->get('dateto') > 0) {
            $where .= "AND a.date between '" . $this->input->get('datefrom') . "'and '" . $this->input->get('dateto') . "'";
        }

        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }

        $sql = "SELECT
                * 
                FROM
                    (
                    SELECT
                        ROW_NUMBER ( ) OVER ( ORDER BY A.code DESC ) AS RowNum,
                        A.* 
                    FROM
                        (
                            SELECT a.id,a.code,a.date, b.fleetplateno as fleet, c.name as supplier, d.part_number, d.code_part, d.price FROM maintenance a LEFT JOIN fleets b ON a.fleet_id = b.id LEFT JOIN supplier c ON a.supplier_id = c.id LEFT JOIN maintenance_detail d ON a.code = d.maintenance_code WHERE a.active = 1 $where
                        ) A 
                    ) B 
                WHERE
                    B.code IS NOT NULL
                $paging
            ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function saveEditMaintenance()
    {
        date_default_timezone_set("Asia/Bangkok");
        $code                         = $this->input->post('code');
        $data['date']         = $this->input->post('tanggal');
        $data['supplier_id']         = $this->input->post('supplier');
        $data['fleet_id']          = $this->input->post('armada');
        $data['updateby']       = $this->session->userdata('id');
        $data['updatetime']     = date("Y-m-d h:i:s");

        $this->db->where('code', $code);
        $this->db->update('maintenance', $data);

        $codeparts    = $this->input->post('codeparts');
        $partnumber     = $this->input->post('partnumber');
        $fleet_id     = $this->input->post('fleet');
        $price   = str_replace(",", "", $this->input->post('price'));

        $adddefect = count($codeparts);


        if ($adddefect >= 0) {
            for ($i = 0; $i < $adddefect; $i++) {
                $id   = $this->input->post('id[' . $i . ']');

                $dt = array(
                    'maintenance_code' => $this->input->post('code'),
                    'code_part' => $codeparts[$i],
                    'part_number' => $partnumber[$i],
                    'fleet_id' => $fleet_id[$i],
                    'price' => $price[$i]
                );
                if (isset($id)) {
                    $this->db->where('id', $id);
                    $this->db->update('maintenance_detail', $dt);
                } else {

                    $this->db->insert('maintenance_detail', $dt);
                }
            }

            for ($i = 0; $i < $adddefect; $i++) {

                if (isset($code)) {
                    $dt = array(
                        'date' => $this->input->post('date'),
                        'nominal' => $price[$i],
                        'coa' => 110,
                        'cost_type' => 1,
                        'active' => 1,
                        'akun_id' => 13,
                    );
                    $this->db->where('order_id', $code);
                    $this->db->update('cost', $dt);
                } else {
                    $dt = array(
                        'order_id' => $this->input->post('code'),
                        'date' => $this->input->post('date'),
                        'nominal' => $price[$i],
                        'coa' => 110,
                        'cost_type' => 1,
                        'active' => 1,
                        'akun_id' => 13,
                    );
                    $this->db->insert('cost', $dt);
                }
            }
        }
    }

    public function saveMaintenance()
    {
        date_default_timezone_set("Asia/Bangkok");
        $data['code']           = $this->input->post('code');
        $data['supplier_id']    = $this->input->post('supplier');
        $data['date']           = $this->input->post('date');
        $data['fleet_id']       = $this->input->post('fleet');
        $data['active']         = 1;
        $data['createby']       = $this->session->userdata('id');
        $data['createdate']     = date("Y-m-d h:i:s");

        $this->db->insert('maintenance', $data);

        $codeparts    = $this->input->post('codeparts');
        $partnumber     = $this->input->post('partnumber');
        $fleet_id     = $this->input->post('fleet');
        $price   = str_replace(",", "", $this->input->post('price'));

        $adddefect = count($partnumber);


        if ($adddefect >= 0) {
            for ($i = 0; $i < $adddefect; $i++) {
                $dt = array(
                    'maintenance_code' => $this->input->post('code'),
                    'date' => $this->input->post('date'),
                    'code_part' => $codeparts[$i],
                    'part_number' => $partnumber[$i],
                    'fleet_id' => $fleet_id,
                    'price' => $price[$i],
                    'active' => 1
                );
                $this->db->insert('maintenance_detail', $dt);
            }
            for ($i = 0; $i < $adddefect; $i++) {
                $dt = array(
                    'order_id' => $this->input->post('code'),
                    'date' => $this->input->post('date'),
                    'nominal' => $price[$i],
                    'coa' => 110,
                    'cost_type' => 1,
                    'active' => 1,
                    'akun_id' => 13,
                );
                $this->db->insert('cost', $dt);
            }
        }
    }

    public function getOneMaintenance()
    {
        $data = $_POST['id'];

        $this->db->select('*');
        $this->db->where('code', $data);
        $result = $this->db->get('maintenance');
        return $result->result_array();
    }

    public function getDetailMaintenance($data = null)
    {

        //$data = $_POST['id'];

        $this->db->select('*');
        $this->db->where('maintenance_code', $data['code']);
        $result = $this->db->get('maintenance_detail');
        return $result->result_array();
    }

    public function getOneDebit()
    {
        $data = $_POST['id'];

        $this->db->select('a.*, b.firstname as nama, b.id as idc');
        $this->db->join('users b', 'a.id_employees = b.id', 'left');
        $this->db->where('a.active', '1');
        $this->db->where('a.id', $data);
        $result = $this->db->get('debitcredit a');

        return $result->result();
    }



    public function getKoordinator()
    {
        $this->db->select('*');
        $this->db->where('id_userroles', '3');
        $result = $this->db->get('users');
        return $result->result_array();
    }

    public function getCredit()
    {
        $akun = $this->session->userdata('id');
        $this->db->select('a.*, b.firstname as nama');
        $this->db->join('users b', 'a.createby = b.id', 'left');
        $this->db->where('a.active', '1');
        $this->db->where('a.createby', $akun);
        $result = $this->db->get('orders a');

        return $result->result_array();
    }

    function selectOne($data = null)
    {
        $this->db->select(" a.id,a.code,a.date, b.fleetplateno as fleet,b.id as fleet_id, c.name as supplier,c.id as supplier_id, d.part_number, d.code_part, d.price");

        if (isset($data["id"]) && $data["id"] > 0) {
            $this->db->where("a.id", $data["id"]);
        }
        $this->db->join('fleets b', 'a.fleet_id = b.id', 'left');
        $this->db->join('supplier c', 'a.supplier_id = c.id', 'left');
        $this->db->join('maintenance_detail d', 'a.code = d.maintenance_code', 'left');
        $result = $this->db->get('maintenance a');
        return $result->row();
    }

    public function editDebit()
    {
        $nominal = $this->input->post('nomnals');
        $ntk = str_replace(",", "", $nominal);
        $id          = $this->input->post('code');
        $data['id_employees']   = $this->input->post('nama');
        $data['dates']          = $this->input->post('date');
        $data['desc_1']         = $this->input->post('necesary');
        $data['desc_2']         = $this->input->post('descripton');
        $data['price']          = $ntk;
        $data['active']         = 1;

        $this->db->where('code', $id);
        $this->db->update('debitcredit', $data);
    }

    public function editRequest()
    {
        $id          = $this->input->post('id');
        $data['necessary']         = $this->input->post('necessary');
        $data['desciption']         = $this->input->post('desc');
        $data['price']          = $this->input->post('nominals');

        $this->db->where('id', $id);
        $this->db->update('requestpayable', $data);
    }

    public function approvedebit()
    {
        $id          = $this->input->post('code');
        $nama        = $this->input->post('nama');

        //
        $this->db->select('*');
        $this->db->where('firstname', $nama);
        $result = $this->db->get('users');
        foreach ($result->result() as $key) {
            $awal   = $key->nominal;
        }


        $tambah = $this->input->post('nominal');

        $add['nominal']        = $awal + $tambah;
        $data['status']        = "VALID";
        $data['trfdate']       = $this->input->post('trfdate');

        $this->db->where('code', $id);
        $this->db->update('debitcredit', $data);

        $this->db->where('firstname', $nama);
        $this->db->update('users', $add);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("code", "desc");
        $result = $this->db->get('maintenance', 1);
        return $result->result_array();
    }



    public function getSupplier()
    {
        $this->db->select('*');
        $result = $this->db->get('supplier');
        return $result->result_array();
    }

    public function Delete()
    {
        $id = $this->input->post('id');
        $this->db->where('order_id', $id);

        if ($this->db->delete('cost')) {
            $data['active']     = 0;

            $this->db->where('code', $id);
            $this->db->update('maintenance', $data);

            $dat['active']     = 0;

            $this->db->where('maintenance_code', $id);
            $this->db->update('maintenance_detail', $dat);
        }
    }
}
