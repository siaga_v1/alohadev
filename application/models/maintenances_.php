<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class maintenances extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getMaintenance(){

        $this->db->select('a.*, b.fleetplateno as fleet, c.name as supplier, d.part_number,d.code_part,d.price');
        $this->db->join('fleets b','a.fleet_id = b.id','left');
        $this->db->join('supplier c','a.supplier_id = c.id','left');
        $this->db->join('maintenance_detail d','a.code = d.maintenance_code','left');
        $this->db->where('a.active', '1');
        $result = $this->db->get('maintenance a');

        return $result->result_array();
        
    }   

    public function saveEditMaintenance()
    {
        date_default_timezone_set("Asia/Bangkok");
        $code                         = $this->input->post('code');
        $data['date']         = $this->input->post('tanggal');
        $data['supplier_id']         = $this->input->post('supplier');
        $data['fleet_id']          = $this->input->post('armada');        
        $data['updateby']       = $this->session->userdata('id');
        $data['updatetime']     = date("Y-m-d h:i:s");
        
        $this->db->where('code', $code);
        $this->db->update('maintenance',$data);

        $codeparts    = $this->input->post('codeparts');
        $partnumber     = $this->input->post('partnumber');
        $fleet_id     = $this->input->post('fleet');
        $price   = str_replace(",", "", $this->input->post('price'));

        $adddefect = count($codeparts);


        if ($adddefect >= 0) {
            for($i=0; $i < $adddefect; $i++) 
            {
                $id   = $this->input->post('id['.$i.']');

                $dt = array(
                    'maintenance_code'=>$this->input->post('code'),
                    'code_part'=>$codeparts[$i],
                    'part_number'=>$partnumber[$i],
                    'fleet_id'=>$fleet_id[$i],
                    'price'=>$price[$i]
                );
                if (isset($id)) {
                    $this->db->where('id', $id);
                    $this->db->update('maintenance_detail', $dt);
                }else{

                    $this->db->insert('maintenance_detail', $dt);

                }
            }
        }
    }

    public function saveMaintenance()
    {
        date_default_timezone_set("Asia/Bangkok");
        $data['code']           = $this->input->post('code');
        $data['supplier_id']    = $this->input->post('supplier');
        $data['date']           = $this->input->post('date');
        $data['fleet_id']       = $this->input->post('fleet');
        $data['active']         = 1;
        $data['createby']       = $this->session->userdata('id');
        $data['createdate']     = date("Y-m-d h:i:s");
                
        $this->db->insert('maintenance',$data);

        $codeparts    = $this->input->post('codeparts');
        $partnumber     = $this->input->post('partnumber');
        $fleet_id     = $this->input->post('fleet');
        $price   = $this->input->post('price');

        $adddefect = count($codeparts);


        if ($adddefect >= 0) {
            for($i=0; $i < $adddefect; $i++) 
            {
                $dt = array(
                    'maintenance_code'=>$this->input->post('code'),
                    'date' => $this->input->post('date'),
                    'code_part'=>$codeparts[$i],
                    'part_number'=>$partnumber[$i],
                    'fleet_id'=>$fleet_id[$i],
                    'price'=>$price[$i]
                );
                $this->db->insert('maintenance_detail', $dt);
            }
            for($i=0; $i < $adddefect; $i++) 
            {
                $dt = array(
                    'order_id'=>$this->input->post('code'),
                    'date'=>$this->input->post('date'),
                    'nominal'=>$price[$i],
                    'cost_id'=>50,
                    'active'=>1,
                    'akun_id'=>13,
                );
                $this->db->insert('cost', $dt);
            }

            for($i=0; $i < $adddefect; $i++) 
            {
                $dt = array(
                    'order_id'=>$this->input->post('code'),
                    'date'=>$this->input->post('date'),
                    'nominal'=>"-".$price[$i]."",
                    'cost_id'=>50,
                    'active'=>1,
                    'akun_id'=>1,
                );
                $this->db->insert('cost', $dt);
            }
        }

            
        
    }

    public function getOneMaintenance()
    {
        $data=$_POST['id'];

        $this->db->select('*');
        $this->db->where('code', $data);
        $result = $this->db->get('maintenance');
        return $result->result_array();
    }

    public function getDetailMaintenance(){

        $data=$_POST['id'];
        $this->db->select('*');
        $this->db->where('maintenance_code', $data);
        $result = $this->db->get('maintenance_detail');
        return $result->result_array();
        
    }   

    public function getOneDebit()
    {
        $data=$_POST['id'];

        $this->db->select('a.*, b.firstname as nama, b.id as idc');
        $this->db->join('users b','a.id_employees = b.id','left');
        $this->db->where('a.active', '1');
        $this->db->where('a.id',$data);
        $result = $this->db->get('debitcredit a');

        return $result->result();
    } 

    

    public function getKoordinator()
    {
        $this->db->select('*');
        $this->db->where('id_userroles', '3');
        $result = $this->db->get('users');
        return $result->result_array();
    }
    
    public function getCredit(){
        $akun = $this->session->userdata('id');
        $this->db->select('a.*, b.firstname as nama');
        $this->db->join('users b','a.createby = b.id','left');
        $this->db->where('a.active', '1');
        $this->db->where('a.createby', $akun);
        $result = $this->db->get('orders a');

        return $result->result_array();
        
    } 

    function selectOne($data = null)
    {
        if($data['id'] > 0){
            $this->db->where('id', $data['id']);
            $result = $this->db->get('debitcredit');
            return $result->row();
        }
    }

    public function editDebit()
    {   $nominal = $this->input->post('nomnals');
        $ntk = str_replace(",", "", $nominal);
        $id          = $this->input->post('code');
        $data['id_employees']   = $this->input->post('nama');
        $data['dates']          = $this->input->post('date');
        $data['desc_1']         = $this->input->post('necesary');
        $data['desc_2']         = $this->input->post('descripton');
        $data['price']          = $ntk;
        $data['active']         = 1;
        
        $this->db->where('code', $id);
        $this->db->update('debitcredit',$data);
    }

    public function editRequest()
    {
        $id          = $this->input->post('id');
        $data['necessary']         = $this->input->post('necessary');
        $data['desciption']         = $this->input->post('desc');
        $data['price']          = $this->input->post('nominals');
        
        $this->db->where('id', $id);
        $this->db->update('requestpayable',$data);
    }

    public function approvedebit()
    {
        $id          = $this->input->post('code');
        $nama        = $this->input->post('nama');

        //
        $this->db->select('*');
        $this->db->where('firstname', $nama);
        $result = $this->db->get('users');
        foreach ($result->result() as $key) {
            $awal   = $key->nominal;
        }

        
        $tambah = $this->input->post('nominal');

        $add['nominal']        = $awal + $tambah;
        $data['status']        = "VALID";
        $data['trfdate']       = $this->input->post('trfdate');
        
        $this->db->where('code', $id);
        $this->db->update('debitcredit',$data);

        $this->db->where('firstname', $nama);
        $this->db->update('users',$add);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("code", "desc");
        $result = $this->db->get('maintenance',1);
        return $result->result_array();
    }

    public function getSupplier()
    {   
        $this->db->select('*');
        $result = $this->db->get('supplier');
        return $result->result_array();
    }

    public function Delete()
    {
        $id=$_POST['id'];
        
        $data['active']     = 0;
            
        $this->db->where('code', $id);
        $this->db->update('maintenance', $data); 
    }
}