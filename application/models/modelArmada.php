<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelArmada extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilArmada(){

        $this->db->select('a.*,b.*,c.*');
        $this->db->join('mst_inv_merk b','a.arm_jenis_code = b.im_id','left');
        $this->db->join('mst_employee c','a.arm_emp_id = c.emp_id','left');
        $result = $this->db->get('mst_armada a');
        return $result->result_array();
        
    }   
    
}