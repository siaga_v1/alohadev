<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelDashboard extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    

    public function totalProduksi(){

    
       // $result = $this->db->query("select SUM(tonnage) as totalton FROM data_produksi");
        //return $result->row();
        //echo $result;
        $y = date('Y');
        
        $today      = date('Y/m/d');
        if(date('D', strtotime($today.' -1 Days')) == 'Sun' )
        {
            $yesterday  = date('Y/m/d', strtotime($today.' -2 Days'));
        }
        else
        {
            $yesterday  = date('Y/m/d', strtotime($today.' -1 Days'));
        }
        
        
        $thismonday = date('Y/m/d', strtotime($today.' this week'));
        $thissunday = date('Y/m/d', strtotime($today.' this sunday'));
        
        $lastmonday = date('Y/m/d', strtotime($thismonday.' -1 week'));
        $lastsunday = date('Y/m/d', strtotime($thissunday.' -1 week'));
        
        $thismonth  = date('Y/m/01');
        $thismonthlast = date('Y/m/t');
        $lastmonth  = date('Y/m/01', strtotime($thismonth.' -1 Months'));
        $lastmonthlast = date('Y/m/t', strtotime($lastmonth));
        
        $thism = date('m', strtotime($thismonth));
        $lastm = date('m', strtotime($lastmonth));
        
        $thisy = date('Y', strtotime($thismonth));
        $lastys = date('Y', strtotime($lastmonth));
        
        $thisyear   = date('Y/m/01  ');
        $lastyear   = date('Y/m/d', strtotime($thisyear.' -1 years'));
        
        $lasty = date('Y', strtotime($lastyear));
        
        $sql = "
                SELECT 
                    SUM( CASE WHEN tgl_produksi = '$today' THEN tonnage ELSE NULL END ) AS order_today,
                    SUM( CASE WHEN tgl_produksi = '$yesterday' THEN tonnage ELSE NULL END ) AS order_yesterday,
                    SUM( CASE WHEN tgl_produksi BETWEEN '$thismonday' AND '$thissunday' THEN tonnage ELSE NULL END ) AS order_thisweek,
                    SUM( CASE WHEN tgl_produksi BETWEEN '$lastmonday' AND '$lastsunday' THEN tonnage ELSE NULL END ) AS order_lastweek,
                    SUM( CASE WHEN tgl_produksi BETWEEN '$thismonth' AND '$thismonthlast' THEN tonnage ELSE NULL END ) AS order_thismonth,
                    SUM( CASE WHEN tgl_produksi BETWEEN '$lastmonth' AND '$lastmonthlast' THEN tonnage  ELSE NULL END ) AS order_lastmonth,
                    SUM( CASE WHEN YEAR(tgl_produksi) = '$y' THEN tonnage ELSE NULL END ) AS order_thisyear,
                    SUM( CASE WHEN YEAR(tgl_produksi) = '$lasty' THEN tonnage ELSE NULL END ) AS order_lastyear,

                    COUNT( CASE WHEN tgl_produksi = '$today' THEN 1 ELSE NULL END ) AS rit_today,
                    COUNT( CASE WHEN tgl_produksi = '$yesterday' THEN 1 ELSE NULL END ) AS rit_yesterday,
                    COUNT( CASE WHEN tgl_produksi BETWEEN '$thismonday' AND '$thissunday' THEN 1 ELSE NULL END ) AS rit_thisweek,
                    COUNT( CASE WHEN tgl_produksi BETWEEN '$lastmonday' AND '$lastsunday' THEN 1 ELSE NULL END ) AS rit_lastweek,
                    COUNT( CASE WHEN tgl_produksi BETWEEN '$thismonth' AND '$thismonthlast' THEN 1 ELSE NULL END ) AS rit_thismonth,
                    COUNT( CASE WHEN tgl_produksi BETWEEN '$lastmonth' AND '$lastmonthlast' THEN 1 ELSE NULL END ) AS rit_lastmonth,
                    COUNT( CASE WHEN YEAR(tgl_produksi) = '$y' THEN 1 ELSE NULL END ) AS rit_thisyear,
                    COUNT( CASE WHEN YEAR(tgl_produksi) = '$lasty' THEN 1 ELSE NULL END ) AS rit_lastyear
                FROM 
                    data_produksi
 
                
        ";
    
        $result = $this->db->query($sql);
        return $result->row();
        
    }   

    public function listProduksi(){

        $this->db->select('p.*,e.*,a.*');
        $this->db->join('mst_armada a','p.arm_id = a.arm_id', 'right');
        $this->db->join('mst_employee e','p.emp_id = e.emp_id', 'right');
        $result = $this->db->get('data_produksi p');
        return $result->result_array();

    }

    public function itemDashboard(){

        $this->db->select('p.*,e.*,a.*,b.*');
        $this->db->join('mst_inv_merk a','p.ii_category = a.im_id', 'left');
        $this->db->join('mst_inv_item_group e','p.ii_type = e.iig_id', 'left');
        $this->db->join('mst_inv_location b','p.ii_loc_rak = b.il_id', 'left');
        $this->db->where('p.ii_stock <= p.ii_min');
        $result = $this->db->get('mst_inv_item p');
        return $result->result_array();

    }
    
}