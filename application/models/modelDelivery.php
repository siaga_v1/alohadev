<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelDelivery extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilPO(){

    	$this->db->select('a.*,c.*,d.*');
       // $this->db->join('data_pengadaan_item_detail b','a.dape_code = b.dapeid_dape_code', 'LEFT');
        $this->db->join('mst_armada c','a.dape_arm_id = c.arm_id', 'left');
        $this->db->join('mst_employee d','a.dape_emp_id = d.emp_id', 'left');
        $this->db->where('a.dape_status IS NULL',null, false);
        //$this->db->where('p.dobi_status );');
        $result = $this->db->get('data_pengadaan a');
        return $result->result_array();

    }  
    public function ambilNomor(){

        $now=date('m-Y');
        $this->db->select('*');
        $this->db->order_by("dape_id", "desc");
        $this->db->like('dape_createtime', $now, 'before');
        $result = $this->db->get('data_pegiriman');
        return $result->result_array();
        
    }

    public function ambilDO()
    {

        $this->db->select('a.*,b.*');
        $this->db->join('data_pegiriman_item b','a.dape_id = b.dape_id', 'left');
        $this->db->where('a.dape_status','1');
        $result = $this->db->get('data_pegiriman a');
        return $result->result_array();
        
    }  

    public function detailDO()
    {
    	$id=$_GET['id'];
        $this->db->select('a.*,b.*,c.*');
        $this->db->join('data_pengadaan_item_detail b','a.dapei_code = b.dapeid_dape_code', 'left');
        $this->db->join('data_pegiriman c','c.dape_id = a.dape_id', 'left');
        $this->db->where('c.dape_id',$id);
        $result = $this->db->get('data_pegiriman_item a');
        return $result->result_array(); 
    } 

    public function simpanDO()
    {

		date_default_timezone_set("Asia/Jakarta");
	    $nowdate = date('ydmGis');
	    $now=date('d-m-Y');

    	$DONumber = $this->input->post('DONumber');

    	$detail = $this->input->post('detail');
        $data['dape_id']           = $this->input->post('DONumber');
        $data['dape_status']       = '1';
        $data['dape_createtime']   = $now;
        
        $this->db->insert('data_pegiriman',$data);
     
        foreach($detail as $details)
        { 	
        	if(isset($details['cek'])){
			$dat=$details['code'];       
        	$datad['dape_id']         = $this->input->post('DONumber');
        	$datad['dapei_code']      = $details['code'];
        
        	$this->db->insert('data_pegiriman_item',$datad);

        	$this->db->query("UPDATE data_pengadaan SET dape_status = '1' WHERE dape_code = '$dat'");
        }
        }
    	
    }
    
}