<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelEmployee extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilEmployee(){

        $this->db->select('u.*, kat.*');
            $this->db->join('mst_kat_emp kat','u.emp_kategori = kat.kat_emp_id', 'right');
            $this->db->where('u.emp_aktif',1);
            $result = $this->db->get('mst_employee u');
        return $result->result_array();
        
    }   
    
}