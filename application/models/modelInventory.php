<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelInventory extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilInventory(){

        $this->db->select('p.*,e.*,a.*,b.*');
        $this->db->join('mst_inv_merk a','p.ii_category = a.im_id', 'left');
        $this->db->join('mst_inv_item_group e','p.ii_type = e.iig_id', 'left');
        $this->db->join('mst_inv_location b','p.ii_loc_rak = b.il_id', 'left');
        $result = $this->db->get('mst_inv_item p');
        return $result->result_array();
        
    }

    public function ambilKategori(){

    $this->db->select('*');
        //$this->db->where('ii_stock',0);
        $result = $this->db->get('mst_inv_merk');
        return $result->result_array();

    }

    public function riwayatItem(){

    	$code=$_GET['id'];

       $this->db->select('u.*,p.*,o.*, sr.*, kat.*');
        $this->db->join('mst_armada kat','u.bk_arm = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.bk_emp = sr.emp_id', 'right');
        $this->db->join('data_barang_keluar_detail p','u.bk_id = p.id_bk', 'right');
        $this->db->join('mst_inv_item o','p.bki_item = o.ii_id', 'right');
        $this->db->where('p.bki_item',$code);
        $result = $this->db->get('data_barang_keluar u');
        return $result->result_array();
        
    }   
    
}