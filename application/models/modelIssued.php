<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelIssued extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public  function listPermintaanBarangApproved(){

        $this->db->select('u.*, sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dpb_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dpb_emp_id = sr.emp_id', 'right');
        $this->db->where('u.dpb_status','9');
        $result = $this->db->get('data_permintaan_barang u');
        return $result->result_array();
    }
    public  function listPermintaanBarangA(){

        $this->db->select('u.*, sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dpb_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dpb_emp_id = sr.emp_id', 'right');
        $this->db->where('u.dpb_status','11');
        $result = $this->db->get('data_permintaan_barang u');
        return $result->result_array();
    }

    public  function detailOrderBarang(){

        $idpb=$_GET['id'];

        $this->db->select('u.*,p.*,o.*, sr.*, kat.*');
        $this->db->join('mst_armada kat','u.dpb_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dpb_emp_id = sr.emp_id', 'right');
        $this->db->join('data_permintaan_barang_item p','u.dpb_code = p.dpbi_dpb_id', 'right');
        $this->db->join('mst_inv_item o','p.dpbi_item_id = o.ii_id', 'right');
        $this->db->where('u.dpb_code',$idpb);
        $this->db->where('p.dpbi_status',0);
        $result = $this->db->get('data_permintaan_barang u');
        return $result->result_array();
    }

    public function ambilNomor(){

        $id=date('m-Y');

        $this->db->select('*');
        $this->db->order_by("dob_code", "desc");
        $this->db->like('dob_date', $id, 'before');
        $result = $this->db->get('data_order_barang');
        return $result->result_array();
        
    }

    public  function detailBarangKeluar(){

        $idpb=$this->input->post('issued');

        $this->db->select('u.*,p.*,o.*, sr.*, kat.*');
        $this->db->join('mst_armada kat','u.bk_arm = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.bk_emp = sr.emp_id', 'right');
        $this->db->join('data_barang_keluar_detail p','u.bk_id = p.id_bk', 'right');
        $this->db->join('mst_inv_item o','p.bki_item = o.ii_id', 'right');
        $this->db->where('u.bk_id',$idpb);
        $result = $this->db->get('data_barang_keluar u');
        return $result->result_array();
    }

    public function issuedAll(){

        $this->db->select('u.*,p.*, sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dpb_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dpb_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dpb_status = p.s_id', 'right');
        $this->db->where('u.dpb_code IS NOT NULL',NULL, FALSE);
        $result = $this->db->get('data_permintaan_barang u');
        return $result->result_array();
    }

   	public	function itemIssued(){

    	$this->db->select('u.*,p.*,o.*, sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dpb_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dpb_emp_id = sr.emp_id', 'right');
        $this->db->join('data_permintaan_barang_item p','u.dpb_code = p.dpbi_dpb_id', 'right');
        $this->db->join('mst_inv_item o','p.dpbi_item_id = o.ii_id', 'right');
        $result = $this->db->get('data_permintaan_barang u');
        return $result->result_array();
    }
}