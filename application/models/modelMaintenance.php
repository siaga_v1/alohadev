<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelMaintenance extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilArmada(){

        $this->db->select('a.*,b.*,c.*');
        $this->db->join('mst_inv_merk b','a.arm_jenis_code = b.im_id','left');
        $this->db->join('mst_employee c','a.arm_emp_id = c.emp_id','left');
        $result = $this->db->get('mst_armada a');
        return $result->result_array();
        
    }  

    public function ambilMaintance(){

        $this->db->select('a.*,b.*,c.*');
        $this->db->join('mst_employee b','a.dama_emp_id = b.emp_id','left');
        $this->db->join('mst_armada c','a.dama_arm_id = c.arm_id','left');
        $result = $this->db->get('data_maintance a');
        return $result->result_array();
        
    } 

    public function ambilDriver(){

    $this->db->select('u.*, kat.*');
    $this->db->join('mst_kat_emp kat','u.emp_kategori = kat.kat_emp_id', 'right');
    $this->db->where('u.emp_aktif',1);
    $this->db->where('u.emp_kategori',6);
    $result = $this->db->get('mst_employee u');
    return $result->result_array();
    }

    public function tambahMaintenance()
    {
        $nt=$this->input->post('idmaintenance');
        $tp=$this->input->post('tanggal');
        $jp=$this->input->post('driver');
        $ai=$this->input->post('armada');
        $ei=$this->input->post('kategori');
        $t=$this->input->post('note');

        $data = array(
            'dama_id' => $nt,
            'dama_date' => $tp,
            'dama_emp_id' => $jp,
            'dama_arm_id' => $ai,
            'dama_kategori' => $ei,
            'dama_status' => 'Not Ready',
            'dama_note' => $t
            );
        $this->db->insert('data_maintance',$data);
    }
}