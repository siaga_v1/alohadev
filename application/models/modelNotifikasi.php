<?php if (!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelNotifikasi extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function notifikasiSPB()
    {

        $this->db->select('count(dpb_date) as cu,dpb_id,dpb_date');
        $this->db->limit(1);
        $this->db->where('dpb_status', '0');
        $this->db->order_by("dpb_id", "ASC");
        $result = $this->db->get('data_permintaan_barang');
        return $result->result();
    }

    public function notifikasiSTNK()
    {

        $today      = date('Y-m-d');
        $y      = date('Y');
        $m      = date('m');
        $yesterday  = date('d', strtotime($today . ' -14 Days'));

        $sql = "SELECT * 
                FROM fleets 
                WHERE 
                    active = 1 
                    AND stnk IS NOT NULL 
                    AND stnk <= '$today' ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function notifikasiPO()
    {

        $this->db->select('count(dob_date) as cu,dob_id,dob_date');
        $this->db->limit(1);
        $this->db->where('dob_status', '11');
        $result = $this->db->get('data_order_barang');
        return $result->num_rows();
    }

    public function notifikasiIssued()
    {

        $this->db->select('*');
        $this->db->where('dpb_status', '9');
        $result = $this->db->get('data_permintaan_barang');
        return $result->num_rows();
    }
}
