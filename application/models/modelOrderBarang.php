<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat uatu class
class modelOrderBarang extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilInventory(){

        $this->db->select('*');
        $this->db->where('ii_stock',0);
        $result = $this->db->get('mst_inv_item');
        return $result->result_array();
        
    }

    public function simpanOrderBarang($idp){
            
                date_default_timezone_set("Asia/Jakarta");
                $nowdate = date('ydmGis');
                $now=date('d-m-Y');
                                       
        $detail = $this->input->post('detail');
                $data['dob_code']           = $this->input->post('idspb');
                $data['dob_date']           = $this->input->post('tanggal');
                $data['dob_arm_id']         = $this->input->post('armada');
                $data['dob_emp_id']         = $this->input->post('user');
                $data['dob_status']         = "0";
                $data['dob_createtime']     = $now;
                $data['dob_createby']       = $idp;
                
        $this->db->insert('data_order_barang',$data);
     
        foreach($detail as $details)
        {   $datad['dobi_dob_id']   = $this->input->post('idspb');
            $datad['dobi_item_id']  = $details['items'];
            $datad['dobi_count']    = $details['qty'];
            $datad['dobi_note']     = $details['note'];
        
            $this->db->insert('data_order_barang_item',$datad);
        }
        
    }   

    public  function listOrderBarang(){

        $this->db->select('u.*, p.*,sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dob_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dob_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dob_status = p.s_id', 'right');
        $this->db->where('u.dob_code IS NOT NULL',NULL, FALSE);
        $this->db->where('u.dob_status','0');
        $result = $this->db->get('data_order_barang u');
        return $result->result_array();
    }

    public  function detailOrderBarang(){

        $idpb=$_GET['id'];

        $this->db->select('u.*,p.*,o.*, sr.*, kat.*,');
        $this->db->join('mst_armada kat','u.dob_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dob_emp_id = sr.emp_id', 'right');
        $this->db->join('data_order_barang_item p','u.dob_code = p.dobi_dob_id', 'right');
        $this->db->join('mst_inv_item o','p.dobi_item_id = o.ii_id', 'right');
        $this->db->where('u.dob_code',$idpb);
        $result = $this->db->get('data_order_barang u');
        return $result->result_array();
    }

    public  function approveOrderBarang(){

        $idpb=$_GET['id'];

        $this->db->query("UPDATE data_order_barang SET dob_status = '9' WHERE dob_code = '$idpb'");
    }

    public  function listOrderBarangAll(){

        $this->db->select('u.*,p.*, sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dob_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dob_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dob_status = p.s_id', 'right');
        $this->db->where('u.dob_code IS NOT NULL',NULL, FALSE);
        $result = $this->db->get('data_order_barang u');
        return $result->result_array();
    }
}