<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelProduksi extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilArmada(){

        $this->db->select('a.*,b.*,c.*');
        $this->db->join('mst_inv_merk b','a.arm_jenis_code = b.im_id','left');
        $this->db->join('mst_employee c','a.arm_emp_id = c.emp_id','left');
        $where=" a.arm_jenis_code='6' or a.arm_jenis_code='5' ";
        $this->db->where('a.arm_status',1);
        $this->db->where($where);
        $this->db->where('a.arm_status',1);
        $result = $this->db->get('mst_armada a');
        return $result->result_array();
        
    }   

    public function ambilDriver(){

        $this->db->select('u.*, kat.*');
            $this->db->join('mst_kat_emp kat','u.emp_kategori = kat.kat_emp_id', 'right');
            $this->db->where('u.emp_aktif',1);
            $this->db->where('u.emp_kategori',"6");
            $result = $this->db->get('mst_employee u');
        return $result->result_array();
        
    }   
    
     public function listProduksi(){

        $this->db->select('p.*,e.*,a.*');
        $this->db->join('mst_armada a','p.arm_id = a.arm_id', 'left');
        $this->db->join('mst_employee e','p.emp_id = e.emp_id', 'left');
        $result = $this->db->get('data_produksi p');
        return $result->result_array();

    }

    public function simpanProduksi()
    {
        $nt=$this->input->post('no_tiket');
        $tp=$this->input->post('tanggal');
        $jp=$this->input->post('jam');
        $ai=$this->input->post('armada');
        $ei=$this->input->post('driver');
        $t=$this->input->post('ton');
        $pp=$this->input->post('project');
        $lp=$this->input->post('lokasi');

        $data = array(
            'nomor_tiket' => $nt,
            'tgl_produksi' => $tp,
            'jam_produksi' => $jp,
            'arm_id' => $ai,
            'emp_id' => $ei,
            'tonnage' => $t,
            'project' => $pp,
            'lokasi' => $lp
            );
        $this->db->insert('data_produksi',$data);
    }
}