<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelPurchase extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilItemPR(){

        $this->db->select('u.*,p.*,o.*, sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dpb_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dpb_emp_id = sr.emp_id', 'right');
        $this->db->join('data_permintaan_barang_item p','u.dpb_code = p.dpbi_dpb_id', 'right');
        $this->db->join('mst_inv_item o','p.dpbi_item_id = o.ii_id', 'right');
        $this->db->where('p.dpbi_status','1');
        $result = $this->db->get('data_permintaan_barang u');
        return $result->result_array();
        
    }  

    public  function detailPurchase(){

        $idpb=$_GET['id'];

        $this->db->select('u.*,p.*,o.*, sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dob_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dob_emp_id = sr.emp_id', 'right');
        $this->db->join('data_order_barang_item p','u.dob_code = p.dobi_dob_id', 'right');
        $this->db->join('mst_inv_item o','p.dobi_item_id = o.ii_id', 'right');
        $this->db->where('u.dob_code',$idpb);
        $result = $this->db->get('data_order_barang u');
        return $result->result_array();
    }

    public  function listOrderBarangApproved(){

        $this->db->select('u.*,p.*, sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dob_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dob_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dob_status = p.s_id', 'right');
        $this->db->where('u.dob_status','13');
        $result = $this->db->get('data_order_barang u');
        return $result->result_array();
    }  

    public  function approvePurchase(){

        $idpb=$this->input->post('idsob');
        $idpr=$this->input->post('idpr');
        
        $this->db->query("UPDATE data_order_barang SET dob_status = '16' WHERE dob_code = '$idpb'");
    }

    public  function listPurcaseAll(){

        $this->db->select('u.*,p.*, sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dob_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dob_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dob_status = p.s_id', 'right');
        $this->db->where('u.dob_code IS NOT NULL',NULL, FALSE);
        $result = $this->db->get('data_order_barang u');
        return $result->result_array();
    }
}