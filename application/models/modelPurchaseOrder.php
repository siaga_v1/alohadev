<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelPurchaseOrder extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilItemPR(){

        $this->db->select('u.*,p.*,o.*, sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dpb_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dpb_emp_id = sr.emp_id', 'right');
        $this->db->join('data_permintaan_barang_item p','u.dpb_code = p.dpbi_dpb_id', 'right');
        $this->db->join('mst_inv_item o','p.dpbi_item_id = o.ii_id', 'right');
        $this->db->where('p.dpbi_status','1');
        $result = $this->db->get('data_permintaan_barang u');
        return $result->result_array();
        
    }  

    public  function detailPOPrint($itle=null){

        //$idpb=$_GET['id'];

        $this->db->select('u.*,p.*,o.*,q.*');
        $this->db->join('data_pengadaan_item_detail p','p.dapeid_dape_code = u.dape_code', 'right');
        $this->db->join('mst_inv_item o','p.dapeid_item_id = o.ii_code', 'right');
        $this->db->join('mst_inv_vendor q','u.dape_vendor = q.iv_code', 'right');
        $this->db->where('u.dape_code',$itle);
        $result = $this->db->get('data_pengadaan u');
        return $result->result_array();
    }

    public function ambilNomor(){

        $id=$_GET['id'];;

        $this->db->select('*');
        $this->db->order_by("dape_code", "desc");
        $this->db->like('dape_code', $id, 'after');
        $result = $this->db->get('data_pengadaan');
        return $result->result_array();
        
    }


    public  function detailPurchase(){

        $idpb=$_GET['id'];

        $this->db->select('u.*,p.*,o.*, sr.*, kat.*,');
        $this->db->join('mst_armada kat','u.dob_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dob_emp_id = sr.emp_id', 'right');
        $this->db->join('data_order_barang_item p','u.dob_code = p.dobi_dob_id', 'right');
        $this->db->join('mst_inv_item o','p.dobi_item_id = o.ii_id', 'right');
        $this->db->where('u.dob_code',$idpb);
        $this->db->where('p.dobi_status IS NULL',null, false);
        $result = $this->db->get('data_order_barang u');
        return $result->result_array();
    }

    public  function listPurchaseOrder(){

        $this->db->select('u.*,p.*, sr.*, kat.*');
        $this->db->join('mst_armada kat','u.dob_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dob_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dob_status = p.s_id', 'right');
        $this->db->where('u.dob_status','16');
        $result = $this->db->get('data_order_barang u');
        return $result->result_array();
    }

    public function listPurchaseOrderBPN(){

        $this->db->select('u.*,p.*, sr.*, kat.*');
        $this->db->join('mst_armada kat','u.dob_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dob_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dob_status = p.s_id', 'right');
        $this->db->where('u.dob_status','12');
        $this->db->where('u.dob_wilayah','BPN');
        $result = $this->db->get('data_order_barang u');
        return $result->result_array();
    }
    public function listPurchaseOrderJKT(){

        $this->db->select('u.*,p.*, sr.*, kat.*');
        $this->db->join('mst_armada kat','u.dob_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dob_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dob_status = p.s_id', 'right');
        $this->db->where('u.dob_status','12');
        $this->db->where('u.dob_wilayah','JKT');
        $result = $this->db->get('data_order_barang u');
        return $result->result_array();
    }

    public  function listPurchaseOrderPrint(){

        $this->db->select('u.*,p.*, sr.*, kat.*');
        $this->db->join('mst_armada kat','u.dob_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dob_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dob_status = p.s_id', 'right');
        $this->db->where('u.dob_status','12');
        $result = $this->db->get('data_order_barang u');
        return $result->result_array();
    }   

    public  function approvePurchase(){

        $idpb=$_GET['id'];
        $idpr=$_GET['pr'];
        $this->db->query("UPDATE data_order_barang SET dob_status = '11', dob_purchase_id='$idpr' WHERE dob_code = '$idpb'");
    }

    public  function listPurchaseOrderaq(){

        $this->db->select('u.*,p.*, sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dob_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dob_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dob_status = p.s_id', 'right');
        $this->db->where('u.dob_stsus','11');
        $result = $this->db->get('data_order_barang u');
        return $result->result_array();
    }

    public function tambahPO(){

        date_default_timezone_set("Asia/Jakarta");
                $nowdate = date('ydmGis');
                $now=date('d-m-Y');
                                       
        $detail = $this->input->post('detail');
        $dat            = $this->input->post('idsob');
                $data['dape_code']           = $this->input->post('idpo');
                $data['dape_sob']            = $this->input->post('idsob');
                $data['dape_pr']             = $this->input->post('idpr');
                $data['dape_status']         = "12";
                $data['dape_vendor']         = $this->input->post('idsupplier');
                $data['dape_emp_id']         = $this->input->post('emp');
                $data['dape_createtime']     = $now;
                $data['dape_createby']       = $idp;
                
        $this->db->insert('data_pengadaan',$data);

        $this->db->query("UPDATE data_order_barang SET dob_status = '12' WHERE dob_code = '$dat'");
     
        foreach($detail as $details)
        {   $datad['dapeid_arm_id']       = $this->input->post('armid');
            $datad['dapeid_dape_code']    = $this->input->post('idpo');
            $datad['dapeid_price']         = $details['harga'];
            $datad['dapeid_item_id']      = $details['item'];
            $datad['dapeid_qty']          = $details['qty'];
            $datad['dapeid_note']         = $details['note'];
        
            $this->db->insert('data_pengadaan_item_detail',$datad);
        }
        

    }

    public function simpanWilayah()
    {

        $wilayah = $this->input->post('wilayah');
        $id= $this->input->post('idpr');
        $kombinasi = $id."/".$wilayah;
        $now=date('d-m-Y');

        $data = array(
               'dob_code'    => $kombinasi,
               'dob_wilayah'    => $wilayah,
               'dob_createtime' => $now,
               'dob_status' => '12'
            );

        $this->db->where('dob_code', $id);
        $this->db->update('data_order_barang', $data); 

        $d = array(
               'dobi_dob_id'    => $kombinasi,
            );

        $this->db->where('dobi_dob_id', $id);
        $this->db->update('data_order_barang_item', $d);
    }

    public function ambilSupplier(){

        $this->db->select('*');
        $result = $this->db->get('mst_inv_vendor');
        return $result->result_array();

    }
}