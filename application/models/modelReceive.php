<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelReceive extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilDO()
    {

        $this->db->select('a.*,b.*');
        $this->db->join('data_pegiriman_item b','a.dape_id = b.dape_id', 'left');
        $this->db->where('a.dape_status','1');
        $result = $this->db->get('data_pegiriman a');
        return $result->result_array();
        
    }  

    public function detailRO()
    {
    	$id=$_GET['id'];
        $this->db->select('a.*,b.*,c.*');
        $this->db->join('data_pengadaan_item_detail b','a.dapei_code = b.dapeid_dape_code', 'left');
        $this->db->join('data_pegiriman c','c.dape_id = a.dape_id', 'left');
        $this->db->where('c.dape_id',$id);
        $result = $this->db->get('data_pegiriman_item a');
        return $result->result_array(); 
    } 



     

    public function ambilNomor(){

        $now=date('m-Y');
        $this->db->select('*');
        $this->db->order_by("dapr_id", "desc");
        $this->db->like('dapr_date', $now, 'before');
        $result = $this->db->get('data_penerimaan');
        return $result->result_array();
        
    }

    public function simpanRO()
    {
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('ydmGis');
        $now=date('d-m-Y');

        $RONumber = $this->input->post('RONumber');

        $detail = $this->input->post('detail');
        $data['dapr_id']     = $this->input->post('RONumber');
        $data['dapr_date']   = $now;
        
        $this->db->insert('data_penerimaan',$data);
     
       // foreach($detail as $details)
        //{   
            //if(isset($details['cek'])){
            $dat=$this->input->post('DONumber');       
            $datad['dapr_id']            = $this->input->post('RONumber');
            $datad['daprid_do']          = $this->input->post('DONumber');
            $datad['daprid_dodate']      = $this->input->post('DODate');
        
            $this->db->insert('data_penerimaan_item_details',$datad);

            $this->db->query("UPDATE data_pegiriman SET dape_status = '2' WHERE dape_id = '$dat'");
        //}
        //}
        
    }
    //}
    
}