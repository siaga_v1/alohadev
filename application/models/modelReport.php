<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelReport extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilArmada(){

        $this->db->select('a.*,b.*,c.*,d.*');
        $this->db->join('data_barang_keluar_detail b','a.bk_id = b.id_bk','Inner');
        $this->db->join('mst_inv_item c','b.bki_item = c.ii_id','inner');
        $this->db->join('mst_armada d','a.bk_arm = d.arm_id','inner');
        $result = $this->db->get('data_barang_keluar a');
        return $result->result_array();
        
    }   
    
}