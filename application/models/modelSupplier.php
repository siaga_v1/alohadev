<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class modelSupplier extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilSupplier(){

        $this->db->select('*');
        $result = $this->db->get('mst_inv_vendor');
        return $result->result_array();
        
    }  

    public function simpanSupplier()
    {
        $is=$this->input->post('idSupplier');
        $ns=$this->input->post('namaSupplier');
        $a=$this->input->post('alamat');
        $kp=$this->input->post('kodepos');
        $ppn=$this->input->post('ppn');
        $cp=$this->input->post('contactPerson');
        $t=$this->input->post('telepon');
        $e=$this->input->post('email');

        $data = array(
            'iv_code' => $is,
            'iv_name' => $ns,
            'iv_email' => $e,
            'iv_phone' => $t,
            'iv_address' => $a,
            'iv_pos' => $kp,
            'iv_ppn' => $ppn,
            'iv_contact_person' => $cp
            );
        $this->db->insert('mst_inv_vendor',$data);
    }
     
    
}