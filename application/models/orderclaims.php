<?php if (!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class orderclaims extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getClaim()
    {
        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('orderclaims');
        return $result->result_array();
    }

    public function lastInput()
    {
        $this->db->select('*');
        //$this->db->like('code', 'OC57', 'after');
        $this->db->where('active', '1');
        $this->db->order_by('orderdate', 'desc');
        $result = $this->db->get('orders', 1);
        return $result->result_array();
    }

    function search_code($query)
    {
        $this->db->like('code', $query, 'both');
        $this->db->order_by('code', 'ASC');
        $this->db->limit(10);
        return $this->db->get('orders')->result();
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('orderclaims', 1);
        return $result->result_array();
    }

    public function Simpan()
    {
        $data['code']           = $this->input->post('code');
        $tgl                    = $this->input->post('orderdate');
        $jam                    = $this->input->post('orderdatetime');
        $data['claimdate']      = $tgl . " " . $jam;
        $data['frameno']        = $this->input->post('norangka');
        $data['modeltype']        = $this->input->post('modelarmada');
        $data['ordercode']      = $this->input->post('ordercode');
        $data['fleet']          = $this->input->post('fleet');
        $data['driver']         = $this->input->post('driver');
        $data['customer']       = $this->input->post('customer');
        $data['shippment']      = $this->input->post('shippment');
        $data['citieso']        = $this->input->post('origin');
        $data['citiesd']        = $this->input->post('destination');
        $data['fleettype']      = $this->input->post('fleettype');
        $data['claimdamage']    = $this->input->post('damageloc');
        $data['claimtype']      = $this->input->post('typeofdamage');
        $data['description']    = $this->input->post('cause');
        $data['cost']           = $this->input->post('cost');
        $data['costcompany']    = $this->input->post('costcompany');
        $data['costdriver']     = $this->input->post('costdriver');
        $data['active']         = 1;

        $this->db->insert('orderclaims', $data);
    }

    public function Delete($value = '')
    {
        $id = $_GET['id'];

        $data['active']     = 0;

        $this->db->where('id', $id);
        $this->db->update('customers', $data);
    }

    public function getOneOrder()
    {
        $data = $_POST['id'];

        $this->db->select('*');
        $this->db->where('code', $data);
        $result = $this->db->get('orders');

        return $result->result();
    }

    public function getOrderload()
    {
        $data = $_POST['id'];

        $this->db->select('*');
        $this->db->where('order_code', $data);
        $result = $this->db->get('orderload');

        return $result->result_array();
    }
}
