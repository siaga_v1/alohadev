<?php if (!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu clas
class orders extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getOrder($data = null)
    {

        //$this->db->select('a.*,b.name as n1,c.name as c2, d.name as cname, g.fleetplateno as fleet, h.name as driver');
        //$this->db->join('cities b','a.id_citieso = b.id', 'left');
        //$this->db->join('cities c','a.id_citiesd = c.id');
        //$this->db->join('customers d','a.id_customers = d.id', 'left');
        //$this->db->join('fleets g','a.id_fleets = g.id', 'left');
        //$this->db->join('drivers h','a.id_drivers = h.id', 'left');
        //$this->db->where('a.active', '1');
        //$result = $this->db->get('orders a',$rowperpage,$rowno);

        //return $result->result_array();

        $where = "";
        if ($this->input->post('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->post('fleetplateno'))) . "%' ";
        }
        if ($this->input->post('origin') != '') {
            $where .= " AND b.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('origin'))) . "%' ";
        }
        if ($this->input->post('destination') != '') {
            $where .= " AND c.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('destination'))) . "%' ";
        }
        if ($this->input->post('moda') != '') {
            $where .= " AND o.createby LIKE '%" . $this->db->escape_str(strtolower($this->input->post('moda'))) . "%' ";
        }

        if ($this->input->post('noVoucher') != '') {
            $where .= " AND o.noVoucher LIKE '%" . $this->db->escape_str(strtolower($this->input->post('noVoucher'))) . "%' ";
        }

        if ($this->input->post('type') != '') {
            $where .= " AND h.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('type'))) . "%' ";
        }

        if ($this->input->post('donumber') != '') {
            $where .= " AND i.noshippent LIKE '%" . $this->input->post('donumber') . "%' ";
        }
        if ($this->input->post('noka') != '') {
            $where .= " AND i.frameno LIKE '%" . $this->input->post('noka') . "%' ";
        }
        if ($this->input->post('customer') != '') {
            $where .= "AND d.name LIKE '%" . $this->input->post('customer') . "%' ";
        }

        if ($this->input->get('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->get('fleetplateno'))) . "%' ";
        }


        ///////


        if ($this->input->get('donumber') != '') {
            $where .= " AND i.noshippent LIKE '%" . $this->input->get('donumber') . "%' ";
        }
        if ($this->input->get('noka') != '') {
            $where .= " AND i.frameno LIKE '%" . $this->input->get('noka') . "%' ";
        }

        if ($this->input->get('origin') != '') {
            $where .= " AND b.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('origin'))) . "%' ";
        }
        if ($this->input->get('destination') != '') {
            $where .= " AND c.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('destination'))) . "%' ";
        }

        if ($this->input->get('Koordinator') != '') {
            $where .= " AND o.createby LIKE '%" . $this->db->escape_str(strtolower($this->input->get('Koordinator'))) . "%' ";
        }
        if ($this->input->get('noVoucher') != '') {
            $where .= " AND o.noVoucher  LIKE '%" . $this->db->escape_str(strtolower($this->input->get('noVoucher'))) . "%' ";
        }

        if ($this->input->get('customer') != '') {
            $where .= " AND d.name LIKE '%" . $this->input->get('customer') . "%' ";
        }

        if ($this->input->get('type') != '') {
            $where .= " AND h.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('type'))) . "%' ";
        }

        if ($this->input->post('start') > 0 && $this->input->post('end') > 0) {
            $where .= " AND o.orderdate BETWEEN '" . $this->input->post('start') . "' AND '" . $this->input->post('end') . "'";
        }

        if ($this->input->get('start') > 0 && $this->input->get('end') > 0) {
            $where .= " AND o.orderdate BETWEEN '" . $this->input->get('start') . "' AND '" . $this->input->get('end') . "'";
        }

        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }

        $sql = "
            SELECT
                * 
            FROM
                (
                SELECT
                    ROW_NUMBER ( ) OVER ( ORDER BY A.orderdate DESC ) AS RowNum,
                    A.* 
                FROM
                    (
                    SELECT
                        o.id,
                        o.code,
                        o.orderdate,
                        o.ritase,
                        o.prices,
                        o.allowances,
                        o.noVoucher,
                        o.shippment,
                        i.id as idol,
                        i.frameno,
                        i.noshippent,
                        i.unitprice,
                        i.address,
                        i.custcode,
                        i.[case],
                        i.irisno,
                        i.machineno,
                        i.type,
                        k.name as ccost,
                        k.nominal2,
                        i.color,
                        b.name AS n1,
                        c.name AS c2,
                        d.name AS cname,
                        g.fleetplateno AS fleet,
                        h.name AS driver,
                        o.updatedatetime AS updatedate,
                        COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,
                        COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,
                        COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,
                        COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,
                        COALESCE ( SUM ( o.prices ), 0 ) AS pricess 
                    FROM
                        orders o
                        LEFT JOIN orderload i ON o.code = i.order_code
                        LEFT JOIN cities b ON i.id_citieso = b.id
                        LEFT JOIN cities c ON i.id_citiesd = c.id
                        LEFT JOIN customers d ON o.id_customers = d.id
                        LEFT JOIN fleets g ON o.id_fleets = g.id
                        LEFT JOIN drivers h ON o.id_drivers = h.id
                        LEFT JOIN costcomponents k ON i.custcode = k.name 
                        
                    WHERE
                        o.active = 1
                        /*AND o.orderdate BETWEEN '2020-10-09 00:00:00' AND '2020-10-10 00:00:00'*/
                        $where
                        
                    GROUP BY
                        o.id,
                        o.code,
                        o.orderdate,
                        o.ritase,
                        o.prices,
                        o.allowances,
                        o.noVoucher,
                        i.address,
                        k.name,
                        k.nominal2,
                        i.custcode,
                        i.irisno,
                        i.noshippent,
                        i.machineno,
                        i.unitprice,
                        i.frameno,
                        i.type,
                        i.id,
                        i.[case],
                        i.color,
                        g.fleetplateno,
                        g.CODE,
                        g.fleetnumber,
                        d.NAME,
                        o.shippment,
                        b.name,
                        c.name,
                        h.name,
                        o.updatedatetime
                    ) A 
                ) B 
            WHERE
                B.id IS NOT NULL

            $paging ORDER BY
                    b.orderdate DESC
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function getOrderApprove($data = null)
    {

        //$this->db->select('a.*,b.name as n1,c.name as c2, d.name as cname, g.fleetplateno as fleet, h.name as driver');
        //$this->db->join('cities b','a.id_citieso = b.id', 'left');
        //$this->db->join('cities c','a.id_citiesd = c.id');
        //$this->db->join('customers d','a.id_customers = d.id', 'left');
        //$this->db->join('fleets g','a.id_fleets = g.id', 'left');
        //$this->db->join('drivers h','a.id_drivers = h.id', 'left');
        //$this->db->where('a.active', '1');
        //$result = $this->db->get('orders a',$rowperpage,$rowno);

        //return $result->result_array();

        $where = "";
        if ($this->input->post('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->post('fleetplateno'))) . "%' ";
        }
        if ($this->input->post('origin') != '') {
            $where .= " AND b.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('origin'))) . "%' ";
        }
        if ($this->input->post('destination') != '') {
            $where .= " AND c.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('destination'))) . "%' ";
        }
        if ($this->input->post('moda') != '') {
            $where .= " AND j.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('moda'))) . "%' ";
        }


        if ($this->input->post('type') != '') {
            $where .= " AND h.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('type'))) . "%' ";
        }

        if ($this->input->post('code') != '') {
            $where .= " AND o.code LIKE '%" . $this->input->post('code') . "%' ";
        }
        if ($this->input->post('noka') != '') {
            $where .= " AND i.frameno LIKE '%" . $this->input->post('noka') . "%' ";
        }
        if ($this->input->post('customer') != '') {
            $where .= "AND d.name LIKE '%" . $this->input->post('customer') . "%' ";
        }

        if ($this->input->get('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->get('fleetplateno'))) . "%' ";
        }


        ///////


        if ($this->input->get('code') != '') {
            $where .= " AND o.code LIKE '%" . $this->input->get('code') . "%' ";
        }
        if ($this->input->get('noka') != '') {
            $where .= " AND i.frameno LIKE '%" . $this->input->get('noka') . "%' ";
        }

        if ($this->input->get('origin') != '') {
            $where .= " AND b.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('origin'))) . "%' ";
        }
        if ($this->input->get('destination') != '') {
            $where .= " AND c.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('destination'))) . "%' ";
        }

        if ($this->input->get('moda') != '') {
            $where .= " AND j.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('moda'))) . "%' ";
        }

        if ($this->input->get('customer') != '') {
            $where .= " AND d.name LIKE '%" . $this->input->get('customer') . "%' ";
        }

        if ($this->input->get('type') != '') {
            $where .= " AND h.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('type'))) . "%' ";
        }

        if ($this->input->post('start') > 0 && $this->input->post('end') > 0) {
            $where .= " AND o.orderdate BETWEEN '" . $this->input->post('start') . "' AND '" . $this->input->post('end') . "'";
        }

        if ($this->input->get('start') > 0 && $this->input->get('end') > 0) {
            $where .= " AND o.orderdate BETWEEN '" . $this->input->get('start') . "' AND '" . $this->input->get('end') . "'";
        }

        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }

        $sql = "
            SELECT
                * 
            FROM
                (
                SELECT
                    ROW_NUMBER () OVER ( ORDER BY A.ritases DESC ) AS RowNum,
                    A.* 
                FROM
                    (
                    SELECT
            o.id,
            o.code,
            o.orderdate,
            o.ritase,
            o.prices,
            o.allowances,
            o.loadqty,
            i.qty,
            o.noVoucher,        
            f.feesaving,
            j.name as moda,
            b.name AS n1,
            c.name AS c2,
            d.name AS cname,
            g.fleetplateno AS fleet,
            h.name AS driver,
            COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,
            COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,
            COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,
            COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,
            COALESCE ( SUM ( o.prices ), 0 ) AS pricess 
        FROM
            orders o
            LEFT JOIN orderload i ON o.code = i.order_code
            LEFT JOIN cities b ON i.id_citieso = b.id
            LEFT JOIN cities c ON i.id_citiesd = c.id
            LEFT JOIN customers d ON o.id_customers = d.id
            LEFT JOIN routes f ON i.id_routes = f.id
            LEFT JOIN fleets g ON o.id_fleets = g.id
            LEFT JOIN fleettypes j ON g.id_fleettypes = j.id
            LEFT JOIN drivers h ON o.id_drivers = h.id 
        WHERE
            o.active = 1 
                        $where
            
        GROUP BY
            o.id,
            o.code,
            o.orderdate,
            o.loadqty,
            o.ritase,
            o.prices,
            o.allowances,
            f.feesaving,
            o.noVoucher,
            g.fleetplateno,
            g.CODE,
            g.fleetnumber,
            d.NAME,
            i.qty,
            j.name,
            b.name,
            c.name,
            h.name 
                        ) A 
                    ) B 
                WHERE
                    B.id IS NOT NULL
            $paging
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function getOrderReport($data = null)
    {

        $where = "";
        if ($this->input->post('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->post('fleetplateno'))) . "%' ";
        }
        if ($this->input->post('origin') != '') {
            $where .= " AND b.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('origin'))) . "%' ";
        }
        if ($this->input->post('destination') != '') {
            $where .= " AND c.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('destination'))) . "%' ";
        }
        if ($this->input->post('moda') != '') {
            $where .= " AND j.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('moda'))) . "%' ";
        }

        if ($this->input->post('noVoucher') != '') {
            $where .= " AND o.noVoucher LIKE '%" . $this->db->escape_str(strtolower($this->input->post('noVoucher'))) . "%' ";
        }

        if ($this->input->post('type') != '') {
            $where .= " AND h.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('type'))) . "%' ";
        }

        if ($this->input->post('donumber') != '') {
            $where .= " AND i.noshippent LIKE '%" . $this->input->post('donumber') . "%' ";
        }
        if ($this->input->post('customer') != '') {
            $where .= "AND d.name LIKE '%" . $this->input->post('customer') . "%' ";
        }

        if ($this->input->get('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->get('fleetplateno'))) . "%' ";
        }


        ///////


        if ($this->input->get('donumber') != '') {
            $where .= " AND i.noshippent LIKE '%" . $this->input->get('donumber') . "%' ";
        }

        if ($this->input->get('origin') != '') {
            $where .= " AND b.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('origin'))) . "%' ";
        }
        if ($this->input->get('destination') != '') {
            $where .= " AND c.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('destination'))) . "%' ";
        }

        if ($this->input->get('moda') != '') {
            $where .= " AND j.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('moda'))) . "%' ";
        }
        if ($this->input->get('noVoucher') != '') {
            $where .= " AND o.noVoucher  LIKE '%" . $this->db->escape_str(strtolower($this->input->get('noVoucher'))) . "%' ";
        }

        if ($this->input->get('customer') != '') {
            $where .= " AND d.name LIKE '%" . $this->input->get('customer') . "%' ";
        }

        if ($this->input->get('type') != '') {
            $where .= " AND h.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('type'))) . "%' ";
        }

        if ($this->input->post('start') > 0 && $this->input->post('end') > 0) {
            $where .= " AND o.[date] BETWEEN '" . $this->input->post('start') . "' AND '" . $this->input->post('end') . "'";
        }

        if ($this->input->get('start') > 0 && $this->input->get('end') > 0) {
            $where .= " AND o.[date] BETWEEN '" . $this->input->get('start') . "' AND '" . $this->input->get('end') . "'";
        }

        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }

        $sql = "SELECT
            * 
        FROM
        (
        SELECT
        ROW_NUMBER ( ) OVER ( ORDER BY A.ritases DESC ) AS RowNum,
        A.* 
        FROM
        (
        SELECT
			o.code,
			o.id,
			o.orderdate,
			o.ritase,
			o.pricesadd,
			o.prices,
			o.allowances,
			OPS = ( SELECT SUM ( C.nominal ) FROM cost c WHERE c.order_id  = o.code AND c.coa = 113 ),
			LOLO = ( SELECT SUM ( C.nominal ) FROM cost c WHERE c.order_id  = o.code AND c.coa = 114 ),
			DEPO = ( SELECT SUM ( C.nominal ) FROM cost c WHERE c.order_id  = o.code AND c.coa = 115 ),
			PARKIR = ( SELECT SUM ( C.nominal ) FROM cost c WHERE c.order_id  = o.code AND c.coa = 116 ),
			KAWALAN = ( SELECT SUM ( C.nominal ) FROM cost c WHERE c.order_id  = o.code AND c.coa = 117 ),
			STORAGE = ( SELECT SUM ( C.nominal ) FROM cost c WHERE c.order_id  = o.code AND c.coa = 118 ),
			RC = ( SELECT SUM ( C.nominal ) FROM cost c WHERE c.order_id  = o.code AND c.coa = 119 ),
			SEAL = ( SELECT SUM ( C.nominal ) FROM cost c WHERE c.order_id  = o.code AND c.coa = 120 ),
			ALIHKAPAL = ( SELECT SUM ( C.nominal ) FROM cost c WHERE c.order_id  = o.code AND c.coa = 121 ),
			CUCI = ( SELECT SUM ( C.nominal ) FROM cost c WHERE c.order_id  = o.code AND c.coa = 122 ),
			SP2 = ( SELECT SUM ( C.nominal ) FROM cost c WHERE c.order_id  = o.code AND c.coa = 123 ),
			h.name AS driver,
			c.name AS c2,
			b.name AS n1,
			d.name AS cname,
			i.unitprice,
			i.qty,
			g.fleetplateno,
			i.frameno,
			o.komSupir,
			o.tabSupir,
			j.name,
			COALESCE ( SUM ( o.ritase ), 0 ) AS ritases 
        FROM
        orders o
        LEFT JOIN orderload i ON o.code = i.order_code
        LEFT JOIN cities b ON i.id_citieso = b.id
        LEFT JOIN cities c ON i.id_citiesd = c.id
        LEFT JOIN customers d ON o.id_customers = d.id
        LEFT JOIN routes f ON i.id_routes = f.id
        LEFT JOIN fleets g ON o.id_fleets = g.id
        LEFT JOIN fleettypes j ON o.id_ordertypes = j.id
        LEFT JOIN drivers h ON o.id_drivers = h.id 
        WHERE
        o.active = 1 
        $where
        GROUP BY
			o.code,
			o.id,
			j.name,
			o.orderdate,
			o.ritase,
			o.pricesadd,
			o.prices,
			o.allowances,
			h.name,
			c.name,
			b.name,
			d.name,
			i.unitprice,
			i.qty,
			i.frameno,
			o.komSupir,
			o.tabSupir,
			g.fleetplateno
        ) A 
        ) B 
        WHERE
        B.id IS NOT NULL
        $paging

        ORDER BY
        b.orderdate
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function getOrderInput($data = null)
    {

        //$this->db->select('a.*,b.name as n1,c.name as c2, d.name as cname, g.fleetplateno as fleet, h.name as driver');
        //$this->db->join('cities b','a.id_citieso = b.id', 'left');
        //$this->db->join('cities c','a.id_citiesd = c.id');
        //$this->db->join('customers d','a.id_customers = d.id', 'left');
        //$this->db->join('fleets g','a.id_fleets = g.id', 'left');
        //$this->db->join('drivers h','a.id_drivers = h.id', 'left');
        //$this->db->where('a.active', '1');
        //$result = $this->db->get('orders a',$rowperpage,$rowno);

        //return $result->result_array();

        $where = "";
        if ($this->input->post('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->post('fleetplateno'))) . "%' ";
        }
        if ($this->input->post('origin') != '') {
            $where .= " AND b.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('origin'))) . "%' ";
        }
        if ($this->input->post('destination') != '') {
            $where .= " AND c.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('destination'))) . "%' ";
        }
        if ($this->input->post('moda') != '') {
            $where .= " AND j.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('moda'))) . "%' ";
        }

        if ($this->input->post('noVoucher') != '') {
            $where .= " AND o.noVoucher LIKE '%" . $this->db->escape_str(strtolower($this->input->post('noVoucher'))) . "%' ";
        }

        if ($this->input->post('type') != '') {
            $where .= " AND h.name LIKE '%" . $this->db->escape_str(strtolower($this->input->post('type'))) . "%' ";
        }

        if ($this->input->post('donumber') != '') {
            $where .= " AND i.noshippent LIKE '%" . $this->input->post('donumber') . "%' ";
        }
        if ($this->input->post('customer') != '') {
            $where .= "AND d.name LIKE '%" . $this->input->post('customer') . "%' ";
        }

        if ($this->input->get('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%" . $this->db->escape_str(strtolower($this->input->get('fleetplateno'))) . "%' ";
        }


        ///////


        if ($this->input->get('donumber') != '') {
            $where .= " AND i.noshippent LIKE '%" . $this->input->get('donumber') . "%' ";
        }

        if ($this->input->get('origin') != '') {
            $where .= " AND b.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('origin'))) . "%' ";
        }
        if ($this->input->get('destination') != '') {
            $where .= " AND c.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('destination'))) . "%' ";
        }

        if ($this->input->get('moda') != '') {
            $where .= " AND j.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('moda'))) . "%' ";
        }
        if ($this->input->get('noVoucher') != '') {
            $where .= " AND o.noVoucher  LIKE '%" . $this->db->escape_str(strtolower($this->input->get('noVoucher'))) . "%' ";
        }

        if ($this->input->get('customer') != '') {
            $where .= " AND d.name LIKE '%" . $this->input->get('customer') . "%' ";
        }

        if ($this->input->get('type') != '') {
            $where .= " AND h.name LIKE '%" . $this->db->escape_str(strtolower($this->input->get('type'))) . "%' ";
        }

        if ($this->input->post('start') > 0 && $this->input->post('end') > 0) {
            $where .= " AND o.[date] BETWEEN '" . $this->input->post('start') . "' AND '" . $this->input->post('end') . "'";
        }

        if ($this->input->get('start') > 0 && $this->input->get('end') > 0) {
            $where .= " AND o.[date] BETWEEN '" . $this->input->get('start') . "' AND '" . $this->input->get('end') . "'";
        }

        $paging = "";
        if (isset($data['limit'])) {
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit * $page) - $limit) + 1 : 1;
            $last   = ($page > 1) ? ($limit * $page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }

        $sql = "
            
            SELECT
                * 
            FROM
                (
                SELECT
                    ROW_NUMBER () OVER ( ORDER BY A.ritases DESC ) AS RowNum,
                    A.* 
                FROM
                    (
                    SELECT
            o.id,
            o.code,
            o.orderdate,
            o.ritase,
            o.prices,
            o.allowances,
            o.loadqty,
            i.qty,
            o.noVoucher,        
            f.feesaving,
            j.name as moda,
            b.name AS n1,
            c.name AS c2,
            d.name AS cname,
            g.fleetplateno AS fleet,
            h.name AS driver,
            COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,
            COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,
            COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,
            COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,
            COALESCE ( SUM ( o.prices ), 0 ) AS pricess 
        FROM
            orders o
            LEFT JOIN orderload i ON o.code = i.order_code
            LEFT JOIN cities b ON i.id_citieso = b.id
            LEFT JOIN cities c ON i.id_citiesd = c.id
            LEFT JOIN customers d ON o.id_customers = d.id
            LEFT JOIN routes f ON i.id_routes = f.id
            LEFT JOIN fleets g ON o.id_fleets = g.id
            LEFT JOIN fleettypes j ON g.id_fleettypes = j.id
            LEFT JOIN drivers h ON o.id_drivers = h.id 
        WHERE
            o.active = 1 
                        $where
            
        GROUP BY
            o.id,
            o.code,
            o.orderdate,
            o.loadqty,
            o.ritase,
            o.prices,
            o.allowances,
            f.feesaving,
            o.noVoucher,
            g.fleetplateno,
            g.CODE,
            g.fleetnumber,
            d.NAME,
            i.qty,
            j.name,
            b.name,
            c.name,
            h.name 
                        ) A 
                    ) B 
                WHERE
                    B.id IS NOT NULL
            $paging
        ";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function getOrderSpot()
    {
        $y = date('Y');

        $today      = date('Y-m-d');
        if (date('D', strtotime($today . ' -1 Days')) == 'Sun') {
            $yesterday  = date('Y-m-d', strtotime($today . ' -2 Days'));
        } else {
            $yesterday  = date('Y-m-d', strtotime($today . ' -1 Days'));
        }


        $thismonday = date('Y-m-d', strtotime($today . ' this week'));
        $thissunday = date('Y-m-d', strtotime($today . ' this sunday'));

        $lastmonday = date('Y-m-d', strtotime($thismonday . ' -1 week'));
        $lastsunday = date('Y-m-d', strtotime($thissunday . ' -1 week'));

        $thismonth  = date('Y-m-01');
        $thismonthlast = date('Y-m-t');
        $lastmonth  = date('Y-m-01', strtotime($thismonth . ' -1 Months'));
        $lastmonthlast = date('Y-m-t', strtotime($lastmonth));

        $thism = date('m', strtotime($thismonth));
        $lastm = date('m', strtotime($lastmonth));

        $thisy = date('Y', strtotime($thismonth));
        $lastys = date('Y', strtotime($lastmonth));

        $thisyear   = date('Y-m-01');
        $lastyear   = date('Y-m-d', strtotime($thisyear . ' -1 years'));

        $lasty = date('Y', strtotime($lastyear));

        $this->db->select('a.*,b.name as n1,c.name as c2, d.name as cname, g.fleetplateno as fleet, h.name as driver');
        $this->db->join('cities b', 'a.id_citieso = b.id', 'left');
        $this->db->join('cities c', 'a.id_citiesd = c.id', 'left');
        $this->db->join('customers d', 'a.id_customers = d.id', 'left');
        $this->db->join('fleets g', 'a.id_fleets = g.id', 'left');
        $this->db->join('drivers h', 'a.id_drivers = h.id', 'left');
        $this->db->where('a.active', '2');
        // $this->db->where('a.orderdate >=', $thismonth);
        // $this->db->where('a.orderdate <=', $thismonthlast);
        $result = $this->db->get('orders a');

        return $result->result_array();
    }

    public function getOrderMonth()
    {
        date_default_timezone_set("Asia/Bangkok");

        $y = date('Y-m-d');

        $today = date('Y-m-d');
        $sql   = "SELECT
    a.*,
    b.name AS n1,
    c.name AS c2,
    d.name AS cname,
    g.fleetplateno AS fleet,
    h.name AS driver 
FROM
    orders a
    LEFT JOIN cities b ON a.id_citieso = b.id
    JOIN cities c ON a.id_citiesd = c.id
    LEFT JOIN customers d ON a.id_customers = d.id
    LEFT JOIN fleets g ON a.id_fleets = g.id
    LEFT JOIN drivers h ON a.id_drivers = h.id
WHERE
    a.[active] = 1
    AND ( a.[createdatetime] > '$today 00:00:00' OR a.[updatedatetime] > '$today 00:00:00' OR a.[date] = '$y' )";

        $result = $this->db->query($sql);

        return $result->result_array();
    }

    public function getOneOrder()
    {
        $data = $_POST['id'];

        $this->db->select('*');
        $this->db->where('id', $data);
        $result = $this->db->get('orders');

        return $result->result();
    }

    public function printSpot()
    {
        $data = $_GET['id'];

        $this->db->select('*');
        $this->db->where('id', $data);
        $this->db->where('active', '2');
        $result = $this->db->get('orders');

        return $result->result_array();
    }

    public function getOrderload()
    {
        $data = $_POST['id'];

        $this->db->select('*');
        $this->db->where('id', $data);
        $result = $this->db->get('orders');

        foreach ($result->result() as $roe) {
            $code = $roe->code;

            $this->db->select('*');
            $this->db->where('order_code', $code);
            $result = $this->db->get('orderload');

            return $result->result_array();
        }
    }

    public function getAdditionalCost()
    {
        $data = $_POST['id'];

        $this->db->select('*');
        $this->db->where('id', $data);
        $result = $this->db->get('orders');

        foreach ($result->result() as $roe) {
            $code = $roe->code;

            $this->db->select('a.*,b.parent as tipe');
            $this->db->join('chartofaccount b', 'a.coa = b.id');
            $this->db->where('a.order_id', $code);
            //$this->db->where('a.akun_id',NULL);
            $result = $this->db->get('cost a');

            return $result->result_array();
        }
    }

    public function adds()
    {
        $this->db->select('*');
        $this->db->where('active', '1');
        $this->db->where('type', '2');
        $result = $this->db->get('costcomponents');

        return $result->result_array();
    }

    public function addss()
    {
        $this->db->select('*');
        $this->db->where('active', '1');
        $this->db->where('parent', '111');
        $result = $this->db->get('chartofaccount');

        return $result->result_array();
    }

    public function editCustomer()
    {
        $id                 = $this->input->post('code');
        $data['name']       = $this->input->post('name');
        $data['address']    = $this->input->post('address');
        $data['phone']      = $this->input->post('phone');
        $data['pic_name']   = $this->input->post('pic_name');
        $data['email']      = $this->input->post('email');
        $data['nickname']   = $this->input->post('nickname');
        $data['term']       = $this->input->post('term');
        $data['ppn']        = $this->input->post('ppn');
        $data['active']     = 1;

        $this->db->where('code', $id);
        $this->db->update('customers', $data);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("code", "desc");
        $result = $this->db->get('orders', 1);
        return $result->result_array();
    }

    public function Simpan()
    {

        $user = $this->session->userdata('id');

        $frame      = $this->input->post('frameno');
        $machine    = $this->input->post('machineno');
        $color      = $this->input->post('color');
        $type       = $this->input->post('type');

        $desc      = $this->input->post('desc');
        $costadd      = $this->input->post('costadd');
        $nomine      = $this->input->post('nomine');

        $a = count($desc);
        $c = $a + 2;

        if ($a >= 1) {



            $or = array();
            $ujsadd2 = 0;
            for ($i = 2; $i < $c; $i++) {
                $ujsadd2 += $nomine[$i];
                $ad[$i] = array(
                    'order_id' => $this->input->post('code'),
                    'cost_id' => $costadd[$i],
                    'nominal' => $nomine[$i],
                    'createby' => $this->session->userdata('id')
                );
            }
            //print_r($test2);
            $this->db->insert_batch('cost', $ad);

            $data['code']           = $this->input->post('code');
            $tgl                    = $this->input->post('orderdate');
            $jam                    = $this->input->post('orderdatetime');
            $ujsawal                = $this->input->post('allowance'); //UANG JALAN 
            $data['orderdate']      = $tgl . " " . $jam;
            $data['id_customers']   = $this->input->post('id_customers');
            $data['description']    = $this->input->post('description');
            $data['id_fleets']      = $this->input->post('id_fleets');
            $data['id_drivers']     = $this->input->post('id_drivers');
            $data['id_drivers2']     = $this->input->post('id_drivers2');
            $data['shipment']       = $this->input->post('id_shippment');
            $data['id_citieso']     = $this->input->post('id_citieso');
            $data['id_citiesd']     = $this->input->post('id_citiesd');
            $data['id_ordertypes']  = $this->input->post('id_ordertypes');
            $data['loadqty']        = $this->input->post('loadqty');
            $data['id_routes']      = $this->input->post('id_routes');
            $data['prices']         = $ujsawal + $ujsadd2; //UANG JALAN SUPIR   
            $data['allowances']      = $this->input->post('price'); // UANG UNIT
            $data['createby']       = $this->session->userdata('id');
            $data['active']         = 1;
            $data['ritase']         = 1;

            $this->db->insert('orders', $data);

            $orde = array();
            for ($i = 0; $i < $data['loadqty']; $i++) {
                $dt[$i] = array(
                    'order_code' => $this->input->post('code'),
                    'frameno' => $frame[$i],
                    'machineno' => $machine[$i],
                    'color' => $color[$i],
                    'type' => $type[$i]
                );
            }

            $this->db->insert_batch('orderload', $dt);



            $this->db->select('*');
            $this->db->where('id', $user);
            $result = $this->db->get('users');


            foreach ($result->result_array() as $row) {
                $iddc = $row['id'];
                $a    = $row['nominal'];
                $b    = $ujsawal + $ujsadd2;

                $c = $a - $b;

                $dat['nominal']     = $c;

                $this->db->where('id', $iddc);
                $this->db->update('users', $dat);
            }
        } else {
            $data['code']           = $this->input->post('code');
            $tgl                    = $this->input->post('orderdate');
            $jam                    = $this->input->post('orderdatetime');
            $ujsawal                = $this->input->post('allowance'); //UANG JALAN 
            $data['orderdate']      = $tgl . " " . $jam;
            $data['id_customers']   = $this->input->post('id_customers');
            $data['description']    = $this->input->post('description');
            $data['id_fleets']      = $this->input->post('id_fleets');
            $data['id_drivers']     = $this->input->post('id_drivers');
            $data['id_drivers2']     = $this->input->post('id_drivers2');
            $data['shipment']       = $this->input->post('id_shippment');
            $data['id_citieso']     = $this->input->post('id_citieso');
            $data['id_citiesd']     = $this->input->post('id_citiesd');
            $data['id_ordertypes']  = $this->input->post('id_ordertypes');
            $data['loadqty']        = $this->input->post('loadqty');
            $data['id_routes']      = $this->input->post('id_routes');
            $data['prices']         = $ujsawal; //UANG JALAN SUPIR   
            $data['allowances']      = $this->input->post('price'); // UANG UNIT

            $data['createby']       = $this->session->userdata('id');
            $data['active']         = 1;
            $data['ritase']         = 1;

            $this->db->insert('orders', $data);

            $orde = array();
            for ($i = 0; $i < $data['loadqty']; $i++) {
                $dt[$i] = array(
                    'order_code' => $this->input->post('code'),
                    'frameno' => $frame[$i],
                    'machineno' => $machine[$i],
                    'color' => $color[$i],
                    'type' => $type[$i]
                );
            }

            $this->db->insert_batch('orderload', $dt);



            $this->db->select('*');
            $this->db->where('id', $user);
            $result = $this->db->get('users');


            foreach ($result->result_array() as $row) {
                $iddc = $row['id'];
                $a    = $row['nominal'];
                $b    = $ujsawal;

                $c = $a - $b;

                $dat['nominal']     = $c;

                $this->db->where('id', $iddc);
                $this->db->update('users', $dat);
            }
        }
    }

    public function simpanPareto_()
    {

        $user = $this->session->userdata('id');

        $frame   = $this->input->post('frameno');
        $machine    = $this->input->post('machineno');
        $color      = $this->input->post('color');
        $type       = $this->input->post('type');
        $bongkar    = $this->input->post('p_scnt');
        $ujs        = $this->input->post('ujs');
        $sales      = $this->input->post('sales');

        $desc      = $this->input->post('desc');
        $costadd      = $this->input->post('costadd');
        $nomine      = $this->input->post('nomine');

        $aeee = count($desc);
        $beee = count($bongkar);
        $c = $aeee + 2;
        $d = $beee + 2;


        //KALAU YANG DI ISI ADDITIONAL
        if ($aeee >= 1) {

            $or = array();
            $ujsadd2 = 0;

            for ($i = 2; $i < $c; $i++) {
                $ujsadd2 += $nomine[$i];
                $ad[$i] = array(
                    'order_id' => $this->input->post('code'),
                    'cost_id' => $costadd[$i],
                    'nominal' => $nomine[$i],
                    'createby' => $this->session->userdata('id')
                );
            }

            $this->db->insert_batch('cost', $ad);


            $data['code']           = $this->input->post('code');
            $tgl                    = $this->input->post('orderdate');
            $jam                    = $this->input->post('orderdatetime');
            $ujsawal                = $this->input->post('allowance'); //UANG JALAN 
            $salesawal              = $this->input->post('price'); //UNIT
            $data['orderdate']      = $tgl . " " . $jam;
            $data['id_customers']   = $this->input->post('id_customers');
            $data['description']    = $this->input->post('description');
            $data['id_fleets']      = $this->input->post('id_fleets');
            $data['id_drivers']     = $this->input->post('id_drivers');
            $data['id_drivers2']    = $this->input->post('id_drivers2');
            $data['shipment']       = $this->input->post('id_shippment');
            $data['id_citieso']     = $this->input->post('id_citieso');
            $data['id_citiesd']     = $this->input->post('id_citiesd');
            $data['id_ordertypes']  = $this->input->post('id_ordertypes');
            $data['loadqty']        = $this->input->post('loadqty');
            $data['id_routes']      = $this->input->post('id_routes');
            $data['prices']         = $ujsawal + $ujsadd2; //UANG JALAN SUPIR   
            $data['allowances']     = $salesawal; // UANG UNIT


            $data['createby']       = $this->session->userdata('id');
            $data['active']         = 1;
            $data['ritase']         = 1;

            $this->db->insert('orders', $data);

            $orde = array();
            for ($i = 0; $i < $data['loadqty']; $i++) {
                $dt[$i] = array(
                    'order_code' => $this->input->post('code'),
                    'frameno' => $frame[$i],
                    'machineno' => $machine[$i],
                    'color' => $color[$i],
                    'type' => $type[$i]
                );
            }

            $this->db->insert_batch('orderload', $dt);

            $this->db->select('*');
            $this->db->where('id', $user);
            $result = $this->db->get('users');


            foreach ($result->result_array() as $row) {
                $iddc = $row['id'];
                $a    = $row['nominal'];
                $b    = $ujsawal + $ujsadd2;

                $c = $a - $b;

                $dat['nominal']     = $c;

                $this->db->where('id', $iddc);
                $this->db->update('users', $dat);
            }
        }

        //KALAU YANG DI ISI BONGKARAN
        if ($beee >= 1) {

            $ord = array();
            $salesadd = 0;
            $ujsadd = 0;
            for ($i = 2; $i < $d; $i++) {
                $salesadd += $sales[$i];
                $ujsadd += $ujs[$i];
                $add[$i] = array(
                    'order_id' => $this->input->post('code'),
                    'cost_id' => $bongkar[$i],
                    'nominal' => $ujs[$i],
                    'nominal2' => $sales[$i],
                    'createby' => $this->session->userdata('id')
                );
            }
            $this->db->insert_batch('cost', $add);

            $data['code']           = $this->input->post('code');
            $tgl                    = $this->input->post('orderdate');
            $jam                    = $this->input->post('orderdatetime');
            $ujsawal                = $this->input->post('allowance'); //UANG JALAN 
            $salesawal              = $this->input->post('price'); //UNIT
            $data['orderdate']      = $tgl . " " . $jam;
            $data['id_customers']   = $this->input->post('id_customers');
            $data['description']    = $this->input->post('description');
            $data['id_fleets']      = $this->input->post('id_fleets');
            $data['id_drivers']     = $this->input->post('id_drivers');
            $data['id_drivers2']    = $this->input->post('id_drivers2');
            $data['shipment']       = $this->input->post('id_shippment');
            $data['id_citieso']     = $this->input->post('id_citieso');
            $data['id_citiesd']     = $this->input->post('id_citiesd');
            $data['id_ordertypes']  = $this->input->post('id_ordertypes');
            $data['loadqty']        = $this->input->post('loadqty');
            $data['id_routes']      = $this->input->post('id_routes');
            $data['prices']         = $ujsawal + $ujsadd; //UANG JALAN SUPIR   
            $data['allowances']     = $salesawal + $salesadd; // UANG UNIT


            $data['createby']       = $this->session->userdata('id');
            $data['active']         = 1;
            $data['ritase']         = 1;

            $this->db->insert('orders', $data);

            $orde = array();
            for ($i = 0; $i < $data['loadqty']; $i++) {
                $dt[$i] = array(
                    'order_code' => $this->input->post('code'),
                    'frameno' => $frame[$i],
                    'machineno' => $machine[$i],
                    'color' => $color[$i],
                    'type' => $type[$i]
                );
            }

            $this->db->insert_batch('orderload', $dt);

            $this->db->select('*');
            $this->db->where('id', $user);
            $result = $this->db->get('users');


            foreach ($result->result_array() as $row) {
                $iddc = $row['id'];
                $a    = $row['nominal'];
                $b    = $ujsawal + $ujsadd;

                $c = $a - $b;

                $dat['nominal']     = $c;

                $this->db->where('id', $iddc);
                $this->db->update('users', $dat);
            }
        }
        if ($beee >= 1  && $aeee >= 1) {

            $ord = array();
            $salesadd = 0;
            $ujsadd = 0;
            for ($i = 2; $i < $d; $i++) {
                $salesadd += $sales[$i];
                $ujsadd += $ujs[$i];
                $add[$i] = array(
                    'order_id' => $this->input->post('code'),
                    'cost_id' => $bongkar[$i],
                    'nominal' => $ujs[$i],
                    'nominal2' => $sales[$i],
                    'createby' => $this->session->userdata('id')
                );
            }
            $this->db->insert_batch('cost', $add);

            $or = array();
            $ujsadd2 = 0;
            for ($i = 2; $i < $c; $i++) {
                $ujsadd2 += $nomine[$i];
                $ad[$i] = array(
                    'order_id' => $this->input->post('code'),
                    'cost_id' => $costadd[$i],
                    'nominal' => $nomine[$i],
                    'createby' => $this->session->userdata('id')
                );
            }

            $this->db->insert_batch('cost', $ad);


            $data['code']           = $this->input->post('code');
            $tgl                    = $this->input->post('orderdate');
            $jam                    = $this->input->post('orderdatetime');
            $ujsawal                = $this->input->post('allowance'); //UANG JALAN 
            $salesawal              = $this->input->post('price'); //UNIT
            $data['orderdate']      = $tgl . " " . $jam;
            $data['id_customers']   = $this->input->post('id_customers');
            $data['description']    = $this->input->post('description');
            $data['id_fleets']      = $this->input->post('id_fleets');
            $data['id_drivers']     = $this->input->post('id_drivers');
            $data['id_drivers2']    = $this->input->post('id_drivers2');
            $data['shipment']       = $this->input->post('id_shippment');
            $data['id_citieso']     = $this->input->post('id_citieso');
            $data['id_citiesd']     = $this->input->post('id_citiesd');
            $data['id_ordertypes']  = $this->input->post('id_ordertypes');
            $data['loadqty']        = $this->input->post('loadqty');
            $data['id_routes']      = $this->input->post('id_routes');
            $data['prices']         = $ujsawal + $ujsadd2   + $ujsadd; //UANG JALAN SUPIR   
            $data['allowances']     = $salesawal + $salesadd; // UANG UNIT


            $data['createby']       = $this->session->userdata('id');
            $data['active']         = 1;
            $data['ritase']         = 1;

            $this->db->insert('orders', $data);

            $orde = array();
            for ($i = 0; $i < $data['loadqty']; $i++) {
                $dt[$i] = array(
                    'order_code' => $this->input->post('code'),
                    'frameno' => $frame[$i],
                    'machineno' => $machine[$i],
                    'color' => $color[$i],
                    'type' => $type[$i]
                );
            }

            $this->db->insert_batch('orderload', $dt);

            $this->db->select('*');
            $this->db->where('id', $user);
            $result = $this->db->get('users');


            foreach ($result->result_array() as $row) {
                $iddc = $row['id'];
                $a    = $row['nominal'];
                $b    = $ujsawal + $ujsadd2   + $ujsadd;

                $c = $a - $b;

                $dat['nominal']     = $c;

                $this->db->where('id', $iddc);
                $this->db->update('users', $dat);
            }
        }
        if ($beee <= 0  && $aeee <= 0) {


            $data['code']           = $this->input->post('code');
            $tgl                    = $this->input->post('orderdate');
            $jam                    = $this->input->post('orderdatetime');
            $ujsawal                = $this->input->post('allowance'); //UANG JALAN 
            $salesawal              = $this->input->post('price'); //UNIT
            $data['orderdate']      = $tgl . " " . $jam;
            $data['id_customers']   = $this->input->post('id_customers');
            $data['description']    = $this->input->post('description');
            $data['id_fleets']      = $this->input->post('id_fleets');
            $data['id_drivers']     = $this->input->post('id_drivers');
            $data['id_drivers2']    = $this->input->post('id_drivers2');
            $data['shipment']       = $this->input->post('id_shippment');
            $data['id_citieso']     = $this->input->post('id_citieso');
            $data['id_citiesd']     = $this->input->post('id_citiesd');
            $data['id_ordertypes']  = $this->input->post('id_ordertypes');
            $data['loadqty']        = $this->input->post('loadqty');
            $data['id_routes']      = $this->input->post('id_routes');
            $data['prices']         = $ujsawal; //UANG JALAN SUPIR   
            $data['allowances']     = $salesawal; // UANG UNIT


            $data['createby']       = $this->session->userdata('id');
            $data['active']         = 1;
            $data['ritase']         = 1;

            $this->db->insert('orders', $data);

            $orde = array();
            for ($i = 0; $i < $data['loadqty']; $i++) {
                $dt[$i] = array(
                    'order_code' => $this->input->post('code'),
                    'frameno' => $frame[$i],
                    'machineno' => $machine[$i],
                    'color' => $color[$i],
                    'type' => $type[$i]
                );
            }

            $this->db->insert_batch('orderload', $dt);

            $this->db->select('*');
            $this->db->where('id', $user);
            $result = $this->db->get('users');


            foreach ($result->result_array() as $row) {
                $iddc = $row['id'];
                $a    = $row['nominal'];
                $b    = $ujsawal;

                $c = $a - $b;

                $dat['nominal']     = $c;

                $this->db->where('id', $iddc);
                $this->db->update('users', $dat);
            }
        }
    }

    public function Delete()
    {
        $id = $_POST['id'];

        $data['active']     = 0;

        $this->db->where('id', $id);
        $this->db->update('orders', $data);

        $this->db->select('*');
        $this->db->where('id', $id);
        $result = $this->db->get('orders');


        foreach ($result->result_array() as $row) {
            $a    = $row['code'];

            $dat['active']     = 0;

            $this->db->where('order_id', $a);
            $this->db->update('cost', $dat);
        }
    }

    public function editOrders()
    {
        # code...
        $dcode           = $this->input->post('code');
        $tgl                    = $this->input->post('orderdates');
        $jam                    = $this->input->post('orderdatetime');
        $data['orderdate']      = $tgl . " " . $jam;
        $data['id_customers']   = $this->input->post('id_customers');
        $data['description']    = $this->input->post('description');
        $data['id_fleets']      = $this->input->post('id_fleets');
        $data['id_drivers']     = $this->input->post('id_drivers');
        $data['id_drivers2']    = $this->input->post('id_drivers2');
        $data['shipment']       = $this->input->post('id_shippment');
        $data['id_citieso']     = $this->input->post('id_citio');
        $data['id_citiesd']     = $this->input->post('id_citiesd');
        $data['id_ordertypes']  = $this->input->post('id_ordertypes');
        $data['loadqty']        = $this->input->post('loadqty');
        $data['id_routes']      = $this->input->post('id_routes');
        $data['prices']         = $this->input->post('allowance'); // UANG JALAN
        $data['allowances']     = $this->input->post('price'); // UANG UNIT
        date_default_timezone_set("Asia/Bangkok");
        $tanggalupdate = date("Y-m-d H:i:s");

        $data['updateby']             = $this->session->userdata('id');
        $data['updatedatetime']       = $tanggalupdate;
        $data['active']         = 1;
        $data['ritase']         = 1;

        $this->db->where('code', $dcode);
        $this->db->update('orders', $data);
        //$this->db->insert('orders',$data);
        $user = $this->session->userdata('id');

        $id      = $this->input->post('id');
        $frame      = $this->input->post('frameno');
        $machine    = $this->input->post('machineno');
        $color      = $this->input->post('color');
        $type       = $this->input->post('type');

        $orde = array();
        for ($i = 0; $i < $data['loadqty']; $i++) {
            $dt[$i] = array(
                'id' => $id[$i],
                'frameno' => $frame[$i],
                'machineno' => $machine[$i],
                'color' => $color[$i],
                'type' => $type[$i]
            );
        }

        //$this->db->insert_batch('orderload', $dt);
        $this->db->update_batch('orderload', $dt, 'id');
    }



    public function editOrderspot()
    {

        $user = $this->session->userdata('id');

        $frame      = $this->input->post('frameno');
        $machine    = $this->input->post('machineno');
        $color      = $this->input->post('color');
        $type       = $this->input->post('type');
        $origin      = $this->input->post('id_citiesomodal');
        $destination = $this->input->post('id_citiesdmodal');
        $loadqty     = $this->input->post('jsqtymodal');
        $routes      = $this->input->post('id_routesmodal');
        $unitprice    = $this->input->post('jsunitmodal');

        $bongkar    = $this->input->post('p_scntmodal');
        $ujs        = $this->input->post('ujsmodal');
        $sales      = $this->input->post('salesmodal');
        $kod        = $this->input->post('kod');

        $fleetsmodal      = $this->input->post('id_fleetsmodal');
        $driversmodal   = $this->input->post('costaddmodal');
        $casemodal    = $this->input->post('casemodal');
        $ordertypemodal    = $this->input->post('id_ordertypesmodal');
        $kode    = $this->input->post('kode');


        $bongkarcount = count($bongkar);
        $ceee = count($kod);

        $addcostcount = count($fleetsmodal);
        $deee = count($kode);

        $addroutes = count($origin);


        print_r($ceee);
        print_r($addcostcount);
        print_r($bongkarcount);
        print_r($deee);




        if ($bongkarcount >= 0) {
            # code...
            $salesadd = 0;
            $ujsadd = 0;
            for ($i = 0; $i < $bongkarcount; $i++) {

                $salesadd += $sales[$i];
                $ujsadd += $ujs[$i];
                $add = array(
                    'order_id' => $this->input->post('code'),
                    'cost_id' => $bongkar[$i],
                    'nominal' => $ujs[$i],
                    'nominal2' => $sales[$i],
                    'createby' => $this->session->userdata('id')
                );

                $this->db->insert('cost', $add);
            }
        }

        if ($ceee >= 0) {
            for ($i = 0; $i < $ceee; $i++) {
                $id = $kod[$i];

                $this->db->where('id', $id);
                $this->db->delete('cost');
            }
        }

        if ($addcostcount >= 0) {
            # code...
            $or = array();
            $ujsadd2 = 0;

            for ($i = 0; $i < $addcostcount; $i++) {

                $ujsadd2 += $nomine[$i];
                $ad = array(
                    'order_id' => $this->input->post('code'),
                    'cost_id' => $costadd[$i],
                    'nominal' => $nomine[$i],
                    'createby' => $this->session->userdata('id')
                );
                $this->db->insert('cost', $ad);
            }
        }

        if ($deee >= 0) {
            for ($i = 0; $i < $deee; $i++) {
                $id = $kode[$i];

                $this->db->where('id', $id);
                $this->db->delete('cost');
            }
        }

        //ORDER
        $dcode                  = $this->input->post('code');
        $tgl                    = $this->input->post('orderdates');
        $jam                    = $this->input->post('orderdatetime');

        $salesawal              = $this->input->post('pricmodal'); //UNIT
        $ujsawal                = $this->input->post('allowancmodal'); //UANG JALAN 

        $data['orderdate']      = $tgl . " " . $jam;
        $data['id_customers']   = $this->input->post('id_customersmodal');
        $data['description']    = $this->input->post('description');
        $data['id_fleets']      = $this->input->post('id_fleets');
        $data['id_drivers']     = $this->input->post('id_drivers');
        $data['id_drivers2']    = $this->input->post('id_drivers2');
        $data['shippment']       = $this->input->post('id_shippment');
        $data['id_citieso']     = $this->input->post('id_citio');
        $data['id_citiesd']     = $this->input->post('id_citiesd');
        $data['id_ordertypes']  = $this->input->post('id_ordertypes');
        $data['loadqty']        = $this->input->post('totalqtymodal');


        $data['id_citieso2']     = $this->input->post('id_citio2');
        $data['id_citiesd2']     = $this->input->post('id_citiesd2');
        $data['loadqty2']        = $this->input->post('loadqty2');
        $data['allowance2']          = $this->input->post('pricelocationmodal'); // UANG Unit

        //$data['id_routes']      = $this->input->post('id_routesmodal');
        $data['prices']         = $this->input->post('allowancmodal'); // UANG JALAN
        $data['pricesadd']      = $ujsadd + $ujsadd2; // UANG JALAN
        $data['allowances']     = $this->input->post('pricemodal'); // UANG UNIT
        $data['allowanceadds']  = $salesadd; // UANG UNIT

        date_default_timezone_set("Asia/Bangkok");
        $tanggalupdate = date("d-m-y H:i:s");

        $data['updateby']             = $this->session->userdata('id');
        $data['updatedatetime']       = $tanggalupdate;
        $data['active']         = 1;
        $data['ritase']         = 1;

        $this->db->where('code', $dcode);
        $this->db->update('orders', $data);
        //$this->db->insert('orders',$data);

        $user = $this->session->userdata('id');

        $frame      = $this->input->post('frameno');
        $machine    = $this->input->post('machineno');
        $color      = $this->input->post('color');
        $type       = $this->input->post('type');



        if ($addroutes >= 0) {

            $orde = array();
            for ($i = 0; $i < $addroutes; $i++) {
                $loop2 = $this->input->post('deliverynomodal_' . $i . '');
                $hasilloop = count($loop2);
                for ($c = 0; $c < $hasilloop; $c++) {
                    # code...
                    $id = $this->input->post('id_' . $i . '[' . $c . ']');
                    $countid = count($id);
                    $dt = array(
                        'order_code' => $this->input->post('code'),
                        'frameno' => $this->input->post('framenomodal_' . $i . '[' . $c . ']'),
                        'machineno' => $this->input->post('machinenomodal_' . $i . '[' . $c . ']'),
                        'color' => $this->input->post('colormodal_' . $i . '[' . $c . ']'),
                        'type' => $this->input->post('typemodal_' . $i . '[' . $c . ']'),
                        'case' => $this->input->post('casemodal_' . $i . '[' . $c . ']'),
                        'custcode' => $this->input->post('custcodemodal_' . $i . '[' . $c . ']'),
                        'address' => $this->input->post('addressmodal_' . $i . '[' . $c . ']'),
                        'irisno' => $this->input->post('irisnomodal_' . $i . '[' . $c . ']'),
                        'id_citieso' => $origin[$i],
                        'id_citiesd' => $destination[$i],
                        'id_routes' => $routes[$i],
                        'unitprice' => $unitprice[$i],
                        'qty' => $loadqty[$i],
                        'noshippent' => $this->input->post('deliverynomodal_' . $i . '[' . $c . ']')
                    );
                    if ($countid > 0) {
                        $this->db->where('id', $id);
                        $this->db->update('orderload', $dt);
                    } else {

                        $this->db->insert('orderload', $dt);
                    }

                    //$this->db->insert('orderload', $dt);
                }
            }
        }
        //$user = $this->session->userdata('id');

        //$id         = $this->input->post('id');
        //$frame      = $this->input->post('frameno');
        //$machine    = $this->input->post('machineno');
        //$color      = $this->input->post('color');
        //$type       = $this->input->post('type');

        //$orde =array();
        //   for($i=0; $i < $data['loadqty']; $i++) 
        //   {
        //       $dt[$i] = array(
        //       'id' =>$id[$i],
        //       'frameno' => $frame[$i], 
        //       'machineno' => $machine[$i], 
        //       'color' => $color[$i],
        //       'type' => $type[$i]
        //       );
        //   }

        //$this->db->insert_batch('orderload', $dt);
        //$this->db->update_batch('orderload', $dt, 'id');
    }

    public function simpanPareto()
    {

        $user = $this->session->userdata('id');

        $frame      = $this->input->post('frameno');
        $machine    = $this->input->post('machineno');
        $color      = $this->input->post('color');
        $type       = $this->input->post('type');

        $bongkar    = $this->input->post('p_scnt');
        $ujs        = $this->input->post('ujs');
        $sales      = $this->input->post('sales');

        $desc      = $this->input->post('desc');
        $costadd   = $this->input->post('costadd');
        $nomine    = $this->input->post('nomine');

        $bongkarcount = count($bongkar);

        $addcostcount = count($desc);


        print_r($addcostcount);
        print_r($bongkarcount);



        if ($bongkarcount >= 0) {
            # code...
            $salesadd = 0;
            $ujsadd = 0;
            for ($i = 0; $i < $bongkarcount; $i++) {

                $salesadd += $sales[$i];
                $ujsadd += $ujs[$i];
                $add = array(
                    'order_id' => $this->input->post('code'),
                    'cost_id' => $bongkar[$i],
                    'nominal' => $ujs[$i],
                    'nominal2' => $sales[$i],
                    'createby' => $this->session->userdata('id')
                );

                $this->db->insert('cost', $add);
            }
        }

        if ($addcostcount >= 0) {
            # code...
            $or = array();
            $ujsadd2 = 0;

            for ($i = 0; $i < $addcostcount; $i++) {

                $ujsadd2 += $nomine[$i];
                $ad = array(
                    'order_id' => $this->input->post('code'),
                    'cost_id' => $costadd[$i],
                    'nominal' => $nomine[$i],
                    'createby' => $this->session->userdata('id')
                );
                $this->db->insert('cost', $ad);
            }
        }

        //ORDER
        $data['code']           = $this->input->post('code');
        $tgl                    = $this->input->post('orderdate');
        $jam                    = $this->input->post('orderdatetime');

        $salesawal              = $this->input->post('price'); //UNIT
        $ujsawal                = $this->input->post('allowance'); //UANG JALAN 

        $data['orderdate']      = $tgl . " " . $jam;
        $data['id_customers']   = $this->input->post('id_customers');
        $data['description']    = $this->input->post('description');
        $data['id_fleets']      = $this->input->post('id_fleets');
        $data['id_drivers']     = $this->input->post('id_drivers');
        $data['id_drivers2']    = $this->input->post('id_drivers2');
        $data['shipment']       = $this->input->post('id_shippment');
        $data['id_citieso']     = $this->input->post('id_citieso');
        $data['id_citiesd']     = $this->input->post('id_citiesd');
        $data['loadqty']        = $this->input->post('loadqty');

        $data['id_citieso2']     = $this->input->post('id_citieso2');
        $data['id_citiesd2']     = $this->input->post('id_citiesd2');
        $data['loadqty2']        = $this->input->post('loadqty2');
        $data['allowance2']          = $this->input->post('pricelocation'); // UANG Unit


        $data['id_ordertypes']  = $this->input->post('id_ordertypes');
        $data['id_routes']      = $this->input->post('id_routes');
        $data['prices']         = $this->input->post('allowance'); // UANG JALAN
        $data['pricesadd']      = $ujsadd + $ujsadd2; // UANG JALAN
        $data['allowances']     = $this->input->post('price'); // UANG UNIT
        $data['allowanceadds']  = $salesadd; // UANG UNIT

        date_default_timezone_set("Asia/Bangkok");
        $tanggalupdate = date("d-m-y H:i:s");

        $data['active']         = 1;
        $data['ritase']         = 1;

        //$this->db->where('code', $dcode);
        //$this->db->update('orders', $data);
        $this->db->insert('orders', $data);

        $user = $this->session->userdata('id');

        $frame      = $this->input->post('frameno');
        $machine    = $this->input->post('machineno');
        $color      = $this->input->post('color');
        $type       = $this->input->post('type');

        $orde = array();
        for ($i = 0; $i < $data['loadqty']; $i++) {
            $dt[$i] = array(
                'order_code' => $this->input->post('code'),
                'frameno' => $frame[$i],
                'machineno' => $machine[$i],
                'color' => $color[$i],
                'type' => $type[$i]
            );
        }

        $this->db->insert_batch('orderload', $dt);


        $frame2      = $this->input->post('frameno2');
        $machine2    = $this->input->post('machineno2');
        $color2      = $this->input->post('color2');
        $type2       = $this->input->post('type2');

        $ord = array();
        for ($i = 0; $i < $data['loadqty2']; $i++) {
            $dt2[$i] = array(
                'order_code' => $this->input->post('code'),
                'frameno' => $frame2[$i],
                'machineno' => $machine2[$i],
                'color' => $color2[$i],
                'type' => $type2[$i]
            );
        }

        $this->db->insert_batch('orderload', $dt2);

        $this->db->select('*');
        $this->db->where('id', $user);
        $result = $this->db->get('users');


        foreach ($result->result_array() as $row) {
            $iddc = $row['id'];
            $a    = $row['nominal'];
            $b    = $ujsawal + $ujsadd + $ujsadd2;

            $c = $a - $b;

            $dat['nominal']     = $c;

            $this->db->where('id', $iddc);
            $this->db->update('users', $dat);
        }
    }

    public function simpanParetoBeta()
    {

        $user = $this->session->userdata('id');

        $frame      = $this->input->post('frameno');
        $machine    = $this->input->post('machineno');
        $color      = $this->input->post('color');
        $type       = $this->input->post('type');
        $irisn      = $this->input->post('irisno');
        $dono       = $this->input->post('deliveryno');

        $origin      = $this->input->post('id_citieso');
        $destination = $this->input->post('id_citiesd');
        $loadqty     = $this->input->post('jsqty');
        $routes      = $this->input->post('id_routes');
        $unitprice    = $this->input->post('jsunit');
        $tabu    = $this->input->post('tabsupir');
        $komsu    = $this->input->post('komsupir');


        $bongkar    = $this->input->post('p_scnt');
        $ujs        = $this->input->post('ujs');
        $sales      = $this->input->post('sales');

        $desc      = $this->input->post('desc');
        $coa1   = $this->input->post('coa1');
        $nomines   = $this->input->post('nomine');


        $costadd   = $this->input->post('costakun');
        $nomine    = $this->input->post('biayaakun');
        $coa    = $this->input->post('coa');
        $noine    = $this->input->post('idakun');


        $ujsadd2 = 0;
        $salesadd = 0;
        $ujsadd = 0;

        if (isset($bongkar)) {
            $bongkarcount = count($bongkar);

            if ($bongkarcount >= 0) {
                for ($i = 0; $i < $bongkarcount; $i++) {

                    $salesadd += $sales[$i];
                    $ujsadd += $ujs[$i];
                    $add = array(
                        'order_id' => $this->input->post('code'),
                        'cost_id' => $bongkar[$i],
                        'nominal' => str_replace(",", "", $ujs[$i]),
                        'nominal2' => str_replace(",", "", $sales[$i]),
                        'active' => 1,
                        'createby' => $this->session->userdata('id')
                    );

                    $this->db->insert('cost', $add);
                }
            }
        }

        if (isset($coa1)) {
            $addcostcounts = count($coa1);
            if ($addcostcounts >= 0) {
                for ($i = 0; $i < $addcostcounts; $i++) {
                    if ($nomines[$i] != 0) {

                        $ad = array(
                            'order_id' => $this->input->post('code'),
                            'date' => $this->input->post('orderdate'),
                            'coa' => $coa1[$i],
                            'nominal' => str_replace(",", "", $nomines[$i]) ?: 0,
                            'description' => $desc[$i],
                            'akun_id' => 14,
                            'nominal2' => 1,
                            'cost_type' => 1,
                            'active' => 1,
                            'createby' => $this->session->userdata('id')
                        );
                        $this->db->insert('cost', $ad);
                    }
                }
            }
        }

        if (isset($noine)) {
            $addcostcount = count($noine);

            if ($addcostcount >= 0) {
                $or = array();

                for ($i = 0; $i < $addcostcount; $i++) {

                    //$ujsadd2 += $noine[$i];
                    if (!empty($nomine[$i])) {
                        $ad = array(
                            'order_id' => $this->input->post('code'),
                            'cost_id' => $costadd[$i],
                            'date' => $this->input->post('orderdate'),
                            'nominal' => str_replace(",", "", $nomine[$i]) ?: 0,
                            'active' => 1,
                            'cost_type' => 1,
                            'nominal2' => 2,
                            'akun_id' => $noine[$i],
                            'coa' => $coa[$i],
                            'createby' => $this->session->userdata('id')
                        );

                        $this->db->insert('cost', $ad);
                    }
                }
            }
        }
        //ORDER}
        $data['code']           = $this->input->post('code');
        $tgl                    = $this->input->post('orderdate');
        $jam                    = $this->input->post('orderdatetime');

        $salesawal              = str_replace(",", "", $this->input->post('price')); //UNIT
        $ujsawal                = str_replace(",", "", $this->input->post('allowance')); //UANG JALAN 

        $data['orderdate']      = $tgl . " " . $jam;
        $data['date']      = $tgl;
        $data['id_customers']   = $this->input->post('id_customers');
        $data['description']    = $this->input->post('description');
        $data['id_fleets']      = $this->input->post('id_fleets');
        $data['id_drivers']     = $this->input->post('id_drivers');
        $data['id_drivers2']    = $this->input->post('id_drivers2');
        $data['shippment']       = $this->input->post('id_shippment');

        $data['id_citieso']     = $this->input->post('id_citieso[0]');
        $data['id_citiesd']     = $this->input->post('id_citiesd[0]');
        $data['loadqty']        = $this->input->post('totalqty');


        $data['feeOps']     = str_replace(",", "", $this->input->post('biayaakun[0]')) != "" ? str_replace(",", "", $this->input->post('biayaakun[0]')) : 0;
        $data['feeLolo']     = str_replace(",", "", $this->input->post('biayaakun[1]')) > 0 ? str_replace(",", "", $this->input->post('biayaakun[1]')) : 0;
        $data['feeDepo']     = str_replace(",", "", $this->input->post('biayaakun[2]')) > 0 ? str_replace(",", "", $this->input->post('biayaakun[2]')) : 0;
        $data['feeParkir']     = str_replace(",", "", $this->input->post('biayaakun[3]')) > 0 ? str_replace(",", "", $this->input->post('biayaakun[3]')) : 0;
        $data['feeKawalan']     = str_replace(",", "", $this->input->post('biayaakun[4]')) > 0 ? str_replace(",", "", $this->input->post('biayaakun[4]')) : 0;
        $data['feeStorage']     = str_replace(",", "", $this->input->post('biayaakun[5]')) > 0 ? str_replace(",", "", $this->input->post('biayaakun[5]')) : 0;
        $data['feeSeal']     = str_replace(",", "", $this->input->post('biayaakun[7]')) > 0 ? str_replace(",", "", $this->input->post('biayaakun[7]')) : 0;
        $data['feeRC']     = str_replace(",", "", $this->input->post('biayaakun[6]')) > 0 ? str_replace(",", "", $this->input->post('biayaakun[6]')) : 0;
        $data['feeAlih']     = str_replace(",", "", $this->input->post('biayaakun[8]')) > 0 ? str_replace(",", "", $this->input->post('biayaakun[8]')) : 0;
        $data['feeCuci']     = str_replace(",", "", $this->input->post('biayaakun[9]')) > 0 ? str_replace(",", "", $this->input->post('biayaakun[9]')) : 0;
        $data['feeSP2']     = str_replace(",", "", $this->input->post('biayaakun[10]')) > 0 ? str_replace(",", "", $this->input->post('biayaakun[10]')) : 0;
        $data['tabSupir']     = $tabu[0] > 0 ? $tabu[0] : 0;
        $data['komSupir']     = $komsu[0] > 0 ? $komsu[0] : 0;

        //$data['id_citieso2']     = $this->input->post('id_citieso2');
        //$data['id_citiesd2']     = $this->input->post('id_citiesd2');
        //$data['loadqty2']        = $this->input->post('loadqty2');
        //$data['allowance2']      = $this->input->post('pricelocation'); // UANG Unit


        $data['id_ordertypes']  = $this->input->post('fleettype[0]');
        $data['prices']         = str_replace(",", "", $this->input->post('allowance')); // UANG JALAN
        $data['pricesadd']      = $ujsadd + $ujsadd2; // UANG JALAN
        $data['allowances']     = str_replace(",", "", $this->input->post('price')); // UANG UNIT
        $data['allowanceadds']  = $salesadd; // UANG UNIT

        date_default_timezone_set("Asia/Bangkok");
        $tanggalupdate = date("d-m-y H:i:s");

        $data['active']         = 1;
        $data['createdatetime']     = date("Y-m-d h:i:s");
        $data['createby']     = $this->session->userdata('id');
        $data['ritase']         = 1;
        $data['noVoucher'] = $this->input->post('noVoucher');

        //$this->db->where('code', $dcode);
        //$this->db->update('orders', $data);
        $this->db->insert('orders', $data);
        //print_r($data);
        $user = $this->session->userdata('id');

        $a = array(
            'order_id' => $this->input->post('code'),
            'coa' => 108,
            'cost_type' => 1,
            'date'    => $this->input->post('orderdate'),
            'nominal' => str_replace(",", "", $this->input->post('allowance')),
            'active' => 1,
            'createby' => $this->session->userdata('id')
        );
        $this->db->insert('cost', $a);

        $b = array(
            'order_id' => $this->input->post('code'),
            'coa' => 112,
            'date'    => $this->input->post('orderdate'),
            'nominal' => str_replace(",", "", $this->input->post('price')),
            'active' => 1,
            'createby' => $this->session->userdata('id')
        );
        $this->db->insert('cost', $b);




        $frame      = $this->input->post('frameno');
        $machine    = $this->input->post('deliveryno');

        if (isset($origin)) {
            $addroutes = count($origin);
            if ($addroutes >= 0) {

                $orde = array();
                for ($i = 0; $i < $addroutes; $i++) {
                    $dt = array(
                        'order_code' => $this->input->post('code'),
                        'id_ordertypes' => $this->input->post('fleettype[0]'),
                        'id_citieso' => $origin[$i],
                        'id_citiesd' => $destination[$i],
                        'id_routes' => $routes[$i],
                        'noshippent' => $machine[$i],
                        'frameno' => $frame[$i],
                        'unitprice' => $unitprice[$i],
                        'qty' => $loadqty[$i],

                    );
                    $this->db->insert('orderload', $dt);

                    $c = array(
                        'order_id' => $this->input->post('code'),
                        'driver_id' => $this->input->post('id_drivers'),
                        'active' => 1,
                        'nominal' => 1,
                        'coa' => 73,
                        'date'    => $this->input->post('orderdate'),
                        'nominal' => $tabu[$i] ?: 0,
                        'createby' => $this->session->userdata('id')
                    );
                    $this->db->insert('cost', $c);

                    $c = array(
                        'order_id' => $this->input->post('code'),
                        'driver_id' => $this->input->post('id_drivers'),
                        'active' => 1,
                        'nominal' => 1,
                        'coa' => 141,
                        'date'    => $this->input->post('orderdate'),
                        'nominal' => $komsu[$i] ?: 0,
                        'createby' => $this->session->userdata('id')
                    );
                    $this->db->insert('cost', $c);
                }
            }
        }
        // 
        $this->db->select('*');
        $this->db->where('id', $user);
        $result = $this->db->get('users');


        foreach ($result->result_array() as $row) {
            $iddc = $row['id'];
            $a    = $row['nominal'];
            $b    = $ujsawal + $ujsadd + $ujsadd2;

            $c = $a - $b;

            $dat['nominal']     = $c;

            $this->db->where('id', $iddc);
            $this->db->update('users', $dat);
        }
    }

    public function editOrderspld()
    {

        $user = $this->session->userdata('id');

        $frame      = $this->input->post('frameno');
        $machine    = $this->input->post('machineno');
        $color      = $this->input->post('color');
        $type       = $this->input->post('type');
        $origin      = $this->input->post('id_citiesomodal');
        $destination = $this->input->post('id_citiesdmodal');
        $loadqty     = $this->input->post('jsqtymodal');
        $routes      = $this->input->post('id_routesmodal');
        $unitprice    = $this->input->post('jsunitmodal');

        $bongkar    = $this->input->post('p_scntmodal');
        $ujs        = $this->input->post('ujsmodal');
        $sales      = $this->input->post('salesmodal');
        $kod        = $this->input->post('kod');

        $desc      = $this->input->post('descmodal');
        $nomine    = $this->input->post('nominemodal');
        $coamodal      = $this->input->post('coamodal');
        $kode    = $this->input->post('kode');

        //Tertagih
        $biayaakun    = $this->input->post('biayaakun');
        $coa   = $this->input->post('coa');

        $ujsadd2 = 0;
        $salesadd = 0;
        $ujsadd = 0;

        $this->db->where('order_id', $this->input->post('code'));
        $this->db->delete('cost');

        if (isset($bongkar)) {
            $bongkarcount = count($bongkar);

            if ($bongkarcount >= 0) {
                # code...
                for ($i = 0; $i < $bongkarcount; $i++) {

                    $salesadd += $sales[$i];
                    $ujsadd += $ujs[$i];
                    $add = array(
                        'order_id' => $this->input->post('code'),
                        'cost_id' => $bongkar[$i],
                        'nominal' => str_replace(",", "", $ujs[$i]),
                        'active' => 1,
                        'nominal2' => str_replace(",", "", $sales[$i]),
                        'createby' => $this->session->userdata('id')
                    );

                    $this->db->insert('cost', $add);
                }
            }
        }

        if (isset($desc)) {
            $addcostcount = count($desc);

            if ($addcostcount >= 0) {
                # code...
                $or = array();

                for ($i = 0; $i < $addcostcount; $i++) {

                    $ujsadd2 += str_replace(",", "", $nomine[$i]);
                    $ad = array(


                        'order_id' => $this->input->post('code'),
                        'date' => $this->input->post('orderdates'),
                        'active' => 1,
                        'cost_type' => 1,
                        'coa' => $coamodal[$i],
                        'description' => $desc[$i],
                        'nominal' => str_replace(",", "", $nomine[$i]),
                        'createby' => $this->session->userdata('id')

                    );
                    $this->db->insert('cost', $ad);
                }
                //for($i=0; $i < $addcostcount; $i++) 
                //{

                //    $ujsadd2 += str_replace(",", "", $nomine[$i]);
                //    $ad = array(


                //        'order_id' =>$this->input->post('code'),
                //        'date' => $this->input->post('orderdates'),
                //        'active' => 1,
                //        'akun_id' => 1, 
                //        'cost_id' => $costadd[$i], 
                //        'nominal' => "-".str_replace(",", "", $nomine[$i])."", 
                //        'createby' => $this->session->userdata('id')

                //    );
                //    $this->db->insert('cost', $ad);

                //}                 

            }
        }

        if (isset($coa)) {
            $addcostcount = count($coa);

            if ($addcostcount >= 0) {
                $or = array();

                for ($i = 0; $i < $addcostcount; $i++) {

                    $ujsadd2 += str_replace(",", "", $biayaakun[$i]);
                    $ad = array(
                        'order_id' => $this->input->post('code'),
                        'date' => $this->input->post('orderdates'),
                        'nominal' => str_replace(",", "", $biayaakun[$i]),
                        'cost_type' => 1,
                        'active' => 1,
                        'coa' => $coa[$i],
                        'createby' => $this->session->userdata('id')
                    );

                    $this->db->insert('cost', $ad);
                }

                //for($i=0; $i < $addcostcount; $i++) 
                //{

                //    $ujsadd2 += str_replace(",", "", $biayaakun[$i]);
                //    $ad = array(
                //        'order_id' =>$this->input->post('code'),
                //        'active' => 1,
                //        'cost_id' => 44, 
                //        'date' => $this->input->post('orderdates'),
                //        'nominal' => "-".str_replace(",", "", $biayaakun[$i])."", 
                //        'akun_id' => 1, 
                //        'createby' => $this->session->userdata('id')
                //    );

                //    $this->db->insert('cost', $ad);

                //}               

            }
        }

        $dcode                  = $this->input->post('code');
        $tgl                    = $this->input->post('orderdates');
        $jam                    = $this->input->post('orderdatetime');

        $salesawal              = str_replace(",", "", $this->input->post('pricmodal')); //UNIT
        $ujsawal                = str_replace(",", "", $this->input->post('allowancmodal')); //UANG JALAN 

        $data['orderdate']      = $tgl . " " . $jam;
        $data['id_customers']   = $this->input->post('id_customersmodal');
        $data['description']    = $this->input->post('description');
        $data['id_fleets']      = $this->input->post('id_fleets');
        $data['id_drivers']     = $this->input->post('id_drivers');
        $data['id_drivers2']    = $this->input->post('id_drivers2');
        $data['shippment']      = $this->input->post('id_shippment');
        $data['id_citieso']     = $this->input->post('id_citiesomodal[0]');
        $data['id_citiesd']     = $this->input->post('id_citiesdmodal[0]');
        $data['id_ordertypes']  = $this->input->post('fleettypeomodal[0]');
        $data['loadqty']        = $this->input->post('totalqtymodal');
        //$data['loadqty']        = $this->input->post('totalqtymodal');


        $data['id_citieso2']     = $this->input->post('id_citio2');
        $data['id_citiesd2']     = $this->input->post('id_citiesd2');
        $data['loadqty2']        = $this->input->post('loadqty2');
        $data['allowance2']          = $this->input->post('pricelocationmodal'); // UANG Unit
        $data['feeOps']     = str_replace(",", "", $this->input->post('biayaakun[0]'));
        $data['feeLolo']     = str_replace(",", "", $this->input->post('biayaakun[1]'));
        $data['feeDepo']     = str_replace(",", "", $this->input->post('biayaakun[2]'));
        $data['feeParkir']     = str_replace(",", "", $this->input->post('biayaakun[3]'));
        $data['feeKawalan']     = str_replace(",", "", $this->input->post('biayaakun[4]'));
        $data['feeStorage']     = str_replace(",", "", $this->input->post('biayaakun[5]'));
        $data['feeSeal']     = str_replace(",", "", $this->input->post('biayaakun[7]'));
        $data['feeRC']     = str_replace(",", "", $this->input->post('biayaakun[6]'));
        $data['feeAlih']     = str_replace(",", "", $this->input->post('biayaakun[8]'));
        $data['feeCuci']     = str_replace(",", "", $this->input->post('biayaakun[9]'));
        $data['feeSP2']     = str_replace(",", "", $this->input->post('biayaakun[10]'));
        $data['tabSupir']     = $this->input->post('tabsupirmodal[0]') ?: 0;
        $data['komSupir']     = $this->input->post('komsupirmodal[0]') ?: 0;
        //$data['id_routes']      = $this->input->post('id_routesmodal');
        $data['prices']         = str_replace(",", "", $this->input->post('allowancmodal')); // UANG JALAN
        $data['pricesadd']      = $ujsadd + $ujsadd2; // UANG JALAN
        $data['allowances']     = str_replace(",", "", $this->input->post('pricemodal')); // UANG UNIT
        $data['allowanceadds']  = $salesadd; // UANG UNIT
        $data['noVoucher'] = $this->input->post('noVoucher');


        date_default_timezone_set("Asia/Bangkok");
        $tanggalupdate = date("Y-m-d H:i:s");

        $data['updateby']             = $this->session->userdata('id');
        $data['updatedatetime']       = $tanggalupdate;
        $data['active']         = 1;
        $data['ritase']         = 1;

        $this->db->where('code', $dcode);
        $this->db->update('orders', $data);
        //$this->db->insert('orders',$data);

        $user = $this->session->userdata('id');

        $a = array(
            'order_id' => $this->input->post('code'),
            'cost_type' => 1,
            'coa' => 108,
            'date'    => $this->input->post('orderdates'),
            'nominal' => str_replace(",", "", $this->input->post('allowancmodal')),
            'active' => 1,
            'createby' => $this->session->userdata('id')
        );
        $this->db->insert('cost', $a);

        $b = array(
            'order_id' => $this->input->post('code'),
            'coa' => 112,
            'date'    => $this->input->post('orderdates'),
            'nominal' => str_replace(",", "", $this->input->post('pricemodal')),
            'active' => 1,
            'createby' => $this->session->userdata('id')
        );
        $this->db->insert('cost', $b);

        $c = array(
            'order_id' => $this->input->post('code'),
            'driver_id' => $this->input->post('id_drivers'),
            'coa' => 73,
            'date'    => $this->input->post('orderdates'),
            'active' => 1,
            'nominal' => $this->input->post('tabsupirmodal[0]') ?: 0,
            'createby' => $this->session->userdata('id')
        );
        $this->db->insert('cost', $c);

        $T = array(
            'order_id' => $this->input->post('code'),
            'driver_id' => $this->input->post('id_drivers'),
            'coa' => 141,
            'date'    => $this->input->post('orderdates'),
            'active' => 1,
            'nominal' => $this->input->post('komsupirmodal[0]') ?: 0,
            'createby' => $this->session->userdata('id')
        );
        $this->db->insert('cost', $T);



        $user = $this->session->userdata('id');

        $frame      = $this->input->post('frameno');
        $machine    = $this->input->post('machineno');
        $color      = $this->input->post('color');
        $type       = $this->input->post('type');


        if (isset($origin)) {
            $addroutes = count($origin);

            if ($addroutes >= 0) {

                $orde = array();
                for ($i = 0; $i < $addroutes; $i++) {
                    $loop2 = $this->input->post('deliverynomodal_' . $i . '');
                    $hasilloop = count($loop2);
                    for ($c = 0; $c < $hasilloop; $c++) {
                        # code...
                        $id = $this->input->post('id_' . $i . '[' . $c . ']');
                        //$countid=count($id);
                        $dt = array(
                            'order_code' => $this->input->post('code'),
                            'frameno' => $this->input->post('framenomodal_' . $i . '[' . $c . ']'),
                            'machineno' => $this->input->post('machinenomodal_' . $i . '[' . $c . ']'),
                            'color' => $this->input->post('colormodal_' . $i . '[' . $c . ']'),
                            'type' => $this->input->post('typemodal_' . $i . '[' . $c . ']'),
                            'case' => $this->input->post('casemodal_' . $i . '[' . $c . ']'),
                            'custcode' => $this->input->post('custcodemodal_' . $i . '[' . $c . ']'),
                            'address' => $this->input->post('addressmodal_' . $i . '[' . $c . ']'),
                            'irisno' => $this->input->post('irisnomodal_' . $i . '[' . $c . ']'),
                            'id_citieso' => $origin[$i],
                            'id_citiesd' => $destination[$i],
                            'id_routes' => $routes[$i],
                            'unitprice' => $unitprice[$i],
                            'id_ordertypes' => $this->input->post('fleettypeomodal[0]'),
                            'qty' => $loadqty[$i],
                            'noshippent' => $this->input->post('deliverynomodal_' . $i . '[' . $c . ']')
                        );
                        if (isset($id)) {
                            $this->db->where('id', $id);
                            $this->db->update('orderload', $dt);
                        } else {

                            $this->db->insert('orderload', $dt);
                        }

                        //$this->db->insert('orderload', $dt);
                    }
                }
            }
        }
        //$user = $this->session->userdata('id');

        //$id         = $this->input->post('id');
        //$frame      = $this->input->post('frameno');
        //$machine    = $this->input->post('machineno');
        //$color      = $this->input->post('color');
        //$type       = $this->input->post('type');

        //$orde =array();
        //   for($i=0; $i < $data['loadqty']; $i++) 
        //   {
        //       $dt[$i] = array(
        //       'id' =>$id[$i],
        //       'frameno' => $frame[$i], 
        //       'machineno' => $machine[$i], 
        //       'color' => $color[$i],
        //       'type' => $type[$i]
        //       );
        //   }

        //$this->db->insert_batch('orderload', $dt);
        //$this->db->update_batch('orderload', $dt, 'id');
    }

    public function simpanSpotBeta()
    {

        $user = $this->session->userdata('id');

        $frame      = $this->input->post('frameno');
        $machine    = $this->input->post('machineno');
        $color      = $this->input->post('color');
        $type       = $this->input->post('type');
        $irisn      = $this->input->post('irisno');
        $dono       = $this->input->post('deliveryno');

        $origin      = $this->input->post('id_citieso');
        $destination = $this->input->post('id_citiesd');
        $loadqty     = $this->input->post('jsqty');
        $routes      = $this->input->post('id_routes');
        $unitprice    = $this->input->post('jsunit');


        $bongkar    = $this->input->post('p_scnt');
        $ujs        = $this->input->post('ujs');
        $sales      = $this->input->post('sales');

        $desc      = $this->input->post('desc');
        $costadd   = $this->input->post('costadd');
        $nomine    = $this->input->post('nomine');

        $bongkarcount = count($bongkar);

        $addcostcount = count($desc);

        $addroutes = count($origin);


        print_r($addcostcount);
        print_r($bongkarcount);
        print_r($addroutes);



        if ($bongkarcount >= 0) {
            # code...
            $salesadd = 0;
            $ujsadd = 0;
            for ($i = 0; $i < $bongkarcount; $i++) {

                $salesadd += $sales[$i];
                $ujsadd += $ujs[$i];
                $add = array(
                    'order_id' => $this->input->post('code'),
                    'cost_id' => $bongkar[$i],
                    'nominal' => $ujs[$i],
                    'nominal2' => $sales[$i],
                    'createby' => $this->session->userdata('id')
                );

                $this->db->insert('cost', $add);
            }
        }

        if ($addcostcount >= 0) {
            # code...
            $or = array();
            $ujsadd2 = 0;

            for ($i = 0; $i < $addcostcount; $i++) {

                $ujsadd2 += $nomine[$i];
                $ad = array(
                    'order_id' => $this->input->post('code'),
                    'cost_id' => $costadd[$i],
                    'nominal' => $nomine[$i],
                    'createby' => $this->session->userdata('id')
                );

                $this->db->insert('cost', $ad);
            }
        }

        //ORDER
        $data['code']           = $this->input->post('code');
        $tgl                    = $this->input->post('orderdate');
        $jam                    = $this->input->post('orderdatetime');

        $salesawal              = $this->input->post('price'); //UNIT
        $ujsawal                = $this->input->post('allowance'); //UANG JALAN 

        $data['orderdate']      = $tgl . " " . $jam;
        $data['id_customers']   = $this->input->post('id_customers');
        $data['description']    = $this->input->post('description');
        $data['id_fleets']      = $this->input->post('id_fleets');
        $data['id_drivers']     = $this->input->post('id_drivers');
        $data['id_drivers2']    = $this->input->post('id_drivers2');
        $data['shippment']       = $this->input->post('id_shippment');

        $data['id_citieso']     = $this->input->post('id_citieso[0]');
        $data['id_citiesd']     = $this->input->post('id_citiesd[0]');
        $data['loadqty']        = $this->input->post('loadqty[0]');

        //$data['id_citieso2']     = $this->input->post('id_citieso2');
        //$data['id_citiesd2']     = $this->input->post('id_citiesd2');
        //$data['loadqty2']        = $this->input->post('loadqty2');
        //$data['allowance2']      = $this->input->post('pricelocation'); // UANG Unit


        $data['id_ordertypes']  = $this->input->post('id_ordertypes');
        $data['prices']         = $this->input->post('allowance'); // UANG JALAN
        $data['pricesadd']      = $ujsadd + $ujsadd2; // UANG JALAN
        $data['allowances']     = $this->input->post('price'); // UANG UNIT
        $data['allowanceadds']  = $salesadd; // UANG UNIT

        date_default_timezone_set("Asia/Bangkok");
        $tanggalupdate = date("d-m-y H:i:s");

        $data['active']         = 2;
        $data['ritase']         = 1;
        $data['noVoucher'] = $this->input->post('noVoucher');
        $data['penerima'] = $this->input->post('penerima');
        $data['invoicean'] = $this->input->post('invoicean');
        $data['feesaving'] = $this->input->post('feesaving');

        //$this->db->where('code', $dcode);
        //$this->db->update('orders', $data);
        $this->db->insert('orders', $data);
        //print_r($data);
        $user = $this->session->userdata('id');

        $frame      = $this->input->post('frameno');
        $machine    = $this->input->post('machineno');
        $color      = $this->input->post('color');
        $type       = $this->input->post('type');



        if ($addroutes >= 0) {

            $orde = array();
            for ($i = 0; $i < $addroutes; $i++) {
                $loop2 = $this->input->post('frameno_' . $i . '');
                $hasilloop = count($loop2);
                for ($c = 0; $c < $hasilloop; $c++) {
                    $dt = array(
                        'order_code' => $this->input->post('code'),
                        'frameno' => $this->input->post('frameno_' . $i . '[' . $c . ']'),
                        'machineno' => $this->input->post('machineno_' . $i . '[' . $c . ']'),
                        'color' => $this->input->post('color_' . $i . '[' . $c . ']'),
                        'type' => $this->input->post('type_' . $i . '[' . $c . ']'),
                        'id_citieso' => $origin[$i],
                        'id_citiesd' => $destination[$i],
                        'id_routes' => $routes[$i],
                        'unitprice' => $unitprice[$i],
                        'qty' => $loadqty[$i],
                        'noshippent' => $this->input->post('deliveryno_' . $i . '[' . $c . ']')
                    );
                    $this->db->insert('orderload', $dt);
                }
            }
        }

        $this->db->select('*');
        $this->db->where('id', $user);
        $result = $this->db->get('users');


        foreach ($result->result_array() as $row) {
            $iddc = $row['id'];
            $a    = $row['nominal'];
            $b    = $ujsawal + $ujsadd + $ujsadd2;

            $c = $a - $b;

            $dat['nominal']     = $c;

            $this->db->where('id', $iddc);
            $this->db->update('users', $dat);
        }
    }
}
