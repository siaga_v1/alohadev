<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu clas
class orders extends CI_Model{

    public function __construct()
        {
            parent::__construct();
        }

    public function getOrder($data = null)
        {

            //$this->db->select('a.*,b.name as n1,c.name as c2, d.name as cname, g.fleetplateno as fleet, h.name as driver');
            //$this->db->join('cities b','a.id_citieso = b.id', 'left');
            //$this->db->join('cities c','a.id_citiesd = c.id');
            //$this->db->join('customers d','a.id_customers = d.id', 'left');
            //$this->db->join('fleets g','a.id_fleets = g.id', 'left');
            //$this->db->join('drivers h','a.id_drivers = h.id', 'left');
            //$this->db->where('a.active', '1');
            //$result = $this->db->get('orders a',$rowperpage,$rowno);

            //return $result->result_array();

            $where = "";
        if($this->input->post('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%".$this->db->escape_str(strtolower($this->input->post('fleetplateno')))."%' ";
        }
        if($this->input->post('origin') != '') {
            $where .= " AND b.name LIKE '%".$this->db->escape_str(strtolower($this->input->post('origin')))."%' ";
        }
        if($this->input->post('destination') != '') {
            $where .= " AND c.name LIKE '%".$this->db->escape_str(strtolower($this->input->post('destination')))."%' ";
        }
        if($this->input->post('Koordinator') != '') {
            $where .= " AND o.createby LIKE '%".$this->db->escape_str(strtolower($this->input->post('Koordinator')))."%' ";
        }

        if($this->input->post('noVoucher') != '') {
            $where .= " AND o.noVoucher LIKE '%".$this->db->escape_str(strtolower($this->input->post('noVoucher')))."%' ";
        }

        if($this->input->post('type') != '') {
            $where .= " AND h.name LIKE '%".$this->db->escape_str(strtolower($this->input->post('type')))."%' ";
        }
        
        if($this->input->post('code') != '') {
            $where .= " AND o.code LIKE '%".$this->input->post('code')."%' ";
        }
        if($this->input->post('noka') != '') {
            $where .= " AND i.frameno LIKE '%".$this->input->post('noka')."%' ";
        }
        if($this->input->post('customer') != '') {
            $where .= "AND d.name LIKE '%".$this->input->post('customer')."%' ";
        }

        if($this->input->get('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%".$this->db->escape_str(strtolower($this->input->get('fleetplateno')))."%' ";
        }


        ///////


        if($this->input->get('code') != '') {
            $where .= " AND o.code LIKE '%".$this->input->get('code')."%' ";
        }
		if($this->input->get('noka') != '') {
            $where .= " AND i.frameno LIKE '%".$this->input->get('noka')."%' ";
        }

        if($this->input->get('origin') != '') {
            $where .= " AND b.name LIKE '%".$this->db->escape_str(strtolower($this->input->get('origin')))."%' ";
        }
        if($this->input->get('destination') != '') {
            $where .= " AND c.name LIKE '%".$this->db->escape_str(strtolower($this->input->get('destination')))."%' ";
        }

        if($this->input->get('Koordinator') != '') {
            $where .= " AND o.createby LIKE '%".$this->db->escape_str(strtolower($this->input->get('Koordinator')))."%' ";
        }
        if($this->input->get('noVoucher') != '') {
            $where .= " AND o.noVoucher  LIKE '%".$this->db->escape_str(strtolower($this->input->get('noVoucher')))."%' ";
        }

        if($this->input->get('customer') != '') {
            $where .= " AND d.name LIKE '%".$this->input->get('customer')."%' ";
        }
        
        if($this->input->get('type') != '') {
            $where .= " AND h.name LIKE '%".$this->db->escape_str(strtolower($this->input->get('type')))."%' ";
        }

        if($this->input->post('start') > 0 && $this->input->post('end') > 0){
            $where .= " AND o.orderdate BETWEEN '".$this->input->post('start')."' AND '".$this->input->post('end')."'";
        }
        
        if($this->input->get('start') > 0 && $this->input->get('end') > 0){
            $where .= " AND o.orderdate BETWEEN '".$this->input->get('start')."' AND '".$this->input->get('end')."'";
        }
        
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
            SELECT
                * 
            FROM
                (
                SELECT
                    ROW_NUMBER () OVER ( ORDER BY A.ritases DESC ) AS RowNum,
                    A.* 
                FROM
                    (
                    SELECT
            o.id,
            o.code,
            o.orderdate,
            o.ritase,
            o.prices,
            o.allowances,
            o.noVoucher,
            i.frameno,
            i.noshippent,
            i.unitprice,
            i.type,
            i.color,
            b.name AS n1,
            c.name AS c2,
            d.name AS cname,
            g.fleetplateno AS fleet,
            h.name AS driver,
            COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,
            COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,
            COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,
            COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,
            COALESCE ( SUM ( o.prices ), 0 ) AS pricess 
        FROM
            orders o
            LEFT JOIN cities b ON o.id_citieso = b.id
            LEFT JOIN orderload i ON o.code = i.order_code
            LEFT JOIN cities c ON o.id_citiesd = c.id
            LEFT JOIN customers d ON o.id_customers = d.id
            LEFT JOIN fleets g ON o.id_fleets = g.id
            LEFT JOIN drivers h ON o.id_drivers = h.id 
        WHERE
            o.active = 1 
            $where
            
        GROUP BY
            o.id,
            o.code,
            o.orderdate,
            o.ritase,
            o.prices,
            o.allowances,
            o.noVoucher,
            i.noshippent,
            i.unitprice,
            i.frameno,
            i.type,
            i.color,
            g.fleetplateno,
            g.CODE,
            g.fleetnumber,
            d.NAME,
            b.name,
            c.name,
            h.name 
                        ) A 
                    ) B 
                WHERE
                    B.id IS NOT NULL
            $paging
        ";
        
        $result = $this->db->query($sql);
        return $result->result(); 
            
        } 
    
    public function getOrderApprove($data = null)
        {

            //$this->db->select('a.*,b.name as n1,c.name as c2, d.name as cname, g.fleetplateno as fleet, h.name as driver');
            //$this->db->join('cities b','a.id_citieso = b.id', 'left');
            //$this->db->join('cities c','a.id_citiesd = c.id');
            //$this->db->join('customers d','a.id_customers = d.id', 'left');
            //$this->db->join('fleets g','a.id_fleets = g.id', 'left');
            //$this->db->join('drivers h','a.id_drivers = h.id', 'left');
            //$this->db->where('a.active', '1');
            //$result = $this->db->get('orders a',$rowperpage,$rowno);

            //return $result->result_array();

            $where = "";
        if($this->input->post('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%".$this->db->escape_str(strtolower($this->input->post('fleetplateno')))."%' ";
        }
        if($this->input->post('origin') != '') {
            $where .= " AND b.name LIKE '%".$this->db->escape_str(strtolower($this->input->post('origin')))."%' ";
        }
        if($this->input->post('destination') != '') {
            $where .= " AND c.name LIKE '%".$this->db->escape_str(strtolower($this->input->post('destination')))."%' ";
        }
        if($this->input->post('Koordinator') != '') {
            $where .= " AND o.createby LIKE '%".$this->db->escape_str(strtolower($this->input->post('Koordinator')))."%' ";
        }


        if($this->input->post('type') != '') {
            $where .= " AND h.name LIKE '%".$this->db->escape_str(strtolower($this->input->post('type')))."%' ";
        }
        
        if($this->input->post('code') != '') {
            $where .= " AND o.code LIKE '%".$this->input->post('code')."%' ";
        }
        if($this->input->post('noka') != '') {
            $where .= " AND i.frameno LIKE '%".$this->input->post('noka')."%' ";
        }
        if($this->input->post('customer') != '') {
            $where .= "AND d.name LIKE '%".$this->input->post('customer')."%' ";
        }

        if($this->input->get('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%".$this->db->escape_str(strtolower($this->input->get('fleetplateno')))."%' ";
        }


        ///////


        if($this->input->get('code') != '') {
            $where .= " AND o.code LIKE '%".$this->input->get('code')."%' ";
        }
        if($this->input->get('noka') != '') {
            $where .= " AND i.frameno LIKE '%".$this->input->get('noka')."%' ";
        }

        if($this->input->get('origin') != '') {
            $where .= " AND b.name LIKE '%".$this->db->escape_str(strtolower($this->input->get('origin')))."%' ";
        }
        if($this->input->get('destination') != '') {
            $where .= " AND c.name LIKE '%".$this->db->escape_str(strtolower($this->input->get('destination')))."%' ";
        }

        if($this->input->get('Koordinator') != '') {
            $where .= " AND o.createby LIKE '%".$this->db->escape_str(strtolower($this->input->get('Koordinator')))."%' ";
        }

        if($this->input->get('customer') != '') {
            $where .= " AND d.name LIKE '%".$this->input->get('customer')."%' ";
        }
        
        if($this->input->get('type') != '') {
            $where .= " AND h.name LIKE '%".$this->db->escape_str(strtolower($this->input->get('type')))."%' ";
        }

        if($this->input->post('start') > 0 && $this->input->post('end') > 0){
            $where .= " AND o.orderdate BETWEEN '".$this->input->post('start')."' AND '".$this->input->post('end')."'";
        }
        
        if($this->input->get('start') > 0 && $this->input->get('end') > 0){
            $where .= " AND o.orderdate BETWEEN '".$this->input->get('start')."' AND '".$this->input->get('end')."'";
        }
        
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
            SELECT
                * 
            FROM
                (
                SELECT
                    ROW_NUMBER () OVER ( ORDER BY A.ritases DESC ) AS RowNum,
                    A.* 
                FROM
                    (
                    SELECT
            o.id,
            o.code,
            o.orderdate,
            o.ritase,
            o.prices,
            b.name AS n1,
            c.name AS c2,
            d.name AS cname,
            g.fleetplateno AS fleet,
            h.name AS driver,
            COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,
            COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,
            COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,
            COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,
            COALESCE ( SUM ( o.prices ), 0 ) AS pricess 
        FROM
            orders o
            LEFT JOIN cities b ON o.id_citieso = b.id
            LEFT JOIN cities c ON o.id_citiesd = c.id
            LEFT JOIN customers d ON o.id_customers = d.id
            LEFT JOIN fleets g ON o.id_fleets = g.id
            LEFT JOIN drivers h ON o.id_drivers = h.id 
        WHERE
            o.active = 1 
            $where
            
        GROUP BY
            o.id,
            o.code,
            o.orderdate,
            o.ritase,
            o.prices,
            g.fleetplateno,
            g.CODE,
            g.fleetnumber,
            d.NAME,
            b.name,
            c.name,
            h.name 
                        ) A 
                    ) B 
                WHERE
                    B.id IS NOT NULL
            $paging
        ";
        
        $result = $this->db->query($sql);
        return $result->result(); 
            
        } 

    public function getOrderInput($data = null)
        {

            //$this->db->select('a.*,b.name as n1,c.name as c2, d.name as cname, g.fleetplateno as fleet, h.name as driver');
            //$this->db->join('cities b','a.id_citieso = b.id', 'left');
            //$this->db->join('cities c','a.id_citiesd = c.id');
            //$this->db->join('customers d','a.id_customers = d.id', 'left');
            //$this->db->join('fleets g','a.id_fleets = g.id', 'left');
            //$this->db->join('drivers h','a.id_drivers = h.id', 'left');
            //$this->db->where('a.active', '1');
            //$result = $this->db->get('orders a',$rowperpage,$rowno);

            //return $result->result_array();

            $where = "";
        if($this->input->post('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%".$this->db->escape_str(strtolower($this->input->post('fleetplateno')))."%' ";
        }
        if($this->input->post('origin') != '') {
            $where .= " AND b.name LIKE '%".$this->db->escape_str(strtolower($this->input->post('origin')))."%' ";
        }
        if($this->input->post('destination') != '') {
            $where .= " AND c.name LIKE '%".$this->db->escape_str(strtolower($this->input->post('destination')))."%' ";
        }
        if($this->input->post('Koordinator') != '') {
            $where .= " AND o.createby LIKE '%".$this->db->escape_str(strtolower($this->input->post('Koordinator')))."%' ";
        }

        if($this->input->post('noVoucher') != '') {
            $where .= " AND o.noVoucher LIKE '%".$this->db->escape_str(strtolower($this->input->post('noVoucher')))."%' ";
        }

        if($this->input->post('type') != '') {
            $where .= " AND h.name LIKE '%".$this->db->escape_str(strtolower($this->input->post('type')))."%' ";
        }
        
        if($this->input->post('code') != '') {
            $where .= " AND o.code LIKE '%".$this->input->post('code')."%' ";
        }
        if($this->input->post('customer') != '') {
            $where .= "AND d.name LIKE '%".$this->input->post('customer')."%' ";
        }

        if($this->input->get('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%".$this->db->escape_str(strtolower($this->input->get('fleetplateno')))."%' ";
        }


        ///////


        if($this->input->get('code') != '') {
            $where .= " AND o.code LIKE '%".$this->input->get('code')."%' ";
        }

        if($this->input->get('origin') != '') {
            $where .= " AND b.name LIKE '%".$this->db->escape_str(strtolower($this->input->get('origin')))."%' ";
        }
        if($this->input->get('destination') != '') {
            $where .= " AND c.name LIKE '%".$this->db->escape_str(strtolower($this->input->get('destination')))."%' ";
        }

        if($this->input->get('Koordinator') != '') {
            $where .= " AND o.createby LIKE '%".$this->db->escape_str(strtolower($this->input->get('Koordinator')))."%' ";
        }
        if($this->input->get('noVoucher') != '') {
            $where .= " AND o.noVoucher  LIKE '%".$this->db->escape_str(strtolower($this->input->get('noVoucher')))."%' ";
        }

        if($this->input->get('customer') != '') {
            $where .= " AND d.name LIKE '%".$this->input->get('customer')."%' ";
        }
        
        if($this->input->get('type') != '') {
            $where .= " AND h.name LIKE '%".$this->db->escape_str(strtolower($this->input->get('type')))."%' ";
        }

        if($this->input->post('start') > 0 && $this->input->post('end') > 0){
            $where .= " AND o.orderdate BETWEEN '".$this->input->post('start')."' AND '".$this->input->post('end')."'";
        }
        
        if($this->input->get('start') > 0 && $this->input->get('end') > 0){
            $where .= " AND o.orderdate BETWEEN '".$this->input->get('start')."' AND '".$this->input->get('end')."'";
        }
        
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
            
            SELECT
                * 
            FROM
                (
                SELECT
                    ROW_NUMBER () OVER ( ORDER BY A.ritases DESC ) AS RowNum,
                    A.* 
                FROM
                    (
                    SELECT
            o.id,
            o.code,
            o.orderdate,
            o.ritase,
            o.prices,
            o.allowances,
            o.loadqty,
            o.noVoucher,        
            f.feesaving,
            j.name as moda,
            b.name AS n1,
            c.name AS c2,
            d.name AS cname,
            g.fleetplateno AS fleet,
            h.name AS driver,
            COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,
            COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,
            COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,
            COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,
            COALESCE ( SUM ( o.prices ), 0 ) AS pricess 
        FROM
            orders o
            LEFT JOIN cities b ON o.id_citieso = b.id
            LEFT JOIN orderload i ON o.code = i.order_code
            LEFT JOIN cities c ON o.id_citiesd = c.id
            LEFT JOIN customers d ON o.id_customers = d.id
            LEFT JOIN routes f ON i.id_routes = f.id
            LEFT JOIN fleets g ON o.id_fleets = g.id
            LEFT JOIN fleettypes j ON g.id_fleettypes = j.id
            LEFT JOIN drivers h ON o.id_drivers = h.id 
        WHERE
            o.active = 1 
                        $where
            
        GROUP BY
            o.id,
            o.code,
            o.orderdate,
            o.loadqty,
            o.ritase,
            o.prices,
            o.allowances,
            f.feesaving,
            o.noVoucher,
            g.fleetplateno,
            g.CODE,
            g.fleetnumber,
            d.NAME,
            j.name,
            b.name,
            c.name,
            h.name 
                        ) A 
                    ) B 
                WHERE
                    B.id IS NOT NULL
            $paging
        ";
        
        $result = $this->db->query($sql);
        return $result->result(); 
            
        } 

    public function getOrderReport($data = null)
        {

            //$this->db->select('a.*,b.name as n1,c.name as c2, d.name as cname, g.fleetplateno as fleet, h.name as driver');
            //$this->db->join('cities b','a.id_citieso = b.id', 'left');
            //$this->db->join('cities c','a.id_citiesd = c.id');
            //$this->db->join('customers d','a.id_customers = d.id', 'left');
            //$this->db->join('fleets g','a.id_fleets = g.id', 'left');
            //$this->db->join('drivers h','a.id_drivers = h.id', 'left');
            //$this->db->where('a.active', '1');
            //$result = $this->db->get('orders a',$rowperpage,$rowno);

            //return $result->result_array();

            $where = "";
        if($this->input->post('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%".$this->db->escape_str(strtolower($this->input->post('fleetplateno')))."%' ";
        }
        if($this->input->post('origin') != '') {
            $where .= " AND b.name LIKE '%".$this->db->escape_str(strtolower($this->input->post('origin')))."%' ";
        }
        if($this->input->post('destination') != '') {
            $where .= " AND c.name LIKE '%".$this->db->escape_str(strtolower($this->input->post('destination')))."%' ";
        }
        if($this->input->post('Koordinator') != '') {
            $where .= " AND o.createby LIKE '%".$this->db->escape_str(strtolower($this->input->post('Koordinator')))."%' ";
        }

        if($this->input->post('noVoucher') != '') {
            $where .= " AND o.noVoucher LIKE '%".$this->db->escape_str(strtolower($this->input->post('noVoucher')))."%' ";
        }

        if($this->input->post('type') != '') {
            $where .= " AND h.name LIKE '%".$this->db->escape_str(strtolower($this->input->post('type')))."%' ";
        }
        
        if($this->input->post('code') != '') {
            $where .= " AND o.code LIKE '%".$this->input->post('code')."%' ";
        }
        if($this->input->post('customer') != '') {
            $where .= "AND d.name LIKE '%".$this->input->post('customer')."%' ";
        }

        if($this->input->get('fleetplateno') != '') {
            $where .= " AND g.fleetplateno LIKE '%".$this->db->escape_str(strtolower($this->input->get('fleetplateno')))."%' ";
        }


        ///////


        if($this->input->get('code') != '') {
            $where .= " AND o.code LIKE '%".$this->input->get('code')."%' ";
        }

        if($this->input->get('origin') != '') {
            $where .= " AND b.name LIKE '%".$this->db->escape_str(strtolower($this->input->get('origin')))."%' ";
        }
        if($this->input->get('destination') != '') {
            $where .= " AND c.name LIKE '%".$this->db->escape_str(strtolower($this->input->get('destination')))."%' ";
        }

        if($this->input->get('Koordinator') != '') {
            $where .= " AND o.createby LIKE '%".$this->db->escape_str(strtolower($this->input->get('Koordinator')))."%' ";
        }
        if($this->input->get('noVoucher') != '') {
            $where .= " AND o.noVoucher  LIKE '%".$this->db->escape_str(strtolower($this->input->get('noVoucher')))."%' ";
        }

        if($this->input->get('customer') != '') {
            $where .= " AND d.name LIKE '%".$this->input->get('customer')."%' ";
        }
        
        if($this->input->get('type') != '') {
            $where .= " AND h.name LIKE '%".$this->db->escape_str(strtolower($this->input->get('type')))."%' ";
        }

        if($this->input->post('start') > 0 && $this->input->post('end') > 0){
            $where .= " AND o.orderdate BETWEEN '".$this->input->post('start')."' AND '".$this->input->post('end')."'";
        }
        
        if($this->input->get('start') > 0 && $this->input->get('end') > 0){
            $where .= " AND o.orderdate BETWEEN '".$this->input->get('start')."' AND '".$this->input->get('end')."'";
        }
        
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
            SELECT
    * 
FROM
    (
    SELECT
        ROW_NUMBER ( ) OVER ( ORDER BY A.ritases DESC ) AS RowNum,
        A.* 
    FROM
        (
        SELECT
            o.code,
            o.id,
            o.noVoucher,
            o.orderdate,
            o.ritase,
            o.loadqty,
            o.pricesadd,
            o.prices,
            o.allowances,
            o.allowanceadds,
            h.name AS driver,
            g.fleetplateno AS fleet,
            c.name AS c2,
            b.name AS n1,
            f.distance,
            d.name AS cname,
            j.name AS moda,
            i.unitprice,
            f.feewash,
            f.feesaving,
            j.nominal,
            COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,
            COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,
            COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,
            COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,
            COALESCE ( SUM ( o.prices ), 0 ) AS pricess 
        FROM
            orders o
            LEFT JOIN cities b ON o.id_citieso = b.id
            LEFT JOIN orderload i ON o.code = i.order_code
            LEFT JOIN cities c ON o.id_citiesd = c.id
            LEFT JOIN customers d ON o.id_customers = d.id
            LEFT JOIN routes f ON i.id_routes = f.id
            LEFT JOIN fleets g ON o.id_fleets = g.id
            LEFT JOIN fleettypes j ON g.id_fleettypes = j.id
            LEFT JOIN drivers h ON o.id_drivers = h.id 
        WHERE
            o.active = 1 
            $where
        GROUP BY
            o.id,
            o.code,
            o.orderdate,
            o.loadqty,
            j.nominal,
            o.ritase,
            o.pricesadd,
            o.allowanceadds,
            o.prices,
            f.feewash,
            o.allowances,
            f.feesaving,
            f.distance,
            o.noVoucher,
            i.unitprice,
            g.fleetplateno,
            g.CODE,
            g.fleetnumber,
            d.NAME,
            j.name,
            b.name,
            c.name,
            h.name 
        ) A 
    ) B 
WHERE
    B.id IS NOT NULL
            $paging
        ";
        
        $result = $this->db->query($sql);
        return $result->result(); 
            
        } 
    

    
    public function getOrderMonth()
        {
            $y = date('Y');
        
            $today      = date('Y-m-d');
            if(date('D', strtotime($today.' -1 Days')) == 'Sun' )
            {
                $yesterday  = date('Y-m-d', strtotime($today.' -2 Days'));
            }
            else
            {
                $yesterday  = date('Y-m-d', strtotime($today.' -1 Days'));
            }
            
            
            $thismonday = date('Y-m-d', strtotime($today.' this week'));
            $thissunday = date('Y-m-d', strtotime($today.' this sunday'));
            
            $lastmonday = date('Y-m-d', strtotime($thismonday.' -1 week'));
            $lastsunday = date('Y-m-d', strtotime($thissunday.' -1 week'));
            
            $thismonth  = date('Y-m-01');
            $thismonthlast = date('Y-m-t');
            $lastmonth  = date('Y-m-01', strtotime($thismonth.' -1 Months'));
            $lastmonthlast = date('Y-m-t', strtotime($lastmonth));
            
            $thism = date('m', strtotime($thismonth));
            $lastm = date('m', strtotime($lastmonth));
            
            $thisy = date('Y', strtotime($thismonth));
            $lastys = date('Y', strtotime($lastmonth));
            
            $thisyear   = date('Y-m-01');
            $lastyear   = date('Y-m-d', strtotime($thisyear.' -1 years'));
            
            $lasty = date('Y', strtotime($lastyear));

            $this->db->select('a.*,b.name as n1,c.name as c2, d.name as cname, g.fleetplateno as fleet, h.name as driver');
            $this->db->join('cities b','a.id_citieso = b.id', 'left');
            $this->db->join('cities c','a.id_citiesd = c.id');
            $this->db->join('customers d','a.id_customers = d.id', 'left');
            $this->db->join('fleets g','a.id_fleets = g.id', 'left');
            $this->db->join('drivers h','a.id_drivers = h.id', 'left');
            $this->db->where('a.active', '1');
            $this->db->where('a.orderdate >=', $thismonday);
            $this->db->where('a.orderdate <=', $thissunday);
            $result = $this->db->get('orders a');

            return $result->result_array();
            
        }    

    

    public function getOrderSpot()
        {
            $y = date('Y');
        
            $today      = date('Y-m-d');
            if(date('D', strtotime($today.' -1 Days')) == 'Sun' )
            {
                $yesterday  = date('Y-m-d', strtotime($today.' -2 Days'));
            }
            else
            {
                $yesterday  = date('Y-m-d', strtotime($today.' -1 Days'));
            }
            
            
            $thismonday = date('Y-m-d', strtotime($today.' this week'));
            $thissunday = date('Y-m-d', strtotime($today.' this sunday'));
            
            $lastmonday = date('Y-m-d', strtotime($thismonday.' -1 week'));
            $lastsunday = date('Y-m-d', strtotime($thissunday.' -1 week'));
            
            $thismonth  = date('Y-m-01');
            $thismonthlast = date('Y-m-t');
            $lastmonth  = date('Y-m-01', strtotime($thismonth.' -1 Months'));
            $lastmonthlast = date('Y-m-t', strtotime($lastmonth));
            
            $thism = date('m', strtotime($thismonth));
            $lastm = date('m', strtotime($lastmonth));
            
            $thisy = date('Y', strtotime($thismonth));
            $lastys = date('Y', strtotime($lastmonth));
            
            $thisyear   = date('Y-m-01');
            $lastyear   = date('Y-m-d', strtotime($thisyear.' -1 years'));
            
            $lasty = date('Y', strtotime($lastyear));

            $this->db->select('a.*,b.name as n1,c.name as c2, d.name as cname,e.unitprice, g.fleetplateno as fleet, h.name as driver');
            $this->db->join('cities b','a.id_citieso = b.id', 'left');
            $this->db->join('cities c','a.id_citiesd = c.id');
            $this->db->join('customers d','a.id_customers = d.id', 'left');
            $this->db->join('orderload e','a.code = e.order_code', 'left');
            $this->db->join('fleets g','a.id_fleets = g.id', 'left');
            $this->db->join('drivers h','a.id_drivers = h.id', 'left');
            $this->db->where('a.active', '2');
            $this->db->where('a.orderdate >=', $thismonday);
            $this->db->where('a.orderdate <=', $thissunday);
            $result = $this->db->get('orders a');

            return $result->result_array();
            
        }    
    
    public function getOneOrder()
        {
            $data=$_POST['id'];

            $this->db->select('*');
            $this->db->where('id',$data);
            $result=$this->db->get('orders');

            return $result->result();
        } 

	
    public function getOrderload()
        {
            $data=$_POST['id'];

            $this->db->select('*');
            $this->db->where('id',$data);
            $result=$this->db->get('orders');

    		foreach($result->result() as $roe){
    			$code = $roe->code;
    			
    			$this->db->select('*');
    			$this->db->where('order_code',$code);
    			$result=$this->db->get('orderload');
    			
    			return $result->result_array();
    		}
        }

    public function getAdditionalCost()
        {
            $data=$_POST['id'];

            $this->db->select('*');
            $this->db->where('id',$data);
            $result=$this->db->get('orders');

            foreach($result->result() as $roe){
                $code = $roe->code;
                
                $this->db->select('a.*,b.type as tipe');
                $this->db->join('costcomponents b','a.cost_id = b.id');
                $this->db->where('a.order_id',$code);
                $result=$this->db->get('cost a');
                
                return $result->result_array();
            }
        }

    public function adds()
        {
            $this->db->select('*');
            $this->db->where('active', '1');
            $this->db->where('type', '2');
            $result = $this->db->get('costcomponents');

          return $result->result_array();
        }
        
    public function addss()
        {
            $this->db->select('*');
            $this->db->where('active', '1');
            $this->db->where('type', '1');
            $result = $this->db->get('costcomponents');

          return $result->result_array();
        }
    
    public function editCustomer()
        {
            $id                 = $this->input->post('code');
            $data['name']       = $this->input->post('name');
            $data['address']    = $this->input->post('address');
            $data['phone']      = $this->input->post('phone');
            $data['pic_name']   = $this->input->post('pic_name');
            $data['email']      = $this->input->post('email');
            $data['nickname']   = $this->input->post('nickname');
            $data['term']       = $this->input->post('term');
            $data['ppn']        = $this->input->post('ppn');
            $data['active']     = 1;
            
            $this->db->where('code', $id);
            $this->db->update('customers', $data); 
        }

    public function getLastID()
        {
            $this->db->select('*');
            $this->db->order_by("code", "desc");
            $result = $this->db->get('orders',1);
            return $result->result_array();
        }

    public function Simpan()
        {
            
    		$user = $this->session->userdata('id');
    		
    		$frame  	= $this->input->post('frameno');
            $machine    = $this->input->post('machineno');
            $color      = $this->input->post('color');
            $type       = $this->input->post('type');

            $desc      = $this->input->post('desc');
            $costadd      = $this->input->post('costadd');
            $nomine      = $this->input->post('nomine');

            $a = count($desc);
            $c = $a + 2;

    		if ($a >= 1 ) {
                
           
    		
            $or =array();
            $ujsadd2 = 0;
                for($i=2; $i < $c; $i++) 
                {
                    $ujsadd2 += $nomine[$i];
                    $ad[$i] = array(
                    'order_id' =>$this->input->post('code'),
                    'cost_id' => $costadd[$i], 
                    'nominal' => $nomine[$i], 
                    'createby' => $this->session->userdata('id')
                    );

                }
            //print_r($test2);
            $this->db->insert_batch('cost', $ad);

            $data['code']           = $this->input->post('code');
            $tgl                    = $this->input->post('orderdate');
            $jam                    = $this->input->post('orderdatetime');
            $ujsawal                = $this->input->post('allowance'); //UANG JALAN 
            $data['orderdate']      = $tgl." ".$jam;
            $data['id_customers']   = $this->input->post('id_customers');
            $data['description']    = $this->input->post('description');
            $data['id_fleets']      = $this->input->post('id_fleets');
            $data['id_drivers']     = $this->input->post('id_drivers');
            $data['id_drivers2']     = $this->input->post('id_drivers2');
            $data['shipment']       = $this->input->post('id_shippment');
            $data['id_citieso']     = $this->input->post('id_citieso');
            $data['id_citiesd']     = $this->input->post('id_citiesd');
            $data['id_ordertypes']  = $this->input->post('id_ordertypes');
            $data['loadqty']        = $this->input->post('loadqty');
            $data['id_routes']      = $this->input->post('id_routes');
            $data['prices']         = $ujsawal + $ujsadd2  ; //UANG JALAN SUPIR   
            $data['allowances']      = $this->input->post('price'); // UANG UNIT

            $data['createby']       = $this->session->userdata('id');
            $data['active']         = 1;
            $data['ritase']         = 1;
                    
            $this->db->insert('orders',$data);

            $orde =array();
                for($i=0; $i < $data['loadqty']; $i++) 
                {
                    $dt[$i] = array(
                    'order_code' =>$this->input->post('code'),
                    'frameno' => $frame[$i], 
                    'machineno' => $machine[$i], 
                    'color' => $color[$i],
                    'type' => $type[$i]
                    );
                }

            $this->db->insert_batch('orderload', $dt);


    		
    		$this->db->select('*');
    		$this->db->where('id', $user);
            $result = $this->db->get('users');
          
    		
    		foreach ($result->result_array() as $row){
                $iddc = $row['id'];
                $a    = $row['nominal'];
                $b    = $ujsawal + $ujsadd2;
                
                $c = $a - $b;
                
                $dat['nominal']     = $c;
                
                $this->db->where('id', $iddc);
                $this->db->update('users', $dat);
                
            }
            } else {
                $data['code']           = $this->input->post('code');
                $tgl                    = $this->input->post('orderdate');
                $jam                    = $this->input->post('orderdatetime');
                $ujsawal                = $this->input->post('allowance'); //UANG JALAN 
                $data['orderdate']      = $tgl." ".$jam;
                $data['id_customers']   = $this->input->post('id_customers');
                $data['description']    = $this->input->post('description');
                $data['id_fleets']      = $this->input->post('id_fleets');
                $data['id_drivers']     = $this->input->post('id_drivers');
                $data['id_drivers2']     = $this->input->post('id_drivers2');
                $data['shipment']       = $this->input->post('id_shippment');
                $data['id_citieso']     = $this->input->post('id_citieso');
                $data['id_citiesd']     = $this->input->post('id_citiesd');
                $data['id_ordertypes']  = $this->input->post('id_ordertypes');
                $data['loadqty']        = $this->input->post('loadqty');
                $data['id_routes']      = $this->input->post('id_routes');
                $data['prices']         = $ujsawal; //UANG JALAN SUPIR   
                $data['allowances']      = $this->input->post('price'); // UANG UNIT

                $data['createby']       = $this->session->userdata('id');
                $data['active']         = 1;
                $data['ritase']         = 1;
                        
                $this->db->insert('orders',$data);

                $orde =array();
                    for($i=0; $i < $data['loadqty']; $i++) 
                    {
                        $dt[$i] = array(
                        'order_code' =>$this->input->post('code'),
                        'frameno' => $frame[$i], 
                        'machineno' => $machine[$i], 
                        'color' => $color[$i],
                        'type' => $type[$i]
                        );
                    }

                $this->db->insert_batch('orderload', $dt);


                
                $this->db->select('*');
                $this->db->where('id', $user);
                $result = $this->db->get('users');
              
                
                foreach ($result->result_array() as $row){
                    $iddc = $row['id'];
                    $a    = $row['nominal'];
                    $b    = $ujsawal;
                    
                    $c = $a - $b;
                    
                    $dat['nominal']     = $c;
                    
                    $this->db->where('id', $iddc);
                    $this->db->update('users', $dat);
                    
                }
            }
        }

    public function simpanPareto_()
        {
            
            $user = $this->session->userdata('id');
            
            $frame   = $this->input->post('frameno');
            $machine    = $this->input->post('machineno');
            $color      = $this->input->post('color');
            $type       = $this->input->post('type');
            $bongkar    = $this->input->post('p_scnt');
            $ujs        = $this->input->post('ujs');
            $sales      = $this->input->post('sales');

            $desc      = $this->input->post('desc');
            $costadd      = $this->input->post('costadd');
            $nomine      = $this->input->post('nomine');

            $aeee = count($desc);
            $beee = count($bongkar);
            $c = $aeee + 2;
            $d = $beee +