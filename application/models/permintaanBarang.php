<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class permintaanBarang extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ambilEmployee(){

        $this->db->select('u.*, kat.*');
            $this->db->join('mst_kat_emp kat','u.emp_kategori = kat.kat_emp_id', 'right');
            $this->db->where('u.emp_aktif',1);
            $this->db->where('u.emp_kategori',48);
            $result = $this->db->get('mst_employee u');
        return $result->result_array();
        
    } 
    
    public function ambilNomor(){

        $now=date('m-Y');
        $this->db->select('*');
        $this->db->order_by("dpb_id", "desc");
        $this->db->like('dpb_createtime', $now, 'before');
        $result = $this->db->get('data_permintaan_barang');
        return $result->result_array();
        
    }   
 
    public	function listPermintaanBarang(){

    	$this->db->select('u.*, p.*,sr.*, kat.*');
        $this->db->join('mst_armada kat','u.dpb_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dpb_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dpb_status = p.s_id', 'right');
        $this->db->where('u.dpb_code IS NOT NULL',NULL, FALSE);
        $this->db->where('u.dpb_status','0');
        $result = $this->db->get('data_permintaan_barang u');
        return $result->result_array();
    }

    public  function listPermintaanBarangApproved(){

        $this->db->select('u.*, sr.emp_code AS ecode,sr.emp_nama AS enama, kat.arm_nomor_pol AS armname,');
        $this->db->join('mst_armada kat','u.dpb_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dpb_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dpb_status = p.s_id', 'right');
        $this->db->where('u.dpb_status','9');
        $result = $this->db->get('data_permintaan_barang u');
        return $result->result_array();
    }

    public  function listPermintaanBarangAll(){

        $this->db->select('u.*,p.*, sr.*, kat.*');
        $this->db->join('mst_armada kat','u.dpb_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dpb_emp_id = sr.emp_id', 'right');
        $this->db->join('mst_status p','u.dpb_status = p.s_id', 'right');
        $this->db->where('u.dpb_code IS NOT NULL',NULL, FALSE);
        $result = $this->db->get('data_permintaan_barang u');
        return $result->result_array();
    }	

   	public	function detailPermintaanBarang(){

   		$idpb=$_GET['id'];

    	$this->db->select('u.*,p.*,o.*,sr.*, kat.*,');
        $this->db->join('mst_armada kat','u.dpb_arm_id = kat.arm_id', 'right');
        $this->db->join('mst_employee sr','u.dpb_emp_id = sr.emp_id', 'right');
        $this->db->join('data_permintaan_barang_item p','u.dpb_code = p.dpbi_dpb_id', 'right');
        $this->db->join('mst_inv_item o','p.dpbi_item_id = o.ii_id', 'right');
        $this->db->where('u.dpb_code',$idpb);
        $result = $this->db->get('data_permintaan_barang u');
        return $result->result_array();
    }


    

    public  function approveSPB(){

        $idpb=$_GET['id'];

        $this->db->query("UPDATE data_permintaan_barang SET dpb_status = '9' WHERE dpb_code = '$idpb'");
    }


    public function simpanPermintaan($idp){
    		
				date_default_timezone_set("Asia/Jakarta");
	            $nowdate = date('ydmGis');
	            $now=date('d-m-Y');
	                                   
    	$detail = $this->input->post('detail');
				$data['dpb_code'] 		    = $this->input->post('idspb');
				$data['dpb_date']           = $this->input->post('tanggal');
				$data['dpb_arm_id']			= $this->input->post('armada');
				$data['dpb_emp_id']			= $this->input->post('user');
				$data['dpb_status']			= "0";
				$data['dpb_createtime']		= $now;
				$data['dpb_createby']		= $idp;
				
        $this->db->insert('data_permintaan_barang',$data);
     
        foreach($detail as $details)
        {	$datad['dpbi_dpb_id'] 	= $this->input->post('idspb');
            $datad['dpbi_item_id']  = $details['items'];
			$datad['dpbi_count']    = $details['qty'];
            $datad['dpbi_note']     = $details['note'];
			$datad['dpbi_status']     ='0';
        
            $this->db->insert('data_permintaan_barang_item',$datad);
        }
        
    }   
    
}   