<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class returnDOS extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ss(){

        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('cities');

        return $result->result_array();
        
    }   

   public function getReturnDO($data = null)
        {

            //$this->db->select('a.*,b.name as n1,c.name as c2, d.name as cname, g.fleetplateno as fleet, h.name as driver');
            //$this->db->join('cities b','a.id_citieso = b.id', 'left');
            //$this->db->join('cities c','a.id_citiesd = c.id');
            //$this->db->join('customers d','a.id_customers = d.id', 'left');
            //$this->db->join('fleets g','a.id_fleets = g.id', 'left');
            //$this->db->join('drivers h','a.id_drivers = h.id', 'left');
            //$this->db->where('a.active', '1');
            //$result = $this->db->get('orders a',$rowperpage,$rowno);

            //return $result->result_array();

    $where = "";
    if($this->input->post('id_customer') != '') {                
                $text = $this->input->post('id_customer');
                $where .= " AND o.id_customers = '".$text."'";
            }
            if($this->input->get('id_customer') != '') {                
                $textq = $this->input->get('id_customer');
                $where .= " AND o.id_customers = '".$textq."'";
            }   
            if($this->input->post('start') > 0 && $this->input->post('end') > 0){
                $where .= " AND o.date BETWEEN '".$this->input->post('start')."' AND '".$this->input->post('end')."'";
            }

            if($this->input->get('start') > 0 && $this->input->get('end') > 0){
                $where .= " AND o.date BETWEEN '".$this->input->get('start')."' AND '".$this->input->get('end')."'";
            }

            if($this->input->post('frameno') != '') {    
                $text2 = $this->input->post('frameno');
                $trim2 = trim(preg_replace('/\s+/', "','", $text2));
                $where .= " AND i.frameno IN ('".str_replace(" ", "','",$trim2)."')";
            }
            if($this->input->get('frameno') != '') {    
                $text2 = $this->input->get('frameno');
                $trim2 = trim(preg_replace('/\s+/', "','", $text2));
                $where .= " AND i.frameno IN ('".str_replace(" ", "','",$trim2)."')";
                
            }
        


       
        
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
            SELECT
    * 
FROM
    (
    SELECT
        ROW_NUMBER ( ) OVER ( ORDER BY A.ritases DESC ) AS RowNum,
        A.* 
    FROM
        (
        SELECT
            o.code,
            o.id,
            o.noVoucher,
            o.orderdate,
            o.ritase,
            o.loadqty,
            o.pricesadd,
            o.prices,
            o.allowances,
            o.allowanceadds,
            h.name AS driver,
            g.fleetplateno AS fleet,
            c.name AS c2,
            b.name AS n1,
            f.distance,
            d.name AS cname,
            j.name AS moda,
            i.unitprice,
            i.id as idol,
            i.frameno as nokont,
            i.noshippent,
            i.rdo,
            i.type,
            i.color,
            f.feewash,
            f.feesaving,
            j.nominal,
            COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,
            COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,
            COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,
            COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,
            COALESCE ( SUM ( o.prices ), 0 ) AS pricess 
        FROM
            orders o
            LEFT JOIN orderload i ON o.code = i.order_code
            LEFT JOIN cities b ON i.id_citieso = b.id
            LEFT JOIN cities c ON i.id_citiesd = c.id
            LEFT JOIN customers d ON o.id_customers = d.id
            LEFT JOIN routes f ON i.id_routes = f.id
            LEFT JOIN fleets g ON o.id_fleets = g.id
            LEFT JOIN fleettypes j ON g.id_fleettypes = j.id
            LEFT JOIN drivers h ON o.id_drivers = h.id 
        WHERE
            o.active = 1 
            $where
        GROUP BY
            o.id,
            i.id,
            o.code,
            o.orderdate,
            o.loadqty,
            j.nominal,
            o.ritase,
            o.pricesadd,
            o.allowanceadds,
            o.prices,
            f.feewash,
            o.allowances,
            f.feesaving,
            f.distance,
            o.noVoucher,
            i.rdo,
            i.unitprice,
            i.noshippent,
            i.frameno,
            i.type,
            i.color,
            g.fleetplateno,
            g.CODE,
            g.fleetnumber,
            d.NAME,
            j.name,
            b.name,
            c.name,
            h.name 
        ) A 
    ) B 
WHERE
    B.id IS NOT NULL
            $paging
        ";
        
        $result = $this->db->query($sql);
        return $result->result(); 
            
        } 

    public function editCities()
    {
       
        date_default_timezone_set("Asia/Bangkok");
        $tanggal = date("d-m-y H:i:s");
        $id     = $this->input->post('id');
        $data['name']           = $this->input->post('cities');
        $data['updateby']       = $this->session->userdata('id');;
        $data['updatedatetime'] = $tanggal;
        $data['active']         = 1;
                
        
        $this->db->where('id', $id);
        $this->db->update('cities',$data);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('cities',1);
        return $result->result_array();
    }

    public function Simpan()
    {

        date_default_timezone_set("Asia/Bangkok");
        $tanggal = date("d-m-y H:i:s");
        $data['id']       = $this->input->post('id');
        $data['name']       = $this->input->post('cities');
        $data['createby']       = $this->session->userdata('id');;
        $data['createdatetime']       = $tanggal;
        $data['active']     = 1;
                
        $this->db->insert('cities',$data);
    }
    
    public function Delete($value='')
    {
        $id=$_GET['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('cities', $data); 
    }
}