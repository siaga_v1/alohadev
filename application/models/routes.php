<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class routes extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getRoutes(){

        $this->db->select('a.*,b.name as n1,c.name as c2, d.name as cname,f.name as type');
        $this->db->join('cities b','a.id_citieso = b.id', 'left');
        $this->db->join('cities c','a.id_citiesd = c.id', 'left');
        $this->db->join('customers d','a.id_customers     = d.id', 'left');
		$this->db->join('fleettypes f','a.id_ordertypes      = f.id', 'left');
        $this->db->where('a.active', '1');
        $this->db->order_by('a.id', 'ASC');
        $result = $this->db->get('routes a');
		
       // $sql = "
       // SELECT A.* FROM 
       //     ( SELECT ROW_NUMBER() OVER (Order by a.id) AS RowNum, 
       //         a.*,
       //         b.name AS n1,
       //        c.name AS c2
       //       FROM routes a
       //         LEFT JOIN cities b ON a.id_citieso = b.id
       //         LEFT JOIN cities c ON a.id_citiesd = c.id
       //       WHERE
       //         a.active = 1
       //     )A
       // WHERE 
       //     A.id IS NOT NULL
       // ";
        
       // $result = $this->db->query($sql);
        return $result->result();
        //return $result->result();
        
    }   
	
	public function getOneOrder()
    {
        $data=$_POST['id'];

        $this->db->select('*');
        $this->db->where('id',$data);
        $result=$this->db->get('routes');

        return $result->result();
    }
	
    function selectOne($data = null)
    {
        if($data['id'] > 0){
            $this->db->where('id', $data['id']);
            $result = $this->db->get('customers');
            return $result->row();
        }
    }

    public function editRoute()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id                 = $this->input->post('code');
        $data['id_citieso']       = $this->input->post('id_citieso');
        $data['id_citiesd']       = $this->input->post('id_citiesd');
        $data['id_customers']       = $this->input->post('id_customer');
        $data['id_ordertypes']       = $this->input->post('type');
        $data['distance']       = $this->input->post('distance');
        $data['allowance']       = str_replace(",", "",$this->input->post('allowance'));
        $data['feesaving']       = str_replace(",", "",$this->input->post('savings'));
        $data['feesales']       = str_replace(",", "",$this->input->post('sales'));
        $data['feewash']       = str_replace(",", "",$this->input->post('wash'));
        $data['ujs']       = str_replace(",", "",$this->input->post('driver'));
        $data['updatedatetime']       = date('d-m-Y h:i:s');
        $data['active']     = 1;
        
        $this->db->where('code', $id);
        $this->db->update('routes', $data); 
        //print_r($data);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('routes',1);
        return $result->result_array();
    }

    public function Simpan()
    {
        $a= $this->input->post('price');
        $b= $this->input->post('savings');
        $c= $this->input->post('sales');
        $d= $this->input->post('wash');
        $e= $this->input->post('driver');


        $data['code']       = $this->input->post('code');
        $data['id_citieso']       = $this->input->post('id_citieso');
        $data['id_citiesd']       = $this->input->post('id_citiesd');
        $data['id_customers']       = $this->input->post('id_customer');
        $data['id_ordertypes']       = $this->input->post('type');
        $data['distance']       = $this->input->post('distance');
        $data['allowance']       = str_replace(",", "", $a);
        $data['feesaving']       = str_replace(",", "", $b);
        $data['feesales']       = str_replace(",", "", $c);
        $data['feewash']       = str_replace(",", "", $d);
        $data['ujs']       = str_replace(",", "", $e);
        $data['active']     = 1;
                
        $this->db->insert('routes',$data);
    }
    
    public function delete($value='')
    {
        $id=$_GET['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('routes', $data); 
    }
}