<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class selfdrives extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getCostComponent(){

        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('costcomponents');

        return $result->result_array();
        
    }   

    public function getAdditionalCost(){

        $this->db->select('a.*, b.name as nama');
        $this->db->join('costcomponents b','a.cost_id = b.id','left');
        $this->db->where('a.active', '1');
        $result = $this->db->get('cost a');

        return $result->result_array();
        
    }   

    public function getOneSelfDrive()
    {
        $data=$_POST['id'];

        $this->db->select('*');
        $this->db->where('id',$data);
        $result = $this->db->get('orderadd');

        return $result->result();
    } 

    public function getOneRequest()
    {
        $data=$_POST['id'];
        $this->db->select('*');
        $this->db->where('id', $data);
        $result = $this->db->get('requestpayable');
        return $result->result();
    }

    public function getKoordinator()
    {
        $this->db->select('*');
        $this->db->where('id_userroles', '3');
        $result = $this->db->get('users');
        return $result->result_array();
    }
    
    public function getCredit(){
        $akun = $this->session->userdata('id');
        $this->db->select('a.*, b.firstname as nama');
        $this->db->join('users b','a.createby = b.id','left');
        $this->db->where('a.active', '1');
        $this->db->where('a.createby', $akun);
        $result = $this->db->get('orders a');

        return $result->result_array();
        
    } 

    function selectOne($data = null)
    {
        if($data['id'] > 0){
            $this->db->where('id', $data['id']);
            $result = $this->db->get('debitcredit');
            return $result->row();
        }
    }

    public function editDebit()
    {   $nominal = $this->input->post('nomnals');
        $ntk = str_replace(",", "", $nominal);
        $id          = $this->input->post('code');
        $data['id_employees']   = $this->input->post('nama');
        $data['dates']          = $this->input->post('date');
        $data['desc_1']         = $this->input->post('necesary');
        $data['desc_2']         = $this->input->post('descripton');
        $data['price']          = $ntk;
        $data['active']         = 1;
        
        $this->db->where('code', $id);
        $this->db->update('debitcredit',$data);
    }

    public function editRequest()
    {
        $id          = $this->input->post('id');
        $data['necessary']         = $this->input->post('necessary');
        $data['desciption']         = $this->input->post('desc');
        $data['price']          = $this->input->post('nominals');
        
        $this->db->where('id', $id);
        $this->db->update('requestpayable',$data);
    }

    public function approvedebit()
    {
        $id          = $this->input->post('code');
        $nama        = $this->input->post('nama');

        //
        $this->db->select('*');
        $this->db->where('firstname', $nama);
        $result = $this->db->get('users');
        foreach ($result->result() as $key) {
            $awal   = $key->nominal;
        }

        
        $tambah = $this->input->post('nominal');

        $add['nominal']        = $awal + $tambah;
        $data['status']        = "VALID";
        $data['trfdate']       = $this->input->post('trfdate');
        
        $this->db->where('code', $id);
        $this->db->update('debitcredit',$data);

        $this->db->where('firstname', $nama);
        $this->db->update('users',$add);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('cost',1);
        return $result->result_array();
    }

    public function getSelfDrive()
    {   
        $this->db->select('*');
        $result = $this->db->get('orderadd');
        return $result->result_array();
    }

   
    public function simpan()
    {
        date_default_timezone_set("Asia/Bangkok");
        $frame = $this->input->post('noka');
        $frame2 = explode(" / ", $frame);
        $data['no_frame']      = $frame2[1];
        $data['date']         = $this->input->post('ecdate');
        $data['ordercode']    = $this->input->post('ordercode');
        $data['type']         = $this->input->post('type');
        $data['color']        = $this->input->post('color');
        $data['description']  = $this->input->post('description');
        $data['nominals']     = $this->input->post('nominals');
        $data['active']       = 1;
        $data['createby']     = $this->session->userdata('id');
        $data['createdatetime']   = date("Y-m-d h:i:s");
                
        $this->db->insert('orderadd',$data);
    }
    
    public function saveEdit()
    {
        date_default_timezone_set("Asia/Bangkok");
        $frame = $this->input->post('norang');
        $id = $this->input->post('codes');
        $frame2 = explode(" / ", $frame);
        if (is_null($frame2[1])) {
            $kode = $frame;
        } else { $kode = $frame2[1]; }
        $data['no_frame']     = $kode;
        $data['date']         = $this->input->post('ecdates');
        $data['ordercode']    = $this->input->post('ordercodes');
        $data['type']         = $this->input->post('types');
        $data['color']        = $this->input->post('colors');
        $data['description']  = $this->input->post('descriptions');
        $data['nominals']     = $this->input->post('nominalss');
        $data['active']       = 1;
        $data['updateby']     = $this->session->userdata('id');
        $data['updatebdatetime']   = date("Y-m-d h:i:s");
                
        
        $this->db->where('id', $id);
        $this->db->update('orderadd', $data); 
    }

    public function Delete($value='')
    {
        $id=$_GET['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('customers', $data); 
    }
}