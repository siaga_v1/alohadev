<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class summarySlips extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function ss(){

        $this->db->select('*');
        $this->db->where('active', '1');
        $result = $this->db->get('cities');

        return $result->result_array();
        
    } 

    public function printInvoice(){
        $id = $this->input->post('code');
        $result = $this->db->query("SELECT
                                    dbo.invoices.noinvoice,
                                    dbo.invoices.code,
                                    dbo.invoices.dateInvoice,
                                    dbo.invoices.faktur,
                                    dbo.customers.address_1,
                                    dbo.customers.npwp,
                                    dbo.customers.nickname,
                                    dbo.customers.address_2,
                                    dbo.customers.address_3,
                                    dbo.customers.cabang,
                                    dbo.customers.norekening,
                                    dbo.invoices.nosummary,
                                    dbo.invoices.nominal,
                                    dbo.invoices.ppn,
                                    dbo.customers.name 
                                FROM
                                    dbo.invoices
                                    INNER JOIN dbo.customers ON dbo.invoices.id_customers = dbo.customers.id 
                                WHERE
                                    dbo.invoices.nosummary = '$id'");

        return $result->result();
        
    } 

    public function listsummary(){

        $this->db->select('a.*, b.name as cname');
        $this->db->where('a.active', '1');
        $this->db->join('customers b','a.id_customers = b.id', 'left');

        $result = $this->db->get('invoices a');

        return $result->result();
        
    }   

    public function invoiceDetail()
    {

        $id = $this->input->post('id');

        $sql= "SELECT o.id,o.noVoucher,o.orderdate,e.nosummary,o.ritase,e.id AS idol,e.type,e.unitprice,e.frameno,e.noshippent,o.prices,b.name AS n1,c.name AS c2,d.name AS cname,g.fleetplateno AS fleet,h.name AS driver,COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,COALESCE ( SUM ( o.prices ), 0 ) AS pricess FROM orders o LEFT JOIN cities b ON o.id_citieso = b.id LEFT JOIN cities c ON o.id_citiesd = c.id LEFT JOIN customers d ON o.id_customers = d.id LEFT JOIN orderload e ON o.code = e.order_code LEFT JOIN fleets g ON o.id_fleets = g.id LEFT JOIN drivers h ON o.id_drivers = h.id WHERE o.active = 1 AND e.nosummary = $id GROUP BY o.id,
                            o.noVoucher,
                            o.code,
                            o.orderdate,
                            o.ritase,
                            e.id,
                            e.type,
                            e.noshippent,
                            e.unitprice,
                            e.frameno,
                            o.prices,
                            e.nosummary,
                            g.fleetplateno,
                            g.CODE,
                            g.fleetnumber,
                            d.NAME,
                            b.name,
                            c.name,
                            h.name";
        $result = $this->db->query($sql);

        return $result->result();
        
    }

    public function getReturnDO($data = null)
        {

        $where = "";
        
        if($this->input->post('code') != '') {
            $where .= " AND o.code LIKE '%".$this->input->post('code')."%' ";
        }


        ///////


        if($this->input->get('code') != '') {
            $where .= " AND o.code LIKE '%".$this->input->get('code')."%' ";
        }

       
        
        $paging = "";
        if(isset($data['limit']) ){
            $limit  = $data['limit'];
            $page   = isset($data['page']) ? $data['page'] : 1;
            $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
            $last   = ($page > 1) ? ($limit*$page) : $limit;
            $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
        }
        
        $sql = "
            SELECT
                * 
            FROM
                (
                SELECT
                    ROW_NUMBER () OVER ( ORDER BY A.ritases DESC ) AS RowNum,
                    A.* 
                FROM
                    (
                    SELECT
            o.id,
            o.code,
            o.orderdate,
            o.shipment,
            o.ritase,
            j.name AS moda,
            o.prices,
            t.datehoaccept,
            b.name AS n1,
            c.name AS c2,
            d.name AS cname,
            g.fleetplateno AS fleet,
            h.name AS driver,
            COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,
            COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,
            COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,
            COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,
            COALESCE ( SUM ( o.prices ), 0 ) AS pricess 
        FROM
            orders o
            LEFT JOIN cities b ON o.id_citieso = b.id
            LEFT JOIN cities c ON o.id_citiesd = c.id
            LEFT JOIN customers d ON o.id_customers = d.id
            LEFT JOIN fleets g ON o.id_fleets = g.id
            LEFT JOIN fleettypes j ON g.id_fleettypes = j.id            
            LEFT JOIN drivers h ON o.id_drivers = h.id 
            LEFT JOIN ordercontrols t ON o.code = t.id_order
        WHERE
            t.id_order IS NULL
            AND o.rdo ='YES'
            
            $where
        GROUP BY
            o.id,
            o.code,
            o.orderdate,
            o.ritase,
            o.prices,
            o.shipment,
            t.datehoaccept,
            j.name,
            g.fleetplateno,
            g.CODE,
            g.fleetnumber,
            d.NAME,
            b.name,
            c.name,
            h.name 
                        ) A 
                    ) B 
                WHERE
                    B.id IS NOT NULL
            $paging
        ";
        
        $result = $this->db->query($sql);
        return $result->result(); 
            
        } 


    public function Search($data = null)
        {
            $where = "";

            if($this->input->post('inputan') != '') {                
                $text = $this->input->post('inputan');
                //$data = explode("\n", $text);
                $trim = trim(preg_replace('/\s+/', "','", $text));
                $where .= " AND e.noshippent IN ('".$trim."')";
            }
            if($this->input->post('inputan2') != '') {    
                $text2 = $this->input->post('inputan2');
                //$data2 = explode("\n", $text2);
                $trim2 = trim(preg_replace('/\s+/', "','", $text2));
                $where .= " AND e.frameno IN ('".$trim2."')";
            }
            /////
            if($this->input->get('inputan') != '') {                
                $textq = $this->input->get('inputan');
                //$data = explode("\n", $textq);
                $trim = trim(preg_replace('/\s+/', "','", $textq));
                $where .= " AND e.noshippent IN ('".$trim."')";
            }
            if($this->input->get('inputan2') != '') {    
                $text2 = $this->input->get('inputan2');
                //$data2 = explode("\n", $text2);
                $trim2 = trim(preg_replace('/\s+/', "','", $text2));
                $where .= " AND e.frameno IN ('".$trim2."')";
                
            }

            $paging = "";

            if(isset($data['limit']) ){
                $limit  = $data['limit'];
                $page   = isset($data['page']) ? $data['page'] : 1;
                $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
                $last   = ($page > 1) ? ($limit*$page) : $limit;
                $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
            }

            $sql = "
                SELECT
                    * 
                FROM
                    (
                    SELECT
                        ROW_NUMBER () OVER ( ORDER BY A.ritases DESC ) AS RowNum,
                        A.* 
                    FROM
                        (
                        SELECT
                            o.id,
                            o.noVoucher,
                            o.orderdate,
                            e.nosummary,
                            o.ritase,
                            e.rdo,
                            o.id_customers,
                            e.id AS idol,
                            e.type,
                            o.allowances,
                            e.unitprice,
                            e.machineno,
                            e.irisno,
                            j.name as moda,
                            e.frameno,
                            e.noshippent,
                            o.prices,
                            b.name AS n1,
                            c.name AS c2,
                            d.name AS cname,
                            g.fleetplateno AS fleet,
                            h.name AS driver,
                            COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,
                            COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,
                            COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,
                            COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,
                            COALESCE ( SUM ( o.prices ), 0 ) AS pricess 
                        FROM
                            orders o
                            LEFT JOIN orderload e ON o.code = e.order_code
                            LEFT JOIN cities b ON e.id_citieso = b.id
                            LEFT JOIN cities c ON e.id_citiesd = c.id
                            LEFT JOIN customers d ON o.id_customers = d.id
                            LEFT JOIN fleets g ON o.id_fleets = g.id
                            LEFT JOIN fleettypes j ON g.id_fleettypes = j.id            

                            LEFT JOIN drivers h ON o.id_drivers = h.id 
                        WHERE
                            o.active = 1
                            AND o.noVoucher IS NOT NULL 
                            $where
                        GROUP BY
                            o.id,
                            o.noVoucher,
                            o.code,
                            o.orderdate,
                            o.ritase,
                            o.allowances,
                            e.id,
                            e.rdo,
                            j.name,
                            o.id_customers,
                            e.type,
                            e.noshippent,
                            e.machineno,
                            e.irisno,
                            e.unitprice,
                            e.frameno,
                            o.prices,
                            e.nosummary,
                            g.fleetplateno,
                            g.CODE,
                            g.fleetnumber,
                            d.NAME,
                            b.name,
                            c.name,
                            h.name 
                        ) A 
                    ) B 
                WHERE
                    B.id IS NOT NULL
                $paging";
        
            $result = $this->db->query($sql);
            return $result->result(); 
            
        } 

    /*public function Searchtrisya($data = null)
        {
            $where = "";

            if($this->input->post('inputan') != '') {                
                $text = $this->input->post('inputan');
                $data = explode(' ', $text);
                $where .= " AND e.noshippent IN ('".str_replace(" ", "','",$text)."')";
            }
            if($this->input->post('inputan2') != '') {    
                $text2 = $this->input->post('inputan2');
                $data2 = explode(' ', $text2);
                $where .= " AND e.frameno IN ('".str_replace(" ", "','",$text2)."')";
            }
            /////
            if($this->input->get('inputan') != '') {                
                $textq = $this->input->get('inputan');
                $data = explode(' ', $textq);
                $where .= " AND e.noshippent IN ('".str_replace(" ", "','",$textq)."')";
            }
            if($this->input->get('inputan2') != '') {    
                $text2 = $this->input->get('inputan2');
                $data2 = explode(' ', $text2);
                $where .= " AND e.frameno IN ('".str_replace(" ", "','",$text2)."')";
                
            }

            $paging = "";

            if(isset($data['limit']) ){
                $limit  = $data['limit'];
                $page   = isset($data['page']) ? $data['page'] : 1;
                $first  = ($page > 1) ? (($limit*$page)-$limit)+1 : 1;
                $last   = ($page > 1) ? ($limit*$page) : $limit;
                $paging .= " AND ( B.RowNum BETWEEN $first AND $last )";
            }

            $sql = "
                SELECT
                    * 
                FROM
                    (
                    SELECT
                        ROW_NUMBER () OVER ( ORDER BY A.ritases DESC ) AS RowNum,
                        A.* 
                    FROM
                        (
                        SELECT
                            o.id,
                            o.noVoucher,
                            o.orderdate,
                            e.nosummary,
                            o.ritase,
                            o.rdo,
                            o.id_customers,
                            e.id AS idol,
                            e.type,
                            o.allowances,
                            e.unitprice,
                            e.machineno,
                            e.irisno,
                            j.name as moda,
                            e.frameno,
                            e.noshippent,
                            o.prices,
                            b.name AS n1,
                            c.name AS c2,
                            d.name AS cname,
                            g.fleetplateno AS fleet,
                            h.name AS driver,
                            COALESCE ( SUM ( o.ritase ), 0 ) AS ritases,
                            COALESCE ( SUM ( o.allowances ), 0 ) AS allowancess,
                            COALESCE ( SUM ( o.allowanceadds ), 0 ) AS allowanceaddss,
                            COALESCE ( SUM ( o.loadqty ), 0 ) AS loadqtys,
                            COALESCE ( SUM ( o.prices ), 0 ) AS pricess 
                        FROM
                            orders o
                            LEFT JOIN orderload e ON o.code = e.order_code
                            LEFT JOIN cities b ON e.id_citieso = b.id
                            LEFT JOIN cities c ON e.id_citiesd = c.id
                            LEFT JOIN customers d ON o.id_customers = d.id
                            LEFT JOIN fleets g ON o.id_fleets = g.id
                            LEFT JOIN fleettypes j ON g.id_fleettypes = j.id            

                            LEFT JOIN drivers h ON o.id_drivers = h.id 
                        WHERE
                            o.active = 1 

                            $where
                        GROUP BY
                            o.id,
                            o.noVoucher,
                            o.code,
                            o.orderdate,
                            o.rdo,
                            o.ritase,
                            o.allowances,
                            e.id,
                            j.name,
                            o.id_customers,
                            e.type,
                            e.noshippent,
                            e.machineno,
                            e.irisno,
                            e.unitprice,
                            e.frameno,
                            o.prices,
                            e.nosummary,
                            g.fleetplateno,
                            g.CODE,
                            g.fleetnumber,
                            d.NAME,
                            b.name,
                            c.name,
                            h.name 
                        ) A 
                    ) B 
                WHERE
                    B.id IS NOT NULL
                $paging";
        
            $result = $this->db->query($sql);
            return $result->result(); 
            
        } */
    
    public function editCities()
    {
       
        date_default_timezone_set("Asia/Bangkok");
        $tanggal = date("d-m-y H:i:s");
        $id     = $this->input->post('id');
        $data['name']           = $this->input->post('cities');
        $data['updateby']       = $this->session->userdata('id');;
        $data['updatedatetime'] = $tanggal;
        $data['active']         = 1;
                
        
        $this->db->where('id', $id);
        $this->db->update('cities',$data);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('cities',1);
        return $result->result_array();
    }

    public function Simpan()
    {

        date_default_timezone_set("Asia/Bangkok");
        $tanggal = date("d-m-y H:i:s");
        $id            = $this->input->post('code');
        $data['noinvoice']      = $this->input->post('noInvoice');
        $data['id_customers']   = $this->input->post('id_customers');
        $data['faktur']   = $this->input->post('noFaktur');
        $data['dateinvoice']    = $this->input->post('dateInvoice');
        $data['term']           = $this->input->post('term');
        $data['nominal']        = $this->input->post('nominal');
        $data['ppn']            = $this->input->post('ppn');
        $data['createby']       = $this->session->userdata('id');;
        $data['createdatetime'] = $tanggal;
        $data['active']         = 1;
                
        $this->db->where('nosummary',$id);
        $this->db->update('invoices',$data);

        print_r($data);
    }

    public function Update()
    {

        date_default_timezone_set("Asia/Bangkok");
        $tanggal = date("d-m-y H:i:s");
        $data['id']       = $this->input->post('id');
        $data['name']       = $this->input->post('cities');
        $data['createby']       = $this->session->userdata('id');;
        $data['createdatetime']       = $tanggal;
        $data['active']     = 1;
                
        $this->db->insert('cities',$data);
    }
    
    public function Delete($value='')
    {
        $id=$_GET['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('cities', $data); 
    }
}