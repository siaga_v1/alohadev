<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem !! ');
//membuat suatu class
class targets extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getTarget(){

        $this->db->select('*');
        $result = $this->db->get('target');

        return $result->result_array();
        
    }   

   public function getOneOrder()
    {
        $data=$_POST['id'];

        $this->db->select('*');
        $this->db->where('id',$data);
        $result=$this->db->get('target');

        return $result->result_array();
    }

    public function editTarget()
    {
        $id      = $this->input->post('modalid');
        $data['month']       = $this->input->post('modalmonth');
        $data['year']       = $this->input->post('modalyear');
        $data['carcarrier']       = $this->input->post('modalcarcarrier');
        $data['tansya']       = $this->input->post('modaltansya');
        $data['towing']       = $this->input->post('modaltowing');
        $data['wingbox']       = $this->input->post('modalwingbox');
        $data['pareto']       = $this->input->post('modalpareto');
        $data['dso']       = $this->input->post('modaldso');
                
        
        $this->db->where('id', $id);
        $this->db->update('target',$data);
    }

    public function getLastID()
    {
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $result = $this->db->get('target',1);
        return $result->result_array();
    }

    public function Simpan()
    {
        $data['id']       = $this->input->post('id');
        $data['month']       = $this->input->post('month');
        $data['year']       = $this->input->post('year');
        $data['carcarrier']       = $this->input->post('carcarrier');
        $data['tansya']       = $this->input->post('tansya');
        $data['towing']       = $this->input->post('towing');
        $data['wingbox']       = $this->input->post('wingbox');
        $data['pareto']       = $this->input->post('pareto');
        $data['dso']       = $this->input->post('dso');
                
        $this->db->insert('target',$data);
    }
    
    public function Delete($value='')
    {
        $id=$_GET['id'];
        
        $data['active']     = 0;
            
        $this->db->where('id', $id);
        $this->db->update('cities', $data); 
    }
}