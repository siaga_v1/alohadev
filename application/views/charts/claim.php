<div class="ibox float-e-margins">
    <div class="ibox-title">
      <h5>TOP 10 Defect / Driver
        </h5>
    </div>
    <div class="ibox-content">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12">
					<p class="text-center">
					  <strong>Claim: January - April</strong>
					</p>
							  
					<div class="chart">
						<canvas id="areaChartClaim" style="height: 300px;"></canvas>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
		<div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover" style="font-size: 7pt;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Driver</th>
                            <th>Defect</th>
                            <th>Charges</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $x=1;
                        $totalcosts = 0;
                        $totalrit = 0;
                        $totalachieves = 0;
                        foreach($rows AS $row)
                        { 
                            if($x <= 50)
                            {
                        ?>
                        <tr>
                            <td><?=$x;?></td>
                            <td><?=$row->driver;?></td>
                            <td><?=$row->ritase;?></td>
                            <td><?=number_format($row->allowances);?></td>
                        </tr>
                        <?php
                            }
                            if($x == 50)
                            {
                        ?>
                                <tr>
                                    <td><?=$x;?></td>
                                    <td colspan="4"> ....</td>
                                </tr>
                        <?php
                            }
                            
                            $totalcosts += $row->allowances;
                            $totalrit += $row->ritase;
                        $x++;
                        } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2" align="center">Total</td>
                                <td><?=number_format($totalrit);?></td>
                                <td><?=number_format($totalcosts);?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
		</div>
        
        <!--<div class="row">
            <div id="claism" class="col-md-12">
            
            </div>
        </div>
        <script type="text/javascript">
        $(function(){
            $.ajax({
                url:'<?php echo base_url();?>index.php/dashboard/truck',
                type:'POST',
                data:'setting=truckachieveslow',
                success: function(echo){
                    $('#claism').html(echo);
                }
            });
        })
        </script>
        -->
    </div>
<script>
$(function(){
        var label = <?=$label?>;
        var datar = <?=$nums?>;
         var barData = {
            labels: label,
            datasets: [
                {
                    label: "Defect",
                    backgroundColor: 'rgba(66, 75, 245, 0.5)',
                    pointBorderColor: "#fff",
                    xAxisID : 0,
                    data: datar
                }
            ]
        };

        Chart.pluginService.register({
            beforeRender: function (chart) {
                if (chart.config.options.showAllTooltips) {
                    // create an array of tooltips
                    // we can't use the chart tooltip because there is only one tooltip per chart
                    chart.pluginTooltips = [];
                    chart.config.data.datasets.forEach(function (dataset, i) {
                        chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                            chart.pluginTooltips.push(new Chart.Tooltip({
                                _chart: chart.chart,
                                _chartInstance: chart,
                                _data: chart.data,
                                _options: chart.options,
                                _active: [sector]
                            }, chart));
                        });
                    });

                    // turn off normal tooltips
                    chart.options.tooltips.enabled = false;
                }
            },
            afterDraw: function (chart, easing) {
                if (chart.config.options.showAllTooltips) {
                    // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                    if (!chart.allTooltipsOnce) {
                        if (easing !== 1)
                            return;
                        chart.allTooltipsOnce = true;
                    }

                    // turn on tooltips
                    chart.options.tooltips.enabled = true;
                    Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                        tooltip.initialize();
                        tooltip.update();
                        // we don't actually need this since we are not animating tooltips
                        tooltip.pivot();
                        tooltip.transition(easing).draw();
                    });
                    chart.options.tooltips.enabled = false;
                }
            }
        })

        var barOptions = {
            responsive: true,
            tooltips: {
              callbacks: {
                title: function(tooltipItems, data) {
                  return '';
                },
                label: function(tooltipItem, data) {
                  var datasetLabel = '';
                  var label = data.labels[tooltipItem.index];
                  return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                }
              }
            },
        showAllTooltips: true,
            scales: {
              yAxes: [{
                ticks: {
                    beginAtZero: true
                }
              }]
             }
        };


        var ctx2 = document.getElementById("areaChartClaim").getContext("2d");
        new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});
});
</script>
</div>