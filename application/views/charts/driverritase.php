<div class="ibox float-e-margins">
    <div class="ibox-title">
    	<h5>TOP 10 Trucks Ritase /Driver
        	<small></small>
        </h5>
    </div>
    <div class="ibox-content">
    	<div class="row">
	    	<div class="col-md-8">
	        	<canvas id="driverritase" height="140"></canvas>
	        </div>
	        <div class="col-md-4">
                <div class="table-responsive">
                    <table class="table table-hover" style="font-size: 7pt;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Driver</th>
                            <th>Ritase</th>
                            <th>Sales</th>
                            <th>Costs</th>
                            <th>Gross Margin</th>
                            <th>GM(%)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $x=1;
                        $totalcosts = 0;
                        $totalprices = 0;
                        $totalachieves = 0;
                        $totalritase = 0;
                        foreach($rows AS $row)
                        { 
                            if($x <= 10)
                            {
                        ?>
                        <tr>
                            <td><?=$x;?></td>
                            <td><?=$row->name;?></td>
                            <td><?=$row->ritase;?></td>
                            <td><?=number_format($row->allowances);?></td>
                            <td><?=number_format($row->prices);?></td>
                            <td><?=number_format($row->achieves);?></td>
                            <td><?=$row->achieves > 0 && $row->prices > 0 ? number_format(($row->achieves/$row->prices)*100) : 0;?></td>
                        </tr>
                        <?php
                            }
                            if($x == 11)
                            {
                        ?>
                                <tr>
                                    <td><?=$x;?></td>
                                    <td colspan="4"> ....</td>
                                </tr>
                        <?php
                            }
                            
                            $totalcosts += $row->allowances;
                            $totalritase += $row->ritase;
                            $totalprices += $row->prices;
                            $totalachieves += $row->achieves;
                            
                        $x++;
                        } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2" align="center">Total</td>
                                <td><?=number_format($totalritase);?></td>
                                <td><?=number_format($totalcosts);?></td>
                                <td><?=number_format($totalprices);?></td>
                                <td><?=number_format($totalachieves);?></td>
                                <td><?=($totalachieves > 0 && $totalprices > 0) ? number_format(($totalachieves/$totalprices)*100) : 0;?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
        <div id="truckRitaseLowLoad" class="col-md-12">
        
        </div>
    </div>
    <script type="text/javascript">
    $(function(){
        $.ajax({
        	url:'<?php echo base_url();?>index.php/dashboard/truck',
            type:'POST',
        	data:'setting=truckritaselow',
            success: function(echo){
                $('#truckRitaseLowLoad').html(echo);
            }
        });
    })
    </script>

    </div>
</div>
<script type="text/javascript">
	$(function() {
		var	label = <?=$label?>;
		var	datar = <?=$dataritase?>;
		 var barData = {
	        labels: label,
	        datasets: [
	            {
	                label: "Data 1",
	                backgroundColor: 'rgba(25, 60, 244, 0.5)',
	                pointBorderColor: "#fff",
	                xAxisID : 0,
	                data: datar
	            }
	        ]
	    };


        Chart.pluginService.register({
            beforeRender: function (chart) {
                if (chart.config.options.showAllTooltips) {
                    // create an array of tooltips
                    // we can't use the chart tooltip because there is only one tooltip per chart
                    chart.pluginTooltips = [];
                    chart.config.data.datasets.forEach(function (dataset, i) {
                        chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                            chart.pluginTooltips.push(new Chart.Tooltip({
                                _chart: chart.chart,
                                _chartInstance: chart,
                                _data: chart.data,
                                _options: chart.options,
                                _active: [sector]
                            }, chart));
                        });
                    });

                    // turn off normal tooltips
                    chart.options.tooltips.enabled = false;
                }
            },
            afterDraw: function (chart, easing) {
                if (chart.config.options.showAllTooltips) {
                    // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                    if (!chart.allTooltipsOnce) {
                        if (easing !== 1)
                            return;
                        chart.allTooltipsOnce = true;
                    }

                    // turn on tooltips
                    chart.options.tooltips.enabled = true;
                    Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                        tooltip.initialize();
                        tooltip.update();
                        // we don't actually need this since we are not animating tooltips
                        tooltip.pivot();
                        tooltip.transition(easing).draw();
                    });
                    chart.options.tooltips.enabled = false;
                }
            }
        })

	    var barOptions = {
	        responsive: true,
            tooltips: {
              callbacks: {
                title: function(tooltipItems, data) {
                  return '';
                },
                label: function(tooltipItem, data) {
                  var datasetLabel = '';
                  var label = data.labels[tooltipItem.index];
                  return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                }
              }
            },
        showAllTooltips: true,
	        scales: {
		      yAxes: [{
		      	ticks: {
		        	beginAtZero: true
		        }
		      }]
   			 }
	    };


	    var ctx2 = document.getElementById("driverritase").getContext("2d");
	    new Chart(ctx2, {type: 'bar', data: barData, options: barOptions});
	})
</script>