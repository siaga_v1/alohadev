<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>TOP 10 Customers
            <small>With custom colors.</small>
        </h5>
    </div>
    <div class="ibox-content">
        <div class="col-md-12">
                <div class="table-responsive">
                
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Plate No.</th>
                                <th>Fleet Status</th>
                                <th>Order Status</th>
                                <th>Last Update</th>
                                <th>Address</th>
                                <th>Geofence</th>
                                <th>Speed</th>
                                <th>Odometer</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if($rows != null && count($rows) > 0){
                                foreach($rows AS $row)
                                {    
                                     $order_status_color = "background-color:yellow;";
                                    if($row->order_status_no == 1){
                                        $order_status_color = "background-color:green;color:white;";
                                    }
                                    
                                    $gps_status_color = "background-color:blue;color:white;";
                                    if(strtolower($row->gps_status) == 'inactive' ){
                                        $gps_status_color = "background-color:black;color:white;";
                                    }if(strtolower($row->gps_status) == 'idle' ){
                                        $gps_status_color = "background-color:orange;color:white;";
                                    }if(strtolower($row->gps_status) == 'stop'){
                                        $gps_status_color = "background-color:red;color:white;";
                                    }
                        ?>
                            <tr>
                                <td><a href="#" onclick="return openmaps('<?php echo $row->fleetplateno; ?>')"><?php echo $row->fleetplateno; ?></a></td>
                                <td style="<?=$gps_status_color?>"><?=$row->gps_status;?></td>
                                <td style="<?=$order_status_color;?>"><?=$row->order_status;?></td>
                                <td><?=$row->gps_datetime;?></td>
                                <td><?=$row->gps_address;?></td>
                                <td><?=$row->gps_hotport;?></td>
                                <td><?=$row->gps_speed;?></td>
                                <td><?=$row->gps_odometer;?></td>
                            </tr>
                        <?php 
                                }
                             } 
                        ?>
                        </tbody>
                    </table>
                
                </div>
            </div>
        </div>
    </div>
</div>

            

<script type="text/javascript">
function openmaps(plateno)
{
    $.ajax({
       url:"<?php echo base_url(); ?>fleet/lastposition",
       type:"POST",
       data:"plateno="+plateno,
       success: function(echo){
            $('#modal_openmaps .modal-body').html(echo);
            $('#modal_openmaps').modal();
       }
    });
    
    return false;
}

</script>
<div id="modal_openmaps" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="width: 95%;margin-left: 30% auto;">
    <div class="modal-content">
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>