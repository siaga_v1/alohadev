<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxYtVXSlnV8ZHZhgcVrvUQu3_Efi6QtT0"></script>
<div class="box-header with-border">
    <h6 class="box-title username"><small>Maps Integrated / <?=$row->gps_vehiclenumber;?> / <?=$row->gps_address;?></small></h6>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" id="closeMaps" data-dismiss="modal"><i class="fa fa-remove"></i></button>
    </div>
</div>
<!-- /.box-header -->
<div class="box-body">
    <div id="map" style="height: 400px;width: 100%;"></div>
    <script>
      $(function(){
        initMap();
      });
	  
    function initMap() {
		
		var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
        var icons = {
            start: new google.maps.MarkerImage(
            // URL
            iconBase+'blue.png',
            // (width,height)
            new google.maps.Size( 80, 60 ),
            // The origin point (x,y)
            new google.maps.Point( 0, 0 ),
            // The anchor point (x,y)
            new google.maps.Point( 50, 50 )
            ),
            end: new google.maps.MarkerImage(
            // URL
            iconBase+'truck.png',
            // (width,height)
            new google.maps.Size( 80, 60 ),
            // The origin point (x,y)
            new google.maps.Point( 0, 0 ),
            // The anchor point (x,y)
            new google.maps.Point( 50, 50 )
            )
        };
		
        var myLatLng = {lat: <?=$row->gps_lat;?>, lng: <?=$row->gps_long;?>};
        
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13 ,
            center: myLatLng
        });
        
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: '<?=$row->gps_address;?>',
            icon: icons.end
        });

    }
      
	  /*
      function initMap() {
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var directionsService = new google.maps.DirectionsService;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: {lat: 107.77, lng: -6.447}
        });
        directionsDisplay.setMap(map);
        calculateAndDisplayRoute(directionsService, directionsDisplay, map);
        
      }
      
	  
      function calculateAndDisplayRoute(directionsService, directionsDisplay, map) {
        directionsService.route({
          origin: {lat: <?=($row->gps_lat != null) ? $row->gps_lat : -6.1 ;?>, lng: <?=($row->gps_long != null) ? $row->gps_long : 107.1;?>},
          destination: {lat: <?=($row->gps_lat != null) ? $row->gps_lat : -6.1 ;?>, lng: <?=$row->gps_long != null ? $row->gps_long : 107.1 ;?>},
          travelMode: google.maps.TravelMode['DRIVING']
        }, function(response, status) {
          if(status == 'OK') 
          {
            directionsDisplay.setDirections(response);
            var leg = response.routes[0].legs[0];
            //alert(JSON.stringify(leg.start_location));
            makeMarker( leg.start_location, icons.start, 'title', map );
            makeMarker( leg.end_location, icons.end, 'title', map );
          } 
          else 
          {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
     
      var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
      var icons = {
      start: new google.maps.MarkerImage(
       // URL
       iconBase+'blue.png',
       // (width,height)
       new google.maps.Size( 80, 60 ),
       // The origin point (x,y)
       new google.maps.Point( 0, 0 ),
       // The anchor point (x,y)
       new google.maps.Point( 50, 50 )
      ),
      end: new google.maps.MarkerImage(
       // URL
        iconBase+'truck.png',
       // (width,height)
       new google.maps.Size( 80, 60 ),
       // The origin point (x,y)
       new google.maps.Point( 0, 0 ),
       // The anchor point (x,y)
       new google.maps.Point( 50, 50 )
      )
     };
     
     function makeMarker( position, icon, title, map ) {
         var marker = new google.maps.Marker({
              position: position,
              map: map,
              icon: icon
         });
     }
	*/
    </script>
</div>
