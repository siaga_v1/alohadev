               
                            <div class="modal-dialog modal-lg">
                                
                                    <div class="modal-content animated bounceInRight">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            
                                            <h4 class="modal-title">Achieve Target</h4>
                                            
                                        </div>

                                        <div class="modal-footer">
                                            <table class="table table-striped table-bordered table-hover dataTables-example" id="tableini">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Fleet Number</th>
                                                    <th>Ritase</th>
                                                    <th>Sales</th>
                                                    <th>Target</th>
                                                    <th>GM%</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    $x=1;
                                                        foreach ($top as $key ) {
                                                            # code...
                                                       
                                                    ?>
                                                <tr>
                                                    <td align="center"><?=$x?></td>
                                                    <td align="left"><?=$key['fleetnumber'];?></td>
                                                    <td><?=$key['ritases'];?></td>
                                                    <td><?=number_format($key['allowancess']);?></td>
                                                    <td><?=number_format($key['tartget']);?></td>
                                                    <td><?php 
                                                        $a = $key['allowancess'];
                                                        $b = $key['tartget'];

                                                        $c = ($a / $b) * 100;
                                                        echo round($c) ;
                                                        ?>%</td>
                                                </tr>
                                                <?php
                                                $x++;
                                                 }
                                                ?>
                                                
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                            </div>
               
                            <!-- MODAL END -->

                            <script>
        $(document).ready(function(){
           $('.dataTables-example').DataTable({
                pageLength: 5,
                responsive: true,
                "order": [[ 2, "desc" ]],

                ordering : false
            });

            //alert("UHUY");
        });

    </script>
