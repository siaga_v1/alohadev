<div class="col-lg-6">
    <a  id="carcarier">
        <div class="ibox">
            <div class="ibox-content lazur-bg  ">
                <div class="row">
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-trophy fa-gede"></i>
                    </div>
                    
                    <div class="col-xs-8 text-right">
                        <span> TRAILER </span>
                        <h3 class="font-bold">T : Rp. <?php if(!empty($dcc[0]['tartget'])){
                            echo number_format($dcc[0]['tartget']);     
                        } else { echo 0;} ?> / Unit</h3>
                        <h3 class="font-bold">A : <?php foreach ($cc as $a ){
                                echo number_format($a['allowancess']);
                        }?></h3>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-lg-6">
    <a  id="tansya">
        <div class="ibox">
            <div class="ibox-content lazur-bg  ">
                <div class="row">
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-trophy fa-gede"></i>
                    </div>
                    
                    <div class="col-xs-8 text-right">
                        <span> BOX </span>
                        <h3 class="font-bold">T : Rp. <?php if(!empty($dtn[0]['tartget'])){
                            echo number_format($dtn[0]['tartget']);     
                        } else { echo 0;} ?> / Unit</h3>
                        <h3 class="font-bold">A : <?php foreach ($tn as $a ){
                                echo number_format($a['allowancess']+$a['allowanceaddss']);
                        }?></h3>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-lg-6">
    <a  id="towing">
        <div class="ibox">
            <div class="ibox-content lazur-bg  ">
                <div class="row">
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-trophy fa-gede"></i>
                    </div>
                    
                    <div class="col-xs-8 text-right">
                        <span> DUTRO </span>
                        <h3 class="font-bold">T : Rp. <?php if(!empty($dtw[0]['tartget'])){
                            echo number_format($dtw[0]['tartget']);     
                        } else { echo 0;} ?> / Unit</h3>
                        <h3 class="font-bold">A : <?php foreach ($tw as $a ){
                                echo number_format($a['allowancess']+$a['allowanceaddss']);
                                
                        }?></h3>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-lg-6">
    <a  id="wingbox">
        <div class="ibox">
            <div class="ibox-content lazur-bg  ">
                <div class="row">
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-trophy fa-gede"></i>
                    </div>
                    
                    <div class="col-xs-8 text-right">
                        <span> WINGBOX </span>
                        <h3 class="font-bold">T : Rp. <?php if(!empty($dwb[0]['tartget'])){
                            echo number_format($dwb[0]['tartget']);     
                        } else { echo 0;} ?> / Unit</h3>
                        <h3 class="font-bold">A : <?php foreach ($wb as $a ){
                                echo number_format($a['allowancess']+$a['allowanceaddss']);
                                
                        }?></h3>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>
<!--<div class="col-lg-4">
    <a href="#" id="boxpareto">
        <div class="ibox">
            <div class="ibox-content lazur-bg  ">
                <div class="row">
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-trophy fa-gede"></i>
                    </div>

                    <div class="col-xs-8 text-right">
                        <span> BOX PARETO </span>
                        <h3 class="font-bold">T : Rp. <?php if(!empty($dpr[0]['tartget'])){
                            echo number_format($dpr[0]['tartget']);     
                        } else { echo 0;} ?> / Unit</h3>
                        <h3 class="font-bold">A : <?php foreach ($pr as $a ){
                            echo number_format($a['allowancess']+$a['allowanceaddss']);

                        }?></h3>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-lg-4">
    <a href="#" id="dsocity">
        <div class="ibox">
            <div class="ibox-content lazur-bg  ">
                <div class="row">
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-trophy fa-gede"></i>
                    </div>
                    
                    <div class="col-xs-8 text-right">
                        <span> DSO CITY </span>
                        <h3 class="font-bold">T : Rp. <?php if(!empty($ddso[0]['tartget'])){
                            echo number_format($ddso[0]['tartget']);     
                        } else { echo 0;} ?> / Unit</h3>
                        <h3 class="font-bold">A : <?php foreach ($dso as $a ){
                                echo number_format($a['allowancess']+$a['allowanceaddss']);
                                
                        }?></h3>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div> -->  

<!-- MODAL INPUT -->
                        
<script>
    $(document).ready(function() {
        
        $("#carcarier").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>dashboard/target',
                type:'POST',
                data:'setting=detailCC',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });

        $("#tansya").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>dashboard/target',
                type:'POST',
                data:'setting=detailTN',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });

        $("#towing").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>dashboard/target',
                type:'POST',
                data:'setting=detailTW',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });

        $("#wingbox").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>dashboard/target',
                type:'POST',
                data:'setting=detailWB',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });

        $("#boxpareto").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>dashboard/target',
                type:'POST',
                data:'setting=detailPR',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });


        $("#dsocity").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>dashboard/target',
                type:'POST',
                data:'setting=detailDSOw',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });

        
    });
</script>