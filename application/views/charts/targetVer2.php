<?php 
    foreach ($target as $key) {
?>
<div class="col-lg-4">
    <a  id="carcarier">
        <div class="ibox">
            <div class="ibox-content lazur-bg  ">
                <div class="row">
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-trophy fa-gede"></i>
                    </div>
                    
                    <div class="col-xs-8 text-right">
                        <h2> <?=$key['name']?> </h2>
                        <h3 class="font-bold">T : Rp. <?=number_format($key['Target'])?> </h3>
                        <h3 class="font-bold">A : Rp. <?=number_format($key['Actual'])?></h3>
                        <h3 class="font-bold">Percentage : <?php $total = $key['Actual']/$key['Target'];
                                                                 echo number_format($total*100);?>%</h3>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>

<?php
    } 
?>
<!--
<div class="col-lg-4">
    <a href="#" id="dsocity">
        <div class="ibox">
            <div class="ibox-content lazur-bg  ">
                <div class="row">
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-trophy fa-gede"></i>
                    </div>
                    
                    <div class="col-xs-8 text-right">
                        <span> DSO CITY </span>
                        <h3 class="font-bold">T : Rp. 5,096,000,000 </h3>
                        <h3 class="font-bold">A : <?php foreach ($dso as $a ){
                                echo number_format($a['allowancess']+$a['allowanceaddss']);
                                
                        }?></h3>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>    

 MODAL INPUT -->
                        
<script>
    $(document).ready(function() {
        
        $("#carcarier").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>dashboard/target',
                type:'POST',
                data:'setting=detailCC',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });

        $("#tansya").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>dashboard/target',
                type:'POST',
                data:'setting=detailTN',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });

        $("#towing").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>dashboard/target',
                type:'POST',
                data:'setting=detailTW',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });

        $("#wingbox").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>dashboard/target',
                type:'POST',
                data:'setting=detailWB',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });

        $("#boxpareto").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>dashboard/target',
                type:'POST',
                data:'setting=detailPR',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });


        $("#dsocity").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>dashboard/target',
                type:'POST',
                data:'setting=detailDSOw',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });

        
    });
</script>