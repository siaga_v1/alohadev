<h5 class="box-title"><strong>TOP 5 Low - Ritase /Truck</strong></h5>
<div class="box-tools pull-right"></div>
<div class="row">
    <div class="col-md-6">
        <div class="col-md-12">
          <p class="text-center">
            <strong>Ritase: <?=date('F Y');?></strong>
          </p>
          <div class="chart">
            <canvas id="truckritaselow" style="height: 260px;"></canvas>
          </div>
        </div>
    </div>
    
    <div class="col-md-3">
        <h5><strong>Table Ritase Low 5</strong>:</h5>
        <div class="table-responsive">
            <table class="table table-hover" style="font-size: 7pt;">
                <thead>
                <tr>
                    <th>Rank</th>
                    <th>Plate No</th>
                    <th>Ritase</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $x=1;
                $totalcosts = 0;
                $totalprices = 0;
                $totalachieves = 0;
                $totalritase = 0;
                $total30 = 0;
                $total5 = 0;
                $total59 = 0;
                $total1019 = 0;
                $total2029 = 0;
                $y=1;
                foreach($rows AS $row)
                { 
                    if($x > $lasttotal)
                    {
                ?>
                <tr>
                    <td><?=$y;?></td>
                    <td><?=$row->fleetplateno;?></td>
                    <td><?=number_format($row->ritase);?></td>
                </tr>
                <?php
                        $y++;
                    }
                
                    $totalritase += $row->ritase;
                    $totalcosts += $row->costs;
                    $totalprices += $row->prices;
                    $totalachieves += $row->achieves;
                    
                    if($row->ritase >= 30){
                        $total30 += $row->ritase;
                    }
                    
                    if($row->ritase >= 20 && $row->ritase <= 29){
                        $total2029 += $row->ritase;
                    }
                    
                    if($row->ritase >= 10 && $row->ritase <= 19){
                        $total1019 += $row->ritase;
                    }
                    
                    if($row->ritase >= 5 && $row->ritase <= 9){
                        $total59 += $row->ritase;
                    }
                    
                    if($row->ritase < 5){
                        $total5 += $row->ritase;
                    }
                    
                    
                $x++;
                } ?>
                        <tr>
                            <td>6</td>
                            <td colspan="2"> ....</td>
                        </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" align="center">Total</td>
                        <td><?=number_format($totalritase);?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    
    <div class="col-md-3 col-lg-3 col-xs-12">
        <div class="row">
            <h5 class="col-md-6 text-left"><strong>Summary</strong>:</h5>
            <!--<h5 class="col-md-6 text-right"><button id="download-truck-ritase" class="btn btn-success btn-xs">Download</button></h5>-->
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered" style="font-size: 7pt;">
                <thead>
                    <tr>
                        <td>Total Unit</td>
                        <th colspan="2"><?=($x-1)." Unit";?></th>
                    </tr>
                    <tr>
                        <td>Total Trip</td>
                        <th colspan="2"><?=$totalritase." Trip ";?></th>
                    </tr>
                    <tr>
                        <td>AVG Trip</td>
                        <th colspan=""><?=round($totalritase);?></th>
                    </tr>
                    <tr>
                        <td>Trip >= 30</td>
                        <th><?=round($total30);?></th>
                        <th><?=(($total30 > 0) ? number_format(($total30/$totalritase)*100,2) : 0)." %"?></th>
                    </tr>
                    <tr>
                        <td>Trip  20-29</td>
                        <th><?=$total2029?></th>
                        <th><?=(($total2029 > 0) ? number_format(($total2029/$totalritase)*100,2) : 0)." %"?></th>
                    </tr>
                    <tr>
                        <td>Trip 10-19</td>
                        <th><?=$total1019?></th>
                        <th><?=(($total1019 > 0) ? number_format(($total1019/$totalritase)*100,2) : 0)." %"?></th>
                    </tr>
                    <tr>
                        <td>Trip 5-9</td>
                        <th><?=$total59;?></th>
                        <th><?=(($total59 > 0) ? number_format(($total59/$totalritase)*100,2) : 0)." %"?></th>
                    </tr>
                    <tr>
                        <td>Trip < 5</td>
                        <th><?=$total5;?></th>
                        <th><?=(($total5 > 0) ? number_format(($total5/$totalritase)*100,2) : 0)." %"?></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
</div>
<script>
$(function() {
    var label = <?=$label?>;
    var datar = <?=$dataritase?>;
    var barData = {
          labels: label,
          datasets: [
              {
                  label: "Data 1",
                  backgroundColor: 'rgba(255, 0, 0, 0.5)',
                  pointBorderColor: "#fff",
                  xAxisID : 0,
                  data: datar
              }
          ]
      };

    Chart.pluginService.register({
            beforeRender: function (chart) {
                if (chart.config.options.showAllTooltips) {
                    // create an array of tooltips
                    // we can't use the chart tooltip because there is only one tooltip per chart
                    chart.pluginTooltips = [];
                    chart.config.data.datasets.forEach(function (dataset, i) {
                        chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                            chart.pluginTooltips.push(new Chart.Tooltip({
                                _chart: chart.chart,
                                _chartInstance: chart,
                                _data: chart.data,
                                _options: chart.options,
                                _active: [sector]
                            }, chart));
                        });
                    });

                    // turn off normal tooltips
                    chart.options.tooltips.enabled = false;
                }
            },
            afterDraw: function (chart, easing) {
                if (chart.config.options.showAllTooltips) {
                    // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                    if (!chart.allTooltipsOnce) {
                        if (easing !== 1)
                            return;
                        chart.allTooltipsOnce = true;
                    }

                    // turn on tooltips
                    chart.options.tooltips.enabled = true;
                    Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                        tooltip.initialize();
                        tooltip.update();
                        // we don't actually need this since we are not animating tooltips
                        tooltip.pivot();
                        tooltip.transition(easing).draw();
                    });
                    chart.options.tooltips.enabled = false;
                }
            }
        })

        var barOptions = {
            responsive: true,
            tooltips: {
              callbacks: {
                title: function(tooltipItems, data) {
                  return '';
                },
                label: function(tooltipItem, data) {
                  var datasetLabel = '';
                  var label = data.labels[tooltipItem.index];
                  return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                }
              }
            },
        showAllTooltips: true,
            scales: {
              yAxes: [{
                ticks: {
                    beginAtZero: true
                }
              }]
             }
        };



      var ctx2 = document.getElementById("truckritaselow").getContext("2d");
      new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});
  })
</script>