<div class="ibox float-e-margins">
    <div class="ibox-title">
      <h5>TOP 10 Trucks Pareto / Sales
          <small></small>
        </h5>
        <div class="box-tools pull-right">
            <button id="download-truck-sales" class="btn btn-success btn-xs">Download</button>
        </div>
    </div>
    <div class="ibox-content">
      <div class="row">
        <div class="col-md-8">
          <div class="col-md-12">
            <p class="text-center">
              <strong>Sales: <?=date('F Y');?> (Data in Millions (Angka dalam Juta))</strong>
            </p>
                      
              <div class="chart">
                <canvas id="areaChartTruckPareto" style="height: 300px;"></canvas>
              </div>
          </div>
        </div>
            
            <div class="col-md-4">
                <div class="table-responsive">
                    <table class="table table-hover" style="font-size: 7pt;">
                        <thead>
                        <tr> 
                            <th>#</th>
                            <th>Plate No</th>
                            <th>Sales</th>
                            <th>Costs</th>
                            <th>Target</th>
                            <th>Gross Margin</th>
                            <th>GM(%)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $x=1;
                        $totalcosts = 0;
                        $totalprices = 0;
                        $totalachieves = 0;
                        foreach($rows AS $row)
                        { 
                            if($x <= 10)
                            {
                        ?>
                        <tr>
                            <td><?=$x;?></td>
                            <td><?=$row->fleetplateno;?></td>
                            <td><?=number_format($row->allowances);?></td>
                            <td><?=number_format($row->prices);?></td>
                            <td><?=number_format($row->pareto);?></td>
                            <td><?=number_format($row->achieves);?></td>
                            <td><?=$row->achieves > 0 && $row->prices > 0 ? number_format(($row->achieves/$row->prices)*100) : 0;?></td>
                        </tr>
                        <?php
                            }
                            if($x == 11)
                            {
                        ?>
                                <tr>
                                    <td><?=$x;?></td>
                                    <td colspan="4"> ....</td>
                                </tr>
                        <?php
                            }
                            
                            $totalcosts += $row->costs;
                            $totalprices += $row->prices;
                            $totalachieves += $row->achieves;
                            
                        $x++;
                        } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2" align="center">Total</td>
                                <td><?=number_format($totalprices);?></td>
                                <td><?=number_format($totalcosts);?></td>
                                <td><?=number_format($totalachieves);?></td>
                                <td><?=($totalachieves > 0 && $totalprices > 0) ? number_format(($totalachieves/$totalprices)*100) : 0;?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div id="truckSalesLowLoad" class="col-md-12">
            
            </div>
        </div>
        <script type="text/javascript">
        $(function(){
            $.ajax({
                url:'<?php echo base_url();?>index.php/dashboard/truck',
                type:'POST',
                data:'setting=truckachieveslow',
                success: function(echo){
                    $('#truckSalesLowLoad').html(echo);
                }
            });
        })
        </script>
        
    </div>
<script>
$(function(){




        var lineData = {
        labels: <?=$label?>,
        datasets: [
            {
                label: "Sales",
                fill : false,
                borderColor: "rgba(250, 88, 130,0.7)",
                pointBackgroundColor: "rgba(250, 88, 130,1)",
                data: <?=$nums?>
            },
            {
                label: "Gross Margin",
                fill : false,
                borderColor: "rgba(255,165,0,1)",
                pointBackgroundColor: "rgba(255,165,0,1)",
                data: <?=$datacost?>
            }
        ]
    };
   Chart.pluginService.register({
            beforeRender: function (chart) {
                if (chart.config.options.showAllTooltips) {
                    // create an array of tooltips
                    // we can't use the chart tooltip because there is only one tooltip per chart
                    chart.pluginTooltips = [];
                    chart.config.data.datasets.forEach(function (dataset, i) {
                        chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                            chart.pluginTooltips.push(new Chart.Tooltip({
                                _chart: chart.chart,
                                _chartInstance: chart,
                                _data: chart.data,
                                _options: chart.options,
                                _active: [sector]
                            }, chart));
                        });
                    });

                    // turn off normal tooltips
                    chart.options.tooltips.enabled = false;
                }
            },
            afterDraw: function (chart, easing) {
                if (chart.config.options.showAllTooltips) {
                    // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                    if (!chart.allTooltipsOnce) {
                        if (easing !== 1)
                            return;
                        chart.allTooltipsOnce = true;
                    }

                    // turn on tooltips
                    chart.options.tooltips.enabled = true;
                    Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                        tooltip.initialize();
                        tooltip.update();
                        // we don't actually need this since we are not animating tooltips
                        tooltip.pivot();
                        tooltip.transition(easing).draw();
                    });
                    chart.options.tooltips.enabled = false;
                }
            }
        })


        var lineOptions = {
        responsive: true,
            tooltips: {
              callbacks: {
                title: function(tooltipItems, data) {
                  return '';
                },
                label: function(tooltipItem, data) {
                  var datasetLabel = '';
                  var label = data.labels[tooltipItem.index];
                  return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                }
              }
            },
        showAllTooltips: true,
            scales: {
              yAxes: [{
                ticks: {
                    beginAtZero: true
                }
              }]
             }
      
    };


    var ctx = document.getElementById("areaChartTruckPareto").getContext("2d");
    new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});

    $('#download-truck-sales').click(function(){
            window.open('<?php echo base_url()?>index.php/excelexport/truckmonth/trucksales','_blank');
        });
});
</script>
</div>