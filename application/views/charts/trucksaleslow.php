<h5 class="box-title"><strong>TOP 5 LOW - Sales /Truck</strong></h5>
<div class="box-tools pull-right"></div>
<div class="row">
    <div class="col-md-8">
        <div class="col-md-12">
          <p class="text-center">
            <strong>Sales: <?=date('F Y');?> (Data in Millions (Angka dalam Juta))</strong>
          </p>
          
          <div class="chart">
            <canvas id="areaChartTruckAchievementLow" style="height: 300px;"></canvas>
          </div>
        </div>
    </div>
    
    <div class="col-md-4">
        <h5><strong>Table Sales Low 5</strong>:</h5>
        <div class="table-responsive">
            <table class="table table-hover" style="font-size: 7pt;">
                <thead>
                <tr>
                    <th>Rank</th>
                    <th>Plate No</th>
                    <th>Sales</th>
                    <th>Costs</th>
                    <th>Gross Margin</th>
                    <th>GM(%)</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $x=1;
                $totalcosts = 0;
                $totalprices = 0;
                $totalachieves = 0;
                $totalmain = 0;
                $y=1;
                foreach($rows AS $row)
                { 
                    if($x > $lasttotal)
                    {
                ?>
                <tr>
                    <td><?=$y;?></td>
                    <td><?=$row->fleetplateno;?></td>
                    <td><?=number_format($row->costs);?></td>
                    <td><?=number_format($row->prices);?></td>
                    <td><?=number_format($row->achieves);?></td>
                    <td><?=($row->prices > 0 && $row->achieves > 0) ? number_format(($row->achieves/$row->prices)*100) : 0;?></td>
                </tr>
                <?php
                        $y++;
                    }
                    $totalcosts += $row->allowances;
                    $totalprices += $row->prices;
                    $totalachieves += $row->achieves;
                    $totalmain += $row->maintenance;
                    
                $x++;
                } ?>
                <tr>
                    <td><?=$y;?></td>
                    <td colspan="4"> ....</td>
                </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" align="center">Total</td>
                        <td><?=number_format($totalachieves);?></td>
                        <td><?=number_format($totalprices);?></td>
                        <td><?=number_format($totalachieves);?></td>
                        <td><?=($totalprices > 0 && $totalachieves > 0) ? number_format(($totalachieves/$totalprices)*100) : 0;?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="table-responsive">
            <div class="row">
                <h5 class="col-md-6 text-left"><strong>Summary</strong>:</h5>
                <!--<h5 class="col-md-6 text-right"><button id="download-truck-sales" class="btn btn-success btn-xs">Download</button></h5>-->
            </div>
            
            <table class="col-md-12 table table-hover table-bordered">
                <thead>
                    <tr>
                        <td>Total Truck</td>
                        <td>Total Sales</td>
                        <td>Total Costs</td>
                        <td>Total Maintenance</td>
                        <td>Total Gross Margin</td>
                        <td>(%) Total GM</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th><?=($x-1);?></th>
                        <th><?=number_format($totalcosts)?></th>
                        <th><?=number_format($totalprices)?></th>
                        <th><?=number_format($totalmain)?></th>
                        <th><?=number_format($totalachieves - $totalmain)?></th>
                        <th><?=($totalprices > 0 && $totalachieves > 0) ? number_format((($totalachieves-$totalmain)/$totalprices)*100) : 0;?>%</th>
                    </tr>
                </tbody>
            </table>
            
        </div>
    </div>
    
</div>
<script>
$(function(){
var lineData = {
        labels: <?=$label?>,
        datasets: [
            {
                label: "Sales",
                fill : false,
                borderColor: "rgba(250, 88, 130,0.7)",
                pointBackgroundColor: "rgba(250, 88, 130,1)",
                data: <?=$datacost?>
            },
            {
                label: "Gross Margin",
                fill : false,
                borderColor: "rgba(255,165,0,1)",
                pointBackgroundColor: "rgba(255,165,0,1)",
                data: <?=$nums?>
            }
        ]
    };

    Chart.pluginService.register({
            beforeRender: function (chart) {
                if (chart.config.options.showAllTooltips) {
                    // create an array of tooltips
                    // we can't use the chart tooltip because there is only one tooltip per chart
                    chart.pluginTooltips = [];
                    chart.config.data.datasets.forEach(function (dataset, i) {
                        chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                            chart.pluginTooltips.push(new Chart.Tooltip({
                                _chart: chart.chart,
                                _chartInstance: chart,
                                _data: chart.data,
                                _options: chart.options,
                                _active: [sector]
                            }, chart));
                        });
                    });

                    // turn off normal tooltips
                    chart.options.tooltips.enabled = false;
                }
            },
            afterDraw: function (chart, easing) {
                if (chart.config.options.showAllTooltips) {
                    // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                    if (!chart.allTooltipsOnce) {
                        if (easing !== 1)
                            return;
                        chart.allTooltipsOnce = true;
                    }

                    // turn on tooltips
                    chart.options.tooltips.enabled = true;
                    Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                        tooltip.initialize();
                        tooltip.update();
                        // we don't actually need this since we are not animating tooltips
                        tooltip.pivot();
                        tooltip.transition(easing).draw();
                    });
                    chart.options.tooltips.enabled = false;
                }
            }
        })


        var lineOptions = {
        responsive: true,
            tooltips: {
              callbacks: {
                title: function(tooltipItems, data) {
                  return '';
                },
                label: function(tooltipItem, data) {
                  var datasetLabel = '';
                  var label = data.labels[tooltipItem.index];
                  return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                }
              }
            },
        showAllTooltips: true,
            scales: {
              yAxes: [{
                ticks: {
                    beginAtZero: true
                }
              }]
             }
      
    };


    var ctx = document.getElementById("areaChartTruckAchievementLow").getContext("2d");
    new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});

  
});
</script>