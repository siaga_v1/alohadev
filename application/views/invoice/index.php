<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <h1>INVOICING</h1>
                    <hr />
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary btn-lg btn-block btn-big">
                                PERIODICAL<br/>INVOICING
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary btn-lg btn-block btn-big">
                                DAILY<br/>INVOICING
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>