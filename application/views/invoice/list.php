<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="11%">#</th>
            <th width="2%">No</th>
            <th>Invoice No</th>
            <th>Receipt No</th>
            <th>Customer Name</th>
            <th>Invoice Dates</th>
            <th>Total Shipment</th>
            <th>Total Price</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
    <?php
    if($rows != null){ 
        $no= ($page > 1) ? $offset+1 : 1;
        foreach ($rows as $row) {
        ?>
        <tr>        
            <td>
                <?php if($row->status == "N"){ ?>
                <a href="#" onclick="return manage_invoice(<?=$row->id?>)" class="btn btn-success btn-xs" title="Edit invoice"><i class="fa fa-edit"></i></a>
                <a href="#" onclick="return delete_invoice(<?=$row->id?>)" class="btn btn-danger btn-xs" title="Delete invoice" ><i class="fa fa-trash"></i></a>
                <?php }else{ ?>
                <a href="#" onclick="return payment_invoice(<?=$row->id?>)" class="btn btn-success btn-xs" title="Payment form" ><i class="fa fa-money"></i></a>
                <?php } ?>
                <a href="#" onclick="return detail_invoice(<?=$row->id?>)" class="btn btn-info btn-xs" title="Detail invoice" ><i class="fa fa-book"></i></a>
            </td>
            <td><?=$no;?></td>
            <td><?=$row->invoiceno?></td>
            <td><?=$row->receiptno?></td>
            <td><?=$row->customer_name?></td>
            <td><?=date("d-F-Y", strtotime($row->dates))?></td>
            <td><?=$row->total_shipment?></td>
            <td align="right"><?=number_format($row->prices)?></td>
            <th><?=get_status_invoice($row->status)?></th>
        </tr>
        <?php 
            $no++;
        }
    }else{
        echo "
        <tr>
            <td colspan='7'>Data not found</td>
        </tr>
        ";
    }
        
    ?>
    </tbody>
</table>
<div class="box-footer">
    <?php echo (isset($paginator)) ? $paginator : ''; ?>
</div>
