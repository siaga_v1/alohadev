<div class="ibox">
    <div class="ibox-title">
        <h4 style="text-transform: uppercase;color: green;font-weight: bolder;">Form Payment Invoice - For <?php echo $invoiceno != "" ? "#$invoiceno" : ""?> <?php //echo $status != "" ? "<b class='btn btn-info'> <b>Status</b> : ".get_status_invoice($status)."</b>" : "";?></h4>
        <?php if($id > 0){?>
        <div class="ibox-tools">
            <a class="" href="<?=base_url().'invoice/pdf/print/'.$code?>" target="_blank">
                Print PDF <i class="fa fa-file-pdf-o"></i>
            </a>
        </div>
        <?php } ?>
    </div>
    <div class="ibox-content" style="border: 1px dotted silver;">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group row">
                    <label class="col-sm-5 col-form-label">Customer Name</label>
                    <div class="col-sm-7">
                            <?=$id_customers > 0 ? "$customer_code - $customer_name" : "";?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">Invoice No <small>No Faktur</small></label>
                    <div class="col-sm-7">
                        <?=$invoiceno?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group row">
                    <label class="col-sm-5 col-form-label">Receipt No <small>No Kwitansi</small></label>
                    <div class="col-sm-7">
                        <?=$receiptno?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">Overdue Date</label>
                    <div class="col-sm-7">
                        <?=$dates_overdue?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">Invoice Date</label>
                    <div class="col-sm-7">
                        <?=$dates?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">Total Receiveble</label>
                    <div class="col-sm-7">
                        <?=number_format($paid)?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">Total Prices</label>
                    <div class="col-sm-7">
                        <?=number_format($prices)?>
                    </div>
                </div>
            </div>
            
             <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">Difference</label>
                    <div class="col-sm-7">
                        <?=number_format($prices-$paid)?>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
    <?php if($id > 0){ ?>
    <div class="table-responsive white-bg" id="load_data_index_invoicedetail">
        
    </div>
     <?php if($status == "S"){ ?>
    <?php /**
    <div class="form-group text-right">
        <a class="btn btn-info" style="text-allign: right;" href="<?=base_url()."ppuout/export/".$code?>" target="_blank">Export Excel <i class="fa fa-file-excel-o"></i></a>
        <a class="btn btn-danger" style="text-allign: right;" href="<?=base_url()."ppuout/pdf/print/".$code?>" target="_blank">Print PDF <i class="fa fa-file-pdf-o"></i></a>
    </div>
    */ ?>
    <div class="table-responsive white-bg" id="load_data_index_invoicedetail">
        
    </div>
    <!-- End Load Main Content of Page -->
    <?php } ?>
    <!-- End Load Main Content of Page -->
    <?php } ?>
</div>
<script type="text/javascript">
$(function() {
   load_data_index_invoicedetail();    
});

function load_data_index_invoicedetail()
{
    $.ajax({
        url:"<?=base_url()."invoicepayment/index";?>",
        type:"POST",
        data:{id_fin_invoices: <?=$id;?>},
        success: function(echo) {
            $('#load_data_index_invoicedetail').html(echo);
        }
    });
}

</script>