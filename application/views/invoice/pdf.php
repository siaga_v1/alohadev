<style>
@page { sheet-size: A4; }
@page receipt { sheet-size: 210mm 300mm }

#main-contents {
    font-family:arial, calibri, cursive;
    font-size:12px;
}

#main-contents table {
    font-family:arial, calibri, cursive;
    font-size:12px;
}

h1.title {
    font-size:18px;
    margin-bottom: 10px;
}
hr.hr-line-solid {
    color: black;
    height: 2px;
    border-collapse:collapse;
}

hr.hr-line-thin {
    color: black;
    height: 1px;
    border-collapse:collapse;
}

.customer-detail {
    margin: 10px 0px;
}

div.company-title {
    font-weight: bold;
    padding: 3px 0px;
    font-size:12px;
}

div.address {
    font-size: 12px;
}

.invoice-header {
    margin: 20px 0px;
}

.side-left {
    width: 48%;
    float: left;
}

.side-right {
    width: 48%;
    float: right;
}

.invoice-header table {
    margin: 0 auto;
    width: 100%;
    padding: 0;
    font-size:12px;
}

.invoice-header table td {
    padding: 0px 0px;
}

.invoice-detail table {
    width:100%;
    border-collapse: collapse;
    font-size:12px;
}

</style>
<div id="main-contents">
    <div class="main-contents">
        <div class="customer-detail">
            <div class="company-title">
            <?=$company->name?>
            </div>
            <div class="address">
            <?=$company->address_line1?> <br />
            <?=$company->address_line2?> <br />
            Telp &nbsp;&nbsp; : &nbsp;<?=$company->phone?>
            </div>
        </div>
        
        <div class="invoice-detail">
            <?=$html_detail;?>
        </div>
        
        <div class="side-left text-center">
            <p>
            Cikarang, <?=date("d-F-Y", strtotime($header->dates))?> <br />
            <?=$company->name?>
            
            <br />
            <br />
            <br />
            <br />
            <br />
            ( <?=$this->session->userdata("firstname");?> )
            </p>
        </div>
        
    </div>
    
</div>