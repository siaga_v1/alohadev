<style>
@page { sheet-size: A4; }
@page receipt { sheet-size: 210mm 300mm }

#main-contents {
    font-family:"courier new", arial, calibri;
}

#main-contents table{
    font-family:"courier new", arial, calibri;
    font-size:12px;
}

p {
    font-family:"courier new", arial, calibri;
    font-size:12px;
}

h1.title {
    font-size:18px;
    margin-bottom: 5px;
    margin-bottom: 0;
}
hr.hr-line-solid {
    color: black;
    height: 1px;
    border-collapse:collapse;
}

div.hr-line-thin {
    border-top: 1px dashed black;
}

.customer-detail {
    margin: 20px 0px;
}

div.company-title {
    font-weight: bold;
    padding: 3px 0px;
}

div.address {
    font-size: 13px;
}

.invoice-header {
    margin: 5px 0px;
}

.side-left {
    width: 48%;
    float: left;
}

.side-right {
    width: 48%;
    float: right;
}

.invoice-header table {
    margin: 0 auto;
    width: 100%;
    padding: 0;
}

.invoice-header table td {
    padding: 10px 0px;
    vertical-align: top;
    margin-bottom: 10px;
}

.invoice-header table td.full-border {
    border:1px solid gray;
}

.invoice-header table td .full-border {
    border:1px solid gray;
    padding: 10px;
}

.invoice-detail table {
    width:100%;
    border-collapse: collapse;
    font-size:12px;
}

.value-spelling {
    text-transform: capitalize;
    font-style: italic;
}

</style>
<div id="main-contents">
    <h1 class="title">KWITANSI</h1>
    
    <hr class="hr-line-solid" />
    
    <div class="main-contents">
        <div class="invoice-header">
             <table cellpadding="0" border="0" cellspacing="0">
                <tr>
                    <td colspan="4" width="80%">&nbsp;</td>
                    <td colspan="2" width="19%" align="right">No. <?=$header->receiptno?></td>
                </tr>
                
                <tr>
                    <td width="20%">Sudah terima dari</td>
                    <td width="2%">:</td>
                    <td colspan="4" width="78%"><?=$header->customer_name?></td>
                </tr>
                
                <tr>
                    <td width="20%">Banyaknya uang</td>
                    <td width="2%">:</td>
                    <td colspan="4" width="78%" class="full-border value-spelling">
                        <?=$totalpricesspell?> rupiah
                    </td>
                </tr>
                
                <tr>
                    <td width="20%">Untuk pembayaran</td>
                    <td width="2%">:</td>
                    <td colspan="2" width="30%">Invoice dengan No.</td>
                    <td colspan="2" width="48%"> <b><?=$header->invoiceno?></b></td>
                </tr>
                <tr>
                    <th colspan="6">&nbsp;</th>
                </tr>
                <tr>
                    <td colspan="4" width="70%">&nbsp;</td>
                    <td colspan="2" rowspan="3" width="30%" align="center">
                        Jakarta, <?=date("d-F-Y", strtotime($header->dates))?> <br />
                        Nama Perusahaan
                        
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        Admin
                        
                    </td>
                </tr>
                
                <tr>
                    <td width="20%">Terbilang</td>
                    <td width="2%"></td>
                    <td colspan="2" width="20%" align="left">
                        <div style="border: 1pxc solid gray;padding:5px;"><b>Rp. <?=number_format($totalprices)?></b></div>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="3" width="100%" align="left">
                        <!-- Detail Rekening Perusahaan -->
                        <p>
                         <br />
                         <br />
                         PT
                        </p>
                        
                        <!-- End Detail Rekening Perusahaan -->
                        
                    </td>
                </tr>
                
            </table>
        </div>
        
        <div class="hr-line-thin"></div>
        
    </div>
    
</div>