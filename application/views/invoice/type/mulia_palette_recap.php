<style>
@page { sheet-size: A4; }
@page receipt { sheet-size: 210mm 300mm }

#main-contents {
    font-family:"courier new", arial, calibri;
}

#main-contents table{
    font-family:arial, calibri, "courier new";
    font-size:12px;
}

p {
    font-family:"courier new", arial, calibri;
    font-size:12px;
}

h1.title {
    font-size:18px;
    margin-bottom: 5px;
    margin-bottom: 0;
}
hr.hr-line-solid {
    color: black;
    height: 1px;
    border-collapse:collapse;
}

div.hr-line-thin {
    border-top: 1px dashed black;
}

.customer-detail {
    margin: 20px 0px;
}

div.company-title {
    font-weight: bold;
    padding: 3px 0px;
}

div.address {
    font-size: 13px;
}

.invoice-header {
    margin: 5px 0px;
}

.side-left {
    width: 48%;
    float: left;
}

.side-right {
    width: 48%;
    float: right;
}

.invoice-header table {
    margin: 0 auto;
    width: 100%;
    padding: 0;
    font-weight: bolder;
    font-size:12px;
}

.invoice-header table td {
    padding: 2px 0px;
    vertical-align: top;
    margin-bottom: 10px;
    font-weight: bold;
}

.invoice-header table td.full-border {
    border:1px solid gray;
}

.invoice-header table td .full-border {
    border:1px solid gray;
    padding: 10px;
}

.invoice-detail table {
    width:100%;
    border-collapse: collapse;
    font-size:11px;
}

.value-spelling {
    text-transform: capitalize;
    font-style: italic;
}

.header-table-detail {
    border-collapse: collapse;
}

</style>
<div id="main-contents">
    <div class="main-contents">
        <div class="invoice-kop" align="center">
            <div class="side-left">
            <h4>
            <img style="margin-left: 90px;" src="<?=base_url()."assets/image/mulia_glass.jpg"?>" width="80px" /> <br />
            PT. MULIAGLASS CONTAINER DIVISION
            </h4>
            </div>
        </div>
        <div style="width: 100%;">
            <h2 align="center">OPNAME PROGRESS</h2>
        </div>
        <div class="invoice-header">
            <div class="side-left">
                <table cellpadding="4" border="0" cellspacing="0">
                    <tr>
                        <td width="20%">PROJECT NAME</td>
                        <td width="2%">:</td>
                        <td width="78%">ONGKOS ANGKUT INTERNAL MULIAGLASS</td>
                    </tr>
                    
                    <tr>
                        <td>CONTRACTOR</td>
                        <td>:</td>
                        <td><?=$company->name?></td>
                    </tr>
                    
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                           <br />
                        </td>
                    </tr>
                    
                </table>
            </div>
            <div class="side-right">
                <table cellpadding="4" border="0" cellspacing="0">
                    <tr>
                        <td width="20%">PR No</td>
                        <td width="2%">:</td>
                        <td width="78%"></td>
                    </tr>
                    
                    <tr>
                        <td>PO No</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    
                    <tr>
                        <td>PO DATE</td>
                        <td>:</td>
                        <td>
                           <br />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>OP DATE</td>
                        <td>:</td>
                        <td>
                           <br />
                        </td>
                    </tr>
                    
                </table>
            </div>
        </div>
        <br />
        <div class="invoice-detail">
            <table cellpadding="4" border="1" cellspacing="0">
                <tbody>
                    <tr>
                        <th rowspan="3">ITEM</th>
                        <th rowspan="3" colspan="2">DESCRIPTION</th>
                        <th rowspan="3">UNIT</th>
                        <th rowspan="3">CONTRACT WORK VOLUME</th>
                        <th rowspan="3">CURR</th>
                        <th rowspan="3">UNIT RATE</th>
                        <th colspan="6">PHYSICAL PROGRESS</th>
                        <th rowspan="3">TOTAL AMOUNT</th>
                    </tr>
                    <tr>
                        <th colspan="2">UP TO THIS MONTH</th>
                        <th colspan="2">UP TO LAST MONTH</th>
                        <th colspan="2">THIS MONTH</th>
                    </tr>
                    <tr>
                        <th>VOLUME</th>
                        <th>%</th>
                        <th>VOLUME</th>
                        <th>%</th>
                        <th>VOLUME</th>
                        <th>%</th>
                    </tr>
                </tbody>
                <tbody>
                    <?php 
$total_prices = 0;
if($inv_details != null){
            $no = 1;
            foreach($inv_details AS $row)
            {
?>  
        <tr>
            <td align="center"><?=$no;?></td>
            <td><?=isset(explode("-",$row->destination)[1]) ? trim(explode("-",$row->destination)[1]) : "" ?></td>
            <td align="center"><?=trim(explode("-",$row->destination)[0])?></td>
            <td align="center">PALLET</td>
            <td align="center"><?=$row->loadqty?></td>
            <td align="center">RP</td>
            <td align="center"><?=number_format($row->unit_price)?></td>
            
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            
            <td align="center"><?=$row->loadqty?></td>
            <td align="center">100%</td>
            
            <td align="right"><?=number_format($row->prices)?></td>
        </tr>
<?php       $total_prices += $row->prices;    
            $no++;
            }
} ?>
                </tbody>
                <tbody>
                    <tr>
                        <td colspan="4">Prepared By :</td>
                        <td colspan="4">Approved By :</td>
                        <td colspan="8"></td>
                    </tr>
                    <tr>
                        <th colspan="2">Contractor</th>
                        <th colspan="2">User</th>
                        <th colspan="2">HOD</th>
                        <th colspan="2">GM</th>
                        <th colspan="5" align="left">Total value of work done to that</th>
                        <th align="right"><?=number_format($total_prices)?></th>
                    </tr>
                    <tr>
                        <td colspan="2" rowspan="9"></td>
                        <td colspan="2" rowspan="9"></td>
                        <td colspan="2" rowspan="9"></td>
                        <td colspan="2" rowspan="9"></td>
                    </tr>
                    <tr>
                        <th colspan="5" align="left">Less advance payment</th>
                        <td align="right"></td>
                    </tr>
                    <tr>
                        <th colspan="5" align="left">Sub Total</th>
                        <th align="right"><?=number_format($total_prices)?></th>
                    </tr>
                    <tr>
                        <th colspan="5" align="left">Profit / Fee</th>
                        <td align="right"></td>
                    </tr>
                    <tr>
                        <th colspan="5" align="left">Sub Total</th>
                        <th align="right"><?=number_format($total_prices)?></th>
                    </tr>
                    <tr>
                        <th colspan="5" align="left">Adjustment PPN 10%</th>
                        <td align="right"></td>
                    </tr>
                    <tr>
                        <th colspan="5" align="left">Total</th>
                        <th align="right"><?=number_format($total_prices)?></th>
                    </tr>
                    <tr>
                        <th colspan="5" align="left">Loss total amount previously paid</th>
                        <td align="right"></td>
                    </tr>
                    <tr>
                        <th colspan="5" align="left">Payable</th>
                        <td align="right"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>