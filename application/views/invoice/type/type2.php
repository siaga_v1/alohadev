<table cellpadding="5" border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Order Date</th>
            <th>Destination</th>
            <th>Type</th>
            <th>Trip ID</th>
            <th>Shipment No</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
<?php 
$total_prices = 0;
if($inv_details != null){
            $no = 1;
            foreach($inv_details AS $row)
            {
?>  
        <tr>
            <td align="center"><?=$no;?></td>
            <td align="center"><?=date("d-m-Y", strtotime($row->orderdate))?></td>
            <td><?=$row->destination?></td>
            <td><?=$row->fleettype_name?></td>
            <td><?=$row->fleettype_name?></td>
            <td><?=$row->shippment?></td>
            <td align="right"><?=number_format($row->prices)?></td>
        </tr>
<?php       $total_prices += $row->prices;    
            $no++;
            }
} ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="right" colspan="6">GRAND TOTAL</td>
            <td align="right"><?=number_format($total_prices)?></td>
        </tr>
    </tfoot>
</table>