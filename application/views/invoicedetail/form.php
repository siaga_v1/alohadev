<form id="form_invoicedetail" name="form_invoicedetail" method="post">
<div class="modal-header text-right">
    <button type="submit" name="btnSubmit" class="btn btn-primary btn-sm">Add Shipment</button>
</div>
<div class="modal-body">
    <input type="hidden" readonly="" onfocus="blur()" id="id_fin_invoices" name="id_fin_invoices" value="<?=$id_fin_invoices?>" />
    <table class="table table-bordered table-hover" id="table_form_invoicedetail" >
        <thead>
            <tr>
                <th># <input type="checkbox" id="checkAll" /></th>
                <th>Shipment No</th>
                <th>Order Date</th>
                <th>Origin</th>
                <th>Destination</th>
                <th>Plate No</th>
                <th>Driver Name</th>
                <th>Prices</th>
                <th>Allowances</th>
            </tr>
        </thead>
        <tbody>
        <?php
        if($rows != null){ 
            $no=1;
            foreach ($rows as $row) {
            ?>
            <tr>        
                <td>
                    <input type="checkbox" name="detail[<?=$row->id?>][id_orders]" value="<?=$row->id?>" />
                </td>
                <td><?=$row->shippment?></td>
                <td><?=date("d/m/Y", strtotime($row->orderdate))?></td>
                <td><?=$row->origin?></td>
                <td><?=$row->destination?></td>
                <td><?=$row->fleetplateno?></td>
                <td><?=$row->driver_name?></td>
                <td class="text-right"><?=number_format($row->prices + $row->pricesadd)?></td>
                <td class="text-right"><?=number_format($row->allowances + $row->allowanceadds - $row->allowancereds)?></td>
            </tr>
            <?php 
                $no++;
            }
        }else{
            echo "
            <tr>
                <td colspan='8'>Data not found</td>
            </tr>
            ";
        }
            
        ?>
        </tbody>
    </table>
</div>
</form>
<script type="text/javascript">
$(function() {
    
    $('#table_form_invoicedetail').DataTable({
        pageLength: 100,
        responsive: true,
        ordering:false
    });
    
    $("#checkAll").change(function() {
        if(this.checked) {
            $('input:checkbox').not(this).prop('checked', this.checked);
        }else{
            $('input:checkbox').not(this).prop('checked', false);
        }
    });
    
});
</script>