<div id="base_invoicepayment">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5 style="text-transform: uppercase;color: green;font-weight: bolder;">Data Payment Invoices #</h5>
            <div class="ibox-tools">
                <a class="" href="" id="btn_refresh_invoicepayment">
                    Refresh <i class="fa fa-refresh"></i>
                </a>
                <a class="" href="" onclick="return manage_invoicepayment(0)">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
        <!-- Form Searching -->
        <div class="ibox-content" id="search_for_detail" style="padding-bottom: 0;">
            <form id="form_search_invoice" name="form_search_invoicepayment" method="post">
                <div class="form-group  row">
                    <label class="col-sm-2 col-form-label">Search something</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="search"/>
                    </div>
                    
                    <div class="col-sm-2">
                        <button class="btn btn-success btn-sm" type="submit">Search data <i class="fa fa-filter"></i></button>
                    </div>
                </div>
            </form>
            <div class="hr-line-dashed"></div>
        </div>
        <!-- End Form Searching -->
    </div>
    <div class="col-lg-12 white-bg">
        <!-- Load Main Content of Page -->
        <div class="table-responsive white-bg" id="load_data_invoicepayment">
                   
        </div>
        <!-- End Load Main Content of Page -->
    </div>
    
    <div class="modal inmodal" id="modal_form_invoicepayment">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" id="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <h4 class="modal-title">Form Pengeluaran Biaya</h4>

                </div>
                <div class="modal-body" id="load_form_invoicepayment">
                </div>
            </div>
        </div>
    </div>
    
</div>
<script type="text/javascript">
$(function() {
    
    load_invoicepayment_list();
    $("#base_invoicepayment").on("click", "#btn_refresh_invoicepayment", function() {
        load_invoicepayment_list();
        return false;
    });
    
    $("#base_invoicepayment #load_data_invoicepayment").on("click", "#btn_back_invoicepayment", function() {
        load_invoicepayment_list();
        return false;
    });
    
    $("#base_invoicepayment").on('click', '.pagination a', function() {
        
       var url = $(this).attr('href');
       var param = $('#form_search_invoicepayment').serialize();
       load_invoicepayment_list(url, param);
       return false; 
       
    });
    
    $("#base_invoicepayment").on('submit','#form_search_invoicepayment', function() {
        
        var url = "";
        var param = $('#form_search_invoice').serialize();
        load_invoicepayment_list(url, param);
        return false; 
        
    });
    
    $("#base_invoicepayment #load_form_invoicepayment").on('submit','#form_invoicepayment', function() {
        
        var formdata = new FormData(this);
        $.ajax({
            url:"<?=base_url()?>invoicepayment/set",
            type:"POST",
            dataType:"jSon",
            data: formdata,
            cache:false,
            processData:false,
            contentType:false,
            success: function(echo) {
                
                if(echo['type'] != 'error') {
                    $('#modal_form_invoicepayment').modal('hide');
                    //load_invoicepayment_list();
                    payment_invoice(echo["id_fin_invoices"]);
                }
                
                setUpAlert(echo['type'],echo['title'],echo['message']);
                
            }     
        });
        
        return false;
   
   });

   
});

function load_invoicepayment_list(url = "", param = "") {
    var url  = (url != "") ? url : "<?php echo base_url() ?>invoicepayment/lists";
    var param = (param != "") ? param : "";
    
    $.ajax({
        url:url,
        type:"POST",
        data:param + "&id_fin_invoices=<?=$id_fin_invoices?>",
        success: function(echo) {
            $('#base_invoicepayment #load_data_invoicepayment').html(echo);
        }
    });
}

function manage_invoicepayment(id = 0) {
    $.ajax({
        url:"<?php echo base_url() ?>invoicepayment/form",
        type:"POST",
        data:"id_fin_invoices=<?=$id_fin_invoices?>&id="+id,
        success: function(echo) {
            $('#base_invoicepayment #load_form_invoicepayment').html(echo);
            $('#modal_form_invoicepayment').modal();
        }
    });
    
    return false;
}

function delete_invoicepayment(id = 0) {
    
    swal({
      title: 'Are you sure?',
      text: "You want to delete this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
            url:"<?=base_url()?>invoicepayment/delete",
            type:"POST",
            dataType:"jSon",
            data:{ id : id },
            success: function(echo) {
                
                if(echo['type'] != 'error') {
                    //load_invoicepayment_list();
                    payment_invoice(echo["id_fin_invoices"]);
                }
                
                setUpAlert(echo['type'],echo['title'],echo['message']);
                
            }     
        });
      }
    });
    
    return false;
    
}
</script>