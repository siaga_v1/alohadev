<style>
.select2-container{
    z-index:100000;
}
</style>
<div class="ibox">
    <div class="ibox-content" style="">
        <form id="form_invoicepayment" name="form_invoicepayment" method="post">
            <input type="hidden" class="form-control" id="id" name="id" value="<?=$id?>" onfocus="blur()" readonly=""/> 
            <input type="hidden" class="form-control" id="id_fin_invoices" name="id_fin_invoices" value="<?=$id_fin_invoices?>" onfocus="blur()" readonly=""/> 
            
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Invoice Number</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="invoiceno" id="invoiceno" value="<?=$invoiceno?>" onfocus="blur()" readonly=""/> 
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Transaction Code</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="code" id="code" value="<?=$code?>" onfocus="blur()" readonly=""/> 
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-sm-3 col-xs-12 control-label">Payment Date</label>
                <div class="col-sm-9 col-xs-12">
                    <div class="input-group">
                        <input type="text" class="form-control datepicker" id="dates_date" style="width: 50%;" value="<?=$dates_date?>" name="dates_date" readonly=""/>
                            <div class="input-group bootstrap-timepicker timepicker" style="width: 50%;">
                                <input readonly="" value="<?=$dates_time?>" id="dates_time" name="dates_time" type="text" class="form-control input-small"/>
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-time"></i>
                                </span>
                            </div>
                    </div>  
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Description (Line 1)</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="description" id="description" value="<?=$description?>"/> 
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Description (Line 2)</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="description2" id="description2" value="<?=$description2?>"/> 
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Description (Line 3)</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="description3" id="description3" value="<?=$description3?>"/> 
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nominals</label>
                <div class="col-sm-9">
                    <input type="number" class="form-control" name="prices" id="prices" value="<?=$prices?>"/> 
                </div>
            </div>
            
            <div class="hr-line-dashed"></div>
            <div class="form-group row">
                <div class="col-sm-12 text-right">
                    <button class="btn btn-white" type="submit">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save changes</button>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
        </form>
    </div>
</div>
<script type="text/javascript">
$(function() {
    $('#dates_date').datepicker({format:'yyyy-mm-dd'});
    $('#dates_time').clockpicker({
        donetext: 'Set Time',
        autoclose: true
    });
    
})
</script>