<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="10%">#</th>
            <th width="2%">No</th>
            <th>Code</th>
            <th>Payment Date</th>
            <th>Invoice No</th>
            <th>Description</th>
            <th>Nominal</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $grand_total_prices = 0;
    if($rows != null){ 
        $no= ($page > 1) ? $offset+1 : 1;
        foreach ($rows as $row) {
        ?>
        <tr>        
            <td align="center">
                <a href="#" class="btn btn-success btn-xs" onclick="return manage_invoicepayment(<?=$row->id?>)"><i class="fa fa-edit"></i></a>
                <a href="#" class="btn btn-danger btn-xs" onclick="return delete_invoicepayment(<?=$row->id;?>)"><i class="fa fa-trash"></i></a>
            </td>
            <td align="center"><?=$no;?></td>
            <td align="left"><?=$row->code?></td>
            <td><?=date("d-M-Y H:i", strtotime($row->payment_date))?></td>
            <td align="left"><?=$row->invoiceno?></td>
            <td align="left"><?=$row->description?></td>
            <td align="right"><?=number_format($row->prices)?></td>
        </tr>
        <?php 
            $grand_total_prices += $row->prices;
            $no++;
        }
    }else{
        echo "
        <tr>
            <td colspan='7'>Data not found</td>
        </tr>
        ";
    }
        
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6" align="right"><b>Grand Total</b></td>
            <td align="right"><b><?=number_format($grand_total_prices)?></b></td>
        </tr>
    </tfoot>
</table>
<div class="box-footer">
    <?php echo (isset($paginator)) ? $paginator : ''; ?>
</div>
<br /><br /><br />
