<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout') ?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper white-bg page-heading">
    <div class="col-lg-10">
        <h2>Budget</h2>

        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a>Finance</a>
            </li>
            <li class="active">
                <strong>Budget</strong>
            </li>
        </ol>
    </div>
</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="row wrapper animated fadeInRight " id="base_budget">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox collapsed float-e-margins ">
                <div class="ibox-title">
                    <h5></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            Search <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="" href="" id="btn_refresh_budget">
                            Refresh <i class="fa fa-refresh"></i>
                        </a>
                        <a class="" href="" onclick="return manage_budget(0)">
                            Add <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
                <!-- Form Searching -->
                <div class="ibox-content">
                    <form id="form_search_budget" name="form_search_budget" method="post">
                        <div class="form-group  row">
                            <label class="col-sm-12 col-form-label">Search By:</label>
                        </div>
                        <div class="hr-line-dashed"></div>

                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group  row">
                                    <label class="col-sm-3 col-form-label">Akun</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="akun" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Keterangan</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="keterangan" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="datefrom" value="<?= date("Y-m-01") ?>" readonly="" />
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="dateto" value="<?= date("Y-m-t") ?>" readonly="" />
                                    </div>
                                </div>

                            </div>

                        </div>


                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button class="btn btn-white btn-sm" type="reset">Cancel</button>
                                <button class="btn btn-primary btn-sm" type="submit">Search data <i class="fa fa-filter"></i></button>
                                <a class="btn btn-info" id="export_excel" href="<?= base_url() . "kas/export_excel" ?>" target="_blank">Export Excel <i class="fa fa-file-excel-o"></i></a>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- End Form Searching -->
                <div class="ibox-footer" id="load_data_budget" name>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        load_budget_list();
        $('input[name=datefrom], input[name=dateto]').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
        $("#base_budget").on("click", "#btn_refresh_budget", function() {
            load_budget_list();
            return false;
        });

        $("#base_budget #load_data_budget").on("click", "#btn_back_budget", function() {
            load_budget_list();
            return false;
        });

        $("#base_budget").on("click", "#export_excel", function() {
            window.open("<?php echo base_url() . "kas/export_excel" ?>?" + $('#form_search_budget').serialize(), "_blank");
            return false;
        });

        $("#base_budget").on('click', '.pagination a', function() {

            var url = $(this).attr('href');
            var param = $('#form_search_budget').serialize();
            load_budget_list(url, param);
            return false;

        });

        $("#base_budget").on('submit', '#form_search_budget', function() {

            var url = "";
            var param = $('#form_search_budget').serialize();
            load_budget_list(url, param);
            return false;

        });

        $("#base_budget #load_data_budget").on('submit', '#form_budget', function() {

            var formdata = new FormData(this);
            $.ajax({
                url: "<?= base_url() ?>kas/set",
                type: "POST",
                dataType: "jSon",
                data: formdata,
                cache: false,
                processData: false,
                contentType: false,
                success: function(echo) {

                    if (echo['type'] != 'error') {
                        load_budget_list();
                    }

                    setUpAlert(echo['type'], echo['title'], echo['message']);

                },
                error: function(data) {
                    console.log(data);
                }
            });

            return false;

        });

    });

    function load_budget_list(url = "", param = "") {
        var url = (url != "") ? url : "<?php echo base_url(); ?>kas/list";
        var param = (param != "") ? param : "";

        $.ajax({
            url: url,
            type: "POST",
            data: param,
            success: function(echo) {
                $('#base_budget #load_data_budget').html(echo);
            }
        });
    }

    function manage_budget(id = 0) {
        $.ajax({
            url: "<?php echo base_url() ?>kas/form",
            type: "POST",
            data: "id=" + id,
            success: function(echo) {
                $('#base_budget #load_data_budget').html(echo);
            },
            error: function(d) {
                console.log(d);
            }
        });

        return false;
    }

    function delete_budget(id = 0) {

        $.ajax({
            url: "<?= base_url() ?>kas/delete",
            type: "POST",
            dataType: "jSon",
            data: "id=" + id,
            success: function(echo) {
                load_budget_list();

            },
            error: function(a) {
                console.log(a);
            }
        })

        return false;

    }
</script>