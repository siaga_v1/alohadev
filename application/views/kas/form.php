<div class="ibox">
    <div class="ibox-title">
        <h5>Form Kas</h5>
    </div>
    <div class="ibox-content" style="display: block;">
        <form class='form-horizontal' id="form_budget" name="form_budget" method="POST">
            <div class="form-group">
                <label class='col-sm-2 control-label'>Code</label>
                <div class='col-sm-5'>
                    <input type='text' class='form-control' style="display: none" id='id' name='id' value="<?= $id ?>">
                    <input type='text' class='form-control inputan' id='code' name='code' readonly="readonly" value="<?= $code ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2  control-label">Date</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control-sm form-control datepicker" autocomplete="off" name="date" value="<?= $date ?>">
                </div>
            </div>
            <div class="form-group">
                <label class='col-sm-2 control-label'>Akun<?= $akun ?></label>
                <div class='col-sm-5'>
                    <select class="form-control chosen-select" id="akun" name="akun">
                        <?= $akun > 0 ? "<option value='$akun' selected='selected'>$akun_name</option>" : ""; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class='col-sm-2 control-label'>Deskripsi</label>
                <div class='col-sm-5'>
                    <input type="text" class="form-control-sm form-control" autocomplete="off" name="deskripsi" value="<?= $deskripsi ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2  control-label">Nominal</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control-sm form-control" autocomplete="off" name="nominal" data-type="currency" value="<?= $nominal ?>">
                </div>
            </div>
            <div class="form-group">
                <label class='col-sm-2 control-label'></label>
                <div class='col-sm-8'>
                    <button class="btn btn-white" type="reset">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function() {
        $('input[name=date]').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $("select[name=akun]").select2({
            ajax: {
                url: "<?php echo base_url(); ?>kas/autoComplete",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() {
                formatCurrency($(this), "blur");
            }
        });
    });

    function formatNumber(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    function formatCurrency(input, blur) {
        var input_val = input.val();

        if (input_val === "") {
            return;
        }

        var original_len = input_val.length;

        var caret_pos = input.prop("selectionStart");

        if (input_val.indexOf(".") >= 0) {

            var decimal_pos = input_val.indexOf(".");

            var left_side = input_val.substring(0, decimal_pos);
            var right_side = input_val.substring(decimal_pos);

            left_side = formatNumber(left_side);

            right_side = formatNumber(right_side);

            if (blur === "blur") {
                right_side += "";
            }

            right_side = right_side.substring(0, 2);

            input_val = "" + left_side + "." + right_side;

        } else {
            input_val = formatNumber(input_val);
            input_val = "" + input_val;

            if (blur === "blur") {
                input_val += "";
            }
        }

        input.val(input_val);

        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }
</script>