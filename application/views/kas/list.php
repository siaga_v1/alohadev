<table class="table table-striped table-bordered table-hover" id='postsList'>
    <thead>
        <tr>
            <th>Action</th>
            <th>#</th>
            <th>Tanggal</th>
            <th>Akun</th>
            <th>Deskripsi</th>
            <th>Nominal</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if ($rows != null) {
            $no = 1;
            $totals = 0;
            foreach ($rows as $row) { ?>
                <tr>
                    <td>
                        <input type="text" style="display: none" id="type<?= $no ?>" name="type<?= $no ?>">

                        <a class="delete" onclick="return delete_budget(<?= $row['id'] ?>)">
                            <span class="label label-primary"> Delete</span>
                        </a>
                        <a class="edit" onclick="return manage_budget(<?= $row['id'] ?>)">
                            <span class="label label-primary"> Edit</span>
                        </a>


                    </td>
                    <td><?= $no ?></td>
                    <td><?= tgl_singkat($row['date']) ?></td>
                    <td><?= $row['name'] ?></td>
                    <td><?= $row['description'] ?></td>
                    <td align="right"><?= number_format($row['nominal']) ?></td>
                </tr>
        <?php
                $no++;
                $totals += $row['nominal'];
            }
            echo "<tr> <td colspan='5'>Total</td><td align='right'><b>" . number_format($totals) . "</b></td></tr>";
        } else {
            echo "
        <tr>
            <td colspan='6' align='center'><b>Data not found</b></td>
        </tr>
        ";
        }

        ?>
    </tbody>
</table>
<div>
    <?php echo (isset($paginator)) ? $paginator : ''; ?>
</div>