<!DOCTYPE html>
<html>


<!--#eb2538 Mirrored from webapplayers.com/inspinia_admin-v2.7.1/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Jun 2018 02:40:46 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Vechile Tracking Application | Login Page</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">
                    <img style="margin-left: auto;
  margin-right: auto;" class="img-responsive"  src="<?php echo base_url();?>assets/img/logo_aloha.png">
                </h1>

            </div>  
            <form action="<?php echo site_url('login/loginPage');?>" method="post" name="login">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username" id="username" name="username">
                </div>
                <div class="form-group">
                    <input type="password" id="password" class="form-control" placeholder="Password" name="password">
                </div>
                <button type="submit" class="btn btn-danger block full-width m-b">Login</button>

                
            </form>
            <?php
                                date_default_timezone_set("Asia/Jakarta");
                                  $nowdate = date('ydmGis');
                                    $now=date('Y');
                                    ?>
            <p class="m-t"> <small>PT. Total Kilat Solution Devlop | <?=$now?></small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.7.1/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Jun 2018 02:40:46 GMT -->
</html>
