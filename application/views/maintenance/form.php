<div class="ibox">
    <div class="ibox-title">Form Maintenance </div>
    <div class="ibox-content" style="display: block;">
        <form class='form-horizontal' id="form_maintenance" name="form_maintenance" method="POST">
            <div class="form-group">
                <label class='col-sm-2 control-label'>Code</label>
                <div class='col-sm-5'>
                    <input type='text' class='form-control' style="display: none" id='id' name='id' value="<?= $id ?>">
                    <input type='text' class='form-control inputan' id='code' name='code' readonly="readonly" value="<?= $code ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2  control-label">Date</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control-sm form-control datepicker" autocomplete="off" name="date" value="<?= $date ?>">

                </div>
            </div>
            <div class="form-group">
                <label class='col-sm-2 control-label'>Supplier <?= $supplier ?></label>
                <div class='col-sm-5'>
                    <select class="form-control chosen-select" id="supplier" name="supplier">
                        <?= $supplier > 0 ? "<option value='$supplier' selected='selected'>$supplier_name</option>" : ""; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class='col-sm-2 control-label'>Fleet <?= $fleet ?></label>
                <div class='col-sm-5'>
                    <select class="form-control chosen-select" id="fleet" name="fleet">
                        <?= $fleet > 0 ? "<option value='$fleet' selected='selected'>$plateno</option>" : ""; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12" id="addPart">
                    <label class="col-sm-2 control-label">
                        Add Parts
                    </label>

                    <div class="col-sm-9" align="left">
                        <button class="btn btn-danger btn-circle Part" id="Part" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div id="formPart">
                <?php
                $no = 0;
                if ($detail != null) {
                    foreach ($detail as $key) {
                ?>
                        <div class="uhuy">
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <button class="btn btn-danger btn-xs remDefect" id="remDefect" type="button"><i class="fa fa-minus"></i> Remove</button>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control codeparts" id="codeparts_<?= $no ?>" name="codeparts[<?= $no ?>]" placeholder="Code Parts" value="<?= $key['code_part'] ?>">
                                    <input type="text" class="form-control codeparts" style="display: none;" id="ids_<?= $no ?>" name="ids[<?= $no ?>]" value="<?= $key['ID'] ?>">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control partnumber" id="partnumber_<?= $no ?>" name="partnumber[<?= $no ?>]" placeholder="Part Number" value="<?= $key['part_number'] ?>">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="price[<?= $no ?>]" id="price_<?= $no ?>" class="form-control" placeholder="Price" value="<?= number_format($key['price']) ?>">
                                </div>
                            </div>
                        </div>
                <?php
                        $no++;
                    }
                }
                ?>
            </div>
            <div class="form-group">
                <label class='col-sm-2 control-label'></label>
                <div class='col-sm-8'>
                    <button class="btn btn-white" type="reset">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function() {
        $('input[name=date]').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });


        $("select[name=supplier]").select2({
            ajax: {
                url: "<?php echo base_url(); ?>supplier/autoComplete",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        $("select[name=fleet]").select2({
            ajax: {
                url: "<?php echo base_url(); ?>fleet/autoComplete",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        var tempatRoute = $('#formPart');
        var getId = document.getElementById("formPart");
        var getClass = getId.getElementsByClassName("uhuy");
        var noRoute = getClass.length;

        $('#addPart').on('click', '.Part', function() {

            $('<div class="uhuy"><div class="form-group"><div class="col-sm-2"><button class="btn btn-danger btn-xs remDefect" id="remDefect_' + noRoute + '" type="button"><i class="fa fa-minus"></i>  Remove</button></div><div class="col-sm-3"><input type="text" class="form-control codeparts" id="codeparts_' + noRoute + '" name="codeparts[' + noRoute + ']" placeholder="Code Parts" ></div><div class="col-sm-3"><input type="text" class="form-control partnumber" id="partnumber_' + noRoute + '" name="partnumber[' + noRoute + ']" placeholder="Part Number"></div><div class="col-sm-3"><input type="text" name="price[' + noRoute + ']" id="price' + noRoute + '" class="form-control prices" placeholder="Price"> </div></div></div>').appendTo(tempatRoute);

            $(".prices").keyup(function() {
                var n = parseInt($(this).val().replace(/\D/g, ''), 10);
                $(this).val(n.toLocaleString());
            });
            noRoute++;
            return false;
        });

        $('#formPart').on('click', '.remDefect', function() {
            $(this).parent().parent().remove();
            noRoute--;
            return false;
        });

    });
</script>