<table class="table table-striped table-bordered table-hover" id='postsList'>
    <thead>
        <tr>
            <th>Action</th>
            <th>#</th>
            <th>Code</th>
            <th>Date</th>
            <th>Fleet</th>
            <th>Part Number</th>
            <th>Code Part</th>
            <th>Price</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if ($rows != null) {
            $formatted_array = array();

            foreach ($rows as $element) {
                $formatted_array[$element['code']][] = $element;
            }
        ?>

            <?php
            $no = 1;
            $total = 0;
            foreach ($formatted_array as $row) : ?>
                <tr>
                    <td rowspan="<?= count($row) ?>" width="10%" style="white-space: nowrap;">
                        <input type="text" style="display: none" id="type<?= $no ?>" name="type<?= $no ?>" value="<?= $row[0]['code'] ?>">

                        <a class="invoice" onclick="return delete_maintenance('<?= $row[0]['code'] ?>')">
                            <span class="label label-primary"> Delete</span>
                        </a>
                        <a class="edit" onclick="return manage_maintenance(<?= $row[0]['id'] ?>)">
                            <span class="label label-primary"> Edit</span>
                        </a>


                    </td>
                    <td rowspan="<?= count($row) ?>"><?= $no ?></td>
                    <td rowspan="<?= count($row) ?>"><?= $row[0]['code'] ?></td>
                    <td rowspan="<?= count($row) ?>"><?= tgl_singkat($row[0]['date']) ?></td>
                    <td rowspan="<?= count($row) ?>"><?= $row[0]['fleet'] ?></td>
                    <?php foreach ($row as $value) : ?>
                        <td><?= $value['part_number'] ?></td>
                        <td><?= $value['code_part'] ?></td>
                        <td align="right"><?= number_format($value['price']) ?></td>
                </tr>
                <tr>
                <?php
                        $total += $value['price'];
                    endforeach; ?>
                </tr>
        <?php
                $no++;
            endforeach;
            echo "<tr> <td colspan='7'>Total</td><td align='right'><b>" . number_format($total) . "</b></td></tr>";
        } else {
            echo "
        <tr>
            <td colspan='8' align='center'><b>Data not found</b></td>
        </tr>
        ";
        }

        ?>
    </tbody>
</table>
<div>
    <?php echo (isset($paginator)) ? $paginator : ''; ?>
</div>