<?php 
                	if ($this->session->userdata('role') == '1') {
                		?>
                <li class="active">
                    <a href="<?php echo site_url('dashboard');?>"><i class="fa fa-home"></i> <span class="nav-label">Beranda</span>
                </a>
                </li>
                <!--<li>
                    <a href="#"><i class="fa fa-cube"></i> <span class="nav-label">Gudang</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo site_url('itemin');?>">Item Masuk</a></li>
                        <li><a href="<?php echo site_url('itemout');?>">Item Keluar</a></li>
                    </ul>
                </li>-->
                <li>
                    <a href="#"><i class="fa fa-dollar"></i> <span class="nav-label">Master Harga</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo site_url('debitcredit/requestpayable');?>">Permintaan Uang</a></li>
                        <li><a href="<?php echo site_url('target');?>">Target </a></li>
                        <li><a href="<?php echo site_url('route');?>">Rute / Trayek</a></li>
                        <li><a href="<?php echo site_url('cost/addroute');?>">Rute Tambahan</a></li>
                        <li><a href="<?php echo site_url('additional');?>">Biaya Tambahan</a></li>
                        <li><a href="<?php echo site_url('feecomponent');?>">Biaya Pengurangan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-briefcase"></i> <span class="nav-label">Operasional</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo site_url('order/pareto');?>">Order</a></li>                        
                        <li><a href="<?php echo site_url('order/orderlist');?>">Data Order</a></li>
                        <li><a href="<?php echo site_url('maintenance');?>">Maintenance</a></li>
                        <li><a href="<?php echo site_url('debitcredit');?>">Debit</a></li>
                        <li><a href="<?php echo site_url('debitcredit/credit');?>">Credit</a></li>
                        <li><a href="<?php echo site_url('downpayment');?>">Kasbon</a></li>
                        <!--
                        <li><a href="<?php echo site_url('debitcredit/saldo');?>">Tabungan Supir</a></li>
                        <li><a href="<?php echo site_url('debitcredit/refund');?>">Refund Saldo</a></li>
                            <li><a href="<?php echo site_url('order');?>">Orders</a></li>-->                        
                        <!--<li><a href="<?php echo site_url('order/operational');?>">Order Opertional</a></li>   
                        <li><a href="<?php echo site_url('order/spotOrder');?>">Order Spot</a></li>   
                        -->                     
                        <li><a href="<?php echo site_url('cost');?>">Biaya Tambahan</a></li>
                        <li><a href="<?php echo site_url('cost/operasional');?>">Biaya Operasional</a></li>
                        <!--<li><a href="<?php echo site_url('order/approveOrder');?>">Order Approval</a></li>
                        <li><a href="<?php echo site_url('realisation');?>">Realisation</a></li>

                        <li><a href="<?php echo site_url('orderbillingrealisation');?>">Realisation Billing</a></li>
                        <li><a href="<?php echo site_url('orderclaim');?>">Claim On</a></li>
                        <li><a href="<?php echo site_url('accident');?>">Accident/Incident</a></li>
                    -->
                        <li><a href="<?php echo site_url('returnDo');?>">Surat Jalan Balik</a></li>
                        <li><a href="<?php echo site_url('invoice/');?>">Summary Slip</a></li>
                        <!--<li><a href="<?php echo site_url('summaryslip/summarylist');?>">Invoice</a></li>-->
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Finance</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo site_url('finance/akun');?>">Akun</a></li>
                        <li><a href="<?php echo site_url('finance/budget');?>">Budget</a></li>
                        <li><a href="<?php echo site_url('driver/komisi');?>">Komisi Supir</a></li>
                        <li><a href="<?php echo site_url('summaryslip/piutang');?>">Piutang</a></li>
                        <li><a href="<?php echo site_url('finance/report');?>">Charts of Account (CoA)</a></li>
                        <li><a href="<?php echo site_url('finance/neraca');?>">Neraca Keuangan</a></li>
                        <li><a href="<?php echo site_url('finance/profitnloss');?>">Profit & Loss</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Master Data</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo site_url('customer');?>">Customer</a></li>
                        <li><a href="<?php echo site_url('driver');?>">Supir</a></li>
                        <li><a href="<?php echo site_url('employee');?>">Karyawan</a></li>
                        <li><a href="<?php echo site_url('fleet');?>">Armada</a></li>
                        <li><a href="<?php echo site_url('city');?>">Kota</a></li>
                        <li><a href="<?php echo site_url('supplier');?>">Supplier</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-cube"></i> <span class="nav-label">Laporan</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo site_url('report/truck');?>">Laporan Truck</a></li>
                        <li><a href="<?php echo site_url('fleet/reportDriver');?>">Laporan Driver</a></li>
                        <li><a href="<?php echo site_url('order/orderlistInput');?>">Laporan Customer</a></li>
                        <li><a href="<?php echo site_url('report/addcost');?>">Laporan Biaya Tambahan</a></li>
                    </ul>
                </li>
            <?php } else if ($this->session->userdata('role') == '3') {?>
               <li class="active">
                    <a href="<?php echo site_url('dashboard');?>"><i class="fa fa-home"></i> <span class="nav-label">Dashboards</span>
                </a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-briefcase"></i> <span class="nav-label">Operational</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo site_url('debitcredit');?>">Debit</a></li>
                        <li><a href="<?php echo site_url('debitcredit/credit');?>">Credit</a></li>
                        <li><a href="<?php echo site_url('order');?>">Orders</a></li>                 
                        <li><a href="<?php echo site_url('order/pareto');?>">Order SPLD</a></li>                        
                        <li><a href="<?php echo site_url('cost');?>">Additional Cost</a></li>
                        <li><a href="<?php echo site_url('order/orderlist');?>">All Order List</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-cube"></i> <span class="nav-label">Report</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo site_url('fleet/reportTruckKR');?>">Report Truck</a></li>
                        <li><a href="<?php echo site_url('fleet/reportDriver');?>">Report Driver</a></li>
                    </ul>
                </li>
            <?php } else if ($this->session->userdata('role') == '4') {?>
                <li class="active">
                    <a href="<?php echo site_url('dashboard');?>"><i class="fa fa-home"></i> <span class="nav-label">Dashboards</span>
                </a>
                </li>

                <li>
                    <a href="<?php echo site_url('order/orderlist');?>"><i class="fa fa-home"></i> <span class="nav-label">All Order List</span>
                </a>
                </li>

                <li>
                    <a href="<?php echo site_url('returnDo');?>"><i class="fa fa-home"></i> <span class="nav-label">Return DO</span>
                </a>
                </li>

                <li>
                    <a href="<?php echo site_url('summaryslip');?>"><i class="fa fa-home"></i> <span class="nav-label">Summary Slip</span>
                </a>
                </li>

                <li>
                    <a href="<?php echo site_url('summaryslip/summarylist');?>"><i class="fa fa-home"></i> <span class="nav-label">Summary List</span>
                </a>
                </li>
                <?php
            } else if ($this->session->userdata('role') == '5') {?>
                <li class="active">
                    <a href="<?php echo site_url('dashboard');?>"><i class="fa fa-home"></i> <span class="nav-label">Dashboards</span>
                </a>
                </li>

                <li>
                    <a href="<?php echo site_url('order/pareto');?>"><i class="fa fa-home"></i> <span class="nav-label">Orders</span>
                </a>
                </li>

                <li>
                    <a href="<?php echo site_url('order/orderlistInput');?>"><i class="fa fa-home"></i> <span class="nav-label">All Order List</span>
                </a>
                </li>
                <?php
            }
                ?>