<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | Trucks </span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Orders</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Master Data
            </li>
            <li class="active">
                <strong>Orders</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Orders Management</h5>
                    <div class="ibox-tools">
                        <a data-toggle="modal" data-target="#myModal5"> 
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>    
                </div>
                
                <div class="ibox-content">
                    <form class="form-horizontal" id="formspb" action="<?php echo site_url('order/saveOrders')?>" method="POST"   >
                        <div class="row">
                    <!-- INFORMASI ORDER -->
                    <div class="col-md-6 col-lg-6 col-xs-12">
                    
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Order Code</label>
                                <div class="col-sm-8">
                                    <?php
                                    if (empty($id))
                                    {
                                        $ccode = "1904000001";
                                    } else {
                                        $key= $id[0]['code'];
                                        $pattern = "/(\d+)/";
                                        $arra = preg_split($pattern, $key, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                                        $ccode = $arra[1]+1;
                                    }
                                    ?>
                                    <input type="text" class="form-control" id="code" name="code" value="OC<?=$ccode?>" readonly=""/>
                                </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 col-xs-12 control-label">Order Date</label>
                            <div class="col-sm-8 col-xs-12">
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" id="orderdate" style="width: 50%;" name="orderdate" readonly=""/>
                                        <div class="input-group bootstrap-timepicker timepicker" style="width: 50%;">
                                            <input readonly="" id="jam" data-autoclose="true" name="orderdatetime" type="text" class="form-control input-small"/>
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-time"></i>
                                            </span>
                                        </div>
                                </div>  
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Customer Name</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                          
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Description</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="description" name="description" rows="2"></textarea>
                            
                            </div>
                        </div>
                        
                        <div class="form-group">

                            <label class="col-sm-12"><h3 class="text-warning">Fleet Information</h3></label>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Fleet (Armada)</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="id_fleets" name="id_fleets" >
                                    <option value=""></option>
                                    <?php

                                        foreach ($fleet as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['fleetplateno']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Driver (Supir)</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="id_drivers" name="id_drivers" >
                                    <option value=""></option>
                                    <?php

                                        foreach ($driver as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 col-lg-6 col-xs-12">
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Shippment No.</label>
                            <div class="col-sm-8">
                                <input type="text" name="test" class="form-control" id="id_shippment" name="id_shippment">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Origin</label>
                            <div class="col-sm-8">
                                <select class="form-control id_citieso" id="id_citieso" name="id_citieso">
                                    <option value=""></option>
                                    
                                    <?php

                                        foreach ($cities as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Destination</label>
                            <div class="col-sm-8">
                                <select class="form-control id_citiesd" id="id_citiesd" name="id_citiesd">
                                    <option value=""></option>
                                    <?php

                                        foreach ($cities as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                     <div class="form-group">

                            <label class="col-sm-12"><h3 class="text-warning"></h3></label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Order Type</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="id_ordertypes" name="id_ordertypes">
                                    <option value=""></option>
                                    <?php

                                        foreach ($ordertypes as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                    
                        

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Route (UJS)</label>
                            <div class="col-sm-8">
                                <select class="form-control id_routes" id="id_routes" name="id_routes">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Additional</label>
                          <div class="col-sm-8">

                            <a class="detail btn btn-success btn-md" data-toggle="modal" data-target="#myModa"> 
                                <i class="fa fa-plus"></i> 
                            </a>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-4 control-label"></label>
                          <div class="col-sm-8">
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <th>Name</th> 
                                    <th>Nominal</th> 
                                </tr>
                                <tr>
                                    <td>Name</td> 
                                    <td>Nominal</td> 
                                </tr>
                            </table>
                          </div>
                        </div>
                    
                    </div>

                      

                         
                </div>
                
                <div class="form-group">
                  <div class="col-sm-12 text-right">
                    <button type="submit" class="btn btn-primary">Save!</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                  </div>
                </div>
                </form>
            </div>

                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB"  >
                            <thead>
                                <tr>
                                    <th style="width: 10%">Action #</th>
                                    <th>No #</th>
                                    <th>Order No.</th>
                                    <th style="width: 10%">Order Date</th>
                                    <th>Customer Name</th>
                                    <th>Origin</th>
                                    <th>Destination</th>
                                    <th style="width: 10%">Fleet</th>
                                    <th>Driver</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                    foreach ($order as $row) {
                                ?>
                                <tr>
                                    <td>
                                        <a class="detail btn btn-success btn-xs detail" value="<?=$row['id']?>"> 
                                            <i class="fa fa-book"></i> 
                                        </a>
                                        <a class="btn btn-primary btn-xs edist" value="<?=$row['id']?>"> 
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a class="btn btn-danger btn-xs" href="<?php echo site_url('customer/deleteCustomer')?>?id=<?=$row['id']?>" onclick="return confirm('Are you sure?');">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        &nbsp;
                                    </td>
                                    <td><?=$no?></td>
                                    <td><?=$row['code']?></td>
                                    <td><?=$row['orderdate']?></td>
                                    <td><?=$row['cname']?></td>
                                    <td><?=$row['n1']?></td>
                                    <td><?=$row['c2']?></td>
                                    <td><?=$row['fleet']?></td>
                                    <td><?=$row['driver']?></td>
                                    
                                </tr>
                                <?php 
                                $no++;
                                }
                                
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="modal inmodal" id="myModa" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="form-horizontal" id="formspb">
                                    <div class="modal-content animated bounceInRight">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            
                                            <h4 class="modal-title">Additional</h4>
                                            
                                        </div>
                                        <div class="modal-body">  
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Additional Name</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" name="name"  type="text" />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Nominal</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" name="nickname"  type="text" />
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            <input type="submit" style="margin-bottom: 5px;" name="ssubmit" id="ssubmit" class="btn btn-primary" value="Save changes">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>



        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>

       <script>
        $(document).ready(function(){
            
            $(".id_citieso").change(function(){
                var origin=$(".id_citieso option:selected").val();
                var dest=$(".id_citiesd option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_routes").html(data);
                        //alert(data);
                },
                });
            });

            $("#ssubmit").click(function(){
                alert("UHUY");
               
            });

            $(".id_citiesd").change(function(){
                var origin=$(".id_citieso option:selected").val();
                var dest=$(".id_citiesd option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_routes").html(data);
                        //alert(data);
                },
                });
            });

           

            $(".id_routes").change(function(){
                var route=$(".id_routes option:selected").val();
                //alert(route);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceAllowance');?>",
                    dataType: "html",
                    data: {
                        routes:route
                        },
                    success:function(data){
                        
                        var qty = parseInt($('#loadqty').val());
                        var harga = (data);     
                        var total = qty * harga;
                        $("#allowance").val(data);
                       // alert(harga);
                },
                });
            });


            $('#orderdate').datepicker({format:'yyyy-mm-dd'});

            $('#jam').clockpicker();

            $('div.ibox-content').slideUp();

//change the chevron
            $('.ibox-tools a.collapse-link i').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');

            $(".detail").click(function(){
                
            });

            setTimeout(function() {
                toastr.options = {
                  closeButton: true,
                  debug: false,
                  progressBar: true,
                  preventDuplicates: false,
                  positionClass: "toast-top-right",
                  onclick: false,
                  showDuration: "1000",
                  hideDuration: "1000",
                  timeOut: "7000",
                  extendedTimeOut: "1000",
                  showEasing: "swing",
                  hideEasing: "linear",
                  showMethod: "fadeIn",
                  hideMethod: "fadeOut"
                };
                toastr.success('Fleet Management System', 'Welcome to FMS');

            }, 1300);
            $('#listSPB').DataTable({
                autoWidth: true,
                ordering : false

            });

            $("#listSPB").on("click", ".edist", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/modaledit');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });


            $(".additional").click(function() {
                //var idcus=$(this).attr('value');
                //$.ajax({
                //    type:"POST",
                //  url: "<?php echo site_url('order/modaledit');?>",
                //    dataType: "html",
                //    data: {
                //        id:idcus
                //        },
                //    success:function(data){
                //        $("#modaledit").html(data);
                //        $("#modaledit").modal();

                        //
               // },
                //});
                alert("UHUY");
            });
            
            $("#listSPB").on("click", ".detail", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/modaldetail');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });

            

        });

    </script>