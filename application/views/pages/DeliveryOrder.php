<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | ANR.</span>
            </li>

            <li>
                <a href="login.html">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Delivery Order</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a href="<?php echo site_url('SuratPermintaanBarang')?>">Pengadaan</a>
            </li>
            <li class="active">
                <strong>Delivery Order</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <form class="form-horizontal" id="formspb" action="<?php echo site_url('masterDelivery/simpanDO')?>" method="POST">
    <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>Form Delivery Order</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            
                                <?php
                                date_default_timezone_set("Asia/Jakarta");
                                  $nowdate = date('ydmGis');

                                    
                                    //echo $nmr;
                                    
                                    if (date('d')=='001' && empty($nomor))
                                        { 
                                            $a = '001'; 
                                        }
                                        elseif($nomor != NULL)
                                        { 
                                            $nmr=$nomor[0]['dape_id'];
                                            $ex = explode('/', $nmr);
                                            $re = $ex[0];
                                            $rex=  explode('-', $re);
                                            $a = sprintf("%03d", $rex[1]+1); 
                                            //$a = sprintf("%02d", $ex[0]+1); 
                                        }else
                                        { 
                                            $a = '001'; 
                                        }
                                    $now=date('d-m-Y');
                                    $c = array('','I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
                                    $tahun=date('y');

                                ?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No DO</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="DONumber" value="DO-<?=$a?>/<?=$c[date('n')]?>/<?=$tahun?>" type="text" readonly="readonly" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ekspedisi</label>
                                    <div class="col-sm-8">
                                        <select name="ekspedisi" id="ekspedisi" class="form-control">
                                            <option>- Pilih -</option>
                                            <option value="Elkana">Elkana</option>
                                        </select>
                                    </div>
                                </div>

                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nomor PO</th>
                                            <th>Tanggal PO</th>
                                            <th>Armada</th>
                                            <th>Diminta Oleh</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                <tbody>
                                    <?php 
                                    $n=1;
                                        foreach ($po as $key ) {   
                                    ?>

                                    <tr>
                                        <td><?=$n?></td>
                                        <td>
                                            <?=$key['dape_code']?>
                                            <input type="text" hidden="hidden" name="detail[<?=$n?>][code]" value="<?=$key['dape_code'];?>">
                                        </td>
                                        <td><?=$key['dape_createtime']?>
                                            <input type="text" hidden="hidden" name="detail[<?=$n?>][date]" value="<?=$key['dape_createtime'];?>">
                                        </td>
                                        <td><?=$key['arm_nomor_pol']?>
                                            <input type="text" hidden="hidden" name="detail[<?=$n?>][nopol]" value="<?=$key['arm_nomor_pol'];?>">
                                        </td>
                                        <td><?=$key['emp_code']?> - <?=$key['emp_nama']?>
                                            <input type="text" hidden="hidden" name="detail[<?=$n?>][emp]" value="<?=$key['emp_id'];?>">
                                        </td>
                                        <td><input type="checkbox" name="detail[<?=$n?>][cek]"></td>
                                    </tr>

                                    <?php 
                                        $n++;} 
                                    ?>

                                </tbody>
                            </table>
                        </div>
                        <div class="ibox-footer">
                            <input type="submit" class="btn btn-info" id="tombol" value="Save">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>
    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>


<script>

    $(document).ready(function(){

        $("#Purchase").change(function(){
            var ini=$(this).val();
            $.ajax({
                type:"POST",
                url: "<?php echo site_url('masterDelivery/getItem');?>",
                dataType: "html",
                data: {
                    labi:ini
                    },
                success:function(data){
                    $("#tabel").html(data);
                    //alert(data);
            },
            });
            });

    });
</script>