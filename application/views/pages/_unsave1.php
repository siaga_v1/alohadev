<style>
    body { 
        font-family:Tempus Sans ITC;
        height: auto;
        font-size:10pt;
    }

    h3{
        color:orange;
    }

    h1{
        font-family:Arial;
        font-size: 11pt;
        margin:0pt;
    }
    .test {
        padding-top: 10px;
        text-align: left;
    }
    .testa {
        padding-top: 7px;
        text-align: left;
    }
    .testb {
        padding-top: 7px;
        text-align: left;
    }
    .text-red{
        font-weight: bolder;
        color:red;
    }
    .detail-desc{
        font-size: 7pt;
    }
    .detail-invoice{
        border-collapse: collapse;
        border-color: silver;
        border-width: thin;
        font-size: 7pt;
    }

    .bo td{
        font-size: 10pt;
    }
    .footer{
        font-size: 10pt;
    }
    hr.style2 {
	border-top: 3px double #8c8b8b;
	}
    #code-text{
        width:100%;
        float: right;
        text-align: right;
    }

    table {
      border-collapse: collapse;
    }

    .bo table, .bo th, .bo td {
      border: 1px solid black;
      padding: 5px;
    }
</style>
<body>
        <table width="100%" >
            <tr>
                <td align="right"><h1>PT. ALOHA LOGISTIK SUKSES<br>JL. KEBON BAWANG VII NO.18 TANJUNG PRIOK 14320<br>TELP : 021-43934691 - 92 FAX : 43933234</h1><hr class="style2"></td>
            </tr>

            <tr>
                <td>
                	<?php
                		function tgl_indo($tanggal){
					        $bulan = array (
					            1 =>   'Januari',
					            'Februari',
					            'Maret',
					            'April',
					            'Mei',
					            'Juni',
					            'Juli',
					            'Agustus',
					            'September',
					            'Oktober',
					            'November',
					            'Desember'
					        );
					        $pecahkan = explode('-', $tanggal);
					        
					        // variabel pecahkan 0 = tanggal
					        // variabel pecahkan 1 = bulan
					        // variabel pecahkan 2 = tahun
					     
					        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
					    }
                	?>
                    <table width="100%">
                        <tr>
                            <td width="50%">No : <?php foreach($invoice as $key){echo $key->noinvoice;} ?></td>
                            <td width="10%" align="right"><?php foreach($invoice as $key){echo tgl_indo($key->dateInvoice);} ?></td>
                        </tr>
                        <tr>
                            <td>
                            	<br>Kepada Yth: <br> 
                            	<?php foreach($invoice as $key){echo $key->name;} ?> <br> 
                            	Up. Finance Dept. <br><br>
                            	Dengan Hormat,<br><br>
                            	Dengan ini kami tagihkan ongkos trucking (surat jalan terlampir): 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr align="center">
                <td>
                    <table width="100%" class="bo">
                        <tr>
                            <td>No</td>
                            <td>Tanggal</td>
                            <td>No. Pol</td>
                            <td>Cust</td>
                            <td>Tujuan</td>
                            <td>Keterangan</td>
                            <td>No Kontainer</td>
                            <td>Biaya Trucking</td>
                            <?php 
                            if (isset($ops)) {
                                ?>
                                <td>Biaya Ops</td>
                                <?php 
                            }
                            if (isset($lolo)) {
                                ?>
                                <td>Biaya Lolo</td>
                                <?php 
                            }
                            if (isset($depo)) {
                                ?>
                                <td>Biaya Depo</td>
                                <?php 
                            }
                            if (isset($parkir)) {
                                ?>
                                <td>Biaya Parkir</td>
                                <?php 
                            }
                            if (isset($kawalan)) {
                                ?>
                                <td>Biaya Kawalan</td>
                                <?php
                            }
                            ?>
                            <td>TOTAL</td>
                        </tr>

                        <?php 
                        $no = 1;
                        $colawal = 8;
                        $col1 = 0;
                        $col2 = 0;
                        $col3 = 0;
                        $col4 = 0;
                        $col5 = 0;
                        $total = 0;
                        $pph = 0;
                        $code = 0;                
                        $totalops=0;               
                        $totallolo=0;              
                        $totaldepo=0;               
                        $totalparkir=0;                   
                        $totalkawalan=0;
                        foreach ($detail as $row) {
                            if ($code != $row->id) {
                                ?>
                                <tr>
                                <td><?=$no?></td>
                                <td>
                                    <?php
                                    $awal = $row->orderdate;
                                    $pecah = explode(" ",$awal);
                                    echo $pecah[0];
                                    ?>
                                </td>
                                <td><?=$row->fleet?></td>
                                <td><?=$row->n1?></td>
                                <td><?=$row->c2?></td>
                                <td><?=$row->n1?></td>
                                <td><?=$row->frameno?></td>
                                <td align="right"><?=number_format($row->unitprice)?></td>
                                <?php 
                                if (isset($ops)) {
                                    ?>
                                    <td><?=$row->feeOps?></td>
                                    <?php 
                                    $col1 = 1;
                                }
                                if (isset($lolo)) {
                                    ?>
                                    <td><?=$row->feeLolo?></td>
                                    <?php 
                                    $col2 = 1;
                                }
                                if (isset($depo)) {
                                    ?>
                                    <td><?=$row->feeDepo?></td>
                                    <?php 
                                    $col3 = 1;
                                }
                                if (isset($parkir)) {
                                    ?>
                                    <td><?=$row->feeParkir?></td>
                                    <?php 
                                    $col4 = 1;
                                }
                                if (isset($kawalan)) {
                                    ?>
                                    <td><?=$row->feeKawalan?></td>
                                    <?php
                                    $col5 = 1;
                                }
                                ?>

                                <td><?=$row->unitprice + $row->feeOps ?></td>
                            </tr>
                                <?php
                                $no++;
                            $total+= $row->unitprice;                           
                            $totalops+= $row->feeOps;                           
                            $totallolo+= $row->feeLolo;                           
                            $totaldepo+= $row->feeDepo;                           
                            $totalparkir+= $row->feeParkir;                           
                            $totalkawalan+= $row->feeKawalan;                           
                            $code= $row->id; 
                            }                                            	
                        }
                        ?>
                        
                        <tr>
                            <td colspan="<?=$col1+$col2+$col3+$col4+$col5+$colawal?>" align="right">DPP</td>
                            <td align="right"><?=number_format($total)?></td>
                        </tr>
                        <?php 
                        if (isset($pph)) {
                            ?>
                        <tr>    
                            <td colspan="<?=$col1+$col2+$col3+$col4+$col5+$colawal?>" align="right">PPN</td>
                            <td align="right"> 
                                <?php 
                                $pph = $total * 0.01; 
                                echo number_format($pph);
                                
                                ?>
                                </td>
                        </tr>
                        <?php
                        }
                        ?> 
                        
                        <tr>
                            <td colspan="<?=$col1+$col2+$col3+$col4+$col5+$colawal?>" align="right">BIAYA OPS</td>
                            <td align="right"><?=number_format($totalops+$totallolo+$totaldepo+$totalparkir+$totalkawalan)?></td>
                        </tr>

                        <tr>
                            <td colspan="<?=$col1+$col2+$col3+$col4+$col5+$colawal?>" align="right">TOTAL</td>
                            <td align="right"><?php $grandtotal = $total + $pph +$totalops+$totallolo+$totaldepo+$totalparkir+$totalkawalan; echo number_format($grandtotal);?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                	TERBILANG : <br>
                	<br>
                	Demikian kami sampaikan tagihan diatas mohon di transfer ke No. Rekening : <br>
                	Bank BCA <br>
                	Ref : 168 197 7371 <br>
                	A/N : SRYANA HIDAYAT
                </td>
            </tr>
        </table>
</body>
