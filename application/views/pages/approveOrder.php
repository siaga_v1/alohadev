<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | Trucks </span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Approve Order</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Master Data
            </li>
            <li class="active">
                <strong>Approve Order</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Approve Order</h5>
                    <div class="ibox-tools">
                        <a data-toggle="modal" data-target="#myModal5"> 
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>    
                </div>
                
                <div class="ibox-content">
                    <form id="form-search-FleetReport" name="form_FleetReport_search" method="post">
                        <div class="ibox-content m-b-sm border-bottom">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                     <label class="control-label" for="customer">Order Code</label>
                                     <input type="text" class="form-control" id="code" name="code" placeholder="Order Code"/>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                      <label class="control-label" for="customer">Fleet Plateno</label>
                                     <input type="text" class="form-control" id="fleetplateno" name="fleetplateno" placeholder="Fleet Plateno"/>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="date_modified">Route</label>
                                        <div class="input-group">
                                            <select class="form-control" autocomplete="off" id="origin" name="origin">
                                            </select>
                                            <span class="input-group-addon">to</span>
                                            <select class="form-control" autocomplete="off" id="destination" name="destination">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="customer">Customer</label>
                                        <select id="customer" name="customer" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="date_added">Driver</label>
                                        <select id="type" name="type" class="form-control">
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="date_modified">Date</label>
                                        <div class="input-daterange input-group" id="datepicker">
                                            <input type="text" class="form-control-sm form-control" autocomplete="off" name="start">
                                            <span class="input-group-addon">to</span>
                                            <input type="text" class="form-control-sm form-control" autocomplete="off" name="end">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="Koordinator">Koordinator</label>
                                        <select id="Koordinator" name="Koordinator" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="noka">No Rangka</label>
                                        <input type="text" id="noka" name="noka" class="form-control">
                                        
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info">Search</button>
                                        <button type="reset" class="btn btn-warning">Reset</button>
                                        <a href="#" id="export-ordertruck-summary" class="btn btn-success"><i class="fa fa-file-excel-o"></i>  Export</a>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    </form> 
                </div>

                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id='postsList' >
                            <thead>
                                <tr>
                                    <th style="width: 10%">Action #</th>
                                    <th>No #</th>
                                    <th>Order No.</th>
                                    <th style="width: 10%">Order Date</th>
                                    <th>Customer Name</th>
                                    <th>Origin</th>
                                    <th>Destination</th>
                                    <th style="width: 10%">Fleet</th>
                                    <th>Driver</th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>

                        </table>
                    </div>
                    <div style='margin-top: 10px;' id='pagination'></div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>



        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>

       <script>
        $(document).ready(function(){
           $('#pagination').on('click','a',function(e){
               e.preventDefault(); 
               var pageno = $(this).attr('data-ci-pagination-page');
               loadPagination(pageno);

               $('#postsList tbody').empty();
                     });

              loadPagination(0);
            $('#form-search-FleetReport').submit(function(){
                    loadPagination(0);
                    return false; 
                });
             // Load pagination
             function loadPagination(pagno){
                var param = $('#form-search-FleetReport').serialize();
                $.ajax({
                 url: '<?=base_url()?>order/loadRecordApprove/'+pagno,
                 type: 'get',
                 data:param,
                 dataType: 'json',
                 success: function(response){
                    $('#pagination').html(response.pagination);
                    createTable(response.result,response.row);
                 }
               });
             }

     // Create table list
             function createTable(result,sno){
               sno = Number(sno);

               $('#postsList tbody').empty();
               for(index in result){
                  var id = result[index].id;
                  var action = result[index].id;
                  var code = result[index].code;
                  var date = result[index].orderdate;
                  var origin = result[index].n1;
                  var destination  = result[index].c2;
                  var customer = result[index].cname;
                  var fleet = result[index].fleet;
                  var driver = result[index].driver;
                  sno+=1;

                  var tr = "<tr>";
                  tr += "<td><a class='detail btn btn-success btn-xs detail' value="+ action +"><i class='fa fa-book'></i></a></td>";
                  tr += "<td>"+ sno +"</a></td>";
                  tr += "<td>"+ code +"</td>";
                  tr += "<td>"+ date +"</td>";
                  tr += "<td>"+ customer +"</td>";
                  tr += "<td>"+ origin +"</td>";
                  tr += "<td>"+ destination +"</td>";
                  tr += "<td>"+ fleet +"</td>";
                  tr += "<td>"+ driver +"</td>";
                  tr += "</tr>";
                  $('#postsList tbody').append(tr);
         
                }
              }

              $("#postsList").on("click", ".detail", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/modalApprove');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });

            $("#postsList").on("click", ".edist", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/modaledit');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });

            $('.input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });

             $.ajax({
                url:'<?php echo base_url();?>customer/getCust',
                type:'POST',
                data:'setting=ambil',
                success: function(echo){
                    $('#customer').html(echo);
                    }
                });

            $.ajax({
                url:'<?php echo base_url();?>customer/getCust',
                type:'POST',
                data:'setting=tipe',
                success: function(echo){
                    $('#type').html(echo);
                    }
                });

            $.ajax({
                url:'<?php echo base_url();?>customer/getCust',
                type:'POST',
                data:'setting=koordinator',
                success: function(echo){
                    $('#Koordinator').html(echo);
                    }
                });
            $.ajax({
                url:'<?php echo base_url();?>customer/getCust',
                type:'POST',
                data:'setting=dari',
                success: function(echo){
                    $('#origin').html(echo);
                    }
                });
            $.ajax({
                url:'<?php echo base_url();?>customer/getCust',
                type:'POST',
                data:'setting=ke',
                success: function(echo){
                    $('#destination').html(echo);
                    }
                });

            $("#export-ordertruck-summary").click(function(){
               window.open("<?php echo base_url();?>order/exportExcel?export=excel&"+$('#form-search-FleetReport').serialize());
               return false; 
            });

        });

    </script>