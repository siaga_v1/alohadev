<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | Trucks </span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Billing</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Master Price
            </li>
            <li class="active">
                <strong>Billings</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Billing Management</h5>
                    <div class="ibox-tools">
                        <a data-toggle="modal" data-target="#myModal5"> 
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>    
                </div>
                
                <div class="ibox-content">
                    <form class="form-horizontal" id="formspb" action="<?php echo site_url('billing/saveBilling')?>" method="POST"   >
                    <!-- INFORMASI ORDER -->
                    
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Billing Code</label>
                                <div class="col-sm-4">
                                    <?php
                                    if (empty($id))
                                    {
                                        $ccode = "kosng";
                                    } else {
                                        $key= $id[0]['code'];
                                        $pattern = "/(\d+)/";
                                        $array = preg_split($pattern, $key, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                                        $ccode = $array[1]+1;
                                    }
                                    ?>
                                    <input type="text" class="form-control" id="code" name="code" value="<?=$array[0]?><?=$ccode?>" readonly=""/>
                                </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Customer Name</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Customer Area</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Origin Name</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Destination</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Order Type</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Order Size Unit</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Route</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Price (Harga Tagihan)</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Address From</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Address To</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Geofence From</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Geofence To</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-4">
                                <textarea class="form-control" id="description" name="description" rows="2"></textarea>
                            
                            </div>
                        </div>
                    
                <div class="form-group">
                  <div class="col-sm-12 text-right">
                    <button type="submit" class="btn btn-primary">Save!</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                  </div>
                </div>
                </form>
            </div>

                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB"  >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Billing Code</th>
                                    <th>Customer Name</th>
                                    <th>Origin</th>
                                    <th>Destination</th>
                                    <th>Order Type</th>
                                    <th>Order Unit</th>
                                    <th>Area</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                    foreach ($billing as $row) {
                                ?>
                                <tr>
                                    <td><?=$no?></td>
                                    <td><?=$row['code']?></td>
                                    <td><?=$row['cname']?></td>
                                    <td><?=$row['corigin']?></td>
                                    <td><?=$row['cdesti']?></td>
                                    <td><?=$row['ortypes']?></td>
                                    <td><?=$row['orsize']?></td>
                                    <td><?=$row['csarea']?></td>
                                    <td><?=$row['prices']?></td>
                                    <td>

                                        <a class="btn btn-primary btn-xs edit" value="<?=$row['id']?>"> 
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a class="btn btn-danger btn-xs" href="<?php echo site_url('customer/deleteCustomer')?>?id=<?=$row['id']?>" onclick="return confirm('Are you sure?');">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                    
                                </tr>
                                <?php 
                                $no++;
                                }
                                
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                        </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>



        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>

       <script>
        $(document).ready(function(){
            $("#id_citieso").change(function(){
                var size=$("#id_ordersizeunits option:selected").val();
                var types=$("#id_ordertypes option:selected").val();
                var origin=$("#id_citieso option:selected").val();
                var dest=$("#id_citiesd option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sizes:size,
                        ordertype:types
                        },
                    success:function(data){
                        $("#id_routes").html(data);
                        //alert(data);
                },
                });
            });

            $("#id_citiesd").change(function(){
                var size=$("#id_ordersizeunits option:selected").val();
                var types=$("#id_ordertypes option:selected").val();
                var origin=$("#id_citieso option:selected").val();
                var dest=$("#id_citiesd option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sizes:size,
                        ordertype:types
                        },
                    success:function(data){
                        $("#id_routes").html(data);
                        //alert(data);
                },
                });
            });

            $("#id_ordertypes").change(function(){
                var size=$("#id_ordersizeunits option:selected").val();
                var types=$("#id_ordertypes option:selected").val();
                var origin=$("#id_citieso option:selected").val();
                var dest=$("#id_citiesd option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sizes:size,
                        ordertype:types
                        },
                    success:function(data){
                        $("#id_routes").html(data);
                        //alert(data);
                },
                });
            });

            $("#id_ordersizeunits").change(function(){
                var size=$("#id_ordersizeunits option:selected").val();
                var types=$("#id_ordertypes option:selected").val();
                var origin=$("#id_citieso option:selected").val();
                var dest=$("#id_citiesd option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sizes:size,
                        ordertype:types
                        },
                    success:function(data){
                        $("#id_routes").html(data);
                        //alert(data);
                },
                });

            //alert(data);
            });

            $("#id_routes").change(function(){
                var route=$("#id_routes option:selected").val();
                //alert(route);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceAllowance');?>",
                    dataType: "html",
                    data: {
                        routes:route
                        },
                    success:function(data){
                        $("#allowance").val(data);
                       // alert(data);
                },
                });
            });


            $('#orderdate').datepicker({format:'yyyy-mm-dd'});

            $('#jam').clockpicker();

            $('div.ibox-content').slideUp();

//change the chevron
            $('.ibox-tools a.collapse-link i').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');

            $(".detail").click(function(){
                $('div.ibox-content').slideDown();
            });

            setTimeout(function() {
                toastr.options = {
                  closeButton: true,
                  debug: false,
                  progressBar: true,
                  preventDuplicates: false,
                  positionClass: "toast-top-right",
                  onclick: false,
                  showDuration: "1000",
                  hideDuration: "1000",
                  timeOut: "7000",
                  extendedTimeOut: "1000",
                  showEasing: "swing",
                  hideEasing: "linear",
                  showMethod: "fadeIn",
                  hideMethod: "fadeOut"
                };
                toastr.success('Fleet Management System', 'Welcome to FMS');

            }, 1300);
            $('#listSPB').DataTable({
                autoWidth: true,
                ordering : false

            });

            $("#listSPB").on("click", ".edit", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('billing/modalBilling');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });

            

        });

    </script>