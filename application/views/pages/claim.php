<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | Trucks </span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Claim Orders</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Master Data
            </li>
            <li class="active">
                <strong>Claim Orders</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Form Claim Management</h5>
                    <div class="ibox-tools">
                        <a data-toggle="modal" data-target="#myModal5"> 
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>    
                </div>
                
                <div class="ibox-content">
                    <form class="form-horizontal" id="formspb" action="<?php echo site_url('orderclaim/saveClaim')?>" method="POST"   >
                        <div class="row">
                    <!-- INFORMASI ORDER -->
                    <div class="col-md-6 col-lg-6 col-xs-12">
                    
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Claim Code</label>
                                <div class="col-sm-8">
                                    <?php
                                    if (empty($id))
                                    {
                                        $ccode = "1904000001";
                                    } else {
                                        $key= $id[0]['code'];
                                        $pattern = "/(\d+)/";
                                        $arra = preg_split($pattern, $key, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                                        $ccode = $arra[1]+1;
                                    }
                                    ?>
                                    <input type="text" class="form-control" id="code" name="code" value="CO<?=$ccode?>" readonly=""/>
                                </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 col-xs-12 control-label">Claim Date</label>
                            <div class="col-sm-8 col-xs-12">
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" id="orderdate" style="width: 50%;" name="orderdate" readonly=""/>
                                        <div class="input-group bootstrap-timepicker timepicker" style="width: 50%;">
                                            <input readonly="" id="jam" data-autoclose="true" name="orderdatetime" type="text" class="form-control input-small"/>
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-time"></i>
                                            </span>
                                        </div>
                                </div>  
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">No Rangka</label>
                            <div class="col-sm-8">
                                <input type="text" name="norangka" id="norangka" class="form-control">
                            </div>
                        </div>
                          
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Model</label>
                            <div class="col-sm-8">
                                <input type="text" name="modelarmada" id="modelarmada" class="form-control">
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Order Code</label>
                            <div class="col-sm-8">
                                <input type="text" name="ordercode" id="ordercode" class="form-control">
                            </div>
                        </div>
                        
                        <div class="form-group">

                            <label class="col-sm-12"><h3 class="text-warning">Fleet Information</h3></label>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Fleet (Armada)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="fleet" name="fleet" readonly=""/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Driver (Supir)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="driver" name="driver" readonly=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Customer</label>
                            <div class="col-sm-8">
                                 <input type="text" class="form-control" id="customer" name="customer" readonly=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Shippment No.</label>
                            <div class="col-sm-8">
                                 <input type="text" class="form-control" id="shippement" name="shippment" readonly=""/>
                            </div>
                        </div>

                        
                    </div>
                    
                    <div class="col-md-6 col-lg-6 col-xs-12">

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Origin</label>
                            <div class="col-sm-8">
                                 <input type="text" class="form-control" id="origin" name="origin" readonly=""/>
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Destination</label>
                            <div class="col-sm-8">
                                 <input type="text" class="form-control" id="destination" name="destination" readonly=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Fleet Type</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="fleettype" name="fleettype" readonly=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Damage Location</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="damageloc" name="damageloc" >
                                    
                                </select> 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Type of Damage</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="typeofdamage" name="typeofdamage" >
                                    
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Cause of Damage</label>
                            <div class="col-sm-8">
                                <textarea name="cause" id="cause" class="form-control"></textarea> 
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Claim Cost</label>
                            <div class="col-sm-8">
                                <div class="i-checks">
                                    <label> 
                                        <input type="radio" value="0" name="asiks" id="a"> <i></i> Asuransi
                                    </label>
                                </div>
                                <div class="i-checks">
                                    <label> 
                                        <input type="radio" value="1" name="asiks" id="sa"> <i></i> Non Asuransi
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Claim Cost</label>
                            <div class="col-sm-8">
                               <input type="text" class="form-control" id="cost" name="cost"/>
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Cost Company</label>
                            <div class="col-sm-8">
                               <input type="text" class="form-control" id="costcompany" name="costcompany"/>
                            </div>
                        </div>
                    
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Cost Driver</label>
                          <div class="col-sm-8">
                                <input type="text" class="form-control" id="costdriver" name="costdriver"/>
                          </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                  <div class="col-sm-12 text-right">
                    <button type="submit" class="btn btn-primary">Save!</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                  </div>
                </div>
                </form>
            </div>

                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB"  >
                            <thead>
                                <tr>
                                    <th>Action #</th>
                                    <th>#</th>
                                    <th>Claim Code</th>
                                    <th>Claim Date</th>
                                    <th>Frame No</th>
                                    <th>Customer Name</th>
                                    <th>Order Code</th>
                                    <th>Fleet</th>
                                    <th>Driver</th>
                                    <th>Claim Charges</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                    foreach ($claim as $row) {
                                ?>
                                <tr>
                                    <td>
                                        <a class="btn btn-success btn-xs detail" value="<?=$row['ordercode']?>"> 
                                            <i class="fa fa-book"></i> 
                                        </a>
                                        <a class="btn btn-primary btn-xs edist" value="<?=$row['ordercode']?>"> 
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td><?=$no?></td>
                                    <td><?=$row['code']?></td>
                                    <td><?=$row['claimdate']?></td>
                                    <td><?=$row['frameno']?></td>
                                    <td><?=$row['customer']?></td>
                                    <td><?=$row['ordercode']?></td>
                                    <td><?=$row['fleet']?></td>
                                    <td><?=$row['driver']?></td>
                                    <td><?php echo number_format($row['cost'])?></td>
                                    
                                </tr>
                                <?php 
                                $no++;
                                }
                                
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>



        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>

    <script>
        $(document).ready(function(){


        $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });

        $.ajax({
            type:"POST",
            url: "<?php echo site_url('orderclaim/getOption');?>",
            dataType: "html",
            data: {
                setting:'damage'
                },
            success:function(data){
                $("#damageloc").html(data);
            // alert("BISA KOK");
                },
            });   

        $.ajax({
            type:"POST",
            url: "<?php echo site_url('orderclaim/getOption');?>",
            dataType: "html",
            data: {
                setting:'type'
                },
            success:function(data){
                $("#typeofdamage").html(data);
            // alert("BISA KOK");
                },
            }); 

        $("#cost").keyup(function(){
            var a = $(this).val();

            var bagi = a * (50/100);
            var bagi1 = a * (50/100);

             $("#costcompany").val(bagi);
             $("#costdriver").val(bagi1);
        });

        
        $("#a").prop("checked", true);

        $('#norangka').typeahead({
            source:  function (query, process) {
            return $.get('<?php echo base_url()?>index.php/orderclaim/get_autocomplete', { query: query }, function (data) {
                    console.log(data);
                    data = $.parseJSON(data);
                    return process(data);
                });
            }

        });

        $("#norangka").change(function(){
            var asli = $(this).val();
            var pecah = asli.split(" / ");  
            var kode = pecah['1'];
            //alert(kode);

            $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('orderclaim/deskripsi');?>",
                    dataType: "html",
                    data: {
                        setting:'unit',
                        code:kode
                        },
                    success:function(data){
                        $("#ordercode").val(data);
                        //$("#modaledit").modal();

                       //alert(data);
                },
            });

            $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('orderclaim/deskripsi');?>",
                    dataType: "html",
                    data: {
                        setting:'model',
                        code:kode
                        },
                    success:function(data){
                        $("#modelarmada").val(data);
                        //$("#modaledit").modal();

                       //alert(data);
                },
            });

            $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('orderclaim/deskripsi');?>",
                    dataType: "html",
                    data: {
                        setting:'customer',
                        code:kode
                        },
                    success:function(data){
                        $("#customer").val(data);
                        },
                    });
        

            $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('orderclaim/deskripsi');?>",
                    dataType: "html",
                    data: {
                        setting:'fleet',
                        code:kode
                        },
                    success:function(data){
                        $("#fleet").val(data);
                        },
                });

            $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('orderclaim/deskripsi');?>",
                    dataType: "html",
                    data: {
                        setting:'driver',
                        code:kode
                        },
                    success:function(data){
                        $("#driver").val(data);
                        },
                });

            $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('orderclaim/deskripsi');?>",
                    dataType: "html",
                    data: {
                        setting:'citieso',
                        code:kode
                        },
                    success:function(data){
                        $("#origin").val(data);
                        },
                });
            $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('orderclaim/deskripsi');?>",
                    dataType: "html",
                    data: {
                        setting:'citiesd',
                        code:kode
                        },
                    success:function(data){
                        $("#destination").val(data);
                        },
                });

            $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('orderclaim/deskripsi');?>",
                    dataType: "html",
                    data: {
                        setting:'type',
                        code:kode
                        },
                    success:function(data){
                        $("#fleettype").val(data);
                        },
                });
        
        });



        $("#ordercode").change(function(){
            var kode = $(this).val();
            alert(kode);
            $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('orderclaim/getDetail');?>",
                    dataType: "json",
                    data: {
                        code:kode
                        },
                    success:function(data){
                        $("#fleet").html(data);
                        //$("#fleettype").html(data.b);
                        //$("#modaledit").modal();

                       alert(data.a);
                },
            });
        });



//change the chevron
            $('#orderdate').datepicker({format:'yyyy-mm-dd'});

            $('#jam').clockpicker();

            $('div.ibox-content').slideUp();


            $('.ibox-tools a.collapse-link i').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');

            setTimeout(function() {
                toastr.options = {
                  closeButton: true,
                  debug: false,
                  progressBar: true,
                  preventDuplicates: false,
                  positionClass: "toast-top-right",
                  onclick: false,
                  showDuration: "1000",
                  hideDuration: "1000",
                  timeOut: "7000",
                  extendedTimeOut: "1000",
                  showEasing: "swing",
                  hideEasing: "linear",
                  showMethod: "fadeIn",
                  hideMethod: "fadeOut"
                };
                toastr.success('Fleet Management System', 'Welcome to FMS');

            }, 1300);

            $('#listSPB').DataTable({
                autoWidth: true,
                ordering : false

            });

            $("#listSPB").on("click", ".edist", function(event) {
                var idcus=$(this).attr('value');
                //alert(idcus);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('orders/modaledit');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });
			
			$("#listSPB").on("click", ".detail", function(event) {
               var idcus=$(this).attr('value');
                //alert(idcus);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('orderclaim/modaledit');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });

            

    });

</script>