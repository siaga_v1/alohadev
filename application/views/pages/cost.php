<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Additional Cost</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Operational
            </li>
            <li class="active">
                <strong>Additional Cost</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Cost</h5>
                    <div class="ibox-tools">
                        
                        <!--<a data-toggle="modal" data-target="#myModal5"> 
                            <i class="fa fa-plus"></i>
                        </a> MODAL INPUT -->
                        <div class="modal inmodal" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <form class="form-horizontal" id="formspb" action="<?php echo site_url('cost/saveCost')?>" method="POST"   >
                                    <div class="modal-content animated bounceInRight">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            
                                            <h4 class="modal-title">Input Cost</h4>
                                            
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Cost Code</label>
                                                    <div class="col-sm-8">
                                                        <?php
                                                            if (empty($id))
                                                            {
                                                                $ccode = "11687";
                                                            } else {
                                                                $key= $id[0]['code'];
                                                                $pattern = "/(\d+)/";

                                                                $array = preg_split($pattern, $key, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                                                                $ccode = $array[1]+1;

                                                            }
                                                        ?>
                                                        <input class="form-control" name="code"  type="text" value="CC<?=$ccode?>" readonly="readonly" />
                                                    </div>
                                            </div>  
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Cost Component</label>
                                                    <div class="col-sm-8">
                                                        <select class="form-control name" name="name" id="name">
                                                            <?php 
                                                                foreach ($cost as $key) {
                                                            ?>

                                                                <option value="<?=$key['id']?>"><?=$key['name']?></option>

                                                            <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Date</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control datepicker" id="date" autocomplete="off"  name="ecdate"  type="text" readonly="" />
                                                    </div>
                                            </div>
                                            
                                            <div class="form-group" id="orderhide">
                                                <label class="col-sm-3 control-label">Order Code</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" name="order" id="order"  type="text" autocomplete="off" />
                                                    </div>
                                            </div>

                                            <div class='form-group ask'>
                                              <label class='col-sm-3 control-label'>Origin</label>
                                              <div class='col-sm-8'>
                                                <select class="form-control id_citio" id="id_citio" name="id_citio">
                                                  <option value=""></option>
                                                    <?php
                                                        foreach ($cities as $key ) {
                                                            echo "<option value='".$key['id']."'>".$key['name']."</option>";
                                                        }
                                                    ?>
                                                  </select>
                                              </div>
                                            </div>
                                                        
                                            <div class='form-group ask'>
                                              <label class='col-sm-3 control-label'>Destination</label>
                                              <div class='col-sm-8'>
                                                <select class='form-control id_citid' id='id_citid' name='id_citiesd'>
                                                  <option value=''></option>
                                                    <?php
                                                        foreach ($cities as $key ) {
                                                            echo "<option value='".$key['id']."'>".$key['name']."</option>";

                                                        }
                                                    ?>
                                                </select>
                                              </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Description</label>
                                                    <div class="col-sm-8">
                                                        <textarea class="form-control" name="description" id="description"></textarea>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Nominals</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" name="nominals" id="nominals"  type="number" />
                                                    </div>
                                            </div>
                                            <div class="form-group 3">
                                                <label class="col-sm-3 control-label">Nominals2</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" name="nominals2" id="nominals2"  type="number" />
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            <input type="submit" style="margin-bottom: 5px;" name="submit" class="btn btn-primary" value="Save changes">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>  
                            <!-- MODAL END -->
                        
                    </div>    
                </div>
                
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB"  >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Order Code</th>
                                    <th>Description</th>
                                    <th>Nominals</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                    foreach ($list as $row) {
                                ?>
                                <tr>
                                    <td><?=$no?></td>
                                    <td><?=$row['code']?></td>
                                    <td><?=$row['nama']?></td>
                                    <td><?=$row['date']?></td>
                                    <td><?=$row['order_id']?></td>
                                    <td><?=$row['description']?></td>
                                    <td><?php echo number_format($row['nominal']);?></td>
                                    <td>
                                        <a class="detail" value="<?=$row['id']?>"> 
                                            <span class="label label-primary"> <i class="fa fa-book"> </i></span>
                                            
                                        </a>
                                        
                                        
                                    </td>
                                </tr>
                                <?php 
                                $no++;
                                }
                                
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                        </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>


<!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>
<!-- Dual Listbox -->
       <script>
        $(document).ready(function(){


            setTimeout(function() {
                toastr.options = {
                  closeButton: true,
                  debug: false,
                  progressBar: true,
                  preventDuplicates: false,
                  positionClass: "toast-top-right",
                  onclick: false,
                  showDuration: "1000",
                  hideDuration: "1000",
                  timeOut: "7000",
                  extendedTimeOut: "1000",
                  showEasing: "swing",
                  hideEasing: "linear",
                  showMethod: "fadeIn",
                  hideMethod: "fadeOut"
                };
                toastr.success('Fleet Management System', 'Welcome to FMS');

            }, 1300);
            $('#listSPB').DataTable({
                pageLength: 10,
                responsive: true,
                ordering : false

            });

            $('#date').datepicker({format:'yyyy-mm-dd'});

            $(".ask, .3").hide();
            $(".name").change(function(){
                var origin=$(".name option:selected").text();
                    if (origin == 'TAMBAHAN LOKASI BONGKARAN') {
                        $(".ask").show();
                    } else {
            $(".ask").hide();

                    }
            });

            $('#order').typeahead({
            source:  function (query, process) {
            return $.get('<?php echo base_url()?>index.php/cost/get_autocomplete', { query: query }, function (data) {
                    console.log(data);
                    data = $.parseJSON(data);
                    return process(data);
                    });
                }

            });

            //$("#name").change(function(){
               // var idcus=$(this).val();
              //  if (idcus  > 14 ) {
              //      $("#orderhide").hide();
              //  } else {
              //      $("#orderhide").show();
               // }
                
           // });

            $("#necessary").change(function(){
                var idcus=$(this).val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('debitcredit/getDetailNecessary');?>",
                    dataType: "html",
                    data: {
                        id:idcus,
                        setting:"nominal",
                        },
                    success:function(data){
                        $("#nominals").val(data);
                },
                });
                 $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('debitcredit/getDetailNecessary');?>",
                    dataType: "html",
                    data: {
                        id:idcus,
                        setting:"deskripsi",
                        },
                    success:function(data){
                        $("#description").val(data);
                },
                });

            });

            $("#id_citio").change(function(){
                //alert("UHUY");
                var origin=$(".id_citio option:selected").val();
                var dest=$(".id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        sett:"aaa",
                        citiesd:dest
                        },
                    success:function(data){
                        $("#nominals").val(data);
                        //alert(data);
                },
                });
            });
            
             $("#id_citid").change(function(){
                //alert("UHUY");
                var origin=$("#id_citio option:selected").val();
                var dest=$("#id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        sett:"aaa",
                        citiesd:dest
                        },
                    success:function(data){
                        $("#nominals").val(data);
                       // alert(data);
                },
                });
            });

            $("#listSPB").on("click", ".edit", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('debitcredit/modaledit');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });
            
            $("#listSPB").on("click", ".detail", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('debitcredit/modalDetail');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });

        });

    </script>