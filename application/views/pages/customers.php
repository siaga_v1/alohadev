<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | Trucks </span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Customers</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Master Data
            </li>
            <li class="active">
                <strong>Customers</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Customer Management</h5>
                    <div class="ibox-tools">
                        <a data-toggle="modal" data-target="#myModal5"> 
                            <i class="fa fa-plus"></i>
                        </a>
                        <!-- MODAL INPUT -->
                        <div class="modal inmodal" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <form class="form-horizontal" id="formspb" action="<?php echo site_url('customer/saveCustomer')?>" method="POST"   >
                                    <div class="modal-content animated bounceInRight">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            
                                            <h4 class="modal-title">Input Customer</h4>
                                            
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Customer Code</label>
                                                    <div class="col-sm-8">
                                                        <?php
                                                            if (empty($id))
                                                            {
                                                                $ccode = "kosng";
                                                            } else {
                                                                $key= $id[0]['code'];
                                                                $kkk= $id[0]['id'];
                                                                $pattern = "/(\d+)/";

                                                                $array = preg_split($pattern, $key, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                                                                $ccode = $array[1]+1;
                                                                $cid = $kkk+1;

                                                            }
                                                        ?>
                                                        <input class="form-control" name="code"  type="text" value="CU<?=$ccode?>" readonly="readonly" />
                                                        <input class="form-control" style="display: none" name="id"  type="text" value="<?=$cid?>" readonly="readonly" />
                                                    </div>
                                            </div>  
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Customer Name</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" name="name"  type="text" />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Customer Nickname</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" name="nickname"  type="text" />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Customer Address</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" name="address_1" type="text"><br>
                                                        <input class="form-control" name="address_2" type="text"><br>
                                                        <input class="form-control" name="address_3" type="text">
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Customer Phone</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" name="phone"  type="text" />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Customer Email</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" name="email"  type="text" />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">PIC Name</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" name="pic_name"  type="text" />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Term</label>
                                                    <div class="col-sm-2">
                                                        <input class="form-control" name="term"  type="text" />
                                                    </div>
                                                    <label class="col-sm-2 control-label">PPN</label>
                                                    <div class="col-sm-2">
                                                        <input class="form-control" name="ppn"  type="text" />
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            <input type="submit" style="margin-bottom: 5px;" name="submit" class="btn btn-primary" value="Save changes">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>  
                            <!-- MODAL END -->
                        
                    </div>    
                </div>
                
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB"  >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Customer Code</th>
                                    <th>Customer Name</th>
                                    <th>Customer Nickname</th>
                                    <th>Customer Email</th>
                                    <th>PIC Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                    foreach ($customer as $row) {
                                ?>
                                <tr>
                                    <td><?=$no?></td>
                                    <td><?=$row['code']?></td>
                                    <td><?=$row['name']?></td>
                                    <td><?=$row['nickname']?></td>
                                    <td><?=$row['email']?></td>
                                    <td><?=$row['pic_name']?></td>
                                    <td>
                                        <a class="edit" value="<?=$row['id']?>"> 
                                            <span class="label label-success"> <i class="fa fa-edit"> </i></span>
                                            
                                        </a>
                                        <a href="<?php echo site_url('customer/deleteCustomer')?>?id=<?=$row['id']?>" onclick="return confirm('Are you sure?');"> 
                                            <span class="label label-danger"> <i class="fa fa-trash"></i></span>
                                           
                                        </a>
                                    </td>
                                </tr>
                                <?php 
                                $no++;
                                }
                                
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                        </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>



        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>

       <script>
        $(document).ready(function(){
            setTimeout(function() {
                toastr.options = {
                  closeButton: true,
                  debug: false,
                  progressBar: true,
                  preventDuplicates: false,
                  positionClass: "toast-top-right",
                  onclick: false,
                  showDuration: "1000",
                  hideDuration: "1000",
                  timeOut: "7000",
                  extendedTimeOut: "1000",
                  showEasing: "swing",
                  hideEasing: "linear",
                  showMethod: "fadeIn",
                  hideMethod: "fadeOut"
                };
                toastr.success('Fleet Management System', 'Welcome to FMS');

            }, 1300);
            $('#listSPB').DataTable({
                pageLength: 10,
                responsive: true,
                ordering : false

            });
            $("#listSPB").on("click", ".edit", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('customer/modaledit');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });

        });

    </script>