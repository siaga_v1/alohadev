<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>
           <li class="dropdown">
                    
                        <?php 
                            if($this->session->userdata('emp_kategori') == null)
                            {
                                echo "kosong";

                            }
                            elseif($this->session->userdata('emp_kategori') == '17') {
                                    echo "
            <a class='dropdown-toggle count-info' data-toggle='dropdown' href='#'>
                                    <i class='fa fa-bell'></i>  <span class='label label-primary'>";


                                                foreach($NSPB as $ro){
                                                    echo $ro->cu;
                                                }

                                    echo "
                                    </span>
                                </a>
                        <ul class='dropdown-menu dropdown-alerts'>
                                            <li>
                                                <a href=".site_url('SuratPermintaanBarang/approveSPB').">
                                                <div>
                                                <b>";

                                                foreach($NSPB as $row){
                                                   $e=strtotime($row->dpb_date);
                                                   $b=time();
                                                   $diff=$b-$e;
                                                    echo $row->cu;
                                                

                                                    echo "</b> Permintaan Approval SPB
                                                    <span class='pull-right text-muted small'>".floor($diff / (60 * 60 * 24))." days ago</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class='divider'></li> </ul>
                    </li>";
                                    
                                    }}

                                elseif($this->session->userdata('emp_kategori') == '2') {
                            echo " 
            <a class='dropdown-toggle count-info' data-toggle='dropdown' href='#'>
                                    <i class='fa fa-bell'></i>  <span class='label label-primary'>";


                                                foreach($NPO as $ro){
                                                    echo $ro->cu;
                                                }

                                    echo "
                                    </span>
                                </a>
                        <ul class='dropdown-menu dropdown-alerts'>
                         <li>
                                        <a href=".site_url('PurchaseOrder').">
                                            <div>
                                                <b>";

                                                foreach($NPO as $row){
                                                   $e=strtotime($row->dpb_date);
                                                   $b=time();
                                                   $diff=$b-$e;
                                                    echo $row->cu;
                                                

                                                    echo "</b> PO Baru
                                                <span class='pull-right text-muted small'>Waktu</span>
                                            </div>
                                        </a>
                                    </li>
                                    
                                    <li class='divider'></li> </ul>
                    </li>";
                                }}
                                     elseif($this->session->userdata('emp_kategori') == '3') {
                            echo "

            <a class='dropdown-toggle count-info' data-toggle='dropdown' href='#'>
                                    <i class='fa fa-bell'></i>  <span class='label label-primary'>".$NI."
                                    </span>
                                </a>
                        <ul class='dropdown-menu dropdown-alerts'>
                                    <li>
                                        <a href=".site_url('masterIssued/').">
                                            <div>
                                                <b>".$NI."</b> Issued Baru
                                                <span class='pull-right text-muted small'>Waktu</span>
                                            </div>
                                        </a>
                                    </li>
                            
                                    <li class='divider'></li>
                                     </ul>
                    </li>";}

                             elseif($this->session->userdata('emp_kategori') == '4') {
                            echo "

            <a class='dropdown-toggle count-info' data-toggle='dropdown' href='#'>
                                    <i class='fa fa-bell'></i>  <span class='label label-primary'>".$NI."
                                    </span>
                                </a>
                        <ul class='dropdown-menu dropdown-alerts'>
                                    <li>
                                        <a href=".site_url('masterIssued/').">
                                            <div>
                                                <b>".$NI."</b> Permintaan Approved Issued
                                                <span class='pull-right text-muted small'>Waktu</span>
                                            </div>
                                        </a>
                                    </li>
                                    
                                    <li class='divider'></li>
                            
                                    <li>
                                        <a href=".site_url('masterIssued/').">
                                            <div>
                                                <b>".$NI."</b> Permintaan Approved PR
                                                <span class='pull-right text-muted small'>Waktu</span>
                                            </div>
                                        </a>
                                    </li>
                                     </ul>
                    </li>";
                               } ?>
                           
                            
                            
                       



            <li>
                <a href="<?php echo site_url("login/logout");?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
    <div class="wrapper wrapper-content">
        <?php if ($this->session->userdata('role') != 3) {?> 
        <div class="row target2">
            
        </div>
        
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Yearly Report
                            <small>With custom colors.</small>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <canvas id="lineChart" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-lg-3">
                   <div class="widget style1 lazur-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-truck fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> Today Orders </span>
                                <h2 class="font-bold"><?=$orders->order_today?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                   <div class="widget style1 yellow-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-truck fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> This Week Orders </span>
                                <h2 class="font-bold"><?=$orders->order_thisweek?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                   <div class="widget style1 blue-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-truck fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> This Month Order </span>
                                <h2 class="font-bold"><?=$orders->order_thismonth?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                   <div class="widget style1 cc-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-truck fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> This Year Orders </span>
                                <h2 class="font-bold"><?=$orders->order_thisyear?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--TARGET CUSTOMERS-->
            <div class="row target">
            
            </div>

            <div class="row">
                <div id="truckRitaseLoad" class="col-md-12">
                
                </div>
            </div>

            <div class="row">
                <div id="driverRitaseLoad" class="col-md-12">
                
                </div>
            </div>

            <div class="row">
                <div id="truckSalesLoad" class="col-md-12">
                
                </div>
            </div>

            <div class="row">
                <div id="truckSalesCarCarrier" class="col-md-12">
                
                </div>
            </div>
            <div class="row">
                <div id="truckSalesTansya" class="col-md-12">
                
                </div>
            </div>
            <div class="row">
                <div id="truckSalesTowing" class="col-md-12">
                
                </div>
            </div>
            <!--<div class="row">
                <div id="truckSalesPareto" class="col-md-12">
                
                </div>
            </div>
            <div class="row">
                <div id="truckSalesDSO" class="col-md-12">
                
                </div>
            </div>

            <div class="row">
                <div id="claimorder" class="col-md-12">
                
                </div>
            </div>-->
            
            <div class="row">
                <div id="truckSalesWingBox" class="col-md-12">
                
                </div>
            </div>

            <div class="row">
                <div id="customerSalesLoad" class="col-md-12">
                
                </div>
            </div>
            <?php
            # code...
        } else { ?>
            <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Yearly Report
                            <small>With custom colors.</small>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <canvas id="lineChart" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-lg-3">
                   <div class="widget style1 lazur-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-truck fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> Today Orders </span>
                                <h2 class="font-bold"><?=$orders->order_today?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                   <div class="widget style1 yellow-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-truck fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> This Week Orders </span>
                                <h2 class="font-bold"><?=$orders->order_thisweek?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                   <div class="widget style1 blue-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-truck fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> This Month Order </span>
                                <h2 class="font-bold"><?=$orders->order_thismonth?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                   <div class="widget style1 cc-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-truck fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> This Year Orders </span>
                                <h2 class="font-bold"><?=$orders->order_thisyear?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div id="truckRitaseLoad" class="col-md-12">
                
                </div>
            </div>
            <div class="row">
                <div id="claimorder" class="col-md-12">
                
                </div>
            </div
        <?php } ?>
            
<div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
</div>


<!--
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    
                    <div class="ibox-title">
                        <h5>Orders</h5>
                    </div>
                    
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="flot-chart">
                                    <div class="flot-chart-content" id="flot-dashboard-chart">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Maintance List</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-hover no-margins">
                            <thead>
                                <tr>
                                    <th>Status</th>
                                    <th>ID Maintance</th>
                                    <th>Date</th>
                                    <th>Armada</th>
                                    <th>Category</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach($maintance as $mtc){
                                        if ($mtc['dama_status']=='Complete') {
                                            $span= "class='label label-success'";
                                        }                                                
                                        elseif ($mtc['dama_status']=='On Progress...') {
                                            $span= "class='label label-primary'";
                                        }
                                        elseif ($mtc['dama_status']=='Pending...') {
                                            $span= "class='label label-warning'";
                                        }
                                        else{$span="";}
                                                
                                ?>                             

                                    <tr>
                                        <td><span <?=$span?> ><?=$mtc['dama_status']?></span></td>
                                        <td><?=$mtc['dama_id']?></td>
                                        <td><?=$mtc['dama_date']?></td>
                                        <td><?=$mtc['arm_nomor_pol']?></td>
                                        <td><?=$mtc['dama_kategori']?></td>
                                        <td>
                                            <?php if ($mtc['dama_status']=='Complete') {
                                            echo "Waktu";
                                        }                                                
                                        elseif ($mtc['dama_status']=='On Progress...') {
                                            echo "<a class='label label-primary'>Finish</a>";
                                        }
                                        elseif ($mtc['dama_status']=='Pending...') {
                                            echo "<a class='label label-navy'>Start</a>";
                                        }
                                        else{$span="";}
                                                
                                ?>          </td>
                                    </tr>

                                <?php
                                
                                        }
                                    

                                 ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

        <div class="col-lg-12">
        <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Stock Item</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                     <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item Code</th>
                                    <th>Item Name</th>
                                    <th>Category</th>
                                    <th>Stock Min</th>
                                    <th>Stock Actual</th>
                                    <th>Location</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $n=1;
                                    foreach ($item as $emp) {
                                ?>
                                <tr>        
                                    <td><?=$n?></td>
                                    <td><?=$emp['ii_code']?></td>
                                    <td><?=$emp['ii_name']?></td>
                                    <td><?=$emp['im_name'] ?></td>
                                    <td><?=$emp['ii_min'] ?></td>
                                    <td><?=$emp['ii_stock'] ?></td>
                                    <td><?=$emp['il_name'] ?></td>
                                    <td><a href="<?php echo site_url('masterInventory/riwayatItem')   ?>?id=<?=$emp['ii_code']?>">Riwayat Item</a></td>
                                </tr>
                                <?php 
                                $n++;
                                }
                                ?>
                            </tbody>
                </table>
            </div>

        </div>
        </div>
        </div>

        </div>
    -->
    
<!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Peity -->
    <script src="<?php echo base_url();?>assets/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/demo/peity-demo.js"></script>

    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="<?php echo base_url();?>assets/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Jvectormap -->
    <script src="<?php echo base_url();?>assets/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

    <!-- EayPIE -->
    <script src="<?php echo base_url();?>assets/js/plugins/easypiechart/jquery.easypiechart.js"></script>

    <!-- Sparkline -->
    <script src="<?php echo base_url();?>assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="<?php echo base_url();?>assets/js/plugins/chartJs/Chart.min.js"></script>
    
    <script src="<?php echo base_url();?>assets/js/demo/sparkline-demo.js"></script>

    
    <script>
    $(document).ready(function() {
        var data2 = <?=$datayears;?>;
        var data3 = <?=$datalastyears;?>;
        var lineData = {
        labels: ["January", "February", "March", "April", "May", "June", "July","Agustus","sep","okt","nov","des"],
        datasets: [

            {
                label: "2020",
                backgroundColor: 'rgba(26,179,148,0.5)',
                borderColor: "rgba(26,179,148,0.7)",
                pointBackgroundColor: "rgba(26,179,148,1)",
                pointBorderColor: "#fff",
                data: data2
            }
        ]
    };

    Chart.pluginService.register({
            beforeRender: function (chart) {
                if (chart.config.options.showAllTooltips) {
                    // create an array of tooltips
                    // we can't use the chart tooltip because there is only one tooltip per chart
                    chart.pluginTooltips = [];
                    chart.config.data.datasets.forEach(function (dataset, i) {
                        chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                            chart.pluginTooltips.push(new Chart.Tooltip({
                                _chart: chart.chart,
                                _chartInstance: chart,
                                _data: chart.data,
                                _options: chart.options,
                                _active: [sector]
                            }, chart));
                        });
                    });

                    // turn off normal tooltips
                    chart.options.tooltips.enabled = false;
                }
            },
            afterDraw: function (chart, easing) {
                if (chart.config.options.showAllTooltips) {
                    // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                    if (!chart.allTooltipsOnce) {
                        if (easing !== 1)
                            return;
                        chart.allTooltipsOnce = true;
                    }

                    // turn on tooltips
                    chart.options.tooltips.enabled = true;
                    Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                        tooltip.initialize();
                        tooltip.update();
                        // we don't actually need this since we are not animating tooltips
                        tooltip.pivot();
                        tooltip.transition(easing).draw();
                    });
                    chart.options.tooltips.enabled = false;
                }
            }
        })

    var lineOptions = {
        responsive: true,
        tooltips: {
          callbacks: {
            title: function(tooltipItems, data) {
              return '';
            },
            label: function(tooltipItem, data) {
              var datasetLabel = '';
              var label = data.labels[tooltipItem.index];
              return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
            }
          }
        },
        showAllTooltips: true 

    };


    var ctx = document.getElementById("lineChart").getContext("2d");
    new Chart(ctx, {type: 'line', data: lineData, options:lineOptions });

    $("#toptarget").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>index.php/dashboard/target',
                type:'POST',
                data:'setting=topachieves',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });

        $("#midtarget").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>index.php/dashboard/target',
                type:'POST',
                data:'setting=midachieves',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });

        $("#lowtarget").click(function(){
            $.ajax({
                url:'<?php echo base_url();?>index.php/dashboard/target',
                type:'POST',
                data:'setting=lowachieves',
                success: function(echo){
                    $("#modaledit").html(echo);
                    $("#modaledit").modal();
                }
            });
        });


    $.ajax({
        url:'<?php echo base_url();?>index.php/dashboard/truck',
        type:'POST',
        data:'setting=truckritase',
        success: function(echo){
            $('#truckRitaseLoad').html(echo);
            }
        });


    $.ajax({
        url:'<?php echo base_url();?>dashboard/truck',
        type:'POST',
        data:'setting=claim',
        success: function(echo){
            $('#claimorder').html(echo);
            }
        });

    $.ajax({
        url:'<?php echo base_url();?>dashboard/truck',
        type:'POST',
        data:'setting=truckachieves',
        success: function(echo){
            $('#truckSalesLoad').html(echo);
            }
        });
    $.ajax({
        url:'<?php echo base_url();?>index.php/dashboard/truck',
        type:'POST',
        data:'setting=trucke',
        success: function(echo){
            $('#truckSalesCarCarrier').html(echo);
            }
        });

    $.ajax({
        url:'<?php echo base_url();?>index.php/dashboard/truck',
        type:'POST',
        data:'setting=truckf',
        success: function(echo){
            $('#truckSalesTansya').html(echo);
            }
        });

    $.ajax({
        url:'<?php echo base_url();?>index.php/dashboard/truck',
        type:'POST',
        data:'setting=truckg',
        success: function(echo){
            $('#truckSalesTowing').html(echo);
            }
        });

    $.ajax({
        url:'<?php echo base_url();?>index.php/dashboard/truck',
        type:'POST',
        data:'setting=truckh',
        success: function(echo){
            $('#truckSalesPareto').html(echo);
            }
        });

    $.ajax({
        url:'<?php echo base_url();?>index.php/dashboard/truck',
        type:'POST',
        data:'setting=trucki',
        success: function(echo){
            $('#truckSalesWingBox').html(echo);
            }
        });


    $.ajax({
        url:'<?php echo base_url();?>index.php/dashboard/target',
        type:'POST',
        data:'setting=achieves',
        success: function(echo){
            //alert("UHYU");
            $('.target').html(echo);
            }
        });

    $.ajax({
        url:'<?php echo base_url();?>index.php/dashboard/target',
        type:'POST',
        data:'setting=achieves2',
        success: function(echo){
            //alert("UHYU");
            $('.target2').html(echo);
            }
        });

    $.ajax({
        url:'<?php echo base_url();?>index.php/dashboard/truck',
        type:'POST',
        data:'setting=driverritase',
        success: function(echo){
            $('#driverRitaseLoad').html(echo);
            }
        });
		
    $.ajax({
        url:'<?php echo base_url();?>index.php/dashboard/customer',
        type:'POST',
        data:'setting=ritase',
        success: function(echo){
            $('#customerSalesLoad').html(echo);
        }
    })

    

    });
</script>