<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>
            <li class="dropdown">
                <a class='dropdown-toggle count-info' data-toggle='dropdown' href='#'>
                    <i class='fa fa-bell'></i>
                    <span class='label label-primary'>
                        <?php $a = count($stnk);

                        echo $a;
                        ?>
                    </span>
                </a>
                <ul class='dropdown-menu dropdown-alerts'>
                    <li>
                        <a href="<?= site_url('fleet') ?>">
                            <div>
                                <b><?= count($stnk) ?></b> STNK Overdue
                                <span class='pull-right text-muted small'>days ago</span>
                            </div>
                        </a>
                    </li>
                    <li class='divider'></li>
                </ul>
            </li>
            <li>
                <a href="<?php echo site_url('login/logout') ?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="ibox-content ">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <span> Last Update : <b><?php
                                            $tgl = date_create($last[0]['orderdate']);
                                            echo date_format($tgl, "d-m-Y H:i:s");
                                            ?></b> </span>
                </div>
            </div>
        </div>
    </div>

    <div class="row fleetpool">

    </div>


    <div class="row">
        <div class="col-lg-9">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Yearly Report Ritase
                        <small></small>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="lineChart" height="130"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <!--
			<div class="col-lg-3">
			<div class="widget style1 lazur-bg">
			<div class="row">
			<div class="col-xs-4">
			<i class="fa fa-truck fa-5x"></i>
			</div>
			<div class="col-xs-8 text-right">
			<span> Today Orders </span>
			<h2 class="font-bold"><?= $orders->order_today ?></h2>
			</div>
			</div>
			</div>
			</div>
        -->
        <div class="col-lg-3">
            <div class="widget style1 yellow-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-truck fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> <?= date('F') ?> Orders</h3>
                        <h2 class="font-bold"><?= $orders->order_thismonth ?></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="widget style1 yellow-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-truck fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> Avg Ritase by Month</h3>
                        <h2 class="font-bold"><?= number_format($orders->order_thisyear / 12) ?></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="widget style1 yellow-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-truck fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> Jan s/d <?= date('M - Y') ?></h3>
                        <h2 class="font-bold"><?= number_format($orders->order_thisyear) ?></h2>
                    </div>
                </div>
            </div>
        </div>
        <!--
		<div class="col-lg-3">
			<div class="widget yellow-bg style1 text-center">
				<div class="m-b-md">
					<i class="fa fa-truck fa-4x"></i>
					<h1 class="m-xs"><?= $orders->order_thismonth ?></h1>
					<h3 class="font-bold no-margins">
						<?= date('F') ?> Orders
					</h3>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="widget cc-bg p-lg text-center">
				<div class="m-b-md">
					<i class="fa fa-truck fa-4x"></i>
					<h1 class="m-xs"><?= $orders->order_thisyear ?></h1>
					<h3 class="font-bold no-margins">
						Year <?= date('Y') ?> Orders
					</h3>
				</div>
			</div>
		</div>
	</div>
    TARGET CUSTOMERS-->



        <div class="row target">

        </div>


        <div class="row periodeFleet">

        </div>


        <div class="row">
            <div id="truckRitaseLoad" class="col-md-12">

            </div>
        </div>

        <div class="row">
            <div id="driverRitaseLoad" class="col-md-12">

            </div>
        </div>

        <div class="row">
            <div id="truckSalesLoad" class="col-md-12">

            </div>
        </div>

        <div class="row">
            <div id="truckSalesCarCarrier" class="col-md-12">

            </div>
        </div>
        <div class="row">
            <div id="truckSalesTansya" class="col-md-12">

            </div>
        </div>
        <div class="row">
            <div id="truckSalesTowing" class="col-md-12">

            </div>
        </div>
        <div class="row">
            <div id="truckSalesPareto" class="col-md-12">

            </div>
        </div>
        <div class="row">
            <div id="truckSalesWingBox" class="col-md-12">

            </div>
        </div>
        <div class="row">
            <div id="truckSalesDSO" class="col-md-12">

            </div>
        </div>

        <div class="row">
            <div id="fleetAvailableLoad" class="col-md-12">

            </div>
        </div>

        <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">

        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Peity -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo/peity-demo.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Jvectormap -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

    <!-- EayPIE -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/easypiechart/jquery.easypiechart.js"></script>

    <!-- Sparkline -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/chartJs/Chart.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/demo/sparkline-demo.js"></script>


    <script>
        $(document).ready(function() {



            $.ajax({
                url: '<?php echo base_url(); ?>index.php/dashboard/truck',
                type: 'POST',
                data: 'setting=truckritase',
                success: function(echo) {
                    $('#truckRitaseLoad').html(echo);
                }
            });

            $.ajax({
                url: '<?php echo base_url(); ?>dashboard/fleetavailable',
                type: 'POST',
                data: 'setting=fleetavailable',
                success: function(echo) {
                    $('#fleetAvailableLoad').html(echo);
                }
            });





            $.ajax({
                url: '<?php echo base_url(); ?>dashboard/truck',
                type: 'POST',
                data: 'setting=truckachieves',
                success: function(echo) {
                    $('#truckSalesLoad').html(echo);
                }
            });
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/dashboard/truck',
                type: 'POST',
                data: 'setting=trucke',
                success: function(echo) {
                    $('#truckSalesCarCarrier').html(echo);
                }
            });

            $.ajax({
                url: '<?php echo base_url(); ?>index.php/dashboard/truck',
                type: 'POST',
                data: 'setting=truckf',
                success: function(echo) {
                    $('#truckSalesTansya').html(echo);
                }
            });

            $.ajax({
                url: '<?php echo base_url(); ?>index.php/dashboard/truck',
                type: 'POST',
                data: 'setting=truckg',
                success: function(echo) {
                    $('#truckSalesTowing').html(echo);
                }
            });

            $.ajax({
                url: '<?php echo base_url(); ?>index.php/dashboard/truck',
                type: 'POST',
                data: 'setting=truckh',
                success: function(echo) {
                    $('#truckSalesPareto').html(echo);
                }
            });

            $.ajax({
                url: '<?php echo base_url(); ?>index.php/dashboard/truck',
                type: 'POST',
                data: 'setting=trucki',
                success: function(echo) {
                    $('#truckSalesWingBox').html(echo);
                }
            });


            $.ajax({
                url: '<?php echo base_url(); ?>index.php/dashboard/target',
                type: 'POST',
                data: 'setting=achieves',
                success: function(echo) {

                    $('.target').html(echo);
                }
            });

            $.ajax({
                url: '<?php echo base_url(); ?>index.php/dashboard/target',
                type: 'POST',
                data: 'setting=fleetpool',
                success: function(echo) {

                    $('.fleetpool').html(echo);
                }
            });

            $.ajax({
                url: '<?php echo base_url(); ?>index.php/dashboard/target',
                type: 'POST',
                data: 'setting=achieves2',
                success: function(echo) {

                    $('.target2').html(echo);
                }
            });

            $.ajax({
                url: '<?php echo base_url(); ?>index.php/dashboard/truck',
                type: 'POST',
                data: 'setting=driverritase',
                success: function(echo) {
                    $('#driverRitaseLoad').html(echo);
                }
            });

            $.ajax({
                url: '<?php echo base_url(); ?>index.php/dashboard/customer',
                type: 'POST',
                data: 'setting=ritase',
                success: function(echo) {
                    $('#customerSalesLoad').html(echo);
                }
            })


            var data2 = <?= $datayears; ?>;
            var yearongoing = <?= date('Y') ?>;

            var lineData = {
                labels: ["January", "February", "March", "April", "May", "June", "July", "Agustus", "sep", "okt", "nov", "des"],
                datasets: [

                    {
                        label: yearongoing,
                        backgroundColor: 'rgba(26,179,148,0.5)',
                        borderColor: "rgba(26,179,148,0.7)",
                        pointBackgroundColor: "rgba(26,179,148,1)",
                        pointBorderColor: "#fff",
                        xAxisID: 0,
                        data: data2
                    }
                ]
            };
            Chart.pluginService.register({
                beforeRender: function(chart) {
                    if (chart.config.options.showAllTooltips) {
                        chart.pluginTooltips = [];
                        chart.config.data.datasets.forEach(function(dataset, i) {
                            chart.getDatasetMeta(i).data.forEach(function(sector, j) {
                                chart.pluginTooltips.push(new Chart.Tooltip({
                                    _chart: chart.chart,
                                    _chartInstance: chart,
                                    _data: chart.data,
                                    _options: chart.options,
                                    _active: [sector]
                                }, chart));
                            });
                        });

                        chart.options.tooltips.enabled = false;
                    }
                },
                afterDraw: function(chart, easing) {
                    if (chart.config.options.showAllTooltips) {
                        if (!chart.allTooltipsOnce) {
                            if (easing !== 1)
                                return;
                            chart.allTooltipsOnce = true;
                        }

                        chart.options.tooltips.enabled = true;
                        Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
                            tooltip.initialize();
                            tooltip.update();
                            tooltip.pivot();
                            tooltip.transition(easing).draw();
                        });
                        chart.options.tooltips.enabled = false;
                    }
                }
            })

            var lineOptions = {
                responsive: true,
                tooltips: {
                    callbacks: {
                        title: function(tooltipItems, data) {
                            return '';
                        },
                        label: function(tooltipItem, data) {
                            var datasetLabel = '';
                            var label = data.labels[tooltipItem.index];
                            //return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        }
                    }
                },
                showAllTooltips: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }

            };


            var ctx = document.getElementById("lineChart").getContext("2d");
            new Chart(ctx, {
                type: 'bar',
                data: lineData,
                options: lineOptions
            });




        });
    </script>