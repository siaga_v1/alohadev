<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>
            <li class="dropdown">
                <a class='dropdown-toggle count-info' data-toggle='dropdown' href='#'>
                    <i class='fa fa-bell'></i>
                    <span class='label label-primary'>
                        <?php $a = count($stnk);

                        echo $a;
                        ?>
                    </span>
                </a>
                <ul class='dropdown-menu dropdown-alerts'>
                    <li>
                        <a href="<?= site_url('fleet') ?>">
                            <div>
                                <b><?= count($stnk) ?></b> STNK Overdue
                                <span class='pull-right text-muted small'>days ago</span>
                            </div>
                        </a>
                    </li>
                    <li class='divider'></li>
                </ul>
            </li>
            <li>
                <a href="<?php echo site_url('login/logout') ?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="ibox-content ">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <span> Last Update : <b><?php
                                            $tgl = date_create($last[0]['orderdate']);
                                            echo date_format($tgl, "d-m-Y H:i:s");
                                            ?></b> </span>
                </div>
            </div>
        </div>
    </div>

    <div class="row periodeTarget">

    </div>

    <div class="row inoutbound">

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Sales Year
                        <small></small>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="lineChartSales" height="130"></canvas>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div id="customerSalesLoad" class="col-md-12">

        </div>
    </div>

    <!--TARGET CUSTOMERS-->


    <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">

    </div>
</div>
<!-- Mainly scripts -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- MENU -->
<script src="<?php echo base_url(); ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/chartJs/Chart.min.js"></script>
<script>
    $(document).ready(function() {

        $.ajax({
            url: '<?php echo base_url(); ?>dashboard/target',
            type: 'POST',
            data: 'setting=periodeTarget',
            success: function(echo) {

                $('.periodeTarget').html(echo);
            }
        });


        $.ajax({
            url: '<?php echo base_url(); ?>index.php/dashboard/target',
            type: 'POST',
            data: 'setting=inoutbound',
            success: function(echo) {

                $('.inoutbound').html(echo);
            }
        });

        $.ajax({
            url: '<?php echo base_url(); ?>index.php/dashboard/customer',
            type: 'POST',
            data: 'setting=ritase',
            success: function(echo) {
                $('#customerSalesLoad').html(echo);
            }
        })

        var data3 = <?= $datalastyears; ?>;
        var yearongoing = <?= date('Y') ?>;

        var lineDatasales = {
            labels: ["January", "February", "March", "April", "May", "June", "July", "Agustus", "sep", "okt", "nov", "des"],
            datasets: [

                {
                    label: yearongoing,
                    backgroundColor: 'rgba(26,179,148,0.5)',
                    borderColor: "rgba(26,179,148,0.7)",
                    pointBackgroundColor: "rgba(26,179,148,1)",
                    pointBorderColor: "#fff",
                    xAxisID: 0,
                    data: data3
                }
            ]
        };

        Chart.pluginService.register({
            beforeRender: function(chart) {
                if (chart.config.options.showAllTooltips) {
                    chart.pluginTooltips = [];
                    chart.config.data.datasets.forEach(function(dataset, i) {
                        chart.getDatasetMeta(i).data.forEach(function(sector, j) {
                            chart.pluginTooltips.push(new Chart.Tooltip({
                                _chart: chart.chart,
                                _chartInstance: chart,
                                _data: chart.data,
                                _options: chart.options,
                                _active: [sector]
                            }, chart));
                        });
                    });

                    chart.options.tooltips.enabled = false;
                }
            },
            afterDraw: function(chart, easing) {
                if (chart.config.options.showAllTooltips) {
                    if (!chart.allTooltipsOnce) {
                        if (easing !== 1)
                            return;
                        chart.allTooltipsOnce = true;
                    }

                    chart.options.tooltips.enabled = true;
                    Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
                        tooltip.initialize();
                        tooltip.update();
                        tooltip.pivot();
                        tooltip.transition(easing).draw();
                    });
                    chart.options.tooltips.enabled = false;
                }
            }
        })

        var barOptions = {
            responsive: true,
            tooltips: {
                callbacks: {
                    title: function(tooltipItems, data) {
                        return '';
                    },
                    label: function(tooltipItem, data) {
                        var datasetLabel = '';
                        var label = data.labels[tooltipItem.index];
                        //return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                    }
                }
            },
            showAllTooltips: true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        };

        var lineOptions = {
            responsive: true,
            tooltips: {
                callbacks: {
                    title: function(tooltipItems, data) {
                        return '';
                    },
                    label: function(tooltipItem, data) {
                        var datasetLabel = '';
                        var label = data.labels[tooltipItem.index];
                        //return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    }
                }
            },
            showAllTooltips: true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }

        };

        var ctx = document.getElementById("lineChartSales").getContext("2d");
        new Chart(ctx, {
            type: 'bar',
            data: lineDatasales,
            options: barOptions
        });





    });
</script>