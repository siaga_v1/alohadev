<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | ANR.</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Purchase Order</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a href="<?php echo site_url('SuratPermintaanBarang')?>">Purchase Order</a>
            </li>
            <li class="active">
                <strong>Tambah Purchase Order</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
                <div class="col-lg-12">
                    
                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>Form Permintaan Barang</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                    <form class="form-horizontal" id="formspb" target="_blank" action="<?php echo site_url('PurchaseOrder/test')?>" method="POST"   >
                        <div class="ibox-content">
                            
                                
                                <?php
                                date_default_timezone_set("Asia/Jakarta");
                                  $nowdate = date('ydmGis');

                                    
                                    //echo $nmr;
                                    
                                    if (date('d')=='01' && empty($nomor))
                                        { 
                                            $a = '01'; 
                                        }
                                        elseif($nomor != NULL)
                                        { 
                                            $nmr=$nomor[0]['dape_code'];
                                            //$ex = explode('/', $nmr);
                                            //$re = $ex[0];
                                            $rex=  explode('-', $nmr);
                                            $a = sprintf("%02d", $rex[1]+1); 
                                            //$a = sprintf("%02d", $ex[0]+1); 
                                        }else
                                        { 
                                            $a = '01'; 
                                        }
                                    $now=date('d-m-Y');
                                    $c = array('','I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
                                    $tahun=date('y');

                                ?>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="idpo" value="<?=$SPB[0]['dob_code']?>-<?=$a;?>" type="text" readonly="readonly" />
                                        <input class="form-control" style="display: none" name="dobi" value="<?=$SPB[0]['dob_code']?>" type="text" />
                                    </div>
                                </div>

                                <div class="form-group test">
                                    <label class="col-sm-2 control-label">Supplier</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="idsupplier">
                                            <option value="0">- Pilih -</option>
                                            <?php
                                                foreach ($Supplier as $spl ) {
                                                    ?>
                                                        <option value="<?php echo $spl['iv_code']?>"><?php echo $spl['iv_name']; ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tanggal</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" readonly="readonly  " type="text" name="tanggal" value="<?=$SPB[0]['dob_date']?>" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Armada</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" readonly="readonly  " type="text" id="arm" value="<?=$SPB[0]['arm_nomor_pol']?>" />

                                        <input type="text" hidden="hidden" name="armid" value="<?=$SPB[0]['arm_id']?>" />
                                      
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Request Part</label>
                                    <div class="col-sm-8">
                                       <input class="form-control" readonly="readonly  " type="text" id="usr" name="tanggal" value="<?=$SPB[0]['emp_code']?> <?=$SPB[0]['emp_nama']?>" />

                                     <input type="text" hidden="hidden" name="emp" value="<?=$SPB[0]['emp_id']?>" />
                                    </div>
                                </div>

                                <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Qty</th>
                                        <th>Note</th>
                                        <th>Harga</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $n=1;
                                        foreach ($SPB as $key) {?>
                                        
                                        <tr>
                                            <td><?=$n;?></td>
                                            <td>
                                                <input type="text" hidden="hidden" name="detail[<?=$n?>][item]" value="<?=$key['ii_code'];?>">
                                                <?=$key['ii_code'];?> <?=$key['ii_name'];?>
                                            </td>
                                            <td>
                                                <input type="text" hidden="hidden" name="detail[<?=$n?>][qty]" value="<?=$key['dobi_count'];?>">
                                                <input type="text" hidden="hidden" name="detail[<?=$n?>][id]" value="<?=$key['dobi_id'];?>"> 
                                                <?=$key['dobi_count'];?>
                                            </td>
                                            <td class="test">
                                                <input type="text" class="form-control b-r-sm " name="detail[<?=$n?>][harga]"> 
                                            </td>
                                            <td>
                                                <input type="text" hidden="hidden" name="detail[<?=$n?>][note]" value="<?=$key['dobi_note'];?>">
                                                <?=$key['dobi_note'];?>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="detail[<?=$n?>][cek]">
                                            </td>
                                        </tr>

                                        <?php
                                        $n++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <div class="form-group test">
                                    <input class=" btn btn-success" name="tombol" id="tombol" type="submit" value="Save" />
                                </div>
                        </div>
                        
                    </form>
                    </div>  
                </div>
            </div>
            
        </div>
        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>


<script>

    $(document).ready(function(){
             
    });
</script>