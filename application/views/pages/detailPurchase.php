


<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | ANR.</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Purchase Request</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a href="<?php echo site_url('SuratPermintaanBarang')?>">Purchase</a>
            </li>
            <li class="active">
                <strong>Detail Purchase Order</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
<form class="form-horizontal" id="formspb" action="<?php echo site_url('masterPurchase/approvePurchase');?>" method="POST"   >
    <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>Form Purchase Order</h5>
                            <div class="ibox-tools">
                                <a class="collapse">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            
                                <?php
                                date_default_timezone_set("Asia/Jakarta");
                                  $nowdate = date('ydmGis');
                                    $now=date('d-m-Y');
                                    ?>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No PR</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="idsob" value="<?=$SPB[0]['dob_code']?>" type="text" readonly="readonly" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tanggal</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" readonly="readonly  " type="text" name="tanggal" value="<?=$SPB[0]['dob_date']?>" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Armada</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" readonly="readonly  " type="text" id="arm" name="tanggal" value="<?=$SPB[0]['armname']?>" />
                                        
                                        <select class="form-control" id="armada" name="armada">
                                            <option value="0">- Pilih Armada -</option>
                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Request Part</label>
                                    <div class="col-sm-8">
                                       <input class="form-control" readonly="readonly  " type="text" id="usr" name="tanggal" value="<?=$SPB[0]['ecode']?> <?=$SPB[0]['enama']?>" />

                                        <select class="form-control" id="user" name="armada">
                                            <option value="0">- Pilih User -</option>
                                        </select>
                                    </div>
                                </div>

                                <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Qty</th>
                                        <th>Note</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $n=1;
                                        foreach ($SPB as $key) {?>
                                        
                                        <tr>
                                            <td><?=$n;?></td>
                                            <td><?=$key['ii_name'];?></td>
                                            <td><?=$key['dobi_count'];?></td>
                                            <td><?=$key['dobi_note'];?></td>
                                        </tr>

                                        <?php
                                        $n++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="ibox-footer">
                            <?php 

                                if($SPB[0]['dob_status']=='12')
                                {
                                    echo "<a  href=".site_url('SuratPermintaanBarang/printSPB')."?id=".$SPB[0]['dob_code']." class='btn btn-success'>Print</a>";
                                }
                                elseif ($SPB[0]['dob_status']=='13' && $this->session->userdata('emp_kategori') == '4' ) {
                                   echo "<input type='submit' class='btn btn-primary' value='Approve'>";
                                }
                            ?>
                        </div>
                    </div>  
                </div>
            </div>
            </form>
        </div>
        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>


<script>

    $(document).ready(function(){

        $("#armada").hide();
        $("#user").hide();

        $('#ubah').click(function(){
            //alert  ('asdasd');
            $('#id').attr('type','hidden');
            $('#arm').attr('type','hidden');
            $('#armada').show();
            $('#usr').attr('type','hidden');
            $('#user').show();
            $('#id').attr('type','hidden');
            $('#id').attr('type','hidden');
        });

        $("#Qty").keypress(function (e) {
             //if the letter is not digit then display error and don't type anything
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                $("#errmsg").html("Masukkan Angka").show().fadeOut("slow");
                       return false;
            }
           });


        $("#tombol").attr('disabled','disabled');
        $("#boxitem").attr('hidden','hidden');

            $('#Note').focus(function(){
                //alert("bisa kok");
            var item = $("#item :selected").text();
            var qty = $("#Qty").val();
            var note = $("#Note").val();
            
                if(item=="0"||qty==""||note==""){
                $("#tombol").attr('disabled','disabled');
            }
            else{
                $("#tombol").removeAttr('disabled');
           }
            });
            
        



        $("#tombol").click(function(){
            
            //variable for display
            //var tanggal = $("#tanggal").val();
            var item = $("#item :selected").text();
            var itemv = $("#item :selected").val();
            var qty = $("#Qty").val();
            var note = $("#Note").val();
            
            
            //input start and end
            //var start= $("#start").val();
            //var start = $("#start :selected").text();
         //   var srt = $("#start").val();
          //  var pecah = start.split('-');
            //tanggal
        //    var tanggal = $("#tanggal").val();
        //    var hacep = tanggal.split(',');
            //enddd
            
            
            //variable for save
         //   var labs = $("#lab :selected").text();
       //     var kelass = $("#kelas :selected").text();
        //    var matkuls = $("#matakuliah :selected").text();
        //    var starts = $("#start :selected").text();
            
            var num=$("#tabel tr").length;
            var hed="<tr>";
            var col2="<td><input type='hidden' name='detail["+num+"][items]' id='tgl"+num+"' value='"+itemv+"' />"+ item +"</td>";
            var col3="<td><input type='hidden' name='detail["+num+"][qty]' id='tgl"+num+"' value='"+qty+"' />"+ qty +"</td>";
            var col4="<td><input type='hidden' name='detail["+num+"][note]' id='tgl"+num+"' value='"+note+"' />"+ note +"</td>";
            var act="<td><button type='submit' class='btn btn-danger fa fa-minus-square' id='btndelete'></button></td>";
            var fot="<tr>";
            
            var apd = hed+col2+col3+col4+act+fot;
        
            $("#tabel").append(apd)
            //alert("bisa kok");
            $('#Qty').val(" ");
            $('#item').prop('selectedIndex',0);
            $('#Note').val(" ");
            $("#tombol").attr('disabled','disabled');
                $("#boxitem").removeAttr('hidden');

            return false;

           // $('#Qty').val(" ");
        });


        $("#lab").change(function(){
            var ini=$(this).val();
            var tanggal = $("#tanggal").val();
            var hacep = tanggal.split(',');
            //alert(hacep[0]);remo
            $.ajax({
                type:"POST",
                url: "<?php echo site_url('booking/getsesi');?>",
                dataType: "html",
                data: {
                    hari:hacep[0],
                    tgl:hacep[1],
                    labi:ini
                    },
                success:function(data){
                    $("#start").html(data);
                    //alert(data);
            },
            });
            });
        
        $(document).on('click','#btndelete',function(){
        var vtr = $(this).parent().parent();
        vtr.remove();
        });
        
        
        $("#kelas").change(function(){
            var data = $(this).val();
            var datad = $("#dos").val();
            //alert(data);
            $.ajax({
                type:"POST",
                url: "<?php echo site_url('booking/getmatkul');?>",
                dataType: "html",
                data: {kelas:data,dosen:datad},
                success:function(data){
                    $("#matakuliah").html(data);
                    //alert(data);
            },
            });
            });
        
            var datad = $("#dos").val();
            //var data = $("#kelas").val();
            //alert(data);
            $.ajax({
                type:"POST",
                url: "<?php echo site_url('booking/getkelas');?>",
                dataType: "html",
                data: {dosen:datad},
                success:function(data){
                    $("#kelas").html(data);
                    //alert(data);
            },
            });
        $("#tanggal").change(function(){
            var tanggal = $(this).val();
            var hacep = tanggal.split(',');
            //alert(hacep[0]);
            $.ajax({
                type:"POST",
                url: "<?php echo site_url('booking/cektgl');?>",
                dataType: "html",
                data: {
                    hari:hacep[1],
                    },
                success:function(data){
                    //$("#start").html(data);
                    alert(data);
            },
            });
        });
        $("#ket").change(function(){
            var lab = $("#lab").val();
            var kelas = $("#kelas").val();
            var matkul = $("#matakuliah").val();
            var tanggal = $("#tanggal").val();
            var hacep = tanggal.split(',');
            var start = $("#start").val();
            //alert(hacep[0]);
            $.ajax({
                type:"POST",
                url: "<?php echo site_url('booking/check');?>",
                dataType: "html",
                data: {
                    lab:lab,
                    kls:kelas,
                    mat:matkul,
                    tgl:hacep[1],
                    srt:start,
                    },
                success:function(){
                    alert("Data Sudah Ada");
                    $("#tombol").attr('disabled','disabled');
            },
            });
        });
        
        
    });
</script>