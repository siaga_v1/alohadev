<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Budget</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Finance
            </li>
            <li class="active">
                <strong>Budget</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Budget</h5>
                    <div class="ibox-tools">
                        <a data-toggle="modal" data-target="#myModal5"> 
                            <i class="fa fa-plus"></i>
                        </a>
                        <!-- MODAL INPUT -->
                        <div class="modal inmodal" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <form class="form-horizontal" id="formspb" action="<?php echo site_url('finance/saveBudget')?>" method="POST">
                                    <div class="modal-content animated bounceInRight">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            
                                            <h4 class="modal-title">Input Budget</h4>
                                            
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Code</label>
                                                    <div class="col-sm-8">
                                                        <?php
                                                            if (empty($id))
                                                            {
                                                                $ccode = "0001";
                                                            } else {
                                                                $key= $id[0]['order_id'];
                                                                $pattern = "/(\d+)/";

                                                                $array = preg_split($pattern, $key, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                                                                $ccode = sprintf("%04d",$array[1]+1);

                                                            }
                                                        ?>
                                                        <input class="form-control" name="code"  type="text" value="KAS<?=$ccode?>" readonly="readonly" />
                                                    </div>
                                            </div>  
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Tanggal</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control datepicker" id="orderdate" autocomplete="off"  name="date"  type="text"  autocomplete="    off" />
                                                    </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Akun</label>
                                                    <div class="col-sm-8">
                                                        <select class="form-control" name="costid" id="costid">
                                                            <option></option>
                                                            <?php 
                                                                foreach ($akun as $key) {
                                                            ?>

                                                                <option value="<?=$key['id']?>"><?=$key['name']?></option>

                                                            <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Description</label>
                                                    <div class="col-sm-8">
                                                        <textarea class="form-control" name="keterangan" id="keterangan"></textarea>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Nominal</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" name="nominals" id="nominals" data-type="currency" type="text" />
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            <input type="submit" style="margin-bottom: 5px;" name="submit" class="btn btn-primary" value="Save changes">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>  
                            <!-- MODAL END -->
                        
                    </div>    
                </div>
                
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB"  >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Kode</th>
                                    <th>Keterangan</th>
                                    <th>Nominal</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                    foreach ($Budget as $row) {
                                ?>
                                <tr>
                                    <td><?=$no?></td>
                                    <td><?=$row['date']?></td>
                                    <td><?=$row['name']?></td>
                                    <td><?=$row['description']?></td>
                                    <td align="right"><?=number_format($row['nominal'])?></td>
                                    <td align="right">
                                        <a class="edit" value="<?=$row['id']?>"> 
                                            <span class="label label-success"> <i class="fa fa-edit"> </i></span>
                                            
                                        </a>
                                        <a class="del" value="<?=$row['order_id']?>"> 
                                            <span class="label label-danger"> <i class="fa fa-trash"> </i></span>
                                            
                                        </a>
                                    </td>
                                </tr>
                                <?php 
                                $no++;
                                }
                                
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4"> Total</td>
                                    <td colspan="2"></td>

                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                        </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>



        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#orderdate').datepicker({format:'yyyy-mm-dd'});

            $("input[data-type='currency']").on({
                keyup: function() {
                    formatCurrency($(this));
                },
                blur: function() { 
                    formatCurrency($(this), "blur");
                }
            });


            function formatNumber(n) {
                return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            }


            function formatCurrency(input, blur) {
                var input_val = input.val();

                if (input_val === "") { return; }

                var original_len = input_val.length;

                var caret_pos = input.prop("selectionStart");

                if (input_val.indexOf(".") >= 0) {

                    var decimal_pos = input_val.indexOf(".");

                    var left_side = input_val.substring(0, decimal_pos);
                    var right_side = input_val.substring(decimal_pos);

                    left_side = formatNumber(left_side);

                    right_side = formatNumber(right_side);

                    if (blur === "blur") {
                        right_side += "";
                    }

                    right_side = right_side.substring(0, 2);

                    input_val = "" + left_side + "." + right_side;

                } else {
                    input_val = formatNumber(input_val);
                    input_val = "" + input_val;

                    if (blur === "blur") {
                        input_val += "";
                    }
                }

                input.val(input_val);

                var updated_len = input_val.length;
                caret_pos = updated_len - original_len + caret_pos;
                input[0].setSelectionRange(caret_pos, caret_pos);
            }

            $('#listSPB').DataTable({
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                        i.replace(/[\Rp. ,]/g, '')*1 :
                        typeof i === 'number' ?
                        i : 0;
                    };

                    total = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    pageTotal = api
                    .column( 4, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    
                    $( api.column( 4 ).footer() ).html(
                        'Rp. '+new Intl.NumberFormat('en-ID').format(pageTotal) +' ( Rp. '+ new Intl.NumberFormat('en-ID').format(total) +' total)'
                        );
                }
            });



            $("#necessary").change(function(){
                var idcus=$(this).val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('debitcredit/getDetailNecessary');?>",
                    dataType: "html",
                    data: {
                        id:idcus,
                        setting:"nominal",
                    },
                    success:function(data){
                        $("#nominals").val(data);
                    },
                });
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('debitcredit/getDetailNecessary');?>",
                    dataType: "html",
                    data: {
                        id:idcus,
                        setting:"deskripsi",
                    },
                    success:function(data){
                        $("#description").val(data);
                    },
                });

            });
            $('#costid').change(function(){
                var ini = $('#costid option:selected').val();
                $.ajax({
                    url:'<?php echo base_url();?>additional/getAkun',
                    type:'POST',
                    data:{id:ini},
                    success: function(ech){
                        $('#akun').val(ech);
                    }
                });
            });

            $("#listSPB").on("click", ".edit", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('finance/modalBudget');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                    },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();
                    },
                });
            });

            $("#listSPB").on("click", ".del", function(event) {
                if(confirm("Do you want to delete")){
                    var idcus=$(this).attr('value');
                    $.ajax({
                        type:"POST",
                        url: "<?php echo site_url('finance/deleteBudget');?>",
                        dataType: "html",
                        data: {
                            id:idcus
                        },
                        success:function(){
                            location.reload();
                        },
                    });
                }
            });

            $("#listSPB").on("click", ".detail", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('debitcredit/modalDetail');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                    },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();
                    },
                });
            });

        });

    </script>