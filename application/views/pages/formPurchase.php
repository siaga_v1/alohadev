
<?php
    date_default_timezone_set("Asia/Jakarta");
    $nowdate = date('ydmGis');
    $now=date('d-m-Y');
 ?>

<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | ANR.</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Order Barang</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a href="<?php echo site_url('SuratPermintaanBarang')?>">Order Barang</a>
            </li>
            <li class="active">
                <strong>List Purchase Order</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content p-xl">
                    <div class="row">
                        
                        <div class="col-sm-6">
                            <h5>From:</h5>
                                <address>
                                    <strong>PT. Arief Nusa Raya</strong><br>
                                    JL. Bonjol Sektor 3<br>
                                    Pondok Karya - Pondok Aren<br>
                                    Tangerang Selatan<br>
                                    HP : 0857 1423 2948
                                </address>
                        </div>
                            
                        <div class="col-sm-6 text-right">
                            <h4>Purchase Order No.</h4>
                            <h4 class="text-navy"><?=$PO[0]['dape_code']?></h4>
                            <span>To:</span>
                                <address>
                                    <strong><?=$PO[0]['iv_name']?></strong><br>
                                    <?=$PO[0]['iv_address']?><br>
                                    P : <?=$PO[0]['iv_phone']?>
                                </address>
                                <p>
                                <span><strong>PO Date:</strong> <?=$PO[0]['dape_createtime']?></span><br/>
                        </div>
                    
                    </div>

                    <div class="table-responsive m-t">
                        <table class="table invoice-table">
                            <thead>
                                <tr>
                                    <th>Item List</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Tax</th>
                                    <th>Total Price</th>
                                </tr>
                            </thead>
                            <tbody>

                                        <?php
                                        $total=0;
                                            foreach ($PO as $key ) {
                                                # code...
                                        ?>
                                <tr>
                                    <td>
                                        <div>
                                            <strong><?=$key['ii_name']?></strong>
                                        </div>
                                        <small>
                                            Deskripsi
                                        </small>
                                    </td>
                                    <td><?=$key['dapeid_qty']?></td>
                                    <td>Rp. <?php echo number_format($key['dapeid_price'])?></td>
                                    <td><?=$key['iv_ppn']?>%</td>
                                    <td>Rp. <?php 
                                        $hasil= $key['dapeid_qty']*$key['dapeid_price'];
                                    echo number_format($hasil)?></td>
                                </tr>

                                            <?php 
                                            $total +=$hasil;
                                             }?>
                                                           </tbody>
                        </table>
                    </div><!-- /table-responsive -->
                    <table class="table invoice-total">
                        <tbody>
                            <tr>
                                <td><strong>Sub Total :</strong></td>
                                <td>Rp.  <?php echo number_format($total)?></td>
                            </tr>
                            
                            <tr>
                                <td><strong>TAX :</strong></td>
                                <td>Rp. <?php
                                     $pajak=$total*($PO[0]['iv_ppn']/100);
                                     echo number_format($pajak);
                                    ?></td>
                            </tr>
                            <tr>
                                <td><strong>TOTAL :</strong></td>
                                <td><strong>Rp. <?php
                                    $grand=$total+$pajak;
                                    echo number_format($grand);
                                ?></strong></td>
                            </tr>
                        </tbody>
                    </table>
                
                    <div class="text-right">
                        <button class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
                    </div>
                </div>
            </div>
</div>
</div>
</div>
 <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>