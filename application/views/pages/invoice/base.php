<!--BREADCRUMB-->
<div class="row wrapper white-bg page-heading">
    <div class="col-lg-10">
        <h2>Invoicing</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a>Finance</a>
            </li>
            <li class="active">
                <strong>Invoice</strong>
            </li>
        </ol>
    </div>
</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="row wrapper animated fadeInRight white-bg" id="base_invoice">
    <div class="row">
        <div class="col-lg-12 white-bg">
            <div class="ibox float-e-margins ">
                <div class="ibox-title">
                    <h5>MANAGEMENT INVOICE</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <a class="" href="" id="btn_refresh_invoice">
                            Refresh <i class="fa fa-refresh"></i>
                        </a>
                        <a class="" href="" onclick="return manage_invoice(0)">
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
                <!-- Form Searching -->
                <div class="ibox-content" style="display: none;">
                    <form id="form_search_invoice" name="form_search_invoice" method="post">
                        <div class="form-group  row">
                            <label class="col-sm-2 col-form-label">Search By:</label>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row">
                            <label class="col-sm-2 col-form-label">Invoice No</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="invoiceno"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Customer Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="customer_name"/> 
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button class="btn btn-white btn-sm" type="reset">Cancel</button>
                                <button class="btn btn-primary btn-sm" type="submit">Search data <i class="fa fa-filter"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- End Form Searching -->
            </div>
        </div>
        <div class="col-lg-12 white-bg">
            <!-- Load Main Content of Page -->
            <div class="table-responsive white-bg" id="load_data_invoice" name>
                       
            </div>
            <!-- End Load Main Content of Page -->
        </div>
    </div>
</div>
<script type="text/javascript">
$(function() {
    
    load_invoice_list();
    $("#base_invoice").on("click", "#btn_refresh_invoice", function() {
        load_invoice_list();
        return false;
    });
    
    $("#base_invoice #load_data_invoice").on("click", "#btn_back_invoice", function() {
        load_invoice_list();
        return false;
    });
    
    $("#base_invoice").on('click', '.pagination a', function() {
        
       var url = $(this).attr('href');
       var param = $('#form_search_invoice').serialize();
       load_invoice_list(url, param);
       return false; 
       
    });
    
    $("#base_invoice").on('submit','#form_search_invoice', function() {
        
        var url = "";
        var param = $('#form_search_invoice').serialize();
        load_invoice_list(url, param);
        return false; 
        
    });
    
    $("#base_invoice #load_data_invoice").on('submit','#form_invoice', function() {
        
        var formdata = new FormData(this);
        $.ajax({
            url:"<?=base_url()?>invoice/set",
            type:"POST",
            dataType:"jSon",
            data: formdata,
            cache:false,
            processData:false,
            contentType:false,
            success: function(echo) {
                
                if(echo['type'] != 'error') {
                    //$('#modal_form_invoice').modal('hide');
                    //load_invoice_list();
                    if(echo['id'] > 0) {
                        manage_invoice(echo['id']);
                    }else{
                        load_invoice_list();
                    }
                    
                }
                
                setUpAlert(echo['type'],echo['title'],echo['message']);
                
            }     
        });
        
        return false;
   
   });
   
});

function load_invoice_list(url = "", param = "") {
    var url  = (url != "") ? url : "<?php echo base_url(); ?>invoice/list";
    var param = (param != "") ? param : "";
    
    $.ajax({
        url:url,
        type:"POST",
        data:param,
        success: function(echo) {
            $('#base_invoice #load_data_invoice').html(echo);
        }
    });
}

function manage_invoice(id = 0) {
    $.ajax({
        url:"<?php echo base_url() ?>invoice/form",
        type:"POST",
        data:"id="+id,
        success: function(echo) {
            $('#base_invoice #load_data_invoice').html(echo);
        }
    });
    
    return false;
}

function detail_invoice(id = 0) {
    $.ajax({
        url:"<?php echo base_url() ?>invoice/detail",
        type:"POST",
        data:"id="+id,
        success: function(echo) {
            $('#base_invoice #load_data_invoice').html(echo);
        }
    });
    
    return false;
}

function payment_invoice(id = 0) {
    $.ajax({
        url:"<?php echo base_url() ?>invoice/payment",
        type:"POST",
        data:"id="+id,
        success: function(echo) {
            $('#base_invoice #load_data_invoice').html(echo);
        }
    });
    
    return false;
}

function delete_invoice(id = 0) {
    
    swal({
      title: 'Are you sure?',
      text: "You want to delete this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
            url:"<?=base_url()?>invoice/delete",
            type:"POST",
            dataType:"jSon",
            data:"id="+id,
            success: function(echo) {
                
                if(echo['type'] == 'success') {
                    load_invoice_list();
                }
                
                setUpAlert(echo['type'],echo['title'],echo['message']);
                
            }     
        });
      }
    });
    
    return false;
    
}
</script>