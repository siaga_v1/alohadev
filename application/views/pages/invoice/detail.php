<div class="ibox">
    <div class="ibox-title">
        <h5>Form Invoice <?=$invoiceno != "" ? "#$invoiceno" : ""?> <?=$status != "" ? "<b class='btn btn-info'> <b>Status</b> : ".get_status_invoice($status)."</b>" : "";?></h5>
        <?php if($id > 0){?>
        <div class="ibox-tools">
            <a class="" href="<?=base_url().'invoice/pdf/print/'.$code?>" target="_blank">
                Print PDF <i class="fa fa-file-pdf-o"></i>
            </a>
        </div>
        <?php } ?>
    </div>
    <div class="ibox-content" style="border: 1px dotted silver;">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group row">
                    <label class="col-sm-5 col-form-label">Customer Name</label>
                    <div class="col-sm-7">
                            <?=$id_customers > 0 ? "$customer_code - $customer_name" : "";?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group row">
                    <label class="col-sm-5 col-form-label">Tipe</label>
                    <div class="col-sm-7">
                        <?php
                            echo ($type != "") ?  "$invoicetype_name" : "";
                        ?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group row">
                    <label class="col-sm-5 col-form-label">Receipt No <small>No Kwitansi</small></label>
                    <div class="col-sm-7">
                        <?=$receiptno?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">Invoice No <small>No Faktur</small></label>
                    <div class="col-sm-7">
                        <?=$invoiceno?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">Invoice Date</label>
                    <div class="col-sm-7">
                        <?=$dates?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">Overdue Date</label>
                    <div class="col-sm-7">
                        <?=$dates_overdue?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group row">
                    <label class="col-sm-5 col-form-label">From Company</label>
                    <div class="col-sm-7">
                        <?=$id_companys > 0 ? "$company_nickname - $company_pic_name" : "";?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">No Faktur Pajak</label>
                    <div class="col-sm-7">
                        <?=$po_number?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">To PIC Name</label>
                    <div class="col-sm-7">
                        <?=$pic_name?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">To PIC Phone Number</label>
                    <div class="col-sm-7">
                        <?=$pic_phone?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">Invoice Address To</label>
                    <div class="col-sm-7">
                        <?=$addressto?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div class="form-group  row">
                    <label class="col-sm-5 col-form-label">Notes</label>
                    <div class="col-sm-7">
                        <?=$notes?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if($id > 0){ ?>
    <div class="hr-line-dashed"></div>
    <h4>Detail Invoices</h4>
    <div class="hr-line-dashed"></div>
    <div class="table-responsive white-bg" id="load_data_index_invoicedetail">
        
    </div>
    <!-- End Load Main Content of Page -->
    <?php } ?>
</div>
<script type="text/javascript">
$(function() {
   load_data_index_invoicedetail();    
});

function load_data_index_invoicedetail()
{
    $.ajax({
        url:"<?=base_url()."invoicedetail/detail";?>",
        type:"POST",
        data:{id_fin_invoices: <?=$id;?>, id_customers: <?=$id_customers?>},
        success: function(echo) {
            $('#load_data_index_invoicedetail').html(echo);
        }
    });
}

</script>