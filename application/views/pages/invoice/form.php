<div class="ibox">
    <div class="ibox-title">
        <h5>Form Invoice <?=$invoiceno != "" ? "#$invoiceno" : ""?></h5>
        <?php if($id > 0){?>
        <div class="ibox-tools">
            <a class="" href="<?=base_url().'invoice/pdf/print/'.$code?>" target="_blank">
                Print PDF <i class="fa fa-file-pdf-o"></i>
            </a>
        </div>
        <?php } ?>
    </div>
    <div class="ibox-content" style="">
        <form id="form_invoice" name="form_invoice" method="post">
            <input type="hidden" class="form-control" id="id" name="id" value="<?=$id?>" onfocus="blur()" readonly=""/>
            <input type="hidden" class="form-control" id="code" name="code" value="<?=$code?>" onfocus="blur()" readonly=""/>
            
            <div class="row">
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Customer Name *</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="id_customers" name="id_customers">
                                <?=$id_customers > 0 ? "<option value='$id_customers' selected='selected'>$customer_code - $customer_name</option>" : "";?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Tipe *</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="type" name="type">
                                <option value=""></option>
                                <?php
                                    echo ($type != "") ?  "<option value='$type' selected='selected'>$invoicetype_name</option>" : "";
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
            
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">Invoice No *<br /><small>No Invoice</small></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="invoiceno" name="invoiceno" value="<?=$invoiceno?>"/>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Receipt No *<br /><small>No Kwitansi</small></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="receiptno" name="receiptno" value="<?=$receiptno?>"/>
                        </div>
                    </div>
                </div>
                
            </div>
            
            <div class="row">
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">Invoice Date *</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="dates" name="dates" value="<?=$dates?>" readonly=""/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">Overdue Date *</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="dates_overdue" name="dates_overdue" value="<?=$dates_overdue?>" readonly=""/>
                        </div>
                    </div>
                </div>
            </div>
            <?php /**
            <div class="row">
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">PR Number</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pr_number" name="pr_number" value="<?=$pr_number?>"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">Recap ID</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="recap_id" name="recap_id" value="<?=$recap_id?>"/>
                        </div>
                    </div>
                </div>
            </div>
            */ ?>
            
            <div class="row">
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">From Company *</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="id_companys" name="id_companys">
                                <?=$id_companys > 0 ? "<option value='$id_companys' selected='selected'>$company_nickname - $company_pic_name</option>" : "";?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">No Faktur Pajak</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="po_number" name="po_number" value="<?=$po_number?>"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">To PIC Name *</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pic_name" name="pic_name" value="<?=$pic_name?>"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">To PIC Phone Number</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pic_phone" name="pic_phone" value="<?=$pic_phone?>"/>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">Invoice Address To *</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="addressto" name="addressto"><?=$addressto?></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">Notes *</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="notes" name="notes"><?=$notes?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="hr-line-dashed"></div>
            <div class="form-group row">
                <div class="col-sm-12 text-right">
                    <?php if($status == "" || $status == null || $status == "N"){ ?>
                    <?php   if($total_shipment > 0) { ?>
                    <a class="btn btn-info" onclick="return sending_invoice(<?=$id?>)">Send Invoice</a>
                    <?php   } ?>
                    <button class="btn btn-white" type="submit">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save changes</button>
                    <?php } ?>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
        </form>
    </div>
    <?php if($id > 0){ ?>
    <div class="table-responsive white-bg" id="load_data_index_invoicedetail">
               
    </div>
    <!-- End Load Main Content of Page -->
    <?php } ?>
</div>
<script type="text/javascript">
$(function() {
   load_data_index_invoicedetail();
   $("input[name=dates]").datepicker({
        format: "dd-mm-yyyy"
   });
   
   $("input[name=dates]").on("change", function() {
        var date_arr  = $(this).val().split('-');
        var date_part = date_arr[2] + '-' +  date_arr[1] + '-' + date_arr[0];
        
        var date = new Date(date_part);
        date.setDate(date.getDate() + 30); // add 30 days 
        $("input[name=dates_overdue]").val(formatDate(date));
   });
   
   $("select[name=id_customers]").select2({
        ajax: {
            url: "<?php echo base_url(); ?>customer/autoComplete",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
    
    $("select[name=type]").select2({
        ajax: {
            url: "<?php echo base_url(); ?>invoicetype/autocomplete",
            dataType: "jSon",
            type:"POST",
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    id_customers: $("#id_customers option:selected").val()
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
    $("select[name=id_companys]").select2();
    $("select[name=id_companys]").select2({
        ajax: {
            url: "<?php echo base_url(); ?>company/autocomplete",
            dataType: "jSon",
            type:"POST",
            delay: 250,
            data: function (params) {
                return {
                    q: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
    
});

function load_data_index_invoicedetail()
{
    $.ajax({
        url:"<?=base_url()."invoicedetail/index";?>",
        type:"POST",
        data:{id_fin_invoices: <?=$id;?>, id_customers: <?=$id_customers?>},
        success: function(echo) {
            $('#load_data_index_invoicedetail').html(echo);
        }
    });
    
    return false;
}

function sending_invoice(id)
{
    swal({
      title: 'Are you sure?',
      text: "You want to sending this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, sending it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
            url:"<?=base_url()?>invoice/send_complete_invoice",
            type:"POST",
            dataType:"jSon",
            data:"id="+id,
            success: function(echo) {
                
                if(echo['type'] == 'success') {
                    load_invoice_list();
                }
                
                setUpAlert(echo['type'],echo['title'],echo['message']);
                
            }     
        });
      }
    });
    
    return false;
}

</script>