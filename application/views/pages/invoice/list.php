<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>No Invoice</th>
            <th>Invoice Date</th>
            <th>Customers</th>
            <th>Create Date</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <?php 
    $no=1;
    foreach ($list as $key) {
    ?>
        <tr>        
            <td><?=$no?></td>
            <td><?=$key->noinvoice?></td>
            <td><?=$key->dateinvoice?></td>
            <td><?=$key->cname?></td>
            <td><?=$key->createdatetime?></td>
            <td width="10%">
                <a class="invoice" id="invoice" value="<?=$key->noinvoice?>"> 
                    <span class="label label-primary"> Invoice</span>                       
                </a>
                <a class="edit" value="<?=$key->id?>"> 
                    <span class="label label-success"> <i class="fa fa-edit"> </i></span>                       
                </a>                        
            </td>
        </tr>
        <?php
        $no++; } 
        ?>
    </tbody>
</table>
<div class="box-footer">
    <?php echo (isset($paginator)) ? $paginator : ''; ?>
</div>
