<style>
@page { sheet-size: A4; }
@page receipt { sheet-size: 210mm 300mm }

#main-contents {
    font-family:"courier new", arial, calibri;
}

#main-contents table{
    font-family:"courier new", arial, calibri;
    font-size:12px;
}

p {
    font-family:"courier new", arial, calibri;
    font-size:12px;
}

h1.title {
    font-size:18px;
    margin-bottom: 5px;
    margin-bottom: 0;
}
hr.hr-line-solid {
    color: black;
    height: 1px;
    border-collapse:collapse;
}

div.hr-line-thin {
    border-top: 1px dashed black;
}

.customer-detail {
    margin: 20px 0px;
}

div.company-title {
    font-weight: bold;
    padding: 3px 0px;
}

div.address {
    font-size: 13px;
}

.invoice-header {
    margin: 5px 0px;
}

.side-left {
    width: 48%;
    float: left;
}

.side-right {
    width: 48%;
    float: right;
}

.invoice-header table {
    margin: 0 auto;
    width: 100%;
    padding: 0;
}

.invoice-header table td {
    padding: 2px 0px;
    vertical-align: top;
    margin-bottom: 10px;
}

.invoice-header table td.full-border {
    border:1px solid gray;
}

.invoice-header table td .full-border {
    border:1px solid gray;
    padding: 10px;
}

.invoice-detail table {
    width:100%;
    border-collapse: collapse;
    font-size:12px;
}

.value-spelling {
    text-transform: capitalize;
    font-style: italic;
}

.header-table-detail {
    border-collapse: collapse;
}

</style>
<div id="main-contents">
    <div class="main-contents">
        <div class="invoice-kop" align="center">
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
        <div class="invoice-header">
             <table cellpadding="0" border="0" cellspacing="0">
                <tr>
                    <td colspan="6" align="right">No. <?=$header->receiptno?></td>
                </tr>
                
                <tr>
                    <td colspan="1" width="20%">No Invoice</td>
                    <td width="2%">:</td>
                    <td colspan="4" width="78%"><?=$header->invoiceno?></td>
                </tr>
                
                <tr>
                    <td colspan="1">Tanggal</td>
                    <td>:</td>
                    <td colspan="4"><?=date("d-F-Y", strtotime($header->dates))?></td>
                </tr>
                
                <tr>
                    <td colspan="1">Customer Name</td>
                    <td>:</td>
                    <td colspan="4">
                        <?=$header->customer_name?>
                        <br />
                        <?=$header->addressto?>
                    </td>
                </tr>
                <tr><td colspan="6">&nbsp;</td></tr>
                <tr>
                    <td colspan="6" align="right">
                        <h3>INVOICE</h3>
                    </td>
                </tr>
                <tr>
                    <td colspan='6'>
                        <?php 
                        
                        $pph23 = 0; 
                        $ppn   = 0;
                        $rowspan = 3;
                        if($header->customer_ppn > 0) {
                            $rowspan += 1;
                            $ppn = ($header->customer_ppn/100)*$totalprices; 
                        }
                        if($header->customer_pph > 0) {
                            $rowspan += 1;
                            $pph23 = (2/100)*$totalprices;
                        }
                        ?>
                        <table border="1" cellpading="1" class="header-table-detail">
                            <tr>
                                <th width="3%">NO</th>
                                <th width="48%">Keterangan</th>
                                <th width="15%">Harga (Rp)</th>
                                <th width="15%">Jumlah (Rp)</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>
                                    Biaya Expedisi Truck <br />
                                    Periode <?=date("d-M-Y", strtotime($header->min_orderdates))?> s/d <?=date("d-M-Y", strtotime($header->max_orderdates))?>
                                </td>
                                <td align="right">
                                    <?=number_format($totalprices)?>
                                </td>
                                <td align="right">
                                    <?=number_format($totalprices)?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-bottom: 1px solid white;"></td>
                                <td>Total</td>
                                <td align="right"><?=number_format($totalprices)?></td>
                            </tr>
                            <?php if($ppn > 0){ ?>
                            <tr>
                                <td colspan="2" style="border-top: 1px solid white;border-bottom: 1px solid white;"></td>
                                <td>PPN</td>
                                <td align="right"><?=number_format($ppn)?></td>
                            </tr>
                            <?php } ?>
                            <?php if($pph23 > 0){ ?>
                            <tr>
                                <td colspan="2" style="border-top: 1px solid white;border-bottom: 1px solid white;"></td>
                                <td>PPH23</td>
                                <td align="right"><?=number_format($pph23)?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="2" style="border-top: 1px solid white;"></td>
                                <td>Grand Total</td>
                                <td align="right"><?=number_format($totalprices-$pph23+$ppn)?></td>
                            </tr>
                        </table>
                    
                    </td>
                </tr>
                
                <tr>
                    <td colspan="4" align="left">
                       <!-- Detail Rekening Perusahaan -->
                        <p>
                        Payment :
                        <br />
                        <?php 
                        $rek_lpjm = array(124); 
                        if(in_array($header->id_customers,$rek_lpjm)){ ?>
                        Transfer Via Rekening Bank BCA KCP CIKARANG <br />
                        a/n : PT. LIA PUTRI JAYA MANDIRI <br />
                        a/c : 3431492316
                        <?php }else{ ?>
                        Transfer Via Rekening Bank BCA KCP CIKARANG <br />
                        a/n : H. DAWON BIN NAAT <br />
                        a/c : 8730034220
                        <?php } ?>
                        </p>
                        
                        <br /><br />
                        <!-- End Detail Rekening Perusahaan -->
                    </td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        Bekasi, <?=date("d-F-Y", strtotime($header->dates));?> <br />
                        <?=$company->name?>
                        
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        
                        <?=$company->pic_name?>
                        <br />
                        <?=$company->pic_jabatan?>
                    </td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                
            </table>
        </div>
    </div>
    
</div>