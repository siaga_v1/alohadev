<style>
@page { sheet-size: A4; }
@page receipt { sheet-size: 210mm 300mm }

#main-contents {
    font-family:arial, calibri, cursive;
    font-size:12px;
}

#main-contents table {
    font-family:arial, calibri, cursive;
    font-size:12px;
}

h1.title {
    font-size:18px;
    margin-bottom: 10px;
}
hr.hr-line-solid {
    color: black;
    height: 2px;
    border-collapse:collapse;
}

hr.hr-line-thin {
    color: black;
    height: 1px;
    border-collapse:collapse;
}

.customer-detail {
    margin: 20px 0px;
}

div.company-title {
    font-weight: bold;
    padding: 3px 0px;
    font-size:12px;
}

div.address {
    font-size: 12px;
}

.invoice-header {
    margin: 20px 0px;
}

.side-left {
    width: 48%;
    float: left;
}

.side-right {
    width: 48%;
    float: right;
}

.invoice-header table {
    margin: 0 auto;
    width: 100%;
    padding: 0;
    font-size:12px;
}

.invoice-header table td {
    padding: 0px 0px;
}

.invoice-detail table {
    width:100%;
    border-collapse: collapse;
    font-size:11px;
}

</style>
<div id="main-contents">
    <div class="main-contents">
        
        <div class="invoice-header">
            <h2 align="center">REKAPITULASI ONGKOS ANGKUTAN INTERNAL MULIA</h2>
        </div>
        
        <div class="invoice-detail">
            <table cellpadding="2" border="1">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>No Polisi</th>
                        <th>Nama</th>
                        <th>Type</th>
                        <th>Quantity</th>
                        <th>Biaya Bongkar <br /> Rp. 5,000</th>
                    </tr>
                </thead>
                <tbody>
            <?php 
            $total_prices = 0;
            if($inv_details != null){
                        $no = 1;
                        foreach($inv_details AS $row)
                        {
            ?>  
                    <tr>
                        <td align="center"><?=$no;?></td>
                        <td align="center"><?=date("d-m-Y", strtotime($row->orderdate))?></td>
                        <td><?=$row->fleetplateno?></td>
                        <td><?=$row->driver_name?></td>
                        <td><?=trim(explode("-",$row->destination)[0])?></td>
                        <td><?=$row->loadqty?></td>
                        <td align="right"><?=number_format($row->prices)?></td>
                    </tr>
            <?php       $total_prices += $row->prices;    
                        $no++;
                        }
            } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="right" colspan="6">GRAND TOTAL</td>
                        <td align="right"><?=number_format($total_prices)?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        
    </div>
    
</div>