<div class="invoice-header">
        
     <table cellpadding="0" border="0" cellspacing="0" width="100%">
        <tr>
            <td width="10%">Kepada</td>
            <td width="70%"> : <b><?=$header->customer_name?></b></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td> : <?=$header->addressto?></td>
        </tr>
    </table>
    
</div>

<table cellpadding="2" border="1">
    <thead>
        <tr>
            <th>NO</th>
            <th>DATE</th>
            <th>NO DO</th>
            <th>NO VOUCHER</th>
            <th>NAME OF CUSTOMERS</th>
            <th>PRICE</th>
            <th>DESC</th>
        </tr>
    </thead>
    <tbody>
<?php 
$total_prices = 0;
if($inv_details != null){
            $no = 1;
            foreach($inv_details AS $row)
            {
?>  
        <tr>
            <td align="center"><?=$no;?></td>
            <td align="center"><?=date("d-m-Y", strtotime($row->orderdate))?></td>
            <td><?=$row->shippment?></td>
            <td><?=$row->noVoucher?></td>
            <td><?=$row->destination?><?=$row->droplocations != "" ? "<br/>" .$row->droplocations: ""; ?></td>
            <td align="right"><?=number_format($row->prices)?></td>
            <td><?=$row->description?></td>
        </tr>
<?php       $total_prices += $row->prices;    
            $no++;
            }
} ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="right" colspan="5">GRAND TOTAL</td>
            <td align="right"><?=number_format($total_prices)?></td>
            <td></td>
        </tr>
    </tfoot>
</table>