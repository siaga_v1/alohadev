<div class="invoice-header">
     <h3 align="center">INVOICE</h3>
     <table cellpadding="0" border="0" cellspacing="0" width="100%">
        <tr>
            <td width="10%">Kepada</td>
            <td width="39%"> : <b><?=$header->customer_name?></b></td>
            <td></td>
            <td width="10%">NO INVOICE</td>
            <td width="39%"> : <b><?=$header->invoiceno?></b></td>
        </tr>
        <tr>
            <td rowspan="2">Alamat</td>
            <td rowspan="2"> : <?=$header->addressto?></td>
            <td rowspan="2"></td>
            <td width="10%">TGL INVOICE</td>
            <td width="39%"> : <b><?=date("d F Y", strtotime($header->dates))?></b></td>
        </tr>
        <tr>
            <td width="10%">PERIODE</td>
            <td width="39%"> : <b><?=date("d-M-Y", strtotime($header->min_orderdates))?> s/d <?=date("d-M-Y", strtotime($header->max_orderdates))?></b></td>
        </tr>
    </table>
    
</div>

<table cellpadding="2" border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>Mobil</th>
            <th>No Polisi</th>
            <th>No Surat Jalan</th>
            <th>Tujuan Kirim</th>
            <th>Lokasi</th>
            <th>Harga</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>
<?php 
$total_prices = 0;
if($inv_details != null){
            $no = 1;
            foreach($inv_details AS $row)
            {
?>  
        <tr>
            <td align="center"><?=$no;?></td>
            <td align="center"><?=date("d-m-Y", strtotime($row->orderdate))?></td>
            <td><?=$row->fleettype_name?></td>
            <td><?=$row->fleetplateno?></td>
            <td><?=$row->shippment?></td>
            <td><?=trim(explode("-",$row->destination)[0])?></td>
            <td><?=isset(explode("-",$row->destination)[1]) ? trim(explode("-",$row->destination)[1]) : "" ?></td>
            <td align="right"><?=number_format($row->prices)?></td>
            <td><?=$row->description?></td>
        </tr>
<?php       $total_prices += $row->prices;    
            $no++;
            }
} ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="right" colspan="7">GRAND TOTAL</td>
            <td align="right"><?=number_format($total_prices)?></td>
            <td></td>
        </tr>
    </tfoot>
</table>