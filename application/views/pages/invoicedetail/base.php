<div id="base_invoicedetail">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Detail Invoice #</h5>
            <div class="ibox-tools">
                <a class="collapse-link" target="#search_for_detail">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="" href="" id="btn_refresh_invoicedetail">
                    Refresh <i class="fa fa-refresh"></i>
                </a>
                <a class="" href="" onclick="return manage_invoicedetail(0)">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
        <!-- Form Searching -->
        <div class="ibox-content" id="search_for_detail" style="padding-bottom: 0;">
            <form id="form_search_invoicedetail" name="form_search_invoicedetail" method="post">
                <div class="form-group  row">
                    <label class="col-sm-2 col-form-label">Shipment No</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="shipment"/>
                    </div>
                    
                    <div class="col-sm-3">
                        <button class="btn btn-success btn-sm" type="submit">Search data <i class="fa fa-filter"></i></button>
                        <a class="btn btn-danger btn-sm" href="" onclick="return delete_invoicedetail()" type="submit">Delete Detail <i class="fa fa-trash"></i></a>
                    </div>
                </div>
            </form>
            <div class="hr-line-dashed"></div>
        </div>
        <!-- End Form Searching -->
    </div>
    <div class="col-lg-12 white-bg">
        <!-- Load Main Content of Page -->
        <div class="table-responsive white-bg" id="load_data_invoicedetail">
                   
        </div>
        <!-- End Load Main Content of Page -->
    </div>
    
    <div class="modal fade" id="modal_form_invoicedetail" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="load_form_invoicedetail">
          
        </div>
      </div>
    </div>
    
</div>
<script type="text/javascript">
$(function() {
    
    load_invoicedetail_list();
    $("#base_invoicedetail").on("click", "#btn_refresh_invoicedetail", function() {
        load_invoicedetail_list();
        return false;
    });
    
    $("#base_invoicedetail #load_data_invoicedetail").on("click", "#btn_back_invoicedetail", function() {
        load_invoicedetail_list();
        return false;
    });
    
    $("#base_invoicedetail").on('click', '.pagination a', function() {
        
       var url = $(this).attr('href');
       var param = $('#form_search_invoicedetail').serialize();
       load_invoicedetail_list(url, param);
       return false; 
       
    });
    
    $("#base_invoicedetail").on('submit','#form_search_invoicedetail', function() {
        
        var url = "";
        var param = $('#form_search_invoicedetail').serialize();
        load_invoicedetail_list(url, param);
        return false; 
        
    });
    
    $("#base_invoicedetail #load_form_invoicedetail").on('submit','#form_invoicedetail', function() {
        
        var formdata = new FormData(this);
        $.ajax({
            url:"<?=base_url()?>invoicedetail/set",
            type:"POST",
            dataType:"jSon",
            data: formdata,
            cache:false,
            processData:false,
            contentType:false,
            success: function(echo) {
                
                if(echo['type'] != 'error') {
                    $('#modal_form_invoicedetail').modal('hide');
                    load_invoicedetail_list();
                }
                
                setUpAlert(echo['type'],echo['title'],echo['message']);
                
            }     
        });
        
        return false;
   
   });
   
   $("#base_invoicedetail").on("change","#checkAll_", function() {
        if(this.checked) {
            $('input:checkbox').not(this).prop('checked', this.checked);
        }else{
            $('input:checkbox').not(this).prop('checked', false);
        }
    });
   
});

function load_invoicedetail_list(url = "", param = "") {
    var url  = (url != "") ? url : "<?php echo base_url() ?>invoicedetail/lists";
    var param = (param != "") ? param : "";
    
    $.ajax({
        url:url,
        type:"POST",
        data:param + "&id_fin_invoices=<?=$id_fin_invoices?>",
        success: function(echo) {
            $('#base_invoicedetail #load_data_invoicedetail').html(echo);
        }
    });
}

function manage_invoicedetail(id = 0) {
    $.ajax({
        url:"<?php echo base_url() ?>invoicedetail/form",
        type:"POST",
        data:"id_customers=<?=$id_customers?>&id_fin_invoices=<?=$id_fin_invoices?>&id="+id,
        success: function(echo) {
            $('#base_invoicedetail #load_form_invoicedetail').html(echo);
            $('#modal_form_invoicedetail').modal();
        }
    });
    
    return false;
}

function delete_invoicedetail(id = 0) {
    
    var data = [];
    $("#base_invoicedetail #table_list_invoicedetail input:checked").each(function() {
       data.push($(this).val());
    });

    swal({
      title: 'Are you sure?',
      text: "You want to delete this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
            url:"<?=base_url()?>invoicedetail/delete",
            type:"POST",
            dataType:"jSon",
            data:{ detail_ : data },
            success: function(echo) {
                
                if(echo['type'] != 'error') {
                    load_invoicedetail_list();
                }
                
                setUpAlert(echo['type'],echo['title'],echo['message']);
                
            }     
        });
      }
    });
    
    return false;
    
}
</script>