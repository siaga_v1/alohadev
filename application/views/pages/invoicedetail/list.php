<table class="table table-striped table-bordered table-hover" id="table_list_invoicedetail">
    <thead>
        <tr>
            <th><input type="checkbox" id="checkAll_" value="0" /></th>
            <th>No</th>
            <th>Shipment No</th>
            <th>Order Date</th>
            <th>Origin</th>
            <th>Destination</th>
            <th>Plate No</th>
            <th>Driver Name</th>
            <th>Prices</th>
            <th>Allowances</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $total_price = 0;
    $total_allowance = 0;
    if($rows != null){ 
        $no= ($page > 1) ? $offset+1 : 1;
        foreach ($rows as $row) {
        ?>
        <tr>        
            <td><input type="checkbox" value="<?=$row->id?>" /></td>
            <td><?=$no;?></td>
            <td><?=$row->shippment?></td>
            <td><?=date("d/m/Y", strtotime($row->orderdate))?></td>
            <td><?=$row->origin?></td>
            <td><?=$row->destination?><?=$row->droplocations != "" ?"<br/>". $row->droplocations : ""; ?></td>
            <td><?=$row->fleetplateno?></td>
            <td><?=$row->driver_name?></td>
            <td class="text-right"><?=number_format($row->prices + $row->pricesadd)?></td>
            <td class="text-right"><?=number_format($row->allowances + $row->allowanceadds - $row->allowancereds)?></td>
        </tr>
        <?php 
                $total_price += ($row->prices + $row->pricesadd);
                $total_allowance += ($row->allowances + $row->allowanceadds - $row->allowancereds);
            $no++;
        }
    }else{
        echo "
        <tr>
            <td colspan='10'>Data not found</td>
        </tr>
        ";
    }
        
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="8" align="right">Total</td>
            <td align="right"><?=number_format($total_price)?></td>
            <td align="right"><?=number_format($total_allowance)?></td>
        </tr>
    </tfoot>
</table>
<div class="box-footer">
    <?php //echo (isset($paginator)) ? $paginator : ''; ?>
</div>
