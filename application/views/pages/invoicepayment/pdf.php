<style>
@page { sheet-size: A4; }
@page receipt { sheet-size: 210mm 300mm }

#main-contents {
    font-family:arial, calibri, cursive;
    font-size:11px;
}

#main-contents table {
    font-family:arial, calibri, cursive;
    font-size:12px;
}

h1.title {
    font-size:18px;
    margin-bottom: 10px;
}
hr.hr-line-solid {
    color: black;
    height: 2px;
    border-collapse:collapse;
}

hr.hr-line-thin {
    color: black;
    height: 1px;
    border-collapse:collapse;
}

.customer-detail {
    margin: 20px 0px;
}

div.company-title {
    font-weight: bold;
    padding: 3px 0px;
    font-size:11px;
}

div.address {
    font-size: 11px;
}

.invoice-header {
    margin: 20px 0px;
}

.side-left {
    width: 48%;
    float: left;
}

.side-right {
    width: 48%;
    float: right;
}

.invoice-header table {
    margin: 0 auto;
    width: 100%;
    padding: 0;
    font-size:11px;
}

.invoice-header table td {
    padding: 0px 0px;
}

.invoice-detail table {
    width:100%;
    border-collapse: collapse;
    font-size:11px;
}

.invoice-detail table thead th {
    
    padding: 1px;
    margin: 1px!important;
    height: 20px;
}

</style>
<div id="main-contents">
    <div class="main-contents">
        <div class="customer-detail">
            <h2 class="company-title">
            Detail Pengeluaran Uang
            </h2>
        </div>
        
        <div class="invoice-header">
        
             <table cellpadding="0" border="0" cellspacing="0" width="100%">
                <tr>
                    <td width="15%">Code</td>
                    <td width="34%"> : <b><?=$header->code?></b></td>
                    
                    <td width="15%">Deskripsi Kebutuhan</td>
                    <td width="34%"> : <b><?=$header->desc_2?></b></td>
                    
                    
                </tr>
                <tr>
                    <td width="15%">Tanggal</td>
                    <td width="34%"> : <b><?=$header->dates?></b></td>
                    
                    <td width="15%">Nominal</td>
                    <td width="34%"> : <b><?=number_format($header->price)?></b></td>
                    
                </tr>
                <tr>
                    <td width="15%">Penerima</td>
                    <td width="34%"> : <b><?=$header->penerima_name?></b></td>
                    
                    <td width="15%">Total Pengeluaran</td>
                    <td width="34%"> : <b><?=number_format($header->paid)?></b></td>
                </tr>
                <tr>
                
                    <td width="15%">Kebutuhan</td>
                    <td width="34%"> : <b><?=$header->desc_1_name?></b></td>
                
                    <td width="15%">Sisa Saldo</td>
                    <td width="34%"> : <b><?=number_format($header->price-$header->paid)?></b></td>
                </tr>
            </table>
            
        </div>
        
        <div class="invoice-detail">
            <?=$html_detail;?>
        </div>
        
        <div class="side-left text-center">
            <p>
            Cikarang, <?=date("d-F-Y", strtotime($header->dates))?> <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            ( <?=$this->session->userdata("firstname");?> )
            </p>
        </div>
        
    </div>
    
</div>