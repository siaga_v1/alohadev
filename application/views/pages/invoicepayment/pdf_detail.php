<table border='1' cellpadding='2'>
    <thead>
        <tr>
            <th width="5%" rowspan="2">No</th>
            <th rowspan="2">Transaction Date</th>
            <th rowspan="2">Koordinator</th>
            <th rowspan="2">Source</th>
            <th rowspan="2">Relation</th>
            <th rowspan="2">Description</th>
            <th colspan="2">Nominal</th>
        </tr>
        <tr>
            <th>Keluar (Out)</th>
            <th>Masuk (In)</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $grand_total_prices_in = 0;
    $grand_total_prices_out = 0;
    if($rows != null){ 
        $no= 1;
        $prices_in = 0;
        $prices_out = 0;
        foreach ($rows as $row) {
            $prices_in = ($row->type == 2) ? $row->prices : 0;
            $prices_out = ($row->type == 1) ? $row->prices : 0;
        ?>
        <tr>        
            <td align="center"><?=$no;?></td>
            <td><?=date("d-M-Y H:i", strtotime($row->dates))?></td>
            <td align="left"><?=$row->koordinator_name?></td>
            <td align="left"><?=$row->ppurelation_name?></td>
            <td align="left"><?=$row->relation_code?></td>
            <td align="left"><?=$row->description?></td>
            <td align="right"><?=number_format($prices_out)?></td>
            <td align="right"><?=number_format($prices_in)?></td>
        </tr>
        <?php 
            $grand_total_prices_in += $prices_in;
            $grand_total_prices_out += $prices_out;
            $no++;
        }
    }else{
        echo "
        <tr>
            <td colspan='9'>Data not found</td>
        </tr>
        ";
    }
        
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6" align="right"><b>Grand Total</b></td>
            <td align="right"><b><?=number_format($grand_total_prices_out)?></b></td>
            <td align="right"><b><?=number_format($grand_total_prices_in)?></b></td>
        </tr>
    </tfoot>
</table>
