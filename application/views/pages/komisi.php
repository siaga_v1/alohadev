<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout') ?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Komisi</h2>

        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Finance
            </li>
            <li class="active">
                <strong>Komisi Supir</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>List Komisi Supir</h5>
                </div>

                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB" style="white-space: nowrap;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Supir</th>
                                    <th>Periode</th>
                                    <th>Komisi (A)</th>
                                    <th>Bonus Hadir (B)</th>
                                    <th>Bonus Ritase (C)</th>
                                    <th>Potongan Izin (D)</th>
                                    <th>Potongan Kasbon (E)</th>
                                    <th>Total Komisi (A + B + C - (D + E)</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($list as $row) {
                                ?>
                                    <tr>
                                        <td><?= $no ?></td>
                                        <td><?= $row['supir'] ?></td>
                                        <td><?= $row['date_start'] ?> - <?= $row['date_end'] ?></td>
                                        <td><?= number_format($row['komisi']) ?></td>
                                        <td><?= number_format($row['bonus_hadir']) ?></td>
                                        <td><?= number_format($row['bonus_ritase']) ?></td>
                                        <td><?= number_format($row['potongan_izin']) ?></td>
                                        <td><?= number_format($row['potongan_kasbon']) ?></td>
                                        <td><?php
                                            $totalkomisi = ($row['komisi'] + $row['bonus_hadir'] + $row['bonus_ritase']) - ($row['potongan_izin'] + $row['potongan_kasbon']);
                                            echo number_format($totalkomisi);
                                            ?>
                                        </td>
                                        <td>
                                            <input type="text" id="id_drivers" style="display: none;" value="<?= $row['iddrv'] ?>">
                                            <a class="detail btn btn-success btn-xs detail" value="<?= $row['code'] ?>">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="pdf btn btn-primary btn-xs pdf" value="<?= $row['code'] ?>">
                                                <i class="fa fa-file-pdf-o"></i>
                                            </a>
                                            <a class="excel btn btn-info btn-xs excel" value="<?= $row['code'] ?>">
                                                <i class="fa fa-file-excel-o"></i>
                                            </a>
                                            <a class="del btn btn-success btn-xs del" value="<?= $row['code'] ?>">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                    $no++;
                                }

                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">

</div>



<!-- Mainly scripts -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


<script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<!-- Chosen -->
<script src="<?php echo base_url(); ?>assets/js/plugins/chosen/chosen.jquery.js"></script>

<!-- JSKnob -->
<script src="<?php echo base_url(); ?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

<!-- Input Mask-->
<script src="<?php echo base_url(); ?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<!-- Data picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- NouSlider -->
<script src="<?php echo base_url(); ?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

<!-- Switchery -->
<script src="<?php echo base_url(); ?>assets/js/plugins/switchery/switchery.js"></script>

<!-- IonRangeSlider -->
<script src="<?php echo base_url(); ?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- MENU -->
<script src="<?php echo base_url(); ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Color picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- Clock picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/clockpicker/clockpicker.js"></script>

<!-- Image cropper -->
<script src="<?php echo base_url(); ?>assets/js/plugins/cropper/cropper.min.js"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?php echo base_url(); ?>assets/js/plugins/fullcalendar/moment.min.js"></script>

<!-- Date range picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/js/plugins/select2/select2.full.min.js"></script>

<!-- TouchSpin -->
<script src="<?php echo base_url(); ?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

<!-- Tags Input -->
<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- Dual Listbox -->
<script src="<?php echo base_url(); ?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/toastr/toastr.min.js"></script>

<script>
    $(document).ready(function() {


        $("#listSPB").DataTable({
            autoWidth: true,
            ordering: false
        });

        $("#listSPB").on("click", ".detail", function() {
            var idcus = $(this).attr('value');
            var iddrv = $('#id_drivers').val();
            alert(idcus);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('driver/modalGaji'); ?>",
                dataType: "html",
                data: {
                    code: idcus,
                    id: iddrv,
                    sett: 'edit'
                },
                success: function(data) {
                    $("#modaledit").html(data);
                    $("#modaledit").modal();
                },
            });
        });


        $("#listSPB").on("click", ".pdf", function(event) {
            var idcus = $(this).attr('value');
            window.open("<?php echo base_url(); ?>driver/printKomisi?id=" + idcus);
            return false;

        });


        $("#listSPB").on("click", ".del", function(event) {
            if (confirm("Do you want to delete")) {
                var idcus = $(this).attr('value');
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('driver/deleteGaji'); ?>",
                    dataType: "html",
                    data: {
                        id: idcus
                    },
                    success: function() {
                        location.reload();
                    },
                });
            }
        });

    });
</script>}