<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout') ?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Drivers</h2>

        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Master Data
            </li>
            <li class="active">
                <strong>Drivers</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Drivers</h5>
                    <div class="ibox-tools">

                    </div>
                </div>

                <div class="ibox-content">
                    <form class="form-horizontal" id="form-search-FleetReport" action="#" method="POST">

                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-12">

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Tanggal</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="date" class="form-control" name="date" autocomplete="off">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Driver</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="drivers" name="drivers">
                                            <option></option>
                                            <?php foreach ($list_driver as $key) {
                                                echo "<option value='" . $key['id'] . "'>" . $key['name'] . "</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">&nbsp;</label>
                                    <div class="col-sm-8">
                                        <button class="btn btn-info">Search</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB" style="white-space: nowrap;">
                            <thead>
                                <tr>
                                    <th>Status</th>
                                    <th>Action</th>
                                    <th>Drivers</th>
                                    <th>Absen Time</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div align="right"> Search Result : <span id='searchresult'></span> record</div>

                    </div>
                    <div id='pagination'></div>

                </div>
            </div>
        </div>
    </div>
</div>



<!-- Mainly scripts -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


<script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<!-- Chosen -->
<script src="<?php echo base_url(); ?>assets/js/plugins/chosen/chosen.jquery.js"></script>

<!-- JSKnob -->
<script src="<?php echo base_url(); ?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

<!-- Input Mask-->
<script src="<?php echo base_url(); ?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<!-- Data picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- NouSlider -->
<script src="<?php echo base_url(); ?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

<!-- Switchery -->
<script src="<?php echo base_url(); ?>assets/js/plugins/switchery/switchery.js"></script>

<!-- IonRangeSlider -->
<script src="<?php echo base_url(); ?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- MENU -->
<script src="<?php echo base_url(); ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Color picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- Clock picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/clockpicker/clockpicker.js"></script>

<!-- Image cropper -->
<script src="<?php echo base_url(); ?>assets/js/plugins/cropper/cropper.min.js"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?php echo base_url(); ?>assets/js/plugins/fullcalendar/moment.min.js"></script>

<!-- Date range picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/js/plugins/select2/select2.full.min.js"></script>

<!-- TouchSpin -->
<script src="<?php echo base_url(); ?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

<!-- Tags Input -->
<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- Dual Listbox -->
<script src="<?php echo base_url(); ?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/toastr/toastr.min.js"></script>

<script>
    $(document).ready(function() {

        $('#date').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('#drivers').chosen();

        $('#pagination').on('click', 'a', function(e) {
            e.preventDefault();
            var pageno = $(this).attr('data-ci-pagination-page');
            loadPagination(pageno);
            $('#listSPB tbody').empty();
        });

        //sloadPagination(0);

        $('#form-search-FleetReport').submit(function() {
            loadPagination(0);
            return false;
        });

        function loadPagination(pagno) {
            var param = $('#form-search-FleetReport').serialize();
            $.ajax({
                url: '<?= base_url() ?>driver/loadListAbsensi/' + pagno,
                type: 'post',
                data: param,
                dataType: 'json',
                success: function(response) {
                    $('#pagination').html(response.pagination);
                    $('#searchresult').html(response.total);
                    createTable(response.result, response.row);
                }
            });
        }

        function createTable(result, sno) {
            sno = Number(sno);

            $('#listSPB tbody').empty();
            for (index in result) {
                var id = result[index].id;
                var id_driver = result[index].id_driver;
                var driver = result[index].name;
                var date_absensi = result[index].waktu_absensi;
                var type = result[index].type;
                sno += 1;

                var tr = "<tr>";
                tr += "<td>" + type + "</td>";
                tr += "<td> <a class='action' value='" + id + "' id='1' alt='Hadir'><span class='label label-success penulisan'>Hadir</span></a> <a class='action' value='" + id + "' id='2'  alt='Izin'><span class='label label-info penulisan'>Izin</span></a> <a class='action' value='" + id + "' id='3'  alt='Libur / Hari Besar'><span class='label label-danger penulisan'>Libur / Hari Besar</span></a></td>";
                tr += "<td>" + driver + "</td>";
                tr += "<td>" + date_absensi + "</td>";
                tr += "</tr>";
                $('#listSPB tbody').append(tr);
            }
            $('.action').click(function() {
                var tulis = $(this).attr('alt');
                if (confirm("Ketrangan : " + tulis + "")) {
                    var idcus = $(this).attr('value');
                    var type = $(this).attr('id');
                    var date = $('#date').val();
                    $.ajax({
                        url: '<?= base_url() ?>driver/update_absensi',
                        data: {
                            idcus: idcus,
                            type: type,
                            date: date,
                        },
                        type: 'post',
                        success: function(echo) {
                            //alert(idcus);
                            loadPagination(0);
                        }
                    });
                }
            });
        }
    });
</script>