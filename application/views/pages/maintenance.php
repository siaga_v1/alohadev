<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout') ?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Maintenance</h2>

        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Operational
            </li>
            <li class="active">
                <strong>Maintenance</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Maintenance</h5>
                    <div class="ibox-tools">
                        <a data-toggle="modal" data-target="#myModal5">
                            <i class="fa fa-plus"></i>
                        </a>
                        <!-- MODAL INPUT -->
                        <div class="modal inmodal" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <form class="form-horizontal" id="formspb" action="<?php echo site_url('maintenance/saveMaintenance') ?>" method="POST">
                                    <div class="modal-content animated bounceInRight">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                                            <h4 class="modal-title">Input Maintenance</h4>

                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Code</label>
                                                <div class="col-sm-9">
                                                    <?php
                                                    if (empty($id)) {
                                                        $ccode = "11687";
                                                    } else {
                                                        $key = $id[0]['code'];
                                                        $pattern = "/(\d+)/";

                                                        $array = preg_split($pattern, $key, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                                                        $ccode = $array[1] + 1;
                                                    }
                                                    ?>
                                                    <input class="form-control" name="code" type="text" value="M<?= $ccode ?>" readonly="readonly" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Date</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control datepicker" id="date" autocomplete="off" name="date" type="text" readonly="" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Supplier</label>
                                                <div class="col-sm-9">
                                                    <select class="form-control supplier" name="supplier" id="supplier">
                                                        <?php
                                                        foreach ($supplier as $key) {
                                                        ?>

                                                            <option value="<?= $key['id'] ?>"><?= $key['name'] ?></option>

                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Fleet</label>
                                                <div class="col-sm-9">
                                                    <select class="form-control chosen-select" name="fleet" id="fleetmodal">
                                                        <option></option>
                                                        <?php
                                                        foreach ($fleet as $key) {
                                                        ?>

                                                            <option value="<?= $key['id'] ?>"><?= $key['fleetplateno'] ?></option>

                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-12" id="addParts">
                                                    <label class="col-sm-2 control-label">
                                                        Add Parts
                                                    </label>

                                                    <div class="col-sm-9" align="left">
                                                        <button class="btn btn-danger btn-circle Parts" id="Parts" type="button">
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="formParts">

                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            <input type="submit" style="margin-bottom: 5px;" name="submit" class="btn btn-primary" value="Save changes">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- MODAL END -->

                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="form-search-FleetReport" action="#" method="POST">

                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-12">

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Tanggal</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="date" class="form-control" name="date" autocomplete="off">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Driver</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="drivers" name="drivers">
                                            <option></option>
                                            <?php foreach ($list_driver as $key) {
                                                echo "<option value='" . $key['id'] . "'>" . $key['name'] . "</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">&nbsp;</label>
                                    <div class="col-sm-8">
                                        <button class="btn btn-info">Search</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>

                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Code</th>
                                    <th>Fleet</th>
                                    <th>Code Part</th>
                                    <th>Part Number</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6"> Total</td>
                                    <td colspan="2"></td>

                                </tr>
                            </tfoot>
                        </table>
                        <div align="right"> Search Result : <span id='searchresult'></span> record</div>
                    </div>
                    <div id='pagination'></div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">

                </div>
                <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>


<!-- Mainly scripts -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


<script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<!-- Chosen -->
<script src="<?php echo base_url(); ?>assets/js/plugins/chosen/chosen.jquery.js"></script>

<!-- JSKnob -->
<script src="<?php echo base_url(); ?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

<!-- Input Mask-->
<script src="<?php echo base_url(); ?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<!-- Data picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- NouSlider -->
<script src="<?php echo base_url(); ?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

<!-- Switchery -->
<script src="<?php echo base_url(); ?>assets/js/plugins/switchery/switchery.js"></script>

<!-- IonRangeSlider -->
<script src="<?php echo base_url(); ?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- MENU -->
<script src="<?php echo base_url(); ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Color picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- Clock picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/clockpicker/clockpicker.js"></script>

<!-- Image cropper -->
<script src="<?php echo base_url(); ?>assets/js/plugins/cropper/cropper.min.js"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?php echo base_url(); ?>assets/js/plugins/fullcalendar/moment.min.js"></script>

<!-- Date range picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/js/plugins/select2/select2.full.min.js"></script>

<!-- TouchSpin -->
<script src="<?php echo base_url(); ?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

<!-- Tags Input -->
<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- Dual Listbox -->
<script src="<?php echo base_url(); ?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/toastr/toastr.min.js"></script>
<!-- Dual Listbox -->
<script>
    $(document).ready(function() {

        $('#pagination').on('click', 'a', function(e) {
            e.preventDefault();
            var pageno = $(this).attr('data-ci-pagination-page');
            loadPagination(pageno);
            $('#listSPB tbody').empty();
        });

        loadPagination(0);

        $('#form-search-FleetReport').submit(function() {
            loadPagination(0);
            return false;
        });

        function loadPagination(pagno) {
            var param = $('#form-search-FleetReport').serialize();
            $.ajax({
                url: '<?= base_url() ?>maintenance/loadMaintenance/' + pagno,
                type: 'post',
                data: param,
                dataType: 'json',
                success: function(response) {
                    $('#pagination').html(response.pagination);
                    $('#searchresult').html(response.total);
                    createTable(response.result, response.row);
                }
            });
        }

        function createTable(result, sno) {
            sno = Number(sno);

            $('#listSPB tbody').empty();
            for (index in result) {
                var id = result[index].id;
                var date = result[index].date;
                var code = result[index].code;
                var fleet = result[index].fleet;
                var code_part = result[index].code_part;
                var part_number = result[index].part_number;
                var price = result[index].price;
                sno += 1;

                var tr = "<tr>";
                tr += "<td>" + sno + "</td>";
                tr += "<td>" + date + "</td>";
                tr += "<td>" + code + "</td>";
                tr += "<td>" + fleet + "</td>";
                tr += "<td>" + code_part + "</td>";
                tr += "<td>" + part_number + "</td>";
                tr += "<td>" + price + "</td>";
                tr += "<td><a class='detail' value='"+ code +"'><span class='label label-success'> <i class='fa fa-book'> </i></span></a> <a class='edit' value='"+ code +"'><span class='label label-info'> <i class='fa fa-edit'> </i></span></a> <a class='del' value='"+ code +"'><span class='label label-primary'> <i class='fa fa-trash'> </i></span></a></td>";
                tr += "</tr>";
                $('#listSPB tbody').append(tr);
            }

            $("#listSPB").on("click", ".edit", function(event) {
                var idcus = $(this).attr('value');
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('maintenance/modaledit'); ?>",
                    dataType: "html",
                    data: {
                        id: idcus
                    },

                    success: function(data) {
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                    },
                });
            });

            $("#listSPB").on("click", ".detail", function(event) {
                var idcus = $(this).attr('value');
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('maintenance/modalDetail'); ?>",
                    dataType: "html",
                    data: {
                        id: idcus
                    },
                    success: function(data) {
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                    },
                });
            });

            $("#listSPB").on("click", ".del", function(event) {
                if (confirm("Do you want to delete")) {
                    var idcus = $(this).attr('value');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('maintenance/deleteMaintenance'); ?>",
                        dataType: "html",
                        data: {
                            id: idcus
                        },

                        success: function() {
                            location.reload();
                        },
                    });
                }
            });
        };


        $('.chosen-select').chosen({
            width: "100%"
        });

        var tempatRoute = $('#formParts');
        var getId = document.getElementById("formParts");
        var getClass = getId.getElementsByClassName("damageloc");
        var noRoute = getClass.length;

        $('#addParts').on('click', '.Parts', function() {


            $('<div class="uhuy"><div class="form-group"><div class="col-sm-2"><button class="btn btn-danger btn-xs remDefect" id="remDefect_' + noRoute + '" type="button"><i class="fa fa-minus"></i>  Remove</button></div><div class="col-sm-3"><input type="text" class="form-control codeparts" id="codeparts_' + noRoute + '" name="codeparts[' + noRoute + ']" placeholder="Code Parts" ></div><div class="col-sm-3"><input type="text" class="form-control partnumber" id="partnumber_' + noRoute + '" name="partnumber[' + noRoute + ']" placeholder="Part Number"></div><div class="col-sm-3"><input type="text" name="price[' + noRoute + ']" id="price' + noRoute + '" class="form-control prices" placeholder="Price"> </div></div></div>').appendTo(tempatRoute);


            $(".prices").keyup(function() {
                var n = parseInt($(this).val().replace(/\D/g, ''), 10);
                $(this).val(n.toLocaleString());
            });

            noRoute++;
            return false;



        });


        $('#date').datepicker({
            format: 'yyyy-mm-dd'
        });





    });
</script>