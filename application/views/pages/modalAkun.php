<div class="modal-dialog modal-lg">
  <form class="form-horizontal" id="formspb" action="<?php echo site_url('finance/saveAkun')?>" method="POST"   >
    <div class="modal-content animated bounceInRight">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            
        <h4 class="modal-title">Edit Akun</h4>
                                            
      </div>

      <div class="modal-body">     
        <div class="form-group">
          <label class="col-sm-3 control-label">Kode</label>
          <div class="col-sm-8">
          
            <input class="form-control" name="codemodal" value="<?php
          foreach ($akun as $key ) {
            echo $key->code;
          }
        ?>"  type="text" autocomplete="off" />

        <input style="display: none" class="form-control" name="idmodal" value="<?php
          foreach ($akun as $key ) {
            echo $key->id;
          }
        ?>"  type="text"  autocomplete="off" />
          </div>
        </div> 

        <div class="form-group">
          <label class="col-sm-3 control-label">Nama</label>
          <div class="col-sm-8">
                                                           
            <input class="form-control" name="namemodal" value="<?php
          foreach ($akun as $key ) {
            echo $key->name;
          }
        ?>"  type="text" autocomplete="off" />
          </div>
        </div> 
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <input type="submit" style="margin-bottom: 5px;" name="submit" class="btn btn-primary" value="Save">
      </div>
    </div>
  </form>
</div>
