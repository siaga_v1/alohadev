<div class='modal-dialog modal-lg'>
  
    <div class='modal-content animated bounceInRight'>
      <form class='form-horizontal' id='formspb' action="#" method='POST'>
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
        <h4 class='modal-title'>Approve Order</h4>
      </div>
  <!-- INFORMASI ORDER -->
      <div class='col-md-6 col-lg-6' style='margin-top: 30px;'>
        <div class="form-group">
          <label class='col-sm-4 control-label'>Order Code</label>
          <div class='col-sm-8'>
            <div class="form-control form-view-text">
              <?php
                foreach ($order as $key ) {
                  echo $key->code;
                }
              ?>
            </div>
          </div>
        </div>
          
        <div class='form-group'>
          <label class='col-sm-4 col-xs-12 control-label'>Order Date</label>
          <div class='col-sm-8 col-xs-12'>
            <div class='input-group'>
              <input type='text' class='form-control datepicker orderdate' id='orderdat' style='width: 50%;' name='orderdate' value='$ex[0]' readonly=''/>
            
              <div class="input-group bootstrap-timepicker timepicker" style="width: 50%;">
                <input readonly="" id="jma" data-autoclose="true" name="orderdatetime" type="text" class="form-control input-small"/>
                <span class="input-group-addon">
                  <i class="glyphicon glyphicon-time"></i>
                </span>
              </div>
            </div>  
          </div>
        </div>
        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Customer Name</label>
            <div class='col-sm-8'>
              <div class="form-control form-view-text">
              <?php
                  foreach ($order as $key ) {
                    foreach ($customer as $row) {
                      if ($row['id'] == $key->id_customers) {
                        echo $row['name'];
                      }
                    }
                  }
                ?>
            </div>
              
          </div>
        </div>
        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Description</label>
          <div class='col-sm-8'>
            <div class="form-control form-view-text">
             <?php
                foreach ($order as $key ) {
                  echo $key->description;
                }
              ?>
            </div>
          </div>
        </div>
                        
        <div class='form-group'>
          <label class='col-sm-12'><h3 class='text-warning'>Fleet Information</h3></label>
        </div>
                        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Fleet (Armada)</label>
            <div class='col-sm-8'>
              <div class="form-control form-view-text">
                <?php
                  foreach ($order as $key ) {
                    foreach ($fleet as $row) {
                      if ($row['id'] == $key->id_fleets) {
                        echo $row['fleetplateno'];
                      }
                    }
                  }
                ?>
              </div>
          </div>
        </div>
                        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Driver (Supir)</label>
            <div class='col-sm-8'>
               <div class="form-control form-view-text">
                <?php
                  foreach ($order as $key ) {
                    foreach ($driver as $row) {
                      if ($row['id'] == $key->id_drivers) {
                        echo $row['name'];
                      }
                    }
                  }
                ?>
              </div>
            </div>
        </div>

      </div>
                    
      <div class='col-md-6 col-lg-6' style='margin-top: 30px;'>
                        
      <div class="form-group">
  			<label class="col-sm-4 control-label">Shippment No.</label>
  			<div class="col-sm-8">
  				<div class="form-control form-view-text">
            <?php
              foreach ($order as $key ) {
                echo $key->description;
              }
            ?>
          </div>
  			</div>
      </div>
		
		  <div class='form-group'>
          <label class='col-sm-4 control-label'>Origin</label>
          <div class='col-sm-8'>
            <div class="form-control form-view-text">
            <?php
              foreach ($order as $key ) {
                foreach ($cities as $row) {
                  if ($row['id'] == $key->id_citieso) {
                    echo $row['name'];
                  }
                }
              }
            ?>
            </div>
          </div>
        </div>
                    
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Destination</label>
          <div class='col-sm-8'>
           <div class="form-control form-view-text">
            <?php
              foreach ($order as $key ) {
                foreach ($cities as $row) {
                  if ($row['id'] == $key->id_citiesd) {
                    echo $row['name'];
                  }
                }
              }
            ?>
            </div>
          </div>
        </div>
		
		<div class="form-group">
			<label class="col-sm-12"><h3 class="text-warning"></h3></label>
		</div>
		
        <div class="form-group">
			<label class="col-sm-4 control-label">Load Qty</label>
			<div class="col-sm-8">
				<div class="form-control form-view-text">
            <?php
              foreach ($order as $key ) {
                echo $key->id_routes ;
              }
            ?>
        </div>
			</div>
        </div>
	
        
                    
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Order Type</label>
            <div class='col-sm-8'>
              <div class="form-control form-view-text">
              <?php
                  foreach ($order as $key ) {
                    foreach ($fleettypes as $row) {
                      if ($row['id'] == $key->id_ordertypes) {
                      echo $row['name'];
                    }
                  }
                }
              ?>
              </div>
            </div>
        </div>
		
                        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Route (UJS)</label>
            <div class='col-sm-8'>
              <div class="form-control form-view-text">
               <?php
                  foreach ($order as $key ) {
                    foreach ($routes as $row) {
                      if ($row->id == $key->id_routes) {
                        echo "".$row->code." | Rp.".number_format($row->ujs)." | ".$row->n1." To ".$row->c2."";
                      }
                    }
                  }
                ?>
              </div>
            </div>
        </div>
                    
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Allowance (UJS)</label>
          <div class='col-sm-8'>
            <div class="form-control form-view-text">
               <?php
                  foreach ($order as $key ) {
                    foreach ($routes as $row) {
                      if ($row->id == $key->id_routes) {
                        echo $row->allowance;
                      }
                    }
                  }
                ?>
              </div>
          </div>
        </div>
                    
      </div>
                
      

  <div class="modal-footer">
    <table class="table table-bordered"  >
		<tr>
			<th align = "center">Frame No</th>
			<th align = "center">Machine No</th>
			<th align = "center">Color</th>
			<th align = "center">Type</th>
		</tr>
		
		<?php		
		foreach($orderload as $row){
		?>
		<tr>
			<td align = "center"><?=$row['frameno']?></td>
			<td align = "center"><?=$row['machineno']?></td>
			<td align = "center"><?=$row['color']?></td>
			<td align = "center"><?=$row['type']?></td>
		</tr>
		<?php
		}
		?>
	</table>
	<div class='form-group'>
        <div class='col-sm-12 text-right' style='padding-right: 30px;'>
          <button type='submit' class='btn btn-success'>APPROVE!</button>
        </div>
      </div>
  </div>

    </form>
  </div>  
</div>

<script>
$(document).ready(function(){
	
	

     $('#jma').clockpicker();
			
				var origin=$("#id_citio option:selected").val();
                var dest=$("#id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_route").html(data);
                        //alert(data);
                },
                });
				
			$("#id_citio").change(function(){
				alert("UHUY");
                var origin=$(".id_citio option:selected").val();
                var dest=$(".id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_route").html(data);
                        //alert(data);
                },
                });
            });
			
			 $("#id_citid").change(function(){
				alert("UHUY");
                var origin=$("#id_citio option:selected").val();
                var dest=$("#id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_route").html(data);
                        //alert(data);
                },
                });
            });
			
			$("#id_route").change(function(){
                var route=$("#id_route option:selected").val();
                //alert(route);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceAllowance');?>",
                    dataType: "html",
                    data: {
                        routes:route
                        },
                    success:function(data){
                        
						var qty = parseInt($('#loadqt').val());
						var harga = (data);		
						var total = qty * harga;
						$("#allowanc").val(total);
                       // alert(harga);
                },
                });
            });
});


</script>