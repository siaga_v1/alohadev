<div class='modal-dialog modal-lg'>  
    <div class='modal-content animated bounceInRight'>
        <form class='form-horizontal' id='formspb' action="<?=base_url()?>downpayment/bayarDownpayment" method='POST'>
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
                <h4 class='modal-title'>Bayar Kasbon</h4>
            </div>
            <!-- INFORMASI ORDER -->
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama</label>
                    <div class="col-sm-8">
                        <?php foreach ($drivers as $key): ?>
                            <?php if ($key['id']==$id): ?>
                                <h3><?php echo $key['name'] ?></h3>
                            <?php endif ?>
                        <?php endforeach ?>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tanggal</label>
                    <div class="col-sm-8">
                        <input class="form-control datepicker" id="orderdate" autocomplete="off"  name="date" readonly="readonly"  type="text" />
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label">Keterangan</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" name="description" id="description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nominal</label>
                    <div class="col-sm-8">
                        <input class="form-control" name="nominals" data-type="currency" id="nominals"  type="text" />
                        <input class="form-control" name="id_drivers" id="id_drivers" style="display: none;" type="text" value="<?=$id?>" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <input type="submit" style="margin-bottom: 5px;" name="submit" class="btn btn-primary" value="Save changes">
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function(){
        $('#orderdate').datepicker({format:'yyyy-mm-dd', autoclose:true});

        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() { 
                formatCurrency($(this), "blur");
            }
        });


        function formatNumber(n) {
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }


        function formatCurrency(input, blur) {
            var input_val = input.val();

            if (input_val === "") { return; }

            var original_len = input_val.length;

            var caret_pos = input.prop("selectionStart");

            if (input_val.indexOf(".") >= 0) {

                var decimal_pos = input_val.indexOf(".");

                var left_side = input_val.substring(0, decimal_pos);
                var right_side = input_val.substring(decimal_pos);

                left_side = formatNumber(left_side);

                right_side = formatNumber(right_side);

                if (blur === "blur") {
                    right_side += "";
                }

                right_side = right_side.substring(0, 2);

                input_val = "" + left_side + "." + right_side;

            } else {
                input_val = formatNumber(input_val);
                input_val = "" + input_val;

                if (blur === "blur") {
                    input_val += "";
                }
            }

            input.val(input_val);

            var updated_len = input_val.length;
            caret_pos = updated_len - original_len + caret_pos;
            input[0].setSelectionRange(caret_pos, caret_pos);
        }

    });


</script>
