<div class='modal-dialog modal-lg'>
  
    <div class='modal-content animated bounceInRight'>
      <form class='form-horizontal' id='formspb' action="site_url('customer/saveEdit')" method='POST'>
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
        <h4 class='modal-title'>Edit Billing</h4>
      </div>
  <!-- INFORMASI ORDER -->
      <div class='col-md-12' style='margin-top: 30px;'>
        <div class="form-group">
          <label class="col-sm-2 control-label">Billing Code</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="code" name="code" readonly="" value="<?php
          foreach ($billing as $key ) {
            echo $key->code;
          }
        ?>" />
            </div>
          
        </div>
        <a href="#demo" data-toggle="collapse">Simple collapsible<hr></a>
    <div id="demo" class="collapse">
        <div class="form-group">
          <label class="col-sm-2 control-label">Customer Name</label>
            <div class="col-sm-4">
              <select class="form-control" id="id_customers" name="id_customers">
                <option value=""></option>
                  <?php
                  foreach ($billing as $key ) {
                    foreach ($customer as $row) {
                      if ($row['id'] == $key->id_customers) {
                        echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['name']."</option>";
                      }
                    }
                  }
                ?>
              </select>
            </div>
          <label class="col-sm-2 control-label">Customer Area</label>
            <div class="col-sm-4">
              <select class="form-control" id="id_customers" name="id_customers">
                <option value=""></option>
                  <?php
                  foreach ($billing as $key ) {
                    foreach ($customer as $row) {
                      if ($row['id'] == $key->id_customers) {
                        echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['name']."</option>";
                      }
                    }
                  }
                ?>
              </select>
            </div>
        </div>
      </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Origin Name</label>
            <div class="col-sm-4">
              <select class="form-control" id="id_customers" name="id_customers">
                <option value=""></option>
                  <?php
                  foreach ($billing as $key ) {
                    foreach ($cities as $row) {
                      if ($row['id'] == $key->id_citieso) {
                        echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['name']."</option>";
                      }
                    }
                  }
                ?>
              </select>
            </div>
          <label class="col-sm-2 control-label">Destination</label>
            <div class="col-sm-4">
              <select class="form-control" id="id_customers" name="id_customers">
                <option value=""></option>
                  <?php
                  foreach ($billing as $key ) {
                    foreach ($cities as $row) {
                      if ($row['id'] == $key->id_citiesd) {
                        echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['name']."</option>";
                      }
                    }
                  }
                ?>
              </select>
            </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Order Types</label>
            <div class="col-sm-4">
              <select class="form-control" id="id_customers" name="id_customers">
                <option value=""></option>
                  <?php
                  foreach ($billing as $key ) {
                    foreach ($ordertypes as $row) {
                      if ($row['id'] == $key->id_ordertypes) {
                        echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['name']."</option>";
                      }
                    }
                  }
                ?>
              </select>
            </div>
          <label class="col-sm-2 control-label">Order Size Unit</label>
            <div class="col-sm-4">
              <select class="form-control" id="id_customers" name="id_customers">
                <option value=""></option>
                  <?php
                  foreach ($billing as $key ) {
                    foreach ($ordersizeunits as $row) {
                      if ($row['id'] == $key->id_ordersizeunits) {
                        echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['name']."</option>";
                      }
                    }
                  }
                ?>
              </select>
            </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Route</label>
            <div class="col-sm-4">
              <select class="form-control" id="id_customers" name="id_customers">
                <option value=""></option>
                  <?php
                  foreach ($billing as $key ) {
                    foreach ($routes as $row) {
                      if ($row['id'] == $key->id_routes) {
                        echo "<option value='".$row['id']."' selected>".$row['allowance']."</option>";
                      }
                    }
                  }
                ?>
              </select>
            </div>
          <label class="col-sm-2 control-label">Price (Harga Tagihan)</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="code" name="code"/>
            </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Address From</label>
            <div class="col-sm-4">
              <textarea class="form-control" id="description" name="description" rows="1">
                <?php
                  foreach ($billing as $key ) {
                    echo $key->areafrom;
                  }
                ?>
              </textarea>
            </div>
          <label class="col-sm-2 control-label">Address To</label>
            <div class="col-sm-4">
              <textarea class="form-control" id="description" name="description" rows="1">
                <?php
                  foreach ($billing as $key ) { echo $key->areato;
                  }
                ?>
              </textarea>
            </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Geofence From</label>
            <div class="col-sm-4">
              <select class="form-control" id="id_customers" name="id_customers">
                <option value=""></option>
                  <?php
                    foreach ($customer as $row) {
                    ?>
                <option value="<?=$row['id']?>"><?=$row['name']?></option>
                  <?php
                    }
                  ?>
              </select>
            </div>
          <label class="col-sm-2 control-label">Geofence To</label>
            <div class="col-sm-4">
              <select class="form-control" id="id_customers" name="id_customers">
                <option value=""></option>
                  <?php
                    foreach ($customer as $row) {
                  ?>
                <option value="<?=$row['id']?>"><?=$row['name']?></option>
                  <?php
                    }
                  ?>
              </select>
            </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Description</label>
            <div class="col-sm-4">
              <textarea class="form-control" id="description" name="description" rows="2"></textarea>
            </div>
        </div>

      </div>
                
      <div class='form-group'>
        <div class='col-sm-12 text-right' style='padding-right: 30px;'>
          <button type='submit' class='btn btn-primary'>Save!</button>
          <button type='reset' class='btn btn-danger'>Reset</button>
        </div>
      </div>
    </form>

  <!--<div class="modal-footer">
    <div class="tabs-container">
      <ul class="nav nav-tabs" role="tablist">
        <li><a class="nav-link active" data-toggle="tab" href="#tab-1"> This is tab</a></li>
        <li><a class="nav-link" data-toggle="tab" href="#tab-2">This is second tab</a></li>
      </ul>

      <div class="tab-content">
        <div role="tabpanel" id="tab-1" class="tab-pane active">
          <div class="panel-body text-left">
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Allowance (UJS)</label>
              <div class='col-sm-8'>
                <input type='text' class='form-control' readonly='' id='allowance' name='allowance' />
              </div>
            </div>
          </div>
        </div>
        <div role="tabpanel" id="tab-2" class="tab-pane">
          <div class="panel-body">
            <strong>Donec quam felis</strong>
            <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects
            and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath </p>
            <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite
            sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet.</p>
          </div>
        </div>
      </div>
    </div>

  </div>-->

  </div>  
</div>