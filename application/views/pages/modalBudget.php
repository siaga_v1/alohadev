<div class="modal-dialog modal-lg">
  <form class="form-horizontal" id="formspb" action="<?php echo site_url('finance/saveEditBudget')?>" method="POST"   >
    <div class="modal-content animated bounceInRight">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            
        <h4 class="modal-title">Edit Budget</h4>
                                            
      </div>

      <div class="modal-body">     
        <div class="form-group" >
          <label class="col-sm-3 control-label">Code</label>
          <div class="col-sm-8">
            
            <input class="form-control" name="modalcode"  type="text" value="<?php foreach($budget as $key){ echo $key->order_id;}?>" readonly="readonly" />
          </div>
        </div>  
        <div class="form-group">
          <label class="col-sm-3 control-label">Tanggal</label>
          <div class="col-sm-8">
            <input class="form-control datepicker" id="modalorderdate" autocomplete="off"  name="modaldate"  type="text" value="<?php foreach($budget as $key){ echo $key->date;}?>"  autocomplete="    off" />
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Akun</label>
          <div class="col-sm-8">
            <select class="form-control" name="modalcostid" id="costid">
              <option></option>
              <?php 
              foreach ($akun as $key) {
                foreach ($budget as $yes) {
                  if ($yes->cost_id == $key['id']) {
                    ?>

                    <option value="<?=$key['id']?>" selected><?=$key['name']?></option>

                    <?php
                  }else{
                    ?>
                    <option value="<?=$key['id']?>"><?=$key['name']?></option>

                    <?php
                  }
                }
              }
              ?>
            </select>
          </div>
        </div>
        <div class="form-group" >
          <label class="col-sm-3 control-label">Description</label>
          <div class="col-sm-8">
            <textarea class="form-control" name="modalketerangan" id="modalketerangan"><?php foreach($budget as $key){ echo $key->description;}?></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label">Nominal</label>
          <div class="col-sm-8">
            <input class="form-control" name="modalnominals" id="nominals" value="<?php foreach($budget as $key){ echo number_format($key->nominal);}?>"  type="text" />
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <input type="submit" style="margin-bottom: 5px;" name="submit" class="btn btn-primary" value="Save">
      </div>
    </div>
  </form>
</div>
<script>
  $(document).ready(function(){
    $('#modalorderdate').datepicker({format:'yyyy-mm-dd'});
  });
</script>
