<div class='modal-dialog modal-lg'>
  <form class='form-horizontal' id='formspb' action="<?php echo site_url('debitcredit/approvedebit');?>" method='POST'>
    <div class='modal-content animated bounceInRight'>
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
        <h4 class='modal-title'>Detail Debit</h4>
      </div>
    <!-- INFORMASI ORDER -->
      <div class="modal-body">
            
          <div class="form-group">
            <label class="col-sm-3 control-label">Expenditure Code</label>
            <div class="col-sm-8"> 
              <input type="text" name="code" readonly="" class="form-control" id="code" value='<?php
                foreach ($debit as $key ) {
                  echo $key->code;
                }
            ?>'/>
            </div>
          </div>  
          
          <div class="form-group">
            <label class="col-sm-3 control-label">Employee Name</label>
            <div class="col-sm-8">
              <input type="text" name="nama" readonly="" class="form-control" id="nama" value='<?php
                foreach ($debit as $key ) {
                  echo $key->nama;
                }
            ?>'/>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">EC Date</label>
            <div class="col-sm-8">
              <input type="text" name="date" readonly="" class="form-control" id="date" value='<?php
                foreach ($debit as $key ) {
                  echo $key->dates;
                }
            ?>'/>
            </div>
          </div>
<?php 

if ($this->session->userdata('jabatan')=='Admin') 
{ 
  foreach ($debit as $key ) {
  $status =  $key->status;
    if ($status =='PENDING') 
    { 
    
    

?>
          <div class="form-group">
            <label class="col-sm-3 control-label">Transfer Date</label>
            <div class="col-sm-8">
              <input type="text" name="trfdate" class="form-control" id="trfdate" autocomplete=" off" />
            </div>
          </div>
            <?php }}}?>                                    
          <div class="form-group">
            <label class="col-sm-3 control-label">Necessary</label>
            <div class="col-sm-8">
              <input type="text" name="desc_1" readonly="" class="form-control" id="desc_1" value='<?php
                foreach ($debit as $key ) {
                  echo $key->desc_1;
                }
            ?>'/>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-3 control-label">Description</label>
            <div class="col-sm-8">
              <input type="text" name="desc_2" readonly="" class="form-control" id="desc_2" value='<?php
                foreach ($debit as $key ) {
                  echo $key->desc_2;
                }
            ?>'/>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-3 control-label">Nominals</label>
            <div class="col-sm-8">
              <input type="text" name="nominal" readonly="" class="form-control" id="nominal" value='<?php
                foreach ($debit as $key ) {
                  echo $key->price;
                }
            ?>'/>
            </div>
          </div>


      </div> 
      <div class="modal-footer">
        <div class='form-group'>
          <div class='col-sm-12 text-right'>
            

            <button type='submit' class='btn btn-primary' >Approve</button>
          </div>
        </div>
      </div>

    </div>
  </form>
</div>

<script>
$(document).ready(function(){
	
    $('#trfdate').datepicker({format:'yyyy-mm-dd'});
			
				var origin=$("#id_citio option:selected").val();
                var dest=$("#id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_route").html(data);
                        //alert(data);
                },
                });
				
			$("#id_citio").change(function(){
				alert("UHUY");
                var origin=$(".id_citio option:selected").val();
                var dest=$(".id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_route").html(data);
                        //alert(data);
                },
                });
            });
			
			 $("#id_citid").change(function(){
				alert("UHUY");
                var origin=$("#id_citio option:selected").val();
                var dest=$("#id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_route").html(data);
                        //alert(data);
                },
                });
            });
			
			$("#id_route").change(function(){
                var route=$("#id_route option:selected").val();
                //alert(route);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceAllowance');?>",
                    dataType: "html",
                    data: {
                        routes:route
                        },
                    success:function(data){
                        
						var qty = parseInt($('#loadqt').val());
						var harga = (data);		
						var total = qty * harga;
						$("#allowanc").val(total);
                       // alert(harga);
                },
                });
            });
});


</script>