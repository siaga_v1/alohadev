<div class='modal-dialog modal-lg'>
  <?php 
      echo "<form class='form-horizontal' id='formspb' action='".site_url('debitcredit/saveEdit')."' method='POST'>";

    ?>
    <div class='modal-content animated bounceInRight'>
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
        <h4 class='modal-title'>Edit Debit</h4>
      </div>
  <!-- INFORMASI ORDER -->
      <div class="modal-body">
            
          <div class="form-group">
            <label class="col-sm-3 control-label">Expenditure Code</label>
            <div class="col-sm-8"> 
              <input type="text" name="code" readonly="" class="form-control" id="code" value='<?php
                foreach ($debit as $key ) {
                  echo $key->code;
                }
            ?>'/>
            </div>
          </div>  
          
          <div class="form-group">
            <label class="col-sm-3 control-label">Employee Name</label>
            <div class="col-sm-8">
             <select class="form-control" name="nama" id="nama">
              <option value=''></option>
                <?php
                  foreach ($debit as $key ) {
                    foreach ($koordinator as $row) {
                      if ($row['id'] == $key->id_employees) {
                        echo "<option value='".$row['id']."' selected>".$row['firstname']."</option>";
                      }else {
                      echo "<option value='".$row['id']."''>".$row['firstname']."</option>";
                    }}
                  }
                ?>
            </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">EC Date</label>
            <div class="col-sm-8">
             <input type="text" name="date" class="form-control" id="date" value='<?php
                foreach ($debit as $key ) {
                  echo $key->dates;
                }
            ?>'/>
            </div>
          </div>
                                                
          <div class="form-group">
            <label class="col-sm-3 control-label">Necessary</label>
            <div class="col-sm-8">
             <select class="form-control" name="necesary" id="necesary">>
              <option value=''></option>
                <?php
                  foreach ($debit as $key ) {
                    foreach ($necessary as $row) {
                      if ($row['id'] == $key->desc_1) {
                        echo "<option value='".$row['id']."' selected>".$row['necessary']."</option>";
                      }else {
                      echo "<option value='".$row['id']."''>".$row['necessary']."</option>";
                    }}
                  }
                ?>
            </select>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-3 control-label">Description</label>
            <div class="col-sm-8">
              <textarea class="form-control" name="descripton" id="descripton">
                <?php
                  foreach ($debit as $key ) {
                    echo $key->desc_2;
                  }
                ?>
              </textarea>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-3 control-label">Nominals</label>
            <div class="col-sm-8">
              <input type="text" name="nomnals" class="form-control" id="nomnals" value='<?php
                foreach ($debit as $key ) {
                  echo $key->price;
                }
            ?>'/>
            </div>
          </div>  

      </div> 

      <div class="modal-footer">
        <div class='form-group'>
          <div class='col-sm-12 text-right'>
            

            <button type='submit' class='btn btn-primary' >Save!</button>

            <button type='reset' class='btn btn-danger' style="margin-bottom: 5px" >Reset</button>
          </div>
        </div>
      </div>

    </div>
  </form>
</div>

<script>
  $(document).ready(function(){
    $('#date').datepicker({format:'yyyy-mm-dd'});

     $("#necesary").change(function(){
                var idcus=$(this).val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('debitcredit/getDetailNecessary');?>",
                    dataType: "html",
                    data: {
                        id:idcus,
                        setting:"nominal",
                        },
                    success:function(data){
                        $("#nomnals").val(data);
                },
                });
                 $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('debitcredit/getDetailNecessary');?>",
                    dataType: "html",
                    data: {
                        id:idcus,
                        setting:"deskripsi",
                        },
                    success:function(data){
                        $("#descripton").val(data);
                },
                });

            });
  });
</script>