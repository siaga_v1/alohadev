<div class='modal-dialog modal-lg'>  
  <div class='modal-content animated bounceInRight'>
    <form class='form-horizontal' id='formspb' action="<?=base_url()?>driver/gajiDriver" method='POST'>
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
        <h4 class='modal-title'>Detail Downpayment </h4>
      </div>
      <!-- INFORMASI ORDER -->

      <div class="modal-footer">
        <div id="testqtymodal" style="overflow-x:auto;">
          <div>

            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th width="10%" rowspan="2">#</th>
                  <th width="2%" rowspan="2">No</th>
                  <th rowspan="2">Tanggal</th>
                  <th rowspan="2">Supir</th>
                  <th rowspan="2">Description</th>
                  <th colspan="2">Nominal</th>
                </tr>
                <tr>
                  <th>Kasbon</th>
                  <th>Bayar</th>
                </tr>
              </thead>
              <tbody>
                <?php
               
                if($detail != null){ 
                  $grand_total_prices_in = 0;
                  $grand_total_prices_out = 0;
                  $no= 1;
                  $prices_in = 0;
                  $prices_out = 0;
                  foreach ($detail as $row) {
                    $prices_in = ($row->type == 2) ? $row->nominal : 0;
                    $prices_out = ($row->type == 1) ? $row->nominal : 0;
                    ?>
                    <tr>        
                      <td align="center">
                        
                      </td>
                      <td align="center" ><?=$no;?><input type="text" name="id"  id="id<?=$no?>" value="<?=$row->idd?>" style='display: none;'></td>
                      <td><?=date("d M Y", strtotime($row->dates))?></td>
                      <td align="left"><?=$row->name?></td>
                      <td align="left"><?=$row->description?></td>
                      <td align="right"><?=number_format($prices_out)?></td>
                      <td align="right" data-cid='<?=$row->type?>'data-bid='<?=$row->idd?>'><?=number_format($prices_in)?></td>
                    </tr>
                    <?php 
                    $grand_total_prices_in += $prices_in;
                    $grand_total_prices_out += $prices_out;
                    $no++;
                  }
                }else{
                  echo "
                  <tr>
                  <td colspan='9'>Data not found</td>
                  </tr>
                  ";
                }

                ?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="5" align="right"><b>Grand Total</b></td>
                  <td align="right"><b><?=number_format($grand_total_prices_out)?></b></td>
                  <td align="right"><b><?=number_format($grand_total_prices_in)?></b></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>


      </div>
    </form>
  </div>  
</div>

    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function(){

  $("td[data-cid='2']").prop({tabindex: 1, contenteditable: true}).on({
    keyup:function() {
      var a = new Intl.NumberFormat('en-ID').format($(this));
      
      //alert(a);
    },
    blur(){ 
      formatCurrency($(this), "blur");
    },

    focusin() {
      this.__html = $(this).html(); 
    },

    focusout() {
    
      var  cid= this.dataset.cid;
      var  bid= this.dataset.bid;
      var  nominal= this.innerHTML;
        
    if (this.__html === nominal) return;  // Nothing has changed.
    $.ajax({
      url: '<?=base_url()?>downpayment/editinline',
      type: 'post',
      data:{
        id : bid,
        nominal:nominal
      },
      dataType: 'html',
      success: function(data){
        alert("Save Success"); 
      }
    });    
    
  }  
})

  $('#dateInvoice').datepicker({format:'yyyy-mm-dd'});

  function formatNumber(n) {
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  }

  function addCommas(nStr)
{
  nStr += '';
  x = nStr.split(',');
  x1 = x[0];
  x2 = x.length > 1 ? ',' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + '.' + '$2');
  }
  return x;
}


  function formatCurrency(input, blur) {
    var input_val = input.val();

    if (input_val === "") { return; }

    var original_len = input_val.length;

    var caret_pos = input.prop("selectionStart");

    if (input_val.indexOf(".") >= 0) {

      var decimal_pos = input_val.indexOf(".");

      var left_side = input_val.substring(0, decimal_pos);
      var right_side = input_val.substring(decimal_pos);

      left_side = formatNumber(left_side);

      right_side = formatNumber(right_side);

      if (blur === "blur") {
        right_side += "";
      }

      right_side = right_side.substring(0, 2);

      input_val = "" + left_side + "." + right_side;

    } else {
      input_val = formatNumber(input_val);
      input_val = "" + input_val;

      if (blur === "blur") {
        input_val += "";
      }
    }

    input.val(input_val);

    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
  }
  
});


</script>