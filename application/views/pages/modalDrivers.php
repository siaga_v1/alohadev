 <div class='modal-dialog modal-lg'>
          <form class='form-horizontal' id='formspb' action='<?php echo site_url('driver/saveEdit')?>' method='POST'>
            <div class='modal-content animated bounceInRight'>
              <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
                <h4 class='modal-title'>Edit Drivers</h4>
              </div>
              
              <div class='modal-body'>
                <div class='form-group'>
                  <label class='col-sm-3 control-label'>ID</label>
                  
                  <div class='col-sm-8'>
                    <input class='form-control' name='id'  type='text' value="<?php
						  foreach ($drivers as $key ) {
							echo $key->id;
						  }
						?>" readonly='readonly' />
				  </div>
                </div>  
                
               
                <div class="form-group">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-8">
                
                <input class="form-control" name="name"  type="text" value="<?php
						  foreach ($drivers as $key ) {
							echo $key->name;
						  }
						?>" autocomplete="off" />
                </div>
                </div>  

                <div class="form-group">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-8">
                                                       
                <input class="form-control" name="phone" value="<?php
						  foreach ($drivers as $key ) {
							echo $key->phone;
						  }
						?>" type="text" autocomplete="off" />
                </div>
                </div>  

                <div class="form-group">
                <label class="col-sm-3 control-label">License ID</label>
                <div class="col-sm-8">
                                                       
                <input class="form-control" name="idlicense" value="<?php
						  foreach ($drivers as $key ) {
							echo $key->idlicense;
						  }
						?>" type="text" autocomplete="off" />
                </div>
                </div>  
                
              </div>
              
              <div class='modal-footer'>
                <button type='button' class='btn btn-danger' data-dismiss='modal'>Reset</button>
                <input type='submit' style='margin-bottom: 5px;' name='submit' class='btn btn-primary' value='Save!'>
              </div>
            </div>
          </form>
        </div>