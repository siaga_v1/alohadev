<div class='modal-dialog modal-lg'>
  
    <div class='modal-content animated bounceInRight'>
      <form class='form-horizontal' id='formspb' action="<?php echo site_url('order/saveEdit')?>" method='POST'>
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
        <h4 class='modal-title'>Edit Order</h4>
      </div>
  <!-- INFORMASI ORDER -->
      <div class='col-md-6 col-lg-6' style='margin-top: 30px;'>
        <div class="form-group">
          <label class='col-sm-4 control-label'>Order Code</label>
          <div class='col-sm-8'>
            <input type='text' class='form-control' id='code' name='code' value="<?php
          foreach ($order as $key ) {
            echo $key->code;
          }
        ?>" readonly=''/>
          </div>
        </div>
          
        <div class='form-group'>
          <label class='col-sm-4 col-xs-12 control-label'>Order Date</label>
          <div class='col-sm-8 col-xs-12'>
            <div class='input-group'>
              <input type='text' class='form-control datepicker' id='orderdates' style='width: 50%;' name='orderdates' value="<?php
          foreach ($order as $key ) {
            $a = $key->orderdate;
            $b = explode(' ',$a);
            echo $b[0];
          }
        ?>" readonly=''/>
            
              <div class='input-group bootstrap-timepicker timepicker' style='width: 50%;'>
                <input readonly='' id='jam' value="<?php
          foreach ($order as $key ) {
            $a = $key->orderdate;
            $b = explode(' ',$a);
            echo $b[1];
          }
        ?>"  data-autoclose='true' name='orderdatetime' type='text' class='form-control input-small'/>
                <span class='input-group-addon'>
                  <i class='glyphicon glyphicon-time'></i>
                </span>
              </div>
            </div>  
          </div>
        </div>
        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Customer Name</label>
            <div class='col-sm-8'>
              <select class='form-control' id='id_customers' name='id_customers'>
                <option value=''></option>
                <?php
                  foreach ($order as $key ) {
                    foreach ($customer as $row) {
                      if ($row['id'] == $key->id_customers) {
                        echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['name']."</option>";
                    }}
                  }
                ?>
              </select>
          </div>
        </div>
        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Description</label>
          <div class='col-sm-8'>
            <textarea class='form-control' id='description' name='description' rows='2'></textarea>
          </div>
        </div>
                        
        <div class='form-group'>
          <label class='col-sm-12'><h3 class='text-warning'>Fleet Information</h3></label>
        </div>
                        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Fleet (Armada)</label>
            <div class='col-sm-8'>
              <select class='form-control' id='id_fleets' name='id_fleets'>
                <option value=''></option>
                 <?php
                  foreach ($order as $key ) {
                    foreach ($fleet as $row) {
                      if ($row['id'] == $key->id_fleets) {
                        echo "<option value='".$row['id']."' selected>".$row['fleetplateno']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['fleetplateno']."</option>";
                    }}
                  }
                ?>
              </select>
          </div>
        </div>
                        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Driver (Supir)</label>
            <div class='col-sm-8'>
            <select class='form-control' id='id_drivers' name='id_drivers'>
                <option value=''></option>
                <?php
                  foreach ($order as $key ) {
                    foreach ($driver as $row) {
                      if ($row['id'] == $key->id_drivers) {
                        echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['name']."</option>";
                    }}
                  }
                ?>
              </select>
            </div>
        </div>

      </div>
                    
      <div class='col-md-6 col-lg-6' style='margin-top: 30px;'>
                        
        <div class="form-group">
			<label class="col-sm-4 control-label">Shippment No.</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" id="id_shippment" name="id_shippment" value="<?php
          foreach ($order as $key ) {
            echo $key->shippment;
          }
        ?>" >
			</div>
        </div>
		
		    <div class='form-group'>
          <label class='col-sm-4 control-label'>Origin</label>
          <div class='col-sm-8'>
            <select class="form-control id_citio" id="id_citio" name="id_citio">
              <option value=""></option>
                <?php
                  foreach ($order as $key ) {
                    foreach ($cities as $row) {
                      if ($row['id'] == $key->id_citieso) {
                        echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['name']."</option>";
                    }}
                  }
                ?>
              </select>
          </div>
        </div>
                    
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Destination</label>
          <div class='col-sm-8'>
            <select class='form-control id_citid' id='id_citid' name='id_citiesd'>
              <option value=''></option>
                <?php
                  foreach ($order as $key ) {
                    foreach ($cities as $row) {
                      if ($row['id'] == $key->id_citiesd) {
                        echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['name']."</option>";
                    }}
                  }
                ?>
            </select>
          </div>
        </div>
		
		<div class="form-group">
			<label class="col-sm-12"><h3 class="text-warning"></h3></label>
		</div>
		
        <div class="form-group">
			<label class="col-sm-4 control-label">Load Qty</label>
			<div class="col-sm-8">
				<select class="form-control loadqty" id="loadqt" name="loadqty">
					<option value=""></option>
					<?php
					  foreach ($order as $key ) {
						  for($i=1;$i<=8;$i++){
							  if($i == $key->loadqty){
								  echo "<option value='".$key->loadqty."' selected>".$key->loadqty."</option>";
							  }else{
							  echo "<option value='".$i."'>".$i."</option>";
							  }
						  }
					  
						//echo "<option value='".$key->loadqty."' selected>".$key->loadqty."</option>";
					  }
					?>
				</select>
			</div>
        </div>
	
        
                    
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Order Type</label>
            <div class='col-sm-8'>
              <select class='form-control' id='id_ordertypes' name='id_ordertypes'>
                <option value=''></option>
                <?php
                  foreach ($order as $key ) {
                    foreach ($fleettypes as $row) {
                      if ($row['id'] == $key->id_ordertypes) {
                        echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['name']."</option>";
                    }}
                  }
                ?>
              </select>
            </div>
        </div>
		
                        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Route (UJS)</label>
            <div class='col-sm-8'>
              <select class='form-control id_routes' id='id_route' name='id_routes'>
                <option value=''></option>
                <?php
                  foreach ($order as $key ) {
                    foreach ($routes as $row) {
                      if ($row['id'] == $key->id_routes) {
                        echo "<option value='".$row['id']."' selected>".$row['allowance']."</option>";
                      }
                    }
                  }
                ?>
              </select>
            </div>
        </div>
                    
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Allowance (UJS)</label>
          <div class='col-sm-8'>
            <input type='text' class='form-control' readonly='' id='allowanc' name='allowance' />
            <input type="text" style="display: none" class="form-control" readonly="" id="pric" name="price" />
          </div>
        </div>
                    
      </div>
                
      

  <div class="modal-footer">
    <table class="table table-bordered"  >
		<tr>
      <th style="display: none" align = "center">id</th>
			<th align = "center">Frame No</th>
			<th align = "center">Machine No</th>
			<th align = "center">Color</th>
			<th align = "center">Type</th>
		</tr>
		
		<?php		
$no=0;
		foreach($orderload as $row){
      
		?>
		<tr>
      <td align = "center" style="display: none"><input type="text" name="id[<?=$no?>]" value="<?=$row['id']?>"><?=$row['id']?></td>
			<td align = "center"><input type="text" name="frameno[<?=$no?>]" value="<?=$row['frameno']?>"></td>
			<td align = "center"><input type="text" name="fmachineno[<?=$no?>]" value="<?=$row['machineno']?>"></td>
			<td align = "center"><input type="text" name="color[<?=$no?>]" value="<?=$row['color']?>"></td>
			<td align = "center"><input type="text" name="type[<?=$no?>]" value="<?=$row['type']?>"></td>
		</tr>
		<?php
    $no++;
		}
		?>
	</table>
	<div class='form-group'>
        <div class='col-sm-12 text-right' style='padding-right: 30px;'>
          <button type='submit' class='btn btn-primary'>Save!</button>
          <button type='reset' class='btn btn-danger'>Reset</button>
        </div>
      </div>
  </div>

    </form>
  </div>  
</div>

<script>
$(document).ready(function(){


        $('#orderdates').datepicker({format:'yyyy-mm-dd'});


				var origin=$("#id_citio option:selected").val();
                var dest=$("#id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_route").html(data);
                        //alert(data);
                },
                });
				
			$("#id_citio").change(function(){
				//alert("UHUY");
                var origin=$(".id_citio option:selected").val();
                var dest=$(".id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_route").html(data);
                        //alert(data);
                },
                });
            });
			
			 $("#id_citid").change(function(){
				//alert("UHUY");
                var origin=$("#id_citio option:selected").val();
                var dest=$("#id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_route").html(data);
                        //alert(data);
                },
                });
            });
			

///////      
            $("#id_route").change(function(){
                var route=$("#id_route option:selected").val();
                //alert(route);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceAllowance');?>",
                    dataType: "html",
                    data: {
                        routes:route
                        },
                    success:function(data){
                        
            $("#allowanc").val(data);
                       // alert(harga);
                },
                });

                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceUnit');?>",
                    dataType: "html",
                    data: {
                        routes:route
                        },
                    success:function(data){
                        
                        var qty = parseInt($('#loadqt').val());
                        var harga = (data);     
                        var total = qty * harga;
                        $("#pric").val(total);
                       // alert(harga);
                },
                });
            });
});


</script>