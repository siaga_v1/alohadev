<div class='modal-dialog modal-lg'>
  <form class='form-horizontal' id='formspb' action="<?php echo site_url('downpayment/saveEdit');?>" method='POST'>
    <div class='modal-content animated bounceInRight'>
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
        <h4 class='modal-title'>Detail Downpayment</h4>
      </div>
    <!-- INFORMASI ORDER -->
      <div class="modal-body">

        <div class="form-group">
          <label class="col-sm-3 control-label">Code</label>
            <div class="col-sm-8"> 
              <input type="text" name="code" readonly="" class="form-control" id="code" value='<?php
                foreach ($downpayment as $key ) {
                  echo $key->code;
                }
                ?>'/>

                <input type="text" name="ref_id" style="display:none;" readonly="" class="form-control" id="ref_id" value='<?php
                foreach ($downpayment as $key ) {
                  echo $key->id;
                }
                ?>'/>
            </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Date</label>
          <div class="col-sm-8">
            <input type="text" name="date" readonly="" class="form-control" id="date" value='<?php
              foreach ($downpayment as $key ) {
                echo $key->dates;
              }
              ?>'/>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Driver Name</label>
          <div class="col-sm-8">
            <select class="form-control" name="name" id="name">
              <option value=''></option>
                <?php
                  foreach ($downpayment as $key ) {
                    foreach ($drivers as $row) {
                      if ($row['id'] == $key->id_drivers) {
                        echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                      }else {
                      echo "<option value='".$row['id']."'>".$row['name']."</option>";
                      }
                    }
                  }
                ?>
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Description</label>
          <div class="col-sm-8">
            <input type="text" name="desc_1" class="form-control" id="desc_1" value='<?php
                foreach ($downpayment as $key ) {
                  echo $key->description;
                }
            ?>'/>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Nominals</label>
          <div class="col-sm-8">
            <input type="text" name="nominal"  class="form-control" id="nominal" value='<?php
                foreach ($downpayment as $key ) {
                  echo number_format($key->prices);
                }
            ?>'/>
          </div>
        </div>
        <div class="form-group" >
          <div class="col-sm-12" id="addPembayaran">
            <label class="col-sm-3" style="text-align:left"><h3 class="text-warning">Detail Pembayaran</h3></label>

            <div class="col-sm-8" align="left">
              <button class="btn btn-danger btn-circle btnPlus" id="btnPlus" type="button">
                <i class="fa fa-plus"></i>
              </button>
            </div>
          </div>
        </div>

        <div id="formDetail">       
          <?php $nomer =0; ?> 
          <?php foreach ($detail as $key){ ?>
            <div class="form-group">
              <label class="col-sm-3 control-label">Tanggal</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" style="display: none;"  id="id_<?=$nomer?>" name="id['<?=$nomer?>']" readonly="" value="<?=$key->id?>">
                <input type="text" class="form-control dateinv" id="dateInvoice_<?=$nomer?>" name="paydate['<?=$nomer?>']" readonly="" value="<?=$key->date?>">
              </div>
              <label class="col-sm-2 control-label">Nominal</label>
              <div class="col-sm-3">
                <input type="text" class="form-control jumlahbayar" id="jumlahbayar_<?=$nomer?>" name="jumlahbayar['<?=$nomer?>']" value="<?=$key->nominal?>">
              </div>
            </div>
          <?php $nomer++; ?>
          <?php } ?>
        </div>

        <hr style="margin-top: 0px;margin-bottom: 0px;">

        <div class="form-group">
          <label class="col-sm-8 control-label" style="margin-top:  0px">Total Kasbon</label>
          <div class="col-sm-3" >
            <h3 style="margin-top:  0px" align="right"><?php foreach ($downpayment as $key ) { echo number_format($key->prices); }?></h3>
          </div>
        </div>

        <hr style="margin-top: 0px;margin-bottom: 0px;">

        <div class="form-group">
          <label class="col-sm-8 control-label" style="margin-top:  0px">Total Bayar</label>
          <div class="col-sm-3">
            <h3 style="margin-top:  0px" id="totalbayar" align="right"></h3>
          </div>
        </div>

        <hr style="margin-top: 0px;margin-bottom: 0px;">

        <div class="form-group">
          <label class="col-sm-8 control-label" style="margin-top:  0px">Sisa</label>
          <div class="col-sm-3">
            <h1 style="margin-top:  0px" id="selisih" align="right"></h1>
          </div>
        </div>

      </div> 

      <div class="modal-footer">
          <button type='submit' class='btn btn-primary'>Save!</button>
          <button type='reset' class='btn btn-danger'>Reset</button>
      </div>

    </div>
  </form>
</div>

<script>
  $(document).ready(function(){

     $("input[data-type='currency']").on({
  keyup: function() {
    formatCurrency($(this));
  },
  blur: function() { 
    formatCurrency($(this), "blur");
  }
});


 function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");

  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "" + left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += "";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}

    $('#trfdate').datepicker({format:'yyyy-mm-dd'});

    var scntDiv = $('#formDetail');
    var div = document.getElementById("formDetail");
    var nodelist = div.getElementsByClassName("dateinv");
    var  i= nodelist.length;

    $('#addPembayaran').on('click', '.btnPlus',function() {

      $("<div class='form-group'><label class='col-sm-3 control-label'>Tanggal</label><div class='col-sm-3'><input type='text' class='form-control dateinv' id='dateInvoice_"+i+"' name='paydate["+i+"]' readonly='' ></div><label class='col-sm-2 control-label'>Nominal</label><div class='col-sm-3'><input type='text' class='form-control jumlahbayar' id='jumlahbayar_"+i+"' name='jumlahbayar["+i+"]'></div></div>").appendTo(scntDiv);

      $(".jumlahbayar").keyup(function(){
                    var n = parseInt($(this).val().replace(/\D/g,''),10);
                    $(this).val(n.toLocaleString());
                });
      
      $('.dateinv').datepicker({format:'yyyy-mm-dd',autoclose : true});

      $('#jumlahbayar_'+i+'').keyup(function(){
        var totalPoints = 0;
        $('.jumlahbayar').each(function(){
          var inputVal = $(this).val();
          var inputvaltt = parseFloat(inputVal.replace(/,/g, ''));
          if ($.isNumeric(inputvaltt)) {
            totalPoints += parseFloat(inputvaltt);
          }
        });

        var a = $('#nominal').val();
        $('#selisih').html(new Intl.NumberFormat('en-ID').format(parseFloat(a.replace(/,/g, ''))-parseInt(totalPoints)));
        $('#totalbayar').html(new Intl.NumberFormat('en-ID').format(parseInt(totalPoints)));
      });

      i++;
      return false;
    });

    var origin=$("#id_citio option:selected").val();
    var dest=$("#id_citid option:selected").val();

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPriceRoutes');?>",
      dataType: "html",
      data: {
        citieso:origin,
        citiesd:dest
      },
      success:function(data){
        $("#id_route").html(data);
      },
    });

    $("#id_citio").change(function(){
      alert("UHUY");
      var origin=$(".id_citio option:selected").val();
      var dest=$(".id_citid option:selected").val();
      $.ajax({
        type:"POST",
        url: "<?php echo site_url('order/getPriceRoutes');?>",
        dataType: "html",
        data: {
          citieso:origin,
          citiesd:dest
        },
        success:function(data){
          $("#id_route").html(data);
        },
      });
    });

    $("#id_citid").change(function(){
      alert("UHUY");
      var origin=$("#id_citio option:selected").val();
      var dest=$("#id_citid option:selected").val();
      $.ajax({
        type:"POST",
        url: "<?php echo site_url('order/getPriceRoutes');?>",
        dataType: "html",
        data: {
          citieso:origin,
          citiesd:dest
        },
        success:function(data){
          $("#id_route").html(data);
        },
      });
    });

    $("#id_route").change(function(){
      var route=$("#id_route option:selected").val();
      $.ajax({
        type:"POST",
        url: "<?php echo site_url('order/getPriceAllowance');?>",
        dataType: "html",
        data: {
          routes:route
        },
        success:function(data){

          var qty = parseInt($('#loadqt').val());
          var harga = (data);		
          var total = qty * harga;
          $("#allowanc").val(total);
        },
      });
    });
  });


</script>