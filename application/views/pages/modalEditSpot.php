  <div class='modal-dialog modal-lg'>  
    <div class='modal-content animated bounceInRight'>
      <form class='form-horizontal' id='formspb' action="<?php echo site_url('order/saveEditspot')?>" method='POST'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
          <h4 class='modal-title'>Edit Order Spot</h4>
        </div>
          <!-- INFORMASI ORDER -->
        <div class="modal-footer">

          <div class='col-md-6 col-lg-6' style='margin-top: 30px;'>
            <!--
            <div class="form-group">
              <label class='col-sm-4 control-label'>Order Code</label>
              <div class='col-sm-8'>
                <input type='text' class='form-control code' id='code' name='code' value="<?php 
                foreach ($order as $key ) { 
                  echo $key->code; 
                } 
                ?>" readonly="" />
              </div>
            </div>
            -->
            <div class="form-group">
              <label class="col-sm-4 control-label">No JO.</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="id_shippment" name="id_shippment" value="<?php foreach ($order as $key ) { echo $key->shippment; }?>" >
                <input type="text" class="form-control" id="code" style="display: none;" name="code" value="<?php foreach ($order as $key ) { echo $key->code; }?>" >
              </div>
            </div>

            <div class='form-group'>
              <label class='col-sm-4 col-xs-12 control-label' id="demo">Order Date</label>
              <div class='col-sm-8 col-xs-12'>
                <div class='input-group'>
                  <input type='text' class='form-control datepicker' id='orderdates' style='width: 50%;' name='orderdates' value="<?php 
                  foreach ($order as $key ) { 
                    $a = $key->orderdate;
                    $b = explode(' ',$a);
                    echo $b[0];
                  }
                  ?>" readonly=''/>
                
                  <div class='input-group bootstrap-timepicker timepicker' style='width: 50%;'>
                    <input readonly='' id='jam' value="<?php
                    foreach ($order as $key ) {
                      $a = $key->orderdate;
                      $b = explode(' ',$a);
                      echo $b[1];
                    }
                    ?>"  data-autoclose='true' name='orderdatetime' type='text' class='form-control input-small'/>
                    <span class='input-group-addon'>
                      <i class='glyphicon glyphicon-time'></i>
                    </span>
                  </div>

                </div>  
              </div>
            </div>

          <!--
            <div class='form-group'>
              <label class='col-sm-4 control-label'>No Bukti / Voucher</label>
                <div class='col-sm-8'>
                  <input class="form-control" type="text" name="noVoucher" value="<?php
                      foreach ($order as $key ) {
                        echo $key->noVoucher;
                      }
                    ?>">
                </div>
            </div>
          -->
            
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Pengirim</label>
                <div class='col-sm-8'>
                  <select class='form-control' id='id_customeres' name='id_customeres'>
                    <option value=''></option>
                    <?php
                      foreach ($order as $key ) {
                        foreach ($customer as $row) {
                          if ($row['id'] == $key->id_customers) {
                            echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                          }else {
                          echo "<option value='".$row['id']."'>".$row['name']."</option>";
                        }}
                      }
                    ?>
                  </select>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Penerima</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="penerimamodal" name="penerimamodal" value="<?php 
                  foreach ($order as $key ) { 
                   echo $key->penerima; 
                  } 
                  ?>">
                </div>
            </div>
            
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Description</label>
              <div class='col-sm-8'>
                <textarea class='form-control' id='description' name='descriptionmodal' rows='2'>
                  <?php 
                  foreach ($order as $key ) { 
                   echo  $key->description; 
                  } 
                  ?>
                  </textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Invoice A/N</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="invoiceanmodal" name="invoiceanmodal" value="<?php 
                  foreach ($order as $key ) { 
                   echo $key->invoicean; 
                  } 
                  ?>">
                </div>
            </div>
            <!--               
            <div class='form-group text-left'>
              <label class='col-sm-12'><h3 class='text-warning'>Fleet Information</h3></label>
            </div>
                            
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Fleet (Armada)</label>
                <div class='col-sm-8'>
                  <select class='form-control' id='id_fleets' name='id_fleets'>
                    <option value=''></option>
                     <?php
                      foreach ($order as $key ) {
                        foreach ($fleet as $row) {
                          if ($row['id'] == $key->id_fleets) {
                            echo "<option value='".$row['id']."' selected>".$row['fleetplateno']."</option>";
                          }else {
                          echo "<option value='".$row['id']."'>".$row['fleetplateno']."</option>";
                        }}
                      }
                    ?>
                  </select>
              </div>
            </div>


                            
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Driver 1</label>
                <div class='col-sm-8'>
                <select class='form-control' id='id_drivers' name='id_drivers'>
                    <option value=''></option>
                    <?php
                      foreach ($order as $key ) {
                        foreach ($driver as $row) {
                          if ($row['id'] == $key->id_drivers) {
                            echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                          }else {
                          echo "<option value='".$row['id']."'>".$row['name']."</option>";
                        }}
                      }
                    ?>
                  </select>
                </div>
            </div>

            <div class='form-group'> 
              <label class='col-sm-4 control-label'>Driver 2</label>
                <div class='col-sm-8'>
                <select class='form-control' id='id_drivers2' name='id_drivers2'>
                    <option value=''></option>
                    <?php
                      foreach ($order as $key ) {
                        foreach ($driver as $row) {
                          if ($row['id'] == $key->id_drivers2) {
                            echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                          }else {
                          echo "<option value='".$row['id']."'>".$row['name']."</option>";
                        }}
                      }
                    ?>
                  </select>
                </div>
              </div>
            -->

          </div>
                        
          <div class='col-md-6 col-lg-6' style='margin-top: 30px;'>
                            
            
            
            <div class="form-group">
              <div class="col-sm-12" id="addRouteModal">
                <label class="col-sm-4 control-label ">
                  Route
                </label>
                <div class="col-sm-8 text-left">
                  <button class="btn btn-danger btn-circle routeModal" id="routeModal" type="button">
                    <i class="fa fa-plus"></i>
                  </button>
                </div>
              </div>
            </div>

            <div id="formRouteModal">
              <?php
              foreach($order as $row){
                  ?>
              <div class="uhuymodal text-left">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Origin</label>
                  <div class="col-sm-8">
                    <select class="form-control chosen-select id_citiesomodal" id="id_citiesomodal" name="id_citiesomodal">
                    <option value=''></option>
                    <?php
                        foreach ($cities as $key) {
                          if ($key['id'] == $row->id_citieso) {
                            echo "<option value='".$key['id']."' selected>".$key['name']."</option>";
                          } else {
                            echo "<option value='".$key['id']."'>".$key['name']."</option>";
                          }
                        }
                    ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label">Destination</label>
                  <div class="col-sm-8">
                    <select class="form-control chosen-select id_citiesdmodal" id="id_citiesdmodal" name="id_citiesdmodal">
                      <option value=""></option>
                      <?php
                        foreach ($cities as $key) {
                          if ($key['id'] == $row->id_citiesd) {
                            echo "<option value='".$key['id']."' selected>".$key['name']."</option>";
                          } else {
                            echo "<option value='".$key['id']."'>".$key['name']."</option>";
                          }
                        }
                    ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label ">Load Qty</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control loadqtymodalada" id="loadqtymodal" name="loadqtymodal" value="<?php foreach ($order as $key ) { echo $key->loadqty; }?>" >

                  </div>
                </div>


                <div class="form-group">
                  <label class="col-sm-4 control-label"></label>
                  <div class="col-sm-8 text-left">
                    <button class="btn btn-danger btn-xs  remRoutemodal" id="remRoutemodal" type="button"><i class="fa fa-minus"></i>  Remove</button>
                  </div>
                </div>

              </div>
              <?php
              
              }
              ?>
            </div>
           <!--
            <div class="form-group text-left" id="teesmodal" style="display: none;">
              <div class="col-sm-12">              
                <label class="col-sm-4 control-label">
                  Tambah Bongkaran
                </label>
              
                <div class="col-sm-8" >
                  <button class="btn btn-danger btn-circle addScntmodal" id="addScntmodal" type="button">
                    <i class="fa fa-plus"></i>
                  </button>
                                      
                  <div>
                  
                  </div>
                </div>

              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label"></label>
              <div class="col-sm-8 text-left" >
                <div id="p_scentsmodal">
                  <?php  
                  $x=0; 
                  foreach($additional as $row){
                    if ($row['tipe'] == 2) {
                      ?>
                  <div>
                    <select class="form-control p_scntmodal chosen-select" id="p_scntmodal_<?=$x?>"  name="p_scntmodal[<?=$x?>]">
                      <?php 
                      $x++;
                      foreach ($adds as $key) {
                        if ($row['cost_id'] == $key['id']) {
                          echo "<option value='".$key['id']."' selected>".$key['name']."</option>";
                        }else {
                          echo "<option value='".$key['id']."'>".$key['name']."</option>";
                        }
                      }; 
                      ?>
                    </select>
                    <input type="text" class="form-control ujsmodal" style="display: none" readonly="" id="ujsmodal_<?=$x-1?>" name="ujsmodal[<?=$x-1?>]"  value="<?=$row['nominal']?>"  />
                    <input type="text" class="form-control salesmodal" style="display: none" readonly="" id="salesmodal_<?=$x-1?>" name="salesmodal[<?=$x-1?>]"  value="<?=$row['nominal2']?>" />
                        <input type="text" class="form-control" style="display: none" id="kod" name="kod[<?=$x-1?>]" value="<?=$row['id']?>" />
                    <a href="#" class="remScntmodals" id="remScntmodals_<?=$row['id']?>">Remove</a>
                  </div>
                    <?php
                    }
                  }
                  ?>
                </div>
              </div>
            </div>
          -->
            
    		
            <div class="form-group">
        			<label class="col-sm-12"><h3 class="text-warning"></h3></label>
        		</div>
            <!--
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Order Type</label>
                <div class='col-sm-8'>
                  <select class='form-control' id='id_ordertypes' name='id_ordertypes'>
                    <option value=''></option>
                    <?php
                      foreach ($order as $key ) {
                        foreach ($fleettypes as $row) {
                          if ($row['id'] == $key->id_ordertypes) {
                            echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                          }else {
                          echo "<option value='".$row['id']."'>".$row['name']."</option>";
                        }}
                      }
                    ?>
                  </select>
                </div>
            </div>
    		-->
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Allowance (UJS)</label>
              <div class='col-sm-8'>
                <input type='text' class='form-control' readonly='' id='allowancmodal' name='allowancmodal' value="<?php
                      foreach ($order as $key ) {
                            echo $key->prices;
                      }
                    ?>">
              </div>
            </div>

            
            <!--
            <div class="form-group">
              <label class="col-sm-4 control-label">Allowance Add (UJS)</label>
              <div class="col-sm-8">
                <input type="text" class="form-control allowancemodal2" readonly="" id="allowancemodal2" name="allowancemodal2" />
              </div>
            </div>
        -->

            <div class='form-group'>
              <label class='col-sm-4 control-label'>Sales</label>
              <div class='col-sm-8'>
                <input type="text" class="form-control pricemodal" readonly="" id="pricemodal" name="pricemodal" value="<?php
                      foreach ($order as $key ) {
                            echo $key->allowances;
                      }
                    ?>">
              </div>
            </div>

            <div class='form-group'>
              <label class='col-sm-4 control-label'>Uang Titip</label>
              <div class='col-sm-8'>
                <input type="text" class="form-control feesavingmodal" readonly="" id="feesavingmodal" name="feesavingmodal" value="<?php
                      foreach ($order as $key ) {
                            echo $key->feesaving;
                      }
                    ?>">
              </div>
            </div>
<!--
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Add Sales</label>
              <div class='col-sm-8'>
                <input type="text" class="form-control pricemodal2"  readonly="" id="pricemodal2" name="pricemodal2" />
              </div>
            </div>
        -->

                        
          </div>

          <div class="form-group text-left" >
            <div class="col-sm-12" id="addCostmodal">
              <label class="col-sm-2">
                <h3 class="text-warning">Add Fleets</h3>          
              </label>

              <div class="col-sm-8">
                <button class="btn btn-danger btn-circle Costmodal" id="Costmodal" type="button">
                  <i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
          </div>

          <div id="formCostmodal">

             <?php   
              $no= -1;
              $pp= 0;
              $tampung = "beda";

              foreach($orderload as $row){
                if ($tampung != $row['id_routes']) {
                  $no++;
                  $pp= 0;
                }
                
                
                ?>
            <div class="col-sm-12 slebewmodal">
              <div class="form-group testmodal[<?=$pp?>]">
                <div class="col-sm-3">
                  <select class="form-control chosen-select fleetsws" id="id_fleetsmodal_<?=$no?>_<?=$pp?>" name="id_fleetsmodal[<?=$pp?>]" >
                    <option value=""></option>
                    <?php
                      
                        foreach ($fleet as $key) {
                          if ($row['id_fleets']== $key['id']) {
                            echo "<option value='".$key['id']."' selected>".$key['fleetplateno']."</option>";
                          }else {
                          echo "<option value='".$key['id']."'>".$key['fleetplateno']."</option>";
                        }
                      }
                    ?>
                  </select>
                </div>
              <div class="col-sm-3">
                <select class="form-control chosen-select drivesss" id="id_driversmodal_<?=$no?>_<?=$pp?>" name="id_driversmodal[<?=$pp?>]" >
                  <option value=""></option>

                    <?php
                      
                        foreach ($driver as $key) {
                          if ($key['id'] == $row['id_drivers']) {
                            echo "<option value='".$key['id']."' selected>".$key['name']."</option>";
                          }else {
                          echo "<option value='".$key['id']."'>".$key['name']."</option>";
                        }
                      }
                    ?>
                </select>
              </div>
              <div class="col-sm-3">
                <input type="text" style="display: none" class="form-control id_ordertypesmodal" readonly="" id="id_ordertypesmodal_<?=$no?>_<?=$pp?>" name="id_ordertypesmodal[<?=$pp?>]"  />
                <input type="text" style="display: none" class="form-control id" readonly="" id="id_<?=$no?>_<?=$pp?>" name="id[<?=$pp?>]" value="<?=$row['id']?>" />
                <input type="text" class="form-control" readonly="" id="textfleetmodal_<?=$no?>_<?=$pp?>" value="<?=$row['id_ordertypes']?>" />
              </div>
              <div class="col-sm-3">
                <input type="text" class="form-control casemodal" id="casemodal_<?=$no?>_<?=$pp?>" name="casemodal[<?=$pp?>]" value="<?=$row['case']?>" />
              </div>
            </div>
                
            </div>
            <?php
            $pp++;
            

              
              $tampung=$row['id_routes'];
            }
            ?>

            
            

          </div>
        

          

          <div class='form-group'>
            <div class='col-sm-12 text-right' style='padding-right: 30px;'>
              <button type='submit' class='btn btn-primary'>Save!</button>
              <button type='reset' class='btn btn-danger'>Reset</button>
            </div>
          </div>
        </div>
      </form>
    </div>  
  </div>

<script>
$(document).ready(function(){

  
  $('.id_citiesomodal').chosen();
  $('.id_citiesdmodal').chosen();
  $('.loadqtymodal').chosen();

      $('.fleetsws1').chosen();
      $('.drivesse1').chosen();

      var totalSalesUtama = 0; 
      //$('.jssalesmodal').each(function(){var inputVal = $(this).val();if ($.isNumeric(inputVal)) { totalSalesUtama += parseFloat(inputVal); }}); $('#pricemodal').val(totalSalesUtama);

  

/*
  var totalPoints = 0;
    $('.nominemodal').each(function(){
      var inputVal = $(this).val();
        if ($.isNumeric(inputVal)) {
          totalPoints += parseFloat(inputVal);
        }
    });

  var totalPoints2 = 0;
    $('.ujsmodal').each(function(){
      var inputVal2 = $(this).val();
        if ($.isNumeric(inputVal2)) {
          totalPoints2 += parseFloat(inputVal2);
        }
    });
    $('#allowancemodal2').val(totalPoints+totalPoints2);
  var totalPoints2 = 0;

  var totalSalesUtama = 0; 
  $('.jssalesmodal').each(function(){var inputVal = $(this).val();if ($.isNumeric(inputVal)) { totalSalesUtama += parseFloat(inputVal); }}); $('#pricemodal').val(totalSalesUtama);

  var totalSales = 0;
    $('.salesmodal').each(function(){
      var inputVal = $(this).val();
        if ($.isNumeric(inputVal)) {
          totalSales += parseFloat(inputVal);
        }
    });
    $('#pricemodal2').val(totalSales);

  var idRoute = $('#id_routesmodal option:selected').val();
    //alert(idRoute);
    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPriceAllowance');?>",
      dataType: "html",
      data: {
      routes:idRoute
      },
      success:function(data){
        $('#allowancmodal').val(data);
        },
    });

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPriceUnit');?>",
      dataType: "html",
      data: {
        routes:idRoute
      },
      success:function(data){
        var qty = parseInt($('#loadqt').val());
        var harga = (data);     
        var total = qty * harga;
        $("#pricemodal").val(total);
      },
    });

  $('#formRouteModal').on('change', '.id_routesmodal',function() {
    var id = $(this).attr('id');
    var isi = $(this).val();
    var pecah = id.split("_");
    var totalPoints = 0;
    var unit = $('#jsqtymodal_'+ pecah['2'] +'').val();

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPricebyRoutes');?>",
      data: {
        route:isi,
        sett:'ujs'
      },
      success:function(dat){
        if(pecah['2'] == 0){ $('#allowancemodal').val(dat); }
      },
    });

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPricebyRoutes');?>",
      data: {
        route:isi,
        sett:'unit'
      },
      success:function(data){
        var totaljssales = parseInt(unit) * parseInt(data);
        if(pecah['2'] == 0){
          $('#priceunitmodal').val(data);
        }
        $('#jsunitmodal_'+ pecah['2'] +'').val(data);
        $('#jssalesmodal_'+ pecah['2'] +'').val(totaljssales);

        $('.jssalesmodal').each(function(){
          var inputVal = $(this).val();
          if ($.isNumeric(inputVal)) {
            totalPoints += parseFloat(inputVal);
          }
        });

        $('#pricemodal').val(totalPoints);
      },
    });
  });

  $('#formRouteModal').on('change', '.id_citiesomodal',function() {
    var id = $(this).attr('id');
    var pecah = id.split("_");
    var origin=$('#'+ id +' option:selected').val();
    var dest=$('#id_citiesdmodal_'+ pecah['2'] +' option:selected').val();

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getDestination');?>",
      dataType: "html",
      data: {
        citieso:origin
      },
      success:function(data){
        $('#id_citiesdmodal_'+ pecah['2'] +'').html(data);
        $('#id_citiesdmodal_'+ pecah['2'] +'').trigger("chosen:updated");
      },
    });

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPriceRoutes');?>",
      dataType: "html",
      data: {
        citieso:origin,
        citiesd:dest,
        sett:'option'
      },
      success:function(data){
        $('#id_routesmodal_'+ pecah['2'] +'').html(data);
        $('#id_routesmodal_'+ pecah['2'] +'').trigger("chosen:updated");
      },
    });
  });

  $('#formRouteModal').on('change', '.id_citiesdmodal',function() { 
    var id = $(this).attr('id');
    var pecah = id.split("_");
    var origin=$('#id_citiesomodal_'+ pecah['2'] +' option:selected').val();
    var dest=$('#'+ id +' option:selected').val();
    var totalPoints = 0;
    var unit = $('#jsqtymodal_'+ pecah['2'] +'').val();

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPriceRoutes');?>",
      dataType: "html",
      data: {
        citieso:origin,
        citiesd:dest,
        sett:'option'
      },
      success:function(data){
        $('#id_routesmodal_'+ pecah['2'] +'').html(data);
        $('#id_routesmodal_'+ pecah['2'] +'').trigger("chosen:updated");
      },
    });
  });
    var codee = $(".code").val();
    //alert(codee);

  $('#formRouteModal').on('change', '.loadqtymodalada',function() { 
    var id = $(this).attr('id');
    var pecah = id.split("_");
    var route = $('#id_routesmodal_'+ pecah['1'] +' option:selected').val();
    var unit = $('#'+ id +' option:selected').val();
    var jsunit = $('#jsunitmodal_'+ pecah['1'] +'').val();
    var jsqty = $('#jsqtymodal_'+ pecah['1'] +'').val();
    var totaljssales = parseInt(unit)*parseInt(jsunit);
    var totalPoints = 0;
    var totalQty = 0;
    var rute = $('#id_routesmodal_'+ pecah['1'] +'').val();
    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/delLoadUnit');?>",
      data: {
        id:codee,
        rute:rute
      },
      success:function(data){
        $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPricebyRoutes');?>",
      data: {
        route:route,
        sett:'unit'
      },
      success:function(data){
        var totaljssales = parseInt(unit) * parseInt(data);
        if(pecah['2'] == 0){
          $('#priceunitmodal').val(data);
        }
        $('#jsunitmodal_'+ pecah['2'] +'').val(data);
        $('#jssalesmodal_'+ pecah['2'] +'').val(totaljssales);
        $('.jssalesmodal').each(function(){
          var inputVal = $(this).val();
          if ($.isNumeric(inputVal)) {
            totalPoints += parseFloat(inputVal);
          }
        });

        $('#pricemodal').val(totalPoints);

        $('.jsqtymodal').each(function(){
          var inputQty = $(this).val();
          if ($.isNumeric(inputQty)) {
            totalQty += parseFloat(inputQty);
          }
        });

        $('#totalQtymodal').val(totalQty);

        var loadloop = 0;
        var tempatLoad = $('#testqtymodal');

        for (loadloop = 0; loadloop < jsqty; loadloop++){
          $('.testmodal_'+ pecah['1'] +'_'+ loadloop +'').remove();
        }

        for (loadloop = 0; loadloop < unit; loadloop++){
          if ($('#id_customers option:selected').text().match(/PARETO/g)) {
            $('#testqtymodal').append('<div class="slebewmodal"><div class="form-group testmodal_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-4"><input type="text" class="form-control" id="do[0]" placeholder="Shipping Document" name="do[0]" /></div><div class="col-sm-4"><input type="text" class="form-control" id="machineno[0]" placeholder="Description" name="machineno[0]" /></div><div    class="col-sm-4"><input type="text" class="form-control" id="color[0]" placeholder="Case" name="color[0]" /></div></div></div>');
          }  

          if (!$('#id_customers option:selected').text().match(/PARETO/g)) {
            $('#testqtymodal').append('<div class="col-sm-12 slebewmodal"><div class="form-group testmodal_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliverynomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliverynomodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="framenomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="framenomodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="machinenomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Engine Number" name="machinenomodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="colormodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Color" name="colormodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="typemodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="typemodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="irisnomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Iris Number" name="irisnomodal_'+ pecah['1'] +'['+ loadloop +']" /></div></div></div>');
          }
        }
      },      
    });

    $('#jssalesmodal_'+ pecah['1'] +'').val(totaljssales);
    $('#jsqtymodal_'+ pecah['1'] +'').val(unit);
      }
    });
    
  });


  var tempatRouteModal = $('#formRouteModal');
  var getIdModal       = document.getElementById("formRouteModal");
  var getClassModal    = getIdModal.getElementsByClassName("id_citiesomodal");
  var noRouteModal     = getClassModal.length - 1;

  $('#addRouteModal').on('click', '.routeModal',function() {
    var ini = $('#id_customers option:selected').text();
    $.ajax({
      url:'<?php echo base_url();?>order/getOrigin',
      type:'POST',
      success: function(echo){
        if (ini.match(/PARETO/g)) {
          $('<div class="uhuymodal text-left"><div class="form-group"><label class="col-sm-4 control-label">Example Pareto</label><div class="col-sm-8"><select class="form-control id_citiesomodal" id="id_citiesomodal_'+ noRouteModal +'" name="id_citiesomodal['+ noRouteModal +']"><option value=""></option>'+ echo +'</select></div></div><div class="form-group"><label class="col-sm-4 control-label">Destination</label><div class="col-sm-8"><select class="form-control id_citiesdmodal" id="id_citiesdmodal_'+ noRouteModal +'" name="id_citiesdmodal['+ noRouteModal +']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label ">Load Qty</label><div class="col-sm-8"><select class="form-control loadqtymodal" id="loadqtymodal_'+ noRouteModal +'" name="loadqtymodal['+ noRouteModal +']"><option value=""></option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option></select></div></div><div class="form-group"><label class="col-sm-4 control-label">Route (UJS)</label><div class="col-sm-8"><select class="form-control id_routesmodal" id="id_routesmodal_'+ noRouteModal +'" name="id_routesmodal['+ noRouteModal +']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label"></label><div class="col-sm-8"><input type="text" class="jsunitmodal" style="display:none" name="jsunitmodal['+ noRouteModal +']" id="jsunitmodal_'+ noRouteModal +'" ><input type="text" style="display:none" class="txtqty" name="loadqty['+ noRoute +']"><input type="text" class="jsqtymodal" style="display:none" name="jsqtymodal['+ noRouteModal +']" id="jsqtymodal_'+ noRouteModal +'"><input type="text" class="jssalesmodal" style="display:none" name="jssalesmodal['+ noRouteModal +']" id="jssalesmodal_'+ noRouteModal +'"><button class="btn btn-danger btn-xs remRoutemodal" id="remRoutemodal_'+ noRouteModal +'" type="button"><i class="fa fa-minus"></i>  Remove</button></div></div></div>').appendTo(tempatRouteModal);
        } else {
          $('<div class="uhuymodal text-left"><div class="form-group"><label class="col-sm-4 control-label">Origin</label><div class="col-sm-8"><select class="form-control id_citiesomodal" id="id_citiesomodal_'+ noRouteModal +'" name="id_citiesomodal['+ noRouteModal +']"><option value=""></option>'+ echo +'</select></div></div><div class="form-group"><label class="col-sm-4 control-label">Destination</label><div class="col-sm-8"><select class="form-control id_citiesdmodal" id="id_citiesdmodal_'+ noRouteModal +'" name="id_citiesdmodal['+ noRouteModal +']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label ">Load Qty</label><div class="col-sm-8"><select class="form-control loadqtymodal" id="loadqtymodal_'+ noRouteModal +'" name="loadqtymodal['+ noRouteModal +']"><option value=""></option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option></select></div></div><div class="form-group"><label class="col-sm-4 control-label">Route (UJS)</label><div class="col-sm-8"><select class="form-control id_routesmodal" id="id_routesmodal_'+ noRouteModal +'" name="id_routesmodal['+ noRouteModal +']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label"></label><div class="col-sm-8"><input type="text" class="jsunitmodal" name="jsunitmodal['+ noRouteModal +']" id="jsunitmodal_'+ noRouteModal +'" style="display:none"><input type="text" class="jsqtymodal" style="display:none" name="jsqtymodal['+ noRouteModal +']" id="jsqtymodal_'+ noRouteModal +'"><input type="text" class="jssalesmodal" style="display:none" name="jssalesmodal['+ noRouteModal +']" id="jssalesmodal_'+ noRouteModal +'"><button class="btn btn-danger btn-xs remRoutemodal" id="remRoutemodal_'+ noRouteModal +'" type="button"><i class="fa fa-minus"></i>  Remove</button></div></div></div>').appendTo(tempatRouteModal);
        }

        $('.id_citiesomodal').chosen();
        $('.id_citiesdmodal').chosen();
        $('.loadqtymodal').chosen();

        $('#formRouteModal').on('change', '.id_routesmodal',function() {
          var id = $(this).attr('id');
          var isi = $(this).val();
          var pecah = id.split("_");
          var totalPoints = 0;
          var unit = $('#jsqtymodal_'+ pecah['2'] +'').val();

          $.ajax({
            type:"POST",
            url: "<?php echo site_url('order/getPricebyRoutes');?>",
            data: {
              route:isi,
              sett:'ujs'
            },
            success:function(dat){
              if(pecah['2'] == 0){ $('#allowancemodal').val(dat); }
            },
          });

          $.ajax({
            type:"POST",
            url: "<?php echo site_url('order/getPricebyRoutes');?>",
            data: {
              route:isi,
              sett:'unit'
            },
            success:function(data){
              var totaljssales = parseInt(unit) * parseInt(data);
              if(pecah['2'] == 0){
                $('#priceunitmodal').val(data);
              }
              $('#jsunitmodal_'+ pecah['2'] +'').val(data);              
              $('#jssalesmodal_'+ pecah['2'] +'').val(totaljssales);
              $('.jssalesmodal').each(function(){
                var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                  totalPoints += parseFloat(inputVal);
                }
              });
              $('#pricemodal').val(totalPoints);
            },
          });
        });

        $('#formRouteModal').on('change', '.id_citiesomodal',function() {
          var id = $(this).attr('id');
          var pecah = id.split("_");
          var origin=$('#'+ id +' option:selected').val();
          var dest=$('#id_citiesdmodal_'+ pecah['2'] +' option:selected').val();

          $.ajax({
            type:"POST",
            url: "<?php echo site_url('order/getDestination');?>",
            dataType: "html",
            data: {
              citieso:origin
            },
            success:function(data){
              $('#id_citiesdmodal_'+ pecah['2'] +'').html(data);
              $('#id_citiesdmodal_'+ pecah['2'] +'').trigger("chosen:updated");
            },
          });

          $.ajax({
            type:"POST",
            url: "<?php echo site_url('order/getPriceRoutes');?>",
            dataType: "html",
            data: {
              citieso:origin,
              citiesd:dest,
              sett:'option'
            },
            success:function(data){
              $('#id_routesmodal_'+ pecah['2'] +'').html(data);
              $('#id_routesmodal_'+ pecah['2'] +'').trigger("chosen:updated");
            },
          });
        });

        $('#formRouteModal').on('change', '.id_citiesdmodal',function() { 
          var id = $(this).attr('id');
          var pecah = id.split("_");
          var origin=$('#id_citiesomodal_'+ pecah['2'] +' option:selected').val();
          var dest=$('#'+ id +' option:selected').val();
          var totalPoints = 0;
          var unit = $('#jsqtymodal_'+ pecah['2'] +'').val();

          $.ajax({
            type:"POST",
            url: "<?php echo site_url('order/getPriceRoutes');?>",
            dataType: "html",
            data: {
              citieso:origin,
              citiesd:dest,
              sett:'option'
            },
            success:function(data){
              $('#id_routesmodal_'+ pecah['2'] +'').html(data);
              $('#id_routesmodal_'+ pecah['2'] +'').trigger("chosen:updated");
            },
          });
        });

        //DUA
        $('#formRouteModal').on('change', '.loadqtymodal',function() { 
          var id = $(this).attr('id');
          var pecah = id.split("_");
          var route = $('#id_routesmodal_'+ pecah['1'] +' option:selected').val();
          var unit = $('#'+ id +' option:selected').val();
                    var unit = $('#'+ id +' option:selected').val();
          var jsunit = $('#jsunitmodal_'+ pecah['1'] +'').val();
          var jsqty = $('#jsqtymodal_'+ pecah['1'] +'').val();
          var jsqtytxt = $('.txtqty').val();
          var totaljssales = parseInt(unit)*parseInt(jsunit);
          var totalPoints = 0;
          var totalQty = 0;

          $.ajax({
            type:"POST",
            url: "<?php echo site_url('order/getPricebyRoutes');?>",
            data: {
              route:route,
              sett:'unit'
            },
            success:function(data){
              var totaljssales = parseInt(unit) * parseInt(data);
              if(pecah['2'] == 0){
                $('#priceunitmodal').val(data);
              }
              $('#jsunitmodal_'+ pecah['2'] +'').val(data);
              $('#jssalesmodal_'+ pecah['2'] +'').val(totaljssales);

              $('.jssalesmodal').each(function(){
                var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                  totalPoints += parseFloat(inputVal);
                }
              });

              $('#pricemodal').val(totalPoints);

              $('.jsqtymodal').each(function(){
                var inputQty = $(this).val();
                if ($.isNumeric(inputQty)) {
                  totalQty += parseFloat(inputQty);
                }
              });

              $('#totalQtymodal').val(totalQty);

              var loadloop = 0;
              var tempatLoad = $('#testqtymodal');

              function addAppend() {
                $('#testqtymodal').append(formAppend);
              }

              if ($('#id_customers option:selected').text().match(/TOYOTA ASTRA/g)) {
                                    for (loadloop = 0; loadloop < jsqtytxt; loadloop++){
                                        ////('test');
                                        $('.test_'+ pecah['1'] +'_'+ loadloop +'').remove();
                                        
                                    }
                                } else {
                                    for (loadloop = 0; loadloop < jsqty; loadloop++){
                                        ////('test');
                                        $('.test_'+ pecah['1'] +'_'+ loadloop +'').remove();
                                        
                                    }
                                }
                                for (loadloop = 0; loadloop < unittxt; loadloop++)
                                {
                                    if ($('#id_customers option:selected').text().match(/PARsadasdETO/g)) 
                                        {
                                        $('#testqtymodal').append('<div class="slebew"><div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-4"><input type="text" class="form-control" id="do[0]" placeholder="Shipping Document" name="do[0]" /></div><div class="col-sm-4"><input type="text" class="form-control" id="machineno[0]" placeholder="Description" name="machineno[0]" /></div><div class="col-sm-4"><input type="text" class="form-control" id="color[0]" placeholder="Case" name="color[0]" /></div></div></div>');
                                        } 
                                    else if ($('#id_customers option:selected').text().match(/AI - DAIHATSU/g)) 
                                        {
                                        $('#testqtymodal').append('<div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="frameno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="frameno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="address_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Address" name="address_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="machineno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Engine Number" name="machineno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="type_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="type_'+ pecah['1'] +'['+ loadloop +']" /></div></div>');
                                        } 
                                    else if ($('#id_customers option:selected').text().match(/ASTRA DAIHATSU MOTOR/g)) 
                                        {
                                        $('#testqtymodal').append('<div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="frameno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="frameno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="machineno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Engine Number" name="machineno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="type_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="type_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="color_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Color" name="color_'+ pecah['1'] +'['+ loadloop +']" /></div></div>');
                                        }
                                    else if ($('#id_customers option:selected').text().match(/AI - ISUZU/g)) 
                                        {
                                        $('#testqtymodal').append('<div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="frameno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="frameno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="machineno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Engine Number" name="machineno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="type_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="type_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="irisno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Iris Number" name="irisno_'+ pecah['1'] +'['+ loadloop +']" /></div></div>');
                                        } 
                                    else if ($('#id_customers option:selected').text().match(/IAMI/g)) 
                                        {
                                        $('#testqtymodal').append('<div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="frameno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="frameno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="machineno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Engine Number" name="machineno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="type_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="type_'+ pecah['1'] +'['+ loadloop +']" /></div></div>');
                                        } 
                                    else if ($('#id_customers option:selected').text().match(/VLD/g)) 
                                        {
                                        $('#testqtymodal').append('<div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="frameno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="frameno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="type_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="type_'+ pecah['1'] +'['+ loadloop +']" /></div></div>');
                                        }
                                    else if ($('#id_customers option:selected').text().match(/TOYOTA ASTRA/g)) 
                                        {
                                        $('#testqtymodal').append('<div class="slebew"><div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-4"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Shipping Document" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-4"><input type="text" class="form-control" id="custcode_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Description" name="custcode_'+ pecah['1'] +'['+ loadloop +']"/></div><div class="col-sm-4"><input type="text" class="form-control" id="case_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Case" name="case_'+ pecah['1'] +'['+ loadloop +']" /></div></div></div>');
                                        }
                                    else 
                                        {
                                            $('#testqtymodal').append('<div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-3"><select class="form-control chosen-select fleetsw" id="id_fleets_'+ pecah['1'] +'_'+ loadloop +'" name="id_fleets_'+ pecah['1'] +'['+ loadloop +']" ><option value=""></option></select></div><div class="col-sm-3"><select class="form-control chosen-select drivess" id="id_drivers_'+ pecah['1'] +'_'+ loadloop +'" name="id_drivers_'+ pecah['1'] +'['+ loadloop +']" ><option value=""></option></select></div><div class="col-sm-3"><input type="text" style="display: none" class="form-control id_ordertypes" readonly="" id="id_ordertypes_'+ pecah['1'] +'_'+ loadloop +'" name="id_ordertypes_'+ pecah['1'] +'['+ loadloop +']" /><input type="text" class="form-control" readonly="" id="textfleet_'+ pecah['1'] +'_'+ loadloop +'" /></div><div class="col-sm-3"><input type="text" class="form-control case" id="case_'+ pecah['1'] +'_'+ loadloop +'" name="id_case_'+ pecah['1'] +'['+ loadloop +']" /></div></div>');
                                                           
                                                $('.fleetsw').chosen();
                                                $('.drivess').chosen();
                                                    //var test = JSON.parse(data);
                                                    $.ajax({
                                                        type:"POST",
                                                        url: "<?php echo site_url('order/tessst');?>",
                                                        dataType: "html",
                                                        data : {
                                                            sett:'drv'
                                                        },
                                                        success:function(driver){
                                                            $('.drivess').html(driver);
                                                            $('.drivess').trigger("chosen:updated");
                                                        },

                                                    }); 
                                                    $.ajax({
                                                        type:"POST",
                                                        url: "<?php echo site_url('order/tessst');?>",
                                                        dataType: "html",
                                                        data : {
                                                        sett:'flt'
                                                        },
                                                        success:function(fleet){
                                                            $('.fleetsw').html(fleet);
                                                            $('.fleetsw').trigger("chosen:updated");

                                                        },
                                                    }); 
                                                                                           
                                        }
                                }

                                ////(totalPoints);
                               // //(harga);
                        },
                        });
                        $('#jssales_'+ pecah['1'] +'').val(totaljssales);
                        $('#jsqty_'+ pecah['1'] +'').val(unit);
                        $('.txtqty').val(unittxt);


                                
                });
            }
        });
            noRoute++;
            return false;
    });

  $('#formRouteModal').on('click', '.remRoutemodal',function() { 
        var id    = $(this).attr('id');
        var pecah = id.split("_"); 
        var totalQty = 0;
        var totalPoints = 0;
        var loadloop = 0;
        var jsqty = $('#jsqtymodal_'+ pecah['1'] +'').val();
        if (pecah['1']=='0') {
            $('#priceunitmodal').val("0");
            $('#allowancemodal').val("0");
        }
        $(this).parent().parent().parent().remove();

        for (loadloop = 0; loadloop < jsqty; loadloop++){
            $('.testmodal_'+ pecah['1'] +'_'+ loadloop +'').remove();
        }

        $('.id_routesmodal').empty();

        

        $('.jssalesmodal').each(function(){
            var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                }
        });

        $('#pricemodal').val(totalPoints);


        $('.jsqtymodal').each(function(){
            var inputQty = $(this).val();
            if ($.isNumeric(inputQty)) {
                totalQty += parseFloat(inputQty);
            }
        });

        $('#totalQtymodal').val(totalQty);

        noRouteModal--;    
        return false;
    });

  $('#p_scentsmodal').on('change', '.p_scntmodal',function() {
    var a = $(this).attr('id');
    var pecah = a.split("_");
    
    var ini = $('#p_scntmodal_'+ pecah['2'] +' option:selected').val();
    //alert(ini);
      $.ajax({
        url:'<?php echo base_url();?>customer/getCust',
        type:'POST',
        data: {
          test:ini,
          setting:'kik'
          },
        success: function(echo){
          //alert(echo);
          $('#ujsmodal_'+ pecah['2'] +'').val(echo);
            var totalPoints = 0;
              $('.nominemodal').each(function(){
                var inputVal = $(this).val();
                  if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                  }
              });

              var totalPoints2 = 0;
                $('.ujsmodal').each(function(){
                  var inputVal2 = $(this).val();
                    if ($.isNumeric(inputVal2)) {
                      totalPoints2 += parseFloat(inputVal2);
                    }
                });

              $('#allowancemodal2').val(totalPoints+totalPoints2);
        }
      });


      $.ajax({
        url:'<?php echo base_url();?>customer/getCust',
        type:'POST',
        data: {
          test:ini,
          setting:'ki'
          },
        success: function(echo){
          $('#salesmodal_'+ pecah['2'] +'').val(echo);
            var totalPoints = 0;
              $('.salesmodal').each(function(){
                var inputVal = $(this).val();
                  if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                  }
              });
            $('#pricemodal2').val(totalPoints);
        }
      });
  });

  $('#p_scentsmodal').on('click','.remScntmodals', function() { 
    var  del = $(this).parent();
    var  id = $(this).attr('id');  
    var  pecah = id.split("_");  

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/delAdditional');?>",
      data: {
        id:pecah['1']
        },
      success:function(data){
        if(data=="YES"){
          del.remove();

          var totalPoints = 0;
              $('.nominemodal').each(function(){
              var inputVal = $(this).val();
              if ($.isNumeric(inputVal)) {
              totalPoints += parseFloat(inputVal);
              }
              });

            var totalPoints2 = 0;
              $('.ujsmodal').each(function(){
              var inputVal2 = $(this).val();
              if ($.isNumeric(inputVal2)) {
              totalPoints2 += parseFloat(inputVal2);
              }
              });
              $('#allowancemodal2').val(totalPoints+totalPoints2);

            var totalSales = 0;
              $('.salesmodal').each(function(){
                var inputVal = $(this).val();
                  if ($.isNumeric(inputVal)) {
                    totalSales += parseFloat(inputVal);
                  }
              });
              $('#pricemodal2').val(totalSales);

                   i--;    
                   return false;
        }else{
          alert("can't delete the row")
        }
      },
    });
  });

  

  var scntDiv = $('#p_scentsmodal');
  //var i = $('#p_scentsmodal').length;
  var div = document.getElementById("p_scentsmodal");
  var nodelist = div.getElementsByClassName("p_scntmodal");
  var  i= nodelist.length - 1;

          $('#teesmodal').on('click', '.addScntmodal',function() {
            $.ajax({
                    url:'<?php echo base_url();?>customer/getCust',
                    type:'POST',
                    data:'setting=kiki',
                    success: function(echo){                            
                        $('<div><select class="form-control p_scntmodal chosen-select" id="p_scntmodal_'+ i +'"  name="p_scntmodal['+ i +']">'+ echo +'</select><input type="text" class="form-control ujsmodal" style="display: none" readonly="" id="ujsmodal_'+ i +'" name="ujsmodal['+ i +']" /><input type="text" class="form-control salesmodal" style="display: none" readonly="" id="salesmodal_'+ i +'" name="salesmodal['+ i +']" /><a href="#" class="remScntmodal" id="remScntmodal">Remove</a></div>').appendTo(scntDiv);
                        
                        $('#p_scntmodal_'+ i +'').change(function(){
                          var ini = $('#p_scntmodal_'+ i +' option:selected').val();
                            $.ajax({
                              url:'<?php echo base_url();?>customer/getCust',
                              type:'POST',
                              data: {
                                    test:ini,
                                    setting:'kik'
                                    },
                              success: function(echo){
                                $('#ujsmodal_'+ i +'').val(echo);
                                  var totalPoints = 0;
                                    $('.nominemodal').each(function(){
                                      var inputVal = $(this).val();
                                        if ($.isNumeric(inputVal)) {
                                          totalPoints += parseFloat(inputVal);
                                        }
                                    });

                                  var totalPoints2 = 0;
                                    $('.ujsmodal').each(function(){
                                      var inputVal2 = $(this).val();
                                        if ($.isNumeric(inputVal2)) {
                                          totalPoints2 += parseFloat(inputVal2);
                                        }
                                      });

                                  $('#allowancemodal2').val(totalPoints+totalPoints2);
                              }
                            });

                            $.ajax({
                              url:'<?php echo base_url();?>customer/getCust',
                              type:'POST',
                              data: {
                                    test:ini,
                                    setting:'ki'
                                    },
                              success: function(echo){
                                $('#salesmodal_'+ i +'').val(echo);
                                  var totalPoints = 0;
                                    $('.salesmodal').each(function(){
                                      var inputVal = $(this).val();
                                      if ($.isNumeric(inputVal)) {
                                        totalPoints += parseFloat(inputVal);
                                      }
                                    });
                                $('#pricemodal2').val(totalPoints);
                              }
                            });
                        });

                    }
            });
            i++;
            return false;
          });
          
          $('#p_scentsmodal').on('click','.remScntmodal', function() { 
            $(this).parent().remove();

            

            var totalPoints = 0;
              $('.nominemodal').each(function(){
              var inputVal = $(this).val();
              if ($.isNumeric(inputVal)) {
              totalPoints += parseFloat(inputVal);
              }
              });

            var totalPoints2 = 0;
              $('.ujsmodal').each(function(){
              var inputVal2 = $(this).val();
              if ($.isNumeric(inputVal2)) {
              totalPoints2 += parseFloat(inputVal2);
              }
              });
              $('#allowancemodal2').val(totalPoints+totalPoints2);

            var totalSales = 0;
              $('.salesmodal').each(function(){
                var inputVal = $(this).val();
                  if ($.isNumeric(inputVal)) {
                    totalSales += parseFloat(inputVal);
                  }
              });
              $('#pricemodal2').val(totalSales);

                   i--;    
                   return false;
          });

*/

          $('#formCostmodal').on('change', '.fleetsws',function() { 
                    var id = $(this).attr('id');
                    var pecah = id.split("_");
                    var fleet = $('#'+ id +' option:selected').val();
                    var origin = $('#id_citiesomodal option:selected').val();
                    var dest = $('#id_citiesdmodal option:selected').val();
                    var cust = $('#id_customeres').val();
                    //alert(id);
                    alert(fleet);

                    $.ajax({
                      type:"POST",
                      url: "<?php echo site_url('order/getFleetType');?>",
                      dataType: "html",
                      data: {
                          id:fleet,
                          sett:'id'
                          },
                      success:function(data){
                        $('#id_ordertypesmodal_'+ pecah['2'] +'').val(data);
                        //alert(origin);
                        //alert(dest);
                        //alert(cust);
                        //alert(data);
                          $.ajax({
                          type:"POST",
                          url: "<?php echo site_url('order/tessst');?>",
                          dataType: "html",
                          data : {
                            origin:origin,
                            dest:dest,
                            cust:cust,
                            moda:data,
                            sett:'ujs'
                          },
                          success:function(ujs){
                          //alert(ujs);
                          //alert(dest);
                          //alert(cust);
                          //alert(data);
                          $('#ujs_'+ pecah['2'] +'').val(ujs);
                          var totalPoints = 0;
                          $('.ujssum').each(function(){
                            var inputVal = $(this).val();
                              if ($.isNumeric(inputVal)) {
                                totalPoints += parseFloat(inputVal);
                              }
                          });
                          $('#allowancmodal').val(totalPoints);

                          //$('.fleetsws').trigger("chosen:updated");
                          //alert(j);
                          },
                          });

                          $.ajax({
                          type:"POST",
                          url: "<?php echo site_url('order/tessst');?>",
                          dataType: "html",
                          data : {
                            origin:origin,
                            dest:dest,
                            cust:cust,
                            moda:data,
                          sett:'unp'
                          },
                          success:function(unp){
                            //alert(unp);
                          $('#unitprice2_'+ pecah['2'] +'').val(unp);
                          
                          //$('.fleetsws').trigger("chosen:updated");
                          //alert(j);


                          },
                          }); 

                            //(data);
                        },
                        });

                      $.ajax({
                          type:"POST",
                          url: "<?php echo site_url('order/getFleetType');?>",
                          dataType: "html",
                          data: {
                              id:fleet,
                              sett:'nama'
                              },
                          success:function(data){
                              $('#textfleetmodal_'+ pecah['2'] +'').val(data);
                              //(data);
                      },
                      });
                    //alert(id);


                    
                });



          var tempatCost2 = $('#formCostmodal');
          var div22 = document.getElementById("formCostmodal");
          var nodelist22 = div22.getElementsByClassName("fleetsws");
          var  j= nodelist22.length;

          $('#addCostmodal').on('click', '.Costmodal',function() {
            //alert('u');
            $('#formCostmodal').append('<div class="form-group test_'+ j +'"><div class="col-sm-3"><select class="form-control chosen-select fleetsws" id="id_fleetsmodal_'+ j +'" name="id_fleetsmodal['+ j +']" ><option value=""></option></select></div><div class="col-sm-3"><select class="form-control chosen-select drivesss" id="id_driversmodal_'+ j +'" name="id_driversmodal['+ j +']" ><option value=""></option></select></div><div class="col-sm-3"><input type="text" style="display: none" class="form-control id_ordertypesmodal" readonly="" id="id_ordertypesmodal_'+ j +'" name="id_ordertypesmodal['+ j +']" /><input type="text" class="form-control" readonly="" id="textfleetmodal_'+ j +'" /></div><div class="col-sm-3"><input type="text" class="form-control case" id="casemodal_'+ j +'" name="id_casemodal['+ j +']" /></div><div class="col-sm-3"><input type="text" class="form-control upr2" id="unitprice2_'+ j +'" name="unitpricemodal['+ j +']" /></div><div class="col-sm-3"><input type="text" class="form-control salespr" id="salespr_'+ j +'" name="salespr['+ j +']" /></div><div class="col-sm-3"><input type="text" class="form-control ujssum" id="ujs_'+ j +'" name="ujs['+ j +']" /></div></div>');
            //alert('uh');
                                                           
                                                $('.fleetsws').chosen();
                                                $('.drivesss').chosen();
                                                    //var test = JSON.parse(data);
                                                    $.ajax({
                                                        type:"POST",
                                                        url: "<?php echo site_url('order/tessst');?>",
                                                        dataType: "html",
                                                        data : {
                                                            sett:'drv'
                                                        },
                                                        success:function(driver){
                                                            $('.drivesss').html(driver);
                                                            $('.drivesss').trigger("chosen:updated");
                                                        },

                                                    }); 
                                                    $.ajax({
                                                        type:"POST",
                                                        url: "<?php echo site_url('order/tessst');?>",
                                                        dataType: "html",
                                                        data : {
                                                        sett:'flt'
                                                        },
                                                        success:function(fleet){
                                                          $('.fleetsws').html(fleet);
                                                            $('.fleetsws').trigger("chosen:updated");
                                                            //alert(j);

                                                        },
                                                    }); 

                                                    
                  j++;
              return false;
          });

          $('#formCostmodal').on('change','.case', function() { 
                var totalPoints = 0;
                    var id = $(this).attr('id');
                    var unitload = $(this).val();
                    var pecah = id.split("_");
                    var unp = $('#unitprice2_'+ pecah['1'] +'').val();
                
                    $('#salespr_'+ pecah['1'] +'').val(unitload * unp);
                    var totalPoints = 0;
                          $('.salespr').each(function(){
                            var inputVal = $(this).val();
                              if ($.isNumeric(inputVal)) {
                                totalPoints += parseFloat(inputVal);
                              }
                          });
                          $('#pricemodal').val(totalPoints);

                
            });
          
          $('#formCostmodal').on('click','.removeadd', function() { 
                 $(this).parent().remove();
                 var totalPoints = 0;
                    $('.nominemodal').each(function(){
                    var inputVal = $(this).val();
                    if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                    }
                    }); 
                  var totalPoints2 = 0;
                    $('.ujsmodal').each(function(){
                    var inputVal2 = $(this).val();
                    if ($.isNumeric(inputVal2)) {
                    totalPoints2 += parseFloat(inputVal2);
                    }
                    });
                    $('#allowancemodal2').val(totalPoints+totalPoints2);
                    j--;    
                 return false;
          });


          $('#orderdates').datepicker({format:'yyyy-mm-dd'});


				  var origin=$("#id_citio option:selected").val();
          var dest=$("#id_citid option:selected").val();
				
			      $("#id_citio").change(function(){
                var origin=$(".id_citio option:selected").val();
                var dest=$(".id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_routesmodal").html(data);
                        //alert(data);
                },
                });
            });
			
			      $("#id_citid").change(function(){
				
                var origin=$("#id_citio option:selected").val();
                var dest=$("#id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_routesmodal").html(data);
                        //alert(data);
                },
                });
            });

            $("#id_routesmodal").change(function(){
              var idRoute = $('#id_routesmodal option:selected').val();
            
              $.ajax({
                type:"POST",
                url: "<?php echo site_url('order/getPriceAllowance');?>",
                dataType: "html",
                data: {
                routes:idRoute
                },
                success:function(data){
                  $('#allowancmodal').val(data);
                  },
              });

              $.ajax({
                type:"POST",
                url: "<?php echo site_url('order/getPriceUnit');?>",
                dataType: "html",
                data: {
                  routes:idRoute
                },
                success:function(data){
                  var qty = parseInt($('#loadqt').val());
                  var harga = (data);     
                  var total = qty * harga;
                  $("#pricemodal").val(total);
                },
              });
            });

            $("#loadqt2").change(function(){
                var unit = $(this).val();
                var origin=$(".id_citio2 option:selected").val();
                var dest=$(".id_citid2 option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sett:'bbb'
                        },
                    success:function(data){
                        var perkalian = parseInt(unit)*parseInt(data);
                       $("#pricelocationmodal").val(perkalian);
                },
                });
            });

            $(".id_citio2").change(function(){
                var origin=$(".id_citio2 option:selected").val();
                var dest=$(".id_citid2 option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sett:'bbb'
                        },
                    success:function(data){
                        //$("#id_routes").html(data);
                        var unit = $("#loadqt2").val();
                        var perkalian = parseInt(unit)*parseInt(data);
                       $("#pricelocationmodal").val(perkalian);
                        //alert(data);
                },
                });
            });
            
            
            $(".id_citid2").change(function(){
                var origin=$(".id_citio2 option:selected").val();
                var dest=$(".id_citid2 option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sett:'bbb'
                        },
                    success:function(data){
                        var unit = $("#loadqt2").val();
                        var perkalian = parseInt(unit)*parseInt(data);
                       $("#pricelocationmodal").val(perkalian);
                },
                });
            });
			
           
});


</script>