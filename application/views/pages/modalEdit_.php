<div class='modal-dialog modal-lg'>
  
    <div class='modal-content animated bounceInRight'>
      <form class='form-horizontal' id='formspb' action="#" method='POST'>
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
        <h4 class='modal-title'>Detail Order</h4>
      </div>
  <!-- INFORMASI ORDER -->
      <div class='col-md-6 col-lg-6' style='margin-top: 30px;'>
        <div class="form-group">
          <label class='col-sm-4 control-label'>Order Code</label>
          <div class='col-sm-8'>
            <input type='text' class='form-control' id='code' name='code' value="<?php
          foreach ($order as $key ) {
            echo $key->code;
          }
        ?>" readonly=''/>
          </div>
        </div>
          
        <div class='form-group'>
          <label class='col-sm-4 col-xs-12 control-label'>Order Date</label>
          <div class='col-sm-8 col-xs-12'>
            <div class='input-group'>
              <input type='text' class='form-control datepicker' id='orderdate' style='width: 50%;' name='orderdate' 

               <?php
                  foreach ($order as $key ) {
                    $ke= $key->orderdate;
                    $pattern = preg_split('/\s+/', $ke);
                   

                        echo "value='".$pattern[0]."'";
                 
                  
                ?>

               readonly=''/>
            
              <div class='input-group bootstrap-timepicker timepicker' style='width: 50%;'>
                <input readonly='' id='jam' <?php 
                                            echo "value='".$pattern[1]."'";
                 
                  }?> data-autoclose='true' name='orderdatetime' type='text' class='form-control input-small'/>
                <span class='input-group-addon'>
                  <i class='glyphicon glyphicon-time'></i>
                </span>
              </div>
            </div>  
          </div>
        </div>
        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Customer Name</label>
            <div class='col-sm-8'>
              <input type='text' class='form-control' readonly='' id='id_customers' name='id_customers'
               
                <?php
                  foreach ($order as $key ) {
                    foreach ($customer as $row) {
                      if ($row['id'] == $key->id_customers) {
                        echo " value='".$row['name']."'";
                      }
                    }
                  }
                ?>
                />
          </div>
        </div>
        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Description</label>
          <div class='col-sm-8'>
            <textarea class='form-control' id='description' name='description' rows='2' readonly=''></textarea>
          </div>
        </div>
                        
        <div class='form-group'>
          <label class='col-sm-12'><h3 class='text-warning'>Fleet Information</h3></label>
        </div>
                        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Fleet (Armada)</label>
            <div class='col-sm-8'>
              <input type='text' class='form-control' readonly='' id='id_fleets' name='id_fleets' 
                 <?php
                  foreach ($order as $key ) {
                    foreach ($fleet as $row) {
                      if ($row['id'] == $key->id_fleets) {
                        echo "value='".$row['fleetplateno']."'";
                      }
                    }
                  }
                ?>
                />
          </div>
        </div>
                        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Driver (Supir)</label>
            <div class='col-sm-8'>
            <input type='text' class='form-control'  readonly='' id='id_drivers' name='id_drivers'
                <?php
                  foreach ($order as $key ) {
                    foreach ($driver as $row) {
                      if ($row['id'] == $key->id_drivers) {
                        echo "value='".$row['name']."'";
                      }
                    }
                  }
                ?>
                >
            </div>
        </div>

      </div>
                    
      <div class='col-md-6 col-lg-6' style='margin-top: 30px;'>
                        
        <div class="form-group">
			<label class="col-sm-4 control-label">Shippment No.</label>
			<div class="col-sm-8">
				<input type="text" name="test" class="form-control" readonly='' id="id_shippment" name="id_shippment" value="<?php
          foreach ($order as $key ) {
            echo $key->shipment;
          }
        ?>" >
			</div>
        </div>
		
		<div class='form-group'>
          <label class='col-sm-4 control-label'>Origin</label>
          <div class='col-sm-8'>
            <input type='text' class="form-control id_citio" readonly='' id="id_citio" name="id_citio"
                <?php
                  foreach ($order as $key ) {
                    foreach ($cities as $row) {
                      if ($row['id'] == $key->id_citieso) {
                        echo "value='".$row['name']."'";
                      }
                    }
                  }
                ?>
              >
              <input type='text' class="form-control citio" style="display: none;" id="citio" name="citio"
                <?php
                  foreach ($order as $key ) {
                    foreach ($cities as $row) {
                      if ($row['id'] == $key->id_citieso) {
                        echo "value='".$row['id']."'";
                      }
                    }
                  }
                ?>
              >
          </div>
        </div>
                    
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Destination</label>
          <div class='col-sm-8'>
            <input type='text' class='form-control id_citid' readonly='' id='id_citid' name='id_citiesd'
              
                <?php
                  foreach ($order as $key ) {
                    foreach ($cities as $row) {
                      if ($row['id'] == $key->id_citiesd) {
                        echo "<option value='".$row['name']."'";
                      }
                    }
                  }
                ?>
            >
            <input type='text' class='form-control citid' style="display: none;" id='citid' name='itiesd'
              
                <?php
                  foreach ($order as $key ) {
                    foreach ($cities as $row) {
                      if ($row['id'] == $key->id_citiesd) {
                        echo "<option value='".$row['id']."'";
                      }
                    }
                  }
                ?>
            >
          </div>
        </div>
		
		<div class="form-group">
			<label class="col-sm-12"><h3 class="text-warning"></h3></label>
		</div>
		
        <div class="form-group">
			<label class="col-sm-4 control-label">Load Qty</label>
			<div class="col-sm-8">
				<input type='text' class="form-control loadqty" readonly='' id="loadqt" name="loadqty"
					
					<?php
					  foreach ($order as $key ) {
						  for($i=1;$i<=8;$i++){
							  if($i == $key->loadqty){
								  echo "value='".$key->loadqty."'";
							  }
						  }
            }
					?>
          >
			</div>
        </div>
	
        
                    
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Order Type</label>
            <div class='col-sm-8'>
              <input type='text' class='form-control' readonly='' id='id_ordertypes' name='id_ordertypes'
               
                <?php
                  foreach ($order as $key ) {
                    foreach ($ordertypes as $row) {
                      if ($row['id'] == $key->id_ordertypes) {
                        echo "value='".$row['name']."'";
                      }
                    }
                  }
                ?>
                >
                <input type='text' class='form-control' readonly='' style="display: none;" id='ordertypes' name='ordertypes'
               
                <?php
                  foreach ($order as $key ) {
                    foreach ($ordertypes as $row) {
                      if ($row['id'] == $key->id_ordertypes) {
                        echo "value='".$row['id']."'";
                      }
                    }
                  }
                ?>
                >
            </div>
        </div>
		
                        
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Route (UJS)</label>
            <div class='col-sm-8'>
              <input type='text' class='form-control id_routes' readonly='' id='id_route' name='id_routes'>
            </div>
        </div>
                    
        <div class='form-group'>
          <label class='col-sm-4 control-label'>Allowance (UJS)</label>
          <div class='col-sm-8'>
            <input type='text' class='form-control' readonly='' id='allowanc' name='allowance' />
          </div>
        </div>
                    
      </div>
                
      

  <div class="modal-footer">
    <table class="table table-bordered"  >
		<tr>
			<th align = "center">Frame No</th>
			<th align = "center">Machine No</th>
			<th align = "center">Color</th>
			<th align = "center">Type</th>
		</tr>
		
		<?php		
		foreach($orderload as $row){
		?>
		<tr>
			<td align = "center"><?=$row['frameno']?></td>
			<td align = "center"><?=$row['machineno']?></td>
			<td align = "center"><?=$row['color']?></td>
			<td align = "center"><?=$row['type']?></td>
		</tr>
		<?php
		}
		?>
	</table>
	<div class='form-group'>
        <div class='col-sm-12 text-right' style='padding-right: 30px;'>
          <button type='submit' class='btn btn-primary'>Save!</button>
          <button type='reset' class='btn btn-danger'>Reset</button>
        </div>
      </div>
  </div>

    </form>
  </div>  
</div>

<script>
$(document).ready(function(){

				var origin=$("#citio").val();
        var dest=$("#citid").val();
        var types=$("#ordertypes").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('orderclaim/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        type:types
                        },
                    success:function(data){
                        $("#id_route").val(data);
                        //alert(data);
                },
                });
				
			$("#id_citio").change(function(){
				alert("UHUY");
                var origin=$(".id_citio option:selected").val();
                var dest=$(".id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_route").html(data);
                        //alert(data);
                },
                });
            });
			
			 $("#id_citid").change(function(){
				alert("UHUY");
                var origin=$("#id_citio option:selected").val();
                var dest=$("#id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_route").html(data);
                        //alert(data);
                },
                });
            });
			
			$("#id_route").change(function(){
                var route=$("#id_route option:selected").val();
                //alert(route);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceAllowance');?>",
                    dataType: "html",
                    data: {
                        routes:route
                        },
                    success:function(data){
                        
						var qty = parseInt($('#loadqt').val());
						var harga = (data);		
						var total = qty * harga;
						$("#allowanc").val(total);
                       // alert(harga);
                },
                });
            });
});


</script>