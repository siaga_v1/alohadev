<div class="modal-dialog modal-lg">
  <form class="form-horizontal" id="formspb" action="<?php echo site_url('employee/saveEdit')?>" method="POST"   >
    <div class="modal-content animated bounceInRight">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            
        <h4 class="modal-title">Edit Employee</h4>
                                            
      </div>

      <div class="modal-body">   
        <div class="form-group">
          <label class="col-sm-3 control-label">ID</label>
          <div class="col-sm-8">
            <input class="form-control" name="modalid" value="<?php foreach ($employee as $key ) { echo $key['id']; }?>"  type="text" autocomplete="off" readonly />

          </div>
        </div>  

        <div class="form-group">
          <label class="col-sm-3 control-label">Nama</label>
          <div class="col-sm-8">

            <input class="form-control" name="modalname" value="<?php foreach ($employee as $key ) { echo $key['name']; }?>"  type="text" autocomplete="off" />
          </div>
        </div>  
        <div class="form-group">
          <label class="col-sm-3 control-label">Tempat Lahir</label>
          <div class="col-sm-8">

            <input class="form-control" name="modalbirthplace" value="<?php foreach ($employee as $key ) { echo $key['birthplace']; }?>" type="text" autocomplete="off" />
          </div>
        </div>  
        <div class="form-group">
          <label class="col-sm-3 control-label">Tanggal Lahir</label>
          <div class="col-sm-8">

            <input class="form-control" name="modalbirthdate" value="<?php foreach ($employee as $key ) { echo $key['birthdate']; }?>" type="text" autocomplete="off" />
          </div>
        </div> 

        <div class="form-group">
          <label class="col-sm-3 control-label">Alamat</label>
          <div class="col-sm-8">
            <textarea class="form-control" name="modaladdress" ><?php foreach ($employee as $key ) { echo $key['address']; }?></textarea>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">No Hp</label>
          <div class="col-sm-8">
            <input class="form-control" name="modalphone" value="<?php foreach ($employee as $key ) { echo $key['phone']; }?>" type="text" autocomplete="off" />
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label">Gaji Pokok</label>
          <div class="col-sm-8">
            <input class="form-control" name="modalsalary" value="<?php foreach ($employee as $key ) { echo $key['salary']; }?>" type="text" autocomplete="off" />
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Uang Makan</label>
          <div class="col-sm-8">
            <input class="form-control" name="modalfeeLunch" value="<?php foreach ($employee as $key ) { echo $key['feeLunch']; }?>" type="text" autocomplete="off" />
          </div>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <input type="submit" style="margin-bottom: 5px;" name="modalsubmit" class="btn btn-primary" value="Save">
      </div>
    </div>
  </form>
</div>
