<div class='modal-dialog modal-lg'>
	<form class='form-horizontal' id='formspb' action='<?php echo site_url('Fleet/saveEdit')?>' method='POST'>
		<div class='modal-content animated bounceInRight'>
			<div class='modal-header'>
				<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
				<h4 class='modal-title'>Edit Fleets</h4>
			</div>

			<div class='modal-body'>
				<div class="form-group">
					<label class="col-sm-3 control-label">ID</label>
					<div class="col-sm-8">
						<?php foreach ($fleets as $key){?>
							<input class="form-control" name="id" value="<?=$key->id?>" type="text" autocomplete="off" readonly />
						<?php } ?>
					</div>
				</div>  

				<div class="form-group">
					<label class="col-sm-3 control-label">No Plate</label>
					<div class="col-sm-8">
						<?php foreach ($fleets as $key){?>                    
							<input class="form-control" name="plateno"  value="<?=$key->fleetplateno?>"  type="text" autocomplete="off" />
						<?php } ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Fleet Type</label>
					<div class="col-sm-8">
						<select class="form-control" name="fleettype"  type="text" >
							<option value="">Select Type Fleet</option>
							<?php foreach ($fleets as $key){  		
								  	foreach ($fleettype as $row){ 
								  		if($key->id_fleettypes == $row['id']) {?> 		
								<option value="<?=$row['id']?>" selected><?=$row['name']?></option>
								<?php
							}else{
								?>
								<option value="<?=$row['id']?>" ><?=$row['name']?></option>

								<?php
							}
							}
							}
							?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">No Frame</label>
					<div class="col-sm-8">
						<?php foreach ($fleets as $key){?>                
							<input class="form-control" name="frameno"  value="<?=$key->noframe?>"  type="text" autocomplete="off" />
						<?php } ?>
					</div>
				</div> 

				<div class="form-group">
					<label class="col-sm-3 control-label">No Machine</label>
					<div class="col-sm-8">
						<?php foreach ($fleets as $key){?>                              
							<input class="form-control" name="engineno"   value="<?=$key->noengine?>" type="text" autocomplete="off" />
						<?php } ?>
					</div>
				</div>

			</div>

			<div class='modal-footer'>
				<button type='button' class='btn btn-danger' data-dismiss='modal'>Reset</button>
				<input type='submit' style='margin-bottom: 5px;' name='submit' class='btn btn-primary' value='Save!'>
			</div>
		</div>
	</form>
</div>