  <div class='modal-dialog modal-lg'>
    <div class='modal-content animated bounceInRight'>
      <form class='form-horizontal' id='formspb' method='POST'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
          <h4 class='modal-title'>Slip Gaji</h4>
        </div>
        <!-- INFORMASI ORDER -->

        <?php
        $as = 0;
        $totals = 0;
        $trip = 0;

        foreach ($detail as $row) {
          $as += $row->komSupir;
          if ($row->c2 == 'IJIN') {
            $izin++;
          }
          if ($row->komSupir != 0) {
            $trip++;
          }

          $a = $row->orderdate;
          $last_day_of_month = date("t", strtotime($a));
        } ?>
        <div class="modal-footer">
          <div class="col-md-6 col-lg-6 col-xs-12">

            <div class="form-group">
              <label class="col-sm-4 control-label">No Slip Gaji</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="code" name="code" value="<?= $code ?>" readonly="" />
                <input type="text" class="form-control" style="display: none" id="code" name="start" value="<?= $start ?>" readonly="" />
                <input type="text" class="form-control" style="display: none" id="code" name="end" value="<?= $end ?>" readonly="" />
                <input type="text" class="form-control" style="display: none" id="id" name="id" readonly="" value="<?= $id ?>" />
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Periode</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="date" name="date" autocomplete="off" value="<?= date('F-Y', strtotime($start)) ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Supir</label>
              <div class="col-sm-8">
                <?php
                foreach ($driver as $row) {
                  if ($row['id'] == $id) {
                    $a = $row['name'];
                  }
                } ?>
                <input type="text" class="form-control" id="supir" name="supir" autocomplete="off" value="<?= $a ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Bonus Hadir</label>
              <div class="col-sm-8">
                <input type="text" class="form-control nominal" id="bonus_hadir" name="bonus_hadir" autocomplete="off" data-type="currency" value="<?php if (isset($bonus_hadir)) {
                                                                                                                                                      echo number_format($bonus_hadir);
                                                                                                                                                    } else {
                                                                                                                                                      if ($izin <= 1) {
                                                                                                                                                        echo number_format(100000);
                                                                                                                                                      } else {
                                                                                                                                                        echo 0;
                                                                                                                                                      }
                                                                                                                                                    } ?>">
                <input type="text" class="form-control" style="display: none" id="totalkomisi" name="totalkomisi" autocomplete="off" value="<?= $as ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Bonus Ritase</label>
              <div class="col-sm-8">
                <input type="text" class="form-control nominal" id="bonus_ritase" name="bonus_ritase" autocomplete="off" data-type="currency" value="<?php if (isset($bonus_ritase)) {
                                                                                                                                                        echo number_format($bonus_ritase);
                                                                                                                                                      } else {
                                                                                                                                                        if ($trip >= $last_day_of_month) {
                                                                                                                                                          echo number_format(500000);
                                                                                                                                                        } else {
                                                                                                                                                          echo 0;
                                                                                                                                                        }
                                                                                                                                                      } ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Potongan Izin</label>
              <div class="col-sm-8">
                <input type="text" class="form-control nominal" id="potongan_izin" name="potongan_izin" autocomplete="off" data-type="currency" value="<?php if ($izin >= 3) {
                                                                                                                                                          echo number_format($trip * 20000);
                                                                                                                                                        } else {
                                                                                                                                                          echo 0;
                                                                                                                                                        } ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Kasbon</label>
              <div class="col-sm-8">
                <input type="text" class="form-control nominal" id="kasbon" name="kasbon" autocomplete="off" data-type="currency" value="<?= number_format($kasbons) ?>">
                Sisa Kasbon : <?php if (isset($kasbon[0]['tkasbon'])) {
                                echo number_format($kasbon[0]['tkasbon'] - $kasbon[0]['tbayar']);
                              } else {
                                echo "0";
                              } ?>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-6 col-xs-12">
            <div class="form-group">
              <label class="col-sm-12">
                <h3 align="left">Total Komisi</h3>
                <h2 align="center"><b id="totalkomkom"></b></h2>
              </label>
            </div>
          </div>


          <div class='form-group'>
            <div class='col-sm-12 text-right'>
              <a href="#" id="export-ordertruck-summary" class='btn btn-primary'>PDF</a>
              <button type='reset' class='btn btn-success'>Excel</button>
            </div>
          </div>

          <div class="form-group 1">
            <div class="col-sm-12 text-left" id="addCostmodal">
              <label class="col-sm-12">
                <h3 class="text-warning">Payroll Details</h3>
              </label>
            </div>
          </div>



          <div id="testqtymodal" style="overflow-x:auto;">
            <div>
              <table class="table" id='postsList' border='1'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>No Mobil</th>
                    <th>Tujuan</th>
                    <th>Komisi</th>
                  </tr>
                </thead>

                <tbody>
                  <?php
                  $no = 1;
                  $total = 0;
                  foreach ($detail as $key) {
                  ?>
                    <tr>
                      <td><?= $no ?></td>
                      <td width="10%">
                        <?php
                        $awal = $key->orderdate;
                        $pecah = explode(" ", $awal);
                        echo $pecah[0]; ?>
                      </td>
                      <td><?= $key->fleet ?></td>
                      <td><?= $key->c2 ?></td>
                      <td><?= number_format($key->komSupir); ?></td>
                    </tr>
                  <?php
                    $total += $key->komSupir;
                    $no++;
                  }
                  ?>
                  <tr>
                    <td colspan="4">Hasil</td>
                    <td colspan="2"> <?= number_format($total); ?>
                      <input type="text" style="display: none" name="nominal" value="<?= $total ?>">
                    </td>

                  </tr>
                </tbody>
              </table>
            </div>
          </div>


        </div>
      </form>
    </div>
  </div>

  <script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/datatables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
  <script>
    $(document).ready(function() {

      $("#date").datepicker({
        format: "dd-mm-yyyy",
        startView: "months",
        minViewMode: "months",
        autoclose: true
      });

      $('#id_customers').chosen();

      var komisi = $('#totalkomisi').val();

      var txtbonus_hadir = $('#bonus_hadir').val();
      var bonus_hadir = txtbonus_hadir.replace(/,/g, "");

      var txtbonus_ritase = $('#bonus_ritase').val();
      var bonus_ritase = txtbonus_ritase.replace(/,/g, "");

      var txtpotongan_izin = $('#potongan_izin').val();
      var potongan_izin = txtpotongan_izin.replace(/,/g, "");

      var txtpotongan_kasbon = $('#kasbon').val();
      var potongan_kasbon = txtpotongan_kasbon.replace(/,/g, "");

      var hasila = parseInt(komisi) + parseInt(bonus_hadir) + parseInt(bonus_ritase) - (parseInt(potongan_izin) + parseInt(potongan_kasbon));

      $('#totalkomkom').text("Rp." + new Intl.NumberFormat('en-ID').format(hasila) + "");

      $('.nominal').keyup(function() {
        var komisi = $('#totalkomisi').val();

        var txtbonus_hadir = $('#bonus_hadir').val();
        var bonus_hadir = txtbonus_hadir.replace(/,/g, "");

        var txtbonus_ritase = $('#bonus_ritase').val();
        var bonus_ritase = txtbonus_ritase.replace(/,/g, "");

        var txtpotongan_izin = $('#potongan_izin').val();
        var potongan_izin = txtpotongan_izin.replace(/,/g, "");

        var txtpotongan_kasbon = $('#kasbon').val();
        var potongan_kasbon = txtpotongan_kasbon.replace(/,/g, "");
        var hasil = parseInt(komisi) + parseInt(bonus_hadir) + parseInt(bonus_ritase) - (parseInt(potongan_izin) + parseInt(potongan_kasbon));

        $('#totalkomkom').text("Rp." + new Intl.NumberFormat('en-ID').format(hasil) + "");
      });

      $("#export-ordertruck-summary").click(function() {
        window.open("<?php echo base_url(); ?>driver/gajidriverkoms?" + $('#formspb').serialize());
        return false;
      });

      $("input[data-type='currency']").on({
        keyup: function() {
          formatCurrency($(this));
        },
        blur: function() {
          formatCurrency($(this), "blur");
        }
      });


      function formatNumber(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      }


      function formatCurrency(input, blur) {
        var input_val = input.val();

        if (input_val === "") {
          return;
        }

        var original_len = input_val.length;

        var caret_pos = input.prop("selectionStart");

        if (input_val.indexOf(".") >= 0) {

          var decimal_pos = input_val.indexOf(".");

          var left_side = input_val.substring(0, decimal_pos);
          var right_side = input_val.substring(decimal_pos);

          left_side = formatNumber(left_side);

          right_side = formatNumber(right_side);

          if (blur === "blur") {
            right_side += "";
          }

          right_side = right_side.substring(0, 2);

          input_val = "" + left_side + "." + right_side;

        } else {
          input_val = formatNumber(input_val);
          input_val = "" + input_val;

          if (blur === "blur") {
            input_val += "";
          }
        }

        input.val(input_val);

        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
      }


    });
  </script>