  <div class='modal-dialog modal-lg'>  
    <div class='modal-content animated bounceInRight'>
      <form class='form-horizontal' id='formspb' action="<?=base_url()?>summarySlip/invoice" method='POST'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
          <h4 class='modal-title'>Buat Invoice</h4>
        </div>
          <!-- INFORMASI ORDER -->
        <div class="modal-footer">
          <div class="col-md-6 col-lg-6 col-xs-12">

            <div class="form-group">
              <label class="col-sm-4 control-label">No Summary</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="code" name="code" value="<?=$id?>" readonly=""/>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Nomor Invoice</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="noInvoice" name="noInvoice" value="<?php foreach($invoice as $inv) { echo $inv->noinvoice; } ?>">
              </div>
            </div>
                        
            <div class="form-group">
              <label class="col-sm-4 col-xs-12 control-label">Tanggal Invoice</label>
              <div class="col-sm-8 col-xs-12">
                <div class="input-group">
                  <input type="text" class="form-control datepicker" id="dateInvoice" style="width: 50%;" name="dateInvoice" readonly="" value="<?php foreach($invoice as $inv) { echo $inv->dateInvoice; } ?>"/>
                  <div class="input-group bootstrap-timepicker timepicker" style="width: 50%;">
                    <input readonly="" name="orderdatetime" type="text" class="form-control input-small"  />
                    <span class="input-group-addon">
                      <i class="glyphicon glyphicon-time"></i>
                    </span>
                  </div>
                </div>  
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">NSFP</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="noFaktur" name="noFaktur" value="<?php foreach($invoice as $inv) { echo $inv->faktur; } ?>">
              </div>
            </div>

            <div class="form-group text-left">
              <label class="col-sm-4 control-label">Customer</label>
              <div class="col-sm-8">
                <select class="form-control chosen-select" id="id_customers" name="id_customers">
                  <option value=""></option>
                  <?php
                      foreach ($customer as $key) {
                          echo "<option value=".$key['id']." >".$key['name']."</option>";
                    }
                  ?>
                </select>
              </div>
            </div>

            <!--
                    foreach ($customer as $key) {
                      if ($inv->idcust == $key['id']) {

                      echo "<option value=".$key['id']." selected>".$key['name']."</option>";
                    }else{
                      echo "<option value=".$key['id'].">".$key['name']."</option>";
                      # code...
                    }
                    }

            <div class="form-group">
              <label class="col-sm-4 control-label">Term of Payment</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="term" name="term">
              </div>
            </div><div class="form-group">
              <label class="col-sm-4 control-label">PIC Name</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="picname" name="picname" />
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Phone No.</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="phoneto" name="phoneto" />
              </div>
            </div><div class="form-group">
              <label class="col-sm-4 control-label">Address From</label>
              <div class="col-sm-8">
                <textarea class="form-control" id="addressFrom" name="addressFrom"></textarea>
              </div>
            </div>-->

          </div>
                    
          <div class="col-md-6 col-lg-6 col-xs-12">
                        
            

            <div class="form-group">
              <label class="col-sm-4 control-label">Alamat</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="address_1" name="address_1" value="<?php foreach($invoice as $inv) { echo $inv->address_1; } ?>" />
                <br>
                <input type="text" class="form-control" id="address_2" name="address_2" value="<?php foreach($invoice as $inv) { echo $inv->address_2; } ?>" />
                <br>
                <input type="text" class="form-control" id="address_3" name="address_3" value="<?php foreach($invoice as $inv) { echo $inv->address_3; } ?>" />
              </div>
            </div>

            <div class="form-group" align="left">
              <label class="col-sm-4 control-label">Pilihan</label>
              <div class="col-sm-4">
                <div class="form-check abc-checkbox">
                  <input class="form-check-input" id="PPH" name="PPH" type="checkbox">
                  <label class="form-check-label" for="PPH">
                    PPH
                  </label>
                </div>
                <div class="form-check abc-checkbox">
                  <input class="form-check-input" id="Operasional" name="Operasional" type="checkbox">
                  <label class="form-check-label" for="Operasional">
                    Operasional
                  </label>
                </div>
                <div class="form-check abc-checkbox">
                  <input class="form-check-input" id="LOLO" name="LOLO" type="checkbox">
                  <label class="form-check-label" for="LOLO">
                    LOLO
                  </label>
                </div>
                <div class="form-check abc-checkbox">
                  <input class="form-check-input" id="DEPO" name="DEPO" type="checkbox">
                  <label class="form-check-label" for="DEPO">
                    DEPO
                  </label>
                </div>

              </div>
              <div class="col-sm-4">
                <div class="form-check abc-checkbox">
                  <input class="form-check-input" id="Parkir" name="Parkir" type="checkbox">
                  <label class="form-check-label" for="Parkir">
                    Parkir
                  </label>
                </div>
                <div class="form-check abc-checkbox">
                  <input class="form-check-input" id="Kawalan" name="Kawalan" type="checkbox">
                  <label class="form-check-label" for="Kawalan">
                    Kawalan
                  </label>
                </div>

              </div>
            </div>
            

            <div class="form-group">
              <label class="col-sm-12"><h2 class="text-warning"></h2></label>
            </div>

          </div>


          <div class='form-group'>
            <div class='col-sm-12 text-right'>
              <button type='submit' class='btn btn-primary'>Save!</button>
              <!--<a id="print" href="<?=base_url()?>summarySlip/invoice?id=<?=$id?>" class='btn btn-primary'>Save & Print</a>-->
              <button type='reset' class='btn btn-danger'>Reset</button>
            </div>
          </div>

          <div class="form-group 1">
            <div class="col-sm-12 text-left" id="addCostmodal">
              <label class="col-sm-12">
                <h3 class="text-warning">Invoice Details</h3>
              </label>
            </div>
          </div>



          <div id="testqtymodal" style="overflow-x:auto;">
            <div>
              <table class="table" id='postsList' border='1'>
                <thead>
                  <tr>
                    <th>No Bukti</th>
                    <th>Tgl Kirim</th>
                    <th>No Seal</th>
                    <th>No Kontainer</th>
                    <th>Supir</th>
                    <th>No. Polisi</th>
                    <th>Tujuan</th>
                    <th>Biaya Trucking</th>
                  </tr>
                </thead>

                <tbody>
                  <?php
                  $total = 0;
                    foreach ($detail as $key) {
                  ?>
                  <tr>
                    <td><?=$key->noVoucher?></td>
                    <td width="10%">
                      <?php 
                          $awal = $key->orderdate;
                          $pecah = explode(" ",$awal);
                          echo $pecah[0]; ?>
                    </td>
                    <td><?=$key->noshippent?></td>
                    <td><?=$key->frameno?></td>
                    <td><?=$key->driver?></td>
                    <td width="10%"><?=$key->fleet?></td>
                    <td width="20%"><?=$key->n1?> - <?=$key->c2?></td>
                    <td><?=$key->unitprice?></td>
                  </tr>
                  <?php
                    $total += $key->unitprice;

                    }
                  ?>
                  <tr>
                    <td colspan="3">Hasil</td>
                    <td></td>
                    <td colspan="5">Hasil</td>
                    <td><?=$total?>
                      <input type="text" style="display: none" name="nominal" value="<?=$total?>" >
                      <input type="text" style="display: none" name="ppn" value="<?php $ppn = 0.1 * $total;
                      echo $ppn;?>" >
                    </td>

                  </tr>
                </tbody>
              </table>
            </div>
          </div>


        </div>
      </form>
    </div>  
  </div>

    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function(){
  

            $('#dateInvoice').datepicker({format:'yyyy-mm-dd'});


  $('#id_customers').chosen();

  $("#id_customers").change(function(){
    var customer=$("#id_customers option:selected").val();
    $.ajax({
      type:"POST",
      url: "<?php echo site_url('summarySlip/getCustomers');?>",
      dataType: "json",
      data: {
        id:customer
      },
      success:function(data){
        //$("#id_routes").html(data);
        $('#address_1').val(data['address_1']);
        $('#address_2').val(data['address_2']);
        $('#address_3').val(data['address_3']);
        $('#picname').val(data['pic_name']);
        $('#phoneto').val(data['phone']);
      },
    });
  });

  $("#id_customers").change(function(){
    var customer=$("#id_customers option:selected").val();
    $.ajax({
      type:"POST",
      url: "<?php echo site_url('summarySlip/getCustomers');?>",
      dataType: "json",
      data: {
        id:customer
      },
      success:function(data){
        //$("#id_routes").html(data);
        $('#address_1').val(data['address_1']);
        $('#address_2').val(data['address_2']);
        $('#address_3').val(data['address_3']);
        $('#picname').val(data['pic_name']);
        $('#phoneto').val(data['phone']);
      },
    });
  });
			
           
});


</script>