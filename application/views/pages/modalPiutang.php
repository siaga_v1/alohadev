  <div class='modal-dialog modal-lg'>  
    <div class='modal-content animated bounceInRight'>
      <form class='form-horizontal' id='formspb' action="<?=base_url()?>summarySlip/savePiutang" method='POST'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
          <h4 class='modal-title'>Form Pembayaran Invoice</h4>
        </div>
          <!-- INFORMASI ORDER -->
        <div class="modal-footer">
          <div class="col-md-6 col-lg-6 col-xs-12">
            <div class="form-group">
              <label class="col-sm-4 col-xs-12 control-label">Invoice Date</label>
              <div class="col-sm-8 col-xs-12">
                <div class="input-group">
                  <input type="text" class="form-control" readonly="" value="<?php foreach($invoice as $inv) { echo $inv->dateinvoice; } ?>"/>
                </div>  
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Invoice Number</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="noInvoice" name="noInvoice" value="<?php foreach($invoice as $inv) { echo $inv->noinvoice; } ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">NSFP</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="noFaktur" name="noFaktur" value="<?php foreach($invoice as $inv) { echo $inv->faktur; } ?>">
              </div>
            </div>

            <div class="form-group text-left">
              <label class="col-sm-4 control-label">Customer Name</label>
              <div class="col-sm-8">
                <input type="text" class="form-control"  value="<?php foreach ($invoice as $inv) {foreach ($customer as $key) {if ($inv->id_customers == $key['id']) {echo $key['name'];}}}?>">
              </div>
            </div>

          </div>
                    
          <div class="col-md-6 col-lg-6 col-xs-12">            
            <div class="form-group">
              <label class="col-sm-4 control-label">DPP</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nominal" name="nominal" value="<?php foreach($invoice as $inv) { echo $inv->nominal; } ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">PPN</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="ppn" name="ppn" value="<?php foreach($invoice as $inv) { echo number_format($inv->ppn); } ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Tanggal Bayar</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="dateInvoice" name="paydate" readonly="" >
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Jumlah Bayar</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="jumlahbayar" name="jumlahbayar">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Selisih Bayar</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="selisih" name="selisih">
              </div>
            </div>
          </div>


          <div class='form-group'>
            <div class='col-sm-12 text-right'>
              <button type='submit' class='btn btn-primary'>Save!</button>
              <!--<a id="print" href="<?=base_url()?>summarySlip/invoice?id=<?=$id?>" class='btn btn-primary'>Save & Print</a>-->
              <button type='reset' class='btn btn-danger'>Reset</button>
            </div>
          </div>


        </div>
      </form>
    </div>  
  </div>

    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function(){
  

  $('#dateInvoice').datepicker({format:'yyyy-mm-dd'});


  $('#id_customers').chosen();

  $('#jumlahbayar').keyup(function(){


    var a = $('#nominal').val();
    var b = $('#ppn').val();
    var c = $(this).val();
    $('#selisih').val(parseInt(a)+parseInt(b)-parseInt(c));
  });

  $("#id_customers").change(function(){
    var customer=$("#id_customers option:selected").val();
    $.ajax({
      type:"POST",
      url: "<?php echo site_url('summarySlip/getCustomers');?>",
      dataType: "json",
      data: {
        id:customer
      },
      success:function(data){
        //$("#id_routes").html(data);
        $('#address_1').val(data['address_1']);
        $('#address_2').val(data['address_2']);
        $('#address_3').val(data['address_3']);
        $('#picname').val(data['pic_name']);
        $('#phoneto').val(data['phone']);
      },
    });
  });

  $("#id_customers").change(function(){
    var customer=$("#id_customers option:selected").val();
    $.ajax({
      type:"POST",
      url: "<?php echo site_url('summarySlip/getCustomers');?>",
      dataType: "json",
      data: {
        id:customer
      },
      success:function(data){
        //$("#id_routes").html(data);
        $('#address_1').val(data['address_1']);
        $('#address_2').val(data['address_2']);
        $('#address_3').val(data['address_3']);
        $('#picname').val(data['pic_name']);
        $('#phoneto').val(data['phone']);
      },
    });
  });
      
           
});


</script>