<div class="modal-dialog modal-lg">
    <form class="form-horizontal" id="formspb" action="<?php echo site_url('selfdrive/saveEdit')?>" method="POST"   >
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            
                <h4 class="modal-title">Input Self Drive</h4>
                                            
            </div>
            
            <div class="modal-body">
                <div class="form-group">
	                <label class="col-sm-3 control-label">Code</label>
	                <div class="col-sm-8">
	                    <input class="form-control" name="codes"  type="text" value="<?php foreach ($detail as $key ) { echo $key->id; } ?>" readonly="readonly" />
                    </div>
                </div>  
                                            
                <div class="form-group">
                    <label class="col-sm-3 control-label">Date</label>
                    <div class="col-sm-8">
                	    <input class="form-control datepicker" id="dates" value="<?php foreach ($detail as $key ) { echo $key->date; } ?>" autocomplete="off"  name="ecdates"  type="text" />
                    </div>
                </div>
                
                <div class="form-group" id="orderhide">
                    <label class="col-sm-3 control-label">Order Code</label>
                    <div class="col-sm-8">
                	    <input class="form-control" name="ordercodes" readonly="readonly" value="<?php foreach ($detail as $key ) { echo $key->ordercode; } ?>" id="ordercodes"  type="text" autocomplete="off" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">No Rangka</label>
                    <div class="col-sm-8">
                	    <input class="form-control" name="norang" id="norang" value="<?php foreach ($detail as $key ) { echo $key->no_frame; } ?>" type="text" autocomplete="off" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Type</label>
                    <div class="col-sm-8">
                	    <input class="form-control" name="types" id="types"  type="text" value="<?php foreach ($detail as $key ) { echo $key->type; } ?>" readonly="readonly" autocomplete="off" />
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label">Color</label>
                    <div class="col-sm-8">
                	    <input class="form-control" name="colors" id="colors"  type="text" value="<?php foreach ($detail as $key ) { echo $key->color; } ?>" readonly="readonly" autocomplete="off" />
                    </div>
                </div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Description</label>
                    <div class="col-sm-8">
                	    <textarea class="form-control" name="descriptions" id="descriptions"> <?php foreach ($detail as $key ) { echo $key->description; } ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Nominals</label>
                    <div class="col-sm-8">
                	    <input class="form-control" name="nominalss" id="nominalss" value="<?php foreach ($detail as $key ) { echo $key->nominals; } ?>"  type="number" />
                    </div>
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <input type="submit" style="margin-bottom: 5px;" name="submit" class="btn btn-primary" value="Save changes">
            </div>
            
        </div>
    </form>
</div>

 <script>
        $(document).ready(function(){


            $('#dates').datepicker({format:'yyyy-mm-dd'});

            $('#norang').typeahead({
            source:  function (query, process) {
            return $.get('<?php echo base_url()?>index.php/orderclaim/get_autocomplete', { query: query }, function (data) {
                    console.log(data);
                    data = $.parseJSON(data);
                    return process(data);
                });
            }

            });

            $("#norang").change(function(){
                    var kode = $(this).val();
                    var strArray = kode.split(" / ");
                    alert(strArray[0]);
                    $.ajax({
                            type:"POST",
                            url: "<?php echo site_url('orderclaim/deskripsi');?>",
                            dataType: "html",
                            data: {
                                setting:'unit',
                                code:strArray[0]
                                },
                            success:function(data){
                                $("#ordercodes").val(data);
                                //$("#modaledit").modal();

                               //alert(data);
                        },
                    });

                    $.ajax({
                            type:"POST",
                            url: "<?php echo site_url('orderclaim/deskripsi');?>",
                            dataType: "html",
                            data: {
                                setting:'jenis',
                                code:strArray[0]
                                },
                            success:function(data){
                                $("#types").val(data);
                                //$("#modaledit").modal();

                               //alert(data);
                        },
                    });

                   

                    $.ajax({
                            type:"POST",
                            url: "<?php echo site_url('orderclaim/deskripsi');?>",
                            dataType: "html",
                            data: {
                                setting:'color',
                                code:strArray[0]
                                },
                            success:function(data){
                                $("#colors").val(data);
                                },
                        });
                
            });
            
           
        });

    </script>