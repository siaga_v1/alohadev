<div class="modal-dialog modal-lg">
  <form class="form-horizontal" id="formspb" action="<?php echo site_url('Supplier/saveEdit')?>" method="POST"   >
    <div class="modal-content animated bounceInRight">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            
        <h4 class="modal-title">Edit Supplier</h4>
                                            
      </div>

      <div class="modal-body">     
        <div class="form-group">
          <label class="col-sm-3 control-label">ID</label>
          <div class="col-sm-8">
          
            <input class="form-control" name="id" value="<?php
          foreach ($Supplier as $key ) {
            echo $key->id;
          }
        ?>"  type="text" autocomplete="off" readonly />
          </div>
        </div> 

        <div class="form-group">
          <label class="col-sm-3 control-label">Supplier</label>
          <div class="col-sm-8">
                                                           
            <input class="form-control" name="supplier" value="<?php
          foreach ($Supplier as $key ) {
            echo $key->name;
          }
        ?>"  type="text" autocomplete="off" />
          </div>
        </div> 
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <input type="submit" style="margin-bottom: 5px;" name="submit" class="btn btn-primary" value="Save">
      </div>
    </div>
  </form>
</div>
