  <div class='modal-dialog modal-lg'>  
    <div class='modal-content animated bounceInRight'>
      <form class='form-horizontal' id='formspb' action="<?php echo site_url('order/saveEditspld')?>" method='POST'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
          <h4 class='modal-title'>Detail Order</h4>
        </div>
          <!-- INFORMASI ORDER -->
        <div class="modal-footer">
          <div class='col-md-6 col-lg-6' style='margin-top: 30px;'>
            <div class="form-group">
              <label class='col-sm-4 control-label'>Kode Order</label>
              <div class='col-sm-8'>
                <input type='text' class='form-control code' id='code' name='code' value="<?php 
                foreach ($order as $key ) { 
                  echo $key->code; 
                } 
                ?>" readonly="" />
              </div>
            </div>
              
            <div class='form-group'>
              <label class='col-sm-4 col-xs-12 control-label' id="demo">Tanggal</label>
              <div class='col-sm-8 col-xs-12'>
                <div class='input-group'>
                  <input type='text' class='form-control datepicker' id='orderdates' style='width: 50%;' name='orderdates' value="<?php 
                  foreach ($order as $key ) { 
                    $a = $key->orderdate;
                    $b = explode(' ',$a);
                    echo $b[0];
                  }
                  ?>" readonly=''/>
                
                  <div class='input-group bootstrap-timepicker timepicker' style='width: 50%;'>
                    <input readonly='' id='jam' value="<?php
                    foreach ($order as $key ) {
                      $a = $key->orderdate;
                      $b = explode(' ',$a);
                      echo $b[1];
                    }
                    ?>"  data-autoclose='true' name='orderdatetime' type='text' class='form-control input-small'/>
                    <span class='input-group-addon'>
                      <i class='glyphicon glyphicon-time'></i>
                    </span>
                  </div>

                </div>  
              </div>
            </div>

            <div class='form-group'>
              <label class='col-sm-4 control-label'>No Bukti / Voucher</label>
                <div class='col-sm-8'>
                  <input class="form-control" type="text" name="noVoucher" value="<?php
                      foreach ($order as $key ) {
                        echo $key->noVoucher;
                      }
                    ?>">
                </div>
            </div>
            
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Customer</label>
                <div class='col-sm-8'>
                  <select class='form-control' id='id_customersmodal' name='id_customersmodal'>
                    <option value=''></option>
                    <?php
                      foreach ($order as $key ) {
                        foreach ($customer as $row) {
                          if ($row['id'] == $key->id_customers) {
                            echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                          }else {
                          echo "<option value='".$row['id']."'>".$row['name']."</option>";
                        }}
                      }
                    ?>
                  </select>
              </div>
            </div>
            
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Keterangan</label>
              <div class='col-sm-8'>
                <textarea class='form-control' id='description' name='description' rows='2'><?php foreach ($order as $key ) { echo $key->description;} ?>   </textarea>
              </div>
            </div>
                            
            <div class='form-group text-left'>
              <label class='col-sm-12'><h3 class='text-warning'>Informasi Moda</h3></label>
            </div>
                            
            <div class='form-group'>
              <label class='col-sm-4 control-label'>No Armada</label>
                <div class='col-sm-8'>
                  <select class='form-control' id='id_fleets' name='id_fleets'>
                    <option value=''></option>
                     <?php
                      foreach ($order as $key ) {
                        foreach ($fleet as $row) {
                          if ($row['id'] == $key->id_fleets) {
                            echo "<option value='".$row['id']."' selected>".$row['fleetplateno']."</option>";
                          }else {
                          echo "<option value='".$row['id']."'>".$row['fleetplateno']."</option>";
                        }}
                      }
                    ?>
                  </select>
              </div>
            </div>


                            
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Supir 1</label>
                <div class='col-sm-8'>
                <select class='form-control' id='id_drivers' name='id_drivers'>
                    <option value=''></option>
                    <?php
                      foreach ($order as $key ) {
                        foreach ($driver as $row) {
                          if ($row['id'] == $key->id_drivers) {
                            echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                          }else {
                          echo "<option value='".$row['id']."'>".$row['name']."</option>";
                        }}
                      }
                    ?>
                  </select>
                </div>
            </div>

            <div class='form-group'> 
              <label class='col-sm-4 control-label'>Supir 2</label>
                <div class='col-sm-8'>
                <select class='form-control' id='id_drivers2' name='id_drivers2'>
                    <option value=''></option>
                    <?php
                      foreach ($order as $key ) {
                        foreach ($driver as $row) {
                          if ($row['id'] == $key->id_drivers2) {
                            echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                          }else {
                          echo "<option value='".$row['id']."'>".$row['name']."</option>";
                        }}
                      }
                    ?>
                  </select>
                </div>
              </div>

          </div>
                        
          <div class='col-md-6 col-lg-6' style='margin-top: 30px;'>
                            
            <!---<div class="form-group">
              <label class="col-sm-4 control-label">Shippment No.</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="id_shippment" name="id_shippment" value="<?php foreach ($order as $key ) { echo $key->shippment; }?>" >
              </div>
            </div>-->

            <div class="form-group" >
              <div class="col-sm-12" id="addRouteModal">
                <label class="col-sm-4 control-label ">
                  Rute
                </label>
                <div class="col-sm-8 text-left">
                  <button class="btn btn-danger btn-circle routeModal" id="routeModal" type="button">
                    <i class="fa fa-plus"></i>
                  </button>
                </div>
              </div>
            </div>

            <div id="formRouteModal">
              <?php
              $x=-1;
              $routesloop="beda";
              foreach($orderload as $row){
                //$x++;
                if ($routesloop != $row['id_routes']) {
                  $x++;
                  ?>
              <div class="uhuymodal text-left">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Tipe Order</label>
                  <div class="col-sm-8">
                    <select class="form-control chosen-select fleettypemodal" id="fleettypemodal_<?=$x?>" name="fleettypeomodal[<?=$x?>]">
                    <option value=''></option>
                    <?php
                        foreach ($fleettypes   as $key) {
                          if ($key['id'] == $row['id_citieso']) {
                            echo "<option value='".$key['id']."' selected>".$key['name']."</option>";
                          } else {
                            echo "<option value='".$key['id']."'>".$key['name']."</option>";
                          }
                        }
                    ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Pabrik</label>
                  <div class="col-sm-8">
                    <select class="form-control chosen-select id_citiesomodal" id="id_citiesomodal_<?=$x?>" name="id_citiesomodal[<?=$x?>]">
                    <option value=''></option>
                    <?php
                        foreach ($cities as $key) {
                          if ($key['id'] == $row['id_citieso']) {
                            echo "<option value='".$key['id']."' selected>".$key['name']."</option>";
                          } else {
                            echo "<option value='".$key['id']."'>".$key['name']."</option>";
                          }
                        }
                    ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label">Tujuan</label>
                  <div class="col-sm-8">
                    <select class="form-control chosen-select id_citiesdmodal" id="id_citiesdmodal_<?=$x?>" name="id_citiesdmodal[<?=$x?>]">
                      <option value=""></option>
                      <?php
                        foreach ($cities as $key) {
                          if ($key['id'] == $row['id_citiesd']) {
                            echo "<option value='".$key['id']."' selected>".$key['name']."</option>";
                          } else {
                            echo "<option value='".$key['id']."'>".$key['name']."</option>";
                          }
                        }
                    ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label ">Load Qty</label>
                  <div class="col-sm-8">

                    <select class="form-control chosen-select loadqtymodalada" id="loadqtymodal_<?=$x?>" name="loadqtymodal[<?=$x?>]" >
                      <?php
                      for($i=1;$i<=20;$i++){                        
                        if($i == $row['qty']){
                          echo "<option value='".$i."' selected>".$row['qty']."</option>";
                        }else{
                        echo "<option value='".$i."'>".$i."</option>";
                        }
                     }
                      
                      ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label">Route (UJS)</label>
                  <div class="col-sm-8">
                    <select class="form-control id_routesmodal" id="id_routesmodal_<?=$x?>" name="id_routesmodal[<?=$x?>]">
                      <option value=""></option>
                      <?php
                        foreach ($routes as $key) {
                          if ($row['id_routes']==$key->id) {
                          //if ($key->id == 109) {
                      //echo "<option selected>".$key->id."</option>";

                            echo "<option value='".$key->id."' selected>".$key->code." | Rp.".number_format($key->ujs)." | ".$key->n1." To ".$key->c2."</option>";
                            $tabsupir = $key->feesaving;
                            $komsupir = $key->feewash;
                          }
                        }
                    ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"></label>
                  <div class="col-sm-8">
                    <input type="text"class="form-control jsunitmodal" style="display:  none; " name="jsunitmodal[<?=$x?>]" id="jsunitmodal_<?=$x?>" value="<?=$row['unitprice']?>" readonly=""/>                  
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label"></label>
                  <div class="col-sm-8 text-left">
                    <input type="text" class="jsqtymodal" name="jsqtymodal[<?=$x?>]" style="display: none" value="<?=$row['qty']?>" id="jsqtymodal_<?=$x?>">
                    <?php 
                    foreach ($order as $key ) { 
                      if ($key->id_customers== '55' ||$key->id_customers== '54') {
                        $total=$row['unitprice']; 
                      }else{
                        $total=$row['qty']*$row['unitprice']; 

                      }

                    }
                    ?>
                    <input type="text" class="jssalesmodal" style="display: none" name="jssalesmodal[<?=$x?>]"  value="<?=$total?>" id="jssalesmodal_<?=$x?>">
                    <input type="text" class="tabsupirmodal" name="tabsupirmodal[<?=$x?>]"  value="<?=$tabsupir?>" id="tabsupirmodal_<?=$x?>">
                    <input type="text" class="komsupirmodal" name="komsupirmodal[<?=$x?>]"  value="<?=$komsupir?>" id="komsupirmodal_<?=$x?>">
                    <button class="btn btn-danger btn-xs  remRoutemodal" id="remRoutemodal_<?=$x?>" type="button"><i class="fa fa-minus"></i>  Remove</button>
                  </div>
                </div>

              </div>
              <?php }
              $routesloop = $row['id_routes'];
              
              }
              ?>
            </div>

            <div class="form-group text-left" id="teesmodal">
              <div class="col-sm-12">              
                <label class="col-sm-4 control-label">
                  Tambah Bongkaran
                </label>
              
              <div class="col-sm-8" >
                <button class="btn btn-danger btn-circle addScntmodal" id="addScntmodal" type="button">
                  <i class="fa fa-plus"></i>
                </button>
                                    
                <div>
                
                </div>
              </div>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label"></label>
              <div class="col-sm-8 text-left" >
                <div id="p_scentsmodal">
                  <?php  
                  $x=0; 
                  foreach($additional as $row){
                    if ($row['tipe'] == 2) {
                      ?>
                  <div>
                    <select class="form-control p_scntmodal chosen-select" id="p_scntmodal_<?=$x?>"  name="p_scntmodal[<?=$x?>]">
                      <?php 
                      $x++;
                      foreach ($adds as $key) {
                        if ($row['cost_id'] == $key['id']) {
                          echo "<option value='".$key['id']."' selected>".$key['name']."</option>";
                        }else {
                          echo "<option value='".$key['id']."'>".$key['name']."</option>";
                        }
                      }; 
                      ?>
                    </select>
                    <input type="text" class="form-control ujsmodal" style="display: none" readonly="" id="ujsmodal_<?=$x-1?>" name="ujsmodal[<?=$x-1?>]"  value="<?=$row['nominal']?>"  />
                    <input type="text" class="form-control salesmodal" style="display: none" readonly="" id="salesmodal_<?=$x-1?>" name="salesmodal[<?=$x-1?>]"  value="<?=$row['nominal2']?>" />
                        <input type="text" class="form-control" style="display: none" id="kod" name="kod[<?=$x-1?>]" value="<?=$row['id']?>" />
                    <a href="#" class="remScntmodals" id="remScntmodals_<?=$row['id']?>">Remove</a>
                  </div>
                    <?php
                    }
                  }
                  ?>
                </div>
              </div>
            </div>
        
            <div class="form-group">
              <label class="col-sm-12"><h3 class="text-warning"></h3></label>
            </div>
                        
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Order Type</label>
                <div class='col-sm-8'>
                  <input type="text" style="display: none" class="form-control totalkuantiti" id="totalkuantiti" placeholder="DO Number" name="totalkuantiti"/>
                  <select class='form-control' id='id_ordertypes' name='id_ordertypes'>
                    <option value=''></option>
                    <?php
                      foreach ($order as $key ) {
                        foreach ($fleettypes as $row) {
                          if ($row['id'] == $key->id_ordertypes) {
                            echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
                          }else {
                          echo "<option value='".$row['id']."'>".$row['name']."</option>";
                        }}
                      }
                    ?>
                  </select>
                </div>
            </div>
        
            
            
            <div class='form-group'>
              <label class='col-sm-4 control-label'>Total Tagihan</label>
              <div class='col-sm-8'>
                <input type="text" class="form-control pricemodal" readonly="" id="pricemodal" name="pricemodal" />
              </div>
            </div>

            <div class='form-group'>
              <label class='col-sm-4 control-label'>Tambahan Tagihan</label>
              <div class='col-sm-8'>
                <input type="text" class="form-control pricemodal2"  readonly="" id="pricemodal2" name="pricemodal2" />
              </div>
            </div>

            <div class='form-group'>
              <label class='col-sm-4 control-label'>Uang Jalan</label>
              <div class='col-sm-8'>
                <input type='text' class='form-control' readonly='' id='allowancmodal' name='allowancmodal' value="<?php
                      foreach ($order as $key ) {
                            echo number_format($key->prices);
                      }
                    ?>">
              </div>
            </div>

            <div class='form-group'>
              <label class='col-sm-4 control-label'>Biaya Lain Lain</label>
              <div class='col-sm-8'>
                <input type="text" class="form-control modalbiayall"  readonly="" id="modalbiayall" name="modalbiayall" data-type="currency" />
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Tambahan Uang Jalan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control allowancemodal2" readonly="" id="allowancemodal2" name="allowancemodal2" />
                <input type="text" class="form-control totalqtymodal" style="display: none" readonly="" id="totalQtymodal" name="totalQtymodal" />

              </div>
            </div>

                        
          </div>

          <div class="form-group  text-left" >
            <div class="col-sm-12">
              <label class="col-sm-3">
                <h3 class="text-warning">Biaya Lain Lain</h3>                       
              </label>
            </div>
          </div>

          <div id="BiayaLain">
            <div class="col-md-6 col-lg-6 col-xs-12">

              <div class="form-group">
                <label class="col-sm-4 control-label ">Biaya Operasional</label>
                <div class="col-sm-8">
                  <input type="text" style="display:none"  class="form-control" id="idakun_0" name="idakun[0]" value="14" />
                  <input type="text" style="display:none"  class="form-control" id="costakun_0" name="costakun[0]" value="31"/>
                  <input type="text" class="form-control nominess" id="biayaakun_0" name="biayaakun[0]" data-type="currency" placeholder="Operasional" value="<?php
                      foreach ($order as $key ) {
                        echo number_format($key->feeOps);
                      }
                    ?>" />
                </div> 
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label ">Biaya LOLO</label>
                <div class="col-sm-8">
                  <input type="text" style="display:none"  class="form-control" id="idakun_1" name="idakun[1]" value="14"/>
                  <input type="text" style="display:none"  class="form-control" id="costakun_1" name="costakun[1]" value="32"/>
                  <input type="text" class="form-control nominess" id="biayaakun_1" name="biayaakun[1]" data-type="currency" placeholder="LOLO" value="<?php
                      foreach ($order as $key ) {
                        echo number_format($key->feeLolo);
                      }
                    ?>"/>
                </div> 
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label ">Biaya DEPO</label>
                <div class="col-sm-8">
                  <input type="text" style="display:none"  class="form-control" id="idakun_2" name="idakun[2]" value="14"/>
                  <input type="text" style="display:none"  class="form-control" id="costakun_2" name="costakun[2]" value="33" />
                  <input type="text" class="form-control nominess" id="biayaakun_2" name="biayaakun[2]" data-type="currency" placeholder="Depo" value="<?php
                      foreach ($order as $key ) {
                        echo number_format($key->feeDepo);
                      }
                    ?>"/>
                </div> 
              </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-12">

              <div class="form-group">
                <label class="col-sm-4 control-label ">Biaya Parkir</label>
                <div class="col-sm-8">
                  <input type="text" style="display:none"  class="form-control" id="idakun_3" name="idakun[3]" value="14"/>
                  <input type="text" style="display:none"  class="form-control" id="costakun_3" name="costakun[3]" value="34"/>
                  <input type="text" class="form-control nominess" id="biayaakun_3" name="biayaakun[3]" data-type="currency" placeholder="Parkir" value="<?php
                      foreach ($order as $key ) {
                        echo number_format($key->feeParkir);
                      }
                    ?>"/>
                </div> 
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label ">Biaya Kawalan</label>
                <div class="col-sm-8">
                  <input type="text" style="display:none"  class="form-control" id="idakun_4" name="idakun[4]" value="14"/>
                  <input type="text" style="display:none"  class="form-control" id="costakun_4" name="costakun[4]" value="36"/>
                  <input type="text" class="form-control nominess" id="biayaakun_4" name="biayaakun[4]" data-type="currency" placeholder="Kawalan" value="<?php
                      foreach ($order as $key ) {
                        echo number_format($key->feeKawalan);
                      }
                    ?>"/>
                </div> 
              </div>
            </div>
          </div>

          <div class="form-group text-left" >
            <div class="col-sm-12" id="addCostmodal">
              <label class="col-sm-2">
                <h3 class="text-warning">Tambahan Uang Jalan</h3>          
              </label>

              <div class="col-sm-8">
                <button class="btn btn-danger btn-circle Costmodal" id="Costmodal" type="button">
                  <i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
          </div>

          <div id="formCostmodal">
            <?php   
              $j=0;
              foreach($additional as $row){
                if ($row['tipe'] == 3 && $row['akun_id'] != 1) {
            ?>

            <div class="form-group text-left">
              <label class="col-sm-1 control-label ">Name</label>
              <div class="col-sm-3">
                <select class="form-control col-md-4 costaddmodal chosen-select" id="costaddmodal_<?=$j?>"  name="costaddmodal[<?=$j?>]">
                  <?php 
                    $j++;
                    foreach ($addss as $key) {
                      if ($row['cost_id'] == $key['id']) {
                        echo "<option value='".$key['id']."' selected>".$key['name']."</option>";
                      }else {
                        echo "<option value='".$key['id']."'>".$key['name']."</option>";
                      }
                    }; 
                  ?>
                </select>
              </div>

              <label class="col-sm-1 control-label ">Description</label>
              <div class="col-sm-3">
                <input type="text" class="form-control descmodal" id="descmodal_<?=$j?>" name="descmodal[<?=$j-1?>]" value="<?=$row['description']?>" />
              </div>

              <label class="col-sm-1 control-label ">Nominal</label>
              <div class="col-sm-2">
                <input type="text" class="form-control nominemodal" id="nominemodal_<?=$j?>" name="nominemodal[<?=$j-1?>]" value="<?=number_format($row['nominal'])?>" />
                <input type="text" class="form-control" style="display: none" id="kode" name="kode[<?=$j-1?>]" value="<?=$row['id']?>" />

              </div> 

              <button class="btn btn-danger btn-circle removeadds" id="removeadds_<?=$row['id']?>" type="button">
                <i class="fa fa-minus"></i>
              </button>
            </div>
                <?php
                    }
                  }
                ?>
          </div>

          <div class="form-group 1">
            <div class="col-sm-12 text-left" id="addCostmodal">
              <label class="col-sm-12">
                <h3 class="text-warning">Load Details</h3>
              </label>
            </div>
          </div>
<!--
          <div id="testqtymodal">
            <?php   
              $no= -1;
              $pp= 0;
              $tampung = "beda";
              if ($key->id_customers =='54') {

                foreach($orderload as $row){
                if ($tampung != $row['id_routes']) {
                  $no++;
                  $pp= 0;


                }
                
                
                ?>

            <div class="col-sm-12 slebewmodal">
              <div class="form-group testmodal_<?=$no?>_<?=$pp?>">
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="deliverynomodal_<?=$no?>_<?=$pp?>" placeholder="Shipping Doc" name="deliverynomodal_<?=$no?>[<?=$pp?>]" value="<?=$row['noshippent']?>"/>

                  <input type="text" class="form-control" style="display: none" id="id_<?=$no?>_<?=$pp?>" placeholder="DO Number" name="id_<?=$no?>[<?=$pp?>]" value="<?=$row['id']?>"/>
                </div>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="custcodemodal_<?=$no?>_<?=$pp?>" placeholder="Cust Code" name="custcodemodal_<?=$no?>[<?=$pp?>]" value="<?=$row['custcode']?>"/>
                </div>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="casemodal_<?=$no?>_<?=$pp?>" placeholder="Case" name="casemodal_<?=$no?>[<?=$pp?>]" value="<?=$row['case']?>" />
                </div>

              </div>
            </div>
            <?php
            $pp++;
            

              
              $tampung=$row['id_routes'];
            }
            ?>



                <?php
              }else{
              foreach($orderload as $row){
                if ($tampung != $row['id_routes']) {
                  $no++;
                  $pp= 0;


                }
                
                
                ?>
            <div class="col-sm-12 slebewmodal">
              <div class="form-group testmodal_<?=$no?>_<?=$pp?>">
                <div class="col-sm-2">
                  <input type="text" class="form-control" id="deliverynomodal_<?=$no?>_<?=$pp?>" placeholder="DO Number" name="deliverynomodal_<?=$no?>[<?=$pp?>]" value="<?=$row['noshippent']?>"/>

                  <input type="text" class="form-control" style="display: none" id="id_<?=$no?>_<?=$pp?>" placeholder="DO Number" name="id_<?=$no?>[<?=$pp?>]" value="<?=$row['id']?>"/>
                </div>
                <div class="col-sm-2">
                  <input type="text" class="form-control" id="framenomodal_<?=$no?>_<?=$pp?>" placeholder="Frame Number"  name="framenomodal_<?=$no?>[<?=$pp?>]" value="<?=$row['frameno']?>"/>
                </div>
                <div class="col-sm-2">
                  <input type="text" class="form-control" id="machinenomodal_<?=$no?>_<?=$pp?>" placeholder="Engine Number" name="machinenomodal_<?=$no?>[<?=$pp?>]" value="<?=$row['machineno']?>"/>
                </div>
                <div class="col-sm-2">
                  <input type="text" class="form-control" id="colormodal_<?=$no?>_<?=$pp?>" placeholder="Color" name="colormodal_<?=$no?>[<?=$pp?>]" value="<?=$row['color']?>"/>
                </div>
                <div class="col-sm-2">
                  <input type="text" class="form-control" id="typemodal_<?=$no?>_<?=$pp?>" placeholder="Type" name="typemodal_<?=$no?>[<?=$pp?>]" value="<?=$row['type']?>"/>
                </div>
                <div class="col-sm-2">
                  <input type="text" class="form-control" id="irisnomodal_<?=$no?>_<?=$pp?>" placeholder="Iris Number" name="irisnomodal_<?=$no?>[<?=$pp?>]" value="<?=$row['irisno']?>"/>
                </div>
              </div>
            </div>
            <?php
            $pp++;
            

              
              $tampung=$row['id_routes'];
            }}
            ?>

          </div>
-->

          <div id="testqtymodal">
            <?php   
              $no= -1;
              $pp= 0;
              $tampung = "beda";
              foreach ($order as $key ) {

                  foreach($orderload as $row){
                    if ($tampung != $row['id_routes']) {
                      $no++;
                      $pp= 0;
                    }
            ?>
                <div class="col-sm-12 slebewmodal">
              <div class="form-group testmodal_<?=$no?>_<?=$pp?>">
                <div class="col-sm-6">
                  <input type="text" class="form-control" id="framenomodal_<?=$no?>_<?=$pp?>" placeholder="No Kontainer"  name="framenomodal_<?=$no?>[<?=$pp?>]" value="<?=$row['frameno']?>"/>
                </div>
                <div class="col-sm-6">
                  <input type="text" class="form-control" id="deliverynomodal_<?=$no?>_<?=$pp?>" placeholder="No Seal" name="deliverynomodal_<?=$no?>[<?=$pp?>]" value="<?=$row['noshippent']?>"/>

                  <input type="text" class="form-control" style="display: none" id="id_<?=$no?>_<?=$pp?>" placeholder="DO Number" name="id_<?=$no?>[<?=$pp?>]" value="<?=$row['id']?>"/>
                </div> 
              </div>
            </div>
                </div>
            <?php
                    $pp++;
                    $tampung=$row['id_routes'];
                  }}
 ?>
          

          </div>
          <div class='form-group'>
            <div class='col-sm-12 text-right' style='padding-right: 30px;'>
              <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </form>
    </div>  
  </div>

<script>
$(document).ready(function(){

  
  $('.id_citiesomodal').chosen();
  $('.id_citiesdmodal').chosen();
  $('.loadqtymodal').chosen();
  $('.fleettypemodal').chosen();

// $("#id_shippment").focus(function(){
//    var id = $(this).attr('id');
//    var pecah = id.split("_");
//   var origin=$('#'+ id +' option:selected').val();
//    var dest=$('#id_citiesdmodal_'+ pecah['2'] +' option:selected').val();
 //   var itu = $('#id_customersmodal').val();
//   // alert(id);
//    $.ajax({
 //     type:'POST',
//      url:'<?php echo base_url();?>order/getOrigin',
 //     dataType:'html',
//      data: {
//        cust:itu
//      },
//      success:function(data){
//        $('#'+ id +'').html(data);
//        $('#'+ id +'').trigger("chosen:updated");
 //     },
 //   });
 // });

 $("input[data-type='currency']").on({
  keyup: function() {
    formatCurrency($(this));
  },
  blur: function() { 
    formatCurrency($(this), "blur");
  }
});


 function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");

  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "" + left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += "";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}

        $('.nominess').change(function() { 
                var totalPoints = 0;
                $('.nominess').each(function(){
                  var inputVal = $(this).val();
                  var inputvaltt = parseFloat(inputVal.replace(/,/g, ''));
                    if ($.isNumeric(inputvaltt)) {
                      totalPoints += parseFloat(inputvaltt);
                    }
                });

                var asu = new Intl.NumberFormat('en-ID', { maximumSignificantDigits: 3 }).format(totalPoints);
                //alert(asu);
                $('#modalbiayall').val(asu);

            });


  var totalPoints = 0;
    $('.nominemodal').each(function(){
      var nomineM = $(this).val();
      var nomineMI = parseFloat(nomineM.replace(/,/g, ''));
        if ($.isNumeric(nomineMI)) {
          totalPoints += parseFloat(nomineMI);
        }
    });

  var totalPoints2 = 0;
    $('.ujsmodal').each(function(){
      var inputVal2 = $(this).val();
        if ($.isNumeric(inputVal2)) {
          totalPoints2 += parseFloat(inputVal2);
        }
    });
    var assuss = new Intl.NumberFormat('en-ID', { maximumSignificantDigits: 3 }).format(totalPoints+totalPoints2);
    $('#allowancemodal2').val(assuss);

    
  //var totalPoints2 = 0;

  var totalSalesUtama = 0; 
  $('.jssalesmodal').each(function(){var inputVal = $(this).val();
    if ($.isNumeric(inputVal)) { 
      totalSalesUtama += parseFloat(inputVal); 
    }
  }); 
  var assu = new Intl.NumberFormat('en-ID', { maximumSignificantDigits: 3 }).format(totalSalesUtama);
  $('#pricemodal').val(assu);

  var totalSales = 0;
    $('.salesmodal').each(function(){
      var inputVal = $(this).val();
        if ($.isNumeric(inputVal)) {
          totalSales += parseFloat(inputVal);
        }
    });
  var assus = new Intl.NumberFormat('en-ID', { maximumSignificantDigits: 3 }).format(totalSales);

    $('#pricemodal2').val(assus);


    var totalSalesss = 0;
    $('.nominess').each(function(){
      var inputVal = $(this).val();
      var inputvaltt = parseFloat(inputVal.replace(/,/g, ''));
        if ($.isNumeric(inputvaltt)) {
          totalSalesss += parseFloat(inputvaltt);
        }
    });
    var asus = new Intl.NumberFormat('en-ID', { maximumSignificantDigits: 3 }).format(totalSalesss);

    $('#modalbiayall').val(asus);

  var idRoute = $('#id_routesmodal option:selected').val();
    //alert(idRoute);
    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPriceAllowance');?>",
      dataType: "html",
      data: {
      routes:idRoute
      },
      success:function(data){
        $('#allowancmodal').val(data);
        },
    });

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPriceUnit');?>",
      dataType: "html",
      data: {
        routes:idRoute
      },
      success:function(data){
        var qty = parseInt($('#loadqt').val());
        var harga = (data);     
        var total = qty * harga;
        $("#pricemodal").val(total);
      },
    });

  $('#formRouteModal').on('change', '.id_routesmodal',function() {
    var id = $(this).attr('id');
    var isi = $(this).val();
    var pecah = id.split("_");
    var totalPoints = 0;
    var unit = $('#jsqtymodal_'+ pecah['2'] +'').val();

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPricebyRoutes');?>",
      data: {
        route:isi,
        sett:'ujs'
      },
      success:function(dat){
        if(pecah['2'] == 0){ $('#allowancemodal').val(dat); }
      },
    });

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPricebyRoutes');?>",
      data: {
        route:isi,
        sett:'unit'
      },
      success:function(data){
        if ($('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var totaljssales =parseInt(data);
              }

              if (!$('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var totaljssales = parseInt(unit) * parseInt(data);
              }
        if(pecah['2'] == 0){
          $('#priceunitmodal').val(data);
        }
        $('#jsunitmodal_'+ pecah['2'] +'').val(data);
        $('#jssalesmodal_'+ pecah['2'] +'').val(totaljssales);

        $('.jssalesmodal').each(function(){
          var inputVal = $(this).val();
          if ($.isNumeric(inputVal)) {
            totalPoints += parseFloat(inputVal);
          }
        });

        $('#pricemodal').val(totalPoints);
      },
    });
  });

  $('#formRouteModal').on('change', '.id_citiesomodal',function() {
    var id = $(this).attr('id');
    var pecah = id.split("_");
    var origin=$('#'+ id +' option:selected').val();
    var dest=$('#id_citiesdmodal_'+ pecah['2'] +' option:selected').val();

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getDestination');?>",
      dataType: "html",
      data: {
        citieso:origin
      },
      success:function(data){
        $('#id_citiesdmodal_'+ pecah['2'] +'').html(data);
        $('#id_citiesdmodal_'+ pecah['2'] +'').trigger("chosen:updated");
      },
    });

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPriceRoutes');?>",
      dataType: "html",
      data: {
        citieso:origin,
        citiesd:dest,
        sett:'option'
      },
      success:function(data){
        $('#id_routesmodal_'+ pecah['2'] +'').html(data);
        $('#id_routesmodal_'+ pecah['2'] +'').trigger("chosen:updated");
      },
    });
  });

  $('#formRouteModal').on('change', '.id_citiesdmodal',function() { 
    var id = $(this).attr('id');
    var pecah = id.split("_");
    var origin=$('#id_citiesomodal_'+ pecah['2'] +' option:selected').val();
    var dest=$('#'+ id +' option:selected').val();
    var totalPoints = 0;
    var unit = $('#jsqtymodal_'+ pecah['2'] +'').val();

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPriceRoutes');?>",
      dataType: "html",
      data: {
        citieso:origin,
        citiesd:dest,
        sett:'option'
      },
      success:function(data){
        $('#id_routesmodal_'+ pecah['2'] +'').html(data);
        $('#id_routesmodal_'+ pecah['2'] +'').trigger("chosen:updated");
      },
    });
  });
    var codee = $(".code").val();
    //alert(codee);

  $('#formRouteModal').on('change', '.loadqtymodalada',function() { 
    var id = $(this).attr('id');
    var pecah = id.split("_");
    var route = $('#id_routesmodal_'+ pecah['1'] +' option:selected').val();
    var unit = $('#'+ id +' option:selected').val();
    var txtunit = $('#'+ id +' option:selected').text();
    var jsunit = $('#jsunitmodal_'+ pecah['1'] +'').val();
    var jsqty = $('#jsqtymodal_'+ pecah['1'] +'').val();
    if ($('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var totaljssales =parseInt(jsunit);
              }

              if (!$('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                var totaljssales = parseInt(unit)*parseInt(jsunit);
                  
              }
    var totalPoints = 0;
    var totalQty = 0;
    var rute = $('#id_routesmodal_'+ pecah['1'] +'').val();
    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/delLoadUnit');?>",
      data: {
        id:codee,
        rute:rute
      },
      success:function(data){
        $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/getPricebyRoutes');?>",
      data: {
        route:route,
        sett:'unit'
      },
      success:function(data){
        if ($('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var totaljssales =parseInt(data);
              }

              if (!$('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var totaljssales = parseInt(unit) * parseInt(data);
              }
        
        $('.loadqtymodalad').val(txtunit);

        if(pecah['2'] == 0){
          $('#priceunitmodal').val(data);
        }
        $('#jsunitmodal_'+ pecah['2'] +'').val(data);
        $('#jssalesmodal_'+ pecah['2'] +'').val(totaljssales);


        $('.jssalesmodal').each(function(){
          var inputVal = $(this).val();
          if ($.isNumeric(inputVal)) {
            totalPoints += parseFloat(inputVal);
          }
        });
        $('#pricemodal').val(totalPoints);

        var totalkuant2 = 0;
          $('.loadqtymodalad').each(function(){
            var inputVl2 = $(this).val();
              if ($.isNumeric(inputVl2)) {
                totalkuant2 += parseFloat(inputVl2);
              }
          });
          $('#totalkuantiti').val(totalkuant2);

        $('.jsqtymodal').each(function(){
          var inputQty = $(this).val();
          if ($.isNumeric(inputQty)) {
            totalQty += parseFloat(inputQty);
          }
        });

        $('#totalQtymodal').val(totalQty);

        var loadloop = 0;
        var tempatLoad = $('#testqtymodal');

        for (loadloop = 0; loadloop < jsqty; loadloop++){
          $('.testmodal_'+ pecah['1'] +'_'+ loadloop +'').remove();
        }

        for (loadloop = 0; loadloop < unit; loadloop++){
          if ($('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
            $('#testqtymodal').append('<div class="slebewmodal"><div class="form-group testmodal_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-4"><input type="text" class="form-control" id="deliverynomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Shipping Document" name="deliverynomodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-4"><input type="text" class="form-control" id="custcodemodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Description" name="custcodemodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div    class="col-sm-4"><input type="text" class="form-control" id="casemodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Case" name="casemodal_'+ pecah['1'] +'['+ loadloop +']" /></div></div></div>');
          }  

          if (!$('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
            $('#testqtymodal').append('<div class="col-sm-12 slebewmodal"><div class="form-group testmodal_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliverynomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliverynomodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="framenomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="framenomodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="machinenomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Engine Number" name="machinenomodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="colormodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Color" name="colormodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="typemodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="typemodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="irisnomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Iris Number" name="irisnomodal_'+ pecah['1'] +'['+ loadloop +']" /></div></div></div>');
          }
        }
      },      
    });

    $('#jssalesmodal_'+ pecah['1'] +'').val(totaljssales);
    $('#jsqtymodal_'+ pecah['1'] +'').val(unit);
      }
    });
    
  });

  $('#formCostmodal').on('change','.nominemodal', function() { 
    var totalPoints = 0;
    $('.nominemodal').each(function(){
      var inputVal = $(this).val();
        if ($.isNumeric(inputVal)) {
          totalPoints += parseFloat(inputVal);
        }
    });

    var totalPoints2 = 0;
      $('.ujsmodal').each(function(){
        var inputVal2 = $(this).val();
          if ($.isNumeric(inputVal2)) {
            totalPoints2 += parseFloat(inputVal2);
          }
      });
      $('#allowancemodal2').val(totalPoints+totalPoints2);

    var totalSales = 0;
      $('.salesmodal').each(function(){
        var inputVal = $(this).val();
          if ($.isNumeric(inputVal)) {
            totalSales += parseFloat(inputVal);
          }
      });
    $('#pricemodal2').val(totalSales);
  });

  var tempatRouteModal = $('#formRouteModal');
  var getIdModal       = document.getElementById("formRouteModal");
  var getClassModal    = getIdModal.getElementsByClassName("id_citiesomodal");
  var noRouteModal     = getClassModal.length - 1;

//ADD ROUTE
  $('#addRouteModal').on('click', '.routeModal',function() {
    var ini = $('#id_customersmodal option:selected').text();
     var itu = $('#id_customersmodal option:selected').val();
        alert(itu);

        $.ajax({
            url:'<?php echo base_url();?>order/getOrigin',
            type:'POST',
            data: {
                    cust:itu
                        },
      success: function(echo){
        if (ini.match(/TOYOTA ASTRA/g)) {
          $('<div class="uhuymodal text-left"><div class="form-group"><label class="col-sm-4 control-label">Example Pareto</label><div class="col-sm-8"><select class="form-control id_citiesomodal" id="id_citiesomodal_'+ noRouteModal +'" name="id_citiesomodal['+ noRouteModal +']"><option value=""></option>'+ echo +'</select></div></div><div class="form-group"><label class="col-sm-4 control-label">Destination</label><div class="col-sm-8"><select class="form-control id_citiesdmodal" id="id_citiesdmodal_'+ noRouteModal +'" name="id_citiesdmodal['+ noRouteModal +']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label ">Load Qty</label><div class="col-sm-8"><select class="form-control loadqtymodal" id="loadqtymodal_'+ noRouteModal +'" name="loadqtymodal['+ noRouteModal +']"><option value=""></option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option></select></div></div><div class="form-group"><label class="col-sm-4 control-label">Route (UJS)</label><div class="col-sm-8"><select class="form-control id_routesmodal" id="id_routesmodal_'+ noRouteModal +'" name="id_routesmodal['+ noRouteModal +']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label"></label><div class="col-sm-8"><input type="text" class="jsunitmodal" style="display:none" name="jsunitmodal['+ noRouteModal +']" id="jsunitmodal_'+ noRouteModal +'" ><input type="text" class="jsqtymodal" style="display:none" name="jsqtymodal['+ noRouteModal +']" id="jsqtymodal_'+ noRouteModal +'"><input type="text" class="jssalesmodal" style="display:none" name="jssalesmodal['+ noRouteModal +']" id="jssalesmodal_'+ noRouteModal +'"><button class="btn btn-danger btn-xs remRoutemodal" id="remRoutemodal_'+ noRouteModal +'" type="button"><i class="fa fa-minus"></i>  Remove</button></div></div></div>').appendTo(tempatRouteModal);
        } else {
          $('<div class="uhuymodal text-left"><div class="form-group"><label class="col-sm-4 control-label">Origin</label><div class="col-sm-8"><select class="form-control id_citiesomodal" id="id_citiesomodal_'+ noRouteModal +'" name="id_citiesomodal['+ noRouteModal +']"><option value=""></option>'+ echo +'</select></div></div><div class="form-group"><label class="col-sm-4 control-label">Destination</label><div class="col-sm-8"><select class="form-control id_citiesdmodal" id="id_citiesdmodal_'+ noRouteModal +'" name="id_citiesdmodal['+ noRouteModal +']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label ">Load Qty</label><div class="col-sm-8"><select class="form-control loadqtymodal" id="loadqtymodal_'+ noRouteModal +'" name="loadqtymodal['+ noRouteModal +']"><option value=""></option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option></select></div></div><div class="form-group"><label class="col-sm-4 control-label">Route (UJS)</label><div class="col-sm-8"><select class="form-control id_routesmodal" id="id_routesmodal_'+ noRouteModal +'" name="id_routesmodal['+ noRouteModal +']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label"></label><div class="col-sm-8"><input type="text" class="jsunitmodal" name="jsunitmodal['+ noRouteModal +']" id="jsunitmodal_'+ noRouteModal +'" style="display:none"><input type="text" class="jsqtymodal" style="display:none" name="jsqtymodal['+ noRouteModal +']" id="jsqtymodal_'+ noRouteModal +'"><input type="text" class="jssalesmodal" style="display:none" name="jssalesmodal['+ noRouteModal +']" id="jssalesmodal_'+ noRouteModal +'"><button class="btn btn-danger btn-xs remRoutemodal" id="remRoutemodal_'+ noRouteModal +'" type="button"><i class="fa fa-minus"></i>  Remove</button></div></div></div>').appendTo(tempatRouteModal);
        }

        $('.id_citiesomodal').chosen();
        $('.id_citiesdmodal').chosen();
        $('.loadqtymodal').chosen();

        $('#formRouteModal').on('change', '.id_routesmodal',function() {
          var id = $(this).attr('id');
          var isi = $(this).val();
          var pecah = id.split("_");
          var totalPoints = 0;
              var itu = $('#id_customersmodal option:selected').val();
          var unit = $('#jsqtymodal_'+ pecah['2'] +'').val();

          $.ajax({
            type:"POST",
            url: "<?php echo site_url('order/getPricebyRoutes');?>",
            data: {
              route:isi,
              sett:'ujs'
            },
            success:function(dat){
              if(pecah['2'] == 0){ $('#allowancmodal').val(dat); }
            },
          });

          $.ajax({
            type:"POST",
            url: "<?php echo site_url('order/getPricebyRoutes');?>",
            data: {
              route:isi,
              sett:'unit'
            },
            success:function(data){
             if ($('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var totaljssales =parseInt(data);
              }

              if (!$('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var totaljssales = parseInt(unit) * parseInt(data);
              }

              if(pecah['2'] == 0){
                $('#priceunitmodal').val(data);
              }

              $('#jsunitmodal_'+ pecah['2'] +'').val(data);   
              $('#jssalesmodal_'+ pecah['2'] +'').val(totaljssales);

              $('.jssalesmodal').each(function(){
                var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                  totalPoints += parseFloat(inputVal);
                }
              });
              $('#pricemodal').val(totalPoints);
            },
          });
        });


        $('#formRouteModal').on('change', '.id_citiesomodal',function() {
          var id = $(this).attr('id');
          var pecah = id.split("_");
          var origin=$('#'+ id +' option:selected').val();
          var itu = $('#id_customersmodal option:selected').val();
          var dest=$('#id_citiesdmodal_'+ pecah['2'] +' option:selected').val();
          //alert(itu);
          $.ajax({
            type:"POST",
            url: "<?php echo site_url('order/getDestination');?>",
            dataType: "html",
            data: {
              citieso:origin,
              cust:itu
            },
            success:function(data){
              $('#id_citiesdmodal_'+ pecah['2'] +'').html(data);
              $('#id_citiesdmodal_'+ pecah['2'] +'').trigger("chosen:updated");
            },
          });

          $.ajax({
            type:"POST",
            url: "<?php echo site_url('order/getPriceRoutes');?>",
            dataType: "html",
            data: {
              citieso:origin,
              citiesd:dest,
              sett:'option'
            },
            success:function(data){
              $('#id_routesmodal_'+ pecah['2'] +'').html(data);
              $('#id_routesmodal_'+ pecah['2'] +'').trigger("chosen:updated");
            },
          });
        });

        $('#formRouteModal').on('change', '.id_citiesdmodal',function() { 
          var id = $(this).attr('id');
          var pecah = id.split("_");
          var origin=$('#id_citiesomodal_'+ pecah['2'] +' option:selected').val();
          var dest=$('#'+ id +' option:selected').val();
          var totalPoints = 0;
          var unit = $('#jsqtymodal_'+ pecah['2'] +'').val();

          $.ajax({
            type:"POST",
            url: "<?php echo site_url('order/getPriceRoutes');?>",
            dataType: "html",
            data: {
              citieso:origin,
              citiesd:dest,
              sett:'option'
            },
            success:function(data){
              $('#id_routesmodal_'+ pecah['2'] +'').html(data);
              $('#id_routesmodal_'+ pecah['2'] +'').trigger("chosen:updated");
            },
          });
        });

        //DUA
        $('#formRouteModal').on('change', '.loadqtymodal',function() { 
          var id = $(this).attr('id');
          var pecah = id.split("_");
          var route = $('#id_routesmodal_'+ pecah['1'] +' option:selected').val();
          var unit = $('#'+ id +' option:selected').val();
          var jsunit = $('#jsunitmodal_'+ pecah['1'] +'').val();
          var jsqty = $('#jsqtymodal_'+ pecah['1'] +'').val();
          if ($('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var totaljssales =parseInt(jsunit);
              }

              if (!$('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var totaljssales = parseInt(unit) * parseInt(jsunit);
              }
          var totalPoints = 0;
          var totalQty = 0;

          $.ajax({
            type:"POST",
            url: "<?php echo site_url('order/getPricebyRoutes');?>",
            data: {
              route:route,
              sett:'unit'
            },
            success:function(data){

              if ($('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var totaljssales =parseInt(data);
              }

              if (!$('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var totaljssales = parseInt(unit) * parseInt(data);
              }

              if(pecah['2'] == 0){
                $('#priceunitmodal').val(data);
              }
              $('#jsunitmodal_'+ pecah['2'] +'').val(data);
              $('#jssalesmodal_'+ pecah['2'] +'').val(totaljssales);

              $('.jssalesmodal').each(function(){
                var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                  totalPoints += parseFloat(inputVal);
                }
              });

              $('#pricemodal').val(totalPoints);

              $('.jsqtymodal').each(function(){
                var inputQty = $(this).val();
                if ($.isNumeric(inputQty)) {
                  totalQty += parseFloat(inputQty);
                }
              });

              $('#totalQtymodal').val(totalQty);

              var loadloop = 0;
              var tempatLoad = $('#testqtymodal');

              function addAppend() {
                $('#testqtymodal').append(formAppend);
              }

              for (loadloop = 0; loadloop < jsqty; loadloop++){
                  $('.testmodal_'+ pecah['1'] +'_'+ loadloop +'').remove();
                }

              for (loadloop = 0; loadloop < unit; loadloop++){
                if ($('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  $('#testqtymodal').append('<div class="slebew"><div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-4"><input type="text" class="form-control" id="deliverynomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Shipping Document" name="deliverynomodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-4"><input type="text" class="form-control" id="custcodemodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Description" name="custcodemodal_'+ pecah['1'] +'['+ loadloop +']"/></div><div class="col-sm-4"><input type="text" class="form-control" id="casemodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Case" name="casemodal_'+ pecah['1'] +'['+ loadloop +']" /></div></div></div>');
                }  

                if (!$('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  $('#testqtymodal').append('<div class="col-sm-12 slebewmodal"><div class="form-group testmodal_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliverynomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliverynomodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="framenomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="framenomodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="machinenomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Engine Number" name="machinenomodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="colormodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Color" name="colormodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="typemodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="typemodal_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="irisnomodal_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Iris Number" name="irisnomodal_'+ pecah['1'] +'['+ loadloop +']" /></div></div></div>');
                }
              }
            },
          });

          $('#jssalesmodal_'+ pecah['1'] +'').val(totaljssales);
          $('#jsqtymodal_'+ pecah['1'] +'').val(unit);
        });
      }
    });

    noRouteModal++;
    return false;

  });
//END ADD ROUTE

  $('#formRouteModal').on('click', '.remRoutemodal',function() { 
        var id    = $(this).attr('id');
        var pecah = id.split("_"); 
        var totalQty = 0;
        var totalPoints = 0;
        var loadloop = 0;
        var jsqty = $('#jsqtymodal_'+ pecah['1'] +'').val();
        var rute = $('#id_routesmodal_'+ pecah['1'] +'').val();
        var codee = $(".code").val();

        $.ajax({
          type:"POST",
          url: "<?php echo site_url('order/delLoadUnit');?>",
          data: {
            id:codee,
            rute:rute
          },
            success:function(data){ 
              alert("DELETE");
            }
          });

         if (pecah['1']=='0') {
                  $('#priceunitmodal').val("0");
                  $('#allowancemodal').val("0");
              }
              $(this).parent().parent().parent().remove();

              for (loadloop = 0; loadloop < jsqty; loadloop++){
                  $('.testmodal_'+ pecah['1'] +'_'+ loadloop +'').remove();
              }

              $('.id_routesmodal').empty();

              

              $('.jssalesmodal').each(function(){
                  var inputVal = $(this).val();
                      if ($.isNumeric(inputVal)) {
                          totalPoints += parseFloat(inputVal);
                      }
              });

              $('#pricemodal').val(totalPoints);


              $('.jsqtymodal').each(function(){
                  var inputQty = $(this).val();
                  if ($.isNumeric(inputQty)) {
                      totalQty += parseFloat(inputQty);
                  }
              });

              $('#totalQtymodal').val(totalQty);

        noRouteModal--;    
        return false;
    });

  $('#p_scentsmodal').on('change', '.p_scntmodal',function() {
    var a = $(this).attr('id');
    var pecah = a.split("_");
    
    var ini = $('#p_scntmodal_'+ pecah['2'] +' option:selected').val();
    //alert(ini);
      $.ajax({
        url:'<?php echo base_url();?>customer/getCust',
        type:'POST',
        data: {
          test:ini,
          setting:'kik'
          },
        success: function(echo){
          //alert(echo);
          $('#ujsmodal_'+ pecah['2'] +'').val(echo);
            var totalPoints = 0;
              $('.nominemodal').each(function(){
                var inputVal = $(this).val();
                  if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                  }
              });

              var totalPoints2 = 0;
                $('.ujsmodal').each(function(){
                  var inputVal2 = $(this).val();
                    if ($.isNumeric(inputVal2)) {
                      totalPoints2 += parseFloat(inputVal2);
                    }
                });

              $('#allowancemodal2').val(totalPoints+totalPoints2);
        }
      });

      $.ajax({
        url:'<?php echo base_url();?>customer/getCust',
        type:'POST',
        data: {
          test:ini,
          setting:'ki'
          },
        success: function(echo){
          $('#salesmodal_'+ pecah['2'] +'').val(echo);
            var totalPoints = 0;
              $('.salesmodal').each(function(){
                var inputVal = $(this).val();
                  if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                  }
              });
            $('#pricemodal2').val(totalPoints);
        }
      });
  });

  $('#p_scentsmodal').on('click','.remScntmodals', function() { 
    var  del = $(this).parent();
    var  id = $(this).attr('id');  
    var  pecah = id.split("_");  

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/delAdditional');?>",
      data: {
        id:pecah['1']
        },
      success:function(data){
        if(data=="YES"){
          del.remove();

          var totalPoints = 0;
              $('.nominemodal').each(function(){
              var inputVal = $(this).val();
              if ($.isNumeric(inputVal)) {
              totalPoints += parseFloat(inputVal);
              }
              });

            var totalPoints2 = 0;
              $('.ujsmodal').each(function(){
              var inputVal2 = $(this).val();
              if ($.isNumeric(inputVal2)) {
              totalPoints2 += parseFloat(inputVal2);
              }
              });
              $('#allowancemodal2').val(totalPoints+totalPoints2);

            var totalSales = 0;
              $('.salesmodal').each(function(){
                var inputVal = $(this).val();
                  if ($.isNumeric(inputVal)) {
                    totalSales += parseFloat(inputVal);
                  }
              });
              $('#pricemodal2').val(totalSales);

                   i--;    
                   return false;
        }else{
          alert("can't delete the row")
        }
      },
    });
  });

  $('#formCostmodal').on('click','.removeadds', function() { 
    var  del = $(this).parent();
    var  id = $(this).attr('id');  
    var  pecah = id.split("_");  

    $.ajax({
      type:"POST",
      url: "<?php echo site_url('order/delAdditional');?>",
      data: {
        id:pecah['1']
        },
      success:function(data){
        if(data=="YES"){
          del.remove();

          var totalPoints = 0;
            $('.nominemodal').each(function(){
              var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                  totalPoints += parseFloat(inputVal);
                }
              }); 
          
          var totalPoints2 = 0;
            $('.ujsmodal').each(function(){
              var inputVal2 = $(this).val();
                if ($.isNumeric(inputVal2)) {
                  totalPoints2 += parseFloat(inputVal2);
                }
              });
          $('#allowancemodal2').val(totalPoints+totalPoints2);
          j--;    
          return false;
        }else{
          alert("can't delete the row")
        }
      },
    });
  });

  var scntDiv = $('#p_scentsmodal');
  //var i = $('#p_scentsmodal').length;
  var div = document.getElementById("p_scentsmodal");
  var nodelist = div.getElementsByClassName("p_scntmodal");
  var  i= nodelist.length - 1;

          $('#teesmodal').on('click', '.addScntmodal',function() {
            $.ajax({
                    url:'<?php echo base_url();?>customer/getCust',
                    type:'POST',
                    data:'setting=kiki',
                    success: function(echo){                            
                        $('<div><select class="form-control p_scntmodal chosen-select" id="p_scntmodal_'+ i +'"  name="p_scntmodal['+ i +']">'+ echo +'</select><input type="text" class="form-control ujsmodal" style="display: none" readonly="" id="ujsmodal_'+ i +'" name="ujsmodal['+ i +']" /><input type="text" class="form-control salesmodal" style="display: none" readonly="" id="salesmodal_'+ i +'" name="salesmodal['+ i +']" /><a href="#" class="remScntmodal" id="remScntmodal">Remove</a></div>').appendTo(scntDiv);
                        
                        $('#p_scntmodal_'+ i +'').change(function(){
                          var ini = $('#p_scntmodal_'+ i +' option:selected').val();
                            $.ajax({
                              url:'<?php echo base_url();?>customer/getCust',
                              type:'POST',
                              data: {
                                    test:ini,
                                    setting:'kik'
                                    },
                              success: function(echo){
                                $('#ujsmodal_'+ i +'').val(echo);
                                  var totalPoints = 0;
                                    $('.nominemodal').each(function(){
                                      var inputVal = $(this).val();
                                        if ($.isNumeric(inputVal)) {
                                          totalPoints += parseFloat(inputVal);
                                        }
                                    });

                                  var totalPoints2 = 0;
                                    $('.ujsmodal').each(function(){
                                      var inputVal2 = $(this).val();
                                        if ($.isNumeric(inputVal2)) {
                                          totalPoints2 += parseFloat(inputVal2);
                                        }
                                      });

                                  $('#allowancemodal2').val(totalPoints+totalPoints2);
                              }
                            });

                            $.ajax({
                              url:'<?php echo base_url();?>customer/getCust',
                              type:'POST',
                              data: {
                                    test:ini,
                                    setting:'ki'
                                    },
                              success: function(echo){
                                $('#salesmodal_'+ i +'').val(echo);
                                  var totalPoints = 0;
                                    $('.salesmodal').each(function(){
                                      var inputVal = $(this).val();
                                      if ($.isNumeric(inputVal)) {
                                        totalPoints += parseFloat(inputVal);
                                      }
                                    });
                                $('#pricemodal2').val(totalPoints);
                              }
                            });
                        });

                    }
            });
            i++;
            return false;
          });
          
          $('#p_scentsmodal').on('click','.remScntmodal', function() { 
            $(this).parent().remove();

            

            var totalPoints = 0;
              $('.nominemodal').each(function(){
              var inputVal = $(this).val();
              if ($.isNumeric(inputVal)) {
              totalPoints += parseFloat(inputVal);
              }
              });

            var totalPoints2 = 0;
              $('.ujsmodal').each(function(){
              var inputVal2 = $(this).val();
              if ($.isNumeric(inputVal2)) {
              totalPoints2 += parseFloat(inputVal2);
              }
              });
              $('#allowancemodal2').val(totalPoints+totalPoints2);

            var totalSales = 0;
              $('.salesmodal').each(function(){
                var inputVal = $(this).val();
                  if ($.isNumeric(inputVal)) {
                    totalSales += parseFloat(inputVal);
                  }
              });
              $('#pricemodal2').val(totalSales);

                   i--;    
                   return false;
          });

          var tempatCost = $('#formCostmodal');
         
          //var j = $('#addCostmodal').length;
          var div2 = document.getElementById("formCostmodal");
          var nodelist2 = div2.getElementsByClassName("costaddmodal");
          var  j= nodelist2.length - 1;

          $('#addCostmodal').on('click', '.Costmodal',function() {
                  $.ajax({
                    url:'<?php echo base_url();?>customer/getCust',
                    type:'POST',
                    data:'setting=cccc',
                    success: function(echo){
                        //$('#p_scnt').html(echo);
                        $('<div class="form-group text-left"><label class="col-sm-1 control-label ">Name</label><div class="col-sm-3"><select class="form-control col-md-4 costaddmodal chosen-select" id="costaddmodal_'+ j +'"  name="costaddmodal['+ j +']">'+ echo +'</select></div><label class="col-sm-1 control-label ">Description</label><div class="col-sm-3"><input type="text" class="form-control descmodal" id="descmodal_'+ j +'" name="descmodal['+ j +']" /></div><label class="col-sm-1 control-label ">Nominal</label><div class="col-sm-2"><input type="text" class="form-control nominemodal" id="nominemodal_'+ j +'" name="nominemodal['+ j +']" /></div> <button class="btn btn-danger btn-circle removeadd" id="removeadd" type="button"><i class="fa fa-minus"></i></button></div>').appendTo(tempatCost);
                        }
                    });
                  j++;
              return false;
          });
          
          $('#formCostmodal').on('click','.removeadd', function() { 
                 $(this).parent().remove();
                 var totalPoints = 0;
                    $('.nominemodal').each(function(){
                    var inputVal = $(this).val();
                    if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                    }
                    }); 
                  var totalPoints2 = 0;
                    $('.ujsmodal').each(function(){
                    var inputVal2 = $(this).val();
                    if ($.isNumeric(inputVal2)) {
                    totalPoints2 += parseFloat(inputVal2);
                    }
                    });
                    $('#allowancemodal2').val(totalPoints+totalPoints2);
                    j--;    
                 return false;
          });


        $('#orderdates').datepicker({format:'yyyy-mm-dd'});


          var origin=$("#id_citio option:selected").val();
          var dest=$("#id_citid option:selected").val();
        
            $("#id_citio").change(function(){
                var origin=$(".id_citio option:selected").val();
                var dest=$(".id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_routesmodal").html(data);
                        //alert(data);
                },
                });
            });
      
            $("#id_citid").change(function(){
        
                var origin=$("#id_citio option:selected").val();
                var dest=$("#id_citid option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_routesmodal").html(data);
                        //alert(data);
                },
                });
            });

            $("#id_routesmodal").change(function(){
              var idRoute = $('#id_routesmodal option:selected').val();
            
              $.ajax({
                type:"POST",
                url: "<?php echo site_url('order/getPriceAllowance');?>",
                dataType: "html",
                data: {
                routes:idRoute
                },
                success:function(data){
                  $('#allowancmodal').val(data);
                  },
              });

              $.ajax({
                type:"POST",
                url: "<?php echo site_url('order/getPriceUnit');?>",
                dataType: "html",
                data: {
                  routes:idRoute
                },
                success:function(data){
                  var qty = parseInt($('#loadqt').val());
                  var harga = (data);  
                  if ($('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var total =  harga;
                  
              }

              if (!$('#id_customersmodal option:selected').text().match(/TOYOTA ASTRA/g)) {
                  var total = qty * harga;
                  
              }   
                  $("#pricemodal").val(total);
                },
              });
            });

            $("#loadqt2").change(function(){
                var unit = $(this).val();
                var origin=$(".id_citio2 option:selected").val();
                var dest=$(".id_citid2 option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sett:'bbb'
                        },
                    success:function(data){
                        var perkalian = parseInt(unit)*parseInt(data);
                       $("#pricelocationmodal").val(perkalian);
                },
                });
            });

            $(".id_citio2").change(function(){
                var origin=$(".id_citio2 option:selected").val();
                var dest=$(".id_citid2 option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sett:'bbb'
                        },
                    success:function(data){
                        //$("#id_routes").html(data);
                        var unit = $("#loadqt2").val();
                        var perkalian = parseInt(unit)*parseInt(data);
                       $("#pricelocationmodal").val(perkalian);
                        //alert(data);
                },
                });
            });
            
            
            $(".id_citid2").change(function(){
                var origin=$(".id_citio2 option:selected").val();
                var dest=$(".id_citid2 option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sett:'bbb'
                        },
                    success:function(data){
                        var unit = $("#loadqt2").val();
                        var perkalian = parseInt(unit)*parseInt(data);
                       $("#pricelocationmodal").val(perkalian);
                },
                });
            });
      
           
});


</script>