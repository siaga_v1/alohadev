<div class='modal-dialog ' style="width:900px;">   
  <div class="modal-content animated bounceInRight">

    <div class='modal-header'>
      <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
      <h4 class='modal-title'>Detail</h4>
    </div>

    <div class="modal-body" style="overflow-x: scroll;">
      <table class="table table-bordered" style="white-space: nowrap;">
        <thead>
          <tr>
            <th>Tgl</th>
            <th>EMKL</th>
            <th>Pabrik</th>
            <th>Tujuan</th>
            <th>No. Container</th>
            <th>Total Tagihan</th>
            <th>UJ</th>
            <th>Tabungan</th>
            <th>Komisi</th>
            <th>Tol</th>
            <th>DEPO</th>
            <th>Uang Muat</th>
            <th>Parkir</th>
            <th>Lembur</th>
            <th>Kawalan</th>
            <th>Angsur</th>
            <th>RC</th>
            <th>Laba</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($list as $row): ?>
            
          <tr>
            <td><?=date('d-m-Y',strtotime($row->orderdate))?></td>
            <td><?=$row->cname?></td>
            <td><?=$row->n1?></td>
            <td><?=$row->c2?></td>
            <td><?=$row->frameno?></td>
            <td><?=number_format($row->unitprice)?></td>
            <td><?=number_format($row->prices)?></td>
            <td><?=number_format($row->feesaving)?></td>
            <td><?=number_format($row->feewash)?></td>
            <td>Tol</td>
            <td>DEPO</td>
            <td>Uang Muat</td>
            <td>Parkir</td>
            <td>Lembur</td>
            <td>Kawalan</td>
            <td>Angsur</td>
            <td>RC</td>
            <td>Laba</td>
          </tr>
          <?php endforeach ?>

        </tbody>
      </table>
    </div>


    <div class="modal-footer">
      <button class="btn btn-danger">Export</button>
    </div>

  </div>
</div>



<script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<script>
  $(document).ready(function(){
  });


</script>