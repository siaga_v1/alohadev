<div class="modal-dialog modal-lg">
  <form class="form-horizontal" id="formspb" action="<?php echo site_url('cost/saveEditCostOffice') ?>" method="POST">
    <div class="modal-content animated bounceInRight">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

        <h4 class="modal-title">Edit Biaya Operasional</h4>

      </div>

      <div class="modal-body">

        <div class="form-group">
          <label class="col-sm-3 control-label">Kode</label>
          <div class="col-sm-8">
            <input class="form-control" name="modalcode" id="code" value="<?php foreach ($list as $row) {
                                                                            echo $row['order_id'];
                                                                          } ?>" type="text" value="" readonly="readonly" />
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Tanggal</label>
          <div class="col-sm-8">
            <input class="form-control datepicker" id="date" autocomplete="off" name="modaldate" value="<?php foreach ($list as $row) {
                                                                                                          echo $row['date'];
                                                                                                        } ?>" readonly="readonly" type="text" />
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Kategori</label>
          <div class="col-sm-8">
            <select class="form-control" name="modalkategori" id="modalkategori">
              <option></option>
              <?php
              foreach ($cost as $key) {
                foreach ($list as $row) {
                  if ($row['nominal2'] == $key['id']) {
              ?>

                    <option value="<?= $key['id'] ?>" selected> <?= $key['name'] ?></option>

                  <?php
                  } else {
                  ?>
                    <option value="<?= $key['id'] ?>"> <?= $key['name'] ?></option>

              <?php
                  }
                }
              }
              ?>
            </select>
            <input class="form-control" name="modalcost_id" id="cost_id" style="display :none;" value="<?php foreach ($list as $row) {
                                                                                                          echo $row['cost_id'];
                                                                                                        } ?>" type="text" readonly="readonly" />
            <input class="form-control" name="idchild" id="idchild" style="display :none;" value="<?php foreach ($list as $row) {
                                                                                                    echo $row['nominal2'];
                                                                                                  } ?>" type="text" readonly="readonly" />
            <input class="form-control" name="modalakun_id" id="akun_id" style="display :none;" value="<?php foreach ($list as $row) {
                                                                                                          echo $row['akun_id'];
                                                                                                        } ?>" type="text" readonly="readonly" />
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Kategori Child</label>
          <div class="col-sm-8">
            <select class="form-control" name="modalkategori_child" id="modalkategori_child">
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Keterangan</label>
          <div class="col-sm-8">
            <input class="form-control" name="modalketerangan" id="keterangan" value="<?php foreach ($list as $row) {
                                                                                        echo $row['description'];
                                                                                      } ?>" type="text" />
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Nominal</label>
          <div class="col-sm-8">
            <input class="form-control" name="modalnominal" id="nominal" value="<?php foreach ($list as $row) {
                                                                                  echo $row['nominal'];
                                                                                } ?>" data-type="currency" type="text" />
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <input type="submit" style="margin-bottom: 5px;" name="submit" class="btn btn-primary" value="Save changes">
      </div>
    </div>
  </form>
</div>
<script>
  $(document).ready(function() {

    $(".datepicker").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true
    });


    var id = $('#idchild').val();
    $.ajax({
      type: "POST",
      url: "<?php echo site_url('cost/getChild'); ?>",
      dataType: "html",
      data: {
        id: id
      },
      success: function(data) {
        $("#modalkategori_child").html(data);
        $('#modalkategori_child').trigger("chosen:updated");

      },
    });

    $("#modalkategori").change(function() {
      var id = $(this).val();
      var text = $('#modalkategori option:selected').text();
      $.ajax({
        type: "POST",
        url: "<?php echo site_url('cost/getChild'); ?>",
        dataType: "html",
        data: {
          id: id
        },
        success: function(data) {
          $("#modalkategori_child").html(data);
          $('#modalkategori_child').trigger("chosen:updated");

        },
      });
    });

  });
</script>