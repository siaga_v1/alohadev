  <div class='modal-dialog modal-lg'>
    <div class='modal-content animated bounceInRight'>
      <form class='form-horizontal' id='formspb' action="<?= base_url() ?>driver/gajiDriver" method='POST'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
          <h4 class='modal-title'>Slip Gaji</h4>
        </div>
        <!-- INFORMASI ORDER -->

        <?php
        $as = 0;
        $totals = 0;

        foreach ($detail as $row) {
          $as += $row->komSupir;
        } ?>
        <div class="modal-footer">
          <div class="col-md-6 col-lg-6 col-xs-12">

            <div class="form-group">
              <label class="col-sm-4 control-label">No Slip Gaji</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="code" name="code" value="<?php $ss = explode("/", $start);
                                                                                      echo "" . $id . "" . $ss[0] . "" . $ss[2] . ""; ?>" readonly="" />
                <input type="text" class="form-control" style="display: none" id="code" name="start" value="<?= $start ?>" readonly="" />
                <input type="text" class="form-control" style="display: none" id="code" name="end" value="<?= $end ?>" readonly="" />
                <input type="text" class="form-control" style="display: none" id="id" name="id" value="<?= $id ?>" readonly="" />
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Supir</label>
              <div class="col-sm-8">
                <?php
                foreach ($driver as $row) {
                  if ($row['id'] == $id) {
                    $a = $row['name'];
                  }
                } ?>
                <input type="text" class="form-control" id="supir" name="supir" autocomplete="off" value="<?= $a ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Bonus Hadir</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="bonus_hadir" data-type="currency" name="bonus_hadir" autocomplete="off">
                <input type="text" class="form-control" style="display: none" id="totalkomisi" name="totalkomisi" autocomplete="off" value="<?= $as ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Bonus Ritase</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="bonus_ritase" data-type="currency" name="bonus_ritase" autocomplete="off">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Potongan Izin</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="potongan_izin" data-type="currency" name="potongan_izin" autocomplete="off">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Kasbon</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="kasbon" data-type="currency" name="kasbon" autocomplete="off">
                Sisa Kasbon : <?php if (isset($kasbon[0]['prices'])) {
                                echo $kasbon[0]['code'];
                              } else {
                                echo "0";
                              } ?>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-6 col-xs-12">








            <div class="form-group">
              <label class="col-sm-12">
                <h2 class="text-warning"></h2>
              </label>
            </div>

          </div>


          <div class='form-group'>
            <div class='col-sm-12 text-right'>
              <button type='submit' class='btn btn-primary'>Save!</button>
              <!--<a id="print" href="<?= base_url() ?>summarySlip/invoice?id=<?= $id ?>" class='btn btn-primary'>Save & Print</a>-->
              <button type='reset' class='btn btn-danger'>Reset</button>
            </div>
          </div>

          <div class="form-group 1">
            <div class="col-sm-12 text-left" id="addCostmodal">
              <label class="col-sm-12">
                <h3 class="text-warning">Payroll Details</h3>
              </label>
            </div>
          </div>



          <div id="testqtymodal" style="overflow-x:auto;">
            <div>
              <table class="table" id='postsList' border='1'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>No Mobil</th>
                    <th>Tujuan</th>
                    <th>Komisi</th>
                  </tr>
                </thead>

                <tbody>
                  <?php
                  $no = 1;
                  $total = 0;
                  foreach ($detail as $key) {
                    if ($key->tipe_absensi == '1') {
                      $platno = $key->platno;
                      $tujuan = $key->tujuan;
                    } else {
                      $platno = get_absensi_type($key->tipe_absensi);
                      $tujuan = get_absensi_type($key->tipe_absensi);
                    }
                  ?>
                    <tr>
                      <td><?= $no ?></td>
                      <td width="10%"><?= $key->tanggal_absensi ?> </td>
                      <td><?= $platno ?></td>
                      <td><?= $tujuan ?></td>
                      <td><?= number_format($key->komSupir); ?></td>
                    </tr>
                  <?php
                    $total += $key->komSupir;
                    $no++;
                  }
                  ?>
                  <tr>
                    <td colspan="4">Hasil</td>
                    <td colspan="2"> <?= number_format($total); ?>
                      <input type="text" style="display: none" name="nominal" value="<?= $total ?>">
                      <input type="text" style="display: none" name="ppn" value="<?php $ppn = 0.1 * $total;
                                                                                  echo $ppn; ?>">
                    </td>

                  </tr>
                </tbody>
              </table>
            </div>
          </div>


        </div>
      </form>
    </div>
  </div>

  <script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/datatables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#dateInvoice').datepicker({
        format: 'yyyy-mm-dd'
      });

      $("input[data-type='currency']").on({
        keyup: function() {
          formatCurrency($(this));
        },
        blur: function() {
          formatCurrency($(this), "blur");
        }
      });


      function formatNumber(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      }


      function formatCurrency(input, blur) {
        var input_val = input.val();

        if (input_val === "") {
          return;
        }

        var original_len = input_val.length;

        var caret_pos = input.prop("selectionStart");

        if (input_val.indexOf(".") >= 0) {

          var decimal_pos = input_val.indexOf(".");

          var left_side = input_val.substring(0, decimal_pos);
          var right_side = input_val.substring(decimal_pos);

          left_side = formatNumber(left_side);

          right_side = formatNumber(right_side);

          if (blur === "blur") {
            right_side += "";
          }

          right_side = right_side.substring(0, 2);

          input_val = "" + left_side + "." + right_side;

        } else {
          input_val = formatNumber(input_val);
          input_val = "" + input_val;

          if (blur === "blur") {
            input_val += "";
          }
        }

        input.val(input_val);

        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
      }

    });
  </script>