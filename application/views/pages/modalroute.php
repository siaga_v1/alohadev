<div class='modal-dialog modal-lg'>
	<form class='form-horizontal' id='formspb' action='<?php echo site_url('route/saveEdit')?>' method='POST'>
		<div class='modal-content animated bounceInRight'>
			<div class='modal-header'>
				<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
				<h4 class='modal-title'>Edit Route</h4>
			</div>

			<div class='modal-body'>
				<div class='form-group'>
					<label class='col-sm-3 control-label'>Kode</label>

					<div class='col-sm-8'>
						<input class='form-control' name='code'  type='text' value="<?php
						foreach ($route as $key ) {
							echo $key->code;
						}
						?>" readonly='readonly' />
					</div>
				</div>  

				<div class='form-group'>
					<label class='col-sm-3 control-label'>Asal</label>
					<div class='col-sm-8'>
						<select class='form-control chosen-select' id='id_citieso' name='id_citieso'>
							<option value=''></option>
							<?php
							foreach ($route as $key ) {
								foreach ($cities as $row) {
									if ($row['id'] == $key->id_citieso) {
										echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
									}else {
										echo "<option value='".$row['id']."'>".$row['name']."</option>";
									}
								}
							}
							?>
						</select>
					</div>
				</div>

				<div class='form-group'>
					<label class='col-sm-3 control-label'>Tujuan</label>

					<div class='col-sm-8'>
						<select class='form-control chosen-select' id='id_citiesd' name='id_citiesd'>
							<option value=''></option>
							<?php
							foreach ($route as $key ) {
								foreach ($cities as $row) {
									if ($row['id'] == $key->id_citiesd) {
										echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
									}else {
										echo "<option value='".$row['id']."'>".$row['name']."</option>";
									}
								}
							}
							?>
						</select>
					</div>
				</div>

				<div class='form-group'>
					<label class='col-sm-3 control-label'>Customer</label>

					<div class='col-sm-8'>
						<select class='form-control chosen-select' id='id_customer' name='id_customer'>
							<option value=''></option>
							<?php
							foreach ($route as $key ) {
								foreach ($customer as $row) {
									if ($row['id'] == $key->id_customers) {
										echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
									}else {
										echo "<option value='".$row['id']."'>".$row['name']."</option>";
									}
								}
							}
							?>
						</select>
					</div>
				</div>

				<div class='form-group'>
					<label class='col-sm-3 control-label'>Tipe Moda</label>

					<div class='col-sm-8'>
						<select class='form-control chosen-select' id='type' name='type'>
							<option value=''></option>
							<?php
							foreach ($route as $key ) {
								foreach ($fleettypes as $row) {
									if ($row['id'] == $key->id_ordertypes) {
										echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
									}else {
										echo "<option value='".$row['id']."'>".$row['name']."</option>";
									}
								}
							}
							?>
						</select>
					</div>
				</div>

				<div class='form-group'>
					<label class='col-sm-3 control-label'>Jarak</label>

					<div class='col-sm-8'>
						<input class='form-control' name='distance'  type='text' value="<?php
						foreach ($route as $key ) {
							echo $key->distance;
						}
						?>" />
					</div>
				</div>

				<div class='form-group'>
					<label class='col-sm-3 control-label'>Harga DPP</label>

					<div class='col-sm-8'>
						<input class='form-control' name='allowance'  type='text' value="<?php foreach ($route as $key ) {echo number_format($key->allowance);}?>" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Komisi Supir</label>
					<div class="col-sm-8">
						<input class="form-control" name="wash" autocomplete="off"  type="text" value="<?php foreach ($route as $key ) {echo number_format($key->feewash);}?>" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Uang Jalan Supir</label>
					<div class="col-sm-8">
						<input class="form-control" name="driver" autocomplete="off" type="text" value="<?php foreach ($route as $key ) {echo number_format($key->ujs);}?>" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Tabungan Supir</label>
					<div class="col-sm-8">
						<input class="form-control" name="savings" autocomplete="off" type="text" value="<?php foreach ($route as $key ) {echo number_format($key->feesaving);}?>" />
					</div>
				</div>

			</div>

			<div class='modal-footer'>
				<button type='button' class='btn btn-danger' data-dismiss='modal'>Reset</button>
				<input type='submit' style='margin-bottom: 5px;' name='submit' class='btn btn-primary' value='Save!'>
			</div>
		</div>
	</form>
</div>
<script>
	$(document).ready(function(){

		$('.chosen-select').chosen({width: "100%"});
	});
</script>