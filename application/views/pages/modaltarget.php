<div class="modal-dialog modal-lg">
  <form class="form-horizontal" id="formspb" action="<?php echo site_url('target/saveEdit')?>" method="POST"   >
    <div class="modal-content animated bounceInRight">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

        <h4 class="modal-title">Input Target</h4>

      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="col-sm-3 control-label">ID</label>
          <div class="col-sm-8">

            <input class="form-control" name="modalid" value="<?=$target[0]['id']?>" type="text" autocomplete="off" readonly />
          </div>
        </div> 
        <div class="form-group">
          <label class="col-sm-3 control-label">Month</label>
          <div class="col-sm-8">
            <select class="form-control" name="modalmonth">
              <?php
              $x =1;
              $bulan = array('0','January','February','March','April','May','June','July','August','September','October','November','December');
              for($x=1;$x<=12;$x++)
              { if ($target[0]['month'] == $x) {
                # code...
              
                ?>        
                <option value="<?=$x?>" selected><?=$bulan[$x]?></option>
              <?php }else 
                    ?>
                <option value="<?=$x?>"><?=$bulan[$x]?></option>

            <?php } ?>
            </select>
          </div>
        </div>  
        <div class="form-group">
          <label class="col-sm-3 control-label">Year</label>
          <div class="col-sm-8">                                                        
            <select class="form-control" name="modalyear">
              <?php
              $x =1;
              for($x=2019;$x<=2024;$x++)
              {
                ?>        
                <option value="<?=$x?>"><?=$x?></option>
                <?php 
              } 
              ?>
            </select>
          </div>
        </div> 
        <div class="form-group">
          <label class="col-sm-3 control-label">Trailer</label>
          <div class="col-sm-8">
            <input class="form-control" name="modalcarcarrier"  type="text" autocomplete="off" value="<?=$target[0]['carcarrier']?>" />
          </div>
        </div>  

        <div class="form-group">
          <label class="col-sm-3 control-label">Box</label>
          <div class="col-sm-8">
            <input class="form-control" name="modaltansya"  type="text" autocomplete="off" value="<?=$target[0]['tansya']?>" />
          </div>
        </div> 

        <div class="form-group">
          <label class="col-sm-3 control-label">Dutro</label>
          <div class="col-sm-8">
            <input class="form-control" name="modaltowing"  type="text" autocomplete="off" value="<?=$target[0]['towing']?>" />
          </div>
        </div> 

        <div class="form-group">
          <label class="col-sm-3 control-label">Wingbox</label>
          <div class="col-sm-8">
            <input class="form-control" name="modalwingbox"  type="text" autocomplete="off" value="<?=$target[0]['wingbox']?>" />
          </div>
        </div> 

         

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <input type="submit" style="margin-bottom: 5px;" name="modalsubmit" class="btn btn-primary" value="Save">
      </div>
    </div>
  </form>
</div>
