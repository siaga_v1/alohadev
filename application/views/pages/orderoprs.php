<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | Trucks </span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Orders</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Master Data
            </li>
            <li class="active">
                <strong>Order Operationals</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Orders Management</h5>
                    <div class="ibox-tools">
                       
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>    
                </div>
                
                <div class="ibox-content">
                    <form class="form-horizontal" id="formspb" action="<?php echo site_url('order/saveOprs')?>" method="POST"   >
                        <div class="row">
                    <!-- INFORMASI ORDER -->
                    <div class="col-md-6 col-lg-6 col-xs-12">
                    
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Order Code</label>
                                <div class="col-sm-8">
                                    <?php
                                    if (empty($id))
                                    {
                                        $ccode = "1904000001";
                                    } else {
                                        $key= $id[0]['code'];
                                        $pattern = "/(\d+)/";
                                        $arra = preg_split($pattern, $key, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                                        $ccode = $arra[1]+1;
                                        $tambah = $this->session->userdata('id');
                                        date_default_timezone_set("Asia/Bangkok");
                                        $nowdate = date('ymdGis');
                                    }
                                    date_default_timezone_set("Asia/Bangkok");
                                    $jam = date("H:i:s");
                                    ?>
                                    <input type="text" class="form-control" id="code" name="code" value="OC<?=$tambah?><?=$nowdate?>" readonly=""/>
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Order Code</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="noVoucher" name="noVoucher" placeholder="No Voucher / Bukti" />
                                </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 col-xs-12 control-label">Order Date</label>
                            <div class="col-sm-8 col-xs-12">
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" id="orderdate" style="width: 50%;" name="orderdate" readonly=""/>
                                        <div class="input-group bootstrap-timepicker timepicker" style="width: 50%;">
                                            <input readonly="" value="<?=$jam?>" name="orderdatetime" type="text" class="form-control input-small"/>
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-time"></i>
                                            </span>
                                        </div>
                                </div>  
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Customer Name</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                          
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Description</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="description" name="description" rows="2"></textarea>
                            
                            </div>
                        </div>
                        
                        <div class="form-group">

                            <label class="col-sm-12"><h3 class="text-warning">Fleet Information</h3></label>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Fleet (Armada)</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_fleets" name="id_fleets" >
                                    <option value=""></option>
                                    <?php

                                        foreach ($fleet as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['fleetplateno']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Driver 1</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_drivers" name="id_drivers" >
                                    <option value=""></option>
                                    <?php

                                        foreach ($driver as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Driver 2</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_drivers2" name="id_drivers2" >
                                    <option value=""></option>
                                    <?php

                                        foreach ($driver as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 col-lg-6 col-xs-12">
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Shippment No.</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="id_shippment" name="id_shippment">
                            </div>
                        </div>

                        <div class="form-group" >
                            <div class="col-sm-12" id="addRoute">
                                <label class="col-sm-4 control-label">
                                    Route
                                </label>

                                <div class="col-sm-8">
                                    <button class="btn btn-danger btn-circle route" id="route" type="button">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                        <div id="formRoute">
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12" id="tees">
                                <label class="col-sm-4 control-label">
                                    Tambah Bongkaran
                                </label>

                                <div class="col-sm-8" >
                                	<button class="btn btn-danger btn-circle addScnt" id="addScnt" type="button">
                                		<i class="fa fa-plus"></i>
                                	</button>
                                	
                                 	<div >
                                 		
                                 	</div>
                                </div>
                            </div>
                        </div>

                        <div id="p_scents">
                        </div>
                       
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Order Type</label>
                            <div class="col-sm-8">
                                 <input type="text" style="display: none" class="form-control id_ordertypes" readonly="" id="id_ordertypes" name="id_ordertypes" />
                                <input type="text" class="form-control" readonly="" id="textfleet" />
                            </div>
                        </div>

                        <!--<div class="form-group">
                          <label class="col-sm-4 control-label">Price (Unit)</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control priceunit" readonly="" id="priceunit" name="priceunit" />
                          </div>
                        </div>-->

                        <div class="form-group">
                          <label class="col-sm-4 control-label">Add Sales</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control addSales" readonly="" id="addSales" name="addSales" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-4 control-label">Total Sales</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control price" readonly="" id="price" name="price" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-4 control-label">Allowance (UJS)</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control allowance" readonly="" id="allowance" name="allowance" />
                            <input type="text" class="form-control pricelocation" readonly="" style="display: 	none" id="pricelocation" name="pricelocation" />
                            <input type="text" class="form-control wash" readonly="" style="display: 	none" id="wash" name="wash" />
                          </div>
                        </div>

                        

                        <div class="form-group">
                          <label class="col-sm-4 control-label">Allowance Add (UJS)</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control allowance2" readonly="" id="allowance2" name="allowance2" />
                            <input type="text" class="form-control price2" style="display: 	none" readonly="" id="price2" name="price2" />
                            <input type="text" class="form-control totalqty" readonly="" style="display: 	none" id="totalQty" name="totalqty" />
                          </div>
                        </div>

                    	<div class="form-group">
							<label class="col-sm-12"><h2 class="text-warning"></h2></label>
                        </div>
                    
                    </div>

                                        
                </div>
                
                <div class="form-group" >
                  <div class="col-sm-12" id="addCost">
                    <label class="col-sm-2">
                    	<h3 class="text-warning">Additional Cost</h3> 

	                    
                	</label>
                	<div class="col-sm-8">
                	<button class="btn btn-danger btn-circle Cost" id="Cost" type="button">
	                        <i class="fa fa-plus"></i>
	                    </button>
	                </div>
                  </div>
                </div>
                <div id="formCost">


	                
	            </div>
		       
                <div class="form-group 1">
                    <label class="col-sm-12"><h3 class="text-warning">Load Detail</h3></label>
                </div>

                <div id="testqty">
                </div>

		        

                <div class="form-group">
                  <div class="col-sm-12 text-right">
                    <button type="submit" class="btn btn-primary">Save!</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                  </div>
                </div>

               


                </form>
            </div>

                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB"  >
                            <thead>
                                <tr>
                                    <th style="width: 10%">Action #</th>
                                    <th>No #</th>
                                    <th>Order No.</th>
                                    <th style="width: 10%">Order Date</th>
                                    <th>Customer Name</th>
                                    <th>Origin</th>
                                    <th>Destination</th>
                                    <th style="width: 10%">Fleet</th>
                                    <th>Driver</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                    foreach ($order as $row) {
                                ?>
                                <tr>
                                    <td>
                                        <a class="detail btn btn-success btn-xs detail" value="<?=$row['id']?>"> 
                                            <i class="fa fa-book"></i> 
                                        </a>
                                        <a class="btn btn-primary btn-xs edist" value="<?=$row['id']?>"> 
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a class="btn btn-danger btn-xs" href="<?php echo site_url('order/deleteOrder')?>?id=<?=$row['id']?>" onclick="return confirm('Are you sure?');">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        &nbsp;
                                    </td>
                                    <td><?=$no?></td>
                                    <td><?=$row['code']?></td>
                                    <td><?=$row['orderdate']?></td>
                                    <td><?=$row['cname']?></td>
                                    <td><?=$row['n1']?></td>
                                    <td><?=$row['c2']?></td>
                                    <td><?=$row['fleet']?></td>
                                    <td><?=$row['driver']?></td>
                                    
                                </tr>
                                <?php 
                                $no++;
                                }
                                
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>




        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>

    <script>
        $(document).ready(function(){

        	$("#id_fleets").change(function(){
                var id=$(this).val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getFleetType');?>",
                    dataType: "html",
                    data: {
                        id:id,
                        sett:'id'
                        },
                    success:function(data){
                        $("#id_ordertypes").val(data);
                        //alert(data);
                },
                });
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getFleetType');?>",
                    dataType: "html",
                    data: {
                        id:id,
                        sett:'nama'
                        },
                    success:function(data){
                        $("#textfleet").val(data);
                        //alert(data);
                },
                });
            });

        	$('#formCost').on('change','.nomine', function() { 
                var totalPoints = 0;
                $('.nomine').each(function(){
                  var inputVal = $(this).val();
                    if ($.isNumeric(inputVal)) {
                      totalPoints += parseFloat(inputVal);
                    }
                });

                var totalPoints2 = 0;
                  $('.ujs').each(function(){
                    var inputVal2 = $(this).val();
                      if ($.isNumeric(inputVal2)) {
                        totalPoints2 += parseFloat(inputVal2);
                      }
                  });
                  $('#allowance2').val(totalPoints+totalPoints2);

                var totalSales = 0;
                  $('.sales').each(function(){
                    var inputVal = $(this).val();
                      if ($.isNumeric(inputVal)) {
                        totalSales += parseFloat(inputVal);
                      }
                  });
                $('#addSales').val(totalSales);
            });
            var scntDiv = $('#p_scents');
            //var i = $('#p_scents').length;
            var div = document.getElementById("p_scents");
            var nodelist = div.getElementsByClassName("p_scnt");
            var  i= nodelist.length - 1;

            $('#tees').on('click', '.addScnt',function() {
                $.ajax({
                        url:'<?php echo base_url();?>customer/getCust',
                        type:'POST',
                        data:'setting=kiki',
                        success: function(echo){         
                            $('<div><div class="form-group"><label class="col-sm-4 control-label"></label><div class="col-sm-8"><select class="form-control p_scnt chosen-select" id="p_scnt_'+ i +'"  name="p_scnt['+ i +']">'+ echo +'</select><input type="text" class="form-control ujs" style="display: none" readonly="" id="ujs_'+ i +'" name="ujs['+ i +']" /><input type="text" class="form-control sales" style="display: none" readonly="" id="sales_'+ i +'" name="sales['+ i +']" /><a href="#" class="remScnt" id="remScnt">Remove</a></div></div></div>').appendTo(scntDiv);
                            
                            $('#p_scnt_'+ i +'').change(function(){
                              var ini = $('#p_scnt_'+ i +' option:selected').val();
                                $.ajax({
                                  url:'<?php echo base_url();?>customer/getCust',
                                  type:'POST',
                                  data: {
                                        test:ini,
                                        setting:'kik'
                                        },
                                  success: function(echo){
                                    $('#ujs_'+ i +'').val(echo);
                                      var totalPoints = 0;
                                        $('.nomine').each(function(){
                                          var inputVal = $(this).val();
                                            if ($.isNumeric(inputVal)) {
                                              totalPoints += parseFloat(inputVal);
                                            }
                                        });

                                      var totalPoints2 = 0;
                                        $('.ujs').each(function(){
                                          var inputVal2 = $(this).val();
                                            if ($.isNumeric(inputVal2)) {
                                              totalPoints2 += parseFloat(inputVal2);
                                            }
                                          });

                                      $('#allowance2').val(totalPoints+totalPoints2);
                                  }
                                });

                                $.ajax({
                                  url:'<?php echo base_url();?>customer/getCust',
                                  type:'POST',
                                  data: {
                                        test:ini,
                                        setting:'ki'
                                        },
                                  success: function(echo){
                                    $('#sales_'+ i +'').val(echo);
                                      var totalPoints = 0;
                                      var totalPoint  = 0;
                                        $('.sales').each(function(){
                                          var inputVal = $(this).val();
                                          if ($.isNumeric(inputVal)) {
                                            totalPoints += parseFloat(inputVal);
                                          }
                                        });
                						$('#addSales').val(totalPoints);

                                        $('.jssales').each(function(){
                                          var inputVa = $(this).val();
                                            if ($.isNumeric(inputVa)) {
                                              totalPoint += parseFloat(inputVa);
                                            }
                                        });
                                    $('#price').val(totalPoint);
                                  }
                                });
                            });

                        }
                });
                i++;
                return false;
              });
          
          $('#p_scents').on('click','.remScnt', function() { 
            $(this).parent().parent().remove();

            

            var totalPoints = 0;
              $('.nomine').each(function(){
              var inputVal = $(this).val();
              if ($.isNumeric(inputVal)) {
              totalPoints += parseFloat(inputVal);
              }
              });

            var totalPoints2 = 0;
              $('.ujs').each(function(){
              var inputVal2 = $(this).val();
              if ($.isNumeric(inputVal2)) {
              totalPoints2 += parseFloat(inputVal2);
              }
              });
              $('#allowance2').val(totalPoints+totalPoints2);

            var totalSales = 0;
              $('.sales').each(function(){
                var inputVal = $(this).val();
                  if ($.isNumeric(inputVal)) {
                    totalSales += parseFloat(inputVal);
                  }
              });
              $('#addSales').val(totalSales);

            var totalSale = 0;
              $('.jssales').each(function(){
                var inputVa = $(this).val();
                  if ($.isNumeric(inputVa)) {
                    totalSale += parseFloat(inputVa);
                  }
              });
              $('#price').val(totalSale);

                   i--;    
                   return false;
          });

          var tempatCost = $('#formCost');
          var div2 = document.getElementById("formCost");
          var nodelist2 = div2.getElementsByClassName("costadd");
          var  j= nodelist2.length - 1;

          $('#addCost').on('click', '.Cost',function() {
                  $.ajax({
                    url:'<?php echo base_url();?>customer/getCust',
                    type:'POST',
                    data:'setting=cccc',
                    success: function(echo){
                        //$('#p_scnt').html(echo);
                        $('<div class="form-group"><label class="col-sm-1 control-label ">Name</label><div class="col-sm-3"><select class="form-control col-md-4 costadd chosen-select" id="costadd_'+ j +'"  name="costadd['+ j +']">'+ echo +'</select></div><label class="col-sm-1 control-label ">Description</label><div class="col-sm-3"><input type="text" class="form-control desc" id="desc_'+ j +'" name="desc['+ j +']" /></div><label class="col-sm-1 control-label ">Nominal</label><div class="col-sm-2"><input type="text" class="form-control nomine" id="nomine_'+ j +'" name="nomine['+ j +']" /></div> <button class="btn btn-danger btn-circle removeadd" id="removeadd" type="button"><i class="fa fa-minus"></i></button></div>').appendTo(tempatCost);
                        }
                    });
                  j++;
              return false;
          });
          
          $('#formCost').on('click','.removeadd', function() { 
                 $(this).parent().remove();
                 var totalPoints = 0;
                    $('.nomine').each(function(){
                    var inputVal = $(this).val();
                    if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                    }
                    }); 
                  var totalPoints2 = 0;
                    $('.ujs').each(function(){
                    var inputVal2 = $(this).val();
                    if ($.isNumeric(inputVal2)) {
                    totalPoints2 += parseFloat(inputVal2);
                    }
                    });
                    $('#allowance2').val(totalPoints+totalPoints2);
                    j--;    
                 return false;
          });
        	


            $(".2, .3, .4, .5, .6, .7, .8").hide();

            $("#loadqty").change(function(){
                var route=$(".id_routes option:selected").val();

                var unit = $(this).val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceUnit');?>",
                    dataType: "html",
                    data: {
                        routes:route
                        },
                    success:function(data){
                        $("#priceunit").val(data);
                        var total = parseInt(unit) * parseInt(data);
                        $("#price").val(total);
                       // alert(harga);
                },
                });
                if (unit == 1) {
                    $(".1").show();
                    $(".2, .3, .4, .5, .6, .7, .8").hide();

                } else if (unit == 2) {
                    $(".1, .2").show();
                    $(".3, .4, .5, .6, .7, .8").hide();
                } else if (unit == 3) {
                    $(".1, .2, .3").show();
                    $(".4, .5, .6, .7, .8").hide();
                }else if (unit == 4) {
                    $(".1, .2, .3, .4").show();
                    $(".5, .6, .7, .8").hide();
                }else if (unit == 5) {
                    $(".1, .2, .3,.4, .5").show();
                    $(".6, .7, .8").hide();
                }else if (unit == 6) {
                    $(".1, .2, .3, .4, .5, .6").show();
                    $(".7, .8").hide();
                }else if (unit == 7) {
                    $(".1, .2, .3,.4, .5, .6, .7").show();
                    $(".8").hide();
                } else {
                    $(".1, .2, .3, .4, .5, .6, .7, .8").show();
                }
            });

            $(".11, .22, .33, .44, .55, .66, .77, .88").hide();

            $("#loadqty2").change(function(){
                var unit = $(this).val();
                var origin=$(".id_citieso2 option:selected").val();
                var dest=$(".id_citiesd2 option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sett:'bbb'
                        },
                    success:function(data){
                        var perkalian = parseInt(unit)*parseInt(data);
                       $("#pricelocation").val(perkalian);
                },
                });
                if (unit == 1) {
                    $(".11").show();
                    $(".22, .33, .44, .55, .66, .77, .88").hide();
                } else if (unit == 2) {
                    $(".11, .22").show();
                    $(".33, .44, .55, .66, .77, .88").hide();
                } else if (unit == 3) {
                    $(".11, .22, .33").show();
                    $(".44, .55, .66, .77, .88").hide();
                }else if (unit == 4) {
                    $(".11, .22, .33, .44").show();
                    $(".55, .66, .77, .88").hide();
                }else if (unit == 5) {
                    $(".11, .22, .33,.44, .55").show();
                    $(".66, .77, .88").hide();
                }else if (unit == 6) {
                    $(".11, .22, .33, .44, .55, .66").show();
                    $(".77, .88").hide();
                }else if (unit == 7) {
                    $(".11, .22, .33, .44, .55, .66, .77,").show();
                    $(".88").hide();
                } else {
                    $(".11, .22, .33, .44, .55, .66, .77, .88").show();
                }
            });


    var tempatRoute = $('#formRoute');
    var getId = document.getElementById("formRoute");
    var getClass = getId.getElementsByClassName("id_citieso");
    var noRoute = getClass.length - 1;

    $('#addRoute').on('click', '.route',function() {
        var ini = $('#id_customers option:selected').text();
        var itu = $('#id_customers option:selected').val();
        //alert(itu);

        $.ajax({
            url:'<?php echo base_url();?>order/getOrigin',
            type:'POST',
            data: {
                    cust:itu
                        },
            success: function(echo){
                if (ini.match(/TOYOTA ASTRA/g)) {
                    $('<div class="uhuy"><div class="form-group"><label class="col-sm-4 control-label">Example Pareto</label><div class="col-sm-8"><select class="form-control id_citieso" id="id_citieso_'+ noRoute +'" name="id_citieso['+ noRoute +']"><option value=""></option>'+ echo +'</select></div></div><div class="form-group"><label class="col-sm-4 control-label">Destination</label><div class="col-sm-8"><select class="form-control id_citiesd" id="id_citiesd_'+ noRoute +'" name="id_citiesd['+ noRoute +']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label ">Load Qty</label><div class="col-sm-8"><select class="form-control loadqty" id="loadqty_'+ noRoute +'"><option value=""></option><option value="1">1</option><option value="1">2</option><option value="1">3</option><option value="1">4</option><option value="1">5</option><option value="1">6</option><option value="1">7</option><option value="1">8</option><option value="1">9</option><option value="1">10</option><option value="1">11</option><option value="1">12</option><option value="1">13</option><option value="1">14</option><option value="1">15</option><option value="1">16</option><option value="1">17</option><option value="1">18</option><option value="1">19</option><option value="1">20</option></select></div></div><div class="form-group"><label class="col-sm-4 control-label">Route (UJS)</label><div class="col-sm-8"><select class="form-control id_routes" id="id_routes_'+ noRoute +'" name="id_routes['+ noRoute +']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label"></label><div class="col-sm-8"><input type="text" class="jsunit" name="jsunit['+ noRoute +']" id="jsunit_'+ noRoute +'" ><input type="text" class="jsqty" style="display:none" name="jsqty['+ noRoute +']" id="jsqty_'+ noRoute +'"><input type="text" style="display:none" class="txtqty" name="loadqty['+ noRoute +']"><input type="text" class="jssales" style="display:none" name="jssales['+ noRoute +']" id="jssales_'+ noRoute +'"><button class="btn btn-danger btn-xs remRoute" id="remRoute_'+ noRoute +'" type="button"><i class="fa fa-minus"></i>  Remove</button></div></div></div>').appendTo(tempatRoute);
                } else {
                $('<div class="uhuy"><div class="form-group"><label class="col-sm-4 control-label">Origin</label><div class="col-sm-8"><select class="form-control id_citieso" id="id_citieso_'+ noRoute +'" name="id_citieso['+ noRoute +']"><option value=""></option>'+ echo +'</select></div></div><div class="form-group"><label class="col-sm-4 control-label">Destination</label><div class="col-sm-8"><select class="form-control id_citiesd" id="id_citiesd_'+ noRoute +'" name="id_citiesd['+ noRoute +']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label ">Load Qty</label><div class="col-sm-8"><select class="form-control loadqty" id="loadqty_'+ noRoute +'" name="loadqty['+ noRoute +']"><option value=""></option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option></select></div></div><div class="form-group"><label class="col-sm-4 control-label">Route (UJS)</label><div class="col-sm-8"><select class="form-control id_routes" id="id_routes_'+ noRoute +'" name="id_routes['+ noRoute +']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label">Price Unit</label><div class="col-sm-8"><input type="text" class="form-control jsunit" style="display:none" name="jsunit['+ noRoute +']" id="jsunit_'+ noRoute +'"><input type="text" class="jsqty" style="display:none" name="jsqty['+ noRoute +']" id="jsqty_'+ noRoute +'"><input type="text" class="jssales" style="display:none" name="jssales['+ noRoute +']" id="jssales_'+ noRoute +'"><button class="btn btn-danger btn-xs remRoute" id="remRoute_'+ noRoute +'" type="button"><i class="fa fa-minus"></i>  Remove</button></div></div></div>').appendTo(tempatRoute);
                }
                $('.id_citieso').chosen();
                $('.id_citiesd').chosen();
                $('.loadqty').chosen();

                $('#formRoute').on('change', '.id_routes',function() {  
                    var id = $(this).attr('id');    
                    var isi = $(this).val();
                    var pecah = id.split("_");
                    var totalPoints = 0;
                    var unit = $('#jsqty_'+ pecah['2'] +'').val();
                    //alert(isi);
                        $.ajax({
                            type:"POST",
                            url: "<?php echo site_url('order/getPricebyRoutes');?>",
                            data: {
                                route:isi,
                                sett:'ujs'
                            },
                            success:function(dat){
                                if(pecah['2'] == 0){ $('#allowance').val(dat); }
                                //alert(dat);
                            },
                        });

                        $.ajax({
                            type:"POST",
                            url: "<?php echo site_url('order/getPricebyRoutes');?>",
                            data: {
                                route:isi,
                                sett:'unit'
                            },
                            success:function(data){
                                var totaljssales = parseInt(unit) * parseInt(data);

                                $('#jsunit_'+ pecah['2'] +'').val(data);
                                if(pecah['2'] == 0){ $('#priceunit').val(data); }

                                $('#jssales_'+ pecah['2'] +'').val(totaljssales);

                                $('.jssales').each(function(){
                                  var inputVal = $(this).val();
                                    if ($.isNumeric(inputVal)) {
                                      totalPoints += parseFloat(inputVal);
                                    }
                                });
                                $('#price').val(totalPoints);
                            },
                        });
                    
                });

                $('#formRoute').on('change', '.id_citieso',function() { 
                    var id = $(this).attr('id');                    
                    var pecah = id.split("_");
        			var itu = $('#id_customers option:selected').val();
                    var origin=$('#'+ id +' option:selected').val();
                    var dest=$('#id_citiesd_'+ pecah['2'] +' option:selected').val();

                        $.ajax({
                            type:"POST",
                            url: "<?php echo site_url('order/getDestination');?>",
                            dataType: "html",
                            data: {
                                citieso:origin,
                                cust:itu
                            },
                            success:function(data){
                                $('#id_citiesd_'+ pecah['2'] +'').html(data);
                                $('#id_citiesd_'+ pecah['2'] +'').trigger("chosen:updated");
                                //alert(data);
                            },
                        });

                        $.ajax({
                            type:"POST",
                            url: "<?php echo site_url('order/getPriceRoutes');?>",
                            dataType: "html",
                            data: {
                                citieso:origin,
                                citiesd:dest,
                                sett:'option'
                            },
                            success:function(data){
                                $('#id_routes_'+ pecah['2'] +'').html(data);
                                $('#id_routes_'+ pecah['2'] +'').trigger("chosen:updated");
                                //alert(data);
                            },
                        });
                });

                $('#formRoute').on('change', '.id_citiesd',function() { 
                    var id = $(this).attr('id');                    
                    var pecah = id.split("_");
                    var origin=$('#id_citieso_'+ pecah['2'] +' option:selected').val();
                    var dest=$('#'+ id +' option:selected').val();
                    var totalPoints = 0;
                    var unit = $('#jsqty_'+ pecah['2'] +'').val();
                    //alert(unit);
                        $.ajax({
                            type:"POST",
                            url: "<?php echo site_url('order/getPriceRoutes');?>",
                            dataType: "html",
                            data: {
                                citieso:origin,
                                citiesd:dest,
                                sett:'option'
                            },
                            success:function(data){
                                $('#id_routes_'+ pecah['2'] +'').html(data);
                                $('#id_routes_'+ pecah['2'] +'').trigger("chosen:updated");
                                //alert(data);
                            },
                        });                        
                });

                $('#formRoute').on('change', '.loadqty',function() { 
                    var id = $(this).attr('id');
                    var pecah = id.split("_");
                    var route = $('#id_routes_'+ pecah['1'] +' option:selected').val();
                    var unit = $('#'+ id +' option:selected').val();
                    var unittxt = $('#'+ id +' option:selected').text();
                    var jsunit = $('#jsunit_'+ pecah['1'] +'').val();
                    var jsqty = $('#jsqty_'+ pecah['1'] +'').val();
                    var jsqtytxt = $('.txtqty').val();
                    var totaljssales = parseInt(unit)*parseInt(jsunit);
                    var totalPoints = 0;
                    var totalQty = 0;

                    //alert(unit);

                        $.ajax({
                            type:"POST",
                            url: "<?php echo site_url('order/getPricebyRoutes');?>",
                            data: {
                                route:route,
                                sett:'unit'
                            },
                            success:function(data){
                                var totaljssales = parseInt(unit) * parseInt(data);

                                $('#jsunit_'+ pecah['2'] +'').val(data);
                                if(pecah['2'] == 0){ $('#priceunit').val(data); }

                                $('#jssales_'+ pecah['2'] +'').val(totaljssales);

                                $('.jssales').each(function(){
                                  var inputVal = $(this).val();
                                    if ($.isNumeric(inputVal)) {
                                      totalPoints += parseFloat(inputVal);
                                    }
                                });
                                $('#price').val(totalPoints);
                                
                                $('.jsqty').each(function(){
                                  var inputQty = $(this).val();
                                    if ($.isNumeric(inputQty)) {
                                      totalQty += parseFloat(inputQty);
                                    }
                                });
                                $('#totalQty').val(totalQty);

                                var loadloop = 0;
                                var tempatLoad = $('#testqty');

                                function addAppend() {
                                    $('#testqty').append(formAppend);
                                }

                                /*alert(jsqty);
                                if ($('#id_customers option:selected').text().match(/TOYOTA ASTRA/g)) {
                                    for (loadloop = 0; loadloop < jsqtytxt; loadloop++){
                                        //alert('test');
                                        $('.test_'+ pecah['1'] +'_'+ loadloop +'').remove();
                                        
                                    }
                                } else {
                                    for (loadloop = 0; loadloop < jsqty; loadloop++){
                                        //alert('test');
                                        $('.test_'+ pecah['1'] +'_'+ loadloop +'').remove();
                                        
                                    }
                                }
                                for (loadloop = 0; loadloop < unittxt; loadloop++){
                                    if ($('#id_customers option:selected').text().match(/PARsadasdETO/g)) {
                                        $('#testqty').append('<div class="slebew"><div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-4"><input type="text" class="form-control" id="do[0]" placeholder="Shipping Document" name="do[0]" /></div><div class="col-sm-4"><input type="text" class="form-control" id="machineno[0]" placeholder="Description" name="machineno[0]" /></div><div class="col-sm-4"><input type="text" class="form-control" id="color[0]" placeholder="Case" name="color[0]" /></div></div></div>');
                                    } else if ($('#id_customers option:selected').text().match(/AI - DAIHATSU/g)) {
                                        $('#testqty').append('<div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="frameno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="frameno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="address_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Address" name="address_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="machineno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Engine Number" name="machineno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="type_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="type_'+ pecah['1'] +'['+ loadloop +']" /></div></div>');
                                    } else if ($('#id_customers option:selected').text().match(/ASTRA DAIHATSU MOTOR/g)) {
                                        $('#testqty').append('<div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="frameno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="frameno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="machineno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Engine Number" name="machineno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="type_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="type_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="color_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Color" name="color_'+ pecah['1'] +'['+ loadloop +']" /></div></div>');
                                    } else if ($('#id_customers option:selected').text().match(/AI - ISUZU/g)) {
                                        $('#testqty').append('<div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="frameno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="frameno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="machineno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Engine Number" name="machineno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="type_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="type_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="irisno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Iris Number" name="irisno_'+ pecah['1'] +'['+ loadloop +']" /></div></div>');
                                    } else if ($('#id_customers option:selected').text().match(/IAMI/g)) {
                                        $('#testqty').append('<div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="frameno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="frameno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="machineno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Engine Number" name="machineno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="type_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="type_'+ pecah['1'] +'['+ loadloop +']" /></div></div>');
                                    } else if ($('#id_customers option:selected').text().match(/VLD/g)) {
                                        $('#testqty').append('<div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="frameno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="frameno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="type_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="type_'+ pecah['1'] +'['+ loadloop +']" /></div></div>');
                                    } else if ($('#id_customers option:selected').text().match(/TOYOTA ASTRA/g)) {
                                        $('#testqty').append('<div class="slebew"><div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-4"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Shipping Document" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-4"><input type="text" class="form-control" id="custcode_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Description" name="custcode_'+ pecah['1'] +'['+ loadloop +']"/></div><div class="col-sm-4"><input type="text" class="form-control" id="case_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Case" name="case_'+ pecah['1'] +'['+ loadloop +']" /></div></div></div>');
                                    } else {
                                        $('#testqty').append('<div class="form-group test_'+ pecah['1'] +'_'+ loadloop +'"><div class="col-sm-2"><input type="text" class="form-control" id="deliveryno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="DO Number" name="deliveryno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="frameno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Frame Number"  name="frameno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="machineno_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Engine Number" name="machineno_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="type_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Type" name="type_'+ pecah['1'] +'['+ loadloop +']" /></div><div class="col-sm-2"><input type="text" class="form-control" id="color_'+ pecah['1'] +'_'+ loadloop +'" placeholder="Color" name="color_'+ pecah['1'] +'['+ loadloop +']" /></div></div>');
                                    }
                                }

                                */
                                //alert(totalPoints);
                               // alert(harga);
                        },
                        });
                        $('#jssales_'+ pecah['1'] +'').val(totaljssales);
                        $('#jsqty_'+ pecah['1'] +'').val(unit);
                        $('.txtqty').val(unittxt);


                                
                });
            }
        });
            noRoute++;
            return false;
    });

    $('#formRoute').on('click', '.remRoute',function() { 
        var id    = $(this).attr('id');
        var pecah = id.split("_"); 
        var totalQty = 0;
        var totalPoints = 0;
        var loadloop = 0;
        var jsqty = $('#jsqty_'+ pecah['1'] +'').val();
        if (pecah['1']=='0') {
            $('#priceunit').val("0");
            $('#allowance').val("0");
        }
        $(this).parent().parent().parent().remove();

        for (loadloop = 0; loadloop < jsqty; loadloop++){
            $('.test_'+ pecah['1'] +'_'+ loadloop +'').remove();
        }

        $('.id_routes').empty();

        

        $('.jssales').each(function(){
            var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                }
        });

        $('#price').val(totalPoints);


        $('.jsqty').each(function(){
            var inputQty = $(this).val();
            if ($.isNumeric(inputQty)) {
                totalQty += parseFloat(inputQty);
            }
        });

        $('#totalQty').val(totalQty);

        noRoute--;    
        return false;
    });

    $('#id_customers').change(function(){
        var countJsunit = $('.uhuy');
        var countRowQty= $('.slebew');
        var totalPoints = 0;
        //alert(countJsunit.length);
        $('.uhuy').remove();
        $('.slebew').remove();

        $('.jssales').each(function(){
            var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                }
        });

        $('#price').val(totalPoints);

        noRoute=-1;    
        //alert(noRoute);
        return false;
    });

    

            $(".id_citieso2").change(function(){
                var origin=$(".id_citieso2 option:selected").val();
                var dest=$(".id_citiesd2 option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sett:'bbb'
                        },
                    success:function(data){
                        //$("#id_routes").html(data);
                        var unit = $("#loadqty2").val();
                        var perkalian = parseInt(unit)*parseInt(data);
                       $("#pricelocation").val(perkalian);
                        //alert(data);
                },
                });
            });
            
            
            $(".id_citiesd2").change(function(){
                var origin=$(".id_citieso2 option:selected").val();
                var dest=$(".id_citiesd2 option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    data: {
                        citieso:origin,
                        citiesd:dest,
                        sett:'bbb'
                        },
                    success:function(data){
                        var unit = $("#loadqty2").val();
                        var perkalian = parseInt(unit)*parseInt(data);
                       $("#pricelocation").val(perkalian);
                },
                });
            });



           

           


            $('#orderdate').datepicker({format:'yyyy-mm-dd'});

            $('#jam').clockpicker();

            $('div.ibox-content').slideUp();

            $('.ibox-tools a.collapse-link i').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');

            $(".detail").click(function(){
                
            });

            setTimeout(function() {
                toastr.options = {
                  closeButton: true,
                  debug: false,
                  progressBar: true,
                  preventDuplicates: false,
                  positionClass: "toast-top-right",
                  onclick: false,
                  showDuration: "1000",
                  hideDuration: "1000",
                  timeOut: "7000",
                  extendedTimeOut: "1000",
                  showEasing: "swing",
                  hideEasing: "linear",
                  showMethod: "fadeIn",
                  hideMethod: "fadeOut"
                };
                toastr.success('Fleet Management System', 'Welcome to FMS');

            }, 1300);
            $('#listSPB').DataTable({
                autoWidth: true,
                ordering : false

            });

            $('.chosen-select').chosen({width: "100%"});


            $("#listSPB").on("click", ".edist", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/modaleditoprs');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });
			
			$("#listSPB").on("click", ".detail", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/modaldetail');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });

            

        });

    </script>

