<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | Trucks </span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout') ?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Orders</h2>

        <ol class="breadcrumb">
            <li>
                <a href="index.html">Beranda</a>
            </li>
            <li>
                Master Data
            </li>
            <li class="active">
                <strong>Order</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox collapsed">

                <div class="ibox-title">
                    <h5>Order</h5>
                    <div class="ibox-tools">

                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content">
                    <form class="form-horizontal" id="formspb" action="<?php echo site_url('order/savePareto') ?>" method="POST">
                        <div class="row">
                            <!-- INFORMASI ORDER -->
                            <div class="col-md-6 col-lg-6 col-xs-12">

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Kode Order</label>
                                    <div class="col-sm-8">
                                        <?php
                                        if (empty($id)) {
                                            $ccode = "1904000001";
                                        } else {
                                            $key = $id[0]['code'];
                                            $pattern = "/(\d+)/";
                                            $arra = preg_split($pattern, $key, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                                            $ccode = $arra[1] + 1;
                                            $tambah = $this->session->userdata('id');
                                            date_default_timezone_set("Asia/Bangkok");
                                            $nowdate = date('ymdGis');
                                        }
                                        date_default_timezone_set("Asia/Bangkok");
                                        $jam = date("H:i:s");
                                        ?>
                                        <input type="text" class="form-control" id="code" name="code" value="OC<?= $tambah ?><?= $nowdate ?>" readonly="" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 col-xs-12 control-label">Tanggal</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control datepicker" id="orderdate" style="width: 50%;" name="orderdate" required autocomplete="off">
                                            <div class="input-group bootstrap-timepicker timepicker" style="width: 50%;">
                                                <input readonly="" value="<?= $jam ?>" name="orderdatetime" type="text" class="form-control input-small" />
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-time"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Customer</label>
                                    <div class="col-sm-8">
                                        <select class="form-control chosen-select" id="id_customers" name="id_customers">
                                            <option value=""></option>
                                            <?php

                                            foreach ($customer as $row) {
                                            ?>

                                                <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>

                                            <?php
                                            }

                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Keterangan</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" id="description" name="description" rows="2"></textarea>

                                    </div>
                                </div>

                                <div class="form-group">

                                    <label class="col-sm-12">
                                        <h3 class="text-warning">Informasi Moda</h3>
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">No Armada</label>
                                    <div class="col-sm-8">
                                        <select class="form-control chosen-select" id="id_fleets" name="id_fleets">
                                            <option value=""></option>
                                            <?php

                                            foreach ($fleet as $row) {
                                            ?>

                                                <option value="<?= $row['id'] ?>"><?= $row['fleetplateno'] ?></option>

                                            <?php
                                            }

                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Supir</label>
                                    <div class="col-sm-8">
                                        <select class="form-control chosen-select" id="id_drivers" name="id_drivers">
                                            <option value=""></option>

                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6 col-lg-6 col-xs-12">

                                <!--<div class="form-group">
                            <label class="col-sm-4 control-label">No Surat Jalan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="id_shippment" name="id_shippment">
                            </div>
                        </div>
                    <div class="form-group">
                            <label class="col-sm-4 control-label">Supir 2</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_drivers2" name="id_drivers2" >
                                    <option value=""></option>
                                    <?php

                                    foreach ($driver as $row) {
                                    ?>

                                            <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>

                                            <?php
                                        }

                                            ?>
                                </select>
                            </div>
                        </div>-->

                                <div class="form-group">
                                    <div class="col-sm-12" id="addRoute">
                                        <label class="col-sm-4 control-label">
                                            Rute
                                        </label>

                                        <div class="col-sm-8">
                                            <button class="btn btn-danger btn-circle route" id="route" type="button">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div id="formRoute">
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12" id="tees">
                                        <label class="col-sm-4 control-label">
                                            Tambah Bongkaran
                                        </label>

                                        <div class="col-sm-8">
                                            <button class="btn btn-danger btn-circle addScnt" id="addScnt" type="button">
                                                <i class="fa fa-plus"></i>
                                            </button>

                                            <div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="p_scents">
                                </div>

                                <!--<div class="form-group">
                            <label class="col-sm-4 control-label">Order Type</label>
                            <div class="col-sm-8">
                                 <input type="text" style="display: none" class="form-control id_ordertypes" readonly="" id="id_ordertypes" name="id_ordertypes" />
                                <input type="text" class="form-control" readonly="" id="textfleet" />
                            </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-4 control-label">Price (Unit)</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control priceunit" readonly="" id="priceunit" name="priceunit" />
                          </div>
                        </div>-->

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Tambahan Tagihan</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control addSales" readonly="" id="addSales" name="addSales" data-type="currency" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Total Tagihan</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control price" readonly="" id="price" name="price" data-type="currency" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Uang Jalan</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control allowance" readonly="" id="allowance" name="allowance" data-type="currency" />
                                        <input type="text" class="form-control pricelocation" readonly="" style="display: 	none" id="pricelocation" name="pricelocation" />
                                        <input type="text" class="form-control wash" readonly="" style="display: 	none" id="wash" name="wash" />
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Tagih</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control biayall" readonly="" id="biayall" name="biayall" data-type="currency" />
                                        <input type="text" class="form-control price2" style="display: 	none" readonly="" id="price2" name="price2" />
                                        <input type="text" class="form-control totalqty" readonly="" style="display: 	none" id="totalQty" name="totalqty" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Biaya</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control allowance2" readonly="" id="allowance2" name="allowance2" data-type="currency" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-12">
                                        <h2 class="text-warning"></h2>
                                    </label>
                                </div>

                            </div>


                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="col-sm-2">
                                    <h3 class="text-warning">Tagih</h3>
                                </label>
                            </div>
                        </div>

                        <div id="BiayaLain">
                            <div class="col-md-6 col-lg-6 col-xs-12">

                                <div class="form-group">
                                    <label class="col-sm-4 control-label ">Biaya Operasional</label>
                                    <div class="col-sm-8">
                                        <input type="text" style="display:none" class="form-control" id="idakun_0" name="idakun[0]" value="14" />
                                        <input type="text" style="display:none" class="form-control" id="costakun_0" name="costakun[0]" value="31" />
                                        <input type="text" style="display:none" class="form-control" id="coa_0" name="coa[0]" value="113" />
                                        <input type="text" class="form-control nomines" id="biayaakun_0" name="biayaakun[0]" data-type="currency" placeholder="Operasional" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label ">Biaya LOLO</label>
                                    <div class="col-sm-8">
                                        <input type="text" style="display:none" class="form-control" id="idakun_1" name="idakun[1]" value="14" />
                                        <input type="text" style="display:none" class="form-control" id="costakun_1" name="costakun[1]" value="32" />
                                        <input type="text" style="display:none" class="form-control" id="coa_1" name="coa[1]" value="114" />
                                        <input type="text" class="form-control nomines" id="biayaakun_1" name="biayaakun[1]" data-type="currency" placeholder="LOLO" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label ">Biaya DEPO</label>
                                    <div class="col-sm-8">
                                        <input type="text" style="display:none" class="form-control" id="idakun_2" name="idakun[2]" value="14" />
                                        <input type="text" style="display:none" class="form-control" id="coa_2" name="coa[2]" value="115" />
                                        <input type="text" style="display:none" class="form-control" id="costakun_2" name="costakun[2]" value="33" />
                                        <input type="text" class="form-control nomines" id="biayaakun_2" name="biayaakun[2]" data-type="currency" placeholder="Depo" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label ">Biaya Parkir</label>
                                    <div class="col-sm-8">
                                        <input type="text" style="display:none" class="form-control" id="idakun_3" name="idakun[3]" value="14" />
                                        <input type="text" style="display:none" class="form-control" id="coa_3" name="coa[3]" value="116" />
                                        <input type="text" style="display:none" class="form-control" id="costakun_3" name="costakun[3]" value="34" />
                                        <input type="text" class="form-control nomines" id="biayaakun_3" name="biayaakun[3]" data-type="currency" placeholder="Parkir" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label ">Biaya Kawalan</label>
                                    <div class="col-sm-8">
                                        <input type="text" style="display:none" class="form-control" id="idakun_4" name="idakun[4]" value="14" />
                                        <input type="text" style="display:none" class="form-control" id="coa_4" name="coa[4]" value="117" />
                                        <input type="text" style="display:none" class="form-control" id="costakun_4" name="costakun[4]" value="36" />
                                        <input type="text" class="form-control nomines" id="biayaakun_4" name="biayaakun[4]" data-type="currency" placeholder="Kawalan" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label ">Biaya Storage</label>
                                    <div class="col-sm-8">
                                        <input type="text" style="display:none" class="form-control" id="idakun_5" name="idakun[5]" value="14" />
                                        <input type="text" style="display:none" class="form-control" id="costakun_5" name="costakun[5]" value="36" />
                                        <input type="text" style="display:none" class="form-control" id="coa_5" name="coa[5]" value="118" />
                                        <input type="text" class="form-control nomines" id="biayaakun_4" name="biayaakun[5]" data-type="currency" placeholder="Storage" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12">

                                <div class="form-group">
                                    <label class="col-sm-4 control-label ">Biaya RC</label>
                                    <div class="col-sm-8">
                                        <input type="text" style="display:none" class="form-control" id="idakun_6" name="idakun[6]" value="14" />
                                        <input type="text" style="display:none" class="form-control" id="costakun_6" name="costakun[6]" value="36" />
                                        <input type="text" style="display:none" class="form-control" id="coa_6" name="coa[6]" value="119" />
                                        <input type="text" class="form-control nomines" id="biayaakun_4" name="biayaakun[6]" data-type="currency" placeholder="RC" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label ">Biaya Seal</label>
                                    <div class="col-sm-8">
                                        <input type="text" style="display:none" class="form-control" id="idakun_7" name="idakun[7]" value="14" />
                                        <input type="text" style="display:none" class="form-control" id="costakun_7" name="costakun[7]" value="36" />
                                        <input type="text" style="display:none" class="form-control" id="coa_7" name="coa[7]" value="120" />
                                        <input type="text" class="form-control nomines" id="biayaakun_4" name="biayaakun[7]" data-type="currency" placeholder="Seal" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label ">Biaya Alih Kapal</label>
                                    <div class="col-sm-8">
                                        <input type="text" style="display:none" class="form-control" id="idakun_8" name="idakun[8]" value="14" />
                                        <input type="text" style="display:none" class="form-control" id="costakun_8" name="costakun[8]" value="36" />
                                        <input type="text" style="display:none" class="form-control" id="coa_8" name="coa[8]" value="121" />
                                        <input type="text" class="form-control nomines" id="biayaakun_4" name="biayaakun[8]" data-type="currency" placeholder="Alih Kapal" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label ">Biaya Cuci Kontainer</label>
                                    <div class="col-sm-8">
                                        <input type="text" style="display:none" class="form-control" id="idakun_9" name="idakun[9]" value="14" />
                                        <input type="text" style="display:none" class="form-control" id="costakun_9" name="costakun[9]" value="36" />
                                        <input type="text" style="display:none" class="form-control" id="coa_9" name="coa[9]" value="122" />
                                        <input type="text" class="form-control nomines" id="biayaakun_4" name="biayaakun[9]" data-type="currency" placeholder="Cuci Kontainer" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label ">Biaya SP2</label>
                                    <div class="col-sm-8">
                                        <input type="text" style="display:none" class="form-control" id="idakun_10" name="idakun[10]" value="14" />
                                        <input type="text" style="display:none" class="form-control" id="costakun_10" name="costakun[10]" value="36" />
                                        <input type="text" style="display:none" class="form-control" id="coa_10" name="coa[10]" value="123" />
                                        <input type="text" class="form-control nomines" id="biayaakun_4" name="biayaakun[10]" data-type="currency" placeholder="SP2" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12" id="addCost">
                                <label class="col-sm-2">
                                    <h3 class="text-warning">Biaya</h3>


                                </label>
                                <div class="col-sm-8">
                                    <button class="btn btn-danger btn-circle Cost" id="Cost" type="button">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div id="formCost">


                        </div>

                        <div class="form-group 1">
                            <label class="col-sm-12">
                                <h3 class="text-warning">Order Lanjutan</h3>
                            </label>
                        </div>

                        <div id="testqty">
                        </div>



                        <div class="form-group">
                            <div class="col-sm-12 text-right">
                                <button type="submit" class="btn btn-primary">Save!</button>
                                <button type="reset" class="btn btn-danger">Reset</button>
                            </div>
                        </div>




                    </form>
                </div>

                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB">
                            <thead>
                                <tr>
                                    <th style="width: 10%">Action #</th>
                                    <th>No #</th>
                                    <th>Order No.</th>
                                    <th style="width: 10%">Order Date</th>
                                    <th>Customer Name</th>
                                    <th>Origin</th>
                                    <th>Destination</th>
                                    <th style="width: 10%">Fleet</th>
                                    <th>Driver</th>
                                    <th>Update Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($order as $row) {
                                ?>
                                    <tr>
                                        <td>
                                            <a class="detail btn btn-success btn-xs detail" value="<?= $row['id'] ?>">
                                                <i class="fa fa-book"></i>
                                            </a>
                                            <a class="btn btn-primary btn-xs edist" value="<?= $row['id'] ?>">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="btn btn-danger btn-xs del" value="<?= $row['id'] ?>">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                            &nbsp;
                                        </td>
                                        <td><?= $no ?></td>
                                        <td><?= $row['code'] ?></td>
                                        <td><?= date('d-m-Y', strtotime($row['orderdate'])) ?></td>
                                        <td><?= $row['cname'] ?></td>
                                        <td><?= $row['n1'] ?></td>
                                        <td><?= $row['c2'] ?></td>
                                        <td><?= $row['fleet'] ?></td>
                                        <td><?= $row['driver'] ?></td>
                                        <td>
                                            <?php
                                            if (!is_null($row['updatedatetime'])) {
                                                date('d-m-Y', strtotime($row['updatedatetime']));
                                            }
                                            ?>
                                        </td>

                                    </tr>
                                <?php
                                    $no++;
                                }

                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">

                </div>
                <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>





<script>
    $(document).ready(function() {

        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() {
                formatCurrency($(this), "blur");
            }
        });


        function formatNumber(n) {
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }


        function formatCurrency(input, blur) {
            var input_val = input.val();

            if (input_val === "") {
                return;
            }

            var original_len = input_val.length;

            var caret_pos = input.prop("selectionStart");

            if (input_val.indexOf(".") >= 0) {

                var decimal_pos = input_val.indexOf(".");

                var left_side = input_val.substring(0, decimal_pos);
                var right_side = input_val.substring(decimal_pos);

                left_side = formatNumber(left_side);

                right_side = formatNumber(right_side);

                if (blur === "blur") {
                    right_side += "";
                }

                right_side = right_side.substring(0, 2);

                input_val = "" + left_side + "." + right_side;

            } else {
                input_val = formatNumber(input_val);
                input_val = "" + input_val;

                if (blur === "blur") {
                    input_val += "";
                }
            }

            input.val(input_val);

            var updated_len = input_val.length;
            caret_pos = updated_len - original_len + caret_pos;
            input[0].setSelectionRange(caret_pos, caret_pos);
        }


        $('.nomines').change(function() {
            var totalPoints = 0;
            $('.nomines').each(function() {
                var inputVal = $(this).val();
                var inputvaltt = parseFloat(inputVal.replace(/,/g, ''));
                if ($.isNumeric(inputvaltt)) {
                    totalPoints += parseFloat(inputvaltt);
                }
            });
            $('#biayall').val(new Intl.NumberFormat('en-ID').format(totalPoints));

        });

        $('#formCost').on('change', '.nomine', function() {
            var totalPoints = 0;
            $('.nomine').each(function() {
                var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                }
            });

            var totalPoints2 = 0;
            $('.ujs').each(function() {
                var inputVal2 = $(this).val();
                if ($.isNumeric(inputVal2)) {
                    totalPoints2 += parseFloat(inputVal2);
                }
            });
            $('#allowance2').val(new Intl.NumberFormat('en-ID').format(totalPoints + totalPoints2));

            var totalSales = 0;
            $('.sales').each(function() {
                var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                    totalSales += parseFloat(inputVal);
                }
            });
            $('#addSales').val(totalSales);
        });

        var scntDiv = $('#p_scents');
        var div = document.getElementById("p_scents");
        var nodelist = div.getElementsByClassName("p_scnt");
        var i = nodelist.length - 1;

        $('#tees').on('click', '.addScnt', function() {
            $.ajax({
                url: '<?php echo base_url(); ?>customer/getCust',
                type: 'POST',
                data: 'setting=kiki',
                success: function(echo) {
                    $('<div><div class="form-group"><label class="col-sm-4 control-label"></label><div class="col-sm-8"><select class="form-control p_scnt chosen-select" id="p_scnt_' + i + '"  name="p_scnt[' + i + ']">' + echo + '</select><input type="text" class="form-control ujs" style="display: none" readonly="" id="ujs_' + i + '" name="ujs[' + i + ']" /><input type="text" class="form-control sales" style="display: none" readonly="" id="sales_' + i + '" name="sales[' + i + ']" /><a href="#" class="remScnt" id="remScnt">Remove</a></div></div></div>').appendTo(scntDiv);

                    $('#p_scnt_' + i + '').change(function() {
                        var ini = $('#p_scnt_' + i + ' option:selected').val();
                        $.ajax({
                            url: '<?php echo base_url(); ?>customer/getCust',
                            type: 'POST',
                            data: {
                                test: ini,
                                setting: 'kik'
                            },
                            success: function(echo) {
                                $('#ujs_' + i + '').val(echo);
                                var totalPoints = 0;
                                $('.nomine').each(function() {
                                    var inputVal = $(this).val();
                                    if ($.isNumeric(inputVal)) {
                                        totalPoints += parseFloat(inputVal);
                                    }
                                });

                                var totalPoints2 = 0;
                                $('.ujs').each(function() {
                                    var inputVal2 = $(this).val();
                                    if ($.isNumeric(inputVal2)) {
                                        totalPoints2 += parseFloat(inputVal2);
                                    }
                                });

                                $('#allowance2').val(new Intl.NumberFormat('en-ID').format(totalPoints + totalPoints2));
                            }
                        });

                        $.ajax({
                            url: '<?php echo base_url(); ?>customer/getCust',
                            type: 'POST',
                            data: {
                                test: ini,
                                setting: 'ki'
                            },
                            success: function(echo) {
                                $('#sales_' + i + '').val(echo);
                                var totalPoints = 0;
                                var totalPoint = 0;
                                $('.sales').each(function() {
                                    var inputVal = $(this).val();
                                    if ($.isNumeric(inputVal)) {
                                        totalPoints += parseFloat(inputVal);
                                    }
                                });
                                $('#addSales').val(totalPoints);

                                $('.jssales').each(function() {
                                    var inputVa = $(this).val();
                                    if ($.isNumeric(inputVa)) {
                                        totalPoint += parseFloat(inputVa);
                                    }
                                });
                                $('#price').val(totalPoint);
                            }
                        });
                    });

                }
            });
            i++;
            return false;
        });

        $('#p_scents').on('click', '.remScnt', function() {
            $(this).parent().parent().remove();



            var totalPoints = 0;
            $('.nomine').each(function() {
                var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                }
            });

            var totalPoints2 = 0;
            $('.ujs').each(function() {
                var inputVal2 = $(this).val();
                if ($.isNumeric(inputVal2)) {
                    totalPoints2 += parseFloat(inputVal2);
                }
            });
            $('#allowance2').val(new Intl.NumberFormat('en-ID').format(totalPoints + totalPoints2));

            var totalSales = 0;
            $('.sales').each(function() {
                var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                    totalSales += parseFloat(inputVal);
                }
            });
            $('#addSales').val(totalSales);

            var totalSale = 0;
            $('.jssales').each(function() {
                var inputVa = $(this).val();
                if ($.isNumeric(inputVa)) {
                    totalSale += parseFloat(inputVa);
                }
            });
            $('#price').val(totalSale);

            i--;
            return false;
        });

        var tempatCost = $('#formCost');
        var div2 = document.getElementById("formCost");
        var nodelist2 = div2.getElementsByClassName("coa1");
        var j = nodelist2.length - 1;

        $('#addCost').on('click', '.Cost', function() {
            $.ajax({
                url: '<?php echo base_url(); ?>customer/getCust',
                type: 'POST',
                data: 'setting=cccc',
                success: function(echo) {
                    $('<div class="form-group"><label class="col-sm-1 control-label ">Name</label><div class="col-sm-3"><select class="form-control col-md-4 coa1 chosen-select" id="coa1_' + j + '"  name="coa1[' + j + ']">' + echo + '</select></div><label class="col-sm-1 control-label ">Description</label><div class="col-sm-3"><input type="text" class="form-control desc" id="desc_' + j + '" name="desc[' + j + ']" /></div><label class="col-sm-1 control-label ">Nominal</label><div class="col-sm-2"><input type="text" class="form-control nomine" id="nomine_' + j + '" name="nomine[' + j + ']" data-type="currency" /><input type="text" style="display:none"  class="form-control noine" id="noine_' + j + '" name="noine[' + j + ']" /></div> <button class="btn btn-danger btn-circle removeadd" id="removeadd" type="button"><i class="fa fa-minus"></i></button></div>').appendTo(tempatCost);

                    $('#costadd_' + j + '').change(function() {
                        var ini = $('#costadd_' + j + ' option:selected').val();
                        $.ajax({
                            url: '<?php echo base_url(); ?>additional/getAkun',
                            type: 'POST',
                            data: {
                                id: ini
                            },
                            success: function(ech) {
                                $('#noine_' + j + '').val(ech);
                            }
                        });
                    });

                    $(".nomine").keyup(function() {
                        var n = parseInt($(this).val().replace(/\D/g, ''), 10);
                        $(this).val(n.toLocaleString());
                    });

                }
            });
            j++;
            return false;
        });

        $('#formCost').on('click', '.removeadd', function() {
            $(this).parent().remove();
            var totalPoints = 0;
            $('.nomine').each(function() {
                var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                }
            });
            var totalPoints2 = 0;
            $('.ujs').each(function() {
                var inputVal2 = $(this).val();
                if ($.isNumeric(inputVal2)) {
                    totalPoints2 += parseFloat(inputVal2);
                }
            });
            $('#allowance2').val(new Intl.NumberFormat('en-ID').format(totalPoints + totalPoints2));
            j--;
            return false;
        });



        $(".2, .3, .4, .5, .6, .7, .8").hide();

        $("#loadqty").keyup(function() {
            var route = $(".id_routes option:selected").val();

            var unit = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('order/getPriceUnit'); ?>",
                dataType: "html",
                data: {
                    routes: route
                },
                success: function(data) {
                    $("#priceunit").val(data);
                    var total = parseInt(unit) * parseInt(data);
                    $("#price").val(total);
                },
            });
            if (unit == 1) {
                $(".1").show();
                $(".2, .3, .4, .5, .6, .7, .8").hide();

            } else if (unit == 2) {
                $(".1, .2").show();
                $(".3, .4, .5, .6, .7, .8").hide();
            } else if (unit == 3) {
                $(".1, .2, .3").show();
                $(".4, .5, .6, .7, .8").hide();
            } else if (unit == 4) {
                $(".1, .2, .3, .4").show();
                $(".5, .6, .7, .8").hide();
            } else if (unit == 5) {
                $(".1, .2, .3,.4, .5").show();
                $(".6, .7, .8").hide();
            } else if (unit == 6) {
                $(".1, .2, .3, .4, .5, .6").show();
                $(".7, .8").hide();
            } else if (unit == 7) {
                $(".1, .2, .3,.4, .5, .6, .7").show();
                $(".8").hide();
            } else {
                $(".1, .2, .3, .4, .5, .6, .7, .8").show();
            }
        });

        $(".11, .22, .33, .44, .55, .66, .77, .88").hide();

        $("#loadqty2").change(function() {
            var unit = $(this).val();
            var origin = $(".id_citieso2 option:selected").val();
            var dest = $(".id_citiesd2 option:selected").val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('order/getPriceRoutes'); ?>",
                data: {
                    citieso: origin,
                    citiesd: dest,
                    sett: 'bbb'
                },
                success: function(data) {
                    var perkalian = parseInt(unit) * parseInt(data);
                    $("#pricelocation").val(perkalian);
                },
            });
            if (unit == 1) {
                $(".11").show();
                $(".22, .33, .44, .55, .66, .77, .88").hide();
            } else if (unit == 2) {
                $(".11, .22").show();
                $(".33, .44, .55, .66, .77, .88").hide();
            } else if (unit == 3) {
                $(".11, .22, .33").show();
                $(".44, .55, .66, .77, .88").hide();
            } else if (unit == 4) {
                $(".11, .22, .33, .44").show();
                $(".55, .66, .77, .88").hide();
            } else if (unit == 5) {
                $(".11, .22, .33,.44, .55").show();
                $(".66, .77, .88").hide();
            } else if (unit == 6) {
                $(".11, .22, .33, .44, .55, .66").show();
                $(".77, .88").hide();
            } else if (unit == 7) {
                $(".11, .22, .33, .44, .55, .66, .77,").show();
                $(".88").hide();
            } else {
                $(".11, .22, .33, .44, .55, .66, .77, .88").show();
            }
        });


        var tempatRoute = $('#formRoute');
        var getId = document.getElementById("formRoute");
        var getClass = getId.getElementsByClassName("id_citieso");
        var noRoute = getClass.length - 1;

        $('#addRoute').on('click', '.route', function() {
            var ini = $('#id_customers option:selected').text();
            var itu = $('#id_customers option:selected').val();

            $.ajax({
                url: '<?php echo base_url(); ?>order/getFleetType',
                success: function(echo) {
                    $('<div class="uhuy"><div class="form-group"><label class="col-sm-4 control-label">Tipe Order</label><div class="col-sm-8"><select class="form-control fleettype" id="fleettype_' + noRoute + '" name="fleettype[' + noRoute + ']"><option value=""></option>' + echo + '</select></div></div><div class="form-group"><label class="col-sm-4 control-label">Pabrik</label><div class="col-sm-8"><select class="form-control id_citieso" id="id_citieso_' + noRoute + '" name="id_citieso[' + noRoute + ']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label">Tujuan</label><div class="col-sm-8"><select class="form-control id_citiesd" id="id_citiesd_' + noRoute + '" name="id_citiesd[' + noRoute + ']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label ">Load Qty</label><div class="col-sm-8"><input type="text" class="form-control loadqty" id="loadqty_' + noRoute + '"></div></div><div class="form-group"><label class="col-sm-4 control-label">Route (UJS)</label><div class="col-sm-8"><select class="form-control id_routes" id="id_routes_' + noRoute + '" name="id_routes[' + noRoute + ']"><option value=""></option></select></div></div><div class="form-group"><label class="col-sm-4 control-label">Price Unit</label><div class="col-sm-8"><input type="text" class="form-control jsunit" style="display:none" name="jsunit[' + noRoute + ']" id="jsunit_' + noRoute + '"><input type="text" class="form-control tabsupir" style="display:none" name="tabsupir[' + noRoute + ']" id="tabsupir_' + noRoute + '"><input type="text" class="form-control komsupir" style="display:none" name="komsupir[' + noRoute + ']" id="komsupir_' + noRoute + '"><input type="text" class="jsqty" style="display:none" name="jsqty[' + noRoute + ']" id="jsqty_' + noRoute + '"><input type="text" class="jssales" style="display:none"  name="jssales[' + noRoute + ']" id="jssales_' + noRoute + '"><button class="btn btn-danger btn-xs remRoute" id="remRoute_' + noRoute + '" type="button"><i class="fa fa-minus"></i>  Remove</button></div></div></div>').appendTo(tempatRoute);
                    $('.id_citieso').chosen();
                    $('.id_citiesd').chosen();
                    $('.fleettype').chosen();

                    $('#formRoute').on('change', '.id_routes', function() {
                        var id = $(this).attr('id');
                        var isi = $(this).val();
                        var pecah = id.split("_");
                        var totalPoints = 0;
                        var unit = $('#jsqty_' + pecah['2'] + '').val();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('order/getPricebyRoutes'); ?>",
                            data: {
                                route: isi,
                                sett: 'ujs'
                            },
                            success: function(dat) {
                                if (pecah['2'] == 0) {
                                    $('#allowance').val(new Intl.NumberFormat('en-ID').format(dat));
                                }
                            },
                        });

                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('order/getPricebyRoutes'); ?>",
                            data: {
                                route: isi,
                                sett: 'unit'
                            },
                            success: function(data) {
                                var totaljssales = unit * data;

                                $('#jsunit_' + pecah['2'] + '').val(data);
                                if (pecah['2'] == 0) {
                                    $('#priceunit').val(new Intl.NumberFormat('en-ID').format(data));
                                }

                                $('#jssales_' + pecah['2'] + '').val(totaljssales);

                                $('.jssales').each(function() {
                                    var inputVal = $(this).val();
                                    if ($.isNumeric(inputVal)) {
                                        totalPoints += parseFloat(inputVal);
                                    }
                                });
                                $('#price').val(new Intl.NumberFormat('en-ID').format(totalPoints));
                            },
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('order/getPricebyRoutes'); ?>",
                            data: {
                                route: isi,
                                sett: 'saving'
                            },
                            success: function(data) {

                                $('#tabsupir_' + pecah['2'] + '').val(data);
                            },
                        });
                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('order/getPricebyRoutes'); ?>",
                            data: {
                                route: isi,
                                sett: 'wash'
                            },
                            success: function(data) {

                                $('#komsupir_' + pecah['2'] + '').val(data);
                            },
                        });
                    });

                    $('#formRoute').on('change', '.fleettype', function() {
                        var id = $(this).attr('id');
                        var pecah = id.split("_");
                        var itu = $('#id_customers option:selected').val();
                        var types = $('#' + id + ' option:selected').val();

                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('order/getOrigin'); ?>",
                            dataType: "html",
                            data: {
                                tipes: types,
                                cust: itu
                            },
                            success: function(data) {
                                $('#id_citieso_' + pecah['1'] + '').html(data);
                                $('#id_citieso_' + pecah['1'] + '').trigger("chosen:updated");
                            },
                        });
                    });

                    $('#formRoute').on('change', '.id_citieso', function() {
                        var id = $(this).attr('id');
                        var pecah = id.split("_");
                        var itu = $('#id_customers option:selected').val();
                        var origin = $('#' + id + ' option:selected').val();
                        var types = $('#fleettype_' + pecah['2'] + ' option:selected').val();
                        var dest = $('#id_citiesd_' + pecah['2'] + ' option:selected').val();

                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('order/getDestination'); ?>",
                            dataType: "html",
                            data: {
                                citieso: origin,
                                tipes: types,
                                cust: itu
                            },
                            success: function(data) {
                                $('#id_citiesd_' + pecah['2'] + '').html(data);
                                $('#id_citiesd_' + pecah['2'] + '').trigger("chosen:updated");
                            },
                        });

                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('order/getPriceRoutes'); ?>",
                            dataType: "html",
                            data: {
                                citieso: origin,
                                tipes: types,
                                citiesd: dest,
                                sett: 'option',
                                cust: itu

                            },
                            success: function(data) {
                                $('#id_routes_' + pecah['2'] + '').html(data);
                                $('#id_routes_' + pecah['2'] + '').trigger("chosen:updated");
                            },
                        });
                    });

                    $('#formRoute').on('change', '.id_citiesd', function() {
                        var id = $(this).attr('id');
                        var pecah = id.split("_");
                        var origin = $('#id_citieso_' + pecah['2'] + ' option:selected').val();
                        var dest = $('#' + id + ' option:selected').val();
                        var totalPoints = 0;
                        var types = $('#fleettype_' + pecah['2'] + ' option:selected').val();
                        var unit = $('#jsqty_' + pecah['2'] + '').val();

                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('order/getPriceRoutes'); ?>",
                            dataType: "html",
                            data: {
                                citieso: origin,
                                tipes: types,
                                citiesd: dest,
                                sett: 'option',
                                cust: itu
                            },
                            success: function(data) {
                                $('#id_routes_' + pecah['2'] + '').html(data);
                                $('#id_routes_' + pecah['2'] + '').trigger("chosen:updated");

                            },
                        });
                    });

                    $('#formRoute').on('keyup', '.loadqty', function() {
                        var id = $(this).attr('id');
                        var pecah = id.split("_");
                        var route = $('#id_routes_' + pecah['1'] + ' option:selected').val();
                        var unit = $(this).val();
                        var unittxt = $('#' + id + '').val();
                        var jsunit = $('#jsunit_' + pecah['1'] + '').val();
                        var jsqty = $('#jsqty_' + pecah['1'] + '').val();
                        var jsqtytxt = $('.txtqty').val();
                        var totaljssales = parseFloat(unit) * parseFloat(jsunit);
                        var totalPoints = 0;
                        var totalQty = 0;


                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('order/getPricebyRoutes'); ?>",
                            data: {
                                route: route,
                                sett: 'unit'
                            },
                            success: function(data) {
                                var totaljssales = data * unit;

                                $('#jsunit_' + pecah['2'] + '').val(data);
                                if (pecah['2'] == 0) {
                                    $('#priceunit').val(data);
                                }

                                $('#jssales_' + pecah['2'] + '').val(totaljssales);

                                $('.jssales').each(function() {
                                    var inputVal = $(this).val();
                                    if ($.isNumeric(inputVal)) {
                                        totalPoints += parseFloat(inputVal);
                                    }
                                });
                                $('#price').val(totalPoints);

                                $('.jsqty').each(function() {
                                    var inputQty = $(this).val();
                                    if ($.isNumeric(inputQty)) {
                                        totalQty += parseFloat(inputQty);
                                    }
                                });
                                $('#totalQty').val(totalQty);

                                var loadloop = 0;
                                var tempatLoad = $('#testqty');

                                function addAppend() {
                                    $('#testqty').append(formAppend);
                                }
                                //alert(unittxt);
                                //for (loadloop = 0; loadloop < unittxt; loadloop++){
                                $('#testqty').append('<div class="form-group test_' + pecah['1'] + '_0"><div class="col-sm-6"><input type="text" class="form-control" id="deliveryno_' + pecah['1'] + '_0" placeholder="No Seal" name="deliveryno[0]" /></div><div class="col-sm-6"><input type="text" class="form-control" id="frameno_' + pecah['1'] + '_0" placeholder="No Kontainer"  name="frameno[0]" /></div></div>');

                                //}


                            },
                        });
                        $('#jssales_' + pecah['1'] + '').val(totaljssales);
                        $('#jsqty_' + pecah['1'] + '').val(unit);
                        $('.txtqty').val(unittxt);



                    });


                }
            });


            noRoute++;
            return false;
        });

        $('#formRoute').on('click', '.remRoute', function() {
            var id = $(this).attr('id');
            var pecah = id.split("_");
            var totalQty = 0;
            var totalPoints = 0;
            var loadloop = 0;
            var jsqty = $('#jsqty_' + pecah['1'] + '').val();
            if (pecah['1'] == '0') {
                $('#priceunit').val("0");
                $('#allowance').val("0");
            }
            $(this).parent().parent().parent().remove();

            for (loadloop = 0; loadloop < jsqty; loadloop++) {
                $('.test_' + pecah['1'] + '_0').remove();
            }

            $('.id_routes').empty();



            $('.jssales').each(function() {
                var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                }
            });

            $('#price').val(totalPoints);


            $('.jsqty').each(function() {
                var inputQty = $(this).val();
                if ($.isNumeric(inputQty)) {
                    totalQty += parseFloat(inputQty);
                }
            });

            $('#totalQty').val(totalQty);

            noRoute--;
            return false;
        });

        $('#id_customers').change(function() {
            var countJsunit = $('.uhuy');
            var countRowQty = $('.slebew');
            var totalPoints = 0;
            $('.uhuy').remove();
            $('.slebew').remove();

            $('.jssales').each(function() {
                var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {
                    totalPoints += parseFloat(inputVal);
                }
            });

            $('#price').val(totalPoints);

            noRoute = -1;
            return false;
        });



        $(".id_citieso2").change(function() {
            var origin = $(".id_citieso2 option:selected").val();
            var dest = $(".id_citiesd2 option:selected").val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('order/getPriceRoutes'); ?>",
                data: {
                    citieso: origin,
                    citiesd: dest,
                    sett: 'bbb'
                },
                success: function(data) {
                    var unit = $("#loadqty2").val();
                    var perkalian = parseInt(unit) * parseInt(data);
                    $("#pricelocation").val(perkalian);

                },
            });
        });


        $(".id_citiesd2").change(function() {
            var origin = $(".id_citieso2 option:selected").val();
            var dest = $(".id_citiesd2 option:selected").val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('order/getPriceRoutes'); ?>",
                data: {
                    citieso: origin,
                    citiesd: dest,
                    sett: 'bbb'
                },
                success: function(data) {
                    var unit = $("#loadqty2").val();
                    var perkalian = parseInt(unit) * parseInt(data);
                    $("#pricelocation").val(perkalian);
                },
            });
        });








        $('#orderdate').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#orderdate').change(function() {
            var dates = $(this).val();

            $.ajax({
                type: 'POST',
                url: '<?= base_url() ?>driver/getDriversAbsensi',
                data: {
                    date: dates,
                },
                success: function(data) {
                    $('#id_drivers').html(data);
                    $('#id_drivers').trigger("chosen:updated");
                }
            });
        });

        $('#jam').clockpicker();

        $('div.ibox-content').slideUp();

        $('.ibox-tools a.collapse-link i').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');

        $(".detail").click(function() {

        });

        $('#listSPB').DataTable({
            autoWidth: true,
            ordering: false

        });

        $('.chosen-select').chosen({
            width: "100%"
        });


        $("#listSPB").on("click", ".edist", function(event) {
            var idcus = $(this).attr('value');
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('order/modaleditspld'); ?>",
                dataType: "html",
                data: {
                    id: idcus
                },
                success: function(data) {
                    $("#modaledit").html(data);
                    $("#modaledit").modal();


                },
            });
        });

        $("#listSPB").on("click", ".del", function(event) {
            if (confirm("Do you want to delete")) {
                var idcus = $(this).attr('value');
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('order/deleteOrder'); ?>",
                    dataType: "html",
                    data: {
                        id: idcus
                    },
                    success: function() {
                        location.reload();
                    },
                });
            }
        });

        $("#listSPB").on("click", ".detail", function(event) {
            var idcus = $(this).attr('value');
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('order/modaldetail'); ?>",
                dataType: "html",
                data: {
                    id: idcus
                },
                success: function(data) {
                    $("#modaledit").html(data);
                    $("#modaledit").modal();

                },
            });
        });



    });
</script>