<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | Trucks </span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Orders</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Master Data
            </li>
            <li class="active">
                <strong>Orders</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Orders Management</h5>
                    <div class="ibox-tools">
                       
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>    
                </div>
                
                <div class="ibox-content">
                    <form class="form-horizontal" id="formspb" action="<?php echo site_url('order/saveOrders')?>" method="POST"   >
                        <div class="row">
                    <!-- INFORMASI ORDER -->
                    <div class="col-md-6 col-lg-6 col-xs-12">
                    
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Order Code</label>
                                <div class="col-sm-8">
                                    <?php
                                    if (empty($id))
                                    {
                                        $ccode = "1904000001";
                                    } else {
                                        $key= $id[0]['code'];
                                        $pattern = "/(\d+)/";
                                        $arra = preg_split($pattern, $key, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                                        $ccode = $arra[1]+1;
                                        $tambah = $this->session->userdata('id');
                                        date_default_timezone_set("Asia/Bangkok");
                                        $nowdate = date('ymdGis');
                                    }
                                    date_default_timezone_set("Asia/Bangkok");
                                    $jam = date("H:i:s");
                                    ?>
                                    <input type="text" class="form-control" id="code" name="code" value="OC<?=$tambah?><?=$nowdate?>" readonly=""/>
                                </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 col-xs-12 control-label">Order Date</label>
                            <div class="col-sm-8 col-xs-12">
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" id="orderdate" style="width: 50%;" name="orderdate" readonly=""/>
                                        <div class="input-group bootstrap-timepicker timepicker" style="width: 50%;">
                                            <input readonly="" value="<?=$jam?>" name="orderdatetime" type="text" class="form-control input-small"/>
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-time"></i>
                                            </span>
                                        </div>
                                </div>  
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Customer Name</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                          
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Description</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="description" name="description" rows="2"></textarea>
                            
                            </div>
                        </div>
                        
                        <div class="form-group">

                            <label class="col-sm-12"><h3 class="text-warning">Fleet Information</h3></label>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Fleet (Armada)</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_fleets" name="id_fleets" >
                                    <option value=""></option>
                                    <?php

                                        foreach ($fleet as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['fleetplateno']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Driver 1</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_drivers" name="id_drivers" >
                                    <option value=""></option>
                                    <?php

                                        foreach ($driver as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Driver 2</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_drivers2" name="id_drivers2" >
                                    <option value=""></option>
                                    <?php

                                        foreach ($driver as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 col-lg-6 col-xs-12">
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Shippment No.</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="id_shippment" name="id_shippment">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Origin</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select id_citieso" id="id_citieso" name="id_citieso">
                                    <option value=""></option>
                                    
                                    <?php

                                        foreach ($cities as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Destination</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select id_citiesd" id="id_citiesd" name="id_citiesd">
                                    <option value=""></option>
                                    <?php

                                        foreach ($cities as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                     <div class="form-group">

                            <label class="col-sm-12"><h3 class="text-warning"></h3></label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Order Type</label>
                            <div class="col-sm-8">
                                <input type="text" style="display: none" class="form-control id_ordertypes" readonly="" id="id_ordertypes" name="id_ordertypes" />
                                <input type="text" class="form-control" readonly="" id="textfleet" />

                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label class="col-sm-4 control-label ">Load Qty</label>
                            <div class="col-sm-8">
                                <select class="form-control loadqty" id="loadqty" name="loadqty">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                </select>
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Route (UJS)</label>
                            <div class="col-sm-8">
                                <select class="form-control id_routes" id="id_routes" name="id_routes">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Allowance (UJS)</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control allowance" readonly="" id="allowance" name="allowance" />
                            <input type="text" style="display: none" class="form-control wash" readonly="" id="wash" name="wash" />
                          </div>
                        </div>


                        <div class="form-group">
                          <label class="col-sm-4 control-label">Unit Price</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control unitprice" readonly="" id="unitprice" name="unitprice" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-4 control-label">Sales</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control price" readonly="" id="price" name="price" />
                          </div>
                        </div>

                     	<div class="form-group">

                            <label class="col-sm-12"><h2 class="text-warning"></h2></label>
                        </div>
                    
                    </div>

                    <div class="form-group" >
	                  <div class="col-sm-12" id="addCost">
	                    <label class="col-sm-2">
	                    	<h3 class="text-warning">Additional Cost</h3> 

		                    
	                	</label>
	                	<div class="col-sm-8">
	                	<button class="btn btn-danger btn-circle Cost" id="Cost" type="button">
		                        <i class="fa fa-plus"></i>
		                    </button>
		                </div>
	                  </div>
	                </div>

	                <div id="formCost">


		                
		            </div>

                        <div class="form-group 1">
                            <label class="col-sm-1 control-label">Frame No</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="frameno[0]" name="frameno[0]" />
                            </div>

                            <label class="col-sm-1 control-label">Machine No</label>
                            <div class="col-sm-2">
                               <input type="text" class="form-control" id="machineno[0]" name="machineno[0]" />
                            </div>

                            <label class="col-sm-1 control-label">Color</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="color[0]" name="color[0]" />
                            </div>

                            <label class="col-sm-1 control-label">Type</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="type[0]" name="type[0]" />
                            </div>
                        </div>

                        <div class="form-group 2">
                            <label class="col-sm-1 control-label">Frame No</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="frameno[1]" name="frameno[1]" />
                            </div>

                            <label class="col-sm-1 control-label">Machine No</label>
                            <div class="col-sm-2">
                               <input type="text" class="form-control" id="machineno[1]" name="machineno[1]" />
                            </div>

                            <label class="col-sm-1 control-label">Color</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="color[1]" name="color[1]" />
                            </div>

                            <label class="col-sm-1 control-label">Type</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="type[1]" name="type[1]" />
                            </div>
                        </div>

                       
                        <div class="form-group 3">
                            <label class="col-sm-1 control-label">Frame No</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="frameno[2]" name="frameno[2]" />
                            </div>

                            <label class="col-sm-1 control-label">Machine No</label>
                            <div class="col-sm-2">
                               <input type="text" class="form-control" id="machineno[2]" name="machineno[2]" />
                            </div>

                            <label class="col-sm-1 control-label">Color</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="color[2]" name="color[2]" />
                            </div>

                            <label class="col-sm-1 control-label">Type</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="type[2]" name="type[2]" />
                            </div>
                        </div>

                        <div class="form-group 4">
                            <label class="col-sm-1 control-label">Frame No</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="frameno[3]" name="frameno[3]" />
                            </div>

                            <label class="col-sm-1 control-label">Machine No</label>
                            <div class="col-sm-2">
                               <input type="text" class="form-control" id="machineno[3]" name="machineno[3]" />
                            </div>

                            <label class="col-sm-1 control-label">Color</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="color[3]" name="color[3]" />
                            </div>

                            <label class="col-sm-1 control-label">Type</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="type[3]" name="type[3]" />
                            </div>
                        </div>

                        <div class="form-group 5">
                            <label class="col-sm-1 control-label">Frame No</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="frameno[4]" name="frameno[4]" />
                            </div>

                            <label class="col-sm-1 control-label">Machine No</label>
                            <div class="col-sm-2">
                               <input type="text" class="form-control" id="machineno[4]" name="machineno[4]" />
                            </div>

                            <label class="col-sm-1 control-label">Color</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="color[4]" name="color[4]" />
                            </div>

                            <label class="col-sm-1 control-label">Type</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="type[4]" name="type[4]" />
                            </div>
                        </div>

                        
                        <div class="form-group 6">
                            <label class="col-sm-1 control-label">Frame No</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="frameno[5]" name="frameno[5]" />
                            </div>

                            <label class="col-sm-1 control-label">Machine No</label>
                            <div class="col-sm-2">
                               <input type="text" class="form-control" id="machineno[5]" name="machineno[5]" />
                            </div>

                            <label class="col-sm-1 control-label">Color</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="color[5]" name="color[5]" />
                            </div>

                            <label class="col-sm-1 control-label">Type</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="type[5]" name="type[5]" />
                            </div>
                        </div>

                        
                        <div class="form-group 7">
                            <label class="col-sm-1 control-label">Frame No</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="frameno[6]" name="frameno[6]" />
                            </div>

                            <label class="col-sm-1 control-label">Machine No</label>
                            <div class="col-sm-2">
                               <input type="text" class="form-control" id="machineno[6]" name="machineno[6]" />
                            </div>

                            <label class="col-sm-1 control-label">Color</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="color[6]" name="color[6]" />
                            </div>

                            <label class="col-sm-1 control-label">Type</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="type[6]" name="type[6]" />
                            </div>
                        </div>

                        <div class="form-group 8">
                            <label class="col-sm-1 control-label">Frame No</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="frameno[7]" name="frameno[7]" />
                            </div>

                            <label class="col-sm-1 control-label">Machine No</label>
                            <div class="col-sm-2">
                               <input type="text" class="form-control" id="machineno[7]" name="machineno[7]" />
                            </div>

                            <label class="col-sm-1 control-label">Color</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="color[7]" name="color[7]" />
                            </div>

                            <label class="col-sm-1 control-label">Type</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="type[7]" name="type[7]" />
                            </div>
                        </div>                
                </div>
                
                <div class="form-group">
                  <div class="col-sm-12 text-right">
                    <button type="submit" class="btn btn-primary">Save!</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                  </div>
                </div>
                </form>
            </div>

                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB"  >
                            <thead>
                                <tr>
                                    <th style="width: 10%">Action #</th>
                                    <th>No #</th>
                                    <th>Order No.</th>
                                    <th style="width: 10%">Order Date</th>
                                    <th>Customer Name</th>
                                    <th>Origin</th>
                                    <th>Destination</th>
                                    <th style="width: 10%">Fleet</th>
                                    <th>Driver</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                    foreach ($order as $row) {
                                ?>
                                <tr>
                                    <td>
                                        <a class="detail btn btn-success btn-xs detail" value="<?=$row['id']?>"> 
                                            <i class="fa fa-book"></i> 
                                        </a>
                                        <a class="btn btn-primary btn-xs edist" value="<?=$row['id']?>"> 
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a class="btn btn-danger btn-xs" href="<?php echo site_url('order/deleteOrder')?>?id=<?=$row['id']?>" onclick="return confirm('Are you sure?');">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        &nbsp;
                                    </td>
                                    <td><?=$no?></td>
                                    <td><?=$row['code']?></td>
                                    <td><?=$row['orderdate']?></td>
                                    <td><?=$row['cname']?></td>
                                    <td><?=$row['n1']?></td>
                                    <td><?=$row['c2']?></td>
                                    <td><?=$row['fleet']?></td>
                                    <td><?=$row['driver']?></td>
                                    
                                </tr>
                                <?php 
                                $no++;
                                }
                                
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>



        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>

       <script>
        $(document).ready(function(){

        	var tempatCost = $('#formCost');
	       
	        var j = $('#addCost').length;

	        $('#addCost').on('click', '.Cost',function() {
	                $.ajax({
		                url:'<?php echo base_url();?>customer/getCust',
		                type:'POST',
		                data:'setting=cccc',
		                success: function(echo){
		                    //$('#p_scnt').html(echo);
		                    $('<div class="form-group"><label class="col-sm-1 control-label ">Name</label><div class="col-sm-3"><select class="form-control col-md-4 costadd chosen-select" id="costadd_'+ j +'"  name="costadd['+ j +']">'+ echo +'</select></div><label class="col-sm-1 control-label ">Description</label><div class="col-sm-3"><input type="text" class="form-control desc" id="desc_'+ j +'" name="desc['+ j +']" /></div><label class="col-sm-1 control-label ">Nominal</label><div class="col-sm-2"><input type="text" class="form-control nomine" id="nomine_'+ j +'" name="nomine['+ j +']" /></div> <button class="btn btn-danger btn-circle removeadd" id="removeadd" type="button"><i class="fa fa-minus"></i></button></div>').appendTo(tempatCost);
		                    }
		                });
	                j++;
	            return false;
	        });
	        
	        $('#formCost').on('click','.removeadd', function() { 
	               $(this).parent().remove();
	               	//alert("bisa kok");
	               j--;    
	               return false;
	        });
        	
            $(".1, .2, .3, .4, .5, .6, .7, .8").hide();

            $("#loadqty").change(function(){
                var unit = $(this).val();
                if (unit == 1) {
                    $(".1").show();
                    $(".2, .3, .4, .5, .6, .7, .8").hide();

                } else if (unit == 2) {
                    $(".1, .2").show();
                    $(".3, .4, .5, .6, .7, .8").hide();
                } else if (unit == 3) {
                    $(".1, .2, .3").show();
                    $(".4, .5, .6, .7, .8").hide();
                }else if (unit == 4) {
                    $(".1, .2, .3, .4").show();
                    $(".5, .6, .7, .8").hide();
                }else if (unit == 5) {
                    $(".1, .2, .3,.4, .5").show();
                    $(".6, .7, .8").hide();
                }else if (unit == 6) {
                    $(".1, .2, .3, .4, .5, .6").show();
                    $(".7, .8").hide();
                }else if (unit == 7) {
                    $(".1, .2, .3,.4, .5, .6, .7").show();
                    $(".8").hide();
                } else {
                    $(".1, .2, .3, .4, .5, .6, .7, .8").show();
                }
            });

            $(".id_citieso").change(function(){
                var origin=$(".id_citieso option:selected").val();
                var dest=$(".id_citiesd option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_routes").html(data);
                        //alert(data);
                },
                });
            });

            $("#id_fleets").change(function(){
                var id=$(this).val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getFleetType');?>",
                    dataType: "html",
                    data: {
                        id:id,
                        sett:'id'
                        },
                    success:function(data){
                        $("#id_ordertypes").val(data);
                        //alert(data);
                },
                });
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getFleetType');?>",
                    dataType: "html",
                    data: {
                        id:id,
                        sett:'nama'
                        },
                    success:function(data){
                        $("#textfleet").val(data);
                        //alert(data);
                },
                });
            });

            $(".id_citiesd").change(function(){
                var origin=$(".id_citieso option:selected").val();
                var dest=$(".id_citiesd option:selected").val();
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceRoutes');?>",
                    dataType: "html",
                    data: {
                        citieso:origin,
                        citiesd:dest
                        },
                    success:function(data){
                        $("#id_routes").html(data);
                        //alert(data);
                },
                });
            });

           

            $(".id_routes").change(function(){
                var route=$(".id_routes option:selected").val();
                //alert(route);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceAllowance');?>",
                    dataType: "html",
                    data: {
                        routes:route
                        },
                    success:function(data){
                        
						$("#allowance").val(data);
                       // alert(harga);
                },
                });

                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceUnit');?>",
                    dataType: "html",
                    data: {
                        routes:route
                        },
                    success:function(data){
                        
                        var qty = parseInt($('#loadqty').val());
                        var harga = (data);     
                        var total = qty * harga;
                        $("#price").val(total);
                        $("#unitprice").val(data);
                       // alert(harga);
                },
                });
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/getPriceWash');?>",
                    dataType: "html",
                    data: {
                        routes:route
                        },
                    success:function(data){
                        
                        var qty = parseInt($('#loadqty').val());
                        var harga = (data);     
                        var total = qty * harga;
                        $("#price").val(total);
                       // alert(harga);
                },
                });
            });


            $('#orderdate').datepicker({format:'yyyy-mm-dd'});

            $('#jam').clockpicker();

            $('div.ibox-content').slideUp();

//change the chevron
            $('.ibox-tools a.collapse-link i').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');

            $(".detail").click(function(){
                
            });

            setTimeout(function() {
                toastr.options = {
                  closeButton: true,
                  debug: false,
                  progressBar: true,
                  preventDuplicates: false,
                  positionClass: "toast-top-right",
                  onclick: false,
                  showDuration: "1000",
                  hideDuration: "1000",
                  timeOut: "7000",
                  extendedTimeOut: "1000",
                  showEasing: "swing",
                  hideEasing: "linear",
                  showMethod: "fadeIn",
                  hideMethod: "fadeOut"
                };
                toastr.success('Fleet Management System', 'Welcome to FMS');

            }, 1300);
            $('#listSPB').DataTable({
                autoWidth: true,
                ordering : false

            });

            $('.chosen-select').chosen({width: "100%"});


            $("#listSPB").on("click", ".edist", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/modaleditspld');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });
			
			$("#listSPB").on("click", ".detail", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/modaldetail');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });

            

        });

    </script>