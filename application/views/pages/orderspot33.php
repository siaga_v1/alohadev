<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | Trucks </span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Spot Orders</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Master Data
            </li>
            <li class="active">
                <strong>Spot Orders</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Orders Management</h5>
                    <div class="ibox-tools">
                       
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>    
                </div>
                
                <div class="ibox-content">
                    <form class="form-horizontal" id="formspb" action="<?php echo site_url('order/saveSpot')?>" method="POST"   >
                        <div class="row">
                    <!-- INFORMASI ORDER -->
                    <div class="col-md-6 col-lg-6 col-xs-12">
                    
                        <div class="form-group" style="display: none">
                            <label class="col-sm-4 control-label">Order Code</label>
                                <div class="col-sm-8">
                                    <?php
                                    if (empty($id))
                                    {
                                        $ccode = "1904000001";
                                    } else {
                                        $key= $id[0]['code'];
                                        $pattern = "/(\d+)/";
                                        $arra = preg_split($pattern, $key, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                                        $ccode = $arra[1]+1;
                                        $tambah = $this->session->userdata('id');
                                        date_default_timezone_set("Asia/Bangkok");
                                        $nowdate = date('ymdGis');
                                    }
                                    date_default_timezone_set("Asia/Bangkok");
                                    $jam = date("H:i:s");
                                    ?>
                                    <input type="text" class="form-control" id="code" name="code" value="OC<?=$tambah?><?=$nowdate?>" readonly=""/>
                                </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">No JO.</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="id_shippment" name="id_shippment">
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label class="col-sm-4 control-label">Order Code</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="noVoucher" name="noVoucher" placeholder="No Voucher / Bukti" />
                                </div>
                        </div>
                        -->
                        <div class="form-group">
                            <label class="col-sm-4 col-xs-12 control-label">Order Date</label>
                            <div class="col-sm-8 col-xs-12">
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" id="orderdate" style="width: 50%;" name="orderdate" readonly=""/>
                                        <div class="input-group bootstrap-timepicker timepicker" style="width: 50%;">
                                            <input readonly="" value="<?=$jam?>" name="orderdatetime" type="text" class="form-control input-small"/>
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-time"></i>
                                            </span>
                                        </div>
                                </div>  
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Pengirim</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_customers" name="id_customers">
                                    <option value=""></option>
                                    <?php

                                        foreach ($customer as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Penerima</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="penerima" name="penerima">
                            </div>
                        </div>
                          
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Merk / Type</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="description" name="description" rows="2"></textarea>
                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Invoice A/N</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="invoicean" name="invoicean">
                            </div>
                        </div>
                    <!--
                        <div class="form-group">

                            <label class="col-sm-12"><h3 class="text-warning">Fleet Information</h3></label>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Fleet (Armada)</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_fleets" name="id_fleets" >
                                    <option value=""></option>
                                    <?php

                                        foreach ($fleet as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['fleetplateno']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Driver 1</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_drivers" name="id_drivers" >
                                    <option value=""></option>
                                    <?php

                                        foreach ($driver as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Driver 2</label>
                            <div class="col-sm-8">
                                <select class="form-control chosen-select" id="id_drivers2" name="id_drivers2" >
                                    <option value=""></option>
                                    <?php

                                        foreach ($driver as $row) {
                                            ?>

                                            <option value="<?=$row['id']?>"><?=$row['name']?></option>

                                            <?php
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                    -->
                    </div>
                    
                    <div class="col-md-6 col-lg-6 col-xs-12">
                        
                        

                        <div class="form-group" >
                            <div class="col-sm-12" id="addRoute">
                                <label class="col-sm-4 control-label">
                                    Route
                                </label>

                                <div class="col-sm-8">
                                    <button class="btn btn-danger btn-circle route" id="route" type="button">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                        <div id="formRoute">
                        </div>

                        <div class="form-group" style="display: none;">
                            <div class="col-sm-12" id="tees">
                                <label class="col-sm-4 control-label">
                                    Tambah Bongkaran
                                </label>

                                <div class="col-sm-8" >
                                	<button class="btn btn-danger btn-circle addScnt" id="addScnt" type="button">
                                		<i class="fa fa-plus"></i>
                                	</button>
                                	
                                 	<div >
                                 		
                                 	</div>
                                </div>
                            </div>
                        </div>

                        <div id="p_scents">
                        </div>
                       
                        <!--<div class="form-group">
                            <label class="col-sm-4 control-label">Order Type</label>
                            <div class="col-sm-8">
                                 <input type="text" style="display: none" class="form-control id_ordertypes" readonly="" id="id_ordertypes" name="id_ordertypes" />
                                <input type="text" class="form-control" readonly="" id="textfleet" />
                            </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-4 control-label">Price (Unit)</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control priceunit" readonly="" id="priceunit" name="priceunit" />
                          </div>
                        </div>-->

                        <div class="form-group" style="display: none;">
                          <label class="col-sm-4 control-label">Add Sales</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control addSales" readonly="" id="addSales" name="addSales" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-4 control-label">Total Sales</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control price" id="price" name="price" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-4 control-label">Allowance (UJS)</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control allowance" id="allowance" name="allowance" />
                            <input type="text" class="form-control pricelocation" readonly="" style="display: 	none" id="pricelocation" name="pricelocation" />
                            <input type="text" class="form-control wash" readonly="" style="display: 	none" id="wash" name="wash" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-4 control-label">Uang Titip</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="feesaving" name="feesaving" />
                          </div>
                        </div>

                        

                        <div class="form-group" style="display: none">
                          <label class="col-sm-4 control-label">Allowance Add (UJS)</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control allowance2" readonly="" id="allowance2" name="allowance2" />
                            <input type="text" class="form-control price2" style="display: 	none" readonly="" id="price2" name="price2" />
                            <input type="text" class="form-control totalqty" readonly="" style="display: 	none" id="totalQty" name="totalqty" />
                          </div>
                        </div>

                    	<div class="form-group">
							<label class="col-sm-12"><h2 class="text-warning"></h2></label>
                        </div>
                    
                    </div>

                                        
                </div>
                
                <div class="form-group">
                  <div class="col-sm-12" id="addCost">
                    <label class="col-sm-2">
                    	<h3 class="text-warning">Add Fleet</h3> 

	                    
                	</label>
                	<div class="col-sm-8">
                	<button class="btn btn-danger btn-circle Cost" id="Cost" type="button">
	                        <i class="fa fa-plus"></i>
	                    </button>
	                </div>
                  </div>
                </div>
                <div id="formCost">


	                
	            </div>
		       
                <div class="form-group">
                  <div class="col-sm-12 text-right">
                    <button type="submit" class="btn btn-primary">Save & Print</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                  </div>
                </div>

               


                </form>
            </div>

                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="listSPB"  >
                            <thead>
                                <tr>
                                    <th style="width: 15%">Action #</th>
                                    <th>Tgl SP</th>
                                    <th>Tanggal Pengiriman</th>
                                    <th>Customer Name</th>
                                    <th>No JO</th>
                                    <th style="width: 10%">Mobil</th>
                                    <th style="width: 10%">Unit</th>
                                    <th style="width: 10%">Fleet</th>
                                    <th>Driver</th>
                                    <th>Origin</th>
                                    <th>Destination</th>
                                    <th>Unit Price</th>
                                    <th>Sales</th>
                                    <th>UJS</th>
                                    <th>Uang Titip</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                    foreach ($order as $row) {
                                ?>
                                <tr>
                                    <td>
                                        <a class="detail btn btn-success btn-xs detail" value="<?=$row['id']?>"> 
                                            <i class="fa fa-book"></i> 
                                        </a>

                                        <a class="btn btn-success btn-xs" href="<?php echo site_url('order/printSpot')?>?id=<?=$row['id']?>" value="<?=$row['id']?>"> 
                                            <i class="fa fa-print"></i> 
                                        </a>

                                        <a class="btn btn-primary btn-xs edist" value="<?=$row['id']?>"> 
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a class="btn btn-danger btn-xs" href="<?php echo site_url('order/deleteOrderSpot')?>?id=<?=$row['id']?>" onclick="return confirm('Are you sure?');">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        &nbsp;
                                    </td>

                                    <td><?=$row['createdatetime']?></td>
                                    <td><?=$row['orderdate']?></td>
                                    <td><?=$row['cname']?></td>
                                    <td><?=$row['shippment']?></td>
                                    <td><?=$row['description']?></td>
                                    <td><?=$row['loadqty']?></td>
                                    <td><?=$row['fleet']?></td>
                                    <td><?=$row['driver']?></td>
                                    <td><?=$row['n1']?></td>
                                    <td><?=$row['c2']?></td>
                                    <td><?=$row['unitprice']?></td>
                                    <td><?=$row['allowances']?></td>
                                    <td><?=$row['prices']?></td>
                                    <td><?=$row['feesaving']?></td>
                                </tr>
                                <?php 
                                $no++;
                                }
                                
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>




        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="