<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Piutang</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li class="active">
                Piutang
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Piutang</h5>
                </div>
                
                 <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id='postsLis' >
                            <thead>
                                <tr>
                                    <th rowspan="2">#</th>
                                    <th rowspan="2">Action</th>
                                    <th rowspan="2">Status</th>
                                    <th rowspan="2">Invoice Date</th>
                                    <th rowspan="2">No Invoice</th>
                                    <th rowspan="2">Customers</th>
                                    <th rowspan="2">No Faktur Pajak</th>
                                    <th rowspan="2">Tgl JT</th>
                                    <th colspan="3">Tertagih</th>
                                    <th colspan="3">Lunas</th>
                                </tr>
                                <tr>
                                    <th>DPP</th>
                                    <th>PPN</th>
                                    <th>Jumlah</th>
                                    <th>Tgl Bayar</th>
                                    <th>Jumlah</th>
                                    <th>Selisih</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php 
                              $no=1;
                              foreach ($list as $key) {

                                date_default_timezone_set("Asia/Bangkok");
                                $tanggalupdate = date("Y-m-d");
                                if (empty($key->pay_date) && $tanggalupdate > $key->tempo) {
                                    $warna = "<span class='label label-danger'> OutSchedule</span>";
                                }elseif (!empty($key->pay_date)) {
                                    $warna = "<span class='label label-info'>".$key->status."</span>";
                                }else{
                                    $warna = "<span class='label label-warning'>".$key->status."</span>";
                                }
                                ?>
                                <tr>
                                    <td nowrap><?=$no?></td>
                                    <td nowrap>
                                        <a class="edit" id="edit" value="<?=$key->noinvoice?>"> 
                                            <span class="label label-success"> <i class="fa fa-edit"> </i></span>
                                            
                                        </a>
                                        
                                    </td>
                                    <td nowrap><?=$warna?></td>
                                    <td nowrap><?=$key->dateinvoice?></td>
                                    <td nowrap><?=$key->noinvoice?></td>
                                    <td nowrap><?=$key->cname?></td>
                                    <td nowrap><?=$key->faktur?></td>
                                    <td nowrap><?=$key->tempo?></td>
                                    <td nowrap><?=number_format($key->nominal)?></td>
                                    <td nowrap><?=number_format($key->ppn)?></td>
                                    <td nowrap><?php $totala = $key->nominal + $key->ppn; echo number_format($totala);?></td>
                                    <td nowrap><?=$key->pay_date?></td>
                                    <td nowrap><?=number_format($key->pay_nominal)?></td>
                                    <td nowrap><?=number_format($key->selisih)?></td>
                                    
                                </tr>
                                <?php
                                $no++; } 
                                ?>
                            </tbody>

                        </table>
                    </div>

                  </div>


                
                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                </div>  
                <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>



        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>

    <script>
        $(document).ready(function(){
        	$("#postsLis").on("click", "#edit", function(event) {
                var id = $(this).attr('value');
                alert(id);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('summarySlip/modalPiutang');?>",
                    dataType: "html",
                    data: {
                        id:id
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();
                    },
                });
            });
            $('#postsLis').DataTable({
                pageLength: 50,
                responsive: true,
                ordering : false

            });

            $("#postsLis").on("click", "#invoice", function(event) {
                var id = $(this).attr('value');
                alert(id);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('summarySlip/modalInvoice');?>",
                    dataType: "html",
                    data: {
                        id:id
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();
                    },
                });
            });

            

        });
    </script>