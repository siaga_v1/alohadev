<style>
    @page {
        sheet-size: A4;
    }

    @page receipt {
        sheet-size: 210mm 300mm
    }


    body {
        font-family: Tempus Sans ITC;
        height: auto;
        font-size: 10pt;
    }

    h3 {
        color: orange;
    }

    h1 {
        font-family: Arial;
        font-size: 11pt;
        margin: 0pt;
    }

    .test {
        padding-top: 10px;
        text-align: left;
    }

    .testa {
        padding-top: 7px;
        text-align: left;
    }

    .testb {
        padding-top: 7px;
        text-align: left;
    }

    .text-red {
        font-weight: bolder;
        color: red;
    }

    .detail-desc {
        font-size: 7pt;
    }

    .detail-invoice {
        border-collapse: collapse;
        border-color: silver;
        border-width: thin;
        font-size: 7pt;
    }

    .bo {
        border-collapse: collapse;
    }

    .bo td {
        font-size: 10pt;
    }

    .footer {
        font-size: 10pt;
    }

    hr.style2 {
        border-top: 3px double #8c8b8b;
    }

    #code-text {
        width: 100%;
        float: right;
        text-align: right;
    }

    table {
        border-collapse: collapse;
    }

    .bo table,
    .bo th,
    .bo td {
        border: 1px solid black;
        padding: 5px;
    }

    table tfoot {
        page-break-inside: avoid !important;
    }
</style>
<table width="100%">
    <thead>
        <tr>
            <td align="right">
                <h1>PT. ALOHA LOGISTIK SUKSES<br>JL. KEBON BAWANG VII NO.18 TANJUNG PRIOK 14320<br>TELP : 021-43934691 - 92 FAX : 43933234</h1>
                <hr class="style2">
            </td>
        </tr>

        <tr>
            <td>
                <?php
                function tgl_indo($tanggal)
                {
                    $bulan = array(
                        1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                    );
                    $pecahkan = explode('-', $tanggal);
                    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
                }
                ?>
                <table width="100%">
                    <tr>
                        <td align="center">
                            <h1> Slip Gaji Supir</h1>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"> </td>
                    </tr>

                    <tr>
                        <td>Nama Driver : <?= $detail[0]['driver'] ?></td>
                    </tr>
                    <tr>
                        <td align="center"> </td>
                    </tr>
                </table>
            </td>
        </tr>
    </thead>
</table>

<table width="100%" class="bo" border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>No. Pol</th>
            <th>Tujuan</th>
            <th>Komisi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        $total = 0;
        $code = 0;
        foreach ($detail as $row) {
            // if($no <= 22) {

        ?>
            <tr>
                <td><?= $no ?></td>
                <td><?php $awal = $row['orderdate'];
                    $pecah = explode(" ", $awal);
                    echo date('d-m-Y', strtotime($pecah[0])); ?></td>
                <td><?= $row['fleet'] ?></td>
                <td><?= $row['c2'] ?></td>
                <td><?= number_format($row['komSupir']) ?></td>
            </tr>

        <?php
            //}
            $no++;
            $total += $row['komSupir'];
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="4" align="right">Gaji</td>
            <td align="right"><?= number_format($total) ?></td>
        </tr>
        <tr>
            <td colspan="4" align="right">Bonus Hadir</td>
            <td align="right">
                <?php
                echo number_format($bonus_h);

                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="right">Bonus Ritase</td>
            <td align="right">
                <?php
                echo number_format($bonus_r);

                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="right">Potongan Izin</td>
            <td align="right">
                <?php
                echo number_format($poti);

                ?>
            </td>
        </tr>

        <tr>
            <td colspan="4" align="right">Kasbon</td>
            <td align="right"><?= number_format($kasbon) ?></td>
        </tr>

        <tr>
            <td colspan="4" align="right">TOTAL</td>
            <td align="right"><?= number_format($komisi + $bonus_h + $bonus_r - $poti - $kasbon) ?></td>
        </tr>
    </tfoot>
</table>