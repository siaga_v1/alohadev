<style>
body { 
    font-family:arial;
    height: auto;
    font-size:14pt;
}

h3{
    color:orange;
}

h1{
    font-size: 14pt;
    margin:0pt;
}
.test {
  padding-top: 10px;
  text-align: left;
}
.testa {
  padding-top: 7px;
  text-align: left;
}
.testb {
  padding-top: 7px;
  text-align: left;
}
.text-red{
    font-weight: bolder;
    color:red;
}
.detail-desc{
    font-size: 7pt;
}
.detail-invoice{
    border-collapse: collapse;
    border-color: silver;
    border-width: thin;
    font-size: 7pt;
}

.detail-invoice tbody td{
    font-size: 8pt;
}
.footer{
    font-size: 8pt;
}
table{
    border-collapse: collapse;
}

hr{
    border-collapse: collapse;
    border: 1px solid black;
}
#code-text{
    width:100%;
    float: right;
    text-align: right;
}
</style>
<body>
    <?php
    function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = penyebut($nilai - 10). " Belas";
        } else if ($nilai < 100) {
            $temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " Seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " Seribu" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
        }     
        return $temp;
    }
 
    function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim(penyebut($nilai));
        } else {
            $hasil = trim(penyebut($nilai));
        }           
        return $hasil;
    }


 
        foreach($invoice as $row) { $total =  $row->ppn + $row->nominal;
                    $aaa= terbilang(round($total)); 
                                    $test = str_word_count($aaa); }
            if ($test > 7) {
        ?>
    <div>
        <div style = "width:55%;float:left;">
            <table width="100%">
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>

            </table>
        </div>

        <div style = "width:45%; float:right;">
               <table width="100%">
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center"><h1><?php foreach($invoice as $row) { echo $row->noinvoice; } ?></h1></td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                </table>
        </div>
    </div>

    <div>
        <div style = "width:25%; float:left;clear: both;">
            <table  width="100%">
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>

        <div style = "width:75%; float:right;">
           <table width="100%" align="right">
                <tr>
                    <td class="testa"><?php foreach($invoice as $row) { echo $row->nickname; } ?></td>
                </tr>
                
                <tr>
                    <td class="testa"><?php foreach($invoice as $row) { echo $row->address_1; } ?></td>
                </tr>
                
                <tr>
                    <td class="testa"><?php foreach($invoice as $row) { echo  "".$row->address_2." - ".$row->address_3.""; } ?></td>
                </tr>
<?php
    function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    function tgl_indo2($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $bulan[ (int)$pecahkan[1] ]. ' ' . $pecahkan[0] ;
    }

    


?>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><b>Rp. <?php foreach($invoice as $row) { $total =  $row->ppn + $row->nominal;
                    echo number_format($total); } ?></b></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td><b><?php foreach($invoice as $row) { $total =  $row->ppn + $row->nominal;
                    echo terbilang(round($total)); } ?> Rupiah</b></td>
                </tr>


                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td class="test">Biaya Pengiriman Kendaraan</td>
                </tr>

                <tr>
                    <td class="testb">Periode <?php echo tgl_indo2(date('Y-m'));?></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td class="testb"><b>NSFP  : <?php foreach($invoice as $row) { echo $row->faktur; } ?></b></td>
                </tr>
            </table>
            <table  width="85%">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr >
                    <td class="testb"><b>Sub Total</b></td>
                    <td class="testb"><b>Rp</b></td>
                    <td class="testb" align="right"><b><?php foreach($invoice as $row) { echo number_format($row->nominal); } ?></b></td>
                </tr>
                <tr>
                    <td class="testb"><b>PPN 10%</b></td>
                    <td class="testb"><b>Rp</b></td>
                    <td class="testb" align="right"><b><?php foreach($invoice as $row) { echo number_format($row->ppn); } ?></b></td>
                </tr>
                <tr>
                    <td class="testb"><b>Total</b></td>
                    <td class="testb"><b>Rp</b></td>
                    <td class="testb" align="right"><b><?php foreach($invoice as $row) { $total =  $row->ppn + $row->nominal;
                   echo number_format($total); } ?></b></td>
                </tr>
            </table>
    </div> 

    </div>

    <div style="width: 100%">
        <table width="100%">
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td align="center">Bank Permata Cabang <?php foreach($invoice as $row) { echo $row->cabang; } ?> A/C No. <?php foreach($invoice as $row) { echo $row->norekening; } ?></td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div style="width: 40%;float: right;">
        <table width="100%">
            
            
            <tr>
                <td align="center"><?php foreach($invoice as $row) { echo tgl_indo($row->dateInvoice); } ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>


            <tr>
                <td align="center" class="bb">IVAN HAMZAH</td>
            </tr>
            <tr>
                <td align="center">Direktur</td>
            </tr>
        </table>
    </div> 
    <?php 
            }else{
       
                ?>

    <div>
        <div style = "width:55%;float:left;">
            <table width="100%">
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>

            </table>
        </div>

        <div style = "width:45%; float:right;">
               <table width="100%">
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center"><h1><?php foreach($invoice as $row) { echo $row->noinvoice; } ?></h1></td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                </table>
        </div>
    </div>

    <div>
        <div style = "width:25%; float:left;clear: both;">
            <table  width="100%">
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>

        <div style = "width:75%; float:right;">
           <table width="100%" align="right">
                <tr>
                    <td class="testa"><?php foreach($invoice as $row) { echo $row->nickname; } ?></td>
                </tr>
                
                <tr>
                    <td class="testa"><?php foreach($invoice as $row) { echo $row->address_1; } ?></td>
                </tr>
                
                <tr>
                    <td class="testa"><?php foreach($invoice as $row) { echo  "".$row->address_2." - ".$row->address_3.""; } ?></td>
                </tr>
                    <?php
                        function tgl_indo($tanggal){
                            $bulan = array (
                                1 =>   'Januari',
                                'Februari',
                                'Maret',
                                'April',
                                'Mei',
                                'Juni',
                                'Juli',
                                'Agustus',
                                'September',
                                'Oktober',
                                'November',
                                'Desember'
                            );
                            $pecahkan = explode('-', $tanggal);
                            
                            // variabel pecahkan 0 = tanggal
                            // variabel pecahkan 1 = bulan
                            // variabel pecahkan 2 = tahun
                         
                            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
                        }

                        function tgl_indo2($tanggal){
                            $bulan = array (
                                1 =>   'Januari',
                                'Februari',
                                'Maret',
                                'April',
                                'Mei',
                                'Juni',
                                'Juli',
                                'Agustus',
                                'September',
                                'Oktober',
                                'November',
                                'Desember'
                            );
                            $pecahkan = explode('-', $tanggal);
                            
                            // variabel pecahkan 0 = tanggal
                            // variabel pecahkan 1 = bulan
                            // variabel pecahkan 2 = tahun
                         
                            return $bulan[ (int)$pecahkan[1] ]. ' ' . $pecahkan[0] ;
                        }

                        

                    ?>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><b>Rp. <?php foreach($invoice as $row) { $total =  $row->ppn + $row->nominal;
                    echo number_format($total); } ?></b></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td><b><?php foreach($invoice as $row) { $total =  $row->ppn + $row->nominal;
                    echo terbilang(round($total)); } ?> Rupiah</b></td>
                </tr>


                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td class="test">Biaya Pengiriman Kendaraan</td>
                </tr>

                <tr>
                    <td class="testb">Periode <?php echo tgl_indo2(date('Y-m'));?></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td class="testb"><b>NSFP  : <?php foreach($invoice as $row) { echo $row->faktur; } ?></b></td>
                </tr>
            </table>
            <table  width="85%">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr >
                    <td class="testb"><b>Sub Total</b></td>
                    <td class="testb"><b>Rp</b></td>
                    <td class="testb" align="right"><b><?php foreach($invoice as $row) { echo number_format($row->nominal); } ?></b></td>
                </tr>
                <tr>
                    <td class="testb"><b>PPN 10%</b></td>
                    <td class="testb"><b>Rp</b></td>
                    <td class="testb" align="right"><b><?php foreach($invoice as $row) { echo number_format($row->ppn); } ?></b></td>
                </tr>
                <tr>
                    <td class="testb"><b>Total</b></td>
                    <td class="testb"><b>Rp</b></td>
                    <td class="testb" align="right"><b><?php foreach($invoice as $row) { $total =  $row->ppn + $row->nominal;
                   echo number_format($total); } ?></b></td>
                </tr>
            </table>
    </div> 

    </div>

    <div style="width: 100%">
        <table width="100%">

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td align="center">Bank Permata Cabang <?php foreach($invoice as $row) { echo $row->cabang; } ?> A/C No. <?php foreach($invoice as $row) { echo $row->norekening; } ?></td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div style="width: 40%;float: right;">
        <table width="100%">
            
            
            <tr>
                <td align="center"><?php foreach($invoice as $row) { echo tgl_indo($row->dateInvoice); } ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>


            <tr>
                <td align="center" class="bb">IVAN HAMZAH</td>
            </tr>
            <tr>
                <td align="center">Direktur</td>
            </tr>
        </table>
    </div> 
           <?php } ?>
</body>
