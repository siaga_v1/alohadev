<style>
    body { 
        font-family:Tempus Sans ITC;
        height: auto;
        font-size:12pt;
    }

    h3{
        color:orange;
    }

    h1{
        font-family:Tempus Sans;
        font-size: 16pt;
        margin:0pt;
    }
    .test {
        padding-top: 10px;
        text-align: left;
    }
    .testa {
        padding-top: 7px;
        text-align: left;
    }
    .testb {
        padding-top: 7px;
        text-align: left;
    }
    .text-red{
        font-weight: bolder;
        color:red;
    }
    .detail-desc{
        font-size: 7pt;
    }
    .detail-invoice{
        border-collapse: collapse;
        border-color: silver;
        border-width: thin;
        font-size: 7pt;
    }

    .bo td{
        font-size: 8pt;
    }
    .footer{
        font-size: 8pt;
    }
    hr{
        border-collapse: collapse;
        border: 1px solid black;
    }
    #code-text{
        width:100%;
        float: right;
        text-align: right;
    }

    table {
      border-collapse: collapse;
    }

    .bo table, .bo th, .bo td {
      border: 1px solid black;
    }
</style>
<body>
        <table width="100%" >
            <tr>
                <td align="center">ARMADA SEJAHTERA<hr></td>
            </tr>

            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="50%">Kepada : <?php foreach($invoice as $key){echo $key->name;} ?></td>
                            <td width="50%" align="right">Invoice No : <?=$id?></td>
                        </tr>
                        <tr>
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr align="center">
                <td>
                    <table width="100%" class="bo">
                        <tr>
                            <td colspan="8">Keterangan</td>
                        </tr>
                        <tr>
                            <td>No</td>
                            <td>Tanggal</td>
                            <td>Kontainer</td>
                            <td>Seal</td>
                            <td>Feet</td>
                            <td>Nama</td>
                            <td>Tujuan</td>
                            <td>Harga</td>
                        </tr>

                        <?php foreach ($detail as $row) {
                        	?>
                        	<tr>
	                            <td>No</td>
	                            <td><?=$row->orderdate?></td>
	                            <td><?=$row->frameno?></td>
	                            <td><?=$row->noshippent?></td>
	                            <td><?=$row->name?></td>
	                            <td><?=$row->n1?></td>
	                            <td><?=$row->c2?></td>
	                            <td><?=$row->unitprice?></td>
                        	</tr>
                        <?php                        	
                        }?>
                        
                        <tr>
                            <td colspan="7"></td>
                            <td>Total</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr align="center">
                <td>
                <table width="100%">
                    <tr align="center">
                        <td width="50%">&nbsp;</td>
                        <td width="50%">Tanggal </td>
                    </tr>
                    <tr align="center">

                        <td width="50%"></td>
                        <td width="50%">Diterima Oleh : &emsp;&emsp; Diketahui Oleh:</td>
                    </tr>
                </table>
            </td>
            </tr>
        </table>
</body>
