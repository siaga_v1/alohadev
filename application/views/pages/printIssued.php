<style>
body { 
    font-family:Courier;
    height: auto;
    font-size:13pt;
}

h3{
    color:orange;
}

h1{
    font-size: 20pt;
}
.text-red{
    font-weight: bolder;
    color:red;
}
.detail-desc{
    font-size: 7pt;
}
.detail-invoice{
    border-collapse: collapse;
    border-color: silver;
    border-width: thin;
    font-size: 7pt;
}

.detail-invoice tbody td{
    font-size: 8pt;
}
.footer{
    font-size: 8pt;
}
table{
    border-collapse: collapse;
}

hr{
    border-collapse: collapse;
    border: 1px solid black;
}
#code-text{
    width:100%;
    float: right;
    text-align: right;
}
</style>
<body>
<div id="code-text" style="float: right;"><?=$SPB[0]['bk_id']?></div>
<table style="width: 100%;" cellpadding="4" cellspacing="5">
    <tr>
        <th colspan="4" align="center" valign="middle">
            <h2>FORM ISSUED ITEM</h2>
            <hr />
        </th>
    </tr>
    <tr>
        <td align="left" width="30%"><b>Nomor Issued</b></td>
        <td align="left" colspan="3" >: <?=$SPB[0]['bk_id']?></td>
    </tr>
    <tr>
        <td align="left"><b>Nomor SPB</b></td>
        <td align="left" colspan="3">: <?=$SPB[0]['bk_ref']?></td>
    </tr>
    <tr>
        <td align="left"><b>Tanggal Issued</b></td>
        <td align="left" width="25%">: <?=$SPB[0]['bk_date']?></td>
    </tr>
    <tr>
        <td align="left"><b>Armada</b></td>
        <td align="left" width="25%">: <?=$SPB[0]['arm_nomor_pol']?></td>
    </tr>
    <tr>
        <td align="left"><b>User</b></td>
        <td align="left" width="25%">: <?=$SPB[0]['emp_nama']?> <?=$SPB[0]['emp_code']?></td>
    </tr>
  
    
    <tr>
        <th colspan="4"><hr /></th>
    </tr>
    <tr>
        <th colspan="4">
        <table width="100%" cellpadding="5">
                <tr>
                    <th width="10%">#</th>
                    <th width="80%" colspan="2">Item</th>
                    <th width="10%">Qty</th>
                </tr>
                     <?php 
        $n=1;
        foreach ($SPB as $key) {
    ?>
    <tr>
        <td align="Center"><?=$n;?></td>
        <td align="left" colspan="2"><?=$key['ii_name'];?></td>
        <td align="center"><?=$key['bki_qty'];?></td>
    </tr>
    <?php $n++; }  ?>
            </table>
        </th>
    </tr>


    <tr>
        <th colspan="4">&nbsp;</th>
    </tr>
    <tr>
        <th colspan="4">&nbsp;</th>
    </tr>
    <tr>
        <th colspan="4">
            <table border="1" width="100%" cellpadding="5">
                <tr>
                    <th width="33%">Menerima</th>
                    <th width="33%">Memberikan</th>
                    <th width="34%">Mengetahui</th>
                </tr>
                <tr>
                    <td>&nbsp; <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <?=$SPB[0]['bk_emp_recive']?>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </th>
    </tr>
    
</table>
