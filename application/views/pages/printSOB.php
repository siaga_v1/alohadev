<style>
body { 
    font-family:Courier;
    height: auto;
    font-size:11pt;
}

h3{
    color:orange;
}

h1{
    font-size: 20pt;
    margin-bottom:  0pt;

}
h2{
    font-size: 16pt;
    margin-bottom:  0pt;
}
.text-red{
    font-weight: bolder;
    color:red;
}
.detail-desc{
    font-size: 7pt;
}
.detail-invoice{
    border-collapse: collapse;
    border-color: silver;
    border-width: thin;
    font-size: 7pt;
}
tr.border_bottom th {
  border-bottom:0.5pt solid black;
  border-top:0.5pt solid black;
}
.detail-invoice tbody td{
    font-size: 8pt;
}
.footer{
    font-size: 8pt;
}
table{
    border-collapse: collapse;
}

hr{
    border-collapse: collapse;
    border: 1px solid black;
}
#code-text{
    width:100%;
    float: right;
    text-align: right;
}
</style>
<body>
<div id="code-text" style="float: right;"></div>
<table style="width: 100%;"cellpadding="4" cellspacing="5">

    <tr>
        <th colspan="4"></th>
    </tr>
    <tr>
        <th colspan="4">

            <table width="100%">
                <tr>
                    <th><h2>Surat Order Barang</h2></th>
                </tr>
            </table>
        </th>
    </tr>

    <tr>
        <th colspan="4"></th>
    </tr>
    <tr>
        <th colspan="4">
            <table width="100%" >
                <tr>
                    <td align="left" width="25%">Tanggal SOB</td>
                    <td align="left" width="2%">: </td>
                    <td align="left"><?=$SPB[0]['dob_date']?></td>
                </tr>

               <tr>
                    <td align="left" width="25%">Nama Pemohon</td>
                    <td align="left" width="2%">: </td>
                    <td align="left"><?=$SPB[0]['emp_code']?> <?=$SPB[0]['emp_nama']?></td>
                </tr>

                <tr>
                    <td align="left" width="25%">Armada</td>
                    <td align="left" width="2%">: </td>
                    <td align="left"><?=$SPB[0]['arm_nomor_pol']?> </td>
                </tr>
                <tr>
                    <td align="left" width="25%">Manufacture</td>
                    <td align="left" width="2%">: </td>
                    <td align="left"><?=$SPB[0]['arm_merek_mobil']?>  </td>
                </tr>
                <tr>
                    <td align="left" width="25%">Chasis Number</td>
                    <td align="left" width="2%">: </td>
                    <td align="left"><?=$SPB[0]['arm_code']?></td>
                </tr>
            </table>
        </th>
    </tr>
    <tr>
        <th colspan="4"></th>
    </tr>
    <tr>
        <th colspan="4">
        <table width="100%" cellspacing="5">
                <tr class="border_bottom">
                    <th width="3%">#</th>
                    <th width="15%">Kode Barang</th>
                    <th width="25%">Nama Barang</th>
                    <th width="10%">Qty</th>
                    <th width="25%">Keterangan</th>
                </tr>
                     <?php 
                        $n=1;
                        foreach ($SPB as $key) {
    ?>
    <tr>
        <td align="Center"><?=$n;?></td>
        <td align="left"><?=$key['ii_code'];?></td>
        <td align="left"><?=$key['ii_name'];?></td>
        <td align="center"><?=$key['dobi_count'];?></td>
        <td align="center"><?=$key['dobi_note'];?></td>
    </tr>
    <?php $n++; }  ?>
            </table>
        </th>
    </tr>


    <tr>
        <th colspan="4">&nbsp;</th>
    </tr>
    <tr>
        <th colspan="4">&nbsp;</th>
    </tr>
    <tr>
        <th colspan="4">
            <table border="1" width="100%">
                <tr>
                    <th width="33%">Memberikan</th>
                    <th width="33%">Menerima</th>
                    <th width="34%">Mengetahui</th>
                </tr>
                <tr>
                    <td>&nbsp; <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </th>
    </tr>
    
</table>
