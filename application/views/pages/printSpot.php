<style>
body { 
    font-family:Arial;
    height: auto;
    font-size:12pt;
}

p  { 
    font-family:Arial;
    height: auto;
    font-size:9pt;
}

h3{
    color:orange;
}

h1{
    font-size: 12pt;
}
.text-red{
    font-weight: bolder;
    color:red;
}
.detail-desc{
    font-size: 7pt;
}
.detail-invoice{
    border-collapse: collapse;
    border-color: silver;
    border-width: thin;
    font-size: 7pt;
}

.detail-invoice tbody td{
    font-size: 8pt;
}
.footer{
    font-size: 8pt;
}

hr{
    border-collapse: collapse;
    border: 1px solid black;
}
#code-text{
    border:thin solid  #000000;
    background-clip: border-box;
    padding: 0.5em;
}
</style>
<body >
<div id="code-text">
<table style="width: 100%;">
    <tr>
        <th >
                    <img width="150px" class="img-responsive" src="<?php echo base_url();?>assets/img/logo_ang.png" align="left">
        </th>
        <th>
            <h1 align="center">SPOT ORDER</h1>
            
        </th>    
        <th colspan="3">
           Tanggal JO : <?php
                        $a = explode(" ", $spot[0]['createdatetime']);
                        echo $a[0];?>
        </th>       
            <hr />
    </tr>

    <tr>
        <td align="left" width="20%">&nbsp;</td>
        <td align="right" colspan="4" > No. JO : <?=$spot[0]['shippment']?></td>
    </tr>
    <tr>
        <td align="left" width="20%"><b>1. Data Customer<b><hr></td>
        <td align="left" colspan="4" ></td>
    </tr>
    <tr>
        <td align="left">Nama Pengirim</td>
        <td align="left" colspan="4" >: 
            <?php foreach ($spot as $key ) { foreach ($customer as $row) { if ($row['id'] == $key['id_customers']) { echo $row['name']; } } } ?> 
        </td>
    </tr>
    <tr>
        <td align="left">Alamat Pengirim</td>
        <td align="left" colspan="4" >: 
            <?php foreach ($spot as $key ) { foreach ($cities as $row) { if ($row['id'] ==  $key['id_citieso']) { echo $row['name']; } } } ?> 
            
        </td>
    </tr>
    <tr>
        <td align="left">Telp</td>
        <td align="left" colspan="4" > </td>
    </tr>
    <tr>
        <td align="left">Nama Penerima</td>
        <td align="left" colspan="4">: <?php echo $spot[0]['penerima']; ?></td>
    </tr>
    <tr>
        <td align="left">Alamat Penerima</td>
        <td align="left" colspan="4">: 
            <?php foreach ($spot as $key ) { foreach ($cities as $row) { if ($row['id'] ==  $key['id_citiesd']) { echo $row['name']; } } } ?> 
                     </td>
    </tr>
    <tr>
        <td align="left">Telp</td>
        <td align="left" colspan="4">: </td>
    </tr>

    <tr>
        <td align="left">&nbsp;</td>
        <td align="left" colspan="4">&nbsp;</td>
    </tr>

    <tr>
        <td align="left"><b>Tanggal Pengambilan</b></td>
        <td align="left" colspan="4">: <?php echo $spot[0]['orderdate']; ?></td>
    </tr>


    <tr>
        <td align="left" width="30%"><b>2. Data Kendaraan<b><hr></td>
        <td align="left" colspan="4" > </td>
    </tr>

    <tr>
        <td align="left">Jml/Merk/Type</td>
        <td align="left" colspan="4">: <?php echo $spot[0]['loadqty']; ?> <?php echo $spot[0]['description']; ?></td>

    </tr>
    <tr>
        <td align="left">Status</td>
        <td align="left" colspan="4">: <input type="checkbox" name="test" value="abc"> Profit <input type="checkbox" name="test" value="abc"> Second</td>
        
    </tr>
    <tr>
        <td align="left">Kondisi Surat</td>
        <td align="left" colspan="4">: <input type="checkbox" name="test" value="abc"> Ada <input type="checkbox" name="test" value="abc"> .............</td>
        
    </tr>
    <tr>
        <td align="left">Jenis Unit</td>
        <td align="left" colspan="4">: <input type="checkbox" name="test" value="abc"> Tansya <input type="checkbox" name="test" value="abc"> Car Carrier <input type="checkbox" name="test" value="abc"> Towing</td>
        
    </tr>
    <tr>
        <td align="left">Uang Jalan</td>
        <td align="left" colspan="4">: <?php echo $spot[0]['prices']; ?></td>
        
    </tr>
    <tr>
        <td align="left">Uang Titip</td>
        <td align="left" colspan="4">: <?php echo $spot[0]['feesaving']; ?></td>
        
    </tr>
    <tr>
        <td align="left">Nominal</td>
        <td align="left" colspan="4">: <?php echo $spot[0]['allowances']; ?></td>
        
    </tr>
    <tr>
        <td align="left">Invoice A/N</td>
        <td align="left" colspan="4">: <?php echo $spot[0]['invoicean']; ?></td>
        
    </tr>
    <tr>
        <td align="left">1.</td>
        <td align="left" colspan="4"></td>
        
    </tr>
    <tr>
        <td align="left">2.</td>
        <td align="left" colspan="4"></td>
        
    </tr>
    <tr>
        <td align="left">Asuransi</td>
        <td align="left" colspan="4">: <input type="checkbox" name="test" value="abc"> Ada <input type="checkbox" name="test" value="abc"> Tidak Ada</td>
        
    </tr>
    <tr>
        <td align="left">Penawaran/Kontrak</td>
        <td align="left" colspan="4">: <input type="checkbox" name="test" value="abc"> Ada <input type="checkbox" name="test" value="abc"> Tidak Ada</td>
        
    </tr>
    <tr>
        <th colspan="5">
            <table border="1" width="100%" cellpadding="5">
                <tr>
                    <th width="20%">Dibuat Oleh</th>
                    <th width="20%">Disetujui Oleh</th>
                    <th width="20%">Disetujui Oleh</th>
                </tr>
                <tr>
                    <td>&nbsp; <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <p>RIRIS D.F<br>MARKETING</p></td>
                    <td>&nbsp; <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <p>IVAN HAMZAH<br>MANAGER MARKETING</p></td>
                    <td>&nbsp; <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <p>HENY PRASETYO<br>MANAGER OPERATION</p></td>
                </tr>
            </table>
        </th>
    </tr>
    
</table>
</div>