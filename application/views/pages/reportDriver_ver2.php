<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | Trucks </span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout') ?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gaji Supir</h2>

        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Report Driver
            </li>
            <li class="active">
                <strong>Gaji Supir</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Gaji Supir</h5>
                    <div class="ibox-tools">
                        <a data-toggle="modal" data-target="#myModal5">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content">
                    <form id="form-search-FleetReport" name="form_FleetReport_search" method="post">
                        <div class="ibox-content m-b-sm border-bottom">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="date_added">Driver</label>
                                        <select id="supir" name="supir" class="form-control chosen-select">

                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="date_modified">Date</label>
                                        <div class="input-daterange input-group" id="datepicker">
                                            <input type="text" class="form-control-sm form-control start" id="start" autocomplete="off" name="start">
                                            <span class="input-group-addon">to</span>
                                            <input type="text" class="form-control-sm form-control end" id="end" autocomplete="off" name="end">
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">

                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">

                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info">Search</button>
                                        <button type="reset" class="btn btn-warning">Reset</button>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id='postsList'>
                            <thead>
                                <tr>
                                    <th>No #</th>
                                    <th>Driver Name</th>
                                    <th>Tabungan</th>
                                    <th>Komisi</th>
                                    <th>Hadir</th>
                                    <th>Trip</th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                            <tfoot>

                            </tfoot>

                        </table>
                    </div>
                    <div style='margin-top: 10px;' id='pagination'></div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">

                </div>
                <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>



<!-- Mainly scripts -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


<script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<!-- Chosen -->
<script src="<?php echo base_url(); ?>assets/js/plugins/chosen/chosen.jquery.js"></script>

<!-- JSKnob -->
<script src="<?php echo base_url(); ?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

<!-- Input Mask-->
<script src="<?php echo base_url(); ?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<!-- Data picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- NouSlider -->
<script src="<?php echo base_url(); ?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

<!-- Switchery -->
<script src="<?php echo base_url(); ?>assets/js/plugins/switchery/switchery.js"></script>

<!-- IonRangeSlider -->
<script src="<?php echo base_url(); ?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- MENU -->
<script src="<?php echo base_url(); ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Color picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- Clock picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/clockpicker/clockpicker.js"></script>

<!-- Image cropper -->
<script src="<?php echo base_url(); ?>assets/js/plugins/cropper/cropper.min.js"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?php echo base_url(); ?>assets/js/plugins/fullcalendar/moment.min.js"></script>

<!-- Date range picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/js/plugins/select2/select2.full.min.js"></script>

<!-- TouchSpin -->
<script src="<?php echo base_url(); ?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

<!-- Tags Input -->
<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- Dual Listbox -->
<script src="<?php echo base_url(); ?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/toastr/toastr.min.js"></script>

<script>
    $(document).ready(function() {
        $('#supir').chosen();


        $('#pagination').on('click', 'a', function(e) {
            e.preventDefault();
            var pageno = $(this).attr('data-ci-pagination-page');
            loadPagination(pageno);

            $('#postsList tbody').empty();
        });

        loadPagination(0);
        $('#form-search-FleetReport').submit(function() {
            loadPagination(0);
            return false;
        });

        function loadPagination(pagno) {
            var param = $('#form-search-FleetReport').serialize();
            $.ajax({
                url: '<?= base_url() ?>driver/driversummary/' + pagno,
                type: 'get',
                data: param,
                dataType: 'json',
                success: function(response) {
                    $('#pagination').html(response.pagination);
                    createTable(response.result, response.row);
                }
            });
        }


        function createTable(result, sno) {
            sno = Number(sno);
            var tabtotal = 0;
            var kastotal = 0;
            var komstotal = 0;
            var rittotal = 0;
            var formatter = new Intl.NumberFormat('id-ID', {
                style: 'currency',
                currency: 'IDR',
            });

            $('#postsList tbody').empty();
            $('#postsList tfoot').empty();
            for (index in result) {
                var action = result[index].name;
                var code = result[index].namac;
                var date = result[index].tabungan;
                var komisi = result[index].komisi;
                var dest = result[index].id;
                var ritase = result[index].ritase;
                var hadir = result[index].hadir;
                sno += 1;

                var tr = "<tr>";
                tr += "<td>" + sno + "</td>";
                tr += "<td>" + action + "</td>";
                tr += "<td>" + formatter.format(date) + "</td>";
                tr += "<td>" + formatter.format(komisi) + "</td>";
                tr += "<td>" + hadir + "</td>";
                tr += "<td>" + ritase + "</td>";
                tr += "<td><a class='detail btn btn-success btn-xs detail' value=" + dest + "><i class='fa fa-book'></i> Gaji </a> </td>";
                tr += "</tr>";
                $('#postsList tbody').append(tr);
                tabtotal += Number(date);
                komstotal += Number(komisi);
                rittotal += Number(ritase);


            }
            var trf = "<tr>";
            trf += "<td colspan ='2'>TOTAL </td>";
            trf += "<td>" + formatter.format(tabtotal) + "</td>";
            trf += "<td>" + formatter.format(komstotal) + "</td>";
            trf += "<td>" + rittotal.toLocaleString() + "</td>";
            trf += "</tr>";
            $('#postsList tfoot').append(trf);
        }

        $("#postsList").on("click", ".detail", function(event) {
            var idcus = $(this).attr('value');
            var start = $('#start').val();
            var end = $('#end').val();
            var supir = $('#supir option:selected').val();
            //alert(supir);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('driver/modalGajiver2'); ?>",
                dataType: "html",
                data: {
                    id: idcus,
                    start: start,
                    end: end,
                    spr: supir
                },
                success: function(data) {
                    $("#modaledit").html(data);
                    $("#modaledit").modal();
                },
            });
        });


        $('.input-daterange').datepicker({
            format: 'dd-M-yyyy',
            keyboardNavigation: true,
            forceParse: false,
            autoclose: true
        });

        $.ajax({
            url: '<?php echo base_url(); ?>customer/getCust',
            type: 'POST',
            data: 'setting=drv',
            success: function(echo) {
                $('#supir').html(echo);
                $('#supir').trigger("chosen:updated");
            }
        });


    });
</script>