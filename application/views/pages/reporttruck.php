<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Report Truck</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Report
            </li>
            <li class="active">
                <strong>Report Truck</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="ibox-content">
        <form id="form-search-FleetReport" name="form_FleetReport_search" method="post">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="customer">Fleet Code</label>
                        <input type="text" class="form-control" id="code" name="code" placeholder="Fleet Report Code"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="customer">Fleet Plateno</label>
                        <input type="text" class="form-control" id="fleetplateno" name="fleetplateno" placeholder="FleetReport Code"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="date_modified">Route</label>
                        <div class="input-group">
                            <select class="form-control" autocomplete="off" id="origin" name="origin">

                            </select>
                            <span class="input-group-addon">to</span>
                            <select class="form-control" autocomplete="off" id="destination" name="destination">

                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="customer">Customer</label>
                        <select id="customer" name="customer" class="form-control">
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="date_added">Fleet Type</label>
                        <select id="type" name="type" class="form-control">

                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="date_modified">Date</label>
                        <div class="input-daterange input-group" id="datepicker">
                            <input type="text" class="form-control-sm form-control" autocomplete="off" name="start" id="start">
                            <span class="input-group-addon">to</span>
                            <input type="text" class="form-control-sm form-control" autocomplete="off" name="end" id="end">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <button type="submit" class="btn btn-info">Search</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <a href="#" id="export-ordertruck-summary" class="btn btn-success"><i class="fa fa-file-excel-o"></i>  Export</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="ibox-footer">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id='postsList' >
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="10%">Fleet Plateno</th>
                        <th>Ritase</th>
                        <th>Pendapatan (A)</th>
                        <th>Uang Jalan (B)</th>
                        <th>Biaya Lain-Lain (C)</th>
                        <th>Sparepart (D)</th>
                        <th>A - (B + C + D) </th>
                        <th>OPS - GAJI - SEWA (E)</th>
                        <th>Laba (A - (B + C + D + E)</th>
                        <th>Revisi Pembayaran Tagihan</th>
                        <th>Laba/Mobil</th>
                    </tr>
                </thead>

                <tbody>
                </tbody>

            </table>
        </div>
                    <div style='margin-top: 10px;' id='pagination'></div>

    </div>
</div>


                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                </div>  




        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('.input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                format:'yyyy-mm-dd',
                autoclose: true
            });

            $.ajax({
                url:'<?php echo base_url();?>customer/getCust',
                type:'POST',
                data:'setting=ambil',
                success: function(echo){
                    $('#customer').html(echo);
                }
            });

            $.ajax({
                url:'<?php echo base_url();?>customer/getCust',
                type:'POST',
                data:'setting=typearm',
                success: function(echo){
                    $('#type').html(echo);
                }
            });

            $("#export-ordertruck-summary").click(function(){
                window.open("<?php echo base_url();?>fleet/ordersummary?export=excel&"+$('#form-search-FleetReport').serialize());
                return false; 
            });

            $('#pagination').on('click','a',function(e){
                e.preventDefault(); 
                var pageno = $(this).attr('data-ci-pagination-page');
                loadPagination(pageno);

                $('#postsList tbody').empty();
            });

            loadPagination(0);
            $('#form-search-FleetReport').submit(function(){
                loadPagination(0);
                return false; 
            });

            function loadPagination(pagno){
                var param = $('#form-search-FleetReport').serialize();
                //alert(param);
                $.ajax({
                    url: '<?=base_url()?>fleet/ordersummary/'+pagno,
                    type: 'get',
                    data:param,
                    dataType: 'json',
                    success: function(response){
                        //alert(response.office);
                        $('#pagination').html(response.pagination);
                        createTable(response.result,response.row,response.office,response.totalcar);
                    }
                });
            }

            function createTable(result,sno,office,totalcar){
                sno = Number(sno);

                $('#postsList tbody').empty();
                for (index in office){
                    var asaa = office[index].costsd;
                    var assa = parseInt(asaa)/parseInt(totalcar);
                }
                for(index in result){
                    var id = result[index].id;
                    var action = result[index].id;
                    var code = result[index].noshippent;
                    var date = result[index].fleetplateno;
                    var origin = result[index].loadqty;
                    var destination  = result[index].allowances;
                    var customer = result[index].ritase;
                    var fleet = result[index].prices;
                    var driver = result[index].driver;
                    var bll = result[index].lainlain;
                    var tagih = result[index].biayatagih;
                    var allowances = result[index].allowances;

                    
                        if (result[index].Maintenance==null) {
                             var updat = 0;
                        } else {
                            var updat = result[index].Maintenance;
                        }
                    var pendapatan = parseInt(allowances) + parseInt(tagih);
                    var pengurang = parseInt(fleet) + parseInt(bll) + parseInt(tagih) + parseInt(updat);
                    sno+=1;

                    var tr = "<tr>";
                    tr += "<td>"+ sno +"</td>";
                    tr += "<td><a class='detail' value='"+sno+"'><input type='text' id='acc_"+sno+"' name='acc' value='"+date+"' style='display: none'>"+ date +"</a></td>";
                    tr += "<td>"+ customer +"</td>";
                    tr += "<td>"+ new Intl.NumberFormat('en-ID').format(pendapatan) +"</td>";
                    tr += "<td>"+ new Intl.NumberFormat('en-ID').format(fleet) +"</td>";
                    tr += "<td>"+ new Intl.NumberFormat('en-ID').format(bll) +"</td>";
                    tr += "<td>"+ new Intl.NumberFormat('en-ID').format(updat) +"</td>";
                    tr += "<td>"+ new Intl.NumberFormat('en-ID').format(pendapatan - pengurang) +"</td>";
                    tr += "<td>"+ new Intl.NumberFormat('en-ID').format(assa) +"</td>";
                    tr += "<td>"+ new Intl.NumberFormat('en-ID').format(pendapatan - pengurang - assa) +"</td>";
                    tr += "<td>"+ new Intl.NumberFormat('en-ID').format(0) +"</td>";
                    tr += "<td>"+ new Intl.NumberFormat('en-ID').format(pendapatan - pengurang - assa) +"</td>";
                    tr += "</tr>";
                    $('#postsList tbody').append(tr);

                }
            }

            $("#postsList").on("click", ".detail", function(event) {
                var id=$(this).attr('value');
                var idcus=$('#acc_'+id+'').val();
                var start=$('#start').val();
                var end=$('#end').val();
                
                window.open("<?php echo base_url();?>fleet/modaldetailreport?export=excel&start="+start+"&fleetplateno="+idcus+"&end="+end+"");
                return false; 
            });
        });

    </script>
