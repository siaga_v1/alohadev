<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Return DO</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Operational
            </li>
            <li class="active">
                <strong>Surat Jalan Balik</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Surat Jalan Balik</h5>
                    <!--
                    <div class="ibox-tools">
                        <a data-toggle="modal" data-target="#myModal5"> 
                            <i class="fa fa-plus"></i>
                        </a>
                         MODAL INPUT
                        <div class="modal inmodal" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <form class="form-horizontal" id="formspb" action="<?php echo site_url('citY/saveCities')?>" method="POST"   >
                                    <div class="modal-content animated bounceInRight">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            
                                            <h4 class="modal-title">Input Cities</h4>
                                            
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                            if (empty($id))
                                                            {
                                                                $ccode = "19040001";
                                                            } else {
                                                                $key= $id[0]['id'];
                                                                $ccode = $key+1;

                                                            }
                                                        ?>
                                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">ID</label>
                                                    <div class="col-sm-8">
                                                       
                                                        <input class="form-control" name="id" value="<?=$ccode?>" type="text" autocomplete="off" readonly />
                                                    </div>
                                            </div>  
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">City</label>
                                                    <div class="col-sm-8">
                                                       
                                                        <input class="form-control" name="cities"  type="text" autocomplete="off" />
                                                    </div>
                                            </div>  
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            <input type="submit" style="margin-bottom: 5px;" name="submit" class="btn btn-primary" value="Save">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>  
                             MODAL END
                        
                    </div> -->

                </div>
                
                <div class="ibox-content">
                  <form id="form-search-FleetReport" name="form_FleetReport_search" method="post">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label" for="customer">Customer</label>
                          <select class="form-control" id="id_customer" name="id_customer">
                            <option>- Select Customer -</option>
                            <?php foreach ($customer as $key): ?>
                              <option value="<?=$key['id']?>"><?=$key['name']?></option>
                            <?php endforeach ?>
                          </select>

                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label" for="customer">Tanggal</label>
                          <div class="input-daterange input-group" id="datepicker">
                            <input type="text" class="form-control-sm form-control" autocomplete="off" name="start">
                            <span class="input-group-addon">to</span>
                            <input type="text" class="form-control-sm form-control" autocomplete="off" name="end">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label" for="customer">No Kontainer</label>
                          <textarea class='form-control' id="frameno" name="frameno" placeholder="No Kontainer" ></textarea>

                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">

                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
 
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <button type="submit" class="btn btn-info">Search</button>
                          <button type="reset" class="btn btn-warning">Reset</button>
                          <a href="#" id="export-ordertruck-summary" class="btn btn-success"><i class="fa fa-file-excel-o"></i>  Export</a>
                        </div>
                      </div>
                    </div>

                  </form> 
                </div>


                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id='postsList' >
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Doc Return</th>
                                    <th>No Kontainer</th>
                                    <th>Order Date</th>
                                    <th>Customer Name</th>
                                    <th>Origin</th>
                                    <th>Destination</th>
                                    <th>Fleet</th>
                                    <th>Driver</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>

                        </table>
                    <div align="right" > Search Result : <span id='searchresult'></span> record</div>

                    </div>
                    <div  id='pagination'></div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                        </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>



        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>

      <script>
        $(document).ready(function(){

          $('.input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });

           $('#pagination').on('click','a',function(e){
               e.preventDefault(); 
               var pageno = $(this).attr('data-ci-pagination-page');
               loadPagination(pageno);
               $('#postsList tbody').empty();
                     });

              loadPagination(0);
            $('#form-search-FleetReport').submit(function(){
                    loadPagination(0);
                    return false; 
                });
             // Load pagination
             function loadPagination(pagno){
                var param = $('#form-search-FleetReport').serialize();
                $.ajax({
                 url: '<?=base_url()?>returnDo/loadRecord/'+pagno,
                 type: 'post',
                 data:param,
                 dataType: 'json',
                 success: function(response){
                    $('#pagination').html(response.pagination);
                    $('#searchresult').html(response.total);
                    createTable(response.result,response.row);
                 }
               });
             }

     // Create table list
             function createTable(result,sno){
               sno = Number(sno);

               $('#postsList tbody').empty();
               for(index in result){
                  var id = result[index].id;
                  var action = result[index].idol;
                  var code = result[index].code;
                  var date = result[index].orderdate;
                  var origin = result[index].n1;
                  var destination  = result[index].c2;
                  var customer = result[index].cname;
                  var fleet = result[index].fleet;
                  var driver = result[index].driver;
                  var shipment = result[index].nokont;
                  var rdo = result[index].rdo;
                  sno+=1;

                  var tr = "<tr>";
                  //tr += "<td><a class='btn btn-success btn-xs' href='<?php echo base_url()?>returnDO/approve?id="+ action +"'><i class='fa fa-check'></i></a></td>";
                  if (rdo == 'YES') {
                  tr += "<td><a class='btn btn-success btn-xs unapprove' id="+ action +"><i class='fa fa-check'></i></a></td>";

                }else{
                  tr += "<td><a class='btn btn-danger btn-xs approve' id="+ action +"><i class='fa fa-check'></i></a></td>";

                }
                  tr += "<td>"+ rdo +"</td>";
                  tr += "<td>"+ shipment +"</td>";
                  tr += "<td>"+ date +"</td>";
                  tr += "<td>"+ customer +"</td>";
                  tr += "<td>"+ origin +"</td>";
                  tr += "<td>"+ destination +"</td>";
                  tr += "<td>"+ fleet +"</td>";
                  tr += "<td>"+ driver +"</td>";
                  tr += "</tr>";
                  $('#postsList tbody').append(tr);
         
                }
              }

              $("#postsList").on("click", ".approve", function(event) {
                var idcus=$(this).attr('id');
               //alert(pageno);
                //alert(idcus);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('returnDO/approve');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                          var pageno2 = $("#pagination ul.pagination li.active a").html();

                        //alert(pageno2); 
                        loadPagination(pageno2);
                    	return false; 
                },
                });
            });

              $("#postsList").on("click", ".unapprove", function(event) {
                var idcus=$(this).attr('id');
               //alert(pageno);
                //alert(idcus);
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('returnDO/unapprove');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                          var pageno2 = $("#pagination ul.pagination li.active a").html();

                        //alert(pageno2); 
                        loadPagination(pageno2);
                      return false; 
                },
                });
            });

            
            $("#export-ordertruck-summary").click(function(){
               window.open("<?php echo base_url();?>order/exportExcel?export=excel&"+$('#form-search-FleetReport').serialize());
               return false; 
            });

        });

    </script>