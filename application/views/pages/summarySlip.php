<!--NAVBAR ATAS-->
<div class="row border-bottom">
  <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
        <i class="fa fa-bars"></i>
      </a>
    </div>

    <ul class="nav navbar-top-links navbar-right">
      <li>
        <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
      </li>

      <li>
        <a href="<?php echo site_url('login/logout') ?>">
          <i class="fa fa-sign-out"></i> Log out
        </a>
      </li>
    </ul>
  </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Summary Slip</h2>

    <ol class="breadcrumb">
      <li>
        <a href="index.html">Home</a>
      </li>
      <li>
        Master Data
      </li>
      <li class="active">
        <strong>Summary Slip</strong>
      </li>
    </ol>
  </div>

  <div class="col-lg-2">

  </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">


        <div class="ibox-title">
          <h5>Summary Slip</h5>
        </div>

        <div class="ibox-content">
          <form class='form-horizontal' id="form-search-FleetReport" name="form_FleetReport_search" method="POST">
            <div class="form-group">
              <label class='col-sm-2 control-label'>No Invoice</label>
              <div class='col-sm-5'>
                <input type='text' class='form-control' style="display: none" id='kode' name='kode'>
                <input type='text' class='form-control inputan' id='nosummary' name='nosummary' readonly="readonly">
                <input type='text' class='form-control inputan' style="display: none" id='totalinv' name='totalinv' readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class='col-sm-2 control-label'>Customer</label>
              <div class='col-sm-5'>
                <select class="form-control" id="id_customer" name="id_customer">
                  <option>- Select Customer -</option>
                  <?php foreach ($customer as $key) : ?>
                    <option value="<?= $key['id'] ?>"><?= $key['name'] ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>


            <div class="form-group">
              <label class='col-sm-2 control-label'>Tanggal</label>
              <div class='col-sm-8'>
                <div class="input-daterange input-group" id="datepicker">
                  <input type="text" class="form-control-sm form-control" autocomplete="off" name="start">
                  <span class="input-group-addon">to</span>
                  <input type="text" class="form-control-sm form-control" autocomplete="off" name="end">
                </div>
              </div>
            </div>
            <div class="form-group" style="display: none">
              <label class="col-sm-2 control-label">Biaya</label>
              <div class='col-sm-8'>
                <div class="i-checks">
                  <?php echo form_checkbox('Operasional', 1, set_checkbox('Operasional', 1), "id='BCA1'"); ?> Operasional
                  <?php echo form_checkbox('LOLO', 2, set_checkbox('LOLO', 2), "id='BCA1'"); ?> LOLO
                  <?php echo form_checkbox('DEPO', 3, set_checkbox('DEPO', 3), "id='BCA1'"); ?> DEPO
                  <?php echo form_checkbox('PARKIR', 4, set_checkbox('PARKIR', 4), "id='BCA1'"); ?> PARKIR
                  <?php echo form_checkbox('KAWALAN', 5, set_checkbox('KAWALAN', 5), "id='BCA1'"); ?> KAWALAN
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Rekening</label>
              <div class='col-sm-8'>
                <div class="i-checks">
                  <?php echo form_radio('rekening', 1, set_checkbox('rekening', 1), "id='BCA1'"); ?> BCA (SRYANA)
                  <?php echo form_radio('rekening', 2, set_checkbox('rekening', 2), "id='BCA1'"); ?> BCA (CHRISTIAN)
                  <?php echo form_radio('rekening', 3, set_checkbox('rekening', 3), "id='BCA1'"); ?> MANDIRI (CHRISTIAN)
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class='col-sm-2 control-label'></label>
              <div class='col-sm-8'>
                <button type="submit" class="btn btn-info">Search</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <a href="#" id="export-ordertruck-summarys" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Export</a>
              </div>
            </div>
          </form>
        </div>


        <div class="ibox-footer">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id='postsList'>
              <thead>
                <tr>
                  <th>Tgl Order</th>
                  <th>∑DO</th>
                  <th>Type</th>
                  <th>No Kontainer</th>
                  <th>Supir</th>
                  <th>No. Polisi</th>
                  <th>Tujuan</th>
                  <th>Biaya Pengiriman</th>
                  <th>No Inv</th>
                </tr>
              </thead>
              <tbody>
              </tbody>

            </table>
            <div align="right"> Search Result : <span id='searchresult'></span> record</div>

          </div>
          <div style='margin-top: 10px;' id='pagination'></div>
        </div>

        <!-- MODAL EDIT -->
        <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">

        </div>
        <!-- MODAL END -->
      </div>
    </div>
  </div>
</div>



<!-- Mainly scripts -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


<script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<!-- Chosen -->
<script src="<?php echo base_url(); ?>assets/js/plugins/chosen/chosen.jquery.js"></script>

<!-- JSKnob -->
<script src="<?php echo base_url(); ?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

<!-- Input Mask-->
<script src="<?php echo base_url(); ?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<!-- Data picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- NouSlider -->
<script src="<?php echo base_url(); ?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

<!-- Switchery -->
<script src="<?php echo base_url(); ?>assets/js/plugins/switchery/switchery.js"></script>

<!-- IonRangeSlider -->
<script src="<?php echo base_url(); ?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- MENU -->
<script src="<?php echo base_url(); ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Color picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- Clock picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/clockpicker/clockpicker.js"></script>

<!-- Image cropper -->
<script src="<?php echo base_url(); ?>assets/js/plugins/cropper/cropper.min.js"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?php echo base_url(); ?>assets/js/plugins/fullcalendar/moment.min.js"></script>

<!-- Date range picker -->
<script src="<?php echo base_url(); ?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/js/plugins/select2/select2.full.min.js"></script>

<!-- TouchSpin -->
<script src="<?php echo base_url(); ?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

<!-- Tags Input -->
<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<!-- Dual Listbox -->
<script src="<?php echo base_url(); ?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/toastr/toastr.min.js"></script>
<!-- Mainly scripts -->

<script>
  $(document).ready(function() {

    //$('#id_customer').change(function(){
    // var a= $('#id_customer option:selected').text();
    // var b= $('#kode').val();
    // var c = new Date().getFullYear();
    //alert(a);
    //  $('#nosummary').val(""+ b +"-"+ a +"/"+ c +"");
    //});

    $('#id_customer').change(function() {
      var a = $('#id_customer option:selected').text();
      var d = $('#id_customer option:selected').val();
      var b = $('#kode').val();
      var c = new Date().getFullYear();
      $.ajax({
        url: '<?= base_url() ?>summarySlip/getIDInvoice',
        type: 'post',
        data: {
          id: d
        },
        dataType: 'html',
        success: function(data) {
          $('#nosummary').val("" + data + "" + a + "/" + c + "");
        }
      });
      //alert(a);
    });

    $('.i-checks').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green',
    });

    var testini = ' <?= $this->session->userdata('id') ?>';


    $('.postsList').DataTable();

    $('#pagination').on('click', 'a', function(e) {
      e.preventDefault();
      var pageno = $(this).attr('data-ci-pagination-page');
      loadPagination(pageno);

      $('#postsList tbody').empty();
    });

    loadPagination(0);

    $('#form-search-FleetReport').submit(function() {
      loadPagination(0);
      return false;

    });

    $('.input-daterange').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      autoclose: true
    });


    function loadPagination(pageno) {
      var param = $('#form-search-FleetReport').serialize();
      //alert(param);
      $.ajax({
        url: '<?= base_url() ?>summarySlip/search/' + pageno,
        type: 'post',
        data: param,
        dataType: 'json',
        success: function(response) {
          //alert(param);
          $('#pagination').html(response.pagination);
          $('#searchresult').html(response.total);
          createTable(response.result, response.row);

        }
      });
    }



    function createTable(result, sno) {
      sno = Number(sno);
      var as = 0;
      var asa = 0;

      $('#postsList tbody').empty();
      for (index in result) {
        var NoBukti = result[index].noVoucher;
        var TglKirim = result[index].orderdate;
        var TglCetak = result[index].orderdate;
        var DO = result[index].ritase;
        var NoSPPDC = result[index].noshippent;
        var NoRangka = result[index].frameno;
        var Supir = result[index].driver;
        var NoPolisi = result[index].fleet;
        var moda = result[index].moda;
        var Origin = result[index].n1;
        var tujuan = result[index].c2;
        var BiayaPengiriman = result[index].unitprice;
        var NoSummarySlip = result[index].nosummary;
        var ops = result[index].feeOps;
        var lolo = result[index].feeLolo;
        var depo = result[index].feeDepo;
        var kawalan = result[index].feeKawalan;
        var parkir = result[index].feeParkir;
        var storage = result[index].feeStorage;
        var rc = result[index].feeRC;
        var alih = result[index].feeAlih;
        var seal = result[index].feeSeal;
        var cuci = result[index].feeCuci;
        var sp2 = result[index].feeSP2;

        sno += 1;

        var tr = "<tr>";
        tr += "<td>" + TglKirim + "</td>";
        tr += "<td>" + DO + "</td>";
        tr += "<td>" + moda + "</td>";
        tr += "<td>" + NoRangka + "</td>";
        tr += "<td>" + Supir + "</td>";
        tr += "<td>" + NoPolisi + "</td>";
        tr += "<td>" + Origin + " - " + tujuan + "</td>";
        tr += "<td>" + new Intl.NumberFormat('en-ID', {
          maximumSignificantDigits: 3
        }).format(BiayaPengiriman) + "</td>";
        tr += "<td>" + NoSummarySlip + "</td>";
        tr += "</tr>";
        $('#postsList tbody').append(tr);
        as += Number(BiayaPengiriman);
        asa += parseInt(ops) + parseInt(lolo) + parseInt(depo) + parseInt(kawalan) + parseInt(parkir) + parseInt(storage) + parseInt(rc) + parseInt(alih) + parseInt(seal) + parseInt(cuci) + parseInt(sp2);
      }
      $('#totalinv').val(as + asa);
    }

    $("#postsList").on("click", ".detail", function(event) {
      var idcus = $(this).attr('value');
      $.ajax({
        type: "POST",
        url: "<?php echo site_url('order/modaldetail'); ?>",
        dataType: "html",
        data: {
          id: idcus
        },
        success: function(data) {
          $("#modaledit").html(data);
          $("#modaledit").modal();

          //alert(data);
        },
      });
    });


    $("#export-ordertruck-summary").click(function() {
      window.open("<?php echo base_url(); ?>summarySlip/search?export=excel&" + $('#form-search-FleetReport').serialize());
      return false;
    });

    $("#export-ordertruck-summarys").click(function() {
      window.open("<?php echo base_url(); ?>summarySlip/search?export=pdf&" + $('#form-search-FleetReport').serialize());
      return false;
    });

  });
</script>