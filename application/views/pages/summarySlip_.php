<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Summary Slip</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Master Data
            </li>
            <li class="active">
                <strong>Summary Slip</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Summary Slip</h5>
                </div>
                
                <div class="ibox-content">
                  <form class='form-horizontal'  id="form-search-FleetReport" name="form_FleetReport_search" method="POST">
                  	<div class="form-group">
                      <label class='col-sm-2 control-label'>No Summary</label>
                      <div class='col-sm-8'>
                        <input type ='text' class='form-control inputan' id='nosummary' name='nosummary'>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class='col-sm-2 control-label'>Order Code</label>
                      <div class='col-sm-8'>
                        <textarea class='form-control inputan' id='inputan' name='inputan' placeholder="multiple with space"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class='col-sm-2 control-label'>Frame No</label>
                      <div class='col-sm-8'>
                        <textarea class='form-control inputan2' id='inputan2' name='inputan2' placeholder="multiple with space"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class='col-sm-2 control-label'></label>
                      <div class='col-sm-8'>
                        <button type="submit" class="btn btn-info">Search</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <a href="#" id="export-ordertruck-summary" class="btn btn-success"><i class="fa fa-file-excel-o"></i>  Export</a>
                      </div>
                    </div>                    
                  </form> 
                </div>


                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id='postsList' >
                            <thead>
                                <tr>
                                    <th>No Bukti</th>
                                    <th>Tgl Kirim</th>
                                    <th>Tgl Cetak</th>
                                    <th>∑DO</th>
                                    <th>No. SP-PDC</th>
                                    <th>No Rangka</th>
                                    <th>Supir</th>
                                    <th>No. Polisi</th>
                                    <th>Tujuan</th>
                                    <th>Biaya Pengiriman</th>
                                    <th>No Summary Slip</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>

                        </table>
                    </div>
                    <div style='margin-top: 10px;' id='pagination'></div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                        </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>



  <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"><