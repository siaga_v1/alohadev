<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Vechile Tracking Application</span>
            </li>

            <li>
                <a href="<?php echo site_url('login/logout')?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Summary Slip</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                Master Data
            </li>
            <li class="active">
                <strong>Summary Slip</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
       

                <div class="ibox-title">
                    <h5>Summary Slip</h5>
                </div>
                
                <div class="ibox-content">
                  <form class='form-horizontal'  id="form-search-FleetReport" name="form_FleetReport_search" method="POST">
                  	<div class="form-group">
                      <label class='col-sm-2 control-label'>No Summary</label>
                      <div class='col-sm-8'>
                        <input type ='text' class='form-control inputan' id='nosummary' name='nosummary'>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class='col-sm-2 control-label'>DO Number / Ship DOC</label>
                      <div class='col-sm-8'>
                        <textarea class='form-control inputan' id='inputan' name='inputan' placeholder="multiple with space"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class='col-sm-2 control-label'>Frame No</label>
                      <div class='col-sm-8'>
                        <textarea class='form-control inputan2' id='inputan2' name='inputan2' placeholder="multiple with space"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class='col-sm-2 control-label'></label>
                      <div class='col-sm-8'>
                        <button type="submit" class="btn btn-info">Search</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <a href="#" id="export-ordertruck-summary" class="btn btn-success"><i class="fa fa-file-excel-o"></i>  Export</a>
                      </div>
                    </div>                    
                  </form> 
                </div>


                <div class="ibox-footer">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id='postsList' >
                            <thead>
                                <tr>
                                    <th>No Bukti</th>
                                    <th>Tgl Kirim</th>
                                    <th>Tgl Cetak</th>
                                    <th>∑DO</th>
                                    <th>No. SP-PDC</th>
                                    <th>Type</th>
                                    <th>No Rangka</th>
                                    <th>Supir</th>
                                    <th>No. Polisi</th>
                                    <th>Tujuan</th>
                                    <th>Biaya Pengiriman</th>
                                    <th>No Summary Slip</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>

                        </table>
                    <div align="right" > Search Result : <span id='searchresult'></span> record</div>
                        
                    </div>
                    <div style='margin-top: 10px;' id='pagination'></div>
                </div>

                <!-- MODAL EDIT -->
                <div class="modal inmodal" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
                            
                        </div>  
                            <!-- MODAL END -->
            </div>
        </div>
    </div>
</div>



  <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/toastr/toastr.min.js"></script>
  <!-- Mainly scripts -->

      <script>
        $(document).ready(function(){
          
                                  
          var testini = ' <?=$this->session->userdata('id')?>';
          

          $('.postsList').DataTable();

          $('#pagination').on('click','a',function(e){
            e.preventDefault(); 
            var pageno = $(this).attr('data-ci-pagination-page');
            loadPagination(pageno);

            $('#postsList tbody').empty();
          });

          loadPagination(0);

          $('#form-search-FleetReport').submit(function(){
            loadPagination(0);
            return false; 
            
          });


          function loadPagination(pageno){
            var param = $('#form-search-FleetReport').serialize();
            //alert(param);
            $.ajax({
              url: '<?=base_url()?>summarySlip/search/'+ pageno,
              type: 'post',
              data:param,
              dataType: 'json',
              success: function(response){
                  //alert("UHUY");
                  $('#pagination').html(response.pagination);
                    $('#searchresult').html(response.total);
                  createTable(response.result,response.row);
                 
              }
            });
          }

          

          function createTable(result,sno){
            sno = Number(sno);

            $('#postsList tbody').empty();
            for(index in result){
              var NoBukti = result[index].noVoucher;
              var TglKirim = result[index].orderdate;
              var TglCetak = result[index].orderdate;
              var DO = result[index].ritase;
              var NoSPPDC = result[index].noshippent;
              var NoRangka  = result[index].frameno;
              var Supir = result[index].driver;
              var NoPolisi = result[index].fleet;
              var moda = result[index].moda;
              var Origin = result[index].n1;
              var tujuan = result[index].c2;
              var BiayaPengiriman = result[index].unitprice;
              var NoSummarySlip = result[index].nosummary;
              sno+=1;

              var tr = "<tr>";
                  tr += "<td>"+ NoBukti +"</td>";
                  tr += "<td>"+ TglKirim +"</td>";
                  tr += "<td>"+ TglCetak +"</td>";
                  tr += "<td>"+ DO +"</td>";
                  tr += "<td>"+ NoSPPDC +"</td>";
                  tr += "<td>"+ moda +"</td>";
                  tr += "<td>"+ NoRangka +"</td>";
                  tr += "<td>"+ Supir +"</td>";
                  tr += "<td>"+ NoPolisi +"</td>";
                  tr += "<td>"+ Origin +" - "+ tujuan +"</td>";
                  tr += "<td>"+ BiayaPengiriman +"</td>";
                  tr += "<td>"+ NoSummarySlip +"</td>";
                  tr += "</tr>";
                  $('#postsList tbody').append(tr);
         
                }
              }

              $("#postsList").on("click", ".detail", function(event) {
                var idcus=$(this).attr('value');
                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('order/modaldetail');?>",
                    dataType: "html",
                    data: {
                        id:idcus
                        },
                    success:function(data){
                        $("#modaledit").html(data);
                        $("#modaledit").modal();

                        //alert(data);
                },
                });
            });

            
            $("#export-ordertruck-summary").click(function(){
               window.open("<?php echo base_url();?>summarySlip/search?export=excel&"+$('#form-search-FleetReport').serialize());
               return false; 
            });

        });

    </script>