<h5 class="text-bold">Filter : 
<?php 
 $filtertext = "";
 if($this->input->post('fleetplateno') != '') { $filtertext .= "Plateno = ".$this->input->post('fleetplateno'); }
 if($this->input->post('type') != '') { $filtertext .= "&nbsp;Type = ".$this->input->post('type'); }
 if($this->input->post('customer') != '') { $filtertext .= "&nbsp;Customer = ".$this->input->post('customer'); }
 if($this->input->post('code') != '') { $filtertext .= "&nbsp;Fleet Code = ".$this->input->post('code'); }
 if($this->input->post('year') != '') { $filtertext .= "&nbsp;Year = ".$this->input->post('year'); }
 if($this->input->post('month') != '') { $filtertext .= "&nbsp;Month = ".date('F', strtotime("2018-".$this->input->post('month')."-01")); }
 echo ($filtertext != "" ? $filtertext : "All Data");
?></h5>
<table class="table table-hover table-bordered">
    <thead>
        <tr>
            <th width="5%">#</th>
            <th>Fleet Plateno</th>
            <th>Ritase</th>
            <th>Load Qty</th>
            <th>Pendapatan (A)</th>
            <th>Uang Jalan (B)</th>
            <th>A - B</th>
            <th>Sparepart (C)</th>
            <th>A - B - C</th>
            <th>OPS - GAJI - SEWA (D)</th>
            <th>Laba (A - B - C - D)</th>
            <th>Revisi Pembayaran Tagihan</th>
            <th>Laba/Mobil</th>
        </tr>
    </thead>
    <tbody>
    <?php 
    $ritase = 0;
    $loadqty = 0;
    $allowance = 0;
    $allowanceadds = 0;
    $allowancereds = 0;
    $prices = 0;
    if($rows != null){
        $no=($page > 1) ? (($limit*($page-1))+1)  : 1;
        foreach($rows AS $row) {
    
    ?>
        <tr>
            <td align="center"><?php echo $no; ?></td>
            <td><?=$row->fleetplateno; ?></td>
            <td><?=$row->ritase;?></td>
            <td><?=$row->loadqty;?></td>
            <td><?=number_format($row->allowances);?></td>
            <td><?=number_format($row->prices);?></td>
            <td><?=number_format($row->allowances-$row->prices);?></td>
            <td><?=number_format($row->Maintenance);?></td>
            <td><?=number_format($row->allowances-($row->prices+$row->allowanceadds))?></td>
        </tr>
    <?php 
            $ritase += $row->ritase;
            $loadqty += $row->loadqty;
            $allowance += $row->allowances;
            $allowanceadds += $row->allowanceadds;
            $prices += $row->prices;
            $no++;
         } 
        }else{ ?>
        <tr>
            <td colspan="6">Data not found</td>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="2">Total</th>
            <th><?=$ritase;?></th>
            <th><?=$loadqty;?></th>
            <th><?=number_format($allowance);?></th>
            <th><?=number_format($prices);?></th>
            <th><?=number_format($allowance-$prices);?></th>
            <th>0</th>
            <th><?=number_format($allowance-($prices+$allowanceadds));?></th>
    </tfoot>
</table>
<div class="box-footer">
    <?php echo (isset($paginator)) ? $paginator : ''; ?>
</div>

<script type="text/javascript">
$('#table-FleetReport ul li a').click(function(){
       var url = $(this).attr('href');
       loadFleetReportList(url);
       return false; 
    });
</script>