<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | ANR.</span>
            </li>

            <li>
                <a href="login.html">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Maintenace</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a href="<?php echo site_url('SuratPermintaanBarang')?>">Maintenace</a>
            </li>
            <li class="active">
                <strong>Tambah Maintenance</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
<form class="form-horizontal" id="formspb" action="<?php echo site_url('masterMaintenance/tambahMaintenance')?>" method="POST">
    <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>Form Maintenance</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            
                                <?php
                                date_default_timezone_set("Asia/Jakarta");
                                  $nowdate = date('ydmGis');
                                    $now=date('d-m-Y');
                                    ?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No Maintence</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="idmaintenance" value="WO-<?php echo$nowdate; ?>" type="text" readonly="readonly" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tanggal</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" readonly="readonly  " type="text" name="tanggal" value="<?php echo $now  ;?>" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Driver</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="driver">
                                            <option value="0">- Pilih -</option>
                                            <?php
                                                foreach ($driver as $drv ) {
                                                    ?>
                                                       <option value="<?php echo $drv['emp_id']?>">(<?php echo $drv['emp_code'];?>) <?php echo $drv['emp_nama'];?> - <?php echo $drv['kat_emp_name'];
                                                       ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Armada</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="armada">
                                            <option value="0">- Pilih -</option>
                                            <?php
                                                foreach ($armada as $armsw ) {
                                                    ?>
                                                        <option value="<?php echo $armsw['arm_id']?>"><?php echo $armsw['arm_nomor_pol']; ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kategori Maintenace</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="kategori">
                                            <option value="0">- Pilih Kategori -</option>
                                            <option value="UNDER CARRIAGE">UNDER CARRIAGE</option>
                                            <option value="LUBRIKASI">LUBRIKASI</option>
                                            <option value="BRAKE SYSTEM">BRAKE SYSTEM</option>
                                            <option value="SERVICE BERKALA">SERVICE BERKALA</option>
                                            <option value="ELECTRIC">ELECTRIC</option>
                                            <option value="ENGINE">ENGINE</option>
                                            <option value="WHEEL">WHEEL</option>
                                            <option value="FIFTH WHEEL">FIFTH WHEEL</option>
                                            <option value="HOSE">HOSE</option>
                                            <option value="TRANSMISI">TRANSMISI</option>
                                            <option value="STEERING">STEERING</option>
                                            <option value="CLUTCH">CLUTCH</option>
                                            <option value="AC">AC</option>
                                            <option value="RADIATOR">RADIATOR</option>
                                            <option value="BOOM">BOOM</option>
                                            <option value="CABIN">CABIN</option>
                                            <option value="CHASIS">CHASIS</option>
                                            <option value="SPRING">SPRING</option>
                                            <option value="OTHERS">OTHERS</option>
                                        </select>
                                    </div>
                                </div>                         

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Note</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" id="Note" name="note" ></textarea>
                                    </div>
                                </div>

                                <div class="form-group" id="data_5">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-8">
                                        <button class="btn btn-info pull-right" id="tombol">Tambah Maintenance</button>
                                    </div>
                                </div>
                        </div>
                    </div>                  

                </div>
            </div>
            </form>
        </div>
        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>


<script>

    $(document).ready(function(){

        
    });
</script>