<!--NAVBAR ATAS-->
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Fleet Management System | ANR</span>
            </li>

            <li>
                <a href="login.html">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
<!--NAVBAR ATAS-->

<!--BREADCRUMB-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Permintaan Barang</h2>
        
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a href="<?php echo site_url('SuratPermintaanBarang')?>">Permintaan Barang</a>
            </li>
            <li class="active">
                <strong>Tambah Permintaan Barang</strong>
            </li>
        </ol>
    </div>

    <div class="col-lg-2">

    </div>

</div>
<!--BREADCRUMB-->

<!--CONTENT-->
<div class="wrapper wrapper-content animated fadeInRight">
<form class="form-horizontal" id="formspb" action="<?php echo site_url('SuratPermintaanBarang/simpanPermintaan')?>" method="POST"   >
    <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>Form Permintaan Barang</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            
                                <?php
                                date_default_timezone_set("Asia/Jakarta");
                                  $nowdate = date('ydmGis');

                                    
                                    //echo $nmr;
                                    
                                    if (date('d')=='001' && empty($nomor))
                                        { 
                                            $a = '001'; 
                                        }
                                        elseif($nomor != NULL)
                                        { 
                                            $nmr=$nomor[0]['dpb_code'];
                                            $ex = explode('/', $nmr);
                                            $re = $ex[0];
                                            $rex=  explode('-', $re);
                                            $a = sprintf("%03d", $rex[1]+1); 
                                            //$a = sprintf("%02d", $ex[0]+1); 
                                        }else
                                        { 
                                            $a = '001'; 
                                        }
                                    $now=date('d-m-Y');
                                    $c = array('','I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
                                    $tahun=date('y');

                                ?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No SPB</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="idspb" value="SPB-<?=$a?>/<?=$c[date('n')]?>/<?=$tahun?>" type="text" readonly="readonly" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tanggal</label>
                                    <div class="col-sm-8">

                                    
                                        <input class="form-control" readonly="readonly  " type="text" name="tanggal" value="<?php echo $now  ;?>" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Armada</label>
                                    <div class="col-sm-4">
                                        <select data-placeholder="Pilih Armada..." class="form-control" name="armada" id="armada" tabindex="2">
                                        
                                            <option value="0">- Pilih -</option>
                                            <?php
                                                foreach ($armada as $armsw ) {
                                                    ?>
                                                        <option value="<?=$armsw['arm_id']?>"><?php echo $armsw['arm_nomor_pol']; ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                        
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="merk" id="merk" readonly="readonly" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Pemohon</label>
                                    <div class="col-sm-8">
                                        <select data-placeholder="Pilih User..." class="chosen-select" name="user" id="user" tabindex="2">
                                            <option value="0">- Pilih User -</option>
                                            <?php
                                                foreach ($employee as $kary ) {
                                                    ?>
                                                        <option value="<?php echo $kary['emp_id']?>"> <?php echo $kary['emp_nama'];?> - (<?php echo $kary['emp_code'];?>)</option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Item</label>
                                    <div class="col-sm-2">
                                        <select data-placeholder="Pilih Item..." class="chosen-select" name="kategori" id="kategori" tabindex="2">
                                            <option value="0">- Kategori -</option>
                                            <?php
                                                foreach ($kategori as $kat ) {
                                                    ?>
                                                        <option value="<?php echo $kat['im_id']?>"> <?php echo $kat['im_name'];?> </option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <select data-placeholder="Pilih Item..." class="chosen-select form-control" name="item" id="item" tabindex="1">
                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Qty</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" id="Qty" autocomplete="off"/><span id="errmsga"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Note</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" id="Note" ></textarea>
                                    </div>
                                </div>

                                <div class="form-group" id="data_5">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-8">
                                        <button class="btn btn-info pull-right" id="tombol">Tambah Item</button>
                                    </div>
                                </div>r
                        </div>
                    </div>

                    <div class="ibox float-e-margins" id="boxitem">
                        <div class="ibox-content">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Qty</th>
                                        <th>Note</th>
                                        <th>-</th>
                                    </tr>
                                </thead>
                                <tbody id="tabel">
                                    
                                </tbody>
                            </table>  
                        </div>

                        <div class="ibox-footer">
                            <button type="submit" class="btn btn-info">Simpan</button>
                        </div>  
                    </div>
                </div>
            </div>
            </form>
        </div>
        <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Chosen -->
    <script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo base_url();?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo base_url();?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo base_url();?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo base_url();?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url();?>assets/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo base_url();?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="<?php echo base_url();?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>


    <script>
     
        $(document).ready(function(){
           

            $('#item').chosen({width: "100%"});
            $('#kategori').chosen({width: "100%"});
            $('#armada').chosen({width: "100%"});
            $('#user').chosen({width: "100%"});
            
    //$('#item').chosen({width: "100%"});
            $("#armada").change(function(){
                var ini=$("#armada option:selected").val();
                //alert(ini);
                //$("#merk").val(ini);

                $.ajax({
                type:"POST",
                url: "<?php echo site_url('SuratPermintaanBarang/getMerk');?>",
                dataType: "html",
                data: {
                      idarm:ini
                      },
                  success:function(data){
                      $("#merk").val(data);
                      //  alert(data);
                    },
                
                });
            });

         $("#user").change(function(){
            var ini=$(this).val();
            //alert(ini);
        });

        $("#kategori").change(function(){
            var ini=$(this).val();
            //alert(ini);

                $.ajax({
                    type:"POST",
                    url: "<?php echo site_url('SuratPermintaanBarang/getItem');?>",
                    dataType: "html",
                    data: {
                        labi:ini
                  },
                  success:function(dat){
                    $("#item").html(dat);
                    $("#item").trigger("chosen:updated");
                },
            });
       
        });

    $("#Qty").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Masukkan Angka").show().fadeOut("slow");
               return false;
        }
    });

        $("#tombol").attr('disabled','disabled');
        $("#boxitem").attr('hidden','hidden');

            $('#Note').focus(function(){
                //alert("bisa kok");
            var item = $("#item :selected").val();
            var qty = $("#Qty").val();
            var note = $("#Note").val();
            
                if(item=="0"||qty==" "||note==" "){
                $("#tombol").attr('disabled','disabled');
            }
            else{
                $("#tombol").removeAttr('disabled');
           }
            });
            
        
        
        $("#tombol").click(function(){
            
            var item = $("#item :selected").text();
            var itemv = $("#item :selected").val();
            var qty = $("#Qty").val();
            var stock= $("#stock").val();
            var note = $("#Note").val();
            var minus = stock - qty ; 

            var num=$("#tabel tr").length;
            var hed="<tr>";
            var col2="<td><input type='hidden' name='detail["+num+"][items]' id='item"+num+"' value='"+itemv+"' />"+ item +"</td>";
            var col3="<td><input type='hidden' name='detail["+num+"][qty]' id='qty"+num+"' value='"+qty+"' />"+ qty +"</td>";
            var col4="<td><input type='hidden' name='detail["+num+"][note]' id='note"+num+"' value='"+note+"' />"+ note +"</td>";
            var act="<td><button type='submit' value='"+num+"' class='btn btn-danger fa fa-minus-square' id='btndelete'></button></td>";
            var fot="<tr>";
            
            var apd = hed+col2+col3+col4+act+fot;
        
            $("#tabel").append(apd)
           
             $('#item').trigger("chosen:updated");
            $('#Qty').val("");
            $('#stock').val("");
            //alert("bisa kok");
           
            $('#Note').val("");
            
            $("#tombol").attr('disabled','disabled');
                $("#boxitem").removeAttr('hidden');

            return false;

           // $('#Qty').val(" ");
        });

        $(document).on('click','#btndelete',function(){
        var i = $(this).val();
        var item = '#item'+i+ '';;
        var qty = '#qty'+i+ '';;
        var vitem = $(item).val();
        var vqty = $(qty).val();
        //alert (test);
        $.ajax({
                type:"POST",
                url: "<?php echo site_url('SuratPermintaanBarang/getStock');?>",
                dataType: "html",
                data: {
                    labi:vitem,
                    },
                success:function(data){
                    //$("#start").html(data);
                    var a = Number(data);
                    var b = Number(vqty);
                    var hasil = a + b;
                    //alert(hasil);
                    $.ajax({
                        type:"POST",
                        url: "<?php echo site_url('SuratPermintaanBarang/plusStock');?>",
                        dataType: "html",
                        data: {
                            qty:hasil,
                            item:vitem
                            },
                            success:function(){
                            //$("#start").html(data);
                            
                            //alert(hasil);
                        },
                    });
            },
            });
        var vtr = $(this).parent().parent();
        vtr.remove();
        });
        
        
        
        
        
        
    });
</script>