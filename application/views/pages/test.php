<style>
body { 
    font-family:calibri;
    height: auto;
    font-size:8pt;
}

h3{
    color:orange;
}

h1{
    font-size: 13pt;
    margin:0pt;
}
.text-red{
    font-weight: bolder;
    color:red;
}
.detail-desc{
    font-size: 7pt;
}
.detail-invoice{
    border-collapse: collapse;
    border-color: silver;
    border-width: thin;
    font-size: 7pt;
}

.detail-invoice tbody td{
    font-size: 8pt;
}
.footer{
    font-size: 8pt;
}
table{
    border-collapse: collapse;
}

hr{
    border-collapse: collapse;
    border: 1px solid black;
}
#code-text{
    width:100%;
    float: right;
    text-align: right;
}
</style>
<body>
<div style = "width:100%">
        <!--
         <div style = "width:100%">
            <table border="1" width="100%">
            <tr>
        <td align="center"><b>No</b></th>
        <td align="center"><b>Kode Barang</b> </th>
        <td align="center"><b>Nama Barang</b> </th>
        <td align="center"><b>Qty</b> </th>
        <td align="center"><b>Set</b> </th>
        <td align="center"><b>Harga</b> </th>
        <td align="center"><b>Harga Total</b> </th>
        <td align="center"><b>Keterangan</b> </th>
            </tr>
            
            <tr>
        <td align="center"><b>No</b></th>
        <td align="center"></th>
        <td align="center"></th>
        <td align="center"></th>
        <td align="center">pcs </th>
        <td align="center"> </th>
        <td align="center"></th>
        <td align="center"><b>Keterangan</b> </th>
            </tr>
            <tr>
        <td align="center" colspan="4" rowspan="4"><b>Note</b><br><br><br><br><br></td>
        <td align="left" colspan="2"> Total</td>
        <td align="left" colspan="2"></td>
            </tr>
            
            <tr>
                <td align="left" colspan="2">Discount</td>
                <td align="left" colspan="2"></td>
            </tr>
            <tr>
                <td align="left" colspan="2">PPN</td>
                <td align="left" colspan="2"></td>
            </tr>
            <tr>
                <td align="left" colspan="2">Grand Total</td>
                <td align="left" colspan="2"></td>
            </tr>
        </table>
         </div>-->
            
         <div style = "width:35%; float:left; padding: 2px">
            <table border='1' width="100%">
                <tr>
                    <td align="center"><h1>Purchase Order</h1></td>
                </tr>
                <tr>
                    <td align="center">ANR-LOG-FP3-02 Rev.002</td>
                </tr>
            </table>
         </div>
            
         <div style = "width:40%; float:left;" >
            
         </div>
        
         <div style = "width:35%; float:right;">
            <table border='0' width="100%">
                <tr>
                    <td align="center"><h1>Logo ANR</h1></td>
                </tr>
            </table>
         </div>
         <div style = "width:35%; float:left;clear: both; padding: 2px">
            <table border='1' width="100%" align="right">
                <tr>
                    <td>No. PO :  </td>
                </tr>
            </table>
            <div ><b>NAMA SUPPLIER :</b></div>
            <table border='1' width="100%">
                <tr>
                    <td>Kepada :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>UP :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Alamat :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Telepon :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Email :</td>
                    <td>Bintang Cosmos</td>
                </tr>
            </table>
        </div>

        <div style = "width:35%; float:right; padding: 2px">
            <table border='1' width="100%" align="right">
                <tr>
                    <td>Tanggal : </td>
                </tr>
            </table>
            <div ><b>DEPT. PURCHASING :</b></div>
            <table border='1' width="100%" align="right">
                <tr>
                    <td colspan="2">Nama Purchasing</td>
                </tr>
                <tr>
                    <td colspan="2">Tel. 081288277065 - 021-22732200</td>
                </tr>
                <tr>
                    <td colspan="2">Jl. Bonjol No.80 Bintaro SEK. 3A, <br>
                        Tangerang Selatan
                        </td>
                </tr>
                <tr>
                    <td colspan="2">ANR@ariefnusa.co.id</td>
                </tr>
                <tr>
                    <td colspan="2">purhcasing@ariefnusa.co.id</td>
                </tr>
            </table>
         </div>
         <div style = "width:100%; clear:both; padding: 2px ">
            <table border="1" width="100%">
            <tr>
        <td align="center"><b>No</b></th>
        <td align="center"><b>Kode Barang</b> </th>
        <td align="center"><b>Nama Barang</b> </th>
        <td align="center"><b>Qty</b> </th>
        <td align="center"><b>Set</b> </th>
        <td align="center"><b>Harga</b> </th>
        <td align="center"><b>Harga Total</b> </th>
        <td align="center"><b>Keterangan</b> </th>
            </tr>
            
            <tr>
        <td align="center"><b>No</b></th>
        <td align="center"></th>
        <td align="center"></th>
        <td align="center"></th>
        <td align="center">pcs </th>
        <td align="center"> </th>
        <td align="center"></th>
        <td align="center"><b>Keterangan</b> </th>
            </tr>
            <tr>
        <td align="center" colspan="4" rowspan="4"><b>Note</b><br><br><br><br><br></td>
        <td align="left" colspan="2"> Total</td>
        <td align="left" colspan="2"></td>
            </tr>
            
            <tr>
                <td align="left" colspan="2">Discount</td>
                <td align="left" colspan="2"></td>
            </tr>
            <tr>
                <td align="left" colspan="2">PPN</td>
                <td align="left" colspan="2"></td>
            </tr>
            <tr>
                <td align="left" colspan="2">Grand Total</td>
                <td align="left" colspan="2"></td>
            </tr>
        </table>
         </div>
         <div style = " clear:both; padding: 2px">
           <table border="1" width="100%">
                <tr>
                    <td align="center"><b>Direktur Utama

                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        Tri Rahayu Ratna Sari
                    </b>
                    </td>
                    <td align="center"><b>Dept. Purchasing

                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        Rizqi Yaumal Fitrah
                    </b>
                    </td>
                </tr>

            </table>
         </div>

            
      </div>
            
         
        
         
<!--
<table style="width: 100%;">
    <tr>
        <td>
            <table border='1' width="70%">
                <tr>
                    <td align="center"><h1>Purchase Order</h1></td>
                    
                </tr>
                <tr>
                    <td>ANR-LOG-FP3-02 Rev.002</td>
                </tr>
            </table>
        </td>
        <td>
            <table  width="100%">
                <tr>
                    <td align="center"></td>
                    
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
        </td>
        <td>
            <table border='0' width="100%">
                <tr rowspan='2'>
                    <td align="center"><h1>Logo ANR</h1></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td></td>
    </tr>

    <tr>
        <td>
            <table border='1' width="70%">
                <tr>
                    <td></td>
                </tr>
            </table>
        </td>
        <td>
            <table border='0' width="100%">
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </td>
        <td>
            <table border='1' width="60%" align="right">
                <tr>
                    <td>Tanggal : </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="70%">
                <tr>
                    <td><b>NAMA SUPPLIER :</b></td>
                </tr>
            </table>
        </td>
        <td>
            <table width="100%">
                <tr>
                    <td><b></b></td>
                </tr>
            </table>
        </td>
        <td>
            <table width="60%" align="right">
                <tr>
                    <td><b>DEPT. PURCHASING :</b></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border='1' width="70%">
                <tr>
                    <td>Kepada :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>UP :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Alamat :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Telepon :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Email :</td>
                    <td>Bintang Cosmos</td>
                </tr>
            </table>
        </td>
        <td> </td>
        <td>
             <table border='1' width="60%" align="right">
                <tr>
                    <td colspan="2">Nama Purchasing</td>
                </tr>
                <tr>
                    <td colspan="2">Tel. 081288277065 - 021-22732200</td>
                </tr>
                <tr>
                    <td colspan="2">Jl. Bonjol No.80 Bintaro SEK. 3A, <br>
                        Tangerang Selatan
                        </td>
                </tr>
                <tr>
                    <td colspan="2">ANR@ariefnusa.co.id</td>
                </tr>
                <tr>
                    <td colspan="2">purhcasing@ariefnusa.co.id</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <table border="1" width="100%">
            <tr>
        <td align="center"><b>No</b></th>
        <td align="center"><b>Kode Barang</b> </th>
        <td align="center"><b>Nama Barang</b> </th>
        <td align="center"><b>Qty</b> </th>
        <td align="center"><b>Set</b> </th>
        <td align="center"><b>Harga</b> </th>
        <td align="center"><b>Harga Total</b> </th>
        <td align="center"><b>Keterangan</b> </th>
            </tr>
            
            <tr>
        <td align="center"><b>No</b></th>
        <td align="center"></th>
        <td align="center"></th>
        <td align="center"></th>
        <td align="center">pcs </th>
        <td align="center"> </th>
        <td align="center"></th>
        <td align="center"><b>Keterangan</b> </th>
            </tr>
            <tr>
        <td align="center" colspan="4" rowspan="4"><b>Note</b><br><br><br><br><br></td>
        <td align="left" colspan="2"> Total</td>
        <td align="left" colspan="2"></td>
            </tr>
            
            <tr>
                <td align="left" colspan="2">Discount</td>
                <td align="left" colspan="2"></td>
            </tr>
            <tr>
                <td align="left" colspan="2">PPN</td>
                <td align="left" colspan="2"></td>
            </tr>
            <tr>
                <td align="left" colspan="2">Grand Total</td>
                <td align="left" colspan="2"></td>
            </tr>
        </table>
    </tr>
    <tr>
        <td align="left">&nbsp;</td>
        
    </tr>
    <tr>
        <td>
            <table border="1" width="100%">
                <tr>
                    <td align="center"><b>Direktur Utama

                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        Tri Rahayu Ratna Sari
                    </b>
                    </td>
                    <td align="center"><b>Dept. Purchasing

                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        Rizqi Yaumal Fitrah
                    </b>
                    </td>
                </tr>

            </table>
        </td>
        
    </tr>
  
    
</table>
-->